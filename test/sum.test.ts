//@ts-ignore
import { expect } from 'chai';
//@ts-ignore
import Form from '../src/components/form2';

const fields = [
  {
    name: '1a',
    value: ' ',
    label: '1aa',
  },
  {
    name: '2a',
    value: ' ',
    label: '2aa',
  },
  {
    name: '3a',
    value: ' ',
    label: '3aa',
  },
];

class NewForm extends Form {}
//@ts-ignore
export default new NewForm({ fields }, { name: 'Fixes-425' });

describe('Check Fixes $425 labels', () => {
  it('$425 labels() check', () =>
    //@ts-ignore
    expect(NewForm.labels()).to.be.deep.equal({
      '1a': '1aa',
      '2a': '2aa',
      '3a': '3aa',
    }));
});
