import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
// The Solid plugin exposes a babel option if you wish to jump on the same transformation. Only applies to JSX files.
// Alternatively could probably use something like rollup babel plugin.
import * as clsxTransform from 'babel-plugin-optimize-clsx';

export default defineConfig({
  plugins: [solidPlugin({ babel: clsxTransform })],
  build: {
    target: 'esnext',
    polyfillDynamicImport: false,
  },
  server: {
    host: '0.0.0.0'
  }
});
