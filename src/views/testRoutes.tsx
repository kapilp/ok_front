import { onCleanup } from 'solid-js';
import { Ws } from '../utils/ws_events_dispatcher';
import { ET, IS_PRODUCTION } from '../utils/enums';
import { Button } from '../components/UI/core/button/src/Button';

export function TestRoute() {
  const onRestart = () => {
    Ws.bindT([ET[ET.get], 'restart_server', Ws.uid], d => 0, null);
  };

  const onTestEmail = () => {
    Ws.bindT([ET[ET.get], 'test_mail', Ws.uid], d => 0, null);
  };

  const onReadLog = () => {
    Ws.bindT([ET[ET.get], 'read_log', Ws.uid], d => /* log = d */ 0, null);
  };

  // TODO currently this feature is disabled in backend
  const online_user_evt = [ET[ET.subscribe], 'subscribe_online_users', Ws.uid];
  const onLineUsers = () => {
    Ws.bindT(online_user_evt, d => /* users = d */ 0, 0, 1);
  };
  // onLineUsers(); // temporary not subscribe
  onCleanup(() => {
    // Ws.trigger([[[ET[ET.unsubscribe], 'subscribe_online_users', online_user_evt[2]], {}]]);
    Ws.unbind(online_user_evt);
  });

  const getCountries = () => {
    Ws.bindT([ET[ET.get], 'countries', Ws.uid], d => d, 0);
  };

  const getLanguages = () => {
    Ws.bindT([ET[ET.get], 'languages', Ws.uid], d => d, 0);
  };

  const getCollectionCount = () => {
    Ws.bindT([ET[ET.get], 'collections_size', Ws.uid], d => d, 0);
  };

  const getChatStatistics = () => {
    Ws.bindT(
      [ET[ET.get], 'chat_statistics', Ws.uid],
      d => {
        console.log(d);
      },
      0,
    );
  };
  if (!IS_PRODUCTION) {
    return (
      <div>
        <div>
          <Button onClick={onRestart} text="Restart Server" />
          <Button onClick={onTestEmail} text="Test Email" />
          <Button onClick={onReadLog} text="Read Log" />
          <Button onClick={getCountries} text="Countries" />
          <Button onClick={getLanguages} text="Languages" />
          <Button onClick={getCollectionCount} text="Collection count" />
          <Button onClick={getChatStatistics} text="Chat Statistics" />
        </div>
        <pre>main tasks: 1. Notification(Activity) 2. Search 3.Forum 4. Wiki 5. Task Management 6. Git 7. Payment Gateway</pre>
      </div>
    );
  }
  return <div />;
}
