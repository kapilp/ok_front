import { createComputed, Match, onCleanup, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Redirect } from '@rturnq/solid-router';
import { Skeleton } from '../components/UI/Skeleton';
import { ET, FailOrSuccessResult, GeneralError, LogoutError, WsStatus } from '../utils/enums';
import { getWsConnected, Ws } from '../utils/ws_events_dispatcher';
import { Alert, AlertTypes } from '../components/UI/core/alert/src/Alert';

export const Logout = () => {
  const routeParams = {}; // todo fix this

  const [state, setState] = createStore({
    header: 'Logging out',
    er: 'Please wait ...',
    // subtitle: '',
    logging_out: true,
  });
  const logoutEvent = [ET[ET.insert], 'logout', Ws.uid];

  onCleanup(Ws.bind$(logoutEvent, onLogout, 1));

  createComputed(() => {
    if (getWsConnected()) {
      setState({ er: GeneralError.NO_ERROR });
      Ws.trigger([[logoutEvent, routeParams]]);
    } else {
      setState({ er: WsStatus.RECONNECTION });
    }
  });

  function onLogout(d: FailOrSuccessResult) {
    if (d.error) {
      setState({ header: GeneralError.ERROR_HEADER });
      setState({ er: d.description });
    } else {
      setState({ logging_out: false });
      setState({ header: LogoutError.LogoutHeader });
      setState({ er: LogoutError.LogoutMessage });
    }
  }

  return (
    <>
      <div class="header">
        <h1>{state.header}</h1>
        {state.er && <Alert type={AlertTypes.ERROR}>{state.er}</Alert>}
      </div>

      <Switch>
        <Match when={state.logging_out}>
          <Skeleton />
          Logging out...
        </Match>
        <Match when={!state.logging_out}>
          <div class="signin">
            <p>
              Sign in
              <Redirect href="/super_login" />
            </p>
          </div>
        </Match>
      </Switch>
    </>
  );
};
