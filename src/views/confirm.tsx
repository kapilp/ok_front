/**
    * This Component does 2 things:
    * 1. Wait for confirmation to be done and redirect to /
    * 2. show the confirmation status. user can request reconfirmation email

    /todo if member is already confirmed and visit this page, it should say that email is already verified.
    /todo add confirmation page in menu if not confirmed, to resend verification email.
    */

import { onCleanup, Show } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ConfirmError, ET, GeneralError, isEr } from '../utils/enums';
import { getQueryParams } from '../utils/url';
import { Ws } from '../utils/ws_events_dispatcher';
import { Alert, AlertTypes } from '../components/UI/core/alert/src/Alert';

export const Confirm = () => {
  const queryParams = getQueryParams();

  const [state, setState] = createStore({
    header: '',
    er: '',
    subtitle: '',
    confirmed: false,
  });

  const { uid } = Ws;
  onCleanup(
    Ws.bindT(
      [ET[ET.subscribe], 'confirm_email_status', uid],
      d => {
        if (!isEr(d, state, setState)) {
          confirmEmail();
        }
      },
      null,
      0,
    ),
  );
  onCleanup(() => {
    Ws.trigger([[[ET[ET.unsubscribe], 'confirm_email_status', uid], {}]]);
  });
  const confirmEmail = () => {
    setState({ confirmed: true });
    setState({ header: ConfirmError.ConfirmedHeader });
    setState({ er: ConfirmError.EmailConfirmed });
  };

  /** try to confirm email */
  if (queryParams?.token) {
    onCleanup(
      Ws.bindT(
        [ET[ET.insert], 'confirm_email', Ws.uid],
        d => {
          if (!isEr(d, state, setState)) {
            confirmEmail();
          }
        },
        queryParams,
        0,
      ),
    );
  }

  if (queryParams?.token) {
    setState({ header: ConfirmError.ConfirmingHeader });
    setState({ subtitle: GeneralError.WAIT });
  } else {
    setState({ header: ConfirmError.ConfirmingHeader });
    setState({ subtitle: ConfirmError.CheckInbox });
  }

  return (
    <>
      <div class="header">
        <h1>{state.header}</h1>
        <h3>{state.subtitle}</h3>
        {state.er && <Alert type={AlertTypes.ERROR}>{state.er}</Alert>}
      </div>
      <Show when={state.confirmed}>Your Email is successfully confirmed</Show>
    </>
  );
};
