import { has } from 'rambda';

// [1, ' - ',2], [...]
export function stringifyRawPattern(pattern: Array<string>, row: { [key: string]: string | number | boolean }) {
  let str = '';
  pattern.forEach(x => {
    if (has(x, row)) {
      str += row[x];
    } else {
      str += x;
    }
  });
  return str;
}
