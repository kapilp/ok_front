// https://github.com/micro-js/is-undefined/blob/master/lib/index.js
function isUndefined(value: any) {
  return typeof value === 'undefined';
}
// https://github.com/micro-js/cookies-enabled/blob/master/lib/index.js
function cookiesEnabled() {
  if (!isUndefined(navigator.cookieEnabled) && navigator.cookieEnabled) {
    document.cookie = 'testCookie=null';
    const enabled = document.cookie.indexOf('testCookie') !== -1;
    document.cookie = `testCookie=null;expires=${new Date(+new Date() - 1)}`;
    return enabled;
  }

  return false;
}
