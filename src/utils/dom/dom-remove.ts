// https://github.com/nk-components/dom-remove/blob/master/index.js
/* Not working because of every import
const _toString = {}.toString;
const every = require('every');
export function remove(el) {
  if (!el) return;
  if ('[object Array]' === _toString.call(el)) {
    return every(el, remove);
  }
  const parent = el.parentNode;
  if (parent) {
    parent.removeChild(el);
    return true;
  }
  return false;
};
*/
// https://github.com/hughsk/remove-element/blob/master/index.js
export function remove(element: HTMLElement) {
  if (element && element.parentNode) element.parentNode.removeChild(element);

  return element;
}
