import { JSX, createEffect, createSignal } from 'solid-js';
//really simple combinator that acts like createMemo for async functions i've found very useful:
// mike: https://discord.com/channels/722131463138705510/734440921776783391/741255983027847248
export function useAsync<T>(asyncFn: () => Promise<T>): () => T | undefined {
  const [getter, setter] = createSignal<T | undefined>(undefined);
  createEffect(() => asyncFn().then(setter));
  return getter;
}
export const useToggle = () => {
  const [isOpen, setIsOpen] = createSignal();
  const setOpen = setIsOpen(true);
  const setClose = setIsOpen(false);
  return { isOpen, setOpen, setClose };
};
