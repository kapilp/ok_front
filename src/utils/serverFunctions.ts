import { Ws } from './ws_events_dispatcher';
import { ET } from './enums';

export const fetchUUID = async (): Promise<{ uuid: string }> => {
  return new Promise((resolve, reject) => Ws.bindT([ET[ET.get], 'get_uuid', Ws.uid], d => resolve(d), {}, 0));
};

export async function getLocalUUID() {
  const localUUID = localStorage.getItem('chatUUID');
  if (!localUUID) {
    return new Promise<string>((resolve, reject) => {
      fetchUUID().then(d => {
        localStorage.setItem('chatUUID', d.uuid);
        resolve(d.uuid);
      });
    });
  }
  return new Promise<string>((resolve, reject) => {
    resolve(localUUID);
  });
}
