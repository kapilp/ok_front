import { Store } from '../components/libx/src/store';
import { ET, makeTableArgs } from '../utils/enums';
import { Ws } from '../utils/ws_events_dispatcher';

export class ColorStore extends Store {
  colors = this.collection({});

  constructor(p) {
    super(p);
    this.fetch();
  }

  async fetch() {
    // make this retry until success
    // Temparary commiting because when [["get", "color_list", 1]] run on _system database on Admin.ok.com website and this function is not resolved.
    return true;
    /* const d = await new Promise((resolve, reject) => {
      // send unsubscribe event if edit is open
      const args = makeTableArgs();
      const evt = [ET[ET.get], 'color_list', Ws.uid];
      Ws.bindT(
        evt,
        d => {
          resolve(d);
        },
        { ...args },
      );
    });
    if (!d.error) {
      console.log(d);
      // this.colors.set(d.)
    } else {
      // setFetchState({ er: d.error });
    } */
  }
}
export class TypeStore extends Store {
  types = this.collection({});

  constructor(p) {
    super(p);
    this.fetch();
  }

  fetch() {}
}
export class PriorityStore extends Store {
  priorities = this.collection({});

  constructor(p) {
    super(p);
    this.fetch();
  }

  fetch() {}
}
export class MemberStore extends Store {
  priorities = this.collection({});

  constructor(p) {
    super(p);
    this.fetch();
  }

  fetch() {}
}
