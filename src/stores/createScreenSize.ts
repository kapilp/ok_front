/** @prettier */

import { createContext, createSignal, onCleanup, useContext } from 'solid-js';
import { useDebounce } from '@/lib/debounce';

const DEBOUCNE_MSEC = 250;

const screenSize = () => ({
  width: window.innerWidth || 0,
  height: window.innerHeight || 0,
});

export const screen_size = createContext(createSignal({}));
export const createScreenSize = () => {
  const [getScreenSize, setScreenSize_] = useContext(screen_size);
  const [setDebounce, cancelDebounce] = useDebounce();

  const setScreenSize = () => {
    setScreenSize_(screenSize());
  };

  window.addEventListener('resize', setDebounce(setScreenSize, DEBOUCNE_MSEC));

  onCleanup(() => {
    window.removeEventListener('resize', setScreenSize);
    cancelDebounce();
  });

  return {
    setScreenSize,
  };
};
