import { CrumbsStore } from "../components/UI/react-breadcrumbs/src/store";
import { ColorStore, MemberStore, PriorityStore, TypeStore } from "./dbStores";
import { TableStore } from "./TablesStore";
// import type { WorkspaceStore } from "./workspace/workspace-store";
// import createWorkspaceStore from "./workspace/workspace-store";
// import type { SelectionProvider } from "./workspace/selection-provider";
// import createSelectionProvider from "./workspace/selection-provider";
// way 1:
// class RootStore {
//   constructor() {
//     //this.todoStore = new TodoStore({ rootStore: this })
//     //this.userStore = new UserStore({ rootStore: this })
//     //this.todosScreenStore = new TodosScreenStore({ rootStore: this })
//   }
// }
// export const rootStore = new RootStore();
// way 2:
// import { createRootStore } from '../components/libx/src/root-store';
// export const rootStore = createRootStore({userStore: UserStore,todoStore: TodoStore});
// way 3:
interface StoreType {
  crumbsStore: any;
  colorStore: ColorStore;
  typeStore: TypeStore;
  priorityStore: PriorityStore;
  tablesStore: TableStore;
}
export function createRootStore() {
  // const selectionProvider = createSelectionProvider();
  // const workspaceStore = createWorkspaceStore(routerStore, selectionProvider);

  const store = {} as StoreType;
  const value = {
    crumbsStore: CrumbsStore([]),
    colorStore: new ColorStore({ rootStore: store }),
    typeStore: new TypeStore({ rootStore: store }),
    priorityStore: new PriorityStore({ rootStore: store }),
    memberStore: new MemberStore({ rootStore: store }),
    tablesStore: new TableStore({ rootStore: store }),
    organizationDashboard: {},
    projectDashboard: {},
    wikiStore: {},
    blogStore: {},
    forumStore: {},
    workspaceStore: {},
    selectionProvider: {},
  };
  Object.assign(store, value);

  // Deserialize state
  // const data = JSON.parse(localStorage.getItem("data") || "{}");
  // workspaceStore.deserialize(data.workspaceStoreData || {});
  // themeStore.deserialize(data.themeStoreData || {});
  //
  // reaction(
  //   () =>
  //     JSON.stringify({
  //       workspaceStoreData: workspaceStore.serialize(),
  //       themeStoreData: themeStore.serialize()
  //     }),
  //   data => {
  //     localStorage.setItem("data", data);
  //   },
  //   { delay: 1000 }
  // );
  return store;
}
export const rootStore = createRootStore();
