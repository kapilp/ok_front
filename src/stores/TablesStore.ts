import { Store } from '../components/libx/src/store';
import { Model } from '../components/libx/src/model';
// posish/src / utils / collection.js;
// export function collection (opts) {
//   return collection({
//     create: (data, opts) => opts.model(data, { stripUndefined: true, parse: true, ...opts }),
//     update: (existing, data, opts) => existing.set(data, { stripUndefined: true, parse: true, ...opts }),
//     ...opts
//   })
// }
export class TableStore extends Store {
  collectionMap = new Map<string, T>();

  constructor(p) {
    super(p);
    this.fetch();
  }

  fetch() {}

  new(id: string) {
    const idAsString: string = id.toString();
    const fromMap = this.collectionMap.get(idAsString);
    if (fromMap) {
      console.log('returning existing table store');
      return fromMap;
    }
    const c = this.collection({});
    this.collectionMap.set(id, c);
    return c;
  }

  getOrNew(id: string) {
    if (id === undefined || id === null) {
      return undefined;
    }
    const idAsString: string = id.toString();
    const fromMap = this.collectionMap.get(idAsString);
    if (fromMap) {
      return fromMap;
    }
    const c = this.collection({
      idAttribute: '_key',
      create: input => new Model(input),
      update: (existing, input) => {
        console.log('updating', existing, input);
        return existing.set(input);
      },
    }); // _id
    this.collectionMap.set(id, c);
    return c;
  }
}
