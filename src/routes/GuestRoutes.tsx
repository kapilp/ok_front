import { Match, Switch } from "solid-js";
import { MatchRoute, NavLink } from "@rturnq/solid-router";
import { css } from "goober";

import { ContactRoute } from "./CommonRoutes";
import { FormServer } from "../components/formServer/FormServer";

import { FlexboxCenter } from "../components/UI/FlexboxCenter";
import { Card } from "../components/UI/core/card/src/Card";
import { getSubDomain } from "../utils/url";

const center = css`
  text-align: center;
  padding: 15px;
`;
export const GuestRoutes = () => (
  <>
    <Switch>
      <Match when={getSubDomain() !== "admin"}>
        <NavLink href="/register" end>
          Register
        </NavLink>
        <NavLink href="/login" end>
          Login
        </NavLink>
      </Match>
      <Match when={getSubDomain() === "admin"}>
        <NavLink href="/super_register" end>
          Register
        </NavLink>
        <NavLink href="/super_login" end>
          Login
        </NavLink>
      </Match>
    </Switch>

    <Switch>
      <Match when={getSubDomain() === "admin"}>
        <Switch>
          <MatchRoute path="super_register">
            <FlexboxCenter direction="column">
              <Card class={center}>
                <FormServer b={["signup2"]} key="" schemaKey="register" t={["signup"]} />
              </Card>
            </FlexboxCenter>
          </MatchRoute>

          <MatchRoute path="super_login">
            <FlexboxCenter direction="column">
              <Card class={center}>
                <FormServer b={["signup2"]} key="" schemaKey="login" t={["login"]} />
              </Card>
            </FlexboxCenter>
          </MatchRoute>
        </Switch>
      </Match>

      <Switch>
        <Match when={getSubDomain() !== "admin"}>
          <MatchRoute path="register">
            <FlexboxCenter direction="column">
              <Card class={center}>
                <FormServer b={["signup2"]} key="" schemaKey="mregister" t={["mregister"]} />
              </Card>
            </FlexboxCenter>
          </MatchRoute>

          <MatchRoute path="login">
            <FlexboxCenter direction="column">
              <Card class={center}>
                <FormServer b={["signup2"]} key="" schemaKey="mlogin" t={["mlogin"]} />
              </Card>
            </FlexboxCenter>
          </MatchRoute>
        </Match>
      </Switch>

      <ContactRoute />
      <MatchRoute path="index">
        Home Page
      </MatchRoute>
      <MatchRoute path="/">
        Home Page
      </MatchRoute>
    </Switch>
    Footer
  </>
);
export default GuestRoutes;
