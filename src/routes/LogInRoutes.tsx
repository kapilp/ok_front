import { Match, Show, Switch } from 'solid-js';
import { MatchRoute, NavLink } from '@rturnq/solid-router';
import { getIsLoggedIn, getWsConnected } from '../utils/ws_events_dispatcher';
import { Logout } from '../views/logout';
import { Table } from '../components/table/Table';
import { Confirm } from '../views/confirm';
import { AccountLayout } from '../layout/account_layout';
import { AdminLayout } from '../layout/admin_layout';
import { OrganizationLayout } from '../layout/org_layout';
import { Home } from '../views/home';
import { ContactRoute, RedirectHomeIfLoggedIn } from './CommonRoutes';
import { Status } from '../components/UI/core/status/src/Status';
import { Layout } from '../components/UI/terra-consumer/packages/terra-consumer-layout/src/Layout';
import { layoutGlobalState, onBack, onNext, stackState } from '../layout/adminLayoutState';
import { GlobalLayout } from '../layout/global_layout';
import { IS_PRODUCTION } from '../utils/enums';
import { Chat } from '../components/UI/Chat';
import { Breadcrumbs } from '../components/UI/react-breadcrumbs/src/breadcrumbs';
import { TestRoute } from '../views/testRoutes';

export const LogInRoutes = () => (
  <Layout {...layoutGlobalState} stackState={stackState} onBack={onBack} onNext={onNext}>
    <Chat />
    {/* <Breadcrumbs /> */}
    {/* <Show when={!IS_PRODUCTION}> */}
    <NavLink href="/global">Global</NavLink>
    {/* </Show> */}
    <Show when={!getWsConnected()}>
      <Status colorClass="attention" visuallyHiddenText="Status Offline">
        Offline
      </Status>
    </Show>
    <Switch>
      <ContactRoute />

      <MatchRoute path="/logout">
        <Switch>
          <Match when={getIsLoggedIn()}>
            <Logout />
          </Match>
          <RedirectHomeIfLoggedIn />
        </Switch>
      </MatchRoute>

      <MatchRoute path="/global">
        <GlobalLayout />
      </MatchRoute>

      <MatchRoute path="/organizations">
        <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="organization" />
      </MatchRoute>

      <MatchRoute path="/projects">
        <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="project" />
      </MatchRoute>
      <MatchRoute path="/account/confirm">
        <Confirm />
      </MatchRoute>
      <MatchRoute path="/account/:page">
        <AccountLayout />
      </MatchRoute>

      <MatchRoute path="/my/page">
        <h1>My Page Coming soon.</h1>
      </MatchRoute>

      <MatchRoute path="/admin">
        <AdminLayout />
      </MatchRoute>
      <MatchRoute path="/admin/:page">
        <AdminLayout />
      </MatchRoute>

      <MatchRoute path="/organization/:org">
        <OrganizationLayout />
      </MatchRoute>
      <MatchRoute path="/test">
        <TestRoute />
      </MatchRoute>
      <MatchRoute path="/index">
        <Home />
      </MatchRoute>
      <MatchRoute path="/">
        <Home />
      </MatchRoute>
    </Switch>
  </Layout>
);
export default LogInRoutes;
