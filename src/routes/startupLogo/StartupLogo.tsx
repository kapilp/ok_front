import { JSX, createSignal } from 'solid-js';
import logo from '../../../public/logo.svg';
import styles from './StartupLogo.module.scss';

export const StartupLogo = () => {
  const messageOptions = [
    "If I'm not back in five minutes... just wait longer.", // Ace Ventura: Pet Detective
    "And that's why you always leave a note.", // Arrested Development
    'Jim never has a second cup of coffee at home.', // Airplane!
    'This is WZAZ in Chicago, where disco lives forever.', // Airplane!
    'Carpe dentum. Seize the teeth.', // Mrs. Doubtfire
    'Ah! Norman Bates!', // Airplane! (it was a really good movie)
    'I am the walrus.', // The Big Lebowski
    'And a good day to you, sir!', // The Big Lebowski (also a really good movie)
    "Do you want ants? Because that's how you get ants!", // Archer
    'Kids you can literally fly!', // Key and Peele
    "Goats are the only animals with an innate sense of democracy. That's why they're called \"Nature's President\".",
    "Goats are stupid, mean, and hardheaded animals. That's why they're called \"Nature's President\".", // Mr. Show
    "I'd like to add you to my professional network",
    'Reticulating splines', // Simcity 2000
    'They mostly come at night. Mostly.', // Aliens
  ];
  const [msg, setMsg] = createSignal('Welcome!');
  if (Math.random() < 0.02) {
    const idx = Math.floor(Math.random() * messageOptions.length);
    setMsg(messageOptions[idx]);
  }
  return (
    <div class={styles.loadingPlaceholder}>
      <img src={logo} alt={logo} />
      <h1 id="welcome-message" class="not_authenticating">
        {msg()}
      </h1>
      <h1 style="display: none" class="authenticating">
        Authenticating...
      </h1>
    </div>
  );
};
export default StartupLogo;
