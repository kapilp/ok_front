import { Match } from 'solid-js';
import { MatchRoute, Redirect } from '@rturnq/solid-router';
import { Contact } from '../views/contact';
import { getIsLoggedIn } from '../utils/ws_events_dispatcher';

export const ContactRoute = () => (
  <MatchRoute path="/contact">
    <Contact />
  </MatchRoute>
);

export const RedirectHomeIfLoggedIn = () => (
  <Match when={getIsLoggedIn()}>
    <Redirect href="/" />
  </Match>
);
