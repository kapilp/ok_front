import { Match, onCleanup, Show, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { MatchRoute, useRoute } from '@rturnq/solid-router';
import { Skeleton } from '../components/UI/Skeleton';
import { IS_PRODUCTION, TableResult } from '../utils/enums';
import { pop, push } from './adminLayoutState';
import { ChatFullScreen } from '../components/UI/Chat';
import { Table } from '../components/table/Table';

export const GlobalLayout = () => {
  const [state, setState] = createStore({
    er: '',
    authorized: true,
    isLoading: true,
    isFetching: false,
    items: [] as TableResult,
    admin_menu: [],
  });

  const setupMenu = () => {
    const menu = [
      {
        text: 'Schema',
        url: '/global/schema',
      },
      {
        text: 'Permission',
        url: '/global/super_permission',
      },
      {
        text: 'Role',
        url: '/global/super_role',
      },
      // {
      //   text: 'Menu',
      //   url: '/global/super_menu',
      // },
      {
        text: 'Template',
        url: '/global/template',
      },
      {
        text: 'Color',
        url: '/global/super_color',
      },
      {
        text: 'Translation',
        url: '/global/translation',
      },
      {
        text: 'User',
        url: '/global/user',
      },
      {
        text: 'Confirm',
        url: '/global/confirm',
      },
      {
        text: 'Session',
        url: '/global/session',
      },
      {
        text: 'Node',
        url: '/global/super_node',
      },
      {
        text: 'Edge',
        url: '/global/super_edge',
      },
    ];

    push(menu);

    setState({ isLoading: false, isFetching: false });
  };

  setupMenu();

  onCleanup(pop);

  const T = () => {
    const { params } = useRoute();
    return <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey={params?.schemaKey} />;
  };

  return (
    <>
      {/* <Show when={!IS_PRODUCTION}>GLOBAL LAYOUT</Show> */}
      <Switch>
        <Match when={state.isLoading}>Loading...</Match>
        <Match when={!state.authorized}>{state.er}</Match>
        <Match when={state.isFetching}>
          <Skeleton />
        </Match>
        <Match when={!state.isFetching}>
          <Switch>
            <MatchRoute path="/global/support">
              <ChatFullScreen />
            </MatchRoute>
            <MatchRoute path="/global/:schemaKey">
              <T />
            </MatchRoute>
            <MatchRoute path="/global">Index</MatchRoute>
          </Switch>
        </Match>
      </Switch>
    </>
  );
};
