import { IS_PRODUCTION } from '../utils/enums';

export const PublicLayout = () => {
  return (
    <>
      {!IS_PRODUCTION && 'PUBLIC LAYOUT'}
      <div class="app">
        <section class="section" />
      </div>
      TODO ADD FOOTER TEMPLATE HERE
    </>
  );
};
//
