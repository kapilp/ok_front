// import { SvgIconPerson } from '../components/UI/core/icon/src/icon/IconPerson';
// import { SvgIconOutlineQuestionMark } from '../components/UI/terra-consumer/packages/terra-consumer-icon/src/icon/IconOutlineQuestionMark';

import { Component } from 'solid-js';
import { createStore } from 'solid-js/store';
import logo from '../../public/logo.svg';
import { stackHook } from '../hooks/stackHook';
import { IS_PRODUCTION } from '../utils/enums';

export const { stackState, pop, push, onRequestClose, setTitle, setActiveIndex, onBack, onNext } = stackHook();
push([
  {
    text: 'Home',
    url: '/',
  },
  {
    text: 'Organizations',
    url: '/organizations',
  },
]);

export interface NavItem {
  isExternal: boolean;
  url: string;
  text: string;
  icon: Component;
}

const data = {
  nav: {
    navItems: [] as NavItem[],
    // navItems: [
    //   {
    //     // any link that doesn't use react router link is external
    //     isExternal: true,
    //     url: 'localhost:8080',
    //     text: 'Home',
    //   },
    //   {
    //     url: '/',
    //     text: 'Dashboard',
    //     icon: <SvgIconPerson />,
    //   },
    //   {
    //     text: 'Messaging',
    //     badgeValue: '12',
    //     icon: <SvgIconPerson />,
    //     subItems: [
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'Inbox',
    //         badgeValue: '2',
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: '2',
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'An Active Link',
    //         isActive: false,
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'Inbox',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'An Active Link',
    //         isActive: true,
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'Inbox',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'Inbox',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#inbox',
    //         text: 'An Active Link',
    //         isActive: false,
    //         badgeValue: 1,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#sent',
    //         text: 'Sent',
    //         badgeValue: 1,
    //       },
    //     ],
    //   },
    //   {
    //     text: 'Health Record',
    //     badgeValue: 0,
    //     icon: <SvgIconPerson />,
    //     subItems: [
    //       {
    //         isExternal: true,
    //         url: '#health',
    //         text: 'Health',
    //         icon: <SvgIconPerson />,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#record',
    //         text: 'Record',
    //         icon: <SvgIconPerson />,
    //       },
    //     ],
    //   },
    //   {
    //     text: 'Test Data',
    //     subItems: [
    //       {
    //         isExternal: true,
    //         url: '#test',
    //         text: 'Test',
    //         icon: <SvgIconPerson />,
    //       },
    //       {
    //         isExternal: true,
    //         url: '#data',
    //         text: 'Data',
    //         icon: <SvgIconPerson />,
    //       },
    //     ],
    //   },
    //   {
    //     text: 'More Tests',
    //     subItems: [
    //       {
    //         isExternal: true,
    //         url: '#more',
    //         text: 'More',
    //       },
    //       {
    //         isExternal: true,
    //         url: '#tests',
    //         text: 'Tests',
    //       },
    //     ],
    //   },
    // ],

    profile: {
      profileLinks: [
        {
          text: 'My Page',
          url: '/my/page',
        },
        {
          // target: '_self',
          url: '/account/profile',
          text: 'My Account',
          subItems: [],
          // isExternal: true,
          icon: 'icon-cog',
        },
        {
          text: 'Administration',
          url: '/admin',
        },
        {
          target: '_self',
          url: '/notifications',
          text: 'Notifications',
          subItems: [],
          // isExternal: true,
          icon: 'icon-message-send',
        },
        {
          // target: '_self',
          url: '/access_logs',
          text: 'Access Logs',
          subItems: [],
          // isExternal: true,
          icon: 'icon-client-access-logs',
        },
        {
          target: '_blank',
          url: IS_PRODUCTION ? 'https://doc.chat.okweb.co.in/' : '/help',
          text: 'Help',
          subItems: [],
          isExternal: true,
          icon: 'icon-client-question-alt',
        },
        /* {
          target: '_self',
          url: '#',
          isExternal: true,
          text: 'English (United States)&#x200E;',
          subItems: [
            {
              'data-locale': 'ar',
              isExternal: true,
              url: '#1',
              text: '&#8235;\u0627\u0644\u0639\u0631\u0628\u064a\u0651\u0629 &#8236;',
            },
            {
              'data-locale': 'es',
              isExternal: true,
              url: '#2',
              text: 'Espa\u00f1ol',
            },
            {
              'data-locale': 'en-gb',
              isExternal: true,
              url: '#3',
              text: 'English (United Kingdom)&#x200E;',
            },
            {
              'data-locale': 'fr-fr',
              isExternal: true,
              url: '#4',
              text: 'Fran\u00e7ais (France)&#x200E;',
            },
          ],
        }, */
      ],
      // comment out userName to see signin
      // userName: getMember()?.email,
      signinUrl: '/super_register',
      signoutUrl: '/logout',
    },
  },

  /* helpItems: [
    {
      text: 'Technical Questions',
      icon: <SvgIconOutlineQuestionMark />,
      children: [
        {
          text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
        },
      ],
    },
    {
      text: 'Get Support ID',
      icon: <SvgIconOutlineQuestionMark />,
      children: [
        {
          text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
        },
      ],
    },
    // {
    //   isExternal: true,
    //   text: 'Link',
    //   url: 'http://localhost:8080/',
    //   icon: <SvgIconOutlineQuestionMark />,
    // },
  ], */
  logo: {
    mobileLogo: {
      url: logo,
      // url: 'https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/2014/02/Olympic-logo.png',
      altText: 'OK Tech Project Management',
    },
    navLogo: {
      url: logo,
      // url: 'https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/2014/02/Olympic-logo.png',
      altText: 'OK Tech Project Management',
      // isCard: true,
      link: {
        url: '/',
      },
    },
  },
};

export const [layoutGlobalState, setAdminLayoutState] = createStore(data);
/*

export const OneFileI18Sample = () => {
  const { t, i18n } = useTranslation();
  const [isOpen, setIsOpen] = createSignal(true);
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
  };

  return (
    <>
      <Show when={isOpen()}>
        <h2>{t('Welcome to React')}</h2>
      </Show>
      <button onClick={_ => setIsOpen(!isOpen())}>Toggle</button>
      <button onClick={() => changeLanguage('de')}>de</button>
      <button onClick={() => changeLanguage('en')}>en</button>
    </>
  );
};
 */
