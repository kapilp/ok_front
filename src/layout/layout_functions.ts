import { produce, SetStoreFunction, Store } from "solid-js/store";
import { isEr, TableError, TableResult, TableResultAll } from "../utils/enums";
import { isObject } from "ramda-adjunct";

export type ItemsState = {
  authorized: boolean;
  isLoading: boolean;
  er: string;
  items: TableResult;
};

export const getMenuDataGet = (d: TableResultAll, state: Store<ItemsState>, setState: SetStoreFunction<ItemsState>, set: () => void) => {
  if (state.isLoading) setState({ isLoading: false });
  if (isEr(d, state, setState)) {
    setState({ authorized: false });
  } else if (isObject(d)) {
    if (d.r) {
      setState({ items: d.r.result ?? [] });
      set();
    } else if (d.n) {
      // TODO
    } else if (d.m) {
      d.m.result.forEach(mod => {
        const findIndex = state.items.findIndex(i => i._key == mod._key);
        if (findIndex !== -1) {
          setState("items", produce<TableResult>(l => {
            l.splice(findIndex, 1, mod);
          }));
        }
      });
      set();
    } else if (d.d) {
      // TODO
    }
  } else {
    setState({ er: TableError.INVALID_VALUE });
  }
};

export const findMenuIdx = (items: TableResult, name: string) => items.findIndex(i => i._key == name);

