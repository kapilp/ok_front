import { Match, onCleanup, Show, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { MatchRoute } from '@rturnq/solid-router';
import { FormServer } from '../components/formServer/FormServer';
import { Skeleton } from '../components/UI/Skeleton';
import { Table } from '../components/table/Table';
import { IS_PRODUCTION, TableResult } from '../utils/enums';
import { AdminIndex } from '../views/admin_index';
import { pop, push } from './adminLayoutState';

export const AdminLayout = () => {
  const [state, setState] = createStore({
    er: '',
    authorized: true,
    isLoading: true,
    isFetching: false,
    items: [] as TableResult,
    admin_menu: [],
  });

  const setupMenu = () => {
    const menu = [
      {
        icon: () => <span class="mif-meter" />,
        text: 'Overview',
        url: '/admin',
      },
      {
        subItems: [
          {
            text: 'Settings',
            url: '/admin/members_settings',
          },
          {
            text: 'Members',
            url: '/admin/members',
          },
          {
            text: 'Groups',
            url: '/admin/groups',
          },
          {
            text: 'Roles',
            url: '/admin/roles',
          },
          {
            text: 'Permissions',
            url: '/admin/permissions',
          },
        ],
        icon: () => <span class="mif-meter" />,
        text: 'Members & Permissions',
        url: '/admin/members',
      },
      {
        subItems: [
          {
            text: 'Settings',
            url: '/admin/work_package_settings',
          },
          {
            text: 'Types',
            url: '/admin/types',
          },
          {
            text: 'Status',
            url: '/admin/status',
          },
          {
            text: 'Workflow',
            url: '/admin/workflows',
          },
          {
            text: 'Custom actions',
            url: '/admin/custom_actions',
          },
          {
            icon: () => <span class="mif-info" />,
            text: 'Time Tracking Activities',
            url: '/admin/activities',
          },
          {
            icon: () => <span class="mif-info" />,
            text: 'Priorities',
            url: '/admin/priorities',
          },
          {
            icon: () => <span class="mif-info" />,
            text: 'Document Categories',
            url: '/admin/document_categories',
          },
        ],
        icon: () => <span class="mif-assignment" />,
        text: 'Work packages',
        url: '/admin/work_package_settings',
      },
      {
        icon: () => <span class="mif-battery-empty" />,
        text: 'Custom fields',
        url: '/admin/custom_fields',
      },
      {
        icon: () => <span class="mif-hammer" />,
        text: 'System settings',
        url: '/admin/settings',
      },
      {
        subItems: [
          {
            text: 'EMail Notifications',
            url: '/admin/mail_notifications',
          },
        ],
        icon: () => <span class="mif-lock" />,
        text: 'Email',
        url: '/admin/mail_notifications',
      },
      {
        subItems: [
          {
            text: 'Settings',
            url: '/admin/auth_settings',
          },
        ],
        icon: () => <span class="mif-lock" />,
        text: 'Authentication',
        url: '/admin/auth_settings',
      },
      {
        icon: () => <span class="mif-news" />,
        text: 'Announcement',
        url: '/admin/announcements',
      },
      {
        icon: () => <span class="mif-pencil" />,
        text: 'Design',
        url: '/admin/design',
      },
      {
        icon: () => <span class="mif-pencil" />,
        text: 'Colors',
        url: '/admin/colors',
      },
      {
        icon: () => <span class="mif-balance-scale" />,
        text: 'Budgets',
        url: '/admin/budgets',
      },
      {
        icon: () => <span class="mif-backlog" />,
        text: 'Backlogs',
        url: '/admin/backlogs',
      },
    ];

    push(menu);
    setState({ fetch_menu: true, isLoading: false });
  };

  setupMenu();

  onCleanup(pop);

  /* const setAdminMenu = () => {
    const idx = findMenuIdx(state.items, 'admin');
    if (idx > -1)
      //setState({ admin_menu: state.items[idx].menu });
      setAdminLayoutState('nav', (l: { navItems: [] }) => {
        l.navItems = state.items[idx].menu;
      });
    setState({ isFetching: false });
  };

  onCleanup(
    Ws.bindT(
      [ET[ET.subscribe], "menu_list", Ws.uid],
      d => getMenuDataGet(d, state, setState, setAdminMenu),
      makeTableArgs().setFilter({ _key: `['admin']` }).setType(ValueType.Object),
      1,
    ),
  ); */
  // Only If userIsAdmin
  // else redirect /
  return (
    <>
      {/* <Show when={!IS_PRODUCTION}>ADMIN LAYOUT</Show> */}

      <Switch>
        <Match when={state.isLoading}>Loading...</Match>
        <Match when={!state.authorized}>{state.er}</Match>
        <Match when={state.isFetching}>
          <Skeleton />
        </Match>
        <Match when={!state.isFetching}>
          <Switch>
            <MatchRoute path="/admin/menu">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="menu" />
            </MatchRoute>

            <MatchRoute path="/admin/projects">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="project" />
            </MatchRoute>

            <MatchRoute path="/admin/groups">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="group" />
            </MatchRoute>

            <MatchRoute path="/admin/roles">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="role" />
            </MatchRoute>

            <MatchRoute path="/admin/members">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/members_settings">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/admin/permissions">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="permission" />
            </MatchRoute>

            <MatchRoute path="/admin/avatars">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/work_package_settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="work_package_setting" />
            </MatchRoute>

            <MatchRoute path="/admin/custom_fields">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/custom_actions">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/attribute_help_texts">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/custom_fields">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/oauth_applications">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/auth_settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/design">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/custom_styles">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/budgets">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/backlogs">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/info">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/webhooks">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>

            <MatchRoute path="/admin/colors">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="color" />
            </MatchRoute>

            <MatchRoute path="/admin/types">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="type" />
            </MatchRoute>

            <MatchRoute path="/admin/priorities">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="priority" />
            </MatchRoute>

            <MatchRoute path="/admin/status">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="status" />
            </MatchRoute>

            <MatchRoute path="/admin/activities">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>

            <MatchRoute path="/admin/doc_categories">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="doc_category" />
            </MatchRoute>

            <MatchRoute path="/admin/media">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="media" />
            </MatchRoute>

            <MatchRoute path="/admin/messages">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="message" />
            </MatchRoute>

            <MatchRoute path="/admin/categories">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="category" />
            </MatchRoute>

            <MatchRoute path="/admin/announcements">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="announcement" />
            </MatchRoute>

            <MatchRoute path="/admin/news">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="news" />
            </MatchRoute>

            <MatchRoute path="/admin/wiki">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="wiki" />
            </MatchRoute>

            <MatchRoute path="/admin/work_packages">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="work_packages" />
            </MatchRoute>

            <MatchRoute path="/admin/forum">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="forum" />
            </MatchRoute>

            <MatchRoute path="/admin/posts">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="post" />
            </MatchRoute>

            <MatchRoute path="/admin/index">
              <AdminIndex />
            </MatchRoute>

            <MatchRoute path="/admin">
              <AdminIndex />
            </MatchRoute>
          </Switch>
        </Match>
      </Switch>
    </>
  );
};
