import { clone } from 'rambda';
import { createComputed, createMemo, Match, onCleanup, Show, Switch, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import UrlPattern from 'url-pattern';
import { MatchRoute, useRoute } from '@rturnq/solid-router';
import { Skeleton } from '../components/UI/Skeleton';
import { Table } from '../components/table/Table';
import { ET, GeneralError, IS_PRODUCTION, isEr, makeTableArgs, OrganizationLayoutError, ProjectLayoutError, TableResult, TableResultAll, WsStatus } from '../utils/enums';
import { getWsConnected, Ws } from '../utils/ws_events_dispatcher';
import { OrgDocumentContext, OrgIDContext, ProjectDocumentContext, ProjectIDContext, ProjectStackContext } from './context';
import { ProjectIndex } from '../views/project_index';
import { pop, push } from './adminLayoutState';
import { Menu } from './org_layout';
import { ChatAdmin } from '../components/UI/ChatAdmin';

export const ProjectLayout = () => {
  const [state, setState] = createStore({
    er: '',
    authorized: true,
    isLoading: true,
    isFetchingData: true,
    isFetchingMenu: true,
    items: [] as TableResult,
    project_menu: [] as Menu[],
  });

  const [getOrgIDContext] = useContext(OrgIDContext);
  const orgId = getOrgIDContext();
  const [getOrgDocumentContext] = useContext(OrgDocumentContext);
  const [, setProjectIDContext] = useContext(ProjectIDContext);
  const [getProjectDocumentContext, setProjectDocumentContext] = useContext(ProjectDocumentContext);
  const [getProjectStackContext, setProjectStackContext] = useContext(ProjectStackContext);

  const { params } = useRoute();
  const projectIdMemo = createMemo(() => params?.project ?? '');
  createComputed(() => {
    setProjectIDContext(projectIdMemo());
  });

  const project_fetch_evt = [ET[ET.get], 'project_list', Ws.uid];

  onCleanup(
    Ws.bind$(
      project_fetch_evt,
      (d: TableResultAll) => {
        if (isEr(d, state, setState)) {
          setState({ authorized: false });
          return;
        }
        // TODO: also consider subscribe and handle modified and delete result
        // and unsubscribe on cleanup
        const { result } = d.r;
        if (result.length == 0) {
          setState({ er: ProjectLayoutError.NO_PROJECT_FOUND });
          // TODO: show notification that organization not exist or Org ID changed.
          setTimeout(() => (window.location.href = '/'), 500);
        } else if (result[0]) {
          const projectDocument = result[0];
          setProjectDocumentContext(projectDocument);
          setProjectStackContext([...getProjectStackContext(), projectDocument]);
          setState({ isFetchingData: false, isLoading: false });
        }
      },
      1,
    ),
  );

  // re trigger fetch event when ws reconnected.
  createComputed(() => {
    if (getWsConnected()) {
      setState({ er: GeneralError.NO_ERROR });
      if (projectIdMemo()) {
        const args = makeTableArgs().setFilter({ id: `="${projectIdMemo()}"` });
        Ws.trigger([
          [
            project_fetch_evt,
            {
              ...args,
              organization: getOrgDocumentContext()?._key,
            },
          ],
        ]);
      } else {
        setState({ er: ProjectLayoutError.NO_PROJECT_ID });
      }
    } else {
      setState({ er: WsStatus.RECONNECTION });
    }
  });

  onCleanup(() => {
    // be very careful: top level component unmount first.
    const newProject = clone(getProjectStackContext());
    newProject.pop();
    setProjectStackContext(newProject);
  });

  const setupMenu = () => {
    const menu = [
      {
        icon: () => <span class="mif-info" />,
        text: 'Overview',
        url: '/organization/:org/project/:project',
      },
      {
        subItems: [
          {
            text: 'Groups',
            url: '/organization/:org/project/:project/groups',
          },
          {
            text: 'Roles',
            url: '/organization/:org/project/:project/roles',
          },
        ],
        icon: () => <span class="mif-meter" />,
        text: 'Members & Permissions',
        url: '/organization/:org/project/:project/members',
      },
      {
        icon: () => <span class="mif-checkmark" />,
        text: 'Activity',
        url: '/organization/:org/project/:project/activities',
      },
      {
        icon: () => <span class="mif-compass" />,
        text: 'Roadmap',
        url: '/organization/:org/project/:project/roadmap',
      },
      {
        icon: () => <span class="mif-clipboard" />,
        text: 'Work packages',
        url: '/organization/:org/project/:project/work_packages',
      },
      {
        icon: () => <span class="mif-chart-bars2" />,
        text: 'Boards',
        url: '/organization/:org/project/:project/boards',
      },
      {
        icon: () => <span class="mif-copy" />,
        text: 'Backlogs',
        url: '/organization/:org/project/:project/backlogs',
      },
      {
        icon: () => <span class="mif-calendar" />,
        text: 'Calender',
        url: '/organization/:org/project/:project/calendar',
      },
      {
        icon: () => <span class="mif-volume-high" />,
        text: 'News',
        url: '/organization/:org/project/:project/news',
      },
      {
        icon: () => <span class="mif-stack" />,
        text: 'Forum',
        url: '/organization/:org/project/:project/forum',
      },
      {
        icon: () => <span class="mif-leanpub" />,
        text: 'Wiki',
        url: '/organization/:org/project/:project/wiki/wiki',
      },
      {
        icon: () => <span class="mif-equalizer2" />,
        text: 'Cost reports',
        url: '/organization/:org/project/:project/cost_reports',
      },
      {
        icon: () => <span class="mif-users" />,
        text: 'Members',
        url: '/organization/:org/project/:project/members',
      },
      {
        icon: () => <span class="mif-calculator" />,
        text: 'Budgets',
        url: '/organization/:org/project/:project/cost_objects',
      },
      {
        icon: () => <span class="mif-bubbles" />,
        text: 'Meetings',
        url: '/organization/:org/project/:project/meetings',
      },
      {
        icon: () => <span class="mif-wrench" />,
        text: 'Project settings',
        url: '/organization/:org/project/:project/settings',
      },
      {
        subItems: [
          {
            text: 'Conversation',
            url: '/organization/:org/project/:project/conversation',
          },
          {
            text: 'My Alias',
            url: '/organization/:org/project/:project/alias',
          },
          {
            text: 'Departments',
            url: '/organization/:org/project/:project/chat_department',
          },
        ],
        icon: () => <span class="mif-wrench" />,
        text: 'Conversation',
        url: '/organization/:org/project/:project/conversation',
      },
      {
        icon: () => <span class="mif-wrench" />,
        text: 'Message',
        url: '/organization/:org/project/:project/message',
      },
      {
        icon: () => <span class="mif-wrench" />,
        text: 'participant',
        url: '/organization/:org/project/:project/participant',
      },
    ];
    push(processMenu(clone(menu), orgId, projectIdMemo()));
    setState({ isFetchingMenu: false });
  };

  setupMenu();

  onCleanup(pop);

  /* const setOrganizationMenu = () => {
    const idx = findMenuIdx(state.items, 'project');
    if (idx > -1) {
      const OldMenu: Menu[] = state.items[idx].menu;
      setAdminLayoutState('nav', (l: { navItems: [] }) => {
        l.navItems[1] = processMenu(clone(OldMenu), orgId, projectIdMemo());
      });
      setState({ isFetchingMenu: false });
    }
  };
  onCleanup(
    Ws.bindT(
      [ET[ET.subscribe], "menu_list", Ws.uid],
      d => { getMenuDataGet(d, state, setState, setOrganizationMenu);  },
      makeTableArgs().setFilter({ _key: `['project']` }).setType(ValueType.Object),
      1,
    ),
  ); */

  function processMenu(menu_: Menu[], org: string, project: string) {
    if (!org) {
      setState({ er: OrganizationLayoutError.EMPTY_ORG_ID });
      return [];
    }
    if (!project) {
      setState({ er: ProjectLayoutError.EMPTY_PROJECT_ID });
      return [];
    }
    for (const x of menu_) {
      x.url = new UrlPattern(x.url).stringify({ org, project });
      if (x.subItems) {
        x.subItems = processMenu(x.subItems, org, project);
      }
    }
    return menu_;
  }

  // enable this when need:
  // $: {menus = processMenu(clone(OldMenu), $org_id, project_id()) }

  return (
    <>
      {/* <Show when={!IS_PRODUCTION}>PROJECT LAYOUT</Show> */}
      {/* JSON.stringify($project_data_ctx) */}
      {/* <div style="display: flex"> */}
      {/*  <h4>Selected Project:&nbsp</h4> */}
      {/*  <h3>{projectIdMemo}</h3> */}
      {/* </div> */}

      <Switch>
        <Match when={state.isLoading}>Loading...</Match>
        <Match when={!state.authorized}>{state.er}</Match>
        <Match when={state.isFetchingData && state.isFetchingMenu}>
          <Skeleton />
        </Match>
        <Match when={!(state.isFetchingData && state.isFetchingMenu)}>
          <Switch>
            <MatchRoute path="/organization/:org/project/:project/activities">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/groups">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="project_group" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/roles">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="project_role" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/roadmap">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/work_packages">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="work_package" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/boards">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="work_packages" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/backlogs">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/calendar">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/news">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="news" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/forum">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="forum" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/wiki">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="wiki" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/cost_reports">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/members">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/cost_objects">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/meetings">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/settings">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/posts">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="post" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/conversation">
              <ChatAdmin />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/message">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="message" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/participant">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="participant" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/alias">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="alias" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/chat_department">
              <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="chat_department" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project">
              <ProjectIndex />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project/index">
              <ProjectIndex />
            </MatchRoute>
          </Switch>
        </Match>
      </Switch>
    </>
  );
};
