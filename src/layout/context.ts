import { createContext, createSignal } from "solid-js";
import { TableResult } from "../utils/enums";

// used context to support multi window app
// Signals unlike state by default don't do equality check.
export const OrgIDContext = createContext(createSignal(""));
export const OrgDocumentContext = createContext(createSignal({ _key: "" } as { [key: string]: any }));

export const ProjectIDContext = createContext(createSignal(""));
export const ProjectDocumentContext = createContext(createSignal({ _key: "" } as { [key: string]: any }));

export const ProjectStackContext = createContext(createSignal([] as TableResult));
