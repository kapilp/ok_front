import { createComputed, createMemo, Match, onCleanup, Show, Switch, untrack, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import { clone } from 'rambda';
import UrlPattern from 'url-pattern';
import { MatchRoute, useRoute } from '@rturnq/solid-router';
import { Skeleton } from '../components/UI/Skeleton';
import { Table } from '../components/table/Table';
import { ET, GeneralError, IS_PRODUCTION, isEr, makeTableArgs, OrganizationLayoutError, TableResult, TableResultAll, WsStatus } from '../utils/enums';
import { getWsConnected, Ws } from '../utils/ws_events_dispatcher';
import { OrgIndex } from '../views/org_index';
import { ProjectLayout } from './project_layout';
import { OrgDocumentContext, OrgIDContext, ProjectStackContext } from './context';
import { pop, push } from './adminLayoutState';
import { Alert, AlertTypes } from '../components/UI/core/alert/src/Alert';
// import { SvgIconPerson } from '../components/UI/core/icon/src/icon/IconPerson';
// import { SvgIconOutlineQuestionMark } from '../components/UI/terra-consumer/packages/terra-consumer-icon/src/icon/IconOutlineQuestionMark';
// import { SvgIconTile } from '../components/UI/core/icon/src/icon/IconTile';

export interface Menu {
  url: string;
  text: string;
  subItems?: Menu[];
  hidden?: boolean;
}

export const OrganizationLayout = () => {
  const [state, setState] = createStore({
    er: '',
    authorized: true,
    isLoading: true,
    isFetchingData: true,
    isFetchingMenu: true,
    items: [] as TableResult,
    organization_menu: [] as Menu[],
  });

  const [, setOrgIDContext] = useContext(OrgIDContext);
  const [getOrgDocumentContext, setOrgDocumentContext] = useContext(OrgDocumentContext);
  const [getProjectStackContext, setProjectStackContext] = useContext(ProjectStackContext);

  const { params } = useRoute();
  const orgIdMemo = createMemo(() => params?.org ?? '');
  createComputed(() => {
    setOrgIDContext(orgIdMemo());
  });

  setOrgDocumentContext({ _key: '' });
  setProjectStackContext([]);

  const org_fetch_evt = [ET[ET.get], 'organization_list', Ws.uid];

  onCleanup(
    Ws.bind$(
      org_fetch_evt,
      (d: TableResultAll): void => {
        if (isEr(d, state, setState)) {
          setState({ authorized: false });
          return;
        }
        // TODO: also consider subscribe and handle modified and delete result
        // and unsubscribe on cleanup
        const { result } = d.r;
        if (result.length == 0) {
          setState({ er: OrganizationLayoutError.NO_ORG_FOUND });
          // TODO: show notification that organization not exist or Org ID changed.
          setTimeout(() => (window.location.href = '/'), 500);
        } else if (result[0]) {
          const orgDocument = result[0];
          setOrgDocumentContext(orgDocument);
          setProjectStackContext([orgDocument]); // assume that first subscribed to ws_connected trigger first.
          setState({ isFetchingData: false, isLoading: false });
        }
      },
      1,
    ),
  );

  // re trigger fetch event when ws reconnected.
  createComputed(() => {
    if (getWsConnected()) {
      setState({ er: GeneralError.NO_ERROR });
      if (orgIdMemo()) {
        Ws.trigger([
          [
            org_fetch_evt,
            makeTableArgs()
              .setFilter({ id: `="${untrack(orgIdMemo)}"` })
              .setAll(true),
          ],
        ]);
      } else {
        setState({ er: OrganizationLayoutError.NO_ORG_ID });
      }
    } else {
      setState({ er: WsStatus.RECONNECTION });
    }
  });

  onCleanup(() => {
    // be very careful: top level component unmount first.
    const newProject = clone(getProjectStackContext());
    newProject.pop();
    setProjectStackContext(newProject);
  });

  const setupMenu = () => {
    const menu = [
      {
        icon: () => () => <span class="mif-meter" />,
        text: 'Overview',
        url: '/organization/:org',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Projects',
        url: '/organization/:org/projects',
      },
      {
        subItems: [
          {
            text: 'Settings',
            url: '/organization/:org/members_settings',
          },
          {
            text: 'Members',
            url: '/organization/:org/members',
          },
          {
            text: 'Groups',
            url: '/organization/:org/groups',
          },
          {
            text: 'Roles',
            url: '/organization/:org/roles',
          },
          // {
          //   text: "Permissions",
          //   url: "/organization/:org/permissions"
          // },
          {
            text: 'Avatars',
            url: '/organization/:org/avatars',
          },
        ],
        icon: () => <span class="mif-meter" />,
        text: 'Members & Permissions',
        url: '/organization/:org/members',
      },
      {
        subItems: [
          {
            text: 'Settings',
            url: '/organization/:org/work_package_settings',
          },
          {
            text: 'Types',
            url: '/organization/:org/types',
          },
          {
            text: 'Status',
            url: '/organization/:org/status',
          },
          {
            text: 'Workflow',
            url: '/organization/:org/custom_fields',
          },
        ],
        icon: () => <span class="mif-assignment" />,
        text: 'Work packages',
        url: '/organization/:org/work_package_settings',
      },
      {
        text: 'Custom actions',
        url: '/organization/:org/custom_actions',
      },
      {
        text: 'Attribute Help Text',
        url: '/organization/:org/attribute_help_texts',
      },
      {
        icon: () => <span class="mif-battery-empty" />,
        text: 'Custom fields',
        url: '/organization/:org/custom_fields',
      },
      {
        icon: () => <span class="mif-hammer" />,
        text: 'System settings',
        url: '/organization/:org/settings',
      },
      {
        icon: () => <span class="mif-pencil" />,
        text: 'Design',
        url: '/organization/:org/design',
      },
      {
        icon: () => <span class="icon_5 icon-custom-fields" />,
        text: 'Overview',
        url: '/organization/:org/custom_styles',
      },
      {
        icon: () => <span class="mif-pencil" />,
        text: 'Colors',
        url: '/organization/:org/colors',
      },
      {
        icon: () => <span class="mif-balance-scale" />,
        text: 'Budgets',
        url: '/organization/:org/budgets',
      },
      {
        icon: () => <span class="mif-backlog" />,
        text: 'Backlogs',
        url: '/organization/:org/backlogs',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Activities',
        url: '/organization/:org/activities',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Document Categories',
        url: '/organization/:org/document_categories',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Wiki',
        url: '/organization/:org/wiki',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Forum',
        url: '/organization/:org/forum',
      },
      {
        icon: () => <span class="mif-info" />,
        text: 'Post',
        url: '/organization/:org/posts',
      },
    ];
    push(processMenu(clone(menu), orgIdMemo()));
    setState({ isFetchingMenu: false });
  };

  setupMenu();

  onCleanup(pop);

  /* const setOrganizationMenu = () => {
    const idx = findMenuIdx(state.items, 'organization');
    if (idx > -1) {
      const OldMenu: Menu[] = state.items[idx].menu;
      setAdminLayoutState('nav', (l: { navItems: [] }) => {
        l.navItems[0] = processMenu(clone(OldMenu), orgIdMemo());
      });
      setState({ isFetchingMenu: false });
    }
  };
  onCleanup(
    Ws.bindT(
      [ET[ET.subscribe], "menu_list", Ws.uid],
      d => {
        getMenuDataGet(d, state, setState, setOrganizationMenu);
      },
      makeTableArgs().setFilter({ _key: `['organization']` }).setType(ValueType.Object),
      1,
    ),
  ); */

  function processMenu(menu_: Menu[], org: string) {
    if (!org) {
      setState({ er: OrganizationLayoutError.EMPTY_ORG_ID });
      return [];
    }
    for (const x of menu_) {
      x.url = new UrlPattern(x.url).stringify({ org });
      if (x.subItems) {
        x.subItems = processMenu(x.subItems, org);
      }
    }
    return menu_;
  }

  return (
    <>
      {/* <Show when={!IS_PRODUCTION}>ORGANIZATION LAYOUT</Show> */}

      {state.er && <Alert type={AlertTypes.ERROR}>{state.er}</Alert>}
      {/* <div style="display: flex"> */}
      {/*  <h4>Selected Organization:&nbsp</h4> */}
      {/*  <h3>{orgIdMemo()}</h3> */}
      {/* </div> */}

      <Switch>
        <Match when={state.isLoading}>Loading...</Match>
        <Match when={!state.authorized}>{state.er}</Match>
        <Match when={state.isFetchingData && state.isFetchingMenu}>
          <Skeleton />
        </Match>
        <Match when={!(state.isFetchingData && state.isFetchingMenu)}>
          <Switch>
            <MatchRoute path="/organization/:org/organizations">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="organization" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/projects">
              <Table fetchConfig={{ organization: getOrgDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="project" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/groups">
              <Table fetchConfig={{ organization: getOrgDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="organization_group" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/roles">
              <Table fetchConfig={{ organization: getOrgDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="organization_role" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/members">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/members_settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            {/* <MatchRoute path="/organization/:org/permissions"> */}
            {/*  <Table fetchConfig={{}} modelComponent={true} */}
            {/*         quickEditComponent={true} schemaKey={"permission"} /> */}
            {/* </MatchRoute> */}
            <MatchRoute path="/organization/:org/avatars">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/work_package_settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/custom_fields">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/custom_actions">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/attribute_help_texts">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/custom_fields">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/oauth_applications">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/auth_settings">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/design">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/custom_styles">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/budgets">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/backlogs">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/info">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/webhooks">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="member" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/colors">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="color" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/types">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="type" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/priorities">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="priority" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/status">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="status" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/activities">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="activity" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/document_categories">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="doc_category" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/media">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="media" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/message">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="message" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/categories">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="category" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/announcements">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="announcement" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/news">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="news" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/wiki">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="wiki" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/work_packages">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="work_packages" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/forum">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="forum" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/posts">
              <Table fetchConfig={{}} modelComponent quickEditComponent schemaKey="post" />
            </MatchRoute>
            <MatchRoute path="/organization/:org/project/:project">
              <ProjectLayout />
            </MatchRoute>
            <MatchRoute path="/organization/:org/index">
              <OrgIndex />
            </MatchRoute>
            <MatchRoute path="/organization/:org">
              <OrgIndex />
            </MatchRoute>
          </Switch>
        </Match>
      </Switch>
    </>
  );
};
