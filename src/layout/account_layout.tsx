import { Match, onCleanup, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { MatchRoute } from '@rturnq/solid-router';
import { FormServer } from '../components/formServer/FormServer';
import { Skeleton } from '../components/UI/Skeleton';
import { TableResult } from '../utils/enums';
import { pop, push } from './adminLayoutState';

export const AccountLayout = () => {
  const [state, setState] = createStore({
    er: '',
    authorized: true,
    isLoading: true,
    isFetching: false,
    items: [] as TableResult,
    account_menu: [],
  });

  const setupMenu = () => {
    const menu = [
      {
        text: 'Profile',
        url: '/account/profile',
      },
      {
        text: 'Setting',
        url: '/account/settings',
      },
      {
        text: 'Change Password',
        url: '/account/change_password',
      },
      {
        text: 'Access Token',
        url: '/account/access_token',
      },
      {
        text: 'Email notifications',
        url: '/account/mail_notifications',
      },
      {
        text: 'Avatar',
        url: '/account/avatar',
      },
      {
        text: 'Delete Account',
        url: '/account/delete_account',
      },
    ];
    push(menu);
    setState({ isLoading: false });
  };

  setupMenu();

  onCleanup(pop);

  /* const setAccountMenu = () => {
    const idx = findMenuIdx(state.items, 'account');
    if (idx > -1) setState({ account_menu: state.items[idx].menu });
  };

  onCleanup(
    Ws.bindT(
      [ET[ET.subscribe], "menu_list", Ws.uid],
      d => {
        getMenuDataGet(d, state, setState, setAccountMenu);
        setState({ isFetching: false });
      },
      makeTableArgs().setFilter({ _key: `['account']` }).setType(ValueType.Object),
      1,
    ),
  ); */

  return (
    <div>
      <Switch>
        <Match when={state.isLoading}>Loading...</Match>
        <Match when={!state.authorized}>{state.er}</Match>
        <Match when={state.isFetching}>
          <Skeleton />
        </Match>
        <Match when={!state.isFetching}>
          <Switch>
            <MatchRoute path="/account/profile">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/settings">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/change_password">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/access_token">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/mail_notifications">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/avatar">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>

            <MatchRoute path="/account/delete_account">
              <FormServer fetchConfig={{}} key="member_setting" schemaKey="setting" />
            </MatchRoute>
          </Switch>
        </Match>
      </Switch>
    </div>
  );
};
