import { JSX } from 'solid-js';
import { createStore } from 'solid-js/store';

import './todo.css';

export default { title: 'Html Test/Drag' };
// https://medium.com/the-andela-way/react-drag-and-drop-7411d14894b9
document.body.ondrop = function (event) {
  event.preventDefault();
  // event.stopPropagation();
};
export function DragExample() {
  const [state, setState] = createStore({
    todos: [
      {
        taskID: 1,
        task: 'Walk the walk',
      },
      {
        taskID: 2,
        task: 'Talk the talk',
      },
      {
        taskID: 3,
        task: 'Jump the jump',
      },
    ],
    completedTasks: [],
    draggedTask: {},
  });
  // dragover vs dragenter events of HTML5 drag & drop
  // https://stackoverflow.com/questions/32676890/dragover-vs-dragenter-events-of-html5-drag-drop
  const onDrag = todo => e => {
    // e.preventDefault();
    console.log(1);
    setState({
      draggedTask: todo,
    });
  };
  const onDragOver = event => {
    event.preventDefault();
    console.log(2);
  };
  const onDrop = event => {
    console.log(3);
    setState({
      completedTasks: [...state.completedTasks, state.draggedTask],
      todos: state.todos.filter(task => task.taskID !== state.draggedTask.taskID),
      draggedTask: {},
    });
  };
  return (
    <div>
      <div class="App">
        <div class="todos">
          {state.todos.map(todo => (
            <div draggable onDragStart={onDrag(todo)}>
              {todo.task}
            </div>
          ))}
        </div>
        <div class="done" onDrop={onDrop} onDragOver={onDragOver}>
          {state.completedTasks.map(task => (
            <div>{task.task}</div>
          ))}
        </div>
      </div>
    </div>
  );
}
