import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { JSX, onCleanup } from 'solid-js';
import { Status } from '../../../components/UI/core/status/src/Status';

export default { title: 'SolidJs/Rendering/React' };

// import styles from './colors.module.scss';
// const cx = classNames.bind(styles);

// <Status colorClass={cx(['success'])} visuallyHiddenText="Status Success">{simpleText}</Status>

// Yeah I made a wrapper example a while back. Similar idea. But children will be incompatible. https://codesandbox.io/s/solid-react-hoc-8m2yd?file=/solid-react.js . It's pretty hard to pass children to react as it's Virtual DOM doesn't handle DOM nodes.. without doing manual refs..
export const FirstComponent = (props: {}) => {
  let container: HTMLDivElement | null;
  const elementSetup = (el: HTMLDivElement) => {
    container = el;
    const children = <div>Sample text</div>;
    window.setTimeout(ReactDOM.render(React.createElement(Status, props, children), container), 0);
  };
  onCleanup(() => {
    ReactDOM.unmountComponentAtNode(container);
  });
  return <div ref={elementSetup} />;
};
