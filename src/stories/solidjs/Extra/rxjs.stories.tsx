import { JSX, createSignal, createMemo, createEffect, For } from 'solid-js';
import { createStore, reconcile } from 'solid-js/store';
import { pipe, tap, map, filter, transform, mergeMap, pairwise } from 'solid-rx';
// https://github.com/ryansolid/solid/tree/master/packages/solid-rx
export default {
  title: 'SolidJs/Extra/RxJs',
};

function selectClass(selected, className) {
  return list => {
    createEffect(
      transform(
        list,
        // wrap selection in accessor function and merge since map operators are not tracked
        // find selected element
        mergeMap(list => () => list.find(el => el === selected())),
        // group prev value with current
        pairwise(),
        // tap value for side effect of setting `className`
        tap(([prevEl, el]) => {
          prevEl && (prevEl.className = '');
          el && (el.className = className);
        }),
      ),
    );
    // return the original signal
    return list;
  };
}

const ForWithSelection = props => {
  const applyClass = selectClass(() => props.selected, 'active');
  return applyClass(<For each={props.each}>{props.children}</For>);
};

// in a component somewhere:

export const RxJs = (props: { n: number }) => {
  const [state, setState] = createStore({ list: [{ id: 1 }, { id: 2 }, { id: 3 }], selected: 1 }); // selected state isnt perfect
  return (
    <div>
      <ForWithSelection each={state.list} selected={state.selected}>
        {row => <div model={row.id}>{row.description}d</div>}
      </ForWithSelection>
      ;
    </div>
  );
};
