import { JSX, Component, For, createSignal, Show } from 'solid-js';
import { Icon } from '@amoutonbrady/solid-heroicons';
import * as solid from '@amoutonbrady/solid-heroicons/solid';
import * as outline from '@amoutonbrady/solid-heroicons/outline';

// @ts-ignore
const solidIcons: string[] = Object.values(solid);
// @ts-ignore
const outlineIcons: string[] = Object.values(outline);

export const SolidHeroIcons = () => {
  const [color, setColor] = createSignal('#000000');
  const [showOutline, setShowOutline] = createSignal(true);

  return (
    <>
      <div
        style={{
          color: color(),
          display: 'grid',
          'grid-template-columns': 'repeat(auto-fill, minmax(50px, 1fr))',
          'grid-gap': '1rem',
        }}
      >
        <Show when={showOutline()}>
          <For each={outlineIcons}>{icon => <Icon path={icon} outline style={{ width: '100%' }} />}</For>
        </Show>

        <Show when={!showOutline()}>
          <For each={solidIcons}>{icon => <Icon path={icon} style={{ width: '100%' }} />}</For>
        </Show>
      </div>

      <div
        style={{
          position: 'fixed',
          bottom: '24px',
          right: '24px',
          background: '#333',
          padding: '15px',
          'border-radius': '15px',
        }}
      >
        <input type="color" onChange={e => setColor(e.target.value)} />
        <button onClick={() => setShowOutline(!showOutline())}>Toggle Solid/Outline</button>
      </div>
    </>
  );
};
