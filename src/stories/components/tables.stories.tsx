export default { title: "Application/Tables" };
import { Fragment } from "../../headLessUi";
import { createEffect, createSignal, For } from "solid-js";

const statusStyles = {
  success: "bg-green-100 text-green-800",
  processing: "bg-yellow-100 text-yellow-800",
  failed: "bg-gray-100 text-gray-800",
};

interface PersonProps {
  name: string;
  email?: string;
  title?: string;
  role?: string;
}

interface TableProps {
  data: PersonProps[];
}
interface TablePropsWithLocation {
  data: {
    name: string;
    people: PersonProps[];
  }[];
}

const people = [
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
  {
    name: "Mathys Léonard",
    email: "mathy@mail.com",
    role: "Senior Executive",
    title: "Manager",
  },
];

const location = [
  {
    name: "Ahmedabad",
    people: [
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
    ],
  },
  {
    name: "Mumbai",
    people: [
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
    ],
  },
  {
    name: "Delhi",
    people: [
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
    ],
  },
  {
    name: "Bangalore",
    people: [
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
      {
        name: "Mathys Léonard",
        email: "mathy@mail.com",
        role: "Senior Executive",
        title: "Manager",
      },
    ],
  },
];

function classNames(...classes: (string | boolean | undefined)[]): string {
  return classes.filter(Boolean).join(" ");
}

 const TableWithHiddenColumn = ({ data }: TableProps) => {
  return (
    <>
      <div class="px-4 sm:px-6 lg:px-8">
        <div class="sm:flex sm:items-center">
          <div class="sm:flex-auto">
            <h1 class="text-xl font-semibold text-gray-900">Table with hidden Columns</h1>
            <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>
          </div>
          <div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
            <button
              type="button"
              class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
            >
              Add user
            </button>
          </div>
        </div>
        <div class="-mx-4 mt-8 overflow-hidden shadow ring-1 ring-black ring-opacity-5 sm:-mx-6 md:mx-0 md:rounded-lg">
          <table class="min-w-full divide-y divide-gray-300">
            <thead class="bg-gray-50">
              <tr>
                <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                  Name
                </th>
                <th scope="col" class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 sm:table-cell">
                  Title
                </th>
                <th scope="col" class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                  Email
                </th>
                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                  Role
                </th>
                <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                  <span class="sr-only">Edit</span>
                </th>
              </tr>
            </thead>
            <tbody class="divide-y divide-gray-200 bg-white">
              <For each={data} fallback={<div>Loading...</div>}>
                {(person) => (
                  <tr>
                    <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{person.name}</td>
                    <td class="hidden whitespace-nowrap px-3 py-4 text-sm text-gray-500 sm:table-cell">{person.title}</td>
                    <td class="hidden whitespace-nowrap px-3 py-4 text-sm text-gray-500 lg:table-cell">{person.email}</td>
                    <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.role}</td>
                    <td class="whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                      <a href="#" class="text-indigo-600 hover:text-indigo-900">
                        Edit<span class="sr-only">, {person.name}</span>
                      </a>
                    </td>
                  </tr>
                )}
              </For>
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

 const TableWithStickyHeader = ({ data }: TableProps) => {
  return (
    <div class="px-4 sm:px-6 lg:px-8">
      <div class="sm:flex sm:items-center">
        <div class="sm:flex-auto">
          <h1 class="text-xl font-semibold text-gray-900">Table with sticky headers</h1>
          <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>
        </div>
        <div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
          <button
            type="button"
            class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
          >
            Add user
          </button>
        </div>
      </div>
      <div class="mt-8 flex flex-col">
        <div class="-my-2 -mx-4 sm:-mx-6 lg:-mx-8">
          <div class="inline-block min-w-full py-2 align-middle">
            <div class="shadow-sm ring-1 ring-black ring-opacity-5">
              <table class="min-w-full border-separate" style={{ borderSpacing: 0 }}>
                <thead class="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      class="sticky top-0 z-10 border-b border-gray-300 bg-gray-50 bg-opacity-75 py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter sm:pl-6 lg:pl-8"
                    >
                      Name
                    </th>
                    <th
                      scope="col"
                      class="sticky top-0 z-10 hidden border-b border-gray-300 bg-gray-50 bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter sm:table-cell"
                    >
                      Title
                    </th>
                    <th
                      scope="col"
                      class="sticky top-0 z-10 hidden border-b border-gray-300 bg-gray-50 bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter lg:table-cell"
                    >
                      Email
                    </th>
                    <th
                      scope="col"
                      class="sticky top-0 z-10 border-b border-gray-300 bg-gray-50 bg-opacity-75 px-3 py-3.5 text-left text-sm font-semibold text-gray-900 backdrop-blur backdrop-filter"
                    >
                      Role
                    </th>
                    <th scope="col" class="sticky top-0 z-10 border-b border-gray-300 bg-gray-50 bg-opacity-75 py-3.5 pr-4 pl-3 backdrop-blur backdrop-filter sm:pr-6 lg:pr-8">
                      <span class="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody class="bg-white">
                  <For each={data} fallback={<div>Loading...</div>}>
                    {(person, i) => {
                      return (
                        <tr>
                          <td class={classNames(true ? "border-b border-gray-200" : "", "whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6 lg:pl-8")}>
                            {person.name}
                          </td>
                          <td class={classNames(true ? "border-b border-gray-200" : "", "whitespace-nowrap px-3 py-4 text-sm text-gray-500 hidden sm:table-cell")}>
                            {person.title}
                          </td>
                          <td class={classNames(true ? "border-b border-gray-200" : "", "whitespace-nowrap px-3 py-4 text-sm text-gray-500 hidden lg:table-cell")}>
                            {person.email}
                          </td>
                          <td class={classNames(true ? "border-b border-gray-200" : "", "whitespace-nowrap px-3 py-4 text-sm text-gray-500")}>{person.role}</td>
                          <td
                            class={classNames(true ? "border-b border-gray-200" : "", "relative whitespace-nowrap py-4 pr-4 pl-3 text-right text-sm font-medium sm:pr-6 lg:pr-8")}
                          >
                            <a href="#" class="text-indigo-600 hover:text-indigo-900">
                              Edit<span class="sr-only">, {person.name}</span>
                            </a>
                          </td>
                        </tr>
                      );
                    }}
                  </For>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

 const TableWithGroupedRows = ({ data }: TablePropsWithLocation) => {
  return (
    <div class="px-4 sm:px-6 lg:px-8">
      <div class="sm:flex sm:items-center">
        <div class="sm:flex-auto">
          <h1 class="text-xl font-semibold text-gray-900">Table with Grouped Rows</h1>
          <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>
        </div>
        <div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
          <button
            type="button"
            class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
          >
            Add user
          </button>
        </div>
      </div>
      <div class="mt-8 flex flex-col">
        <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
            <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
              <table class="min-w-full">
                <thead class="bg-white">
                  <tr>
                    <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                      Name
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Title
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Email
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Role
                    </th>
                    <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                      <span class="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody class="bg-white">
                  <For each={data} fallback={<div>Loading...</div>}>
                    {(location) => (
                      <Fragment>
                        <tr class="border-t border-gray-200">
                          <th colSpan={5} scope="colgroup" class="bg-gray-50 px-4 py-2 text-left text-sm font-semibold text-gray-900 sm:px-6">
                            {location.name}
                          </th>
                        </tr>
                        <For each={location.people} fallback={<div>Loading...</div>}>
                          {(person, index) => (
                            <tr class={classNames(true ? "border-gray-300" : "border-gray-200", "border-t")}>
                              <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{person.name}</td>
                              <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.title}</td>
                              <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.email}</td>
                              <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.role}</td>
                              <td class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                <a href="#" class="text-indigo-600 hover:text-indigo-900">
                                  Edit<span class="sr-only">, {person.name}</span>
                                </a>
                              </td>
                            </tr>
                          )}
                        </For>
                      </Fragment>
                    )}
                  </For>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

 const TableWithCheckBox = ({ data }: TableProps) => {
  const [checked, setChecked] = createSignal(false);
  const [indeterminate, setIndeterminate] = createSignal(false);
  const [selectedPeople, setSelectedPeople] = createSignal([{}]);

  function toggleAll() {
    setSelectedPeople(checked() ? [] : data);
    setChecked(!checked() && !indeterminate());
    setIndeterminate(false);
  }

  return (
    <div class="px-4 sm:px-6 lg:px-8">
      <div class="sm:flex sm:items-center">
        <div class="sm:flex-auto">
          <h1 class="text-xl font-semibold text-gray-900">Table With CheckBox</h1>
          <p class="mt-2 text-sm text-gray-700">A list of all the users in your account including their name, title, email and role.</p>
        </div>
        <div class="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
          <button
            type="button"
            class="inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
          >
            Add user
          </button>
        </div>
      </div>
      <div class="mt-8 flex flex-col">
        <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
            <div class="relative overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
              {selectedPeople().length > 0 && (
                <div class="absolute top-0 left-12 flex h-12 items-center space-x-3 bg-gray-50 sm:left-16">
                  <button
                    type="button"
                    class="inline-flex items-center rounded border border-gray-300 bg-white px-2.5 py-1.5 text-xs font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-30"
                  >
                    Bulk edit
                  </button>
                  <button
                    type="button"
                    class="inline-flex items-center rounded border border-gray-300 bg-white px-2.5 py-1.5 text-xs font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-30"
                  >
                    Delete all
                  </button>
                </div>
              )}
              <table class="min-w-full table-fixed divide-y divide-gray-300">
                <thead class="bg-gray-50">
                  <tr>
                    <th scope="col" class="relative w-12 px-6 sm:w-16 sm:px-8">
                      <input
                        type="checkbox"
                        class="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500 sm:left-6"
                        ref={(checkbox1) => {
                          console.log(">>", checkbox1.checked);
                        }}
                        checked={checked()}
                        onChange={(inp) => {
                          console.log(inp);
                          // console.log
                          toggleAll();
                          // setChecked(true)
                        }}
                      />
                    </th>
                    <th scope="col" class="min-w-[12rem] py-3.5 pr-3 text-left text-sm font-semibold text-gray-900">
                      Name
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Title
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Email
                    </th>
                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                      Role
                    </th>
                    <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                      <span class="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody class="divide-y divide-gray-200 bg-white">
                  <For each={data} fallback={<div>Loading...</div>}>
                    {(person) => (
                      <tr class={selectedPeople().includes(person) ? "bg-gray-50" : undefined}>
                        <td class="relative w-12 px-6 sm:w-16 sm:px-8">
                          {selectedPeople().includes(person) && <div class="absolute inset-y-0 left-0 w-0.5 bg-indigo-600" />}
                          <input
                            type="checkbox"
                            class="absolute left-4 top-1/2 -mt-2 h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500 sm:left-6"
                            value={person.email}
                            checked={selectedPeople().includes(person)}

                            // onChange={(e) => setSelectedPeople(e.target. ? [...selectedPeople(), person] : selectedPeople().filter((p) => p !== person))}
                          />
                        </td>
                        <td class={classNames("whitespace-nowrap py-4 pr-3 text-sm font-medium", selectedPeople().includes(person) ? "text-indigo-600" : "text-gray-900")}>
                          {person.name}
                        </td>
                        <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.title}</td>
                        <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.email}</td>
                        <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{person.role}</td>
                        <td class="whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                          <a href="#" class="text-indigo-600 hover:text-indigo-900">
                            Edit<span class="sr-only">, {person.name}</span>
                          </a>
                        </td>
                      </tr>
                    )}
                  </For>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export const TablesComponents = () => {
  return (
    <div class="min-h-full ">
      <TableWithHiddenColumn data={people} />
      <div class="h-5" />
      <TableWithStickyHeader data={people} />
      <div class="h-5" />
      <TableWithGroupedRows data={location} />
      <div class="h-5" />
      <TableWithCheckBox data={people} />
    </div>
  );
};
