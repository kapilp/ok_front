// commented because crashing
// import { JSX, Switch } from 'solid-js';
// import { Router, pathIntegration, MatchRoute, NavLink } from '@rturnq/solid-router';
// import './nav.css';
//
// export default { title: 'Library/Rturnq Router' };
//
// // components
// function Layout(props: { children: JSX.Element }) {
//   return (
//     <div>
//       <h1>Solid MatchRouter</h1>
//       <hr />
//       <Nav />
//       <hr />
//       {props.children}
//     </div>
//   );
// }
//
// function Nav() {
//   return (
//     <ul>
//       <li>
//         <NavLink class="link" activeClass="active" href="/" end>
//           Home Page
//         </NavLink>
//       </li>
//       <li>
//         <NavLink class="link" activeClass="active" href="/about" end>
//           About Page
//         </NavLink>
//       </li>
//       <li>
//         <NavLink class="link" activeClass="active" href="/widgets/30" end>
//           Widget Page
//         </NavLink>
//       </li>
//       <li>
//         <NavLink class="link" activeClass="active" href="/not-found" end>
//           Not found
//         </NavLink>
//       </li>
//     </ul>
//   );
// }
//
// // Pages
// function About(props: {}) {
//   return <p>About Page{JSON.stringify(props)}</p>;
// }
// function Home(props: {}) {
//   return <p>Home Page{JSON.stringify(props)}</p>;
// }
// function NotFound(props: {}) {
//   return <p>404 Page{JSON.stringify(props)}</p>;
// }
// function WidgetPage(props: { id: string }) {
//   return <div>Widget id: {props.id}</div>;
// }
//
// const App = () => {
//   return (
//     <Layout>
//       <Switch fallback={<NotFound />}>
//         <MatchRoute path="/" end>
//           <Home />
//         </MatchRoute>
//         <MatchRoute path="/about">
//           <About />
//         </MatchRoute>
//         <MatchRoute path="widgets/:id">{route => <WidgetPage id={route.params.id} />}</MatchRoute>
//       </Switch>
//     </Layout>
//   );
// };

// commented because crashing
// export const RouteApplication = () => {
//   return (
//     <Router integration={pathIntegration()}>
//       <App />
//     </Router>;
//   );
// };
