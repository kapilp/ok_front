import { Router, pathIntegration } from '@rturnq/solid-router';
import { ActionFooterStandardSingleEndActionExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/standard/SingleEndAction';
import { ActionFooterStandardSingleStartActionExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/standard/SingleStartAction';
import { ActionFooterStandardMultipleStartEndActionsExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/standard/MultipleStartEndActions';
import { ActionFooterBlockSingleActionExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/block/SingleAction';
import { ActionFooterBlockMultipleActionsExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/block/MultipleActions';
import { ActionFooterCenteredMultipleActionsExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/centered/MultipleActions';
import { ActionFooterCenteredSingleActionExample } from '../../components/UI/core/action-footer/src/terra-dev-site/doc/example/centered/SingleAction';
import { Base } from '../../components/UI/core/base/src/Base';
import { ArrangeAlignAllContainers } from '../../components/UI/core/arrange/src/terra-dev-site/doc/example/ArrangeAlignAllContainers';
import { ArrangeAlignFill } from '../../components/UI/core/arrange/src/terra-dev-site/doc/example/ArrangeAlignFill';
import { ArrangeAlignFitStart } from '../../components/UI/core/arrange/src/terra-dev-site/doc/example/ArrangeAlignFitStart';
import { ArrangeAlignFitEnd } from '../../components/UI/core/arrange/src/terra-dev-site/doc/example/ArrangeAlignFitEnd';
import { BadgeIntent } from '../../components/UI/core/badge/src/terra-dev-site/doc/example/BadgeIntent';
import { BadgeSize } from '../../components/UI/core/badge/src/terra-dev-site/doc/example/BadgeSize';
import { BadgeIcon } from '../../components/UI/core/badge/src/terra-dev-site/doc/example/BadgeIcon';
import { BadgeVisuallyHiddenText } from '../../components/UI/core/badge/src/terra-dev-site/doc/example/BadgeVisuallyHiddenText';
import { ButtonVariant } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonVariant';
import { ButtonIcon } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonIcon';
import { ButtonBlock } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonBlock';
import { ButtonAnchor } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonAnchor';
import { ButtonOnClick } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonOnClick';
import { ButtonCompact } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonCompact';
import { ButtonDisabled } from '../../components/UI/core/button/src/terra-dev-site/doc/example/ButtonDisabled';
import { CardPaddingHR } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardPaddingHR';
import { CardPaddingVertical } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardPaddingVertical';
import { CardPaddingHorizontal } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardPaddingHorizontal';
import { CardPadding } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardPadding';
import { CardContentCentered } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardContentCentered';
import { CardDefault } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardDefault';
import { CardRaised } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardRaised';
import { CardVisuallyHiddenText } from '../../components/UI/core/card/src/terra-dev-site/doc/example/CardVisuallyHiddenText';
import { CellGridBoth } from '../../components/UI/core/cell-grid/src/terra-dev-site/doc/example/CellGridBoth';
import { CellGridVertical } from '../../components/UI/core/cell-grid/src/terra-dev-site/doc/example/CellGridVertical';
import { CellGridExample } from '../../components/UI/core/cell-grid/src/terra-dev-site/doc/example/CellGridExample';
import { CellGridHorizontal } from '../../components/UI/core/cell-grid/src/terra-dev-site/doc/example/CellGridHorizontal';
import { ContentContainerStandard } from '../../components/UI/core/content-container/src/terra-dev-site/doc/example/ContentContainerStandard';
import { ContentContainerFill } from '../../components/UI/core/content-container/src/terra-dev-site/doc/example/ContentContainerFill';
import { ContentContainerScrollRef } from '../../components/UI/core/content-container/src/terra-dev-site/doc/example/ContentContainerScrollRef';
import { ScrollVertical } from '../../components/UI/core/scroll/src/terra-dev-site/doc/example/ScrollVertical';
import { ScrollHorizontal } from '../../components/UI/core/scroll/src/terra-dev-site/doc/example/ScrollHorizontal';
import { DividerWithWrappingText } from '../../components/UI/core/divider/src/terra-dev-site/doc/example/DividerWithWrappingText';
import { DividerExample } from '../../components/UI/core/divider/src/terra-dev-site/doc/example/DividerExample';
import { DividerWithText } from '../../components/UI/core/divider/src/terra-dev-site/doc/example/DividerWithText';
import { GridDefault } from '../../components/UI/core/grid/src/terra-dev-site/doc/example/GridDefault';
import { GridResponsive } from '../../components/UI/core/grid/src/terra-dev-site/doc/example/GridResponsive';
import { GridNested } from '../../components/UI/core/grid/src/terra-dev-site/doc/example/GridNested';
import { HeadingColors } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingColors';
import { HeadingVariations } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingVariations';
import { HeadingWeights } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingWeights';
import { HeadingSizes } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingSizes';
import { HeadingLevels } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingLevels';
import { HeadingVisuallyHidden } from '../../components/UI/core/heading/src/terra-dev-site/doc/example/HeadingVisuallyHidden';
import { IconAllStatic } from '../../components/UI/core/icon/src/terra-dev-site/doc/example/IconStatic';
import { IconAllThemeable } from '../../components/UI/core/icon/src/terra-dev-site/doc/example/IconThemeable';
import { Heading } from '../../components/UI/core/heading/src/Heading';
import { ProfileImageDefault } from '../../components/UI/core/profile-image/src/terra-dev-site/doc/example/ProfileImageDefault';
import { ProfileImageFitTypes } from '../../components/UI/core/profile-image/src/terra-dev-site/doc/example/ProfileImageFitTypes';
import { ImageDefault } from '../../components/UI/core/image/src/terra-dev-site/doc/example/ImageDefault';
import { ImageFitTypes } from '../../components/UI/core/image/src/terra-dev-site/doc/example/ImageFitTypes';
import { ProgressBarColor } from '../../components/UI/core/progress-bar/src/terra-dev-site/doc/example/ProgressBarColor';
import { ProgressBarGradient } from '../../components/UI/core/progress-bar/src/terra-dev-site/doc/example/ProgressBarTwoColors';
import { ProgressBarDefault } from '../../components/UI/core/progress-bar/src/terra-dev-site/doc/example/ProgressBarDefault';
import { ProgressBarSize } from '../../components/UI/core/progress-bar/src/terra-dev-site/doc/example/ProgressBarSize';
import { TransparentSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/TransparentSectionHeader';
import { AccordionSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/AccordionSectionHeader';
import { DefaultSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/DefaultSectionHeader';
import { ClosedSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/ClosedSectionHeader';
import { LongTitleAccordionSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/LongTitleAccordionSectionHeader';
import { OnClickSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/OnClickSectionHeader';
import { LongTitleSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/LongTitleSectionHeader';
import { OpenSectionHeader } from '../../components/UI/core/section-header/src/terra-dev-site/doc/example/OpenSectionHeader';
import { DefaultToggle } from '../../components/UI/core/toggle/src/terra-dev-site/doc/example/DefaultToggle';
import { AnimatedToggle } from '../../components/UI/core/toggle/src/terra-dev-site/doc/example/AnimatedToggle';
import { CustomButtonTextShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/CustomButtonTextShowHide';
import { InitiallyOpenShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/InitiallyOpenShowHide';
import { NoPreviewShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/NoPreviewShowHide';
import { ButtonAlignCenterShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/ButtonAlignCenterShowHide';
import { ButtonAlignRightShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/ButtonAlignRightShowHide';
import { DefaultShowHide } from '../../components/UI/core/show-hide/src/terra-dev-site/doc/example/DefaultShowHide';
import { SpacerExample } from '../../components/UI/core/spacer/src/terra-dev-site/doc/example/SpacerExample';
import { SpacerShortVsLongHandExample } from '../../components/UI/core/spacer/src/terra-dev-site/doc/example/SpacerShortVsLongHandExample';
import { SpacerResponsiveExample } from '../../components/UI/core/spacer/src/terra-dev-site/doc/example/SpacerResponsiveExample';
import { StatusDefault } from '../../components/UI/core/status/src/terra-dev-site/doc/example/StatusDefault';
import { StatusArrange } from '../../components/UI/core/status/src/terra-dev-site/doc/example/StatusArrange';
import { StatusIcon } from '../../components/UI/core/status/src/terra-dev-site/doc/example/StatusIcon';
import { StatusImage } from '../../components/UI/core/status/src/terra-dev-site/doc/example/StatusImage';
import { ToggleAlignmentAndGlyph } from '../../components/UI/core/status-view/src/terra-dev-site/doc/example/ToggleAlignmentAndGlyph';
import { CustomStatusView } from '../../components/UI/core/status-view/src/terra-dev-site/doc/example/Custom';
import { ToggleVariantsStatusView } from '../../components/UI/core/status-view/src/terra-dev-site/doc/example/ToggleVariants';
import { DefaultToolbar } from '../../components/UI/core/toolbar/src/terra-dev-site/doc/example/DefaultToolbar';
import { DefaultVisuallyHiddenText } from '../../components/UI/core/visually-hidden-text/src/terra-dev-site/doc/example/DefaultVisuallyHiddenText';
import { RefCallbackVisuallyHiddenText } from '../../components/UI/core/visually-hidden-text/src/terra-dev-site/doc/example/RefCallbackVisuallyHiddenText';
import { TagFallbacks } from '../../components/UI/core/tag/src/terra-dev-site/doc/example/TagFallbacks';
import { TagHref } from '../../components/UI/core/tag/src/terra-dev-site/doc/example/HrefTag';
import { TagDefault } from '../../components/UI/core/tag/src/terra-dev-site/doc/example/TagDefault';
import { TextColors } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextColors';
import { TextDisplays } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextDisplays';
import { TextWordWrapped } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextWordWrapped';
import { TextVariations } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextVariations';
import { TextFontSizes } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextFontSizes';
import { TextWeights } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextWeights';
import { TextVisuallyHidden } from '../../components/UI/core/text/src/terra-dev-site/doc/example/TextVisuallyHidden';
import { ButtonGroupWithIcons } from '../../components/UI/core/button-group/src/terra-dev-site/doc/example/ButtonGroupWithIcons';
import { ButtonGroupSingleSelect } from '../../components/UI/core/button-group/src/terra-dev-site/doc/example/ButtonGroupSingleSelect';
import { ButtonGroupMultiSelect } from '../../components/UI/core/button-group/src/terra-dev-site/doc/example/ButtonGroupMultiSelect';
import { ButtonGroupDisabledButtons } from '../../components/UI/core/button-group/src/terra-dev-site/doc/example/ButtonGroupDisabledButtons';
import { ButtonGroupIsBlock } from '../../components/UI/core/button-group/src/terra-dev-site/doc/example/ButtonGroupIsBlock';
import { MixinsExample } from '../../components/UI/core/breakpoints/src/terra-dev-site/doc/example/MixinsExample';
import { ActiveBreakpointProviderExample } from '../../components/UI/core/breakpoints/src/terra-dev-site/doc/example/ActiveBreakpointProviderExample';
import { PlaceholderDefault } from '../../components/UI/core/doc-template/src/terra-dev-site/doc/example/PlaceholderDefault';
import { PlaceholderLight } from '../../components/UI/core/doc-template/src/terra-dev-site/doc/example/PlaceholderLight';
import { BadgeExamples } from '../../components/UI/core/doc-template/src/terra-dev-site/doc/example/BadgeExamples';
// commented because crashing
// import { Markdown } from '../../components/UI/core/markdown/src/Markdown';
// import { MarkdownExample } from '../../components/UI/core/markdown/src/terra-dev-site/doc/markdown/Markdown.doc';
import { BreakpointExample } from '../../components/UI/core/responsive-element/src/terra-dev-site/doc/example/BreakpointExample';
import { ResizeExample } from '../../components/UI/core/responsive-element/src/terra-dev-site/doc/example/ResizeExample';
import { ResizeUncontrolledExample } from '../../components/UI/core/responsive-element/src/terra-dev-site/doc/example/UncontrolledBreakpointExample';
import { AlertExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/AlertExample';
import { AlertErrorExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/ErrorExample';
import { AlertWarningExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/WarningExample';
import { AlertUnverifiedExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/UnverifiedExample';
import { AlertCustomExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/CustomExample';
import { AlertLongTextExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/LongTextExample';
import { AlertUnsatisfiedExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/UnsatisfiedExample';
import { AlertActionAndDismissibleExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/ActionAndDismissibleExample';
import { AlertInfoExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/InfoExample';
import { AlertAdvisoryExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/AdvisoryExample';
import { AlertSuccessExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/SuccessExample';
import { AlertDismissibleExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/DismissibleExample';
import { AlertActionExample } from '../../components/UI/core/alert/src/terra-dev-site/doc/example/ActionExample';
import { AvatarUser } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarUser';
import { AvatarColorVariants } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarColorVariants';
import { AvatarOneInitial } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarOneInitial';
import { AvatarTwoInitials } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarTwoInitials';
import { AvatarIsDeceased } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarIsDeceased';
import { AvatarImage } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarImage';
import { AvatarSize } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/avatar/AvatarSize';
import { FacilityExample } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/facility/Facility';
import { FacilityImage } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/facility/FacilityImage';
import { FacilitySize } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/facility/FacilitySize';
import { FacilityColorVariants } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/facility/FacilityColorVariants';
import { GenericAvatarVariants } from '../../components/UI/core/avatar/src/terra-dev-site/doc/example/generic/GenericAvatarVariants';
import { DemographicsBanner } from '../../components/UI/core/demographics-banner/src/DemographicsBanner';
import { DemographicsBannerDeceased } from '../../components/UI/core/demographics-banner/src/terra-dev-site/doc/example/DemographicsBannerDeceased';
import { DemographicsBannerAdditionalAges } from '../../components/UI/core/demographics-banner/src/terra-dev-site/doc/example/DemographicsBannerAdditionalAges';
import { DemographicsBannerSmallContainer } from '../../components/UI/core/demographics-banner/src/terra-dev-site/doc/example/DemographicsBannerSmallContainer';
import { DemographicsBannerAdditionalDetails } from '../../components/UI/core/demographics-banner/src/terra-dev-site/doc/example/DemographicsBannerAdditionalDetails';
import { DemographicsBannerBasic } from '../../components/UI/core/demographics-banner/src/terra-dev-site/doc/example/DemographicsBannerBasic';
import { DialogDefault } from '../../components/UI/core/dialog/src/terra-dev-site/doc/example/DialogDefault';
import { DialogNoClose } from '../../components/UI/core/dialog/src/terra-dev-site/doc/example/DialogNoClose';
import { DialogLongContent } from '../../components/UI/core/dialog/src/terra-dev-site/doc/example/DialogLongContent';
import { OpenCloseTextToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/OpenCloseTextToggleButton';
import { OpenCloseEventToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/OpenCloseEventToggleButton';
import { IsIconOnlyToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/IsIconOnlyToggleButton';
import { DefaultToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/DefaultToggleButton';
import { AnimatedToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/AnimatedToggleButton';
import { ButtonAttrsToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/ButtonAttrsToggleButton';
import { IsInitiallyOpenToggleButton } from '../../components/UI/core/toggle-button/src/terra-dev-site/doc/example/IsInitiallyOpenToggleButton';
import { DefaultActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/DefaultActionHeader';
import { MaximizeCloseActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/MaximizeCloseActionHeader';
import { BackCloseActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/BackCloseActionHeader';
import { CloseActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/CloseActionHeader';
import { LongWrappingTextHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/LongWrappingTextHeader';
import { BackPreviousNextCloseActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/BackPreviousNextCloseActionHeader';
import { MinimizeCustomButtonActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/MinimizeCustomButtonActionHeader';
import { BackActionHeader } from '../../components/UI/core/action-header/src/terra-dev-site/doc/example/BackActionHeader';
import { ListExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/List';
import { ListDividedExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListDivided';
import { ListPaddedExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListPadded';
import { ListItemExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListItem';
import { ListSectionExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListSection';
import { ListSubsectionExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListSubsection';
import { ListSubsectionHeaderExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListSubsectionHeader';
import { FieldExamples } from '../../components/UI/core/form-field/src/terra-dev-site/doc/example/FieldExamples';
import { BlankExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/common/BlankExample';
import { NumberInputExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/common/NumberInputExample';
import { ControlledDefaultInvalidExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/controlled/DefaultInvalidExample';
import { ControlledDisabledExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/controlled/DisabledExample';
import { ControlledDisabledInvalidExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/controlled/DisabledInvalidExample';
import { ControlledDefaultExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/controlled/DefaultExample';
import { ControlledDefaultIncompleteExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/controlled/DefaultIncompleteExample';
import { UncontrolledDefaultExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/DefaultExample';
import { UncontrolledDisabledInvalidExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/DisabledInvalidExample';
import { UncontrolledDefaultInvalidExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/DefaultInvalidExample';
import { UncontrolledDisabledExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/DisabledExample';
import { UncontrolledDefaultIncompleteExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/DefaultIncompleteExample';
import { UncontrolledInvalidWithIncompleteExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/InvalidWithIncompleteExample';
import { UncontrolledFileExample } from '../../components/UI/core/form-input/src/terra-dev-site/doc/example/uncontrolled/FileExample';
import { DefaultHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/DefaultHyperlink';
import { DisabledHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/DisabledHyperlink';
import { ExternalHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/ExternalHyperlink';
import { AudioHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/AudioHyperlink';
import { VideoHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/VideoHyperlink';
import { ImageHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/ImageHyperlink';
import { DocumentHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/DocumentHyperlink';
import { UnderlineHiddenHyperlink } from '../../components/UI/core/hyperlink/src/terra-dev-site/doc/example/UnderlineHiddenHyperlink';
import { OneColumn } from '../../components/UI/core/dynamic-grid/src/terra-dev-site/doc/example/OneColumn';
import { TwoColumn } from '../../components/UI/core/dynamic-grid/src/terra-dev-site/doc/example/TwoColumn';
import { ULayout } from '../../components/UI/core/dynamic-grid/src/terra-dev-site/doc/example/ULayout';
import { ResponsiveGrid } from '../../components/UI/core/dynamic-grid/src/terra-dev-site/doc/example/ResponsiveGrid';
import { DashboardLayout } from '../../components/UI/core/dynamic-grid/src/terra-dev-site/doc/example/Dashboard';
import { OverlayExample } from '../../components/UI/core/overlay/src/terra-dev-site/doc/example/OverlayExample';
import { LoadingOverlayExample } from '../../components/UI/core/overlay/src/terra-dev-site/doc/example/LoadingOverlayExample';
import { DefaultHtmlTable } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/DefaultHtmlTable';
import { NoStripedHtmlTable } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/NoStripedHtmlTable';
import { StandardPaddingHtmlTable } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/StandardPaddingHtmlTable';
import { HtmlTableWithLongContent } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/HtmlTableWithLongContent';
import { HtmlTableWithCustomCells } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/HtmlTableWithCustomCells';
import { CompactPaddingHtmlTable } from '../../components/UI/core/html-table/src/terra-dev-site/doc/example/CompactPaddingHtmlTable';
import { DefaultSwitch } from '../../components/UI/core/switch/src/terra-dev-site/doc/example/DefaultSwitch';
import { SwitchWithOnChange } from '../../components/UI/core/switch/src/terra-dev-site/doc/example/SwitchWithOnChange';
import { DisabledSwitch } from '../../components/UI/core/switch/src/terra-dev-site/doc/example/DisabledSwitch';
import { DefaultToggleSectionHeader } from '../../components/UI/core/toggle-section-header/src/terra-dev-site/doc/example/DefaultToggleSectionHeader';
import { AnimatedToggleSectionHeader } from '../../components/UI/core/toggle-section-header/src/terra-dev-site/doc/example/AnimatedToggleSectionHeader';
import { TransparentToggleSectionHeader } from '../../components/UI/core/toggle-section-header/src/terra-dev-site/doc/example/TransparentToggleSectionHeader';
import { PaginatorExample } from '../../components/UI/core/paginator/src/terra-dev-site/doc/example/PaginatorExample';
import { PaginatorNoPagesExample } from '../../components/UI/core/paginator/src/terra-dev-site/doc/example/PaginatorNoPagesExample';
import { ProgressivePaginatorExample } from '../../components/UI/core/paginator/src/terra-dev-site/doc/example/ProgressivePaginatorExample';
import { DefaultTableExample } from '../../components/UI/core/table/src/terra-dev-site/doc/example/DefaultTable';
import { StandardPaddingTable } from '../../components/UI/core/table/src/terra-dev-site/doc/example/StandardPaddingTable';
import { CompactPaddingTable } from '../../components/UI/core/table/src/terra-dev-site/doc/example/CompactPaddingTable';
import { StripedTable } from '../../components/UI/core/table/src/terra-dev-site/doc/example/StripedTable';
import { CustomHeaderFooterNodeTable } from '../../components/UI/core/table/src/terra-dev-site/doc/example/CustomHeaderFooterNodeTable';
import { ScrollingTable } from '../../components/UI/core/table/src/terra-dev-site/doc/example/ScrollingTable';
import { HeaderRowExample } from '../../components/UI/core/table/src/terra-dev-site/doc/example/HeaderRowExample';
import { HeaderCheckMarkCellExample } from '../../components/UI/core/table/src/terra-dev-site/doc/example/HeaderCheckMarkCellExample';
import { TableSectionExample } from '../../components/UI/core/table/src/terra-dev-site/doc/example/SectionExample';
import { RowExample } from '../../components/UI/core/table/src/terra-dev-site/doc/example/RowExample';
import { CheckMarkTableGuide } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/CheckMarkTable';
import { ConsumeIconAllStatic } from '../../components/UI/terra-consumer/packages/terra-consumer-icon/stories/IconStatic';
import { ConsumeIconAllThemeable } from '../../components/UI/terra-consumer/packages/terra-consumer-icon/stories/IconThemeable';
import { NavBurgerButton } from '../../components/UI/terra-consumer/packages/terra-consumer-nav/src/components/nav-burger-button/NavBurgerButton';
import { HelpButtonStory } from '../../components/UI/terra-consumer/packages/terra-consumer-nav/stories/HelpButton';
import { NavLogoStory } from '../../components/UI/terra-consumer/packages/terra-consumer-nav/stories/NavLogo';
import { SafeHtmlStory } from '../../components/UI/terra-consumer/packages/terra-consumer-nav/stories/SafeHtml';
import { SimpleNavStory } from '../../components/UI/terra-consumer/packages/terra-consumer-nav/stories/SimpleNav';
// import { PortalLayoutStory } from '../../components/UI/terra-consumer/packages/terra-consumer-layout/stories/PortalLayout';
import { DexLayoutStory } from '../../components/UI/terra-consumer/packages/terra-consumer-layout/stories/DexLayout';
import { PercentageWidthTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/PercentageWidthTable';
import { ScalarWidthTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/ScalarWidthTable';
import { SectionTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/SectionTable';
import { SortedTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/SortedTable';
import { StaticWidthTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/StaticWidthTable';
import { MultiSelectTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/MultiSelectTable';
import { SingleSelectTable } from '../../components/UI/core/table/src/terra-dev-site/doc/guides/SingleSelectTable';
import { FieldsetExamples } from '../../components/UI/core/form-fieldset/src/terra-dev-site/doc/example/FieldsetExamples';
import { DefaultTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/Textarea';
import { FixedSmallSizeTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/FixedSmallSize';
import { FixedMediumSizeTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/FixedMediumSize';
import { FixedLargeSizeTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/FixedLargeSize';
import { ResizableTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/Resizable';
import { FillContainerTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/FillContainer';
import { InvalidTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/Invalid';
import { IncompleteTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/Incomplete';
import { DisabledTextAreaExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/Disabled';
import { RequiredTextAreaFieldExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/RequiredTextareaField';
import { IncompleteTextAreaFieldExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/IncompleteTextareaField';
import { TextAreaFieldExample } from '../../components/UI/core/form-textarea/src/terra-dev-site/doc/example/TextareaField';
import { InlineRadiosExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/InlineRadiosExample';
import { MultipleRadiosExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/MultipleRadiosExample';
import { MobileButtonRadioExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/MobileButton';
import { DisabledRadioExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/DisabledRadioExample';
import { HiddenLabelRadioExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/HiddenLabelRadioExample';
import { LongTextRadioExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/LongTextRadioExample';
import { DefaultRadioExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/radio/DefaultRadioExample';
import { DefaultRadioFieldExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/field/DefaultRadioField';
import { ControlledRadioFieldExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/field/ControlledRadioField';
import { OptionalRadioFieldExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/field/OptionalRadioField';
import { InlinedRadioFieldExample } from '../../components/UI/core/form-radio/src/terra-dev-site/doc/example/field/InlineRadioField';
import { MultipleCheckboxesExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/MultipleCheckboxesExample';
import { DisabledCheckboxExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/DisabledCheckboxExample';
import { HiddenLabelCheckboxExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/HiddenLabelCheckboxExample';
import { InlineCheckboxesExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/InlineCheckboxesExample';
import { DefaultCheckboxExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/DefaultCheckboxExample';
import { MobileCheckboxExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/MobileCheckboxExample';
import { LongTextCheckboxExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/checkbox/LongTextCheckboxExample';
import { OptionalCheckboxFieldExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/field/OptionalCheckboxField';
import { ControlledCheckboxFieldExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/field/ControlledCheckboxField';
import { InlineCheckboxFieldExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/field/InlineCheckboxField';
import { DefaultCheckboxFieldExample } from '../../components/UI/core/form-checkbox/src/terra-dev-site/doc/example/field/DefaultCheckboxField';
import { IssueForm } from '../../homePage/IssueForm/Index';

export default { title: 'Library/Terra core' };

export const ActionFooterStory = () => {
  return (
    <Base>
      <div>
        <code>
          Terra Action Footer The terra-action-footer component is a footer bar that contains two sockets, start and end, for placing actionable items such as buttons and
          hyperlinks. If no actions are provided, the footer bar collapses to a themeable height and maintains the top border. Terra provides a Standard, Centered, and Block-style
          variations for Action Footer:
        </code>
        <ActionFooterStandardSingleStartActionExample title="Single Start Action" />
        <ActionFooterStandardSingleEndActionExample title="Single End Action" />
        <ActionFooterStandardMultipleStartEndActionsExample title="Multiple Start and End Actions" />
        <br />
        <code>
          Terra Centered Action Footer The centered action footer component is a footer bar that contains a single, centered socket for placing actionable items such as buttons and
          hyperlinks. If no actions are provided, the footer bar collapses to a themeable height and maintains the top border.
        </code>
        <ActionFooterCenteredSingleActionExample title="Single Action" />
        <ActionFooterCenteredMultipleActionsExample title="Multiple Actions" />
        <br />
        <code>
          Terra Block Action Footer The block action footer component is a footer bar that contains a single socket for placing actionable items such as buttons and hyperlinks. If
          no actions are provided, the footer bar collapses to a themeable height and maintains the top border.
        </code>
        <ActionFooterBlockSingleActionExample title="Single Action" />
        <ActionFooterBlockMultipleActionsExample title="Multiple Actions" />
      </div>
    </Base>
  );
};

export const ActionHeaderStory = () => {
  return (
    <Base>
      <div>
        The terra-action-header component is a header bar containing a title and optional actionable items such as links and buttons.
        <DefaultActionHeader />
        <BackActionHeader />
        <CloseActionHeader />
        <BackCloseActionHeader />
        <BackPreviousNextCloseActionHeader />
        <MaximizeCloseActionHeader />
        <MinimizeCustomButtonActionHeader />
        <LongWrappingTextHeader />
      </div>
    </Base>
  );
};

export const AlertStory = () => {
  return (
    <Base>
      <div>
        The Terra Alert component is a notification banner that can be rendered in your application when there is information that you want to bring to the user's attention. The
        Alert component supports a number of built-in notification types that render with pre-defined colors and icons that help the user understand the severity and meaning of the
        notification. The supported alert types and recommended uses are:
        <AlertExample title="Default Alert" />
        <AlertErrorExample title="Error Alert" />
        <AlertWarningExample title="Warning Alert" />
        <AlertUnsatisfiedExample title="Unsatisfied Alert" />
        <AlertUnverifiedExample title="Unverified Alert" />
        <AlertAdvisoryExample title="Advisory Alert" />
        <AlertInfoExample title="Information Alert" />
        <AlertSuccessExample title="Success Alert" />
        <AlertCustomExample title="Custom Alert" />
        <AlertLongTextExample title="Long Text Alert" />
        <AlertActionExample title="Alert with Action" />
        <AlertDismissibleExample title="Dismissible Alert" />
        <AlertActionAndDismissibleExample title="Dismissible Alert with Actions" />
      </div>
    </Base>
  );
};

export const ArrangeStory = () => {
  return (
    <Base>
      <div>
        The arrange component provides content containers with a fit (start and/or end region) and fill (middle region).
        <ArrangeAlignFitStart title="Align Individual Containers - FitStart" />
        <ArrangeAlignFill title="Align Individual Containers - Fill" />
        <ArrangeAlignFitEnd title="Align Individual Containers - FitEnd" />
        <ArrangeAlignAllContainers title="Align All Containers" />
      </div>
    </Base>
  );
};

export const AvatarStory = () => {
  return (
    <Base>
      <div>
        The Avatar variant represents a person - it displays an image or initials in a circular frame. If a valid image is not provided then the avatar falls back to displaying
        initials.
        <AvatarUser title="Default - Fallback Icon" />
        <AvatarOneInitial title="Initials (1)" />
        <AvatarTwoInitials title="Initials (2)" />
        <AvatarIsDeceased title="Is Deceased" />
        <AvatarImage title="Image" />
        <AvatarSize title="Override Size" />
        <AvatarColorVariants title="Toggle Color Variants - Theme Specific" />
        The Facility variant represents a location - it displays an image in a circular frame. If an image is not provided, a fallback facility icon displays.
        <FacilityExample title="Default - Fallback Icon" />
        <FacilityImage title="Image" />
        <FacilitySize title="Override Size" />
        <FacilityColorVariants title="Toggle Color Variants - Theme Specific" />
        The Generic variant represents multiple users - it displays a shared user icon in a circular frame.
        <GenericAvatarVariants title="Generic Avatar - Icons" />
      </div>
    </Base>
  );
};

export const BadgeStory = () => {
  return (
    <Base>
      <div>
        The badge component displays content classification.
        <BadgeIntent title="Intent" />
        <BadgeSize title="Size" />
        <BadgeIcon title="Icon" />
        <BadgeVisuallyHiddenText title="VisuallyHiddenText" />
      </div>
    </Base>
  );
};

export const BaseStory = () => {
  return (
    <Base>
      <h1>No visual Elements</h1>
    </Base>
  );
};

export const BreakpointsStory = () => {
  return (
    <Base>
      <div>
        terra-breakpoints contains components and utilities related to Terra's supported responsive breakpoints.
        <ActiveBreakpointProviderExample />
        <MixinsExample />
      </div>
    </Base>
  );
};

export const ButtonStory = () => {
  return (
    <Base>
      <div>
        The button component provides users a way to trigger actions in the UI. It can be modified in color and type, and can optionally display an icon. Submit a request if
        additional variants not provided are needed.
        <ButtonVariant title="Variant" />
        <ButtonBlock title="Block" />
        <ButtonCompact title="Compact" />
        <ButtonAnchor title="Anchor" />
        <ButtonDisabled title="Disabled" />
        <ButtonOnClick title="Click" />
        <ButtonIcon title="Icon" />
      </div>
    </Base>
  );
};

export const ButtonGroupStory = () => {
  return (
    <Base>
      <div>
        The Terra Button Group component is a controlled component that groups buttons and allows consumers to manage the selection state of each button in the group. By default,
        the buttons in the Button Group do not remain selected when clicked. Therefore, consumers must manage the selection state of the buttons because the Button Group does not
        keep track of the selection state. A controlled Button Group gives the consumer the flexibility to compose a single-select button group as well as a multi-select button
        group. This component is intended to group buttons with similar context or to toggle content, it is not intended to be used as a form element.
        <ButtonGroupWithIcons title="Text and Icon - Default, no selected state" />
        <ButtonGroupSingleSelect title="Single Select - Manage single button selection" />
        <ButtonGroupMultiSelect title="Multi Select - Manage multiple button selections" />
        <ButtonGroupDisabledButtons title="Disabled Buttons - Default, no selected state" />
        <ButtonGroupIsBlock title="ButtonGroup Is Block - width stretches to fill container" />
      </div>
    </Base>
  );
};

export const CardStory = () => {
  return (
    <Base>
      <div>
        Card is a basic container with some base styling to help separate elements with different content
        <CardDefault title="Card basic example" />
        <CardRaised title="Card basic raised example" />
        <br />
        <CardPadding title="Card plus Card Body with default padding" />
        <CardPaddingVertical title="Card plus Card Body with only vertical padding" />
        <CardPaddingHorizontal title="Card plus Card Body with only horizontal padding" />
        <CardPaddingHR title="Card plus padded and non-padded children" />
        <CardContentCentered title="Centered content inside card" />
        <CardVisuallyHiddenText title="Card with Visually Hidden Text" />
      </div>
    </Base>
  );
};

export const CellGridStory = () => {
  return (
    <Base>
      <div>
        The terra-cell-grid is a structural row based layout for aligning columns with 100% of its parents width.
        <CellGridExample title="Default" /> <br />
        <CellGridVertical title="Divided Vertically" /> <br />
        <CellGridHorizontal title="Divided Horizontally" /> <br />
        <CellGridBoth title="Divided Both Directions" /> <br />
      </div>
    </Base>
  );
};

export const ContentContainerStory = () => {
  return (
    <Base>
      <div>
        The Terra ContentContainer is a structural component for the purpose of arranging content with an optional header and/or footer. The footer is not responsive to mobile
        keyboard positioning.
        <ContentContainerStandard title="Standard Container" />
        <ContentContainerFill title="Fill Container" />
        Waiting for solid to update.....................
        <ContentContainerScrollRef title="Scroll Ref Container" />
      </div>
    </Base>
  );
};

export const DemographicsBannerStory = () => {
  return (
    <Base>
      <div>
        The demographics component is used to display demographic information about a person in a condensed, easy to read format. Demographics Banner must be inside the Base
        component with locale. All related translations are required to be loaded before Demographics Banner renders.
        <DemographicsBannerBasic title="Basic Demographics Banner" />
        <DemographicsBanner title="Demographics Banner with Missing Data" />
        <DemographicsBannerDeceased title="Deceased Demographics Banners" />
        <DemographicsBannerAdditionalAges title="Demographics Banner with Gestational and Post Menstrural Ages" />
        <DemographicsBannerAdditionalDetails title="Demographics Banner with Additional Details" />
        <DemographicsBannerSmallContainer title="Demographics Banner in a Small Container" />
      </div>
    </Base>
  );
};

export const DialogStory = () => {
  return (
    <Base>
      <div>
        Dialogs are temporary views that can be used in a myriad of ways. Dialogs have the ability to alert users to act on critical information. In doing so, Dialogs may allow
        users to avoid destructive decisions, and also extend user workflows without disorienting the user.
        <DialogDefault title="Dialog - Default" />
        {/* <DialogLongContent title="Dialog - Long Content" /> */}
        {/* <DialogNoClose title="Dialog - No Close Button" /> */}
      </div>
    </Base>
  );
};

export const DividerStory = () => {
  return (
    <Base>
      <div>
        The divider component is used to visually separate content on a layout. ## Examples
        <DividerExample title="Default" />
        <DividerWithText title="Custom Text" />
        <DividerWithWrappingText title="Custom Text" />
      </div>
    </Base>
  );
};

export const DocsStory = () => {
  return (
    <Base>
      <h1>Not written on main site</h1>
    </Base>
  );
};

export const DocTemplateStory = () => {
  return (
    <Base>
      <div>
        <p>This very page was generated using the DocTemplate. The template supports more than one Props Table and example, even though those were not featured here.</p>
        <BadgeExamples />
        <PlaceholderDefault />
        <PlaceholderLight />
      </div>
    </Base>
  );
};

export const DropdownButtonStory = () => {
  return <Base>terra-dropdown-button</Base>;
};

export const DynamicGridStory = () => {
  return (
    <Base>
      The DynamicGrid component provides users a way to dynamically configure a CSS Grid using configuration. The component supports defining custom regions across multiple
      responsive breakpoints.
      <OneColumn />
      <TwoColumn />
      <ULayout />
      <DashboardLayout />
      <ResponsiveGrid />
    </Base>
  );
};

export const FormCheckboxStory = () => {
  return (
    <div>
      The Terra Form Checkbox is a responsive input component rendered as a box next to label text. When activated, a check mark shall appear. Focus can be activated through
      tabbing and the checked state can be toggled with the space bar.
      <DefaultCheckboxExample title="Default Checkbox" />
      <DisabledCheckboxExample title="Disabled Checkbox" />
      <HiddenLabelCheckboxExample title="Hidden Label Checkbox" />
      <LongTextCheckboxExample title="Long Text Checkbox" />
      <InlineCheckboxesExample title="Multiple Inline Checkboxes" />
      <MultipleCheckboxesExample title="Multiple Checkboxes - first defaults to checked" />
      <MobileCheckboxExample title="[Theme Specific] Resize checkboxes on non-desktop touch device" />
      Form Checkbox Field CheckboxField is a component of terra-form-checkbox that provides a container for rendering related checkboxes. CheckboxField include displays for help
      text, error text, required or optional indication, and invalid states.
      <DefaultCheckboxFieldExample title="Default CheckboxField Example" />
      <ControlledCheckboxFieldExample title="Optional CheckboxField Example" />
      <InlineCheckboxFieldExample title="Controlled CheckboxField Example" />
      <OptionalCheckboxFieldExample title="Inline CheckboxField Example" />
    </div>
  );
};

export const FormFieldStory = () => {
  return (
    <div>
      Terra Form Field The Form Field component handles the layout of the label, help text and error text associated with a form element. Additionally, it provides required,
      optional, and invalid indicators.
      <FieldExamples />
    </div>
  );
};

export const FormFieldsetStory = () => {
  return (
    <div>
      Generic form fieldset component which handles the layout of a standard form fieldset including help text, legend, value and widget placement.
      <FieldsetExamples />
    </div>
  );
};

export const FormInputStory = () => {
  return (
    <Base>
      <div>
        Terra Form Input
        <BlankExample title="Default Input" />
        <NumberInputExample title="Numeric Input" />
        <ControlledDefaultExample title="Controlled Example - Valid" />
        <ControlledDisabledExample title="Controlled Example - Valid Disabled" />
        <ControlledDefaultInvalidExample title="Controlled Example - Invalid" />
        <ControlledDisabledInvalidExample title="Controlled Example - Invalid Disabled" />
        <ControlledDefaultIncompleteExample
          title="Controlled Example - Incomplete"
          description="Applies theme-specific styling for incomplete, this controlled example shows a simple version of incomplete styling being removed after value is added. ***Note: Only use incomplete if given specific guidance, reserved for specific applications when no value has been provided. Not for general use.***"
        />
        <UncontrolledDefaultExample title="Uncontrolled Example - Valid" />
        <UncontrolledDisabledExample title="Uncontrolled Example - Valid Disabled" />
        <UncontrolledDefaultInvalidExample title="Uncontrolled Example - Invalid" />
        <UncontrolledDisabledInvalidExample title="Uncontrolled Example - Invalid Disabled" />
        <UncontrolledDefaultIncompleteExample
          title="Uncontrolled Example - Incomplete"
          description="Applies theme-specific styling for incomplete. ***Note: Only use incomplete if given specific guidance, reserved for specific applications when no value has been provided. Not for general use.***"
        />
        <UncontrolledInvalidWithIncompleteExample
          title="Uncontrolled Example - Invalid And Incomplete With Invalid Taking Precedence"
          description="Applies theme-specific styling for incomplete and invalid, invalid-only styling displays taking precedence. ***Note: Only use incomplete if given specific guidance, reserved for specific applications when no value has been provided. Not for general use.***"
        />
        <UncontrolledFileExample title="Uncontrolled Example - File Input" />
      </div>
    </Base>
  );
};

export const FormRadioStory = () => {
  return (
    <div>
      The Terra Form Radio is a responsive input component rendered as a radio button next to label text. When activated, a dot shall appear. Use the name attribute to group radio
      buttons together. Tabbing switches focus between radio button groups; arrow keys switch between radio buttons of the same group. The checked state can be activated with the
      space bar.
      <DefaultRadioExample title="Default Radio" />
      <DisabledRadioExample title="Disabled Radio" />
      <HiddenLabelRadioExample title="Hidden Label Radio" />
      <LongTextRadioExample title="Long Text Radio" />
      <InlineRadiosExample title="Multiple Inline Radios" />
      <MultipleRadiosExample title="Multiple Radios - first defaults to checked" />
      <MobileButtonRadioExample title="[Theme Specific] Resize radio buttons on non-desktop touch device" />
      RadioField is a component of terra-form-radio that provides a container for rendering related radio buttons. RadioField include displays for help text, error text, required
      or optional indication, and invalid states.
      <DefaultRadioFieldExample title="Default RadioField Example" />
      <OptionalRadioFieldExample title="Optional RadioField Example" />
      <ControlledRadioFieldExample title="Controlled RadioField Example" />
      <InlinedRadioFieldExample title="Inline RadioField Example" />
    </div>
  );
};

export const FormSelectStory = () => {
  return <Base>terra-form-select</Base>;
};

export const FormTextareaStory = () => {
  return (
    <div>
      terra-form-textarea
      <DefaultTextAreaExample />
      <FixedSmallSizeTextAreaExample />
      <FixedMediumSizeTextAreaExample />
      <FixedLargeSizeTextAreaExample />
      <ResizableTextAreaExample />
      <FillContainerTextAreaExample />
      <InvalidTextAreaExample />
      <IncompleteTextAreaExample description="Applies theme-specific styling for incomplete, this example shows simple version of incomplete styling being removed after value is added. ***Note: Only use incomplete if given specific guidance, reserved for specific applications when no value has been provided. Not for general use.***" />
      <DisabledTextAreaExample />
      <b>Terra Form Textarea Field</b>
      <TextAreaFieldExample />
      <RequiredTextAreaFieldExample />
      <IncompleteTextAreaFieldExample />
    </div>
  );
};

export const GridStory = () => {
  return (
    <Base>
      <div>
        The grid component provides a flexbox based grid system.
        <GridDefault title="Default Grid" />
        <GridResponsive title="Responsive Grid" />
        <GridNested title="Nested Grid" />
      </div>
    </Base>
  );
};

export const HeadingStory = () => {
  return (
    <Base>
      <div>
        <HeadingLevels />

        <HeadingSizes />
        <HeadingColors description="By passing in a className, authors can manually set the text color of any '<Heading />' component." />

        <HeadingWeights />

        <HeadingVisuallyHidden />
        <HeadingVariations description="You can mix and match props on the '<Heading />' component to create various heading styles." />
      </div>
    </Base>
  );
};

export const htmlTableStory = () => {
  return (
    <Base>
      The Terra HTML Table is a component for creating read-only, non-responsive, standard semantic tables. If an interactive table is required, please refer to the terra-table.
      <DefaultHtmlTable />
      <NoStripedHtmlTable title="Table without zebra stripes" />
      <CompactPaddingHtmlTable title="Table with compact padding" />
      <StandardPaddingHtmlTable title="Table with standard padding" />
      <HtmlTableWithLongContent title="Table with long content" />
      <HtmlTableWithCustomCells title="Table with custom cells" />
    </Base>
  );
};

export const HyperlinkStory = () => {
  return (
    <Base>
      The terra hyperlink component allows linking to other web pages, files, locations within the same page, email addresses, or any other URL.
      <DefaultHyperlink />
      <DisabledHyperlink />
      <ExternalHyperlink description="An external variant adds a custom icon to indicate the destination is external to the source." />
      <AudioHyperlink description="An audio variant adds a custom icon to indicate the content linked is audio." />
      <VideoHyperlink description="A video variant adds a custom icon to indicate the content linked is a video." />
      <ImageHyperlink description="An image variant adds a custom icon to indicate the content linked is an image." />
      <DocumentHyperlink description="A document variant adds a custom icon to indicate the content linked is document (.pdf, .doc, .xls, etc.)." />
      <UnderlineHiddenHyperlink description="Option to hide the default underline when applied with theme-specific styling. Reserve for use when terra-hyperlink is applied in special situations within lists, other controls, or other components." />
    </Base>
  );
};

export const I18nStory = () => {
  return <Base>terra-i18n</Base>;
};

export const IconStory = () => {
  return (
    <Base>
      <div>
        <div>
          <IconAllStatic />
        </div>
        <Heading level={4}>Themeable Icons</Heading>
        <div>
          <IconAllThemeable />
        </div>
      </div>
    </Base>
  );
};

export const ImageStory = () => {
  return (
    <Base>
      <div>
        The terra-image component provides styling for visual imagery.
        <ImageDefault />
        <ImageFitTypes />
      </div>
    </Base>
  );
};

export const ListStory = () => {
  return (
    <Base>
      The Terra List is a structural component to vertically arrange content within list/list items. If a list implementation contains selectable list options the role prop should
      be set to "listbox" for accesibility. Two padding variants are provide for list item content, standard and compact. If different padding is desired use the defaulted style of
      'none' and set the padding on the list item's child content with your own css values, preferrably themeable variables.
      <ListExample title="Default List" />
      <ListDividedExample title="Divided List" />
      <ListPaddedExample title="Padded List" />
      <h3>List Item:</h3>
      The list item provides a pattern for selection/selectability for li surrounded child content. Whenever, the list item has a prop `isSelectable=true` then `aria-label` must be
      provided to List for accessibility. The onSelect event is the primary means for interaction, as it yields the associated metaData when valid selection occurs, though
      individual onClick and onKeyDown events can be applied through the use of customProps if greater granularity is required.
      <ListItemExample />
      <h3> List Section</h3>
      <ListSectionExample />
      <h3>List Subsection</h3>
      <ListSubsectionExample />
      <h3>List Subsection Header</h3>
      <ListSubsectionHeaderExample />
    </Base>
  );
};

// commented because crashing
// export const MarkdownStory = () => {
//   return (
//     <Base>
//       <div>
//         <MarkdownExample />
//       </div>
//     </Base>
//   );
// };

export const MixinsStory = () => {
  return (
    <Base>
      <h1>No Visual Elements</h1>
    </Base>
  );
};

export const OverlayStory = () => {
  return (
    <Base>
      The Overlay component is a component that creates an semi-transparent overlay screen that blocks interactions with the elements underneath the display. There are two types of
      overlays: fullscreen and relative to its container.
      <OverlayExample title="Overlay" />
      <LoadingOverlayExample title="Loading Overlay" />
    </Base>
  );
};

export const PaginatorStory = () => {
  return (
    <Base>
      Standard paginator to be used for data sets of known and unknown size. Provides first, last, previous, next, and paged functionality.
      <PaginatorExample />
      <PaginatorNoPagesExample title="Paginator Example Without Pages" />
      <ProgressivePaginatorExample />
    </Base>
  );
};

export const ProfileImageStory = () => {
  return (
    <Base>
      <div>
        The terra-profile-image component displays an avatar image while the profile image is being loaded. If the profile image successfully loads, the avatar image is replaced
        with the profile image. Otherwise, the avatar image is left as is. All styling set on the profile image will be applied to the avatar image.
        <ProfileImageDefault />
        <ProfileImageFitTypes />
      </div>
    </Base>
  );
};

export const ProgressBarStory = () => {
  return (
    <Base>
      <div>
        The progress bar component provides users a way to display the progress of process or activity in a graphical manner. It can be modified in height and fill color. Its
        styling is set such that, the ProgressBar element will occupy full width of its parent element and will flex based on the width of the parent container. Note: The progress
        bar displays fill with respect to percentage (value between 0 and 100). If max isn't specified in the input to the component, then value corresponds to a percentage value.
        <ProgressBarDefault title="Default" />
        <ProgressBarSize title="Size" />
        <ProgressBarColor title="Color" description="By passing in a colorClass, authors can manually set the bar color of any `<ProgressBar />` component." />
        <ProgressBarGradient
          title="Foreground and Background Color"
          description="By passing in a colorClass, authors can manually set the bar color of any `<ProgressBar />` component."
        />
      </div>
    </Base>
  );
};

export const PropsTableStory = () => {
  return <Base />;
};

export const ResponsiveElementStory = () => {
  return (
    <Base>
      <div>
        The Responsive Element provides a way to conditionally render components during breakpoint or resize changes. The Responsive Element can be set to be responsive to the
        parent of the component or the window. By default, event listeners will be bound to the immediate parent of the component and invoke the onChange and onResize callback
        functions as a result of resize changes to the bound target.
        <BreakpointExample title="Responsive Breakpoints" description="An example demonstrating the callback events during a breakpoint change." />
        <ResizeExample title="Responsive Resizing" description="An example demonstrating the callback events during a resize." />
        <ResizeUncontrolledExample />
      </div>
    </Base>
  );
};

export const ScrollStory = () => {
  return (
    <Base>
      <div>
        The terra-scroll is a content view that hides data accessible with scrolling and provides a ref. The expectation is that any consumer needing to provide a scrollable area
        within their implementation should be using this component, or one of our other terra components that implements Scroll. Scroll provides a menthod to access the node
        managing scrolling, so values such as scrollTop can be easily set, as well as the correct prefixes to consistently sytle scrolling across supported browsers. The
        terra-scroll expands to fill it's parent container, so parent nodes are expected to have an explicit height set.
        <ScrollVertical title="Vertical Scroll" />
        <ScrollHorizontal title="Horizontal Scroll" />
      </div>
    </Base>
  );
};

export const SearchFieldStory = () => {
  return <Base>terra-search-field</Base>;
};

export const SectionHeaderStory = () => {
  return (
    <Base>
      <div>
        Section Header presentational component that provides an onClick callback.
        <DefaultSectionHeader />
        <LongTitleSectionHeader />
        <LongTitleAccordionSectionHeader />
        <ClosedSectionHeader />
        <OpenSectionHeader />
        <OnClickSectionHeader />
        <AccordionSectionHeader />
        <TransparentSectionHeader />
      </div>
    </Base>
  );
};

export const ShowHideStory = () => {
  return (
    <Base>
      <div>
        Show Hide Component that will show a preview of content and then expand it with a Show More button.
        <DefaultShowHide title="Default ShowHide" />
        <CustomButtonTextShowHide title="Custom Button Text ShowHide" />
        <InitiallyOpenShowHide title="Initially Open ShowHide" />
        <ButtonAlignCenterShowHide title="Button Align Center ShowHide" />
        <ButtonAlignRightShowHide title="Button Align Right ShowHide" />
        <NoPreviewShowHide title="No Preview ShowHide" />
      </div>
    </Base>
  );
};

export const SignatureStory = () => {
  return <Base>terra-signature</Base>;
};

export const SpacerStory = () => {
  return (
    <Base>
      <div>
        This component is used to provide margin and/or padding space between two components based on the given values.
        <SpacerExample title="Spacer" description="Spacing default button with a padding value of large+4 and primary button with padding value of large+2" />
        <SpacerResponsiveExample />
        <SpacerShortVsLongHandExample title="Spacer Short Vs Long Hand" />
      </div>
    </Base>
  );
};

export const StatusStory = () => {
  return (
    <Base>
      <div>
        The status component provides a customizable color indictor to signify a specific condition.
        <StatusDefault title="Status with text" />
        <StatusImage title="Status with Image" />
        <StatusIcon title="Status with Icon" />
        <StatusArrange title="Status with Arrange" />
      </div>
    </Base>
  );
};

export const StatusViewStory = () => {
  return (
    <Base>
      <div>
        debugger; Presents an icon, title, message, and/or buttons based on a status type scenario. Scenarios include no-data, no-matching-results, not-authorized, error, or a
        custom scenario.
        <ToggleVariantsStatusView title="Variants" />
        <CustomStatusView title="Custom: Icon + message + buttons" />
        Fix component : handleOnGlyphChange() not working:
        <ToggleAlignmentAndGlyph />
      </div>
    </Base>
  );
};

export const SwitchStory = () => {
  return (
    <Base>
      The terra-switch component, like its real-world counterpart, is used to indicate whether a setting is ON or OFF and provide users with instantaneous feedback.
      <DefaultSwitch />
      <SwitchWithOnChange />
      <DisabledSwitch />
    </Base>
  );
};

export const TableStory = () => {
  return (
    <div>
      Terra Table is a structural component used to create data tables. Table provides means to handle row selection and hooks for sortable columns.
      <DefaultTableExample />
      <StandardPaddingTable />
      <CompactPaddingTable />
      <StripedTable
        title="Striped Table, No Padding Table"
        description="(Note: the no padding style table is intended for tables with custom cell content that includes it's own internal spacing)"
      />
      <CustomHeaderFooterNodeTable description="Additional toolbars can be placed above and/or below (e.g. pagination) any table to enhance usability:" />
      <ScrollingTable />
      <h2>Header</h2>
      The table's header is comprised of a cells and an optional selectAllColumn. The checkLabel should be provided if the table has toggle behavior.
      <HeaderRowExample />
      <br />
      <HeaderCheckMarkCellExample />
      <h2>Section</h2>
      <TableSectionExample />
      <h2>Row</h2>
      <RowExample />
      <h1>Guides:</h1> Implementing a Single Select Table
      <SingleSelectTable /> Implementing a Multi Select Table <MultiSelectTable />
      Implementing a Section Table
      <SectionTable />
      Implementing a Sorted Table //Todo fix sorting rows not working...
      <SortedTable />
      Implementing a Striped Table <StripedTable />
      Static Width Columns <StaticWidthTable />
      Percentage Width Columns <PercentageWidthTable />
      Scalar Width Columns
      <ScalarWidthTable />
      Terra Table - Implementing a CheckMark Table
      {/* Todo when select all it not toggle icon */}
      <CheckMarkTableGuide />
    </div>
  );
};

export const TagStory = () => {
  return (
    <Base>
      <div>
        The tag component is used for tagging and provides users a way to trigger actions in the UI. Each tag should have text. It can optionally display an icon along with the
        text.
        <TagDefault title="Default Tag with onClick" />
        <TagHref title="Tag with href" />
        <TagFallbacks title="Tag with no onClick or href" description="These styles are provided for when this component is missing recommended elements for best practice use." />
      </div>
    </Base>
  );
};

export const TextStory = () => {
  return (
    <Base>
      <div>
        The font size, font weight, font family, and font color in terra components are set to defaults in terra-base which is then inherited into all components. Components can
        override these base styles as needed in their specific component CSS. In some cases, you may need text that differs from the base font styles, though, the text doesn't
        belong to a specific component. In these cases, the text component may be helpful. Using terra-text, you can create text that differs from the base text styles by changing
        the font size, font weight, and color. However, there are some drawbacks to be aware of with this component. The styles set with this component are not themable and will be
        static. If this is a concern, we recommend building a custom component that handles your font styles that need to differ from the base font styles so you can control the
        themability of them.
        <TextDisplays title="Displays" />
        <TextFontSizes
          title="Font Sizes"
          description="The font-size values represent the pixel values of our UX defined font-scale. Then the `Text` component will render these font-sizes with rem units."
        />
        <TextColors title="Colors" description="By passing in a className, authors can manually set the text color of any '<Text />' component." />
        <TextWeights title="Weight" />
        The isVisuallyHidden prop can be used to hide the content visually, yet provide the content to screen readers. More info on this approach can be found here:{' '}
        <TextVisuallyHidden title="Visually Hidden" />
        <TextVariations title="Variations" description="You can mix and match props on the `<Text />` component to create various text styles." />
        <TextWordWrapped title="Word Wrapped" description="The `isWordWrapped` prop can be used to apply cross-browser word-wrapping styles." />
      </div>
    </Base>
  );
};

export const ToggleStory = () => {
  return (
    <Base>
      <div>
        Toggle component that transitions content in and out. Use aria-expanded and aria-controls state on the triggering component. The objective of aria-expanded state is to
        indicate whether regions of the content are collapsible, and to expose whether a region is currently expanded or collapsed.The aria-controls state indicates the element
        (terra-toggle) that is controlled by the triggering element. For toggle button functionality, see
        <DefaultToggle />
        <AnimatedToggle title="isAnimated Toggle" />
      </div>
    </Base>
  );
};

export const ToggleButtonStory = () => {
  return (
    <Base>
      <div>
        Toggle Button component that transitions content in and out with the click on a button.
        <h3>Bug: Animated Switch not work, because toogle component has problem</h3>
        <DefaultToggleButton title="Default ToggleButton" />
        <AnimatedToggleButton title="isAnimated ToggleButton" />
        <OpenCloseTextToggleButton title="Open / Close Button Text ToggleButton" />
        <IsIconOnlyToggleButton title="isIconOnly ToggleButton" />
        <IsInitiallyOpenToggleButton title="isInitiallyOpen ToggleButton" />
        <ButtonAttrsToggleButton title="Custom ToggleButton Button" />
        <OpenCloseEventToggleButton title="OnOpen / OnClose Callback ToggleButton" />
      </div>
    </Base>
  );
};

export const TogglesectionHeaderStory = () => {
  return (
    <Base>
      Toggle section header component that transitions content in and out with a click.
      <DefaultToggleSectionHeader title="Default" />
      <AnimatedToggleSectionHeader title="Animated" />
      <TransparentToggleSectionHeader title="Transparent" />
    </Base>
  );
};

export const ToolbarStory = () => {
  return (
    <Base>
      <div>
        Note: currently this only work if multiple childrens in toolbar:
        <br />
        <br />
        The terra-toolbar component is used to display bar containing items such as buttons, button groups, and links.
        <DefaultToolbar />
      </div>
    </Base>
  );
};

export const VisuallyHiddenTextStory = () => {
  return (
    <Base>
      <div>
        Visually Hidden Text is a component designed for screen readers that renders text on the dom, but is not visible to the UI. Some components such as badge will have a visual
        indication to note their hierarchy on the page, but that indication is lost when a screen reader is used on the web page. Visually Hidden text would allow your application
        to provide that additional context to a screen reader. Another reason for this component is when you want to produce more semantic markup for a screen reader only
        experience. Some various examples include: - You may want to provide a visually hidden header to allow for better readability. - You may need to provide different
        instructions for visual users vs non visual users. - div nodes have a difficult understanding the aria-label attributes as well. In these instances, it's recommended to use
        visually hidden text.
        <DefaultVisuallyHiddenText title="VisuallyHiddenText with empty string" />
        <RefCallbackVisuallyHiddenText title="VisuallyHiddenText with ref Callback" />
      </div>
    </Base>
  );
};
export const ConsumeExample = () => {
  return (
    <>
      <ConsumeIconAllStatic />
      <ConsumeIconAllThemeable />
    </>
  );
};
export const BurgerMenu = () => <NavBurgerButton />;

// commented because crashing
// export const HelpButton = () => (
//   <Router integration={pathIntegration()}>
//     <HelpButtonStory />
//   </Router>
// );
export const SafeHtmlTest = () => <SafeHtmlStory />;
export const NavLogo = () => <NavLogoStory />;
// Note: On resize Pop up menu sometimes close but it should not close.
// export const SimpleNav = () => <SimpleNavStory />;
// export const PortalLayoutStoryExample = () => <PortalLayoutStory />;
// export const DexLayoutStoryExample = () => <DexLayoutStory />;
export const IssueFormStoryExample = () => <IssueForm />;
