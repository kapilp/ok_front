import { AccountLayout } from '../../layout/account_layout';
import { AdminIndex } from '../../views/admin_index';
import { AdminLayout } from '../../layout/admin_layout';
// import Confirm from '../../src/views/confirm'
import { Contact } from '../../views/contact';
import { GlobalSetting } from '../../views/globalsetting';
import { Home } from '../../views/home';
// import Logout from '../../src/views/logout'
import { MyPage } from '../../views/my_page';
import { OrgIndex } from '../../views/org_index';
// import OrgLayout from '../../src/views/org_layout'
import { ProjectIndex } from '../../views/project_index';
// import {ProjectLayout} from '../../src/views/project_layout'

export default { title: 'View' };

export const AdminIndex_ = () => <AdminIndex />;
export const Contact_ = () => <Contact />;
export const GlobalSetting_ = () => <GlobalSetting />;
export const Home_ = () => <Home />;
export const MyPage_ = () => <MyPage />;
export const OrgIndex_ = () => <OrgIndex />;
// export const OrgLayout_ = () => <OrgLayout />
export const ProjectIndex_ = () => <ProjectIndex />;
// export const ProjectLayout_ = () => <ProjectLayout />

export const AccountLayout_ = () => <AccountLayout />;
export const AdminLayout_ = () => (
  <div>
    <AdminLayout />
  </div>
);
// export const Confirm_ = () => <Confirm />
// export const Logout_ = () => <Logout />
