// import { showDebug } from '../../components/UI/debug';
// showDebug.set(true)
import classNames from "classnames/bind";

import { JSX, Component, createEffect, createMemo, createSignal, For, onCleanup, mergeProps, Show, splitProps } from "solid-js";
import { createStore } from "solid-js/store";
import { spread, Dynamic } from "solid-js/web";
// import Table from '../../components/UI/Table'
import { css } from "goober";
import isEqual from "fast-deep-equal";
// import { Link } from '@rturnq/solid-router';
import { DeleteModal } from "../../components/UI/DeleteModal";
import { DropDownMenu } from "../../components/UI/DropDownButton";
import { Html, HtmlBase, HtmlBaseDiv } from "../../components/UI/Html";
// https://jdenticon.com/js-get-started/typescript.html
// import JDentiIcon from '../../components/UI/Jdenticon' // Cannot find module '../transformers/0'
// https://github.com/takoyaro/svavatar
// import {Svavatar} from 'svavatar' // Error: Can't resolve 'svavatar'
// import { Setting } from '../../components/UI/Setting';
// import { Skeleton } from '../../components/UI/Skeleton';
// import { Button, ButtonVariants } from '../../components/UI/core/button/src/Button';
// import SortableList from '../../components/UI/SortableList'
import { Time } from "../../components/UI/Time";
// import Model from '../../components/UI/Model'
import { NightMode } from "../../components/UI/NightMode";
import { RecentNotification } from "../../components/UI/RecentNotification";

// import Tippy from '../../components/UI/Tippy'
// import Title from '../../components/UI/Title'

import { SvgIconError } from "../../components/UI/core/icon/src/icon/IconError";

import { FocusTrap } from "../../components/UI/FocusTrap";
import { useOnclickOutside } from "../../components/UI/CoolOnclickoutside/CoolOnclickoutside";
import { useWebAnimations } from "../../components/use-web-animations/src/useWebAnimations";
import styles from "./webanim.module.scss";
import { Setting } from "../../components/UI/Setting";
import { Skeleton } from "../../components/UI/Skeleton";
import { Button } from "../../components/UI/core/button/src/Button";
import { I18ReactExample } from "../../components/react-i18next/example/react/src/App";
import { OneFileI18Sample } from "../../components/react-i18next/example/react/src/OneFileSample";
import { stackHook } from "../../hooks/stackHook";
import { FlexboxCenter } from "../../components/UI/FlexboxCenter";
import { SolidHeroIcons } from "./icons/SolidHeroicons";
import { OpticsTestExample } from "./library/Optics";
import { isFunction } from "../../utils/enums";

import { Breadcrumbs } from "../../components/UI/react-breadcrumbs/src/breadcrumbs";
import { Breadcrumb } from "../../components/UI/react-breadcrumbs/src/breadcrumb";

export default { title: "Library/UI" };

export const Html_ = () => <Html html={["signup"]} />;
// export const JDentiIcon_ =<JDentiIcon, props: {html: ["signup"]} })
// export const Svavatar_ = () => { const props = { type: "human", mood: "happy", seed: "tako", };  return { Component: Svavatar, props }}
// export const Model_ =<Model/>
export const NightMode_ = () => <NightMode />;
export const RecentNotification_ = () => <RecentNotification />;

export const Skeleton_ = () => <Skeleton />;
/* export const SortableList_ = () => {
    const props = {
    }
    return { Component: SortableList, props, on: {click: action('clicked')}, }} */
export const Time_ = () => <Time />;
/* export const Tippy_ =<Tippy/>
export const Title_ =<Title, props: { currentRoute: { params: {  schemaKey: 'note'     }       }    } }) */

export const Ticker = () => {
  const [state, setState] = createStore({ count: 0 });
  const t = setInterval(() => setState({ count: state.count + 1 }), 1000);

  // remove interval when Component destroyed:
  onCleanup(() => {
    clearInterval(t);
  });

  return <div>{state.count}</div>;
};

export const ObjectTest = () => {
  const [state, setState] = createStore({ options: {} });
  const t = setInterval(() => setState({ options: { add: { pos: "t" } } }), 1500);
  onCleanup(() => {
    clearInterval(t);
  });
  return <div>{state.options?.add?.pos ?? "f"}</div>;
};

function SvgEditSquare(props: JSX.SVGAttributes<SVGSVGElement>) {
  return (
    <svg width="1em" height="1em" viewBox="0 0 24 24" {...props}>
      <path d="M20 11.5a.5.5 0 111 0v7a2.5 2.5 0 01-2.5 2.5H5.486a2.5 2.5 0 01-2.5-2.5v-13a2.5 2.5 0 012.5-2.5H12.5a.5.5 0 110 1H5.486a1.5 1.5 0 00-1.5 1.5v13a1.5 1.5 0 001.5 1.5H18.5a1.5 1.5 0 001.5-1.5v-7zm-1.146-8.354l2 2a.5.5 0 010 .708l-8 8A.5.5 0 0112.5 14h-2a.5.5 0 01-.5-.5v-2a.5.5 0 01.146-.354l8-8a.5.5 0 01.708 0zM18.5 4.207l-7.5 7.5V13h1.293l7.5-7.5L18.5 4.207z" />
    </svg>
  );
}
export const SvgExample = () => (
  <div>
    <SvgEditSquare />
  </div>
);
export const ResetFilterButton = () => {
  const [state, setState] = createStore({ filterSettingsNew: {} });
  const onResetFilter = () => setState({ filterSettingsNew: {} }); // this works
  // const onResetFilter = () => setState('filterSettingsNew', (l) => { delete l['a']; }); // This not works!
  const addFilter = () => setState({ filterSettingsNew: { a: 1 } });
  return (
    <div>
      <Show when={!!Object.keys(state.filterSettingsNew).length}>
        <button onClick={onResetFilter}>Reset Filters </button>
      </Show>
      <button onClick={addFilter}>Add Filters </button>
    </div>
  );
};

export const Resizing = () => {
  const [state, setState] = createStore({ resizingIndex: -1, rows: [1, 2, 3, 4] });
  const onChangeIndex = () => setState({ resizingIndex: -1 }); // this works
  const addFilter = () => setState({ resizingIndex: 2 });
  return (
    <div>
      <For each={state.rows}>
        {(h, i) => (
          <>
            <div class={`${state.resizingIndex === i() ? "resizing" : ""} resize-line`} />
            <button onClick={onChangeIndex}>Reset Filters </button>
            <button onClick={addFilter}>Add Filters </button>
          </>
        )}
      </For>
    </div>
  );
};

function List(props: { items: []; onItemClick: (i: number) => (e: MouseEvent) => void }) {
  return (
    <div>
      {props.items.map((x, i) => (
        <div onClick={props.onItemClick(i)}>
          <a href="javascript:void(0)">{x}</a>
        </div>
      ))}
    </div>
  );
}

function List2(props: { items: []; index: number }) {
  return props.items.map((n, index) => <Item key={index} index={index} number={n} />);
}

/*

const history = createHistory();

const Content = label => () => <p>{`Content ${label}`}</p>;

const shouldOpenModal = locationPath => /\bmodal\b/.test(locationPath);

const ReactRouterModal = props => (
  <Modal
    isOpen={shouldOpenModal(props.location.pathname)}
    onRequestClose={() => history.push("/basic")}>
    <div>
      <Link to="/basic/modal/a">Link A</Link><br />
      <Link to="/basic/modal/b">Link B</Link>
      <div>
        <Switch location={props.location}>
          <Route path="/basic/modal/a" component={Content('A')} />
          <Route path="/basic/modal/b" component={Content('B')} />
        </Switch>
      </div>
    </div>
  </Modal>
);

function App() {
  return (
    <Router history={history}>
      <div>
        <Link to="/basic/modal" class="btn btn-primary">Modal</Link>
        <Route path="/basic/modal" component={ReactRouterModal} />
      </div>
    </Router>
  );
} */
export const Focus = () => {
  const [state, setState] = createStore({ visible: false });
  const toogleState = () => setState({ visible: !state.visible });
  let input;
  const setRef = (el) => {
    input = el;
    el.focus();
  };
  return (
    <div>
      <button onClick={toogleState}>Toogle</button>
      <Show when={state.visible}>
        <input ref={setRef} value="hello" />
        <button onClick={() => input.focus()}>Focus</button>
      </Show>
    </div>
  );
};
export const SettingsTest = () => {
  return (
    <div>
      <Setting />
    </div>
  );
};

/* import { customElement } from 'solid-element';
customElement('my-todo', { someProp: 'one', otherProp: 'two' }, (props, { element }) => {
  return <div>Hello From Webcomponents</div>;
});

export const WebComponentExample = () => (
  <div>
    <my-todo></my-todo>
  </div>
); */

export const DropDownMenuExample = () => {
  return <DropDownMenu />;
};
export const WebComp1 = () => {
  const [state, setState] = createStore({ visible: false });
  return (
    <pfe-modal>
      <button slot="pfe-modal--trigger">Launch modal</button>
      <h3 slot="pfe-modal--header">Nulla amet reprehenderit enim incididunt duis officia enim est sunt</h3>
      &lt;p&gt;Id magna nostrud commodo aliqua. Fugiat culpa fugiat aliqua aute proident. Velit reprehenderit incididunt nostrud est eiusmod consequat voluptate sunt anim.
      Incididunt elit irure labore aliqua quis dolor excepteur occaecat officia. Non cillum sit amet commodo incididunt deserunt adipisicing ad deserunt et consectetur
      sunt.&lt;/p&gt;
      <button onClick={(_) => setState({ visible: !state.visible })}>ToogleVisible</button>
      {state.visible ? "Hello" : "None"}
      <pfe-cta>
        <a href="#">Learn more</a>
      </pfe-cta>
    </pfe-modal>
  );
};
export const WebCompPfeModalInput = () => {
  const [state, setState] = createStore({ visible: false });
  return (
    <pfe-modal>
      <button slot="pfe-modal--trigger">Save Profile</button>
      <input value="name" />
    </pfe-modal>
  );
};
export const WebComp2 = () => {
  return (
    <pfe-card>
      <h2 slot="pfe-card--header">Default card</h2>
      <p>
        This is the default pfe-card and <a href="#">a link</a>.
      </p>
      <p>
        Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the
        overall value proposition.
      </p>
      <p>Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
      <div slot="pfe-card--footer">
        <pfe-cta pfe-priority="secondary">
          <a href="#">Learn more</a>
        </pfe-cta>
      </div>
    </pfe-card>
  );
};

export const DeleteModalExample = () => {
  return (
    <div>
      <DeleteModal />
    </div>
  );
};
export const ClassNamesTest = () => {
  const [state, setState] = createStore({ show: true });
  const Toogle = () => setState({ show: !state.show });
  return (
    <div class={classNames({ "foo-bar": state.show })}>
      hello<button onClick={Toogle}>Toogle1</button>
    </div>
  );
};

const ChildSpiltTest = (props: { one?: true }) => {
  const [value, remainingProps] = splitProps(props, ["value"]);

  const D = "input";
  return (
    <div>
      See Console
      <Dynamic component={D} {...value} class="a" />
    </div>
  );
};
export const SpiltTest = () => {
  return <ChildSpiltTest one={false} value="hello1s" />;
};

export const DynamicElementTest = () => {
  return (
    <div>
      <Dynamic component="a">Hello</Dynamic>
    </div>
  );
};

const Def1 = (props: {}) => {
  props = mergeProps({}, { data: [], value: [], disabled: false }, props);
  console.log(props);
  return <div>See console</div>;
};
export const Def2 = () => {
  return <Def1 value={undefined} />;
};
const Def4 = (props: { children: HTMLElement[] }) => {
  console.log(props);
  // props.children.forEach(x => (x.classList = []));
  // props.children.forEach(x => console.log(x));
  return <div>{props.children}</div>;
};
export const Defa5 = () => {
  return (
    <Def4>
      <h1 class="a">hello</h1>
      <h2 class="b">world</h2>
    </Def4>
  );
};
export const DynamicTest = () => {
  const H1 = `h1`;
  return (
    <Def4>
      <Dynamic component="h3" class="a">
        hello
      </Dynamic>
    </Def4>
  );
};
const ArrayTestInternal = (props) => {
  const textContent = <>{props.text ? <span aria-hidden="true">{props.text}</span> : null}</>;
  const TextContent2 = <span aria-hidden="true">456</span>;
  const content = [textContent, TextContent2, null, undefined, props.children];
  return (
    <div>
      <Dynamic component="span">{content}</Dynamic>
    </div>
  );
};
export const ArrayTest = () => {
  const [state, setState] = createStore({ text: "first" });
  const alterText = () => setState({ text: "second" });
  const setNull = () => setState({ text: null });
  return (
    <div>
      <ArrayTestInternal text={state.text} />
      <button onClick={alterText}>Alter</button>
      <button onClick={setNull}>Set Null</button>
    </div>
  );
};
export const ConditionalPropsTest = () => {
  const [state, setState] = createStore({ show: true });
  const onToggle = () => setState({ show: !state.show });
  return (
    <div>
      {state.show ? "show" : "notshow"}
      <span data={state.show ? "b" : undefined}>Hello</span>
      <button onClick={onToggle}>Alter</button>
    </div>
  );
};
export const TerraIconError = () => {
  return (
    <div>
      <SvgIconError />
    </div>
  );
};

export const HtmlBaseTest = () => {
  const [state, setState] = createStore({ value: "<b>Hello</b>", className: "a" });
  const onToggle = () => setState({ value: "<i>Working</i>", className: "b" });
  return (
    <div>
      <HtmlBase value={state.value} />
      <HtmlBaseDiv value={state.value} class={state.className} />
      <button onClick={onToggle}>Alter</button>
    </div>
  );
};
export const HtmlComponentTest = () => {
  const [state, setState] = createStore({ value: "<b>Hello</b>" });
  const onToggle = () => setState({ value: "<i>Working</i>" });
  return (
    <div>
      <Html html={["signup", "signup2"]} />
      <button onClick={onToggle}>Alter</button>
    </div>
  );
};
export const FocusTrapTest = () => {
  return (
    <div>
      <FocusTrap>
        <div id="modal-dialog" class="modal">
          <button>Ok</button>
          <button>Cancel</button>
        </div>
      </FocusTrap>
    </div>
  );
};
export const TwoObjectStyleExpandExample = () => {
  const [state, setState] = createStore({ one: { color: "red" }, two: { "background-color": "black" } });
  const onToggle = () => setState({ one: { color: "blue" }, two: { "background-color": "pink" } });
  return (
    <div>
      <span style={{ ...state.one, ...state.two }}>Hello</span>
      <button onClick={onToggle}>Alter</button>
    </div>
  );
};
// https://github.com/wellyshen/react-cool-onclickoutside
export const CoolClickOutside = () => {
  const [state, setState] = createStore({ openMenu: false });

  const handleBtnClick = (): void => {
    setState({ openMenu: !state.openMenu });
  };

  const closeMenu = (): void => {
    setState({ openMenu: false });
  };

  const ref = useOnclickOutside(() => {
    closeMenu();
  });

  return (
    <div class="App">
      <h1 class="title">React Cool Onclickoutside</h1>
      <p class="subtitle">React hook to listen for clicks outside of the component(s).</p>
      <div class="dropdown" ref={ref}>
        <button class="dropdown-btn" onClick={handleBtnClick} type="button">
          Dropdown button
        </button>
        {state.openMenu && (
          <div class="dropdown-menu" onClick={closeMenu}>
            <a class="dropdown-item" href="#">
              Action 1
            </a>
            <a class="dropdown-item" href="#">
              Action 2
            </a>
            <a class="dropdown-item" href="#">
              Action 3
            </a>
          </div>
        )}
      </div>
    </div>
  );
};
// https://github.com/wellyshen/use-web-animations
export const CoolWebAnimationStory = () => {
  const { refFn, animationState } = useWebAnimations({
    keyframes: {
      transform: "translateX(500px)", // Move by 500px
      background: ["red", "blue", "green"], // Go through three colors
    },
    timing: {
      delay: 500, // Start with a 500ms delay
      duration: 1000, // Run for 1000ms
      iterations: 2, // Repeat once
      direction: "alternate", // Run the animation forwards and then backwards
      easing: "ease-in-out", // Use a fancy timing function
    },
    onReady: ({ playState, animate, animation }) => {
      // Triggered when the animation is ready to play (Google Chrome: available in v84+)
    },
    onUpdate: ({ playState, animate, animation }) => {
      // Triggered when the animation enters the running state or changes state
    },
    onFinish: ({ playState, animate, animation }) => {
      // Triggered when the animation enters the finished state (Google Chrome: available in v84+)
    },
    // More useful options...
  });

  return (
    <div class="container">
      <p>🍿 Animation is {animationState.playState}</p>
      <div class="target" ref={refFn} />
    </div>
  );
};

export const WebAnimationCodeSandbox = (props) => {
  const { refFn, animationState } = useWebAnimations<HTMLDivElement>({
    keyframes: { transform: ["translateX(0)", "translateX(270px)"] },
    timing: {
      duration: 250,
      fill: "forwards",
      easing: "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
    },
  });

  const replay = () => {
    animationState && animationState.animation && animationState.animation.play();
  };

  return (
    <div class={styles.App}>
      <h1 class={styles.title}>useWebAnimations</h1>
      <p class={styles.subtitle}>React hook for highly-performant and manipulable animations using Web Animations API.</p>
      <div class={styles.frame} onClick={replay}>
        <div class={styles.target} ref={refFn} />
        <div class={`${styles.target} ${styles["target-base"]}`} />
      </div>
    </div>
  );
};
export const FunctionalComponentTestNotWork = () => {
  const [state, setState] = createStore({ logo: "" });
  // Not update because first statement state.logo is not tracked by calling component.
  // its this component responsibility to track it.
  // and when its executed its not tracked by returned value,
  // but if state.logo has initial value it will work because <div>{state.logo}</div> monitors state.logo.
  // so dont return conditionally jsx but write condition on jsx
  const LogoRenderElement = () => state.logo && <div>{state.logo}</div>;
  const Toggle = () => setState({ logo: state.logo ? "" : "new Text" });

  return (
    <div>
      <LogoRenderElement /> <button onClick={Toggle}>Toogle</button>
    </div>
  );
};
export const FunctionalComponentTest0 = () => {
  const [state, setState] = createStore({ logo: "" });
  const LogoRenderElement = () => state.logo && <div>{state.logo}</div>;
  const Toggle = () => setState({ logo: state.logo ? "" : "new Text" });
  return (
    <div>
      <LogoRenderElement /> <Button text="Toggle" onClick={Toggle} />
    </div>
  );
};

export const FunctionalComponentTest = () => {
  const [state, setState] = createStore({ logo: "" });
  const LogoRenderElement = () => state.logo && <div>{state.logo}</div>;
  const Toggle = () => setState({ logo: state.logo ? "" : "new Text" });
  return (
    <div>
      <LogoRenderElement /> <Button text="Toggle" onClick={Toggle} />
    </div>
  );
};
export const Null = () => (
  <div>
    {null}
    {undefined}
  </div>
);
type PushFn = (item: JSX.Element) => void;
type PopFn = () => void;
type onRequestClose = () => void;
type setTitle = () => string;
// StackChildNormal
const StackChild = (p: { children: (props: { pop: PopFn; push: PushFn; onRequestClose: onRequestClose; setTitle: setTitle }) => JSX.Element }) => {
  const [state, setState] = createStore({ stack: [() => p.children({ pop, push, onRequestClose, setTitle })] });
  const setTitle = () => "";
  const onRequestClose = () => {};
  const pop = () => {
    if (state.stack.length > 1) {
      setState((prevState) => {
        prevState.stack.pop();
      });
    }
  };
  const push = (item: JSX.Element) => {
    setState((prevState) => {
      prevState.stack.push(item);
    });
  };
  const slides = createMemo(() => state.stack.map((item, index) => <h2> {item} </h2>));
  return (
    <>
      {slides}
      <button
        onClick={() =>
          push(
            <ul>
              <li>Hello</li>
            </ul>
          )
        }
      >
        Push
      </button>
      <button onClick={pop}>Pop</button>
    </>
  );
};
export const Stack = () => {
  return (
    <StackChild>
      {(props) => (
        <>
          <ul>
            <li
              onClick={() =>
                props.push(
                  <ul>
                    <li>Bar</li>
                  </ul>
                )
              }
            >
              Hello
            </li>
            <li onClick={props.pop}>Foo</li>
          </ul>
        </>
      )}
    </StackChild>
  );
};
export const StackHook = () => {
  const { stackState, pop, push, onRequestClose, setTitle, setActiveIndex, onBack, onNext } = stackHook();
  const slides = createMemo(() => stackState.stack.map((item, index) => <h2> {item} </h2>));
  return (
    <>
      {slides}
      index: {stackState.activeIndex}
      <button disabled={stackState.isBackDisabled} onClick={onBack}>
        Back
      </button>
      <button disabled={stackState.isNextDisabled} onClick={onNext}>
        Next
      </button>
      <button
        onClick={() =>
          push(
            <ul>
              <li>Hello</li>
            </ul>
          )
        }
      >
        Push
      </button>
      <button onClick={pop}>Pop</button>
    </>
  );
};
export const MultipleCreateMemo = () => {
  const [state, setState] = createStore({ logo: "" });
  // let element = createMemo(() => (state.logo ? 'Hi' : 'Bi')); or:
  let element = <>{state.logo ? "Hii" : "Bii"}</>;
  element = createMemo(() => <h1>{element}</h1>);
  const Toggle = () => setState({ logo: state.logo ? "" : "new Text" });
  return (
    <div>
      {element}
      {state.logo}
      <Button text="Toggle" onClick={Toggle} />
    </div>
  );
};
const SafeHtml1: Component<{ text: string } & JSX.HTMLAttributes<HTMLSpanElement>> = (props) => {
  const [customProps, otherProps] = splitProps(props, ["text"]);
  const node = () => {
    const el = (<span innerHTML={customProps.text} />) as Element;
    spread(el, otherProps);
    return el;
  };
  return node();
};
const SafeHtml2: Component<{ text: string; tag?: string } & JSX.HTMLAttributes<HTMLSpanElement>> = (props) => {
  const [customProps, otherProps] = splitProps(props, ["text", "tag"]);
  return () => <Dynamic component={props.tag || "span"} innerHTML={customProps.text} {...otherProps} />;
};

export const SafeHtml23 = () => {
  const [text, setText] = createSignal("<strong>Hello world</strong>");
  const changeText = () => setText("<em>I am in italic now!</em>");

  const [style, setStyle] = createSignal({ color: "red" });
  const changeStyle = () => setStyle({ color: "green" });

  const [tag, setTag] = createSignal("p");
  const changeTag = () => setTag("h1");

  return (
    <>
      <button onClick={changeText}>Change the text</button>
      <button onClick={changeStyle}>Change the color</button>
      <button onClick={changeTag}>Change the tag</button>
      <SafeHtml2 text={text()} tag={tag()} style={style()} />
    </>
  );
};
// const conditionTest = ()=>{
//   return p.children && p.children.length > 0 ? (<h2>Hi</h2> : <h3>By</h3>
// }
export const ButtonArgsExample = (p) => {
  return <button>{p.label}</button>;
};
ButtonArgsExample.args = {
  label: "Button",
};
export const I8ReactEReactExampleStory = () => {
  return <I18ReactExample />;
};
export const I18OneFile = () => <OneFileI18Sample />;

const DemoRepositionDetector = () => {
  console.log("remounted");
  return <h1>Mounted</h1>;
};
export const RepositionTestExample = () => {
  const [show, setShow] = createSignal(true);
  const useSame = <DemoRepositionDetector />;
  const changeShow = () => setShow(!show());
  return (
    <>
      <Show when={show()}>{useSame}</Show>
      <button onClick={changeShow}>Toggle</button>
    </>
  );
};

// difference between createMemo that return jsx and jsx?
export const RepositionTestExampleMemo = () => {
  const [show, setShow] = createSignal(true);

  const useSame = createMemo(() => <DemoRepositionDetector />);
  const changeShow = () => setShow(!show());
  return (
    <>
      <Show when={show()}>{useSame()}</Show>
      <button onClick={changeShow}>Toggle</button>
    </>
  );
};
export const RepositionTestExampleMemo2 = () => {
  const [show, setShow] = createSignal(true);

  const notSame = createMemo(() => {
    console.log(show(), "new jsx");
    return <DemoRepositionDetector />;
  });
  const changeShow = () => setShow(!show());
  return (
    <>
      <Show when={show()}>{notSame()}</Show>
      <button onClick={changeShow}>Toggle</button>
    </>
  );
};
export const RepositionTestExampleCondition = () => {
  const [show, setShow] = createSignal(true);

  const changeShow = () => setShow(!show());
  return (
    <>
      <Show when={show()}>
        <DemoRepositionDetector />
      </Show>
      <button onClick={changeShow}>Toggle</button>
    </>
  );
};
const boxClassName = css`
  width: 10vw;
  height: 10vh;
  background: #ff7043;
`;
const centerClassName = css`
  width: 50vw;
  height: 50vh;
  background: #bcaaa4;
`;
export const FlexboxCenterExample = () => (
  <FlexboxCenter class={centerClassName}>
    <div class={boxClassName} />
  </FlexboxCenter>
);
export const HeroIconsExample = () => <SolidHeroIcons />;
export const OpticsExample = () => <OpticsTestExample />;

export const FunctionDefaultValueExample = () => {
  const [state, setState] = createStore({ values: { a: { b: 2 } } });
  const validate = (values = state.values) => {
    console.log("dont work this:", values);
  };
  validate();
  const onClick = () => {
    setState({ values: { b: { c: 3 } } });
    validate();
  };
  return (
    <div>
      <button onClick={onClick}>Change State</button>
    </div>
  );
};
export const ReturnFunctionTest = () => {
  const [state, setState] = createStore({ errors: {} });
  let i = 0;
  const timer = setInterval(() => {
    setState("errors", "name", ++i);
  }, 1000);

  onCleanup(() => {
    clearInterval(timer);
  });

  const error = () => state.errors.name;
  return <>{error}2</>;
};
export const PromiseTest = () => {
  async function abc() {
    return "hi";
  }
  new Promise(async (resolve) => {
    // const v = resolve(abc()); // v is undefined, but resolves perfect
    // abc().then(v => resolve(v)); // this works
    const v = await abc();
    resolve(v); // this works too
  }).then((r) => console.log("r is: ", r));
};
// this test
// class A{constructor(){this.a=100} df(){console.log(this.a)}}
// (new A).df.apply({a:500})
// class A{constructor(){this.a=100, this.df = ()=>console.log(this.a)}} // not work
export const ObjectPropertiesTest = () => {
  form.$("itineraryItems[0].hotel").update({
    name: "The Plaza",
    starRating: "4.0",
  });
  form.$("itineraryItems[1].hotel").update({
    name: "Beverly Hills Hotel",
    starRating: "5.0",
  });
  class D {
    hooks() {
      form.$("itineraryItems[0].hotel").update({
        name: "The Plaza",
        starRating: "4.0",
      });
      form.$("itineraryItems[1].hotel").update({
        name: "Beverly Hills Hotel",
        starRating: "5.0",
      });
      return {
        onInit(form) {
          form.update({
            itineraryItems: [
              {
                hotel: {
                  name: "Shangri-La Hotel",
                  starRating: "5.0",
                },
              },
              {
                hotel: null,
              },
              {
                hotel: {
                  name: "Trump Hotel",
                  starRating: "5.0",
                },
              },
            ],
          });
          form.$("itineraryItems[0].hotel").update({
            name: "The Plaza",
            starRating: "4.0",
          });
          form.$("itineraryItems[1].hotel").update({
            name: "Beverly Hills Hotel",
            starRating: "5.0",
          });
        },
      };
    }
  }
  const d = new D();
  d.hooks().onInit();

  return <div>hi</div>;
};
export interface Menu {
  url: string;
  text: string;
  subItems?: Menu[];
  hidden?: boolean;
}

const TreeSidebar = (props: { menu: Menu[]; className?: string; isHorizontal?: boolean }) => {
  props = mergeProps({}, { isHorizontal: true }, props);
  return (
    <ul>
      <For each={props.menu ?? []}>
        {(m) => (
          <>
            <Show when={!m.hidden}>
              <li>
                <a href={m.path}>{m.name}</a>
              </li>
            </Show>

            {m.children && <TreeSidebar menu={m.children} />}
          </>
        )}
      </For>
    </ul>
  );
};

const guest_menu = [
  {
    name: "Register",
    path: "/super_register",
  },
  {
    name: "Login",
    path: "/super_login",
  },
];
const subdomain_guest_menu = [
  {
    name: "Register",
    path: "/register",
  },
  {
    name: "Login",
    path: "/login",
  },
];
export const TreeSideBarExample = <TreeSidebar class="right" menu={subdomain_guest_menu} />;
export const TreeSidebarExample2 = () => {
  const menu = [
    {
      icon: "mif-meter",
      name: "Overview",
      path: "/organization/:org",
    },
    {
      icon: "mif-info",
      name: "Projects",
      path: "/organization/:org/projects",
    },
    {
      children: [
        {
          name: "Settings",
          path: "/organization/:org/members_settings",
        },
        {
          name: "Members",
          path: "/organization/:org/members",
        },
        {
          name: "Groups",
          path: "/organization/:org/groups",
        },
        {
          name: "Roles",
          path: "/organization/:org/roles",
        },
        {
          name: "Permissions",
          path: "/organization/:org/permissions",
        },
        {
          name: "Avatars",
          path: "/organization/:org/avatars",
        },
      ],
      icon: "mif-meter",
      name: "Members & Permissions",
      path: "/organization/:org/members",
    },
    {
      children: [
        {
          name: "Settings",
          path: "/organization/:org/work_package_settings",
        },
        {
          name: "Types",
          path: "/organization/:org/types",
        },
        {
          name: "Status",
          path: "/organization/:org/status",
        },
        {
          name: "Workflow",
          path: "/organization/:org/custom_fields",
        },
      ],
      icon: "mif-assignment",
      name: "Work packages",
      path: "/organization/:org/work_package_settings",
    },
    {
      name: "Custom actions",
      path: "/organization/:org/custom_actions",
    },
    {
      name: "Attribute Help Text",
      path: "/organization/:org/attribute_help_texts",
    },
    {
      icon: "mif-battery-empty",
      name: "Custom fields",
      path: "/organization/:org/custom_fields",
    },
    {
      icon: "mif-hammer",
      name: "System settings",
      path: "/organization/:org/settings",
    },
    {
      icon: "mif-pencil",
      name: "Design",
      path: "/organization/:org/design",
    },
    {
      icon: "icon5 icon-custom-fields",
      name: "Overview",
      path: "/organization/:org/custom_styles",
    },
    {
      icon: "mif-pencil",
      name: "Colors",
      path: "/organization/:org/colors",
    },
    {
      icon: "mif-balance-scale",
      name: "Budgets",
      path: "/organization/:org/budgets",
    },
    {
      icon: "mif-backlog",
      name: "Backlogs",
      path: "/organization/:org/backlogs",
    },
    {
      icon: "mif-info",
      name: "Activities",
      path: "/organization/:org/activities",
    },
    {
      icon: "mif-info",
      name: "Document Categories",
      path: "/organization/:org/document_categories",
    },
    {
      icon: "mif-info",
      name: "Wiki",
      path: "/organization/:org/wiki",
    },
    {
      icon: "mif-info",
      name: "Forum",
      path: "/organization/:org/forum",
    },
    {
      icon: "mif-info",
      name: "Post",
      path: "/organization/:org/posts",
    },
  ];
  return <TreeSidebar menu={menu} />;
};
