import { Table } from '../../components/table/Table';

export default { title: 'Library/Table' };

export const TableDemo = () => <Table />;

export const MenuTable_ = () => {
  const props = {
    modelComponent: false,
    quickEditComponent: false,
    schemaKey: 'super_menu',
    pass: [],
    query: {},
    requiredFilter: [],
    fetchConfig: { type: 4 },
    syncQueryParams: true,
    refresh: () => 0,
    isdraggable: false,
    border: true,
    resize: true,
    depth: 0,
  };
  return (
    <div>
      <Table {...props} />
    </div>
  );
};
export const OrgTable_ = () => {
  const props = {
    modelComponent: false,
    quickEditComponent: true,
    schemaKey: 'organization',
    pass: [],
    query: {},
    requiredFilter: [],
    fetchConfig: { type: 4 },
    syncQueryParams: true,
    refresh: () => 0,
    isdraggable: false,
    border: true,
    resize: true,
    depth: 0,
  };
  return (
    <div>
      <Table {...props} />
    </div>
  );
};
export const NewsTable_ = () => {
  const props = {
    modelComponent: false,
    quickEditComponent: true,
    schemaKey: 'news',
    pass: [],
    query: {},
    requiredFilter: [],
    fetchConfig: { type: 4 },
    syncQueryParams: true,
    refresh: () => 0,
    isdraggable: false,
    border: true,
    resize: true,
    depth: 0,
  };
  return (
    <div>
      <Table {...props} />
    </div>
  );
};

export const NestedComponent = p => {
  return (
    <div>
      {p.children}
      <NestedComponent>123</NestedComponent>
    </div>
  );
};
