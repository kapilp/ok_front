import { Base } from '../../components/UI/core/base/src/Base';
import { Markdown } from '../../components/UI/core/markdown/src/Markdown';
import { ThemeContextProviderExample } from '../../components/UI/framework/theme-context/src/terra-dev-site/doc/example/ThemeContextProviderExample';
import { DefaultThemeProvider } from '../../components/UI/framework/theme-provider/src/terra-dev-site/doc/example/DefaultThemeProvider';
import { DefaultSlidePanel } from '../../components/UI/framework/slide-panel/src/terra-dev-site/doc/example/DefaultSlidePanel';
import { SlidePanelStart } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelStart.test';
import { SlidePanelEnd } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelEnd.test';
import { SlidePanelOverlay } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelOverlay.test';
import { SlidePanelLarge } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelLarge.test';
import { SlidePanelFill } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelFill.test';
import { SlidePanelSmall } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelSmall.test';
import { SlidePanelNoFill } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelNoFill.test';
import { SlidePanelSquish } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelSquish.test';
import { SlidePanelFullscreen } from '../../components/UI/framework/slide-panel/src/terra-dev-site/test/slide-panel/SlidePanelFullscreen.test';
import { AbstractModalAlertDialog } from '../../components/UI/framework/abstract-modal/src/terra-dev-site/doc/example/AbstractModalAlertDialog';
// Todo:
// import StandaloneAggregatorExample from '../../components/UI/framework/aggregator/src/terra-dev-site/doc/example/StandaloneAggregatorExample';
// import CustomRenderAggregatorExample from '../../components/UI/framework/aggregator/src/terra-dev-site/doc/example/CustomRenderAggregatorExample';
// import AggregatorInModalExample from '../../components/UI/framework/aggregator/src/terra-dev-site/doc/example/AggregatorInModalExample';
// import CombinedDisclosureExample from '../../components/UI/framework/aggregator/src/terra-dev-site/doc/example/CombinedDisclosureExample';
// import DisclosureBypassExample from '../../components/UI/framework/aggregator/src/terra-dev-site/doc/example/DisclosureBypassExample';
import { AbstractModalIsFullscreen } from '../../components/UI/framework/abstract-modal/src/terra-dev-site/doc/example/AbstractModalIsFullscreen';
import { AbstractModalCloseOnOutsideClick } from '../../components/UI/framework/abstract-modal/src/terra-dev-site/doc/example/AbstractModalCloseOnOutsideClick';
import { AbstractModalIsOpen } from '../../components/UI/framework/abstract-modal/src/terra-dev-site/doc/example/AbstractModalIsOpened';
import { HeaderWireframe } from '../../components/UI/framework/application-header-layout/src/terra-dev-site/doc/example/HeaderWireframe';
import { MenuWireframe } from '../../components/UI/framework/application-menu-layout/src/terra-dev-site/doc/example/MenuWireframe';
import { ApplicationHeaderNameStandard } from '../../components/UI/framework/application-name/src/terra-dev-site/doc/example/ApplicationHeaderNameStandard';
import { ApplicationMenuNameStandard } from '../../components/UI/framework/application-name/src/terra-dev-site/doc/example/ApplicationMenuNameStandard';
import { ApplicationHeaderUtilityExample } from '../../components/UI/framework/application-utility/src/terra-dev-site/doc/example/ApplicationHeaderUtilityExample';
import { ApplicationMenuUtilityExample } from '../../components/UI/framework/application-utility/src/terra-dev-site/doc/example/ApplicationMenuUtilityExample';
import { MenuUtilityMenuExample } from '../../components/UI/framework/application-utility/src/terra-dev-site/doc/example/MenuUtilityMenuExample';
import { DefaultBrandFooter } from '../../components/UI/framework/brand-footer/src/terra-dev-site/doc/example/DefaultBrandFooter';
import { VerticalBrandFooter } from '../../components/UI/framework/brand-footer/src/terra-dev-site/doc/example/VerticalBrandFooter';
import { DefaultDialogModal } from '../../components/UI/framework/dialog-modal/src/terra-dev-site/doc/example/DefaultDialogModal';
import { DialogModalWithLongText } from '../../components/UI/framework/dialog-modal/src/terra-dev-site/doc/example/DialogModalWithLongText';
import { DialogModalWithCustomHeaderAndCustomFooter } from '../../components/UI/framework/dialog-modal/src/terra-dev-site/doc/example/DialogModalWithCustomHeaderAndCustomFooter';
import { ListExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/List';
import { ListDividedExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListDivided';
import { ListPaddedExample } from '../../components/UI/core/list/src/terra-dev-site/doc/example/ListPadded';
import { BoundedHookshotExample } from '../../components/UI/framework/hookshot/src/terra-dev-site/doc/example/BoundedHookshotExample';
import { HookshotExample } from '../../components/UI/framework/hookshot/src/terra-dev-site/doc/example/HookshotExample';
import { CoordsHookshotExample } from '../../components/UI/framework/hookshot/src/terra-dev-site/doc/example/CoordsHookshotExample';
import { PopupDimensions } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupDimensions';
import { PopupBounded } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupBounded';
import { PopupStandard } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupStandard';
import { PopupWithArrow } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupWithArrow';
import { PopupAttachmentBehavior } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupAttachmentBehavior';
import { PopupAttachment } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupAttachments';
import { PopupNoHeader } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupNoHeader';
import { PopupInsideModal } from '../../components/UI/framework/popup/src/terra-dev-site/doc/example/PopupInsideModal';
import { CompleteNotificationDialog } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/CompleteNotificationDialog';
import { CompleteNotificationDialogWithLongMessage } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/CompleteNotificationDialogWithLongMessage';
import { NoVariantNotificationDialog } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/NoVariantNotificationDialog';
import { NotificationDialogWithLongText } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/NotificationDialogWithLongText';
import { NotificationDialogOnModalManager } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/NotificationDialogOnModalManager';
import { ContentNotificationDialog } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/ContentNotificationDialog';
import { RejectFirstNotificationDialog } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/RejectFirstNotificationDialog';
import { EmphasizedRejectNotificationDialog } from '../../components/UI/framework/notification-dialog/src/terra-dev-site/doc/example/EmphasizedRejectNotificationDialog';
import { BasicConsumer } from '../../components/UI/framework/embedded-content-consumer/src/terra-dev-site/doc/example/BasicConsumer';
import { DataStatusConsumer } from '../../components/UI/framework/embedded-content-consumer/src/terra-dev-site/doc/example/DataStatusConsumer';
import { CustomEventConsumer } from '../../components/UI/framework/embedded-content-consumer/src/terra-dev-site/doc/example/CustomEventConsumer';
import { CustomEventsConsumer } from '../../components/UI/framework/embedded-content-consumer/src/terra-dev-site/doc/example/CustomEventsConsumer';
import { NavigationPromptExample } from '../../components/UI/framework/navigation-prompt/src/terra-dev-site/doc/navigation-prompt/examples/NavigationPromptExample';
import { MyMenuItemExample } from '../../components/UI/framework/menu/src/terra-dev-site/doc/my/MenuItemExample';
import { BasicMenu } from '../../components/UI/framework/menu/src/terra-dev-site/doc/example/BasicMenu';
import { LayoutStandard } from '../../components/UI/framework/layout/src/terra-dev-site/doc/example/LayoutStandard';
import { LayoutMenuDisabled } from '../../components/UI/framework/layout/src/terra-dev-site/doc/example/LayoutMenuDisabled';
import { LayoutNoHeader } from '../../components/UI/framework/layout/src/terra-dev-site/doc/example/LayoutNoHeader';
import { ApplicationTabsExample } from '../../components/UI/framework/application-links/src/terra-dev-site/doc/example/ApplicationTabsExample';
import { ApplicationTabsWithIconsExample } from '../../components/UI/framework/application-links/src/terra-dev-site/doc/example/ApplicationTabsWithIconsExample';

import { ApplicationBase } from '../../components/UI/terra-application/src/application-base/ApplicationBase';

export default { title: 'Library/Terra Framework' };

export const AbstractModalStory = () => (
  <div>
    The abstract modal is a structural component that provides the ability to display portal'd content in a layer above the app. It consists of an overlay and an unstyled absolute
    positioned div in which content can be placed. The abstract modal is not intended to be consumed directly, but rather wrapped in a higher order component. Higher order
    components can provide the abstract modal with sizing, visual styles, and content (e.g; header, body, and actionable buttons). The abstract modals maximum size is constrained
    by the viewport size, so the content placed inside the modal needs to be responsive.
    <AbstractModalIsOpen title="Default Modal" />
    <AbstractModalCloseOnOutsideClick title="Disable closing the modal when clicking on the overlay" />
    <AbstractModalIsFullscreen title="Fullscreen Modal" />
    <AbstractModalAlertDialog title="Alert Dialog Modal" />
  </div>
);
export const AggregatorStory = () => (
  <div>
    <h1>TODO: Fix Examples</h1>
    The Aggregator provides focus-based mechanisms for managing actions across disparate components.
    {/* <StandaloneAggregatorExample title="Standalone Aggregator" /> */}
    {/* <CustomRenderAggregatorExample title="Custom Render Aggregator" /> */}
    {/* <CombinedDisclosureExample title="Combined Discloure Aggregator" /> */}
    {/* <DisclosureBypassExample title="Disclosure Bypass Aggregator" /> */}
    {/* <AggregatorInModalExample title="Aggregator In Modal" /> */}
  </div>
);

function NotDocumented() {
  return <h1>Not Documented on terra-ui site</h1>;
}

export const ApplicationHeaderLayoutStory = () => (
  <div>
    <NotDocumented />
    This component renders an application header layout. To be used with a terra-layout or terra-navigation-layout.
    <HeaderWireframe title="Header - Wireframe" />
  </div>
);
export const ApplicationLayoutStory = () => <div />;
export const ApplicationLinksStory = () => (
  <div>
    This packages contains a list and tab form of ApplicationLinks, to be used in horizontal and vertical display styles. Selection is managed by react-router.
    <ApplicationTabsExample title="Tabs Example" />
    <ApplicationTabsWithIconsExample title="Tabs With Icons Example" />
  </div>
);
export const ApplicationMenuLayoutStory = () => (
  <div>
    <NotDocumented />
    This component renders an application menu layout. To be used with terra-layout or terra-navigation-layout.
    <MenuWireframe />
  </div>
);
export const ApplicationNameStory = () => (
  <div>
    <NotDocumented />
    Houses the title of the application, along with a logo. There are two versions - a header version and menu version. These should be used with the corresponding header and menu
    layouts.
    <ApplicationHeaderNameStandard title="Application Header Name" />
    <ApplicationMenuNameStandard title="Application Menu Name" />
  </div>
);
export const ApplicationNavigationStory = () => <div />;
export const ApplicationUtilityStory = () => (
  <div>
    The Utility is used to disclose a utility menu. There are two versions - a header version and menu version. These should be used with the corresponding
    terra-application-header-layout and terra-application-menu-layout components.
    <ApplicationHeaderUtilityExample title="Header: Utility" />
    <ApplicationMenuUtilityExample title="Menu: Utility" />
    {/* <MenuUtilityMenuExample title="Utility Menu" /> */}
  </div>
);
export const BrandFooterStory = () => (
  <div>
    A standard footer for application layout which provides content areas to display options such as branding, copyright information, logo and navigation to related pages.
    <DefaultBrandFooter title="BrandFooter - Default" />
    <br />
    <VerticalBrandFooter title="BrandFooter - Vertical Navigation Bar" />
  </div>
);
export const CollapsibleMenuViewStory = () => <div />;
export const DateInputStory = () => <div />;
export const DatePickerStory = () => <div />;
export const DateTimePickerStory = () => <div />;
export const DialogModalStory = () => (
  <div>
    The Dialog Modal allows for disclosing accessible modals with dynamic heights. The components is placed at an 7000 z-index. The dialog modal should not be disclosed from the
    terra-modal-manager component. If you need to display another modal while using the modal manager, use the modal stacking functionality provided in terra-modal-manager or the
    terra-notification-dialog.
    <DefaultDialogModal title="Default Dialog Modal" />
    <DialogModalWithLongText title="Dialog Modal With Long Text" />
    <DialogModalWithCustomHeaderAndCustomFooter title="Dialog Modal With Custom Header and Custom Footer" />
  </div>
);
export const DisclosureManagerStory = () => (
  <div>
    <h1>No visual Elements to display</h1>
  </div>
);
export const EmbeddedContentConsumerStory = () => (
  <div>
    The Embedded Content Consumer is the managed application component which is embedding web content within an iframe. The Embedded Content Consumer is responsible for the
    communication between its embedded content to its framework, such that interaction with embedded content appears seemless.
    <BasicConsumer title="Basic Embedded Content Consumer" />
    <DataStatusConsumer title="Embedded Content Consumer Lifecycle Statuses" />
    <CustomEventConsumer title="Embedded Content Consumer that Listens for a Custom Event" />
    <CustomEventsConsumer title="Embedded Content Consumer with Seamless Communication" />
  </div>
);
export const FormValidationStory = () => <div />;
export const HookshotStory = () => (
  <div>
    The Terra Hookshot component positions content according to a targeted attachment, ensuring they stay connected.
    <HookshotExample title="Window Bound Hookshot" />
    <h3>Container Bound Hookshot</h3>
    <BoundedHookshotExample title="Container Bound Hookshot" />
    <h3>Coordinate Targeted Hookshot</h3>
    <CoordsHookshotExample title="Coordinate Targeted Hookshot" />
  </div>
);
export const InfiniteListStory = () => <div />;
export const LayoutStory = () => (
  <div>
    The Layout component provides a responsive starting point for the positioning of application components.
    <LayoutStandard title="Layout - Standard" />
    <LayoutMenuDisabled title="Layout - No Menu" />
    <LayoutNoHeader title="Layout - No Header" />
  </div>
);
export const MenuStory = () => (
  <div>
    <MyMenuItemExample /> <BasicMenu isArrowDisplayed={false} contentWidth="500px" boundingRef={undefined} />
  </div>
);
export const ModalManagerStory = () => <div />;
export const NavigationLayoutStory = () => <div />;
export const NavigationPromptStory = () => (
  <div>
    The NavigationPrompt and NavigationPromptCheckpoint components allow for the detection and management of components with unsaved state.
    <NavigationPromptExample />
  </div>
);
export const NavigationSideMenuStory = () => <div />;
export const NotificationDialogStory = () => (
  <div>
    terra-notification-dialog is a notification dialog component built over the terra-abstract-modal. It has the highest z-index of 9001. It is a common component to be used for
    confirmation/acceptance criteria style dialogs.
    <CompleteNotificationDialog title="Complete Notification Dialog" />
    <br />
    <CompleteNotificationDialogWithLongMessage title="Complete Notification Dialog With Long Message" />
    <br />
    <NoVariantNotificationDialog title="No Variant Notification Dialog" />
    <br />
    <NotificationDialogWithLongText title="Notification Dialog With Long Text" />
    <br />
    <h1>Make Model example working:</h1>
    {/* <NotificationDialogOnModalManager title="Notification Dialog On Modal Manager" /> */}
    <br />
    <ContentNotificationDialog title="Notification Dialog with Content" />
    <br />
    <RejectFirstNotificationDialog title="Notification Dialog with Reject action first" />
    <br />
    <EmphasizedRejectNotificationDialog title="Notification Dialog with Reject Action emphasized" />
  </div>
);
export const PopupStory = () => (
  <div style="margin: 100px;">
    The Terra Popup is higher order component that launches [terra-hookshot][1] positioned content with the ability to display a dynamic arrow.
    <br />
    <PopupStandard title="Popup Standard Implementation" />
    <br />
    <PopupWithArrow title="Popup Display With Arrow" />
    <br />
    <PopupAttachmentBehavior title="Popup Attachment Behavior" />
    <br />
    <PopupAttachment title="Popup Attachments" />
    <br />
    <PopupDimensions title="Popup Size Variants" />
    <br />
    <PopupBounded title="Popup Bounded" />
    <br />
    <PopupNoHeader title="Popup Bounded With No Header" />
    <br />
    {/* <PopupInsideModal title="Popup Inside Modal" /> */}
  </div>
);
export const SlideGroupStory = () => <div />;
export const SlidePanelStory = () => (
  <div>
    The Terra SlidePanel component is a progressive disclosure mechanism that allows additional content to be shown and hidden in a variety of ways.
    <DefaultSlidePanel title="SlidePanel - Controlled Demo" panelBehavior="overlay" panelPosition="end" panelSize="small" />
    <SlidePanelOverlay title="SlidePanel - Behavior - Overlay" />
    <SlidePanelSquish title="SlidePanel - Behavior - Squish" />
    <SlidePanelStart title="SlidePanel - Position - Start" />
    <SlidePanelEnd title="SlidePanel - Position - End" />
    <SlidePanelSmall title="SlidePanel - Size - Small" />
    <SlidePanelLarge title="SlidePanel - Size - Large" />
    <SlidePanelFullscreen title="SlidePanel - Fullscreen" />
    <SlidePanelFill title="SlidePanel - Fill" />
    <SlidePanelNoFill title="SlidePanel - Fill Off" />
  </div>
);
export const SlidePanelManagerStory = () => <div />;
export const TabsStory = () => <div />;
export const ThemeContextStory = () => (
  <div>
    <Markdown
      src={`# Theme Context
      A React context to provide the current theme. This package is intended to be used by Terra components only. Any terra consumers or custom Terra components should consume the context from the \`terra-application\` package.

      This context can be used to apply any theme related changes to a component.

      The most common use would be to apply a theme class to the root tag of the component to apply theme variables. See below for an example.
      `}
    />
    <ThemeContextProviderExample />
  </div>
);
export const ThemeProviderStory = () => (
  <div>
    The theme provider component provides a theme to Terra UI components via CSS custom properties a.k.a CSS variables. This is accomplished by setting a CSS class, generated from
    the themeName prop, which contains defined CSS custom properties for the specified theme on the html element.
    <DefaultThemeProvider />
  </div>
);
export const TimeOnputStory = () => <div />;

export const AppBase = () => (
  <ApplicationBase>
    <h1>Hello</h1>
  </ApplicationBase>
);
