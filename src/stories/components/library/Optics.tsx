import * as O from 'optics-ts';

export const OpticsTestExample = () => {
  type User = {
    name: string;
    age?: number | undefined;
  };

  const age = O.optic<User>().prop('age').optional();
  const userWithAge: User = {
    name: 'Betty',
    age: 42,
  };
  console.log(O.preview(age)(userWithAge));
  // ==> 42

  const userWithoutAge: User = {
    name: 'Max',
    age: undefined,
  };
  console.log(O.preview(age)(userWithoutAge));
  return <div>See Console</div>;
};
