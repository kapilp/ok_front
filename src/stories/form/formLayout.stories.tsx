import Validator from 'validatorjs';
import { FormServer } from '../../components/formServer/FormServer';
import { FieldType } from '../../utils/enums';

export default { title: 'Form/Form Layout' };
export const LoginForm1 = () => {
  const props = {
    t: [],
    b: [],
    key: null,
    schemaKey: '',
    form: ['', ''],
    fetchConfig: { type: 4, project: null },
    buttonLabels: { save: 'Log in' },
    showDebug: true,
    headerSchema: {
      fields: [
        { name: '_key', label: 'Key', type: FieldType.text, isRequired: false, isDisabled: false, description: '', props: {} },
        { name: 'from', label: 'From', type: FieldType.uuid, isRequired: false, isDisabled: false, description: '', props: { isLocal: true } },
        { name: 'to', label: 'To', type: FieldType.hidden, isRequired: false, isDisabled: false, description: '', props: {} },
        { name: 'message', label: 'Message', type: FieldType.textarea, isRequired: true, isDisabled: false, description: '', props: {} },
      ],
      formProps: { notify: false },
      r: { result: [{ _key: '', from: '', to: 'admin', message: '' }] },
    },
  };
  return (
    <div>
      <FormServer {...props} />
    </div>
  );
};
export const LoginForm2 = () => {
  const props = {
    t: [],
    b: [],
    key: null,
    schemaKey: 'login',
    form: ['', ''],
    fetchConfig: { type: 4, project: null },
    buttonLabels: { save: 'Log in' },
    showDebug: true,
    headerSchema: {
      fields: [
        { name: '_key', label: 'Key', type: FieldType.text, isRequired: false, isDisabled: false, description: '', props: {} },
        { name: 'id', label: 'ID', type: FieldType.text, isRequired: false, isDisabled: false, description: '' },
        { name: 'name', label: 'Name', type: FieldType.text, isRequired: false, isDisabled: false, description: '', props: {} },
        { name: 'message', label: 'Message', type: FieldType.textarea, isRequired: true, isDisabled: false, description: '', props: {} },
        { name: 'email', label: 'Email', type: FieldType.text, isRequired: false, isDisabled: false, description: '', props: {} },
        { name: 'phone', label: 'Phone', type: FieldType.text, isRequired: false, isDisabled: false, description: '', props: {} },
      ],
      layout: {
        _key: {},
        id: {},
        name: {},
        message: {},
        j1: {
          type: FieldType.fieldSet,
          layoutCss: {
            defaultTemplate: {
              'grid-template-columns': '1fr  ',
              'grid-template-rows': '50px',
              'grid-gap': '10px',
            },
            tiny: {
              'grid-template-columns': '1fr 1fr',
              'grid-template-rows': '50px',
              'grid-gap': '10px',
            },
          },
          props: {
            legend: 'Address',
          },
          layout: { email: {}, phone: {} },
        },
      },
      formProps: {
        layoutCss: {
          defaultTemplate: {
            'grid-template-columns': '1fr  ',
            'grid-template-rows': '50px',
            'grid-gap': '10px',
          },
          tiny: {
            'grid-template-columns': '1fr 1fr',
            'grid-template-rows': '50px',
            'grid-gap': '10px',
          },
        },
        validationC: {},
        rulesC: { slug: { makeSlug: [{}, 'name'], lowerCase: [{}, 'pname'] } },
        rules: {
          name: { id: ['slug'] },
        },
        validations: {
          name: { id: { DVR: { sync: ['required'] } } },
        },
      },

      r: { result: [{ _key: '', from: '', to: 'admin', message: '' }] },
    },
  };
  return (
    <div>
      <FormServer {...props} />
    </div>
  );
};
/*
Make terra form components working
make form layout with custom html
make form calculaions, form validations
make login, and sigin form in cernter
field should have touched property. On blur set to true

Test:
make sure member register and login works

see mobx-react-form again

Test All the Pages and form working




convert issue form to solidjs to learn form
Make a Login Signup center by making a Center component

Think how can I make Preview form with schema
First Make Native Select working

Make Issue Form like Terra ui with Schema
    Only Toggle visible status of field. So its a Calculation
Show new entry on Modal
     Use terra disclouser manager to protect on reloading etc
   on modal should be a title, on footer use submit and cancel buttons


Replace to terra-fields
make responsive form layout



Converter Task:
Fix native Select component and use it on form
Fix Date and Time Input to solid and use it on Form

 */
export const ValidationTestDVRExample = () => {
  const data = {
    name: 'John',
    email: 'johndoe@gmail.com',
    age: 28,
  };

  const rules = {
    name: 'required',
    email: 'required|email',
    age: 'min:18',
  };

  const validation = new Validator(data, rules);

  console.log(validation.passes(1)); // true
  console.log(validation.fails()); // false
  return <div>Validation Test</div>;
};
export const ValidationTestDVRExampleFail = () => {
  const validation = new Validator(
    {
      name: 'D',
      email: 'not an email address.com',
    },
    {
      name: 'size:3',
      email: 'required|email',
    },
  );

  validation.fails(); // true
  validation.passes(); // false

  // Error messages
  console.log(validation.errors.first('email')); // 'The email format is invalid.'
  console.log(validation.errors.get('email')); // returns an array of all email error messages
  return <div>Validation Test</div>;
};
