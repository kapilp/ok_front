import { Form } from '../../components/form2';
import { FixesLabels, FixesProps, FixesSubmit, FixesTypes, FixesValidation, FixesValues } from '../../components/formTests/mobxformFixes';
import { FormTestExample1 } from '../../components/formTests/example';

export default { title: 'Form/Mobx Form' };
export const TestMenu = () => {
  return <div>test</div>;
};
export const Document = () => {
  return <FormTestExample1 />;
};
export const FixesLabelsStory = () => <FixesLabels />;
export const FixesPropsStory = () => <FixesProps />;
// export const FixesSubmitStory = () => <FixesSubmit />;
export const FixesTypesStory = () => <FixesTypes />;
// export const FixesValidationStory = () => <FixesValidation />;
export const FixesValuesStory = () => <FixesValues />;
