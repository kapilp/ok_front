// import {Checkboxes} from '../../components/form/input/Checkboxes';
// import {CodeMirror} from '../../components/form/input/codemirror/CodeMirror';
// import {DropZone} from '../../components/form/input/DropZone';
// import {DateRange} from '../../components/form/input/DateRange';
// import {Emoji} from '../../components/form/input/Emoji';
// import {CLEditor} from '../../components/form/input/CLEditor';
// import {ArrayForm} from '../../components/form/input/Array';
// import {Url} from '../../components/form/input/Url';
import { For, Match, onCleanup, Show, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import { GeneralInput } from '../../components/form/GeneralInput';
import { ArrayInput } from '../../components/form/input/Array';
import { ControlledCodeMirror } from '../../components/form/input/codemirror/Code';
// // Can't resolve 'https://cdnjs.cloudflare.com/ajax/libs/jsoneditor/7.0.5/jsoneditor.min.js'
// import {JsonEditor} from '../../components/form/input/JsonEditor';
// // Can't resolve 'https://unpkg.com/flatpickr/dist/flatpickr.js'
import { DateTimePicker } from '../../components/form/input/Flatpicker';
import { Input } from '../../components/form/input/Input';
import { JsonEditor } from '../../components/form/input/JsonEditor';
// import { Sources } from 'quill';
// import { ReactQuill } from '../../components/form/input/Quill';
// import {File} from '../../components/form/input/File';
// import {Radio} from '../../components/form/input/Radio';
// import {Select} from '../../components/form/input/Select';
import { Textarea } from '../../components/form/input/Textarea';
import { SingleSelect } from '../../components/form/tableform/SingleSelect';
import { TableForm } from '../../components/form/tableform/TableForm';
import { FieldType } from '../../utils/enums';

export default { title: 'Form/Inputs' };
// export const Checkboxes_ = () => <Checkboxes />
export const Color_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="color" />
  </div>
);
export const Email_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="email" />
  </div>
);
// export const File_ = () => <File />
export const Hidden_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="hidden" />
  </div>
);
export const Number_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.valueAsNumber)} type="number" />
  </div>
);
export const Password_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="password" />
  </div>
);
export const Range_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="range" />
  </div>
);
export const Search_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="search" />
  </div>
);
export const Text_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.value)} type="text" />
  </div>
);
export const Checkbox_ = () => (
  <div>
    <Input value="test1@o-k.tech" onChange={e => console.log(e.target.checked)} type="checkbox" />
  </div>
);
// export const Radio_ = () => <Radio />
// export const Select_ = () => <Select />
export const Textarea_ = () => (
  <div>
    <Textarea value="test1@o-k.tech" onChange={e => console.log(e.target.value)} />
  </div>
);
export const JsonEditor_ = () => (
  <div>
    <JsonEditor value={{ a: 12 }} onChange={_ => 3} />
  </div>
);

// export const DropZone_ = () => <DropZone />
// export const DateRange_ = () => <DateRange />
// export const Emoji_ = () => <Emoji />
// export const CLEditor_ = () => <CLEditor />

export const ArrayForm_ = () => {
  const [state, setState] = createStore({ value: ['kapil'] });
  return (
    <div>
      <ArrayInput value={state.value} onChange={v => setState('value', v)} ar={true} />
      <button onClick={() => setState({ value: ['New Value'] })}> change</button>
      <br />
      value: {JSON.stringify(state.value)}
    </div>
  );
};
// export const Url_ = () => <Url />
export const GeneralInput_ = () => (
  <div>
    <GeneralInput type={FieldType.text} value="test1@o-k.tech" onChange={e => console.log(e.target.value)} />
  </div>
);

export const InputOnInput = () => {
  const [state, setState] = createStore({ filter: '', list: [1, 2, 3] });
  const onInput = e => {
    setState({ filter: e.target.value, list: [1, 2, 3] });
  };
  return (
    <div>
      <For each={state.list}>
        {(l, i) => (
          <Show when={i() >= 0}>
            <Switch>
              <Match when={state.list[i()]}>
                <input type="search" class={`align-${'center'} colIndex${2 + i()}`} placeholder=" &#128269;" value={state.filter || ''} onInput={onInput} />
              </Match>
            </Switch>
          </Show>
        )}
      </For>
    </div>
  );
};
export const InputPageChange = () => {
  const [state, setState] = createStore({ current_page: 3, pages: [1, 2, 3] });
  const onPageChange = (e: Event) => {
    setState({ current_page: Number(e.target.value), pages: [1, 2, 3, 4] });
  };
  return (
    <div>
      <Show when={state.pages}>
        {state.current_page}
        <input value={state.current_page} onInput={onPageChange} />
      </Show>
    </div>
  );
};

const TableWrap = p => <div>{p.children}</div>;
const H1 = p => <h1>{p.children}</h1>;
export const Work = () => (
  <TableWrap>
    <H1>hello</H1>
    <H1>world</H1>
  </TableWrap>
);

export const MyFlatPicker = () => {
  function getFirstValue(newValue) {
    const value = Array.isArray(newValue) && newValue.length === 1 ? newValue[0].getTime() : newValue.getTime();
    console.log(value);
  }
  const [state, setState] = createStore({ value: 604800000 });
  const t = setInterval(() => {
    setState({ value: state.value + 330000 });
  }, 1000);
  onCleanup(() => {
    clearInterval(t);
  });
  const [stateOptions, setStateOptions] = createStore({ wrap: false });
  const t2 = setInterval(() => {
    setStateOptions({ wrap: !stateOptions.wrap });
  }, 1000);
  onCleanup(() => {
    clearInterval(t2);
  });
  const [stateOptionsFn, setStateOptionsFn] = createStore({ fn1: () => console.log(1) });
  const t3 = setInterval(() => {
    setStateOptionsFn({ fn1: () => console.log(2) });
  }, 1000);
  onCleanup(() => {
    clearInterval(t3);
  });
  return (
    <div>
      {state.value}
      <DateTimePicker value={state.value} onChange={getFirstValue} name="Today" />
      <DateTimePicker value={state.value} options={stateOptions} name="Tommorrow" />
      <DateTimePicker value={state.value} onMonthChange={stateOptionsFn.fn1} name="Past Tommorrow" />
    </div>
  );
};

export const InputWitDisabled = () => {
  const props = {
    disabled: true,
  };
  return (
    <>
      <input {...props} />
    </>
  );
};
export const CodeMirrorTest = () => {
  return (
    <div>
      <ControlledCodeMirror
        value=""
        options={{
          mode: 'htmlmixed',
          theme: 'cobalt',
          lineNumbers: true,
          lineWrapping: true,
        }}
        onBeforeChange={(editor, data, value) => {
          // setState({value});
          console.log('set controlled', { value });
        }}
        onChange={(editor, value) => {
          console.log('controlled', { value });
        }}
      />
    </div>
  );
};
export const SingleSelectTest = () => {
  const [state, setState] = createStore({
    data: [
      { _key: '1', name: '1' },
      { _key: '2', name: '2' },
      { _key: '3', name: '3' },
      { _key: '4', name: '4' },
    ],
    value: '1',
    dp: ['_key', ' - ', 'name'],
    key: '_key',
  });
  return (
    <div>
      <SingleSelect data={state.data} value={state.value} dp={state.dp} key={state.key} onChange={v => setState('value', v)} />
      <button onClick={() => setState({ value: '4' })}>Change</button>
      <br />
      value: {JSON.stringify(state)}
    </div>
  );
};

export const TableFormTest = () => {
  const [state, setState] = createStore({
    data: [
      { _key: '1', name: '1' },
      { _key: '2', name: '2' },
      { _key: '3', name: '3' },
      { _key: '4', name: '4' },
    ],
    values: ['1', '4'],
    dp: ['_key', ' - ', 'name'],
    key: '_key',
  });
  return (
    <div>
      <TableForm data={state.data} value={state.values} dp={state.dp} keyIdx={state.key} onChange={v => setState('values', v)} />
      {JSON.stringify(state)}
    </div>
  );
};

// Quill=========
/*
const EMPTY_DELTA = { ops: [] };

export const QuillEditor = props => {
  const [state, setState] = createStore({
    theme: 'snow',
    enabled: true,
    readOnly: false,
    value: EMPTY_DELTA,
    events: [],
    selection: undefined as Range,
  });
  const formatRange = range => {
    return range ? [range.index, range.index + range.length].join(',') : 'none';
  };

  const onEditorChange = (value, delta, source, editor) => {
    setState({
      value: editor.getContents(),
      events: [`[${source}] text-change`, ...state.events],
    });
  };

  const onEditorChangeSelection = (range: Range, source: Sources, editor: ReactQuill.ReactQuill.UnprivilegedEditor) => {
    setState({
      selection: range,
      events: [`[${source}] selection-change(${formatRange(state.selection)} -> ${formatRange(range)})`, ...state.events],
    });
  };

  const onEditorFocus = (range: Range, source: Sources, editor: UnprivilegedEditor) => {
    setState({
      events: [`[${source}] focus(${formatRange(range)})`].concat(state.events),
    });
  };

  const onEditorBlur = (previousRange, source) => {
    setState({
      events: [`[${source}] blur(${formatRange(previousRange)})`].concat(state.events),
    });
  };

  const onToggle = () => {
    setState({ enabled: !state.enabled });
  };

  const onToggleReadOnly = () => {
    setState({ readOnly: !state.readOnly });
  };

  const onSetContents = () => {
    setState({ value: 'This is some <b>fine</b> example content' });
  };

  const renderToolbar = () => {
    const enabled = state.enabled;
    const readOnly = state.readOnly;
    const selection = formatRange(state.selection);
    return (
      <div>
        <button onClick={onToggle}>{enabled ? 'Disable' : 'Enable'}</button>
        <button onClick={onToggleReadOnly}>Set {readOnly ? 'read/Write' : 'read-only'}</button>
        <button onClick={onSetContents}>Fill contents programmatically</button>
        <button disabled={true}>Selection: ({selection})</button>
      </div>
    );
  };

  const renderSidebar = () => {
    return (
      <div style={{ overflow: 'hidden', float: 'right' }}>
        <textarea style={{ display: 'block', width: 300, height: 300 }} value={JSON.stringify(state.value, null, 2)} readOnly={true} />
        <textarea style={{ display: 'block', width: 300, height: 300 }} value={state.events.join('\n')} readOnly={true} />
      </div>
    );
  };
  const modules = {
    toolbar: [
      [{ font: [] }, { size: [] }],
      [{ align: [] }, 'direction'],
      ['bold', 'italic', 'underline', 'strike'],
      [{ color: [] }, { background: [] }],
      [{ script: 'super' }, { script: 'sub' }],
      ['blockquote', 'code-block'],
      [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
      ['link', 'image', 'video'],
      ['clean'],
    ],
  };

  return (
    <div>
      {renderToolbar()}
      <hr />
      {renderSidebar()}
      {state.enabled && (
        <ReactQuill
          theme={state.theme}
          value={state.value}
          readOnly={state.readOnly}
          onChange={onEditorChange}
          onChangeSelection={onEditorChangeSelection}
          onFocus={onEditorFocus}
          onBlur={onEditorBlur}
          modules={modules}
        />
      )}
    </div>
  );
};
*/
