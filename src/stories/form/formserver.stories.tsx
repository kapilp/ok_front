import { JSX, For } from "solid-js";
import { createStore } from "solid-js/store";
import { FormServer } from "../../components/formServer/FormServer";

export default { title: "Form/Form1" };

// export const TableForm_ = () => <TableForm />
export const SignUpForm_ = () => {
  const props = {
    t: ["signup"],
    b: ["signup2"],
    key: null,
    schemaKey: "register",
    form: ["", "", ""],
    fetchConfig: { type: 4, project: null },
    buttonLabels: { save: "Sign up" },
    showdbg: true,
  };
  return (
    <div>
      <FormServer {...props} />
    </div>
  );
};
export const LoginForm = () => {
  const props = {
    t: [],
    b: [],
    key: null,
    schemaKey: "login",
    form: ["", ""],
    fetchConfig: { type: 4, project: null },
    buttonLabels: { save: "Log in" },
    showdbg: true,
  };
  return (
    <div>
      <FormServer {...props} />
    </div>
  );
};
// export const Label_ = () => <Label name="myLabel" />

export const DemoForm = (props) => {
  const [state, setState] = createStore({ form: ["1", "2", "3"] });
  return (
    <div>
      <For each={state.form} fallback={<div>Loading Form Loop...</div>}>
        {(item, i) => (
          <input
            onChange={(e) => {
              const newform = [...state.form];
              newform[i()] = e.target.value;
              setState({ form: newform });
            }}
          >
            {" "}
            {state.form[i()]}{" "}
          </input>
        )}
      </For>
      {JSON.stringify(state.form)}
    </div>
  );
};

export const DemoFormAuhor = (props) => {
  const [state, setState] = createStore({ form: ["1", "2", "3"] });
  return (
    <div>
      <For each={state.form} fallback={<div>Loading Form Loop...</div>}>
        {(item, i) => <input value={item} onChange={(e) => setState("form", i(), e.target.value)} />}
      </For>
      {JSON.stringify(state.form)}
    </div>
  );
};

export const DemoFormAuhorMod = (props) => {
  const [state, setState] = createStore({ form: ["1", "2", "3"] });
  const onChange = (i) => (e) => setState("form", i, e.target.value);
  return (
    <div>
      <For each={state.form} fallback={<div>Loading Form Loop...</div>}>
        {(item, i) => <input value={item} onChange={onChange(i())} />}
      </For>
      {JSON.stringify(state.form)}
    </div>
  );
};
export const DemoFormAuhor2 = (props) => {
  const [state, setState] = createStore({ form: ["1", "2", "3"] });
  const onChange = (i, e) => setState("form", i, e.target.value);
  return (
    <div>
      <For each={state.form} fallback={<div>Loading Form Loop...</div>}>
        {(item, i) => <input value={item} onChange={[onChange, i()]} />}
      </For>
      {JSON.stringify(state.form)}
    </div>
  );
};

const Input = (props: { value: string; onChange: (e: Event) => void }) => {
  return <input value={props.value} onChange={props.onChange} />;
};
export const DemoFormAuthor3 = () => {
  const [state, setState] = createStore({ form: ["1", "2", "3"] });
  const onChange = (i, e) => setState("form", i, e.target.value);
  return (
    <div>
      <For each={state.form} fallback={<div>Loading Form Loop...</div>}>
        {(item, i) => <Input value={item} onChange={[onChange, i()]} />}
      </For>
      {JSON.stringify(state.form)}
    </div>
  );
};
