// import { observable, observe, action, computed, isObservableArray, toJS, asMap, untracked } from 'mobx';
import _ from 'lodash';
import { klona } from 'klona/lite';
import { createComputed, on, untrack } from 'solid-js';
import { createStore } from 'solid-js/store';
import Base from './Base';
// export default  not works !! fixed it
import { $try, $hasFiles, $isBool, $isEvent, pathToStruct } from './utils';
// export default  not works !! fixed it
import { parseInput, parseCheckOutput, defaultValue, defaultClearValue } from './parser';

// fix: this should save to a state not in instace
const setupFieldProps = (setFieldState, props, data) =>
  setFieldState(
    klona({
      $label: props.$label || (data && data.label) || '',
      $placeholder: props.$placeholder || (data && data.placeholder) || '',
      $disabled: props.$disabled || (data && data.disabled) || false,
      $bindings: props.$bindings || (data && data.bindings) || 'default',
      $related: props.$related || (data && data.related) || [],
      $validators: props.$validators || (data && data.validators) || null, // toJS
      $validatedWith: props.$validatedWith || (data && data.validatedWith) || 'value',
      $rules: props.$rules || (data && data.rules) || null,
      $observers: props.$observers || (data && data.observers) || null,
      $interceptors: props.$interceptors || (data && data.interceptors) || null,
      $extra: props.$extra || (data && data.extra) || null,
      $options: props.$options || (data && data.options) || {},
      $hooks: props.$hooks || (data && data.hooks) || {},
      $handlers: props.$handlers || (data && data.handlers) || {},
    }),
  );

const setupDefaultProp = (instance, data, props, update, { isEmptyArray }) =>
  parseInput(instance.$input, {
    nullable: true,
    isEmptyArray,
    type: instance.type,
    unified: update ? defaultValue({ type: instance.type }) : data && data.default,
    separated: props.$default,
    fallback: instance.$initial,
  });

export default class Field extends Base {
  // fields = observable.map ? observable.map({}) : asMap({});
  hasInitialNestedFields = false;

  incremental = false;

  id;

  key;

  name;

  path;

  state;

  $observers;

  $interceptors;

  $hooks = {};

  $handlers = {};

  $input = $ => $;

  $output = $ => $;

  // Default was this variable reactive. I moved it to
  initialState = () => ({
    $options: undefined,
    $value: undefined,
    $type: undefined,
    $label: undefined,
    $placeholder: undefined,
    $default: undefined,
    $initial: undefined,
    $bindings: undefined,
    $extra: undefined,
    $related: undefined,
    $validatedWith: undefined,

    $validators: undefined,
    $rules: undefined,

    $disabled: false,
    $focused: false,
    $touched: false,
    $changed: false,
    $blurred: false,
    $deleted: false,

    $clearing: false,
    $resetting: false,

    $error: null, // added // on error()  // on resetValidation()
    autoFocus: false,
    showError: false,

    errorSync: null,
    errorAsync: null,

    validationErrorStack: [],
    validationFunctionsData: [],
    validationAsyncData: {},

    files: undefined,
  });

  fieldState: any;

  setFieldState: any;

  constructor({ key, path, data = {}, props = {}, update = false, state }) {
    super();
    const [fields, setFields] = createStore({}); // I think this is nested fields
    const [fieldState, setFieldState] = createStore(this.initialState());
    this.fields = fields;
    this.setFields = setFields;
    this.fieldState = fieldState;
    this.setFieldState = setFieldState;
    this.state = state;

    this.setupField(key, path, data, props, update);
    this.checkValidationPlugins();
    this.initNestedFields(data, update);

    this.incremental = this.hasIncrementalKeys !== 0;

    this.debouncedValidation = _.debounce(this.validate, this.state.options.get('validationDebounceWait', this), this.state.options.get('validationDebounceOptions', this));

    this.observeValidationOnBlur();
    this.observeValidationOnChange();

    // this.initMOBXEvent('observers');
    // this.initMOBXEvent('interceptors');

    this.execHook('onInit');
  }

  /* ------------------------------------------------------------------ */
  /* COMPUTED */

  get checkValidationErrors() {
    return (
      (this.fieldState.validationAsyncData.valid === false && !_.isEmpty(this.fieldState.validationAsyncData)) ||
      !_.isEmpty(this.fieldState.validationErrorStack) ||
      _.isString(this.fieldState.errorAsync) ||
      _.isString(this.fieldState.errorSync)
    );
  }

  get checked() {
    return this.type === 'checkbox' ? this.value : undefined;
  }

  get value() {
    return this.getComputedProp('value');
  }

  set value(newVal) {
    if (this.fieldState.$value === newVal) return;
    // handle numbers
    if (this.state.options.get('autoParseNumbers', this) === true) {
      if (_.isNumber(this.fieldState.$initial)) {
        if (new RegExp('^-?\\d+(,\\d+)*(\\.\\d+([eE]\\d+)?)?$', 'g').exec(newVal)) {
          // this.fieldState.$value = _.toNumber(newVal);
          this.setFieldState('$value', _.toNumber(newVal));
          return;
        }
      }
    }
    // handle parse value
    this.setFieldState('$value', newVal);
  }

  get initial() {
    return this.fieldState.$initial ? this.fieldState.$initial : this.getComputedProp('initial');
  }

  get default() {
    return this.fieldState.$default ? this.fieldState.$default : this.getComputedProp('default');
  }

  set initial(val) {
    this.setFieldState('$initial', parseInput(this.$input, { separated: val }));
  }

  set default(val) {
    this.setFieldState('$default', parseInput(this.$input, { separated: val }));
  }

  get actionRunning() {
    return this.submitting || this.clearing || this.resetting;
  }

  get type() {
    return this.fieldState.$type;
  }

  get label() {
    return this.fieldState.$label;
  }

  get placeholder() {
    return this.fieldState.$placeholder;
  }

  get extra() {
    return this.fieldState.$extra;
  }

  get options() {
    return this.fieldState.$options;
  }

  get bindings() {
    return this.fieldState.$bindings;
  }

  get related() {
    return this.fieldState.$related;
  }

  get disabled() {
    return this.fieldState.$disabled;
  }

  get rules() {
    return this.fieldState.$rules;
  }

  get validators() {
    return this.fieldState.$validators;
  }

  get validatedValue() {
    return parseCheckOutput(this, this.fieldState.$validatedWith);
  }

  get error() {
    if (this.fieldState.showError === false) return null;
    return this.fieldState.errorASync || this.fieldState.errorSync || this.fieldState.$error || null;
  }

  get hasError() {
    return this.checkValidationErrors || this.check('hasError', true);
  }

  get isValid() {
    return !this.checkValidationErrors && this.check('isValid', true);
  }

  get isDefault() {
    return !_.isNil(this.default) && _.isEqual(this.default, this.value);
  }

  get isDirty() {
    return !_.isUndefined(this.initial) && !_.isEqual(this.initial, this.value);
  }

  get isPristine() {
    return !_.isNil(this.initial) && _.isEqual(this.initial, this.value);
  }

  get isEmpty() {
    if (this.hasNestedFields) return this.check('isEmpty', true);
    if (_.isBoolean(this.value)) return !!this.fieldState.$value;
    if (_.isNumber(this.value)) return false;
    if (_.isDate(this.value)) return false;
    return _.isEmpty(this.value);
  }

  get resetting() {
    return this.hasNestedFields ? this.check('resetting', true) : this.fieldState.$resetting;
  }

  get clearing() {
    return this.hasNestedFields ? this.check('clearing', true) : this.fieldState.$clearing;
  }

  get focused() {
    return this.hasNestedFields ? this.check('focused', true) : this.fieldState.$focused;
  }

  get blurred() {
    return this.hasNestedFields ? this.check('blurred', true) : this.fieldState.$blurred;
  }

  get touched() {
    return this.hasNestedFields ? this.check('touched', true) : this.fieldState.$touched;
  }

  get changed() {
    return this.hasNestedFields ? this.check('changed', true) : this.fieldState.$changed;
  }

  get deleted() {
    return this.hasNestedFields ? this.check('deleted', true) : this.fieldState.$deleted;
  }

  /* ------------------------------------------------------------------ */
  /* EVENTS HANDLERS */

  sync = (e, v = null) => {
    this.setFieldState('$changed', true);

    const $get = $ => ($isBool($, this.value) ? $.target.checked : $.target.value);

    // assume "v" or "e" are the values
    if (_.isNil(e) || _.isNil(e.target)) {
      if (!_.isNil(v) && !_.isNil(v.target)) {
        v = $get(v); // eslint-disable-line
      }

      this.value = $try(e, v);
      return;
    }

    if (!_.isNil(e.target)) {
      this.value = $get(e);
      return;
    }

    this.value = e;
  };

  onChange = (...args) => (this.type === 'file' ? this.onDrop(...args) : this.execHandler('onChange', args, this.sync));

  onToggle = (...args) => this.execHandler('onToggle', args, this.sync);

  onBlur = (...args) =>
    this.execHandler('onBlur', args, () => {
      if (!this.fieldState.$blurred) {
        this.setFieldState('$blurred', true);
      }

      this.setFieldState('$focused', false);
    });

  onFocus = (...args) =>
    this.execHandler('onFocus', args, () => {
      this.setFieldState({
        $focused: true,
        $touched: true,
      });
    });

  onDrop = (...args) =>
    this.execHandler('onDrop', args, () => {
      const e = args[0];
      let files = null;

      if ($isEvent(e) && $hasFiles(e)) {
        files = _.map(e.target.files);
      }

      this.setFieldState('files', files || args);
    });

  /**
   Prototypes
   */
  // @action
  // Make this perfect. default was it set to instance vars, but now it should save to a state.
  setupField($key, $path, $data, $props, update) {
    this.key = $key;
    this.path = $path;
    this.id = this.state.options.get('uniqueId').apply(this, [this]);
    const struct = this.state.struct();
    const structPath = pathToStruct(this.path);
    const isEmptyArray = Array.isArray(struct)
      ? struct.filter(s => s.startsWith(structPath)).find(s => s.substr(structPath.length, 2) === '[]')
      : Array.isArray(_.get(struct, this.path));

    const { $type, $input, $output } = $props;

    // eslint-disable-next-line
    // if (_.isNil($data)) $data = '';

    if (_.isPlainObject($data)) {
      const { type, input, output } = $data;

      this.name = _.toString($data.name || $key);
      this.setFieldState('$type', $type || type || 'text');
      this.$input = $try($input, input, this.$input);
      this.$output = $try($output, output, this.$output);

      this.setFieldState(
        '$value',
        parseInput(this.$input, {
          isEmptyArray,
          type: this.type,
          unified: $data.value,
          separated: $props.$value,
          fallback: $props.$initial,
        }),
      );

      this.setFieldState(
        '$initial',
        parseInput(this.$input, {
          nullable: true,
          isEmptyArray,
          type: this.type,
          unified: $data.initial,
          separated: $props.$initial,
          fallback: this.fieldState.$value,
        }),
      );

      this.setFieldState(
        '$default',
        setupDefaultProp(this, $data, $props, update, {
          isEmptyArray,
        }),
      );

      setupFieldProps(this.setFieldState, $props, $data);
      return;
    }

    /* The field IS the value here */
    this.name = _.toString($key);
    this.setFieldState('$type', $type || 'text');
    this.$input = $try($input, this.$input);
    this.setFieldState('$output', $try($output, this.fieldState.$output));

    this.setFieldState(
      '$value',
      parseInput(this.$input, {
        isEmptyArray,
        type: this.type,
        unified: $data,
        separated: $props.$value,
      }),
    );

    this.setFieldState(
      '$initial',
      parseInput(this.$input, {
        nullable: true,
        isEmptyArray,
        type: this.type,
        unified: $data,
        separated: $props.$initial,
        fallback: this.fieldState.$value,
      }),
    );

    this.setFieldState(
      '$default',
      setupDefaultProp(this, $data, $props, update, {
        isEmptyArray,
      }),
    );

    setupFieldProps(this.setFieldState, $props, $data);
  }

  getComputedProp(key: string) {
    if (this.incremental || this.hasNestedFields) {
      const $val = key === 'value' ? this.get(key, false) : untrack(() => this.get(key, false));

      return !_.isEmpty($val) ? $val : [];
    }

    const val = this.fieldState[`$${key}`];

    if (_.isArray(val)) {
      return [].slice.call(val);
    }

    return val;
  }

  checkValidationPlugins() {
    const { drivers } = this.state.form.validator;
    const form = this.state.form.name ? `${this.state.form.name}/` : '';

    if (_.isNil(drivers.dvr) && !_.isNil(this.rules)) {
      throw new Error(`The DVR validation rules are defined but no DVR plugin provided. Field: "${form + this.path}".`);
    }

    if (_.isNil(drivers.vjf) && !_.isNil(this.validators)) {
      throw new Error(`The VJF validators functions are defined but no VJF plugin provided. Field: "${form + this.path}".`);
    }
  }

  // @action
  initNestedFields(field, update) {
    const fields = _.isNil(field) ? null : field.fields;

    if (_.isArray(fields) && !_.isEmpty(fields)) {
      this.hasInitialNestedFields = true;
    }

    this.initFields({ fields }, update);

    if (!update && _.isArray(fields) && _.isEmpty(fields)) {
      if (_.isArray(this.value) && !_.isEmpty(this.value)) {
        this.hasInitialNestedFields = true;
        this.initFields({ fields, values: this.value }, update);
      }
    }
  }

  // @action
  invalidate(message, async = false) {
    if (async === true) {
      this.setFieldState('errorAsync', message);
      return;
    }

    if (_.isArray(message)) {
      this.setFieldState('validationErrorStack', message);
      this.showErrors(true);
      return;
    }

    // this.validationErrorStack.unshift(message);
    this.setFieldState('validationErrorStack', [message, ...this.fieldState.validationErrorStack]);
    this.showErrors(true);
  }

  // @action
  setValidationAsyncData(valid = false, message = '') {
    // this.validationAsyncData = { valid, message };
    this.setFieldState('validationAsyncData', { valid, message });
  }

  // @action
  resetValidation(deep = false) {
    this.setFieldState({ showError: true, errorSync: null, errorAsync: null, validationAsyncData: {}, validationFunctionsData: [], validationErrorStack: [], $error: null });
    if (deep) this.each(field => field.resetValidation());
  }

  // @action
  clear(deep = true) {
    this.setFieldState('$clearing', true);
    this.setFieldState('$touched', false);
    this.setFieldState('$changed', false);
    this.setFieldState('$blurred', false);

    this.setFieldState('$value', defaultClearValue({ value: this.fieldState.$value }));
    this.setFieldState('files', undefined);

    if (deep) this.each(field => field.clear(true));

    this.validate({
      showErrors: this.state.options.get('showErrorsOnClear', this),
    });
  }

  // @action
  reset(deep = true) {
    this.setFieldState('$resetting', true);
    this.setFieldState('$touched', false);
    this.setFieldState('$changed', false);
    this.setFieldState('$blurred', false);

    const useDefaultValue = this.fieldState.$default !== this.fieldState.$initial;
    if (useDefaultValue) this.setFieldState('$value', this.fieldState.$default);
    if (!useDefaultValue) this.setFieldState('$value', this.fieldState.$initial);
    this.setFieldState('files', undefined);

    if (deep) this.each(field => field.reset(true));

    this.validate({
      showErrors: this.state.options.get('showErrorsOnReset', this),
    });
  }

  // @action
  focus() {
    // eslint-disable-next-line
    this.state.form.each(field => field.setFieldState('autoFocus', false));
    this.setFieldState('autoFocus', true);
  }

  // @action
  showErrors(show = true) {
    this.setFieldState({ showError: show, errorSync: _.head(this.fieldState.validationErrorStack) });
    this.each(field => field.showErrors(show));
  }

  // @action
  showAsyncErrors() {
    if (this.fieldState.validationAsyncData.valid === false) {
      this.setFieldState('errorAsync', this.fieldState.validationAsyncData.message);
      return;
    }
    this.setFieldState('errorAsync', null);
  }

  observeValidationOnBlur() {
    const opt = this.state.options;
    if (opt.get('validateOnBlur', this)) {
      /* this.disposeValidationOnBlur = observe(this, '$focused',
        change =>
          change.newValue === false &&
          this.debouncedValidation({
            showErrors: opt.get('showErrorsOnBlur', this),
          }),
      ); */
      createComputed(
        on(
          () => this.fieldState.$focused,
          v => v === false && this.debouncedValidation({ showErrors: opt.get('showErrorsOnBlur', this) }),
        ),
      );
    }
  }

  observeValidationOnChange() {
    const opt = this.state.options;
    if (opt.get('validateOnChange', this)) {
      // this.disposeValidationOnChange = observe(this, '$value', () => !this.actionRunning && this.debouncedValidation({showErrors: opt.get('showErrorsOnChange', this),}),);
      createComputed(
        on(
          () => this.fieldState.$value,
          v => !this.actionRunning && this.debouncedValidation({ showErrors: opt.get('showErrorsOnChange', this) }),
        ),
      );
    } else if (opt.get('validateOnChangeAfterInitialBlur', this) || opt.get('validateOnChangeAfterSubmit', this)) {
      /* this.disposeValidationOnChange = observe(this, '$value', () =>
          !this.actionRunning &&
          ((opt.get('validateOnChangeAfterInitialBlur', this) && this.blurred) || (opt.get('validateOnChangeAfterSubmit', this) && this.state.form.submitted)) &&
          this.debouncedValidation({showErrors: opt.get('showErrorsOnChange', this),}), ); */
      createComputed(
        on(
          () => this.fieldState.$value,
          v =>
            !this.actionRunning &&
            ((opt.get('validateOnChangeAfterInitialBlur', this) && this.blurred) || (opt.get('validateOnChangeAfterSubmit', this) && this.state.form.submitted)) &&
            this.debouncedValidation({ showErrors: opt.get('showErrorsOnChange', this) }),
        ),
      );
    }
  }

  // initMOBXEvent(type) {
  //   if (!_.isArray(this[`$${type}`])) return;
  //
  //   let fn;
  //   if (type === 'observers') fn = this.observe;
  //   if (type === 'interceptors') fn = this.intercept;
  //   this[`$${type}`].map(obj => fn(_.omit(obj, 'path')));
  // }

  bind(props = {}) {
    return this.state.bindings.load(this, this.bindings, props);
  }
}
