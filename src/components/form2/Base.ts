import _ from 'lodash';

import { createStore } from 'solid-js/store';
import { $try, $isEvent, hasIntKeys } from './utils';

export default class Base {
  // add: any;
  // clear: any;
  // del: any;
  // fields: any;
  // hooks: any;
  // path: any;
  // reset: any;
  // submit: any;
  noop = () => {};
  baseState: any;
  setBaseState: any;

  constructor() {
    const [baseState, setBaseState] = createStore({
      $submitted: 0,
      $submitting: false,
      $validated: 0,
      $validating: false,
    });
    this.baseState = baseState;
    this.setBaseState = setBaseState;
  }
  execHook = (name, fallback = {}) => $try(fallback[name], this.$hooks[name], this.hooks && this.hooks.apply(this, [this])[name], this.noop).apply(this, [this]);
  execHandler = (name, args, fallback = null) => [
    $try(
      this.$handlers[name] && this.$handlers[name].apply(this, [this]),
      this.handlers && this.handlers.apply(this, [this])[name] && this.handlers.apply(this, [this])[name].apply(this, [this]),
      fallback,
      this.noop,
    ).apply(this, [...args]),
    this.execHook(name),
  ];
  get submitted() {
    return this.baseState.$submitted;
  }

  // this on Form and Field not work
  get submitting() {
    return this.baseState.$submitting;
  }

  get validated() {
    return this.baseState.$validated;
  }

  get validating() {
    return this.baseState.$validating;
  }

  get hasIncrementalKeys() {
    return Object.keys(this.fields).length && hasIntKeys(this.fields);
  }
  get hasNestedFields() {
    return Object.keys(this.fields).length !== 0;
  }
  get size() {
    return Object.keys(this.fields).length;
  }
  /**
     Interceptor
     */
  //intercept = opt => this.MOBXEvent(_.isFunction(opt) ? { type: 'interceptor', call: opt } : { type: 'interceptor', ...opt });
  /**
     Observer
     */
  //observe = opt => this.MOBXEvent(_.isFunction(opt) ? { type: 'observer', call: opt } : { type: 'observer', ...opt });
  /**
     Event Handler: On Clear
     */
  onClear = (...args) =>
    this.execHandler('onClear', args, e => {
      e.preventDefault();
      this.clear(true);
    });
  /**
     Event Handler: On Reset
     */
  onReset = (...args) =>
    this.execHandler('onReset', args, e => {
      e.preventDefault();
      this.reset(true);
    });
  /**
     Event Handler: On Submit
     */
  onSubmit = (...args) =>
    this.execHandler('onSubmit', args, (e, o = {}) => {
      e.preventDefault();
      this.submit(o);
    });
  /**
     Event Handler: On Add
     */
  onAdd = (...args) =>
    this.execHandler('onAdd', args, (e, val) => {
      e.preventDefault();
      this.add($isEvent(val) ? null : val);
    });
  /**
     Event Handler: On Del
     */
  onDel = (...args) =>
    this.execHandler('onDel', args, (e, path) => {
      e.preventDefault();
      this.del($isEvent(path) ? this.path : path);
    });
}
