import { createStore } from 'solid-js/store';
// import { action, computed, observable, asMap } from 'mobx';
import _ from 'lodash';
import Base from './Base';
import Validator from './Validator';
import State from './State';
import Field from './Field';

// Learning:
// Prefer merged store like this form

export default class Form extends Base {
  // changed: boolean;
  // each: any;
  // execHook: (name: any, fallback?: {}) => any;
  // fields: any;
  // handlers: {};
  // hooks: {};
  // initFields: any;
  // setFields: any;
  // touched: boolean;
  name;

  state;

  validator;

  $hooks = {};

  $handlers = {};

  // @observable $submitting = false; // use this from base class
  // @observable $validating = false; // uee this from base class
  // @observable fields = observable.map ? observable.map({}) : asMap({}); // done
  constructor(setup = {}, { name = null, options = {}, plugins = {}, bindings = {}, hooks = {}, handlers = {} } = {}) {
    super();
    this.initFieldsState();
    this.name = name;
    this.$hooks = hooks;
    this.$handlers = handlers;
    // load data from initializers methods
    const initial = _.each(
      {
        setup,
        options,
        plugins,
        bindings,
      },
      (val, key) => (_.isFunction(this[key]) ? _.merge(val, this[key].apply(this, [this])) : val),
    );
    this.state = new State({
      form: this,
      initial: initial.setup,
      options: initial.options,
      bindings: initial.bindings,
    });
    this.validator = new Validator({
      form: this,
      plugins: initial.plugins,
    });
    this.initFields(initial.setup);
    this.debouncedValidation = _.debounce(this.validate, this.state.options.get('validationDebounceWait'), this.state.options.get('validationDebounceOptions'));

    // execute validation on form initialization
    if (this.state.options.get('validateOnInit') === true) {
      this.validator.validate({
        showErrors: this.state.options.get('showErrorsOnInit'),
      });
    }
    this.execHook('onInit');
  }

  initFieldsState() {
    const [fields, setFields] = createStore({}); // nested fields
    this.fields = fields;
    this.setFields = setFields;
  }

  /* ------------------------------------------------------------------ */
  /* COMPUTED */
  //
  get validatedValues() {
    const data = {};
    this.each(
      (
        $field, // eslint-disable-line
      ) => (data[$field.path] = $field.validatedValue),
    );

    return data;
  }

  //
  // Check Field Computed Values Function from Actions.ts
  get clearing() {
    return this.check('clearing', true);
  }

  get resetting() {
    return this.check('resetting', true);
  }

  get error() {
    return this.validator.error();
  }

  get hasError() {
    return !!this.validator.error() || this.check('hasError', true);
  }

  get isValid() {
    return !this.validator.error() && this.check('isValid', true);
  }

  get isPristine() {
    return this.check('isPristine', true);
  }

  get isDirty() {
    return this.check('isDirty', true);
  }

  get isDefault() {
    return this.check('isDefault', true);
  }

  get isEmpty() {
    return this.check('isEmpty', true);
  }

  get focused() {
    return this.check('focused', true);
  }

  get touched() {
    return this.check('touched', true);
  }

  get changed() {
    return this.check('changed', true);
  }

  get disabled() {
    return this.check('disabled', true);
  }

  makeField(data) {
    return new Field(data);
  }

  /**
     Init Form Fields and Nested Fields
     */
  // @action
  init($fields = null) {
    // _.set(this, "fields", observable.map ? observable.map({}) : asMap({}));
    this.initFieldsState();
    this.state.initial.props.values = $fields; // eslint-disable-line
    this.state.current.props.values = $fields; // eslint-disable-line
    this.initFields({
      fields: $fields || this.state.struct(),
    });
  }

  // @action
  invalidate(message = null) {
    this.validator.setError(message || this.state.options.get('defaultGenericError') || true);
  }

  showErrors(show = true) {
    this.each(field => field.showErrors(show));
  }

  /**
     Clear Form Fields
     */
  // @action
  clear() {
    this.$touched = false;
    this.$changed = false;
    this.each(field => field.clear(true));
  }

  /**
     Reset Form Fields
     */
  // @action
  reset() {
    this.$touched = false;
    this.$changed = false;
    this.each(field => field.reset(true));
  }
}
window.f = Form;
