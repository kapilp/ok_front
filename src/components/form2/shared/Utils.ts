import _ from 'lodash';
import utils from '../utils';
import parser from '../parser';
/**
  Field Utils
*/
export default {
  /**
     Fields Selector
     */
  select(path, fields = null, isStrict = true) {
    const $path = parser.parsePath(path);
    const keys = _.split($path, '.');
    const head = _.head(keys);
    keys.shift();
    let $fields = _.isNil(fields) ? this.fields[head] : fields[head];
    let stop = false;
    _.each(keys, $key => {
      if (stop) return;
      if (_.isNil($fields)) {
        $fields = undefined;
        stop = true;
      } else {
        $fields = $fields.fields[$key];
      }
    });
    if (isStrict) utils.throwError(path, $fields);
    return $fields;
  },
  /**
      Get Container
     */
  container($path) {
    const path = parser.parsePath(utils.$try($path, this.path));
    const cpath = _.trim(path.replace(new RegExp('[^./]+$'), ''), '.');
    if (!!this.path && _.isNil($path)) {
      return cpath !== '' ? this.state.form.select(cpath, null, false) : this.state.form;
    }
    return cpath !== '' ? this.select(cpath, null, false) : this;
  },
  /**
      Has Field
     */
  has(path) {
    return _.has(this.fields, path);
  },
  /**
     Map Fields
    */
  map(cb) {
    return Object.values(this.fields).map(cb);
  },
  /**
   * Iterates deeply over fields and invokes `iteratee` for each element.
   * The iteratee is invoked with three arguments: (value, index|key, depth).
   *
   * @param {Function} iteratee The function invoked per iteration.
   * @param {Array|Object} [fields=form.fields] fields to iterate over.
   * @param {number} [depth=1] The recursion depth for internal use.
   * @returns {Array} Returns [fields.values()] of input [fields] parameter.
   * @example
   *
   * JSON.stringify(form)
   * // => {
   *   "fields": {
   *     "state": {
   *       "fields": {
   *         "city": {
   *           "fields": { "places": {
   *                "fields": {},
   *                "key": "places", "path": "state.city.places", "$value": "NY Places"
   *              }
   *           },
   *           "key": "city", "path": "state.city", "$value": "New York"
   *         }
   *       },
   *       "key": "state", "path": "state", "$value": "USA"
   *     }
   *   }
   * }
   *
   * const data = {};
   * form.each(field => data[field.path] = field.value);
   * // => {
   *   "state": "USA",
   *   "state.city": "New York",
   *   "state.city.places": "NY Places"
   * }
   *
   */
  each(iteratee, fields = null, depth = 0) {
    const $fields = fields || this.fields;
    _.each(Object.values($fields), (field, index) => {
      iteratee(field, index, depth);
      if (Object.keys(field.fields).length !== 0) {
        this.each(iteratee, field.fields, depth + 1);
      }
    });
  },
};
