// import { observable, action, toJS, extendObservable, set } from "mobx";
import _ from 'lodash';
import { uniqueId } from './utils';
import {    } from 'solid-js';
import { createStore } from 'solid-js/store';

export default class Options {
  constructor() {
    const [optionsState, setOptionsState] = createStore({
      uniqueId,
      fallback: true,
      defaultGenericError: null,
      submitThrowsError: true,
      showErrorsOnInit: false,
      showErrorsOnSubmit: true,
      showErrorsOnBlur: true,
      showErrorsOnChange: true,
      showErrorsOnClear: false,
      showErrorsOnReset: true,
      validateOnInit: true,
      validateOnBlur: true,
      validateOnChange: false,
      validateOnChangeAfterInitialBlur: false,
      validateOnChangeAfterSubmit: false,
      validateDisabledFields: false,
      validateDeletedFields: false,
      validatePristineFields: true,
      strictUpdate: false,
      strictDelete: true,
      softDelete: false,
      retrieveOnlyDirtyValues: false,
      retrieveOnlyEnabledFields: false,
      autoParseNumbers: false,
      validationDebounceWait: 250,
      validationDebounceOptions: {
        leading: false,
        trailing: true,
      },
    });
    this.optionsState = optionsState;
    this.setOptionsState = setOptionsState;
  }
  get(key = null, field = null) {
    // handle field option
    if (_.has(field, 'path')) {
      if (_.has(field.$options, key)) {
        return field.$options[key];
      }
    }
    // fallback on global form options
    if (key) return this.optionsState[key];
    return this.optionsState;
  }

  set(options) {
    this.setOptionsState(options);
  }
}
