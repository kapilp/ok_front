// import { useStrict, configure } from "mobx";
import Form from './Form';
import Field from './Field';
/* shared prototype methods */
import fieldInitializer from './shared/Initializer';
import fieldHelpers from './shared/Helpers';
import fieldActions from './shared/Actions';
import fieldUtils from './shared/Utils';
import fieldEvents from './shared/Events';
// const extend = ($class, $obj) => $obj.forEach(mixin => Object.getOwnPropertyNames(mixin).forEach(name => ($class.prototype[name] = mixin[name]))); // eslint-disable-line
const extend = ($class, $obj) => $obj.forEach(mixin => Object.assign($class.prototype, mixin));
const shared = [fieldInitializer, fieldActions, fieldHelpers, fieldUtils, fieldEvents];
extend(Form, shared);
extend(Field, shared);
export default Form;
export { Form, Field };
