import { createStore, SetStoreFunction, Store } from 'solid-js/store';

import _ from 'lodash';

import { uniqueId } from './utils';

import OptionsModel from './models/OptionsModel';
import OptionsInterface from './models/OptionsInterface';

export default class Options implements OptionsInterface {
  options: OptionsModel = {
    uniqueId,
    fallback: true,
    defaultGenericError: null,
    submitThrowsError: true,
    showErrorsOnInit: false,
    showErrorsOnSubmit: true,
    showErrorsOnBlur: true,
    showErrorsOnChange: true,
    showErrorsOnClear: false,
    showErrorsOnReset: true,
    validateOnInit: true,
    validateOnBlur: true,
    validateOnChange: false,
    validateOnChangeAfterInitialBlur: false,
    validateOnChangeAfterSubmit: false,
    validateDisabledFields: false,
    validateDeletedFields: false,
    validatePristineFields: true,
    strictUpdate: false,
    strictDelete: true,
    softDelete: false,
    retrieveOnlyDirtyValues: false,
    retrieveOnlyEnabledFields: false,
    autoParseNumbers: false,
    validationDebounceWait: 250,
    validationDebounceOptions: {
      leading: false,
      trailing: true,
    },
    stopValidationOnError: false,
    validationOrder: undefined,
  };

  optionsState: Store<typeof options>;

  setOptionsState: SetStoreFunction<typeof options>;

  constructor() {
    const [optionsState, setOptionsState] = createStore(options);
    this.optionsState = optionsState;
    this.setOptionsState = setOptionsState;
  }

  get(key: string, field: any = null): OptionsModel {
    // handle field option
    if (_.has(field, 'path')) {
      if (_.has(field.$options, key)) {
        return field.$options[key];
      }
    }

    // fallback on global form options
    if (key) return _.get(this.optionsState, key);
    return this.optionsState;
  }

  set(options: OptionsModel): void {
    this.setOptionsState(options);
  }
}
