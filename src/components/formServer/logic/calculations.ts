import { isEmptyObject } from '../../../utils/enums';
import { normalizeId } from 'normalize-id';
export const useCalculations = (optionsState, form) => {
  // Test get with nested state
  const previousOrCurrentValue = (prevValue, name) => (name && name[0] == 'p' ? prevValue : form().$(name).value);
  const makeSlug = (prevValue: any, args: {}, getField: string) => {
    const val = previousOrCurrentValue(prevValue, getField);
    //if (!val) console.log('value must be a string');
    return normalizeId(val ?? '');
  };
  const lowerCase = (prevValue: any, args: {}, getField: string) => {
    return previousOrCurrentValue(prevValue, getField) + '456';
  };
  const allCalculationFunctions = {
    makeSlug,
    lowerCase,
  };
  const processOneRule = (rulePart: {}, to: string) => {
    let previousFnReturnValue;
    Object.keys(rulePart).forEach(keyFn => {
      const fn = allCalculationFunctions[keyFn];
      if (!fn) {
        console.log(keyFn, ' validator function not found');
        return;
      }
      const currentFnReturnValue = fn(previousFnReturnValue, ...rulePart[keyFn]);
      // setState('values', to, currentFnReturnValue);
      form().$(to).set(currentFnReturnValue);
      previousFnReturnValue = currentFnReturnValue;
    });
  };
  const processAllRulesField = (fieldEffects: string[], to: string) => {
    const rulesParts = optionsState?.rulesC ?? {};
    // Todo fix: previous result can be passed to next function if needed
    fieldEffects.forEach(id => {
      const rulePart = rulesParts?.[id];
      if (!rulePart) {
        console.log(id, ' rule component not found');
      } else {
        processOneRule(rulePart, to);
      }
    });
  };
  const processAllFieldsRules = (rules: { [key: string]: string[] }, changedField: string): boolean => {
    Object.keys(rules).forEach(id => {
      processAllRulesField(rules[id], id);
    });
    return true;
  };
  const doCalculations = (changedField: string): boolean => {
    const rules = optionsState?.rules?.[changedField] ?? {};
    if (isEmptyObject(rules)) return true;

    return processAllFieldsRules(rules, changedField);
  };

  return { doCalculations };
};
