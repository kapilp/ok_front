import classNames from 'classnames';
import { For, Match, Show, Switch } from 'solid-js';
import { css } from 'goober';
import { FieldType, FormFields, getIsProduction } from '../../utils/enums';
import { HtmlBase } from '../UI/Html';
import { Button, ButtonTypes, ButtonVariants } from '../UI/core/button/src/Button';
import { DynamicGrid } from '../UI/core/dynamic-grid/src/DynamicGrid';
import { Fieldset } from '../UI/core/form-fieldset/src/Fieldset';
import { useTranslation } from '../react-i18next/src';
import { ActionFooter } from '../UI/core/action-footer/src/ActionFooter';
import { Spacer } from '../UI/core/spacer/src/Spacer';
import { GeneralInput } from './GeneralInput';
import { ServerFormProperties, useFormServer } from './logic/useFormServer';
import { Alert, AlertTypes } from '../UI/core/alert/src/Alert';
import { Region } from '../UI/core/dynamic-grid/src/Region';

// make model form working
// make user able to add custom fields to any form
// make form work for multiple level modification by user
// make update & delete return projection of inserted/updated members too
// schema can be made to not make projection for inserted/updated/deleted members
// make focus working too

// For validation can use : https://github.com/formsy/formsy-react/blob/master/src/validationRules.ts
// todo set validation like: https://codesandbox.io/s/solid-form-validation-2cdti?file=/src/validation.js
// great from library: https://github.com/joeattardi/lwc-forms
// There are 4 options to layout:
// CellGrid
// DynamicGrid -- using
// Field == inline prop
// Grid

const displayNoneCss = css`
  display: none;
`;

export const FormServer = (props: Readonly<ServerFormProperties>) => {
  const { fields, values, form, layout, state, optionsState, serverState, onChange, onApply, onDelete, onCancel } = useFormServer(props);
  const { t, i18n } = useTranslation();

  // Getting Region template from Props so only one layout is possible.
  const AllInputDisplay = (pInternal: { layout: {}; layoutCss: {} }) => (
    <DynamicGrid {...(pInternal.layoutCss ?? {})}>
      <For each={Object.keys(pInternal.layout)} fallback={<div>Loading Form Loop...</div>}>
        {id => (
          <Switch
            fallback={
              <Region defaultPosition={fields()[id]?.props?.region ?? {}} className={form()!.$(id).options.hidden && displayNoneCss}>
                <GeneralInput field={form()!.$(id)} showKeyRev={optionsState.showKeyRev} disabled={optionsState.disabled || form()!.$(id).disabled} onChange={onChange(id)} />
              </Region>
            }
          >
            <Match when={pInternal.layout[id].type == FieldType.fieldSet}>
              <Fieldset {...(pInternal.layout[id]?.props ?? {})}>
                <AllInputDisplay layout={pInternal.layout[id].layout} layoutCss={pInternal.layout[id]?.layoutCss ?? {}} />
              </Fieldset>
            </Match>
            <Match when={pInternal.layout[id].type == FieldType.tab}>
              <Fieldset {...(pInternal.layout[id]?.props ?? {})}>
                <AllInputDisplay layout={pInternal.layout[id].layout} layoutCss={pInternal.layout[id]?.layoutCss ?? {}} />
              </Fieldset>
            </Match>
          </Switch>
        )}
      </For>
    </DynamicGrid>
  );

  return (
    <Show when={serverState.isFetchedTopTemplate && serverState.isFetchedBottomTemplate}>
      <Show when={props.t}>
        <HtmlBase value={serverState.topHtmlTemplate} />
      </Show>
      <Show when={Object.keys(fields()).length > 0 && values() && form()} fallback={<div>Loading Form...</div>}>
        <form className={props.schemaKey} onSubmit={form()!.onSubmit}>
          <AllInputDisplay layoutCss={optionsState.layoutCss} layout={layout()} />
          <ActionFooter
            start={
              <Show when={optionsState.buttonLabels?.cancel ?? true}>
                <Button
                  isDisabled={form().submitting}
                  className={classNames('cancel', { loading: form().submitting })}
                  text={optionsState.buttonLabels?.cancel ?? 'Cancel'}
                  onClick={onCancel}
                />
              </Show>
            }
            end={
              <>
                <Spacer isInlineBlock marginRight="medium">
                  <Button
                    type={ButtonTypes.SUBMIT}
                    variant={ButtonVariants.EMPHASIS}
                    isDisabled={form()!.submitting}
                    className={classNames('submit', 'success', { loading: form()!.submitting })}
                    text={optionsState.buttonLabels?.save || 'Submit'}
                  />
                </Spacer>
                <Show when={optionsState.buttonLabels?.apply}>
                  <Button
                    type={ButtonTypes.SUBMIT}
                    isDisabled={form()!.submitting}
                    className={classNames('submit', 'success', { loading: form()!.submitting })}
                    text={optionsState.buttonLabels.apply || 'Apply'}
                    onClick={onApply}
                  />
                </Show>

                <Show when={props.onDeleteOneRow}>
                  <Button text="Delete" onClick={onDelete} />
                </Show>
                {/* <button type="button" on:click={onReset}>Reset</button> */}
              </>
            }
          />
        </form>
      </Show>
      {state.er && <Alert type={AlertTypes.ERROR}>{state.er}</Alert>}
      {/*! getIsProduction() && */ form() && <pre>{JSON.stringify(form()!.values())}</pre>}
      <Show when={props.b}>
        <HtmlBase value={serverState.bottomHtmlTemplate} />
      </Show>
    </Show>
  );
};
