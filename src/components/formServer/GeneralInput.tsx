import { Match, Switch } from 'solid-js';
import { Hook } from 'flatpickr/dist/types/options';
import { FieldType, FormValue } from '../../utils/enums';
import { ArrayInput } from '../form/input/Array';
// import {JsonEditor} from '../form/input/JsonEditor';
// import {Flatpicker} from '../form/input/Flatpicker';
import { DateTimePicker } from '../form/input/Flatpicker';
// import {DropZone} from '../form/input/DropZone';
// import {DateRange} from '../form/input/DateRange';
// import {Emoji} from '../form/input/Emoji';
// import {CLEditor} from '../form/input/CLEditor';
// import {TableForm} from './tableform/TableForm';
// import {ArrayForm} from '../form/input/Array';
import { JsonEditor } from '../form/input/JsonEditor';
// import {Checkboxes} from '../form/input/Checkboxes';
import { File } from '../form/input/File';
// import {Radio} from '../form/input/Radio';
import { TableFormBoolProperties } from '../form/tableform/TODOBoolPropTableForm';
import { RadioField } from '../UI/core/form-radio/src/RadioField';
import { InputField } from '../UI/core/form-input/src/InputField';
import { UUIDInput } from '../form/input/UUID';
import { TextareaField } from '../UI/core/form-textarea/src/TextareaField';
import { CheckboxField } from '../UI/core/form-checkbox/src/CheckboxField';
import { SingleSelect } from '../form/tableform/SingleSelect';
import { SubDomain } from '../form/input/SubDomain';
import { TableForm } from '../form/tableform/TableForm';
import { ChatInput } from '../form/input/ChatInput';
import Field from '../form2/Field';
import { Checkbox } from '../UI/core/form-checkbox/src/Checkbox';

// Task:
// Optional: Make General Input component support parse and format too. (when needed)
// FieldSet example is very good to write new field.
// make first nested schema / array form working.
// add validators to all the forms.

interface Properties {
  field: Field;
  disabled?: boolean; // done
  onChange: (value: string | number | string[]) => void;
  // isSaving?: boolean
  dom?: HTMLElement | ((e: HTMLElement) => void);
  class?: string;
  showKeyRev?: boolean;
}

export const GeneralInput = (props: Properties) => {
  if (props.h) {
    return <div />;
  }
  if (!props.showKeyRev) {
    if (['Key', 'Rev'].includes(props.field.label || '')) {
      return <div />;
    }
  }
  const getFirstValue: Hook | Hook[] = newValue => {
    const value = Array.isArray(newValue) && newValue.length === 1 ? newValue[0].getTime() : newValue[0].getTime();
    if (props.onChange) props.onChange(value);
  };
  /* const extraProps = {};
  if (type === FormType.select) {
      extraProps.multiSelect = false;
    } else if (type === FormType.multi_select) {
      extraProps.multiSelect = true;
    } else if (type === FormType.multi_select_bool_properties) {
      extraProps.multiSelect = true;
      extraProps.boolprop = true;
    } */

  return (
    <Switch fallback={<div>Unknown Component type: {props.field.type}</div>}>
      <Match when={props.field.type === FieldType.color}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="color"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.email}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="email"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.number || props.field.type === FieldType.serial}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(parseFloat(e.target.value))}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="number"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.password}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="password"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.range}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="range"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.search}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="search"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.text}>
        <InputField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="text"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.checkbox}>
        <CheckboxField required={props.field.isRequired}>
          <Checkbox
            id={props.field.id}
            labelText={props.field.label}
            onChange={e => props.onChange(e.target.checked)}
            value={props.field.value as string}
            checked={!!props.field.value}
            onBlur={props.field.onBlur}
            onFocus={props.field.onFocus}
            disabled={props.disabled}
            error={props.field.error}
            isInvalid={props.field.error}
            {...props.field.options}
          />
        </CheckboxField>
      </Match>
      <Match when={props.field.type === FieldType.radio}>
        <RadioField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          type="radio"
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.textarea || props.field.type === FieldType.codemirror}>
        <TextareaField
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.chatInput}>
        <ChatInput field={props.field} onChange={e => props.onChange(e.target.value)} />
      </Match>
      <Match when={props.field.type === FieldType.select}>
        <SingleSelect
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string}
          onChange={props.onChange}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...(props.field.options ?? {})}
        />
      </Match>
      <Match when={props.field.type === FieldType.file}>
        <File field={props.field} onChange={props.onChange} />
      </Match>
      <Match when={props.field.type === FieldType.text_array}>
        <ArrayInput
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as string[]}
          onChange={props.onChange}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...(props.field.options ?? {})}
        />
      </Match>
      <Match when={props.field.type === FieldType.multi_select}>
        <TableForm
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as Array<{ _key: string; value: {} }>}
          onChange={props.onChange}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...(props.field.options ?? {})}
        />
      </Match>
      <Match when={props.field.type === FieldType.multi_select_bool_properties}>
        <TableFormBoolProperties
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value as Array<{ _key: string; value: {} }>}
          onChange={props.onChange}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          error={props.field.error}
          isInvalid={props.field.error}
          {...(props.field.options ?? {})}
        />
      </Match>
      <Match when={props.field.type === FieldType.jsonEditor}>
        <JsonEditor field={props.field} onChange={props.onChange} disabled={props.disabled} />
      </Match>
      <Match when={props.field.type === FieldType.flatPicker}>
        <DateTimePicker
          inputId={props.field.id}
          label={props.field.label}
          value={props.field.value}
          onChange={getFirstValue}
          onBlur={props.field.onBlur}
          onFocus={props.field.onFocus}
          required={props.field.isRequired}
          disabled={props.disabled}
          f
          error={props.field.error}
          isInvalid={props.field.error}
          {...props.field.options}
        />
      </Match>
      <Match when={props.field.type === FieldType.uuid}>
        <UUIDInput value={props.field.value} onChange={props.onChange} onBlur={props.field.onBlur} onFocus={props.field.onFocus} {...(props.field.options ?? {})} />
      </Match>
      <Match when={props.field.type === FieldType.subdomain}>
        <SubDomain value={props.field.value} onChange={props.onChange} {...(props.field.options ?? {})} />
      </Match>
      <Match when={props.field.type === FieldType.url}>
        <>
          <span>https://</span>
          <InputField
            inputId={props.field.id}
            label={props.field.label}
            value={props.field.value as string}
            onChange={e => props.onChange(e.target.value)}
            onBlur={props.field.onBlur}
            onFocus={props.field.onFocus}
            type="text"
            required={props.field.isRequired}
            disabled={props.disabled}
            error={props.field.error}
            isInvalid={props.field.error}
            {...props.field.options}
          />
          <span>.{props.field.options?.domain ?? ''}</span>
        </>
      </Match>
    </Switch>
  );
};
