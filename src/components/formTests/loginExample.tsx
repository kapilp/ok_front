import { Form } from '../form2/index';

// import validatorjs from 'validatorjs';
// import { dvr } from '../form2/validators/DVR';

export const FormTestLogin = () => {
  const oldFields = [
    { name: 'email', label: 'Email', type: 7, isRequired: true, disabled: false, description: '', props: { placeholder: 'example@email.com' } },
    { name: 'pass', label: 'Password', type: 13, isRequired: true, disabled: false, description: '', props: { placeholder: 'Password' } },
  ];
  const fields = [
    {
      name: 'email',
      label: 'Email',
      placeholder: 'Insert Email',
      // rules: 'required|email|string|between:5,25',
    },
    {
      name: 'password',
      label: 'Password',
      placeholder: 'Insert Password',
      disabled: true,
      // rules: 'required|string|between:5,25',
    },
    {
      name: 'passwordConfirm',
      label: 'Password Confirmation',
      placeholder: 'Confirm Password',
      // rules: 'required|string|same:password',
    },
  ];
  const hooks = {
    onSuccess(form) {
      alert('Form is valid! Send the request here.');
      // get field values
      console.log('Form Values!', form.values());
    },
    onError(form) {
      alert('Form has errors!');
      // get all form errors
      console.log('All form errors', form.errors());
    },
  };
  const plugins = {
    //dvr: dvr(validatorjs),
  };
  const myForm = new Form({ fields }, { plugins, hooks });
  return (
    <form onSubmit={myForm.onSubmit}>
      <label htmlFor={myForm.$('email').id}>{myForm.$('email').label}</label>
      <input {...myForm.$('email').bind()} />
      <p>{myForm.$('email').error}</p>

      <label htmlFor={myForm.$('password').id}>{myForm.$('password').label}</label>
      <input {...myForm.$('password').bind()} />
      <p>{myForm.$('password').error}</p>

      <label htmlFor={myForm.$('passwordConfirm').id}>{myForm.$('passwordConfirm').label}</label>
      <input {...myForm.$('passwordConfirm').bind()} />
      <p>{myForm.$('passwordConfirm').error}</p>

      <button type="button" onClick={_ => console.log(myForm.$('email'))}>
        log field
      </button>
      {/* ... other inputs ... */}

      <button type="submit" onClick={myForm.onSubmit}>
        Submit
      </button>
      <button type="button" onClick={myForm.onClear}>
        Clear
      </button>
      <button type="button" onClick={myForm.onReset}>
        Reset
      </button>

      <p>{myForm.error}</p>
      {JSON.stringify(myForm.values())}
    </form>
  );
};
