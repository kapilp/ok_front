import { assert, expect } from 'chai';
import { Form505 } from './tests/data/forms/fixes/form505';
import { Form425 } from './tests/data/forms/fixes/form425';
import { Form507 } from './tests/data/forms/fixes/form507';
import { FormB } from './tests/data/forms/fixes/formb';
import { FormC } from './tests/data/forms/fixes/formc';
import { FormA } from './tests/data/forms/fixes/forma';
import { Form480 } from './tests/data/forms/fixes/form480';
import { Form481 } from './tests/data/forms/fixes/form481';
import { FormQ1 } from './tests/data/forms/fixes/formq1';
import { FormR } from './tests/data/forms/fixes/formr';
import { FormQ2 } from './tests/data/forms/fixes/formq2';
import { FormS } from './tests/data/forms/fixes/forms';
import { FormP } from './tests/data/forms/fixes/formp';
import { Form514 } from './tests/data/forms/fixes/form514';
import { FormQ } from './tests/data/forms/fixes/formq';
import { FormO } from './tests/data/forms/fixes/formo';
import { FormL } from './tests/data/forms/fixes/forml';
import { FormM } from './tests/data/forms/fixes/formm';
import { FormH } from './tests/data/forms/fixes/formh';
import { FormI } from './tests/data/forms/fixes/formi';
import { FormF } from './tests/data/forms/fixes/formf';
import { FormG } from './tests/data/forms/fixes/formg';
import { FormD } from './tests/data/forms/fixes/formd';
import { FormE } from './tests/data/forms/fixes/forme';
import { Form495 } from './tests/data/forms/fixes/form495';
import { Form472 } from './tests/data/forms/fixes/form472';
import { Form492 } from './tests/data/forms/fixes/form492';
import { Form } from '../form2/index';
// must converted hooks(){ to   hooks = () => { to menu work!!!!!!
//   todo    // Storybook story c menu is gone:
export const FixesLabels = () => {
  // Check Fixes $425 label
  expect(Form425.labels()).to.be.deep.equal({
    '1a': '1aa',
    '2a': '2aa',
    '3a': '3aa',
  });
  return <div>Fixes Labels</div>;
};
export const FixesProps = () => {
  // Check Fixes $E props check
  expect(FormE.select('places').extra).to.be.instanceof(Array);
  expect(FormE.$('places').extra).to.have.lengthOf(3);

  //describe("Check Fixes $G props check", () => {
  //it('$G items[0] label should be equal to "ItemLabel"', () =>
  expect(FormG.$('items[0]').label).to.be.equal('ItemLabel');
  //it('$G items[9] label should be equal to "ItemLabel"', () =>
  expect(FormG.$('items[9]').label).to.be.equal('ItemLabel');
  //it('$G items[10] label should be equal to "ItemLabel"', () =>
  expect(FormG.$('items[10]').label).to.be.equal('ItemLabel');
  //it('$G items[20] label should be equal to "ItemLabel"', () =>
  expect(FormG.$('items[20]').label).to.be.equal('ItemLabel');
  //it('$G items[0].name label should be equal to "ItemsNameLabel"', () =>
  expect(FormG.$('items[0].name').label).to.be.equal('ItemsNameLabel');
  //it('$G items[9].name label should be equal to "ItemsNameLabel"', () =>
  expect(FormG.$('items[9].name').label).to.be.equal('ItemsNameLabel');
  //it('$G items[10].name label should be equal to "ItemsNameLabel"', () =>
  expect(FormG.$('items[10].name').label).to.be.equal('ItemsNameLabel');
  //it('$G items[20].name label should be equal to "ItemsNameLabel"', () =>
  expect(FormG.$('items[20].name').label).to.be.equal('ItemsNameLabel');
  //});
  //describe("Check Fixes $H value computed check", () => {
  //it('$H items[0].alternateName label should be equal to "Alternate Name Label"', () =>
  expect(FormH.$('items[0].alternateName').label).to.be.equal('Alternate Name Label');
  //it('$H items[2].alternateName label should be equal to "Alternate Name Label"', () =>
  expect(FormH.$('items[2].alternateName').label).to.be.equal('Alternate Name Label');
  //});
  //describe("Check Fixes $H props check", () => {
  //it("$H items[0].name related should not be empty array", () =>
  expect(FormH.$('items[0].name').related).to.not.be.empty;
  //it("$H items[0].name related should be array", () =>
  expect(FormH.$('items[0].name').related).to.be.instanceof(Array);
  //it("$H items[0].name validators should not be empty array", () =>
  expect(FormH.$('items[0].name').validators).to.not.be.empty;
  //it("$H items[0].name validators should be array", () =>
  expect(FormH.$('items[0].name').validators).to.be.instanceof(Array);
  //it("$H items[0].name extra should not be empty array", () =>
  expect(FormH.$('items[0].name').extra).to.not.be.empty;
  //it("$H items[0].name extra should be array", () =>
  expect(FormH.$('items[0].name').extra).to.be.instanceof(Array);
  //});
  //describe("Check Fixes $I rules check", () => {
  //it('$I layout.column1[0].title rules should be a equal to "string|required"', () =>
  expect(FormI.$('layout.column1[0].title').rules).to.be.equal('string|required');
  //it('$I deep.nested.column2[0].title rules should be a equal to "string|required"', () =>
  expect(FormI.$('deep.nested.column2[0].title').rules).to.be.equal('string|required');
  //it('$I deep.nested.column3[0].title rules should be a equal to "string|required"', () =>
  expect(FormI.$('deep.nested.column3[0].title').rules).to.be.equal('string|required');
  //it('$I users[0].settings[0].name default should be a equal to "Default Name"', () =>
  expect(FormI.$('users[0].settings[0].name').default).to.be.equal('Default Name');
  //it("$I users[0].settings[0].active default should be true", () =>
  expect(FormI.$('users[0].settings[0].active').default).to.be.true;
  //it('$I users[0].settings[0].name initial should be a equal to "Initial Name"', () =>
  expect(FormI.$('users[0].settings[0].name').initial).to.be.equal('Initial Name');
  //it("$I users[0].settings[0].active initial should be true", () =>
  expect(FormI.$('users[0].settings[0].active').initial).to.be.true;
  //it("$I users[0].settings[0].active value should be false", () =>
  expect(FormI.$('users[0].settings[0].active').value).to.be.false;
  //it("$I users[0].settings[0].bool initial should be false", () =>
  expect(FormI.$('users[0].settings[0].bool').initial).to.be.false;
  //it("$I users[0].settings[0].bool value should be false", () =>
  expect(FormI.$('users[0].settings[0].bool').value).to.be.false;
  //it("$I users[0].settings[0].anotherBool value should be false", () =>
  expect(FormI.$('users[0].settings[0].anotherBool').value).to.be.false;
  //});
  //describe("Check Fixes $Q nested paths check", () => {
  //it('$Q incident[0].type path should be "incident.0.type"', () =>
  expect(FormQ.$('incident').$(0).$('type').path).to.be.equal('incident.0.type');
  //it('$Q incident[0].value path should be "incident.0.value"', () =>
  expect(FormQ.$('incident').$(0).$('value').path).to.be.equal('incident.0.value');
  //it('$Q incident[0].options path should be "incident.0.options"', () =>
  expect(FormQ.$('incident').$(0).$('options').path).to.be.equal('incident.0.options');
  //});
  //describe("Check Fixes $Q1 checks", () => {
  //it("$Q1 tags hasNestedFields should be true", () =>
  expect(FormQ1.$('tags').hasNestedFields).to.be.true;
  //it("$Q1 tags hasInitialNestedFields should be false", () =>
  expect(FormQ1.$('tags').hasInitialNestedFields).to.be.false;
  //it('$Q1 tags[0].id value should be equal to "x"', () =>
  expect(FormQ1.$('tags[0].id').value).to.be.equal('x');
  //it('$Q1 tags[0].name value should be equal to "y"', () =>
  expect(FormQ1.$('tags[0].name').value).to.be.equal('y');
  //it('$Q1 other label should be equal to "Other!!!"', () =>
  expect(FormQ1.$('other').label).to.be.equal('Other!!!');
  //it('$Q1 other.nested value should be equal to "nested-value"', () =>
  expect(FormQ1.$('other.nested').value).to.be.equal('nested-value');
  //});
  //describe("Check Fixes $Q2 checks", () => {
  //it("$Q2 tags hasNestedFields should be false", () =>
  expect(FormQ2.$('tags').hasNestedFields).to.be.false;
  //it("$Q2 tags hasInitialNestedFields should be false", () =>
  expect(FormQ2.$('tags').hasInitialNestedFields).to.be.false;
  //});

  return <div>fixes Props</div>;
};
export const FixesSubmit = () => {
  //describe("Form submit() decoupled callback", () => {
  // $L
  //it("$L.submit() should call onError callback on invalid form", done => {
  FormL.submit({
    onError: form => {
      form.state.options.set({ validateOnChange: true });
      form.$('email').set('value', 'notAnEmailYet');
      //describe("Form $L onError() checks", () => {
      //it('$L state.options "validateOnChange" should be true', () =>
      expect(form.state.options.get('validateOnChange')).to.be.true;
      //it('$L email value should be equal to "notAnEmailYet"', () =>
      expect(form.$('email').value).to.be.equal('notAnEmailYet');
      //it("$L email hasError should be true", () =>
      expect(form.$('email').hasError).to.be.true;
      //it("$L form submitted should be 1", () =>
      expect(form.submitted).to.equal(1);
      //});
      // eslint-disable-next-line
      expect(form.isValid).to.be.false;
      done();
    },
  });
  //});
  // $472
  //describe("$472 submit", () => {
  //it("$472 submit", done => {
  Form472.submit().then(() => {
    done();
  });
  //});
  //});
  // $480
  //describe("$480 submit", () => {
  //it("$480 submit", done => {
  Form480.submit().then(instance => {
    //describe("Form $480 submit checks", () => {
    //it("$480 .isValid should be false.", () =>
    expect(Form480.isValid).to.be.false;
    //it("$480 $(passwordConfirm).isValid should be false.", () =>
    expect(Form480.$('passwordConfirm').isValid).to.be.false;
    const msg = 'The Password Confirmation field is required when Password Required is true.';
    //it("$480 $(passwordConfirm).error should be equal " + msg, () =>
    expect(Form480.$('passwordConfirm').error).to.be.equal(msg);

    //});
    done();
  });
  //});
  //});
  return <div>FixesSubmit</div>;
};
export const FixesTypes = () => {
  //describe("$L Field types checks", () => {
  //it('$L email type should be equal to "email"', () =>
  // hook function should not be arrow, but menu is gone when i fix it.
  // try to run it manually on main app...
  expect(FormL.$('email').type).to.be.equal('email'); // #415
  //});

  return <div>FixesTypes</div>;
};
export const FixesValidation = () => {
  //describe("$M Check jobs[0] ", () => {
  //it("$M Check jobs[0]", done => {
  FormM.$('jobs[0]')
    .validate()
    .then(({ isValid }) => {
      // eslint-disable-next-line
      expect(isValid).to.be.false;
      done();
    });
  //});
  //});
  //describe("Check Fixes $B validation", () => {
  //it("$B people isValid should be false", () =>
  expect(FormB.$('people').isValid).to.be.false;
  //it("$B people hasError should be true", () =>
  expect(FormB.$('people').hasError).to.be.true;
  //it("$B emptyArray isValid should be false", () =>
  expect(FormB.$('emptyArray').isValid).to.be.false;
  //it("$B emptyArray hasError should be true", () =>
  expect(FormB.$('emptyArray').hasError).to.be.true;
  // });

  return <div>FixesValidation</div>;
};
export const FixesValues = () => {
  //describe("$A Field values checks", () => {
  //it('$A qwerty value should be equal to "0"', () =>
  expect(FormA.$('qwerty').value).to.be.equal(0);
  // });
  // describe("$B Field values checks", () => {
  //it('$B inventoryLevel.value value should be equal to "2"', () =>
  expect(FormB.$('inventoryLevel.value').value).to.be.equal(2);
  //it('$B addOns[0].nested.value value should be equal to "3"', () =>
  expect(FormB.$('addOns[0].nested.value').value).to.be.equal(3);
  // });
  // describe("$C Field values checks", () => {
  //it('$C itineraryItems[0].hotel.name value value should be equal to "The Plaza"', () =>
  expect(FormC.$('itineraryItems[0].hotel.name').value).to.be.equal('The Plaza');
  //it('$C itineraryItems[1].hotel.name value value should be equal to "Beverly Hills Hotel"', () =>
  expect(FormC.$('itineraryItems[1].hotel.name').value).to.be.equal('Beverly Hills Hotel');
  //it('$C itineraryItems[2].hotel.name value value should be equal to "Trump Hotel"', () =>
  expect(FormC.$('itineraryItems[2].hotel.name').value).to.be.equal('Trump Hotel');
  // });
  // describe("$D Field values checks", () => {
  //it('$D itineraryItem value value should be equal to "itinerary-item-value"', () =>
  expect(FormD.$('itineraryItem').value).to.be.equal('itinerary-item-value');
  //it('$D itineraryItems[0].hotel.name value value should be equal to "New Test Name"', () =>
  expect(FormD.$('itineraryItems[0].hotel.name').value).to.be.equal('New Test Name');
  //it('$D itineraryItems[1].hotel.name value value should be equal to "New Test Name"', () =>
  expect(FormD.$('itineraryItems[1].hotel.name').value).to.be.equal('New Test Name');
  //it('$D itineraryItems[2].hotel.name value value should be equal to "New Test Name"', () =>
  expect(FormD.$('itineraryItems[2].hotel.name').value).to.be.equal('New Test Name');
  // });
  // describe("$C Form values() method checks", () => {
  {
    const prop = {
      0: 'itineraryItems[0].hotel.name',
      1: 'itineraryItems[1].hotel.name',
      2: 'itineraryItems[2].hotel.name',
    };
    //it(`$C values() ${prop[0]} should be equal to "The Plaza"`, () =>
    // expect(FormC.values()).to.have.deep.property(prop[0], 'The Plaza'); // ok
    //it(`$C values() ${prop[1]} should be equal to "Beverly Hills Hotel"`, () =>
    // expect(FormC.values()).to.have.deep.property(prop[1], 'Beverly Hills Hotel'); // ok
    //it(`$C values() ${prop[2]} should be equal to "Trump Hotel"`, () =>
    // expect(FormC.values()).to.have.deep.property(prop[2], 'Trump Hotel'); // ok
    // });
  }
  // describe("$D Form values() method checks", () => {
  {
    const prop = {
      0: '[0].hotel.name',
      1: '[1].hotel.name',
      2: '[2].hotel.name',
    };
    //it(`$D values() ${prop[0]} should be equal to "New Test Name"`, () =>
    // expect(FormD.$('itineraryItems').values()).to.have.deep.property(prop[0], 'New Test Name');// ok
    //it(`$D values() ${prop[1]} should be equal to "New Test Name"`, () =>
    // expect(FormD.$('itineraryItems').values()).to.have.deep.property(prop[1], 'New Test Name');// ok
    //it(`$D values() ${prop[2]} should be equal to "New Test Name"`, () =>
    // expect(FormD.$('itineraryItems').values()).to.have.deep.property(prop[2], 'New Test Name');// ok
    // });
  }
  // describe("Check Nested $E values()", () => {
  //it("$E places values() should be array", () =>
  // todo when i uncomment whole menu is gone
  // expect(FormE.$('places').values()).to.be.instanceof(Array);
  //it("$E places values() should be length of 0", () =>
  // expect(FormE.values().places).to.have.lengthOf(0);
  //it("$E places values() should be length of 0", () =>
  // expect(FormE.$('places').values()).to.have.lengthOf(0);
  // });
  // describe("Check Nested $F value computed", () => {
  //it('$F inventoryLevel.value value should be equal to "2"', () =>
  // expect(FormF.$('inventoryLevel.value').value).to.be.equal(2);
  //it("$F places value should be array", () =>
  // expect(FormF.$('places').value).to.be.instanceof(Array);
  //it("$F places value should be length of 2", () =>
  // expect(FormF.$('places').value).to.have.lengthOf(2);
  //it("$F skills value should be array", () =>
  // expect(FormF.$('skills').value).to.be.instanceof(Array);
  //it("$F skills value should be length of 0", () =>
  // expect(FormF.$('skills').value).to.have.lengthOf(0);
  //it('$F date value should be equal to "1976-07-02T22:00:00.000Z"', () =>
  // expect(FormF.$('date').value.getTime()).to.be.equal(new Date(1976, 6, 3).getTime());
  //it("$F members[0].hobbies value should be array", () =>
  // expect(FormF.$('members[0].hobbies').value).to.be.instanceof(Array);
  //it("$F members[0].hobbies value should be length of 3", () =>
  // expect(FormF.$('members[0].hobbies').value).to.have.lengthOf(3);
  //it("$F members[1].hobbies value should be array", () =>
  // expect(FormF.$('members[1].hobbies').value).to.be.instanceof(Array);
  //it("$F ids value should be length of 3", () =>
  // expect(FormF.$('ids').value).to.have.lengthOf(3);
  //it("$F ids value should be array", () =>
  // expect(FormF.$('ids').value).to.be.instanceof(Array);
  // });
  // describe("Check Nested $H value computed", () => {
  //it('$H items[0].name value should be equal to "Item #A"', () =>
  // expect(FormH.$('items[0].name').value).to.be.equal('Item #A');
  //it('$H items[2].name value should be equal to "Item #3"', () =>
  // expect(FormH.$('items[2].name').value).to.be.equal('Item #3');
  //it("$H singleFieldArray value should be array", () =>
  // expect(FormH.$('singleFieldArray').value).to.be.instanceof(Array);
  //it("$H singleFieldEmptyArray value should be array", () =>
  // expect(FormH.$('singleFieldEmptyArray').value).to.be.instanceof(Array);
  //it("$H singleFieldEmptyObject value should be object", () =>
  // expect(FormH.$('singleFieldEmptyObject').value).to.be.instanceof(Object);
  //it("$H singleFieldArray value should be array", () =>
  // expect(FormH.$('singleFieldArray').value).to.have.lengthOf(1);
  //it("$H singleFieldEmptyArray value should be array", () =>
  // expect(FormH.$('singleFieldEmptyArray').value).to.have.lengthOf(0);
  // });
  // describe("Check Fixes $I values", () => {
  //it('$I layout.column1[0].title value should be a equal to "THE NEW TITLE"', () =>
  expect(FormI.$('layout.column1[0].title').value).to.be.equal('THE NEW TITLE');
  //it('$I deep.nested.column2[0].title value should be a equal to "THE NEW TITLE"', () =>
  expect(FormI.$('deep.nested.column2[0].title').value).to.be.equal('THE NEW TITLE');
  //it('$I deep.nested.column3[0].title value should be a equal to "THE NEW TITLE"', () =>
  expect(FormI.$('deep.nested.column3[0].title').value).to.be.equal('THE NEW TITLE');
  // });
  // describe("Check Fixes $M values", () => {
  //it("$M people[0].name value should be null", () =>
  expect(FormM.$('people[0].name').value).to.be.null;
  //it("$M items[0].name value should be equal to zero", () =>
  expect(FormM.$('items[0].name').value).to.be.equal(0);
  //it("$M number value should be equal to zero", () =>
  expect(FormM.$('number').value).to.be.equal(0);
  //it("$M array value should be length of 3", () =>
  expect(FormM.$('array').value).to.have.lengthOf(3);
  //it("$M array value should be array", () =>
  expect(FormM.$('array').value).to.be.instanceof(Array);
  //it('$M array[0].name value should be a equal to ""', () =>
  expect(FormM.$('array[0].name').value).to.be.equal('');
  //it('$M array[1].name value should be a equal to ""', () =>
  expect(FormM.$('array[0].name').value).to.be.equal('');
  //it('$M array[2].name value should be a equal to ""', () =>
  expect(FormM.$('array[2].name').value).to.be.equal('');
  // });
  // describe("Check Fixes $O values", () => {
  //it("$O roles value should be an array", () =>
  expect(FormO.$('roles').value).to.be.instanceof(Array);
  //it("$O roles value should be empty", () =>
  expect(FormO.$('roles').value).to.be.empty;
  //it("$O roles value should be an array", () =>
  expect(FormO.$('array').value).to.be.instanceof(Array);
  //it("$O roles value should be empty", () =>
  expect(FormO.$('array').value).to.be.empty;
  // });
  // describe("Check Fixes $P values", () => {
  const values = { street: '123 Fake St.', zip: '12345' };
  const labels = { street: 'street-label', zip: 'zip-label' };
  //it("$P address values() check", () =>
  expect(FormP.$('address').values()).to.be.deep.equal(values);
  //it("$P address value check", () =>
  expect(FormP.$('address').value).to.be.deep.equal(values);
  //it("$P address value check", () =>
  expect(FormP.$('address').labels()).to.be.deep.equal(labels);
  // });
  // describe("Check Fixes $Q values", () => {
  const a = [{ id: 1, name: 'name' }];
  const b = [{ id: 1, name: 'name', value: 'some val' }];
  //it("$Q arrayFieldA values() check", () =>
  expect(FormQ.$('arrayFieldA').values()).to.be.deep.equal(a);
  //it("$Q arrayFieldB values() check", () =>
  expect(FormQ.$('arrayFieldB').values()).to.be.deep.equal(b);
  //it("$Q arrayFieldA value check", () =>
  expect(FormQ.$('arrayFieldA').value).to.be.deep.equal(a);
  //it("$Q arrayFieldB value check", () =>
  expect(FormQ.$('arrayFieldB').value).to.be.deep.equal(b);
  // });
  // describe("Check Fixes $Q1 values", () => {
  //it("$Q1 values check", () =>
  expect(FormQ1.values()).to.be.deep.equal({
    other: {
      nested: 'nested-value',
    },
    tags: [
      {
        id: 'x',
        name: 'y',
      },
    ],
  });
  // });
  // describe("Check Fixes $R values", () => {
  {
    const a = FormR.values().organization;
    const b = FormR.$('organization').value;
    //it("$R values().organization check", () =>
    expect(a).to.be.deep.equal(b);
    //it("$R organization value check", () =>
    expect(b).to.be.deep.equal(b);
    // });
  }
  // describe("Check Fixes $S deleting by path", () => {
  {
    const a = FormS.$('array');
    const hasItemToDelete3 = FormS.has('item_to_delete3');
    //it("$S array field check", () =>
    expect(a.size).to.eq(0);
    //it("$S deleted from root", () =>
    expect(hasItemToDelete3).to.be.false;
    // });
  }
  // describe("Check Fixes $425 values", () => {
  //it("$425 values() check", () =>
  expect(Form425.values()).to.be.deep.equal({
    '1a': ' ',
    '2a': ' ',
    '3a': ' ',
  });
  // });
  // describe("$481 Field values checks", () => {
  //it('$481 length value should be equal to "0"', () =>
  expect(Form481.$('length').value).to.be.equal(0);
  // });
  // describe("separated has correct definition", () => {
  //it("", () => {
  expect(Form492.$('club.name').value).to.be.equal('');
  expect(Form492.$('club.city').value).to.be.equal('');
  expect(Form492.values()).to.be.deep.equal({
    club: {
      name: '',
      city: '',
    },
  });
  // });
  // });
  // describe("set null value", () => {
  //it("", () => {
  {
    const fields = ['club', 'club.name', 'club.city'];
    const labels = {
      club: 'Club',
      'club.name': 'Name',
      'club.city': 'City',
    };
    const values = {
      club: {
        name: 'JJSC',
        city: 'Taipei',
      },
    };
    debugger;
    const Form4 = new Form({ fields, labels, values }, { name: '$495' });
  }
  expect(Form495.$('club.name').value).to.be.equal('JJSC');
  expect(Form495.$('club.city').value).to.be.equal('Taipei');
  Form495.$('club').set(null);
  expect(Form495.values()).to.be.deep.equal({
    club: {
      name: '',
      city: '',
    },
  });
  // });
  // });
  // describe("falsy fallback", () => {
  //it("", () => {
  expect(Form505.$('club.name').value).to.be.equal('JJSC');
  expect(Form505.$('club.city').value).to.be.equal('Taipei');
  expect(Form505.$('club').has('area')).to.be.equal(false);
  expect(() => Form495.$('club.area')).to.throw('field is not defined');
  // });
  // });
  // describe("null date", () => {
  //it("", () => {
  expect(Form507.$('people.0.birthday').value).to.be.equal(null);
  expect(Form507.$('people').add().$('birthday').value).to.be.equal(null);
  // });
  // });
  // describe("update with input", () => {
  //it("", () => {
  expect(Form514.$('priority').value).to.be.equal(1);
  expect(Form514.$('itineraryItems.0.hotel.starRating').value).to.be.equal(5);
  // });
  // });
  // describe("new form with nested array values", () => {
  //it("", () => {
  {
    const fields = ['purpose', 'trip.itineraryItems[].hotel.name', 'trip.itineraryItems[].hotel.starRating'];
    const values = {
      purpose: 'Summer vacation',
      trip: {
        itineraryItems: [
          {
            hotel: {
              name: 'Shangri-La Hotel',
              starRating: '5.0',
            },
          },
          {
            hotel: null,
          },
          {
            hotel: {
              name: 'Trump Hotel',
              starRating: '5.0',
            },
          },
        ],
      },
    };
    const $516 = new Form({ fields, values }, { name: 'Form 516' });
    expect($516.$('purpose').value).to.be.equal('Summer vacation');
    expect($516.$('trip.itineraryItems').size).to.be.equal(3);
    // });
    // });
  }
  // describe("update to nested array items", () => {
  //it("", () => {
  {
    const fields = ['bulletin', 'bulletin.jobs', 'bulletin.jobs[].jobId', 'bulletin.jobs[].companyName'];
    const values = {
      bulletin: {
        jobs: [
          {
            jobId: 1,
            companyName: 'Acer',
          },
          {
            jobId: 2,
            companyName: 'Asus',
          },
        ],
      },
    };
    const $521 = new Form({ fields, values }, { name: 'Form 521' });
    // debugger
    $521.update({
      bulletin: {
        jobs: [
          {
            jobId: 1,
            companyName: 'Apple',
          },
        ],
      },
    });
    expect($521.$('bulletin.jobs').size).to.be.equal(1);
    expect($521.$('bulletin.jobs.0.jobId').value).to.be.equal(1);
    expect($521.$('bulletin.jobs.0.jobId').isDirty).to.be.equal(false);
    expect($521.$('bulletin.jobs.0.companyName').value).to.be.equal('Apple');
    expect($521.$('bulletin.jobs.0.companyName').isDirty).to.be.equal(true);
    expect($521.$('bulletin.jobs.0').isDirty).to.be.equal(true);
    expect($521.$('bulletin.jobs').isDirty).to.be.equal(true);
    // });
    // });
  }
  // describe("#523", () => {
  //it("", () => {
  {
    const fields = [
      {
        name: 'fieldA',
        label: 'fieldA',
      },
      {
        name: 'fieldB',
        label: 'fieldB',
        fields: [
          {
            name: 'nestedB',
            label: 'nestedB',
          },
        ],
      },
    ];
    const $523 = new Form({ fields }, { name: 'Form 523' });
    expect($523.isDirty).to.be.false;
    // });
    // });
  }
  // describe("update nested nested array items", () => {
  {
    //it("", () => {
    const fields = ['pricing', 'pricing.value[]', 'pricing.value[].prices[]', 'pricing.value[].prices[].money', 'pricing.value[].prices[].quantity'];
    const values = {
      pricing: {
        value: [
          {
            prices: [
              {
                money: 35,
                quantity: 1,
              },
            ],
          },
        ],
      },
    };
    const $526 = new Form({ fields, values }, { name: 'Form 526' });
    console.debug('pricing.value.0.initial', $526.$('pricing.value.0').initial);
    console.debug('pricing.value.0.prices.initial', $526.$('pricing.value.0.prices').initial);
    $526.update({
      pricing: {
        value: [
          {
            prices: [
              {
                money: 35,
                quantity: 1,
              },
              {
                money: 100,
                quantity: 3,
              },
            ],
          },
        ],
      },
    });
    console.debug('pricing.value.0.initial', $526.$('pricing.value.0').initial);
    console.debug('pricing.value.0.prices.initial', $526.$('pricing.value.0.prices').initial);
    expect($526.$('pricing.value').isDirty).to.be.equal(true);
    expect($526.$('pricing.value.0').isDirty).to.be.equal(true);
    expect($526.$('pricing.value.0.prices').isDirty).to.be.equal(true);
    // });
    // });
  }
  // describe("falsy fallback for array items", () => {
  //it("", () => {
  {
    const fields = ['purpose', 'trip.itineraryItems[].hotel.name', 'trip.itineraryItems[].hotel.starRating'];
    const values = {
      purpose: 'Summer vacation',
      trip: {
        itineraryItems: [
          {
            hotel: {
              name: 'Shangri-La Hotel',
              starRating: '5.0',
              favorite: true,
            },
          },
          {
            hotel: null,
          },
          {
            hotel: {
              name: 'Trump Hotel',
              starRating: '5.0',
              favorite: false,
            },
          },
        ],
      },
    };
    const $527 = new Form({ fields, values }, { name: 'Form 527', options: { fallback: false } });
    expect($527.$('purpose').value).to.be.equal('Summer vacation');
    expect($527.$('trip.itineraryItems').size).to.be.equal(3);
    expect($527.$('trip.itineraryItems.0.hotel')).not.to.be.undefined;
    expect($527.select('trip.itineraryItems.0.hotel.favorite', null, false)).to.be.undefined;
    // });
    // });
  }
  return <div>FixesValues</div>;
};
// export const TestOptions = () => {
//   //describe("Field values checks", () => {
//   //it('Fixes $A state.options.get(loadingMessage) should be equal to "Custom Loading Message..."', () =>
//   expect($fixes.$A.state.options.get('loadingMessage')).to.be.equal('Custom Loading Message...');
//   // });
//   // describe("Check Flat $B error prop", () => {
//   //it('Flat $B state.options defaultGenericError should be equal to "Custom Generic Error"', () =>
//   expect($flat.$B.state.options.get('defaultGenericError')).to.be.equal('Custom Generic Error');
//   // });
//   // describe("Check Flat $I error prop", () => {
//   //it("Flat $I state.options.get(strictUpdate) should be true", () =>
//   expect($flat.$I.state.options.get('strictUpdate')).to.be.true;
//   //it("Flat $I state.options.get() should be an object", () =>
//   expect($flat.$I.state.options.get()).to.be.an('object');
//   // });
//
//   return <div>TestOptions</div>;
// };
// export const TestPromises = () => {
//   flat.validate($flat);
//   nested.validate($nested);
//   fixes.validate($fixes);
//
//   return <div>TestPromises</div>;
// };
