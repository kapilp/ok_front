// import { assert, expect } from 'chai';
// import { FormFlatA } from '../tests/data/forms/flat/form.a';
// import { FormFlatI } from '../tests/data/forms/flat/form.i';
// import { FormFlatL } from '../tests/data/forms/flat/form.l';
// import { FormFlatH } from '../tests/data/forms/flat/form.h';
// import { FormFlatG } from '../tests/data/forms/flat/form.g';
// import { FormFlatQ } from '../tests/data/forms/flat/form.q';
// import { FormFlatR } from '../tests/data/forms/flat/form.r';
// import { FormFlatB } from '../tests/data/forms/flat/form.b';
// import { FormFlatC } from '../tests/data/forms/flat/form.c';
// import { FormFlatD } from '../tests/data/forms/flat/form.d';
// import { FormFlatM } from '../tests/data/forms/flat/form.m';
// import { FormFlatN } from '../tests/data/forms/flat/form.n';
// import { FormFlatO } from '../tests/data/forms/flat/form.o';
// import { FormFlatP } from '../tests/data/forms/flat/form.p';
// export default { title: 'Mobx Flat Form' };
// export const FlatActions = () => {
//   //describe("$L Form", () => {
//   //it("$L username.value should be empty", () =>
//   expect(FormFlatL.$('username').value).to.be.empty;
//   //it("$L email.value should be empty", () =>
//   expect(FormFlatL.$('email').value).to.be.empty;
//   //it("$L password.value should be empty", () =>
//   expect(FormFlatL.$('password').value).to.be.empty;
//   //it("$L password.error should be null", () =>
//   expect(FormFlatL.$('password').error).to.be.null;
//   // });
//   // describe("$H Form fields", () => {
//   //it('$H username.value should be equal to "SteveJobs"', () =>
//   expect(FormFlatH.$('username').value).to.be.equal('SteveJobs');
//   //it('$H email.value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormFlatH.$('email').value).to.be.equal('s.jobs@apple.com');
//   //it('$H email.label should be equal to "Email"', () =>
//   expect(FormFlatH.$('email').label).to.be.equal('Email');
//   //it("$H devSkills.value should be empty", () =>
//   expect(FormFlatH.$('devSkills').value).to.be.empty;
//   // });
//   // describe("$O Form fields", () => {
//   //it('$O username.value should be equal to "TestUser"', () =>
//   expect(FormFlatO.$('username').value).to.be.equal('TestUser');
//   //it('$O email.value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormFlatO.$('email').value).to.be.equal('s.jobs@apple.com');
//   //it('$O email.label should be equal to "E-mail"', () =>
//   expect(FormFlatO.$('email').label).to.be.equal('E-mail');
//   //it("$O error should be null", () => expect(FormFlatO.error).to.be.null);
//   // });
//   // describe("$P Form fields", () => {
//   //it('$P username.value should be equal to "TestUser"', () =>
//   expect(FormFlatP.$('username').value).to.be.equal('TestUser');
//   //it('$P email.value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormFlatP.$('email').value).to.be.equal('s.jobs@apple.com');
//   //it('$P username.label should be equal to "UserName"', () =>
//   expect(FormFlatP.$('username').label).to.be.equal('UserName');
//   //it('$P passwordConfirm.label should be equal to "Confirm Password"', () =>
//   expect(FormFlatP.$('passwordConfirm').label).to.be.equal('Confirm Password');
//   //it("$P username.isValid should be false", () =>
//   expect(FormFlatP.$('username').isValid).to.be.false;
//   //it("$P email.isValid should be false", () =>
//   expect(FormFlatP.$('email').isValid).to.be.false;
//   //it("$P terms.disabled should be true", () =>
//   expect(FormFlatP.$('terms').disabled).to.be.true;
//   //it("$P error should be null", () => expect(FormFlatP.error).to.be.null);
//   // });
//   // describe("$Q Form fields", () => {
//   //it('$Q username.value should be equal to "SteveJobs"', () =>
//   expect(FormFlatQ.$('username').value).to.be.equal('SteveJobs');
//   //it('$Q username.label should be equal to "Username"', () =>
//   expect(FormFlatQ.$('username').label).to.be.equal('Username');
//   //it('$Q email.value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormFlatQ.$('email').value).to.be.equal('s.jobs@apple.com');
//   //it('$Q email.label should be equal to "Email"', () =>
//   expect(FormFlatQ.$('email').label).to.be.equal('Email');
//   // });
//   // describe("$R Form fields", () => {
//   //it('$R email.value should be equal to "invalid"', () =>
//   expect(FormFlatR.$('email').value).to.be.equal('invalid');
//   //it("$R email.hasError should be true", () =>
//   expect(FormFlatR.$('email').hasError).to.be.true;
//   //it("$R email.isValid should be false", () =>
//   expect(FormFlatR.$('email').isValid).to.be.false;
//   //it("$R emailConfirm.hasError should be true", () =>
//   expect(FormFlatR.$('emailConfirm').hasError).to.be.true;
//   //it("$R emailConfirm.isValid should be false", () =>
//   expect(FormFlatR.$('emailConfirm').isValid).to.be.false;
//   // });
//   // describe("others fields", () => {
//   //it('$C error should be equal to "The user already exist."', () =>
//   expect(FormFlatC.error).to.be.equal('The user already exist');
//   //it("$D devSkills.isValid should be false", () =>
//   expect(FormFlatD.$('devSkills').isValid).to.be.false;
//   //it('$D username.value should be equal to "JonathanIve"', () =>
//   expect(FormFlatD.$('username').value).to.be.equal('JonathanIve');
//   //it("$D terms.value should be false", () =>
//   expect(FormFlatD.$('terms').value).to.be.false;
//   //it("$D terms.hasError should be false", () =>
//   expect(FormFlatD.$('terms').hasError).to.be.false;
//   //it("$D revenue.hasError should be true", () =>
//   expect(FormFlatD.$('revenue').hasError).to.be.true;
//   //it('$D revenue.value should be equal to "aaa"', () =>
//   expect(FormFlatD.$('revenue').value).to.be.equal('aaa');
//   //it('$D assets.value should be equal to "0"', () =>
//   expect(FormFlatD.$('assets').value).to.be.equal(0);
//   //it("$D assets.isEmpty should be false", () =>
//   expect(FormFlatD.$('assets').isEmpty).to.be.false;
//   //it('$D terms.label should be equal to "Accept Terms of Service"', () =>
//   expect(FormFlatD.$('terms').label).to.be.equal('Accept Terms of Service');
//   //it('$I username.value should be equal to "SteveJobs"', () =>
//   expect(FormFlatI.$('username').value).to.be.equal('SteveJobs');
//   //it('$M username.default should be equal to "Claudio"', () =>
//   expect(FormFlatM.$('username').default).to.be.equal('Claudio');
//   //it('$M username.value should be equal to "Claudio"', () =>
//   expect(FormFlatM.$('username').value).to.be.equal('Claudio');
//   //it('$M username.initial should be equal to "SteveJobs"', () =>
//   expect(FormFlatM.$('username').initial).to.be.equal('SteveJobs');
//   //it("$M username.isValid should be false", () =>
//   expect(FormFlatM.$('username').isValid).to.be.false;
//   //it("$N email.isValid should be false", () =>
//   expect(FormFlatN.$('email').isValid).to.be.false;
//   //it('$N email.value should be equal to "12345"', () =>
//   expect(FormFlatN.$('email').value).to.be.equal('12345');
//   // });
//   // describe("Field values type checks", () => {
//   //it("$A terms.value is a boolean", () =>
//   assert.isBoolean(FormFlatA.$('terms').value, '$A terms.value is not a boolean');
//   //it("$A revenue.value is string", () =>
//   assert.isString(FormFlatA.$('revenue').value, '$A revenue.value is not string');
//   //it("$A assets.value is a number", () =>
//   assert.isNumber(FormFlatA.$('assets').value, '$A assets.value is not a number');
//   // });
//
//   return <div>FlatActions</div>;
// };
// export const FlatProps = () => {
//   //describe("Check Flat $A placeholder field prop", () => {
//   //it('$A username placeholder should be equal to "Username Placeholder"', () =>
//   expect(FormFlatA.$('username').placeholder).to.be.equal('Username Placeholder');
//   //it('$A username type should be equal to "text"', () =>
//   expect(FormFlatA.$('username').type).to.be.equal('text');
//   //it('$A username bindings should be equal to "default"', () =>
//   expect(FormFlatA.$('username').bindings).to.be.equal('default');
//   //it('$A devSkills value should be equal to "5" (string)', () =>
//   expect(FormFlatA.$('devSkills').value).to.be.equal('5');
//   //it("$A devSkills value should be a string", () =>
//   expect(FormFlatA.$('devSkills').value).to.be.a('string');
//   //it("$A devSkills get(value) should be equal to 5 (number)", () =>
//   expect(FormFlatA.$('devSkills').get('value')).to.be.equal(5);
//   //it("$A devSkills get(value) should be a number", () =>
//   expect(FormFlatA.$('devSkills').get('value')).to.be.a('number');
//   //it("$A devSkills id should start with devSkills--", () =>
//   expect(FormFlatA.$('devSkills').id.substring(0, 11)).to.be.equal('devSkills--');
//   // });
//   // describe("Check Flat $B value field prop", () => {
//   //it("$B username value should be empty string", () =>
//   expect(FormFlatB.$('username').value).to.be.equal('');
//   // });
//   // describe("Check Flat $R extended field prop", () => {
//   //it("$R email should have property newFieldProp", () =>
//   expect(FormFlatR.$('email')).to.have.property('newFieldProp');
//   //it("$R email newFieldProp should be true", () =>
//   expect(FormFlatR.$('email').newFieldProp).to.be.true;
//   // });
//   // describe("Check Flat $G prop", () => {
//   //it("$G username error should be null", () =>
//   expect(FormFlatG.errors().username).to.be.null;
//   // });
//
//   return <div>FlatProps</div>;
// };
// export const FlatSubmit = () => {
//   //describe("Form submit() decoupled callback", () => {
//   // $A
//   //it("$A.submit() should call onSuccess callback on valid form", done => {
//   FormFlatA.submit({
//     onSuccess: form => {
//       expect(form.submitted).to.equal(1);
//       expect(form.isValid).to.be.true;
//       done();
//     },
//   });
//   // });
//   // $I
//   //it("$I.submit() should call onSuccess callback on valid form", done => {
//   FormFlatI.submit({
//     onSuccess: form => {
//       expect(form.submitted).to.equal(1);
//       expect(form.isValid).to.be.true;
//       done();
//     },
//   });
//   // });
//   // $N
//   //it("$N.submit() should call onError callback on invalid form", done => {
//   FormFlatN.submit({
//     onError: form => {
//       expect(form.submitted).to.equal(1);
//       expect(form.isValid).to.be.false;
//       done();
//     },
//   });
//   // });
//   // });
//
//   return <div>FlatSubmit</div>;
// };
// export const FlatTypes = () => {
//   const checkHelperIsObject = helper =>
//     _.each($forms, (form, key) =>
//       //it(`${key} ${helper}() is object`, () =>
//       assert.isObject(form[helper](), `${key}.${helper}() is not object`),
//     );
//   const checkComputedIsBoolean = computed =>
//     _.each($forms, (form, key) =>
//       //it(`${key} ${computed} is boolean`, () =>
//       assert.isBoolean(form[computed], `${key}.${computed} is not boolean`),
//     );
//   // describe("Check validate() returns promise that resolves to boolean", () => {
//   _.each($forms, (form, key) => {
//     //it(`${key} validate() is promise that resolves to boolean`, () => {
//     const promise = form.validate();
//     expect(promise).to.be.a('promise');
//     return promise.then(result => expect(result.isValid).to.be.a('boolean'));
//     // })
//   });
//   // );
//   // });
//   // describe("Check FORM validate(key) returns promise that resolves to boolean", () =>
//   _.each($forms, (form, formKey) =>
//     //describe(`${formKey} form`, () =>
//     form.each(field => {
//       //it(`validate('${field.path}') is promise that resolves to boolean`, () => {
//       const promise = form.validate(field.path);
//       expect(promise).to.be.a('promise');
//       return promise.then(result => expect(result.isValid).to.be.a('boolean'));
//     }),
//   );
//   // ));
//   // describe("Check FIELD validate(key) returns promise that resolves to boolean", () =>
//   _.each($forms, (form, formKey) => {
//     //describe(`${formKey} form`, () =>
//     form.each(field => {
//       //it(`validate('${field.path}') is promise that resolves to boolean`, () => {
//       const promise = field.validate();
//       expect(promise).to.be.a('promise');
//       return promise.then(result => expect(result.isValid).to.be.a('boolean'));
//     });
//   });
//   // ));
//   // describe("Check form helpers returns object", () => {
//   checkHelperIsObject('types');
//   checkHelperIsObject('get');
//   checkHelperIsObject('values');
//   checkHelperIsObject('errors');
//   checkHelperIsObject('labels');
//   checkHelperIsObject('placeholders');
//   checkHelperIsObject('defaults');
//   checkHelperIsObject('initials');
//   // });
//   // describe("Check form computed returns boolean", () => {
//   checkComputedIsBoolean('validating');
//   checkComputedIsBoolean('hasError');
//   checkComputedIsBoolean('isDirty');
//   checkComputedIsBoolean('isPristine');
//   checkComputedIsBoolean('isDefault');
//   checkComputedIsBoolean('isValid');
//   checkComputedIsBoolean('isEmpty');
//   checkComputedIsBoolean('focused');
//   checkComputedIsBoolean('touched');
//   checkComputedIsBoolean('changed');
//   checkComputedIsBoolean('disabled');
//   // });
//
//   return <div>FlatTypes</div>;
// };
