import validatorjs from 'validatorjs';
import { Form } from '../../../../../form2/index';
import { dvr } from '../../../../../form2/validators/DVR';
const plugins = {
  dvr: dvr(validatorjs),
};
const fields = [
  {
    name: 'passwordConfirm',
    label: 'Password Confirmation',
    rules: 'required_if:passwordRequired,true',
    value: '',
  },
  {
    name: 'passwordRequired',
    label: 'Password Required',
    type: 'checkbox',
    value: true,
    output: value => (value === true ? 'true' : 'false'),
  },
];
class NewForm extends Form {}
export const Form480 = new NewForm({ fields }, { plugins, name: 'Fixes-480' });
