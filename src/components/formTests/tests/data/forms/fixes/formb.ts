import validatorjs from 'validatorjs';
import { Form } from '../../../../../form2/index';
import { dvr } from '../../../../../form2/validators/DVR';
console.log(Form, dvr);
const plugins = {
  dvr: dvr(validatorjs),
};
const fields = ['people', 'people[]', 'inventoryLevel.value', 'addOns[].nested.value'];
const values = {
  emptyArray: [],
  people: [1, 2, 3, 4, 5, 6],
  inventoryLevel: {
    value: 2,
  },
  addOns: [
    {
      nested: {
        value: 3,
      },
    },
  ],
};
const rules = {
  emptyArray: 'required|array',
  people: 'required|array|between:3,5',
};
export const FormB = new Form({ fields, values, rules }, { plugins, name: 'Fixes-B' });
