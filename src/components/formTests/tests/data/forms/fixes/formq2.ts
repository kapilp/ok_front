import { Form } from '../../../../../form2/index';
const fields = ['multiselectArray', 'multiselectObject', 'tags[]', 'tags[].id', 'tags[].name'];
const values = {
  multiselectArray: ['iMac', 'iPhone'],
  multiselectObject: { value: 'watch', label: 'Watch' },
};
class NewForm extends Form {}
export const FormQ2 = new NewForm({ fields, values }, { name: 'Fixes-Q2' });
