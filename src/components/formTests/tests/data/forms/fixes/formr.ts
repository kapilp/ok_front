import { Form } from '../../../../../form2/index';
const fields = [
  {
    name: 'organization',
    fields: [
      {
        name: 'nested',
      },
    ],
  },
];
class NewForm extends Form {}
export const FormR = new NewForm({ fields }, { name: 'Fixes-R' });
