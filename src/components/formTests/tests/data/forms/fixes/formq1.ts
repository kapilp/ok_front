import { Form } from '../../../../../form2/index';
const struct = ['tags[]', 'tags[].id', 'tags[].name'];
const fields = [
  {
    name: 'tags',
    label: 'Tags!!!',
  },
  {
    name: 'other',
    label: 'Other!!!',
    fields: [
      {
        name: 'nested',
        value: 'nested-value',
      },
    ],
  },
];
class NewForm extends Form {
  hooks() {
    return {
      onInit() {
        this.$('tags').add([
          {
            id: 'x',
            name: 'y',
          },
        ]);
        // EQUIVALENT
        // this.$('tags').add();
        // this.$('tags[0]').set({
        //   id: 'x',
        //   name: 'y',
        // });
        // EQUIVALENT
        // this.update({
        //   tags: [{
        //     id: 'x',
        //     name: 'y',
        //   }],
        // });
      },
    };
  }
}
export const FormQ1 = new NewForm({ struct, fields }, { name: 'Fixes-Q1' });
