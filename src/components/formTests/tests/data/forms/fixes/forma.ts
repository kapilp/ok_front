// import { Form } from '../../../../../src/components/form2';
import { Form } from '../../../../../form2/index';
const fields = ['qwerty'];
const values = { qwerty: 0 };
class NewForm extends Form {
  hooks = () => {
    return {
      onInit(form) {
        form.state.options.set({
          loadingMessage: 'Custom Loading Message...',
        });
      },
    };
  };
}
export const FormA = new NewForm({ fields, values }, { name: 'Fixes-A' });
