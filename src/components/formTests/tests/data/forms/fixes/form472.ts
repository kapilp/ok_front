import { Form } from '../../../../../form2/index';
const fields = {};
class HookedForm extends Form {
  hooks = () => {
    return {
      onSuccess: form => form.values(),
      onSubmit: instance => instance,
    };
  };
}
export const Form472 = new HookedForm({ fields }, { name: 'Fixes-472' });
