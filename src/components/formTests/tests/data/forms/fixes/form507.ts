import { Form } from '../../../../../form2/index';
import { expect } from 'chai';
const fields = ['people', 'people[]', 'people[].name', 'people[].birthday'];
const types = {
  'people[].birthday': 'date',
};
const values = {
  people: [
    {
      name: 'adam',
      birthday: null,
    },
  ],
};
class NewForm extends Form {}
export const Form507 = new NewForm({ fields, types, values }, { name: '$507' });
