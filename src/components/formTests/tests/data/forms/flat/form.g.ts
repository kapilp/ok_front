import { Form } from '../../../../../form2/index';
const fields = {
  username: {
    label: 'Username',
  },
  email: {
    label: 'Email',
  },
  password: {
    label: 'Password',
  },
};
export const FormFlatG = new Form({ fields }, { name: 'Flat-G' });
