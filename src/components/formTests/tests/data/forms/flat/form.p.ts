import validatorjs from 'validatorjs';
import { Form } from '../../../../../form2/index';
import { dvrExtend } from '../../extension/dvr';
import { shouldBeEqualTo } from '../../extension/vjf';
import { dvr } from '../../../../../form2/validators/DVR';
import { vjf } from '../../../../../form2/validators/VJF';
const fields = ['username', 'email', 'password', 'passwordConfirm', 'terms'];
const values = {
  username: 'SteveJobs',
  email: 's.jobs@apple.com',
  terms: true,
};
const defaults = {
  username: 'TestUser',
};
const labels = {
  passwordConfirm: 'Confirm Password',
};
const validators = {
  email: shouldBeEqualTo('username'),
};
const rules = {
  username: 'email',
};
const disabled = {
  terms: true,
};
class NewForm extends Form {
  plugins() {
    return {
      vjf: vjf(),
      dvr: dvr({
        package: validatorjs,
        extend: dvrExtend,
      }),
    };
  }
  hooks() {
    return {
      onInit(form) {
        form.$('username').set('label', 'UserName');
        form.reset();
      },
    };
  }
}
export const FormFlatP = new NewForm(
  {
    fields,
    values,
    defaults,
    labels,
    disabled,
    validators,
    rules,
  },
  { name: 'Flat-P' },
);
