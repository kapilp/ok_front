import ajv from 'ajv';
import { Form } from '../../../../../form2/index';
import { svk } from '../../../../../form2/validators/SVK';
const fields = {
  username: {
    label: 'Username',
    value: 'SteveJobs',
  },
  email: {
    label: 'Email',
    value: 's.jobs@apple.com',
  },
  password: {
    label: 'Password',
    value: 'thinkdifferent',
  },
};
const schema = {
  type: 'object',
  properties: {
    username: { type: 'string', minLength: 6, maxLength: 20 },
    email: { type: 'string', format: 'email', minLength: 5, maxLength: 20 },
    password: { type: 'string', minLength: 6, maxLength: 20 },
  },
};
const plugins = {
  svk: svk({
    package: ajv,
    schema,
  }),
};
class NewForm extends Form {
  options() {
    return {
      strictUpdate: true,
    };
  }
  hooks() {
    return {
      onInit(form) {
        form.update({ username: 'JonathanIve' });
        form.reset(); // to const FormFlat = or initial values
      },
    };
  }
}
export const FormFlatI = new NewForm({ fields }, { plugins, name: 'Flat-I' });
