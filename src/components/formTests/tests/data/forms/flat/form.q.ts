import { Form } from '../../../../../form2/index';
const fields = [
  {
    name: 'username',
    label: 'Username',
    value: 'SteveJobs',
  },
  {
    name: 'email',
    label: 'Email',
    value: 's.jobs@apple.com',
  },
];
export const FormFlatQ = new Form({ fields }, { name: 'Flat-Q' });
