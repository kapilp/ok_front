import { Form, Field } from '../../../../../form2/index';
import { isEmail, shouldBeEqualTo } from '../../extension/vjf';
import { vjf } from '../../../../../form2/validators/VJF';
const fields = {
  email: {
    label: 'Username',
    value: 's.jobs@apple.com',
    validators: [isEmail],
    related: ['emailConfirm'],
  },
  emailConfirm: {
    label: 'Email',
    value: 's.jobs@apple.com',
    validators: [isEmail, shouldBeEqualTo('email')],
  },
};
class NewField extends Field {
  newFieldProp = false;
  constructor(data) {
    super(data);
    this.newFieldProp = true;
  }
}
class NewForm extends Form {
  makeField(data) {
    return new NewField(data);
  }
  options() {
    return {
      validateOnChange: true,
    };
  }
  plugins() {
    return {
      vjf: vjf(),
    };
  }
  hooks() {
    return {
      onInit(form) {
        form.update({
          email: 'invalid',
        });
      },
    };
  }
}
export const FormFlatR = new NewForm({ fields }, { name: 'Flat-R' });
