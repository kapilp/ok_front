import ajv from 'ajv';
import { Form } from '../../../../../form2/index';
import { checkUser } from '../../extension/vjf';
import { svk } from '../../../../../form2/validators/SVK';
import { vjf } from '../../../../../form2/validators/VJF';
const fields = {
  username: {
    label: 'Username',
    value: 'SteveJobs',
    validators: [checkUser],
  },
  email: {
    label: 'Email',
    value: '12345',
  },
  password: {
    label: 'Password',
    value: 'thinkdifferent',
  },
};
const schema = {
  type: 'object',
  properties: {
    username: { type: 'string', minLength: 6, maxLength: 20 },
    email: { type: 'string', format: 'email', minLength: 5, maxLength: 20 },
    password: { type: 'string', minLength: 6, maxLength: 20 },
  },
};
const plugins = {
  vjf: vjf(),
  svk: svk({
    package: ajv,
    schema,
  }),
};
class NewForm extends Form {
  hooks() {
    return {
      onInit(form) {
        // subsequent clear and reset
        form.clear(); // to empty values
        form.reset(); // to const FormFlat = or initial values
      },
    };
  }
}
export const FormFlatN = new NewForm({ fields }, { plugins, name: 'Flat-N' });
