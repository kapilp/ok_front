import validatorjs from 'validatorjs';
import { Form, Field } from '../../../../../form2/index';
import { isEmail, shouldBeEqualTo } from '../../extension/vjf';
import { dvr } from '../../../../../form2/validators/DVR';
const fields = {
  email: {
    label: 'Email',
    value: '',
    rules: 'required|email',
  },
};
class NewForm extends Form {
  options() {
    return {
      validateOnInit: true,
      showErrorsOnInit: true,
    };
  }
  plugins() {
    return {
      dvr: dvr(validatorjs),
    };
  }
}
export const FormFlatS = new NewForm({ fields }, { name: 'Flat-S' });
