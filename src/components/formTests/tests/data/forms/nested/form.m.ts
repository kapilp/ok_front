import { Form } from '../../../../../form2/index';
const fields = {
  club: 'HELLO',
  members: [
    {
      firstname: 'Clint',
      lastname: 'Eastwood',
      hobbies: ['Soccer', 'Baseball'],
    },
    {
      firstname: 'Charlie',
      lastname: 'Chaplin',
      hobbies: ['Golf', 'Basket'],
    },
  ],
};
export const FormNestedM = new Form({ fields }, { name: 'Nested-M' });
