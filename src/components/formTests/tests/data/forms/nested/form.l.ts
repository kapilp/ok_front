import { Form } from '../../../../../form2/index';
const fields = {
  state: {
    city: {
      places: {
        centralPark: true,
        statueOfLiberty: false,
        empireStateBuilding: true,
        brooklynBridge: false,
      },
    },
  },
};
export const FormNestedL = new Form({ fields }, { name: 'Nested-L' });
