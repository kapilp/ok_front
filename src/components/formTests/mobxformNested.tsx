// import { assert, expect } from 'chai';
// import { FormNestedA } from '../tests/data/forms/nested/form.a';
// import { FormNestedB } from '../tests/data/forms/nested/form.b';
// import { FormNestedG } from '../tests/data/forms/nested/form.g';
// import { FormNestedH } from '../tests/data/forms/nested/form.h';
// import { FormNestedI } from '../tests/data/forms/nested/form.i';
// import { FormNestedM1 } from '../tests/data/forms/nested/form.m1';
// import { FormNestedC } from '../tests/data/forms/nested/form.c';
// import { FormNestedT1 } from '../tests/data/forms/nested/form.t1';
// import { FormNestedD } from '../tests/data/forms/nested/form.d';
// import { FormNestedE } from '../tests/data/forms/nested/form.e';
// import { FormNestedF } from '../tests/data/forms/nested/form.f';
// import { FormNestedO } from '../tests/data/forms/nested/form.o';
// import { FormNestedP } from '../tests/data/forms/nested/form.p';
// import { FormNestedQ } from '../tests/data/forms/nested/form.q';
// import { FormNestedR } from '../tests/data/forms/nested/form.r';
// import { FormNestedL } from '../tests/data/forms/nested/form.l';
// import { FormNestedM } from '../tests/data/forms/nested/form.m';
// import { FormNestedN } from '../tests/data/forms/nested/form.n';
// import { FormNestedZ } from '../tests/data/forms/nested/form.z';
// import { FormNestedS } from '../tests/data/forms/nested/form.s';
// import { FormNestedT } from '../tests/data/forms/nested/form.t';
// import { FormNestedU } from '../tests/data/forms/nested/form.u';
// export default { title: 'Mobx Nested Form' };
// export const NestedActions = () => {
//   //describe("Form error test", () => {
//   //it("$A select() should throw error", () =>
//   expect(() => FormNestedA.select('some.test')).to.throw(Error);
//   //it("$A get() should throw error", () =>
//   expect(() => FormNestedA.get('not-allowed-prop')).to.throw(Error);
//   //it("$T del() should throw error", () =>
//   expect(() => FormNestedT.$('notIncrementalFields').del(99)).to.throw(Error);
//   // });
//   // describe("Form autoFocus test", () => {
//   //it("$T notIncrementalFields autoFocus should be true", () =>
//   expect(FormNestedT.$('notIncrementalFields').autoFocus).to.be.true;
//   // });
//
//   return <div>NestedActions</div>;
// };
// export const NestedLabels = () => {
//   //describe("Check Nested $A Unified Properties (label)", () => {
//   //it('$A user.email label should be equal to "Email"', () =>
//   expect(FormNestedA.$('user.email').label).to.be.equal('Email');
//   //it('$A user.emailConfirm label should be equal to "Confirm User Email"', () =>
//   expect(FormNestedA.$('user.emailConfirm').label).to.be.equal('Confirm User Email');
//   //it('$A user.emailConfirm default should be equal to "Default Value"', () =>
//   expect(FormNestedA.$('user.emailConfirm').default).to.be.equal('Default Value');
//   // });
//   // describe("Check Nested $I Separated Properties (labels)", () => {
//   //it('$I state.city.places.centralPark label should be equal to "Central Park"', () =>
//   expect(FormNestedI.$('state.city.places.centralPark').label).to.be.equal('Central Park');
//   //it('$I state.city.places.statueOfLiberty label should be equal to "Statue of Liberty"', () =>
//   expect(FormNestedI.$('state.city.places.statueOfLiberty').label).to.be.equal('Statue of Liberty');
//   //it('$I state.city.places.empireStateBuilding label should be equal to "Empire State Building"', () =>
//   expect(FormNestedI.$('state.city.places.empireStateBuilding').label).to.be.equal('Empire State Building');
//   //it('$I state.city.places.brooklynBridge label should be equal to "Brooklyn Bridge"', () =>
//   expect(FormNestedI.$('state.city.places.brooklynBridge').label).to.be.equal('Brooklyn Bridge');
//   // });
//   // describe("Check Nested $O Separated Properties (labels)", () => {
//   //it('$O club label should be equal to "Label Club"', () =>
//   expect(FormNestedO.$('club').label).to.be.equal('Label Club');
//   //it('$O club.name label should be equal to "Label Club Name"', () =>
//   expect(FormNestedO.$('club.name').label).to.be.equal('Label Club Name');
//   //it('$O club.city label should be equal to "Label Club City"', () =>
//   expect(FormNestedO.$('club.city').label).to.be.equal('Label Club City');
//   //it('$O members label should be equal to "Label Members"', () =>
//   expect(FormNestedO.$('members').label).to.be.equal('Label Members');
//   //it('$O members[1].firstname label should be equal to "Label Member FirstName"', () =>
//   expect(FormNestedO.$('members[1].firstname').label).to.be.equal('Label Member FirstName');
//   //it('$O members[1].hobbies label should be equal to "Label Member Hobby"', () =>
//   expect(FormNestedO.$('members[1].hobbies').label).to.be.equal('Label Member Hobby');
//   // });
//
//   return <div>NestedLabels</div>;
// };
// export const NestedPerformance = () => {
//   //describe("Form performance", () => {
//   //it("DVR enabled", () => {
//   const t0 = Date.now();
//   FormNestedZ.submit();
//   const span = Date.now() - t0;
//   expect(span).lessThan(5000);
//   // });
//   // });
//
//   return <div>NestedPerformance</div>;
// };
// export const NestedProps = () => {
//   const checkDeepPropEqual = (target, prop, path, value) =>
//     //it(`${target.state.form.name} get([${prop}]) ${path} should be equal to "${value}"`, () =>
//     expect(target.get(prop)).to.have.deep.property(path, value);
//   const checkDeepPropType = (target, prop, path, type) =>
//     //it(`${target.state.form.name} get([${prop}]) ${path} should be an "${type}"`, () =>
//     expect(target.get(prop)).to.have.deep.property(path).that.is.an(type);
//   // describe("Check $A Nested Fields", () => {
//   //it('$A user.email container() path should be equal to "user"', () =>
//   expect(FormNestedA.$('user.email').container().path).to.be.equal('user');
//   //it("$A user container() should be $A", () =>
//   expect(FormNestedA.$('user').container()).to.equal(FormNestedA);
//   //it('$A user.email.value should be equal to "notAnEmail"', () =>
//   expect(FormNestedA.$('user.email').value).to.be.equal('notAnEmail');
//   //it('$A user.emailConfirm.value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormNestedA.$('user.emailConfirm').value).to.be.equal('s.jobs@apple.com');
//   //it('$A user.password error should be equal to "Password Invalid"', () =>
//   expect(FormNestedA.$('user.password').error).to.be.equal('Password Invalid');
//   //it("$A user.password hasError should be true", () =>
//   expect(FormNestedA.$('user.password').hasError).to.be.true;
//   //it("$A user.password isValid should be false", () =>
//   expect(FormNestedA.$('user.password').isValid).to.be.false;
//   //it('$A user.devSkills value should be equal to "5" (string)', () =>
//   expect(FormNestedA.$('user.devSkills').value).to.be.equal('5');
//   //it("$A user.devSkills value should be a string", () =>
//   expect(FormNestedA.$('user.devSkills').value).to.be.a('string');
//   //it("$A user.devSkills get(value) should be equal to 5 (number)", () =>
//   expect(FormNestedA.$('user.devSkills').get('value')).to.be.equal(5);
//   //it("$A user.devSkills get(value) should be a number", () =>
//   expect(FormNestedA.$('user.devSkills').get('value')).to.be.a('number');
//   //it("$A get(id) should exist", () => expect(FormNestedA.get("id").user).to.exist);
//   //it("$A get(value) should exist", () =>
//   expect(FormNestedA.get('value').user).to.be.empty;
//   // });
//   // describe("Check $B Nested Fields", () => {
//   //it("$B state.city.places id should start with state-city-places--", () =>
//   expect(FormNestedB.$('state.city.places').id.substring(0, 19)).to.be.equal('state-city-places--');
//   //it('$B state.city label should be equal to "City"', () =>
//   expect(FormNestedB.$('state.city').label).to.be.equal('City');
//   //it("$B state.city.places value should not be empty", () =>
//   expect(FormNestedB.$('state.city.places').value).to.not.be.empty;
//   //it("$B state.city.places isEmpty should be false", () =>
//   expect(FormNestedB.$('state.city.places').isEmpty).to.be.false;
//   //it('$B state.city.places label should be equal to "NY Cool Places"', () =>
//   expect(FormNestedB.$('state.city.places').label).to.be.equal('NY Cool Places');
//   //it('$B state.city.places.statueOfLiberty label should be equal to "Statue of Liberty"', () =>
//   expect(FormNestedB.$('state.city.places.statueOfLiberty').label).to.be.equal('Statue of Liberty');
//   //it("$B state.city.places.statueOfLiberty value should be false", () =>
//   expect(FormNestedB.$('state.city.places.statueOfLiberty').value).to.be.false;
//   //it("$B state.city.places.centralPark value should be false", () =>
//   expect(FormNestedB.$('state.city.places.centralPark').value).to.be.false;
//   //it("$B state.city.places.empireStateBuilding value should be false", () =>
//   expect(FormNestedB.$('state.city.places.empireStateBuilding').value).to.be.false;
//   // });
//   // describe("Check $C Nested Fields", () => {
//   //it('$C state.city label should be equal to "City"', () =>
//   expect(FormNestedC.$('state.city').label).to.be.equal('City');
//   //it("$C state.city value should be be object", () =>
//   expect(FormNestedC.$('state.city').value).to.be.an('object');
//   //it('$C state.city value should have prop "places"', () =>
//   expect(FormNestedC.$('state.city').value).to.have.property('places');
//   //it("$C state.city.places value should be object", () =>
//   expect(FormNestedC.$('state.city.places').value).to.be.an('object');
//   //it('$C state.city.places value should have prop "statueOfLiberty"', () =>
//   expect(FormNestedC.$('state.city.places').value).to.have.property('statueOfLiberty');
//   //it("$C state.city.places isEmpty should be false", () =>
//   expect(FormNestedC.$('state.city.places').isEmpty).to.be.false;
//   //it('$C state.city.places label should be equal to "NY Cool Places"', () =>
//   expect(FormNestedC.$('state.city.places').label).to.be.equal('NY Cool Places');
//   //it('$C state.city.places.statueOfLiberty label should be equal to "Statue of Liberty"', () =>
//   expect(FormNestedC.$('state.city.places.statueOfLiberty').label).to.be.equal('Statue of Liberty');
//   //it("$C state.city.places.statueOfLiberty value should be false", () =>
//   expect(FormNestedC.$('state.city.places.statueOfLiberty').value).to.be.false;
//   //it("$C state.city.places.centralPark value should be false", () =>
//   expect(FormNestedC.$('state.city.places.centralPark').value).to.be.false;
//   //it("$C state.city.places.empireStateBuilding value should be false", () =>
//   expect(FormNestedC.$('state.city.places.empireStateBuilding').value).to.be.false;
//   checkDeepPropType(FormNestedC, ['value'], 'state.fields.city.value', 'object');
//   checkDeepPropEqual(FormNestedC, ['label'], 'state.fields.city.label', 'City');
//   // });
//   // describe("Check form get() values for Nested Fields", () => {
//   checkDeepPropEqual(FormNestedB, 'value', 'state.city.places.empireStateBuilding', false);
//   checkDeepPropEqual(FormNestedC, 'value', 'state.city.places.empireStateBuilding', false);
//   checkDeepPropEqual(FormNestedC.$('state.city'), 'value', 'places.empireStateBuilding', false);
//   // });
//   // describe("Check Nested Fields path property", () => {
//   const path = {
//     a: 'user.email',
//     b: 'state.city.places',
//     c: 'state.city.places.statueOfLiberty',
//   };
//   //it(`$A ${path.a} path path should be equal to ${path.a}`, () =>
//   expect(FormNestedA.$(path.a).path).to.be.equal(path.a);
//   //it(`$B ${path.b} path path should be equal to ${path.b}`, () =>
//   expect(FormNestedB.$(path.b).path).to.be.equal(path.b);
//   //it(`$C ${path.c} path path should be equal to ${path.c}`, () =>
//   expect(FormNestedC.$(path.c).path).to.be.equal(path.c);
//   // });
//   // describe("Check Nested $C Fields computed deep check()", () => {
//   //it("$C check(isEmpty, deep=true) should be false", () =>
//   expect(FormNestedC.check('isEmpty', true)).to.be.false;
//   //it("$C check(isDirty, deep=true) should be true", () =>
//   expect(FormNestedC.check('isDirty', true)).to.be.true;
//   //it("$C .isDirty should be true", () => expect(FormNestedC.isDirty).to.be.true);
//   // to fix // fixed ?
//   //it("$C check(isPristine, deep=true) should be false", () =>
//   expect(FormNestedC.check('isPristine', true)).to.be.false;
//   // to fix // fixed ?
//   //it("$C .isPristine should be false", () =>
//   expect(FormNestedC.isPristine).to.be.false;
//   //it("$C check(isDefault, deep=true) should be false", () =>
//   expect(FormNestedC.check('isDefault', true)).to.be.false;
//   //it("$C check(hasError, deep=true) should be false", () =>
//   expect(FormNestedC.check('hasError', true)).to.be.false;
//   // });
//   // describe("Check Nested $D props after state clear()", () => {
//   //it("$D state isEmpty should be false", () =>
//   expect(FormNestedD.$('state').isEmpty).to.be.false;
//   //it("$D state.city isEmpty should be false", () =>
//   expect(FormNestedD.$('state.city').isEmpty).to.be.false;
//   //it("$D state.city.places isEmpty should be false", () =>
//   expect(FormNestedD.$('state.city.places').isEmpty).to.be.false;
//   // });
//   // describe("Check Nested $N bindings props", () => {
//   const membersFirstNameBindings = FormNestedN.$('members').$(0).$('firstname').bind();
//   //it("$N membersFirstNameBindings floatingLabelText should be equal to empty string", () =>
//   expect(membersFirstNameBindings).to.have.property('floatingLabelText', 'First Name Label');
//   //it('$N membersFirstNameBindings value should be equal to "Clint"', () =>
//   expect(membersFirstNameBindings).to.have.property('value', 'Clint');
//   //it('$N membersFirstNameBindings hintText should be equal to "Insert First Name"', () =>
//   expect(membersFirstNameBindings).to.have.property('hintText', 'Insert First Name');
//   const membersLastNameBindings = FormNestedN.$('members').$(0).$('lastname').bind({ value: 'Hello!!!' });
//   //it('$N membersLastNameBindings value should be equal to "Hello!!!"', () =>
//   expect(membersLastNameBindings).to.have.property('value', 'Hello!!!');
//   const hobbiesBindings = FormNestedN.$('members').$(1).$('hobbies').$(0).bind();
//   //it("$N hobbiesBindings floatingLabelText should be equal to empty string", () =>
//   expect(hobbiesBindings).to.have.property('floatingLabelText', '');
//   //it('$N hobbiesBindings value should be equal to "Golf"', () =>
//   expect(hobbiesBindings).to.have.property('value', 'Golf');
//   //it('$N hobbiesBindings hintText should be equal to "Insert Hobbies"', () =>
//   expect(hobbiesBindings).to.have.property('hintText', 'Insert Hobbies');
//   // });
//   // describe("Check Nested $S get() value after set() value", () => {
//   checkDeepPropEqual(FormNestedS, ['value'], 'club.fields.name.value', 'club-name-set-value-intercepted');
//   checkDeepPropEqual(FormNestedS, ['value'], 'club.fields.city.value', 'club-city-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.0.fields.firstname.value', 'members-0-firstname-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.0.fields.lastname.value', 'members-0-lastname-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.0.fields.hobbies.fields.0.value', 'members-0-hobbies-0-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.0.fields.hobbies.fields.1.value', 'members-0-hobbies-1-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.1.fields.firstname.value', 'members-1-firstname-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.1.fields.lastname.value', 'members-1-lastname-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.1.fields.hobbies.fields.0.value', 'members-1-hobbies-0-set-value');
//   checkDeepPropEqual(FormNestedS, ['value'], 'members.fields.1.fields.hobbies.fields.1.value', 'members-1-hobbies-1-set-value');
//   // });
//   // describe("Check Nested $T add() and del()", () => {
//   //it("$T hobbies fields.size should be equal to 2", () =>
//   expect(FormNestedT.$('hobbies').fields.size).to.equal(2);
//   //it("$T member.hobbies fields.size should be equal to 2", () =>
//   expect(FormNestedT.$('member.hobbies').fields.size).to.equal(2);
//   // });
//   // describe("Check Nested $T value on add()", () => {
//   //it('$T member.hobbies container() path should be equal to "member"', () =>
//   expect(FormNestedT.$('member.hobbies').container().path).to.equal('member');
//   //it("$T member container() path should be $T", () =>
//   expect(FormNestedT.$('member').container()).to.equal(FormNestedT);
//   //it('$T container(member.hobbies) path should be equal to "member"', () =>
//   expect(FormNestedT.container('member.hobbies').path).to.equal('member');
//   //it('$T member.hobbies.1 container() path should be equal to "member.hobbies"', () =>
//   expect(FormNestedT.$('member.hobbies').$(1).container().path).to.equal('member.hobbies');
//   //it('$T container(member.hobbies.1) path should be equal to "member.hobbies"', () =>
//   expect(FormNestedT.container('member.hobbies.1').path).to.equal('member.hobbies');
//   //it('$T member container(hobbies.1) path should be equal to "member.hobbies"', () =>
//   expect(FormNestedT.$('member').container('hobbies.1').path).to.equal('member.hobbies');
//   //it('$T member container(hobbies[1]) path should be equal to "member.hobbies"', () =>
//   expect(FormNestedT.$('member').container('hobbies[1]').path).to.equal('member.hobbies');
//   //it("$T member.hobbies value should be array", () =>
//   expect(FormNestedT.$('member.hobbies').value).to.be.instanceof(Array);
//   //it("$T member.hobbies value should have length of 2", () =>
//   expect(FormNestedT.$('member.hobbies').value).to.have.lengthOf(2);
//   //it('$T member.hobbies[1] initial should be equal to "BBB"', () =>
//   expect(FormNestedT.$('member.hobbies[1]').initial).to.equal('BBB');
//   //it('$T member.hobbies[1] default should be equal to "BBB"', () =>
//   expect(FormNestedT.$('member.hobbies[1]').default).to.equal('BBB');
//   // it('$T member.info[0] default should be an object', () =>
//   //   expect(FormNestedT.$('member.info[0]').default).to.be.an('object');
//   // it('$T member.info[0] initial should be an object', () =>
//   //   expect(FormNestedT.$('member.info[0]').initial).to.be.an('object');
//   //it("$T member.info[0] value should be an object", () =>
//   expect(FormNestedT.$('member.info[0]').value).to.be.an('object');
//   //it('$T member.info[0].firstname value should be equal to "AAA"', () =>
//   expect(FormNestedT.$('member.info[0].firstname').value).to.equal('AAA');
//   //it('$T member.info[0].lastname value should be equal to "BBB"', () =>
//   expect(FormNestedT.$('member.info[0].lastname').value).to.equal('BBB');
//   //it('$T member.info[0].type value should be equal to "XXX"', () =>
//   expect(FormNestedT.$('member.info[0].type').value).to.equal('XXX');
//   //it('$T member.info[1].type value should be equal to "XXX-2"', () =>
//   expect(FormNestedT.$('member.info[1].type').value).to.equal('XXX-2');
//   //it('$T member.info[2].type value should be equal to "YYY"', () =>
//   expect(FormNestedT.$('member.info[2].type').value).to.equal('YYY');
//   //it('$T member.data[0].update value should be equal to "update-value"', () =>
//   expect(FormNestedT.$('member.data[0].update').value).to.equal('update-value');
//   //it('$T member.data[0].subzero value should be equal to "subzero-value"', () =>
//   expect(FormNestedT.$('member.data[0].subzero').value).to.equal('subzero-value');
//   //it('$T member.data[0].subzero label should be equal to "subzero-label"', () =>
//   expect(FormNestedT.$('member.data[0].subzero').label).to.equal('subzero-label');
//   //it("$T notIncrementalFields fields.get(notIncrementalKey) should be an object", () =>
//   expect(FormNestedT.$('notIncrementalFields').fields.get('notIncrementalKey')).to.be.an('object');
//   //it('$T notIncrementalFields[notIncrementalKey] value should be equal to "XXX"', () =>
//   expect(FormNestedT.$('notIncrementalFields[notIncrementalKey]').value).to.equal('XXX');
//   //it('$T notIncrementalFields[notIncrementalKey] label should be equal to "XXX-label"', () =>
//   expect(FormNestedT.$('notIncrementalFields[notIncrementalKey]').label).to.equal('XXX-label');
//   //it("$T notIncrementalFields .fields.size should be equal to 1", () =>
//   expect(FormNestedT.$('notIncrementalFields').fields.size).to.equal(1);
//   // });
//   // describe("Check $T1 Nested Fields", () => {
//   //it("$T1 state.options.get(softDelete) should be true", () =>
//   expect(FormNestedT1.state.options.get('softDelete')).to.be.true;
//   //it("$T1 hobbies .fields.size should be equal to 3", () =>
//   expect(FormNestedT1.$('hobbies').fields.size).to.equal(3);
//   //it("$T1 hobbies[0].deleted should be false", () =>
//   expect(FormNestedT1.$('hobbies[0]').deleted).to.be.false;
//   //it("$T1 hobbies[1].deleted should be true", () =>
//   expect(FormNestedT1.$('hobbies[1]').deleted).to.be.true;
//   //it("$T1 hobbies[2].deleted should be false", () =>
//   expect(FormNestedT1.$('hobbies[2]').deleted).to.be.false;
//   //it("$T1 get(value) value to be deep equal", () =>
//   expect(FormNestedT1.get('value')).to.be.deep.equal({
//     hobbies: ['AAA', 'CCC'],
//   });
//   // });
//   // describe("Check $U Nested Fields", () => {
//   //it('$U user.email value should be equal to "notAnEmail"', () =>
//   expect(FormNestedU.$('user.email').value).to.be.equal('notAnEmail');
//   //it('$U user.emailConfirm value should be equal to "s.jobs@apple.com"', () =>
//   expect(FormNestedU.$('user.emailConfirm').value).to.be.equal('s.jobs@apple.com');
//   // });
//
//   return <div>NestedProps</div>;
// };
// export const NestedState = () => {
//   //describe("Form state extra", () => {
//   //it('$A state.$extra should have "foo" property', () =>
//   expect(FormNestedA.state.$extra).to.have.property('foo');
//   //it('$A state.$extra "foo" prop should be "bar"', () =>
//   expect(FormNestedA.state.extra('foo')).to.be.equal('bar');
//   //it("$B state.extra() should be array", () =>
//   expect(FormNestedB.state.extra()).to.be.instanceof(Array);
//   // });
//
//   return <div>NestedState</div>;
// };
// export const NestedSubmit = () => {
//   //describe("Nested Form Manual submit()", () => {
//   // $R
//   //it("$R.submit() should call onSuccess callback", done => {
//   FormNestedR.$('members')
//     .submit()
//     .then(instance => {
//       expect(instance.submitted).to.equal(1);
//       expect(instance.hasError).to.be.false; // eslint-disable-line
//       expect(instance.isValid).to.be.true; // eslint-disable-line
//       done();
//     });
//   // });
//   // });
//
//   return <div>NestedSubmit</div>;
// };
// export const NestedValidation = () => {
//   // $A
//   // describe("$A.validate()", () => {
//   //it("$A.validate()", done => {
//   FormNestedA.validate().then(form => {
//     expect(form.validated).to.equal(1);
//     expect(form.isValid).to.be.false;
//     done();
//   });
//   // });
//   // });
//   // describe("Check Nested $A validation", () => {
//   //it("$A user.emailConfirm isValid should be false", () =>
//   expect(FormNestedA.$('user.emailConfirm').isValid).to.be.false;
//   //it("$A user.emailConfirm error should be null", () =>
//   expect(FormNestedA.$('user.emailConfirm').error).to.be.null;
//   // });
//   // describe("Check Nested $U validation", () => {
//   //it("$U user.emailConfirm isValid should be false", () =>
//   expect(FormNestedU.$('user.emailConfirm').isValid).to.be.false;
//   //it("$U user.emailConfirm error should be null", () =>
//   expect(FormNestedU.$('user.emailConfirm').error).to.be.null;
//   // });
//   // describe("Check Nested $E validation", () => {
//   //it("$E state isValid should be false", () =>
//   expect(FormNestedE.$('state').isValid).to.be.false;
//   //it("$E state.city isValid should be false", () =>
//   expect(FormNestedE.$('state.city').isValid).to.be.false;
//   // });
//   // describe("Check Nested $F validation", () => {
//   //it("$F state isValid should be false", () =>
//   expect(FormNestedF.$('state').isValid).to.be.false;
//   //it("$F state.city isValid should be false", () =>
//   expect(FormNestedF.$('state.city').isValid).to.be.false;
//   //it("$F state.city.places isValid should be false", () =>
//   expect(FormNestedF.$('state.city.places').isValid).to.be.false;
//   // });
//   // describe("Check Nested $G validation", () => {
//   //it("$G state isValid should be false", () =>
//   expect(FormNestedG.$('state').isValid).to.be.false;
//   //it("$G state.city isValid should be false", () =>
//   expect(FormNestedG.$('state.city').isValid).to.be.false;
//   //it("$G state.city.places isValid should be false", () =>
//   expect(FormNestedG.$('state.city.places').isValid).to.be.false;
//   // });
//   // describe("Check Nested $H validation", () => {
//   //it("$H state isValid should be false", () =>
//   expect(FormNestedH.$('state').isValid).to.be.false;
//   //it("$H state.city isValid should be false", () =>
//   expect(FormNestedH.$('state.city').isValid).to.be.false;
//   //it("$H state.city.places isValid should be false", () =>
//   expect(FormNestedH.$('state.city.places').isValid).to.be.false;
//   // });
//   // describe("Check Nested $M1 validation", () => {
//   //it("$M1 club isValid should be false", () =>
//   // F
//   expect(FormNestedM1.$('club').isValid).to.be.false;
//   //it("$M1 club error should be equal to", () =>
//   // ERR
//   expect(FormNestedM1.$('club').error).to.be.equal('The Club is a required field');
//   //it("$M1 members[0].firstname isValid should be false", () =>
//   // F
//   expect(FormNestedM1.$('members[0].firstname').isValid).to.be.false;
//   //it("$M1 members.0.firstname error should be equal to", () =>
//   // ERR
//   expect(FormNestedM1.$('members.0.firstname').error).to.be.equal('The First Name is a required field');
//   //it("$M1 members[0].lastname isValid should be true", () =>
//   expect(FormNestedM1.$('members[0].lastname').isValid).to.be.true;
//   //it("$M1 members[0].yearOfBirth isValid should be true", () =>
//   expect(FormNestedM1.$('members[0].yearOfBirth').isValid).to.be.true;
//   //it("$M1 members[1].yearOfBirth isValid should be false", () =>
//   // F
//   expect(FormNestedM1.$('members[1].yearOfBirth').isValid).to.be.false;
//   //it("$M1 members[1].yearOfBirth error should be equal to", () =>
//   // ERR
//   expect(FormNestedM1.$('members[1].yearOfBirth').error).to.be.equal('The Year of Birth must be a `number` type, but the final value was: `""`.');
//   //it("$M1 members[1].hobbies[0] isValid should be true", () =>
//   expect(FormNestedM1.$('members[1].hobbies[0]').isValid).to.be.true;
//   //it("$M1 members[1].hobbies[1] isValid should be false", () =>
//   // F
//   expect(FormNestedM1.$('members[1].hobbies[1]').isValid).to.be.false;
//   //it("$M1 members[1].hobbies[1] error should be equal to", () =>
//   // ERR
//   expect(FormNestedM1.$('members[1].hobbies[1]').error).to.be.equal('The Hobbie is a required field');
//   // });
//   // describe("Check Nested $R validation", () => {
//   //it("$R email isValid should be true", () =>
//   expect(FormNestedR.$('email').isValid).to.be.true;
//   //it("$R club.name isValid should be true", () =>
//   expect(FormNestedR.$('club.name').isValid).to.be.true;
//   //it("$R club.city isValid should be true", () =>
//   expect(FormNestedR.$('club.city').isValid).to.be.true;
//   //it("$R members[1].firstname isValid should be true", () =>
//   expect(FormNestedR.$('members[1].firstname').isValid).to.be.true;
//   //it("$R members[1].lastname isValid should be true", () =>
//   expect(FormNestedR.$('members[1].lastname').isValid).to.be.true;
//   //it("$R members[1].hobbies[0] isValid should be true", () =>
//   expect(FormNestedR.$('members[1].hobbies[1]').isValid).to.be.true;
//   //it("$R members[1].hobbies[1] isValid should be true", () =>
//   expect(FormNestedR.$('members[1].hobbies[1]').isValid).to.be.true;
//   // });
//   // describe("Check Nested $S validation", () => {
//   //it("$S club.name isValid should be false", () =>
//   expect(FormNestedS.$('club.name').isValid).to.be.false;
//   //it("$S club.city isValid should be false", () =>
//   expect(FormNestedS.$('club.city').isValid).to.be.false;
//   //it("$S members[1].firstname isValid should be false", () =>
//   expect(FormNestedS.$('members[1].firstname').isValid).to.be.false;
//   //it("$S members[1].lastname isValid should be false", () =>
//   expect(FormNestedS.$('members[1].lastname').isValid).to.be.false;
//   //it("$S members[1].hobbies[0] isValid should be false", () =>
//   expect(FormNestedS.$('members[1].hobbies[1]').isValid).to.be.false;
//   //it("$S members[1].hobbies[1] isValid should be false", () =>
//   expect(FormNestedS.$('members[1].hobbies[1]').isValid).to.be.false;
//   // });
//
//   return <div>NestedValiation</div>;
// };
// export const NestedValues = () => {
//   //describe("Check Nested $F Unified Properties (values)", () => {
//   //it("$F state.city id should be a string", () =>
//   expect(FormNestedF.$('state.city').id).to.be.a('string');
//   //it("$F state.city value should be an object", () =>
//   expect(FormNestedF.$('state.city').value).to.be.an('object');
//   //it("$F state.city value should be an object", () =>
//   expect(FormNestedF.$('state.city').value).to.have.property('places');
//   //it("$F state.city.places value should be an object", () =>
//   expect(FormNestedF.$('state.city.places').value).to.be.an('object');
//   //it("$F state.city.places value should be an object", () =>
//   expect(FormNestedF.$('state.city.places').value).to.have.property('statueOfLiberty');
//   // });
//   // describe("Check Nested $I Separated Properties (values)", () => {
//   //it("$I state.city.places.centralPark value should be true", () =>
//   expect(FormNestedI.$('state.city.places.centralPark').value).to.be.true;
//   //it("$I state.city.places.statueOfLiberty value should be false", () =>
//   expect(FormNestedI.$('state.city.places.statueOfLiberty').value).to.be.false;
//   //it("$I state.city.places.empireStateBuilding value should be true", () =>
//   expect(FormNestedI.$('state.city.places.empireStateBuilding').value).to.be.true;
//   //it("$I state.city.places.brooklynBridge value should be false", () =>
//   expect(FormNestedI.$('state.city.places.brooklynBridge').value).to.be.false;
//   // });
//   // describe("Check Nested $L Values", () => {
//   //it("$L state.city.places.centralPark value should be true", () =>
//   expect(FormNestedL.$('state.city.places.centralPark').value).to.be.true;
//   //it("$L state.city.places.statueOfLiberty value should be false", () =>
//   expect(FormNestedL.$('state.city.places.statueOfLiberty').value).to.be.false;
//   //it("$L state.city.places.empireStateBuilding value should be true", () =>
//   expect(FormNestedL.$('state.city.places.empireStateBuilding').value).to.be.true;
//   //it("$L state.city.places.brooklynBridge value should be false", () =>
//   expect(FormNestedL.$('state.city.places.brooklynBridge').value).to.be.false;
//   // });
//   // describe("Check Nested $M Values", () => {
//   //it('$M club value should be equal to "HELLO"', () =>
//   expect(FormNestedM.$('club').value).to.be.equal('HELLO');
//   //it('$M members[0].firstname value should be equal to "Clint"', () =>
//   expect(FormNestedM.$('members[0].firstname').value).to.be.equal('Clint');
//   //it('$M members[1].firstname value should be equal to "Charlie"', () =>
//   expect(FormNestedM.$('members[1].firstname').value).to.be.equal('Charlie');
//   //it('$M members[0].hobbies[1] value should be equal to "Baseball"', () =>
//   expect(FormNestedM.$('members[0].hobbies[1]').value).to.be.equal('Baseball');
//   //it('$M members[1].hobbies[0] value should be equal to "Golf"', () =>
//   expect(FormNestedM.$('members[1].hobbies[0]').value).to.be.equal('Golf');
//   //it('$M members[1].hobbies[1] value should be equal to "Basket"', () =>
//   expect(FormNestedM.$('members[1].hobbies[1]').value).to.be.equal('Basket');
//   // });
//   // describe("Check Nested $N Values", () => {
//   //it('$N nested.field value should be equal to "5" (string)', () =>
//   expect(FormNestedN.$('nested.field').value).to.be.equal('5');
//   //it("$N nested.field value should be a string", () =>
//   expect(FormNestedN.$('nested.field').value).to.be.a('string');
//   //it("$N nested.field get(value) should be equal to 5 (number)", () =>
//   expect(FormNestedN.$('nested.field').get('value')).to.be.equal(5);
//   //it("$N nested.field get(value) should be a number", () =>
//   expect(FormNestedN.$('nested.field').get('value')).to.be.a('number');
//   //it("$N club value should be equal to empty object", () =>
//   expect(FormNestedN.$('club').value).to.deep.equal({ name: '', city: '' });
//   //it('$N members.0.firstname value should be equal to "Clint"', () =>
//   expect(FormNestedN.$('members.0.firstname').value).to.be.equal('Clint');
//   //it("$N members.1.firstname value should be empty", () =>
//   expect(FormNestedN.$('members.1.firstname').value).to.be.empty;
//   //it('$N members.0.hobbies.1 value should be equal to "Baseball"', () =>
//   expect(FormNestedN.$('members.0.hobbies.1').value).to.be.equal('Baseball');
//   //it('$N members.1.hobbies.0 value should be equal to "Golf"', () =>
//   expect(FormNestedN.$('members.1.hobbies.0').value).to.be.equal('Golf');
//   //it('$N members.1.hobbies.1 value should be equal to "Basket"', () =>
//   expect(FormNestedN.$('members.1.hobbies').$(1).value).to.be.equal('Basket');
//   // });
//   // describe("Check Nested $O value computed check", () => {
//   //it("$O members[1].hobbies value should be array", () =>
//   expect(FormNestedO.$('members[1].hobbies').value).to.be.array;
//   //it("$O members[1].hobbies value should be length of 2", () =>
//   expect(FormNestedO.$('members[1].hobbies').value).to.have.lengthOf(2);
//   // });
//   // describe("Check Nested $O Values", () => {
//   //it("$O club.name value should be equal to be empty", () =>
//   expect(FormNestedO.$('club.name').value).to.be.empty;
//   //it('$O club.city value should be equal to "New York"', () =>
//   expect(FormNestedO.$('club.city').value).to.be.equal('New York');
//   //it('$O members[0].firstname value should be equal to "Clint"', () =>
//   expect(FormNestedO.$('members[0].firstname').value).to.be.equal('Clint');
//   //it("$O members[1].firstname value should be empty", () =>
//   expect(FormNestedO.$('members[1].firstname').value).to.be.empty;
//   //it('$O members[1].lastname value should be equal to "Chaplin"', () =>
//   expect(FormNestedO.$('members[1].lastname').value).to.be.equal('Chaplin');
//   //it('$O members[0].hobbies[1] value should be equal to "Baseball"', () =>
//   expect(FormNestedO.$('members[0].hobbies[1]').value).to.be.equal('Baseball');
//   //it('$O members[1].hobbies[0] value should be equal to "Golf"', () =>
//   expect(FormNestedO.$('members[1].hobbies[0]').value).to.be.equal('Golf');
//   //it('$O members[1].hobbies[1] value should be equal to "Basket"', () =>
//   expect(FormNestedO.$('members[1].hobbies[1]').value).to.be.equal('Basket');
//   // });
//   // describe("Check Nested $P Values after reset", () => {
//   //it("$P club.name value should be empty", () =>
//   expect(FormNestedP.$('club.name').value).to.be.empty;
//   //it("$P members[0].firstname value should be empty", () =>
//   expect(FormNestedP.$('members[0].firstname').value).to.be.empty;
//   //it("$P members[1].firstname value should be empty", () =>
//   expect(FormNestedP.$('members[1].firstname').value).to.be.empty;
//   //it("$P members[0].hobbies[1] value should be empty", () =>
//   expect(FormNestedP.$('members[0].hobbies[1]').value).to.be.empty;
//   //it("$P members[1].hobbies[0] value should be empty", () =>
//   expect(FormNestedP.$('members[1].hobbies[0]').value).to.be.empty;
//   //it("$P members[1].hobbies[1] value should be empty", () =>
//   expect(FormNestedP.$('members[1].hobbies[1]').value).to.be.empty;
//   // });
//   // describe("Check Nested $R values()", () => {
//   //it("$R values() should be empty object", () =>
//   expect(FormNestedR.values()).to.be.deep.equal(FormNestedR.initials());
//   //it("$R (members).value should be equal to (members).initial", () =>
//   expect(FormNestedR.$('members').value).to.be.deep.equal(FormNestedR.$('members').initial);
//   // });
//   // describe("Check Nested $Q Values after reset", () => {
//   //it('$Q club.name value should be equal to "HELLO"', () =>
//   expect(FormNestedQ.$('club.name').value).to.be.equal('HELLO');
//   //it('$Q club.city value should be equal to "NY"', () =>
//   expect(FormNestedQ.$('club.city').value).to.be.equal('NY');
//   //it('$Q members[1].firstname value should be equal to "Charlie"', () =>
//   expect(FormNestedQ.$('members[1].firstname').value).to.be.equal('Charlie');
//   //it('$Q members[1].hobbies[1] value should be equal to "Basket"', () =>
//   expect(FormNestedQ.$('members[1].hobbies[1]').value).to.be.equal('Basket');
//   // });
//   // describe("Check Nested $S Values", () => {
//   //it('$S club.name value should be equal to "club-name-set-value-intercepted"', () =>
//   expect(FormNestedS.$('club.name').value).to.be.equal('club-name-set-value-intercepted');
//   //it('$S club.city value should be equal to "club-city-set-value"', () =>
//   expect(FormNestedS.$('club.city').value).to.be.equal('club-city-set-value');
//   //it("$S club.bouncer value should be empty string", () =>
//   expect(FormNestedS.$('club.bouncer').value).to.equal('');
//   // });
//
//   return <div>NestedValues</div>;
// };
// // export const NestedComputed = () => {
// //   flat.isValid($flat);
// //   flat.hasError($flat);
// //   flat.isDirty($flat);
// //   flat.isEmpty($flat);
// //   flat.isPristine($flat);
// //   flat.isPristine($flat);
// //   nested.isValid($nested);
// //
// //   return <div>NestedComputed</div>;
// // };
