// import { observable, isObservableProp } from 'mobx';
import { model, Model } from '../model';
import { isObservableProp } from 'mobx';
import {    } from 'solid-js';
import { createStore } from 'solid-js/store';

describe('My Test', () => {
  class D {
    a = 90;
    get abc() {
      return this.a;
    }
    set abc(b) {
      this.a = b + 100;
    }
  }
  it('assign test', () => {
    const m = new D();
    expect(m.abc).toBe(90);
    Object.assign(m, { abc: 500 });
    expect(m.abc).toBe(600);
  });

  type Theme = {
    id: string;
    name: string;
    primaryColor: string;
    textColor: string;
  };

  it('store test', () => {
    const themes = [{ id: 1 }, { id: 10 }];
    // very nice pattern:
    const [store, setStore] = createStore({
      themes,
      theme: themes[0],
      changeTheme: (theme: Theme) => {
        setStore({ theme });
      },
      serialize() {
        return {
          selectedTheme: store.theme.id,
        };
      },
      deserialize(data) {
        store.changeThemeById(data.selectedTheme);
      },
    });
    expect(store.theme.id).toBe(1);
    store.changeTheme('power');
    expect(store.theme).toBe('power');
  });
});

describe('Model', () => {
  describe('constructor', () => {
    it('sets properties', () => {
      class M extends Model {
        hello: string; //@observable
      }

      const m = new M({ hello: 'world' });
      expect(m.state.hello).toBe('world');
    });

    it('calls #parse when setting attributes', () => {
      class M extends Model {
        hello: string; // @observable

        parse() {
          return { hello: 'sucker' };
        }
      }

      const m = new M({ hello: 'world' }, { parse: true });
      expect(m.state.hello).toBe('sucker');
    });
  });

  describe('#set', () => {
    it('calls #parse when options.parse = true', () => {
      class M extends Model {
        hello: string; // @observable

        parse() {
          return { hello: 'sucker' };
        }
      }

      const m = new M();
      expect(m.set({ hello: 'world' }, { parse: true })).toBe(m);
      expect(m.state.hello).toBe('sucker');
    });

    it('does not #parse when no parse option is given', () => {
      class M extends Model {
        hello: string; // @observable

        parse() {
          return { hello: 'sucker' };
        }
      }

      const m = new M();
      expect(m.set({ hello: 'world' })).toBe(m);
      expect(m.state.hello).toBe('world');
    });

    it('strips out undefined values', () => {
      class M extends Model {
        hello: string; // @observable
      }

      const m = new M({ hello: 'world' }, { stripUndefined: true });
      expect(m.set({ hello: undefined }, { stripUndefined: true })).toBe(m);
      expect(m.state.hello).toBe('world');
    });
  });

  describe('#pick', () => {
    it('returns a subset of properties', () => {
      class M extends Model {
        hello: string; // @observable
        world: string; // @observable
      }

      const m = new M({ hello: 'world', world: 'hello' });
      const picked = m.pick(['hello']);
      expect(picked.hello).toBe('world');
      expect('world' in picked).toBeFalsy();
    });
  });
});
// Below Tests are not working.....
describe('model', () => {
  it('returns a new model when nothing is specified', () => {
    const m = model();
    m.set({ hello: 'world' });
    expect((m as any).hello).toBe('world');
  });

  it('enhances the specified object', () => {
    const existing = { hello: 'guys' };
    const m = model(existing);
    m.set({ hello: 'world' });
    expect(existing.hello).toBe('world');
  });

  describe('extendObservable', () => {
    it('works', () => {
      const m = model().extendObservable({
        hello: 'world',
        get world() {
          return 1337;
        },
      });
      expect(m.hello).toBe('world');
      expect(m.world).toBe(1337);
      expect(isObservableProp(m, 'hello')).toBeTruthy();
    });
  });

  describe('withActions', () => {
    it('attaches actions', () => {
      const m = model()
        .assign({
          cid: 'hey',
        })
        .extendObservable({
          hello: 'people',
        })
        .withActions({
          setHello(this: any, s: string) {
            this.hello = s;
          },
        });

      m.setHello('world');
      expect(m.hello).toBe('world');
      expect(m.cid).toBe('hey');
    });

    it('fails when not a function', () => {
      expect(() => model().withActions({ hello: 'haha' })).toThrow(/hello.*string/i);
    });
  });

  describe('decorate', () => {
    it('invokes the function', () => {
      const m = model()
        .assign({ hello: 'people' })
        .decorate(t => {
          const v = {
            validate() {
              return t.hello === 'world';
            },
          };
          Object.assign(t, v);
          return null! as typeof v;
        });
      expect(m.validate()).toBe(false);
      m.set({ hello: 'world' });
      expect(m.validate()).toBe(true);
    });
  });
});
