// import { observable, IObservableArray, action } from 'mobx'
// import { referenceOne, referenceMany } from '.'
// import { moveItem } from 'mobx-utils'
import {  createSignal,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { referenceMany, referenceOne } from './normalization';

import map from 'lodash/map';
import filter from 'lodash/filter';
import find from 'lodash/find';
import some from 'lodash/some';
import every from 'lodash/every';
import reduce from 'lodash/reduce';
import chunk from 'lodash/chunk';
import forEach from 'lodash/forEach';
import orderBy from 'lodash/orderBy';

/**
 * Used in various Lodash functions such as `map`, `filter`..
 */
export interface IIteratee<T, M> {
  (input: T, index: number): M;
}

/**
 * Indexed collection used for reusing models.
 */
export interface ICollection<T> {
  /**
   * The actual item map.
   */
  items: () => Array<T>;
  /**
   * Getter for the items length.
   */
  length: number;
  /**
   * Adds one or more models to the end of the collection.
   */
  add(models: T | T[]): this;
  /**
   * Multi-version of `create`.
   */
  create(data: any[], createOpts?: ICollectionOptions<T>): T[];
  /**
   * Like `set`, but will add regardless of whether an id is present or not.
   * This has the added risk of resulting multiple model instances if you don't make sure
   * to update the existing model once you do have an id. The model id is what makes the whole
   * one-instance-per-entity work.
   */
  create(data: any, createOpts?: ICollectionOptions<T>): T;
  /**
   * Gets items by ids.
   */
  get(id: any[]): Array<T | undefined>;
  /**
   * Gets a item by id.
   */
  get(id: any): T | undefined;
  /**
   * Same as the singular version, but with multiple.
   */
  set<D>(data?: D[], setOpts?: ICollectionOptions<T>): D extends undefined ? undefined : T[];
  /**
   * Adds a single item and maps it using the mapper in the options.
   */
  set<D>(data?: D, setOpts?: ICollectionOptions<T>): D extends undefined ? undefined : T;
  /**
   * Clears the collection.
   */
  clear(): this;
  /**
   * Maps over the items.
   */
  map<M>(iteratee: IIteratee<T, M>): M[];
  /**
   * Filters the items.
   */
  filter(iteratee: IIteratee<T, boolean>): T[];
  /**
   * Determines if there are any items that match the predicate.
   */
  some(iteratee: IIteratee<T, boolean>): boolean;
  /**
   * Determines if all items match the predicate.
   */
  every(iteratee: IIteratee<T, boolean>): boolean;
  /**
   * Chunks the collection.
   */
  chunk(size?: number): Array<Array<T>>;
  /**
   * Reduces on the items.
   */
  reduce<R>(iteratee: IIteratee<T, R>, seed?: R): R;
  /**
   * Finds a particular item.
   */
  find(iteratee: IIteratee<T, boolean>): T | undefined;
  /**
   * Orders the items based on iteratees and orders.
   */
  orderBy(iteratees: Array<IIteratee<T, any> | Object | string>, orders?: Array<boolean | string>): T[];
  /**
   * Removes an item based on ID or the item itself.
   */
  remove(modelOrId: T | string): this;
  /**
   * Slices the array.
   */
  slice(start?: number, end?: number): T[];
  /**
   * Moves an item from one index to another, using MobX's `move`.
   */
  move(fromIndex: number, toIndex: number): this;
  /**
   * Returns the item at the specified index.
   */
  at(index: number): T | undefined;
  /**
   * Runs a `forEach` over the items and returns the collection.
   */
  forEach(iteratee: IIteratee<T, any>): this;
  /**
   * Given a single or list of ids and a collection with models,
   * returns the model(s) the IDs represent. If `field` is specified, it
   * will be used instead of the source collection model ID.
   * Only the first matching model per ID is returned.
   * For "one/many-to-many" type references, use `referenceMany`.
   *
   * @param {Array<string>} ids
   * @param {keyof T} field
   */
  referenceOne<K extends keyof T>(id: ModelId): T | null;
  referenceOne<K extends keyof T>(ids: Array<ModelId>): Array<T>;
  referenceOne<K extends keyof T>(id: T[K], field?: keyof T): T | null;
  referenceOne<K extends keyof T>(ids: Array<T[K]>, field?: keyof T): Array<T>;
  referenceOne<K extends keyof T>(ids: T[K] | Array<T[K]>, field?: keyof T): T | null | Array<T>;
  /**
   * Given a single or list of ids and a collection with models,
   * returns the models that match `field`.
   * All matching models are returned and flattened.
   * For "one-to-one" type references, use `referenceOne`.
   *
   * @param {Array<string>} ids
   * @param {keyof T} field
   */
  referenceMany<K extends keyof T>(ids: T[K] | Array<T[K]>, field: keyof T): Array<T>;
}

/**
 * Called when the collection wants to add a new item.
 */
export interface ICreateFunc<T> {
  (input: any, opts: ICollectionOptions<T>): T;
}

/**
 * Called when the collection wants to update an existing item.
 */
export interface IUpdateFunc<T> {
  (existing: T, input: any, opts: ICollectionOptions<T>): T;
}

/**
 * Function used for getting the ID of a particular input.
 */
export interface IGetId<T> {
  // Returned value will be stringified.
  (obj: T, opts: ICollectionOptions<T>): any;
}

/**
 * Collection options.
 */
export interface ICollectionOptions<T> {
  /**
   * May be used by callbacks like `getModelId` and `getDataId`
   */
  idAttribute?: string;
  /**
   * Called when the collection wants to add a new item.
   */
  create?: ICreateFunc<T>;
  /**
   * Called when the collection wants to update an existing item with more data.
   */
  update?: IUpdateFunc<T>;
  /**
   * Used to get the ID of a model that was passed in. Whatever is returned from
   * this should be coercible to a string, and is used for indexing.
   */
  getModelId?: IGetId<T>;
  /**
   * Used to get an ID from input data, used to determine whether to create
   * or update.
   */
  getDataId?: IGetId<any>;
}

/**
 * Default collection options.
 */
export const defaults: ICollectionOptions<any> = {
  create: (input, opts) => input,
  getDataId: (input: any, opts) => input[opts.idAttribute || 'id'],
  getModelId: (existing, opts) => existing.state?.[opts.idAttribute || 'id'] || existing[opts.idAttribute || 'id'],
  idAttribute: 'id',
  update: (existing, input, opts) => Object.assign(existing, input),
};

/**
 * Possible model IDs.
 */
export type ModelId = string | number | Date;

/**
 * Creates a collection.
 *
 * @type {T} The item type.
 * @type {O} Additional options.
 */
export function collection<T>(opts?: ICollectionOptions<T>): ICollection<T> {
  opts = Object.assign({}, defaults, opts);
  // Holds the actual items.
  const [state, setState] = createStore({ items: [] as Array<T> });
  const idMap = new Map<string, T>();

  const self = {
    state,
    setState,
    items: () => state.items, // use function because items is not always same reference.
    add, // : action(add),
    get,
    set, //: action(set),
    create, //: action(create),
    remove, //: action(remove),
    clear, //: action(clear),
    filter: bindLodashFunc(() => state.items, filter),
    some: bindLodashFunc(() => state.items, some),
    every: bindLodashFunc(() => state.items, every),
    find: bindLodashFunc(() => state.items, find),
    orderBy: bindLodashFunc(() => state.items, orderBy),
    map: bindLodashFunc(() => state.items, map),
    reduce: bindLodashFunc(() => state.items, reduce),
    chunk: bindLodashFunc(() => state.items, chunk),
    referenceOne: ((ids: any, field: any): any => (referenceOne as any)(self, ids, field)) as any,
    referenceMany: ((ids: any, field: any): any => (referenceMany as any)(self, ids, field)) as any,
    forEach: (iteratee: IIteratee<T, any>) => {
      forEach(state.items, iteratee);
      return self;
    },
    at: (index: number) => state.items[index],
    slice,
    move,
    get length() {
      return state.items.length;
    },
  };

  function get(id: ModelId[]): Array<T | undefined>;
  function get(id: ModelId): T | undefined;
  function get(id: ModelId | ModelId[]): (T | undefined) | Array<T | undefined> {
    /*tslint:disable-next-line*/
    if (id === undefined || id === null) {
      return undefined;
    }

    if (Array.isArray(id)) {
      return id.map(get) as T[];
    }

    const idAsString: string = id.toString();
    const fromMap = idMap.get(idAsString);
    if (fromMap) {
      return fromMap;
    }

    const found = state.items.find(item => {
      const modelId = opts!.getModelId!(item, opts!);
      if (!modelId) {
        return false;
      }

      return modelId.toString() === idAsString;
    });

    if (found !== undefined) {
      idMap.set(idAsString, found);
    }

    return found;
  }

  function set(data?: any[], setOpts?: ICollectionOptions<T>): T[] | undefined;
  function set(data?: any, setOpts?: ICollectionOptions<T>): T | undefined;
  function set(data?: any | any[], setOpts?: ICollectionOptions<T>): T | T[] | undefined {
    setOpts = Object.assign({}, opts as any, setOpts);
    if (!data) {
      return undefined;
    }

    if (Array.isArray(data)) {
      return data.map(d => set(d, setOpts) as T);
    }

    let dataId = opts!.getDataId!(data, setOpts!);
    if (dataId === undefined) {
      return undefined;
    }

    if (dataId === null) {
      throw new TypeError(`${dataId} is not a valid ID`);
    }

    dataId = dataId.toString();

    let existing = get(dataId as string);
    if (existing) {
      opts!.update!(existing, data, Object.assign({}, opts as any, setOpts));
      return existing;
    }

    const created = opts!.create!(data, Object.assign({}, opts as any, setOpts));

    // If creating this object resulted in another object with the same ID being
    // added, reuse it instead of adding this new one.
    existing = get(dataId as string);
    if (existing) {
      opts!.update!(existing, data, Object.assign({}, opts as any, setOpts));
      return existing;
    }

    //state.items.push(created);
    setState('items', [...state.items, created]);
    idMap.set(dataId, created);
    return created;
  }

  function create(data: any[], createOpts?: ICollectionOptions<T>): T[];
  function create(data: any, createOpts?: ICollectionOptions<T>): T;
  function create(data: any[] | any, createOpts?: ICollectionOptions<T>): T[] | T {
    if (Array.isArray(data)) {
      return data.map(d => create(d, createOpts));
    }

    createOpts = Object.assign({}, opts as any, createOpts);
    const dataId = createOpts!.getDataId!(data, createOpts!);
    if (dataId !== undefined && dataId !== null) {
      return set(data, createOpts) as any;
    }

    const created = createOpts!.create!(data, createOpts!);
    add(created);
    return created;
  }

  function add(models: T | T[]): ICollection<T> {
    if (!Array.isArray(models)) {
      return add([models]);
    }

    // Filter out existing models.
    //models = models.filter(x => state.items.indexOf(x) === -1);
    models = models.filter(item => state.items.map(x => x.id).indexOf(item.id) === -1);
    //state.items.push(...models);
    setState('items', [...state.items, ...models]);
    return self;
  }

  function remove(modelOrId: T | ModelId): ICollection<T> {
    if (Array.isArray(modelOrId)) {
      // added
      modelOrId.forEach(m => remove(m));
      return self;
    }

    const model = typeof modelOrId === 'string' || typeof modelOrId === 'number' || modelOrId instanceof Date ? get(modelOrId) : modelOrId;
    if (!model) {
      return self;
    }

    //state.items.remove(model); //mobx mobx/src/types/observablearray.ts:444
    //const idx = state.items.indexOf(model);
    //if (idx > -1) {setState('items', produce(s => {s.items.splice(idx, 1);}),);}
    const modelId = opts!.getModelId!(model, opts!);
    setState('items', t => t.filter(item => opts!.getModelId!(item, opts!) !== modelId)); // immutable

    /* istanbul ignore else */
    if (modelId !== null && modelId !== undefined) {
      idMap.delete(modelId.toString());
    }

    return self;
  }

  function clear(): ICollection<T> {
    //state.items.clear(); // mobx
    setState('items', []); // mobx/src/types/observablearray.ts:362
    idMap.clear();
    return self;
  }

  function slice(start?: number, end?: number) {
    return state.items.slice(start, end);
  }

  function move(fromIndex: number, toIndex: number) {
    setState('items', moveItem(state.items, fromIndex, toIndex)); // mobx /mobx-utils/src/array.ts:17
    return self;
  }

  return self;
}

/**
 * Utility for binding lodash functions.
 */
function bindLodashFunc(items: () => Array<any>, func: any): any {
  return (...args: any[]) => func(items(), ...args);
}

// mobx /mobx-utils/src/array.ts:17
function moveItem<T>(oldItems: Array<T>, fromIndex: number, toIndex: number) {
  checkIndex(oldItems, fromIndex);
  checkIndex(oldItems, toIndex);
  if (fromIndex === toIndex) {
    return oldItems;
  }

  let newItems: T[];
  if (fromIndex < toIndex) {
    newItems = [...oldItems.slice(0, fromIndex), ...oldItems.slice(fromIndex + 1, toIndex + 1), oldItems[fromIndex], ...oldItems.slice(toIndex + 1)];
  } else {
    // toIndex < fromIndex
    newItems = [...oldItems.slice(0, toIndex), oldItems[fromIndex], ...oldItems.slice(toIndex, fromIndex), ...oldItems.slice(fromIndex + 1)];
  }
  // oldItems.replace(newItems); // mobx (very big fn) // mobx/src/types/observablearray.ts:365
  // oldItems.splice(0, oldItems.length, newItems); // modifying existing array (also could return newItems)

  return newItems; // can not can not return oldItems, because its same reference and hence not updated by setState
}
/**
 * Checks whether the specified index is within bounds. Throws if not.
 *
 * @private
 * @param {Array<any>} target
 * @param {number }index
 */
// mobx-utils/src/array.ts:52
function checkIndex(target: Array<any>, index: number) {
  if (index < 0) {
    throw new Error(`array Index out of bounds: ${index} is negative`);
  }
  const length = target.length;
  if (index >= length) {
    throw new Error(`array Index out of bounds: ${index} is not smaller than ${length}`);
  }
}
