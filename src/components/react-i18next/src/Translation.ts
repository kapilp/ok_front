import { useTranslation } from './useTranslation';
import i18next, { ReactOptions, i18n, ThirdPartyModule, WithT, TFunction, Resource } from 'i18next';
import { splitProps } from 'solid-js';
export type Namespace = string | string[];

export interface TranslationProps {
  children: (
    t: TFunction,
    options: {
      i18n: i18n;
      lng: string;
    },
    ready: boolean,
  ) => JSX.Element;
  ns?: Namespace;
  i18n?: i18n;
}
export function Translation(props: TranslationProps) {
  const [p, options] = splitProps(props, ['ns', 'children']);

  const [t, i18n, ready] = useTranslation(p.ns, options);
  return p.children(
    t,
    {
      i18n,
      lng: i18n.language,
    },
    ready,
  );
}
