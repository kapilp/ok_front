// import { useSSR } from './useSSR';
// import { composeInitialProps } from './context';
// import { getDisplayName } from './utils';
// Need to see usage to improve this
import { Resource } from 'i18next';

export function withSSR(): <Props>(
  WrappedComponent: React.ComponentType<Props>,
) => {
  ({
    initialI18nStore,
    initialLanguage,
    ...rest
  }: {
    initialI18nStore: Resource;
    initialLanguage: string;
  } & Props): React.FunctionComponentElement<Props>;
  getInitialProps: (ctx: unknown) => Promise<any>;
};

// export function withSSR() {
//   return function Extend(WrappedComponent) {
//     function I18nextWithSSR({ initialI18nStore, initialLanguage, ...rest }) {
//       useSSR(initialI18nStore, initialLanguage);
//       return React.createElement(WrappedComponent, {
//         ...rest,
//       });
//     }
//     I18nextWithSSR.getInitialProps = composeInitialProps(WrappedComponent);
//     I18nextWithSSR.displayName = `withI18nextSSR(${getDisplayName(WrappedComponent)})`;
//     I18nextWithSSR.WrappedComponent = WrappedComponent;
//     return I18nextWithSSR;
//   };
// }
