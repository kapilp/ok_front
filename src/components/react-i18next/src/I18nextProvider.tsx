import { I18nContext } from './context';
import i18next, { ReactOptions, i18n, ThirdPartyModule, WithT, TFunction, Resource } from 'i18next';
export interface I18nextProviderProps {
  i18n: i18n;
  defaultNS?: string;
}
export function I18nextProvider(props: I18nextProviderProps) {
  return (
    <I18nContext.Provider i18n={props.i18n} defaultNs={props.defaultNS}>
      {props.children}
    </I18nContext.Provider>
  );
}
