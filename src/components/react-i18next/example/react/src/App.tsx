import { JSX, Suspense } from 'solid-js';
import { useTranslation } from '../../../src/useTranslation';
// import { Trans } from '../../../src/Trans';
import logo from './logo.svg';
import './App.css';

// Component using the Trans component
// function MyComponent() {
//   return (
//     <Trans i18nKey="description.part1">
//       To get started, edit <code>src/App.js</code> and save to reload.
//     </Trans>
//   );
// }
// page uses the hook
function Page() {
  const { t, i18n } = useTranslation();
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
  };
  return (
    <div className="App">
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>{'title'}</h2>;<button onClick={() => changeLanguage('de')}>de</button>
        <button onClick={() => changeLanguage('en')}>en</button>
      </div>
      <div className="App-intro">MyComponent</div>
      <div>{t('description.part2')}</div>
    </div>
  );
}
// loading component for suspense fallback
const Loader = () => (
  <div className="App">
    <img src={logo} className="App-logo" alt="logo" />
    <div>loading...</div>
  </div>
);
// here app catches the suspense from page in case translations are not yet loaded
export const I18ReactExample = () => {
  return (
    <Suspense fallback={<Loader />}>
      <Page />
    </Suspense>
  );
};
