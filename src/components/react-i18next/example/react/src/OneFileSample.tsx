import i18n from 'i18next';
import { initReactI18next } from '../../../src/context';
import { useTranslation } from '../../../src/useTranslation';
import { JSX, createSignal, Show } from 'solid-js';
// https://react.i18next.com/getting-started
i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: {
          'Welcome to React': 'Welcome to React and react-i18next',
        },
      },
      de: {
        translation: {
          'Welcome to React': 'DE',
        },
      },
    },
    lng: 'en',
    fallbackLng: 'en',

    interpolation: {
      escapeValue: false,
    },
  });

export const OneFileI18Sample = () => {
  const { t, i18n } = useTranslation();
  const [isOpen, setIsOpen] = createSignal(true);
  const changeLanguage = lng => {
    i18n.changeLanguage(lng);
  };

  return (
    <>
      <Show when={isOpen()}>
        <h2>{t('Welcome to React')}</h2>
      </Show>
      <button onClick={_ => setIsOpen(!isOpen())}>Toggle</button>
      <button onClick={() => changeLanguage('de')}>de</button>
      <button onClick={() => changeLanguage('en')}>en</button>
    </>
  );
};
