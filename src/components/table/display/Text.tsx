export const TextView = (props: { value: string; props: {} }) => {
  return <span {...props.props}>{props.value}</span>;
};
