import UrlPattern from 'url-pattern';

import { NavLink } from '@rturnq/solid-router';
import { useContext } from 'solid-js';
import { Button, ButtonVariants } from '../../UI/core/button/src/Button';
import { OrgIDContext, ProjectIDContext } from '../../../layout/context';

export const Url = (props: { href: string; value: string; props: {} }) => {
  return (
    <NavLink name={props.href} {...props.props} end>
      {props.value}
    </NavLink>
  );
};
export const MakeUrl = (props: { dp?: string; id: string; value: string }) => {
  const [getOrgIDContext] = useContext(OrgIDContext);
  const orgId = getOrgIDContext();
  const [getProjectIDContext] = useContext(ProjectIDContext);
  const projectId = getProjectIDContext();
  // make sure orgId and projectId is string, otherwise it give error: Missing pattern at 23
  function makeUrl(dp: string, id: string) {
    if (id && dp) {
      return new UrlPattern(dp).stringify({
        id,
        org: orgId,
        project: projectId,
      });
    }
    return '';
  }
  if (!props.dp) return '';
  if (!props.id) return '';
  return (
    <NavLink component={Button} href={makeUrl(props.dp, props.id)} {...props} end e text={props.value} isBlock variant={ButtonVariants.ACTION} isCompact>
      {props.value}
    </NavLink>
  );
};
