export const Time = (props: { value: string; props: {} }) => {
  /*  $: {
    const t = new Date(value);
    value = t.toLocaleString();
  }
*/
  return <span {...props.props}>{props.value}</span>;
};
