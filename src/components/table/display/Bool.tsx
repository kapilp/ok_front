export const Bool = (props: { value: boolean }) => {
  return (
    <span>
      <input type="checkbox" checked={!!props.value} disabled />
    </span>
  );
};
