/*
Creating a Drag-and-Drop File Uploader with React & TypeScript
https://spin.atomicobject.com/2018/09/13/file-uploader-react-typescript/

The ultimate guide to drag and drop in React
https://blog.logrocket.com/drag-and-drop-in-react/

https://github.com/search?q=react+drag+drop

Very easy 2 Declarative components:
Draggable.js
Droppable.js
https://github.com/asbjornenge/react-drag-and-drop/tree/master/src

29 items:
https://reactjsexample.com/tag/drag-drop/

React Drag and Drop example
https://gist.github.com/gmaclennan/9f2e5f182c39fe56dbad0d0bd69d7fe2

Build a Drag and Drop (DnD) layout builder with React and ImmutableJS
https://medium.com/javascript-in-plain-english/build-a-drag-and-drop-dnd-layout-builder-with-react-and-immutablejs-78a0797259a6

How to use the Drag and Drop API
https://flaviocopes.com/drag-and-drop/

How To Use The HTML Drag-And-Drop API In React
https://www.smashingmagazine.com/2020/02/html-drag-drop-api-react/

React drag and drop
https://medium.com/the-andela-way/react-drag-and-drop-7411d14894b9

HTML5 - Drag & drop
https://www.tutorialspoint.com/html5/html5_drag_drop.htm

How to Implement Drag and Drop Feature for Your React Component
https://www.pluralsight.com/guides/implement-drag-drop-react-component

Drag and Drop Elements with Vanilla JavaScript and HTML
https://www.digitalocean.com/community/tutorials/js-drag-and-drop-vanilla-js

https://html.spec.whatwg.org/multipage/dnd.html

Drag'n'Drop with mouse events
https://javascript.info/mouse-drag-and-drop

Playing around react d&d:
https://www.youtube.com/watch?v=NW8erkUgqus
*/
import { isEmpty } from "rambda";
import { createStore, Store, SetStoreFunction } from "solid-js/store";
import { FailOrSuccessResult, makeTableDropArgs, TableResult } from "../../../utils/enums";
import { Ws } from "../../../utils/ws_events_dispatcher";
import { DragData, DragState, ItemsState } from "./table_types";
import { ICollection } from "../../libx/src/collection";

export const [dragData, setDragData] = createStore({} as DragData); // global store
export const DragItemTypes = {
  TABLE_ROW: "table",
};

export function clearHoverStatus() {
  const rows = document.querySelectorAll<HTMLElement>(".tree-row");
  for (let i = 0; i < rows.length; i++) {
    const row = rows[i];
    const hoverBlock = row.children[row.children.length - 1] as HTMLElement;
    hoverBlock.style.display = "none";
    if (hoverBlock.children[0]) (hoverBlock.children[0] as HTMLElement).style.opacity = "0.1";
    if (hoverBlock.children[1]) (hoverBlock.children[1] as HTMLElement).style.opacity = "0.1";
    if (hoverBlock.children[2]) (hoverBlock.children[2] as HTMLElement).style.opacity = "0.1";
  }
}
/*export function getElementTop(element, tableRef) {
  // Fixed header, need special calculation
  let scrollTop = tableRef.querySelector('.drag-tree-table-body').scrollTop;
  let actualTop = element.offsetTop - scrollTop;
  let current = element.offsetParent;
  while (current !== null) {
    actualTop += current.offsetTop;
    current = current.offsetParent;
  }
  return actualTop;
}
export function getElementLeft(element) {
  let actualLeft = element.offsetLeft;
  let current = element.offsetParent;
  while (current !== null) {
    actualLeft += current.offsetLeft;
    current = current.offsetParent;
  }
  return actualLeft;
}*/
export const getInitialDragState = (isDraggable?: boolean): DragState => ({
  isTree: false,
  isDraggable: isDraggable ?? false,
  expandedRowsKeys: {} as { [key: string]: boolean },
  isDragging: false,
  onlySameLevelCanDrag: false,
});
export function tableRowDrag(
  drag_evt: string[],
  itemCollection: Store<ItemsState>,
  dragState: Store<DragState>,
  setDragState: SetStoreFunction<DragState>,
  hightRowChange: boolean | undefined
) {
  enum dropPosition {
    none = 0,
    top,
    center,
    bottom,
  }
  let dragX = 0;
  let dragY = 0;
  const dragId = "";
  let targetId: string | null = "";
  let targetRevId: string | null = "";
  let whereInsert = dropPosition.none;
  // drag main function 1
  function onDraggingOver(e: DragEvent) {
    e.preventDefault();
    const isSourceData = e.dataTransfer ? e.dataTransfer.types.includes("source") : false;
    if (!isSourceData) return;
    if (isEmpty(dragData)) return;
    setDragState({ isDragging: true });
    if (e.pageX == dragX && e.pageY == dragY) return;
    dragX = e.pageX;
    dragY = e.clientY;
    targetId = null;
    targetRevId = null;
    filter(e.pageX, e.clientY, dragData);
    if (e.clientY < 100) {
      window.scrollTo(0, scrollY - 6);
    } else if (e.clientY > document.body.clientHeight - 160) {
      window.scrollTo(0, scrollY + 6);
    }
  }
  // drag main function 2
  // dragend
  async function drop(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();
    // not work:
    // const isSourceData = e.dataTransfer.types.includes('source')
    // if (!isSourceData) return
    if (isEmpty(dragData) || dragData.type != DragItemTypes.TABLE_ROW) {
      return;
    }
    // fix it should return in some time limit.
    const d: FailOrSuccessResult = await new Promise((resolve, reject) => {
      Ws.bindT(
        drag_evt,
        (d) => {
          resolve(d);
        },
        makeTableDropArgs().setValue([dragData.dragId, dragData.dragRevId, targetId, targetRevId, whereInsert])
      );
    });
    if (!d[0]) {
      clearHoverStatus();
      setDragState({ isDragging: false });
      return;
    }
    clearHoverStatus();
    // resetTreeData(dragData); //Main Function // No need anymore
    setDragState({ isDragging: false });
    if (targetId !== undefined) {
      if (hightRowChange !== undefined) {
        const rowEle: HTMLElement | null = document.querySelector("[tree-id='" + dragData.dragId + "']");
        if (rowEle) {
          rowEle.style.backgroundColor = "rgba(64,158,255,0.5)";
          setTimeout(() => {
            rowEle.style.backgroundColor = "rgba(64,158,255,0)";
          }, 2000);
        }
      }
    }
    setDragData({});
  }
  // todo: fix only one line, this function is unsed while draggging over itemCollection.items.
  //* 1. get all tree row
  //* 2. this just show hover position
  //*
  // Find matching lines, handle drag and drop styles
  function filter(x: number, y: number, sourceData: DragData) {
    if (!sourceData.dragParentNode) return;
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
    const dragRect = sourceData.dragParentNode.getBoundingClientRect();
    const dragW = dragRect.left + sourceData.dragParentNode.clientWidth;
    const dragH = dragRect.top + sourceData.dragParentNode.clientHeight;
    if (x >= dragRect.left && x <= dragW && y >= dragRect.top && y <= dragH) {
      // The original block currently being dragged is not allowed to be inserted
      // console.log('same element !');
      return;
    }
    targetId = null;
    targetRevId = null;
    let hoverBlock: HTMLElement | null = null;
    let targetIdTemp: string | null = null;
    let targetIdRevTemp: string | null = null;
    whereInsert = dropPosition.none;
    const rows = document.querySelectorAll<HTMLElement>(".tree-row");
    let row: HTMLElement;
    for (let i = 0; i < rows.length; i++) {
      row = rows[i];
      const rect = row.getBoundingClientRect();
      const rx = rect.left;
      const ry = rect.top;
      const rw = row.clientWidth;
      const rh = row.clientHeight;
      if (x > rx && x < rx + rw && y > ry && y < ry + rh) {
        const diffY = y - ry;
        const pId = row.getAttribute("tree-p-id"); // It is not allowed to change the hierarchical structure, only the upper and lower order logic
        if (dragState.onlySameLevelCanDrag && pId !== sourceData.dragPId) {
          return;
        }
        targetIdTemp = row.getAttribute("tree-id");
        targetIdRevTemp = row.getAttribute("tree-rev-id");
        hoverBlock = row.children[row.children.length - 1] as HTMLElement;
        const rowHeight = row.offsetHeight;
        if (diffY / rowHeight > 3 / 4) {
          whereInsert = dropPosition.bottom;
        } else if (diffY / rowHeight > 1 / 4) {
          if (dragState.onlySameLevelCanDrag) {
            // It is not allowed to change the hierarchical structure, only the upper and lower order logic
            return;
          }
          whereInsert = dropPosition.center;
        } else {
          whereInsert = dropPosition.top;
        }
        break;
      }
    }
    if (!targetIdTemp) {
      // Can't match to clear the previous state
      clearHoverStatus();
      whereInsert = dropPosition.none;
      return;
    }
    //let canDrag: boolean = true;
    // Todo can disable some row drag and drop. set canDrag == false.
    //if (beforeDragOver) { //todo fix this
    //const curRow = getItemById(itemCollection.items, sourceData.dragId)
    //const targetRow = getItemById(itemCollection.items, targetIdTemp)
    //canDrag = beforeDragOver(curRow, targetRow, whereInsert)
    //}
    //if (canDrag == false) return;
    if (hoverBlock) {
      hoverBlock.style.display = "block";
      // let rowHeight = row.offsetHeight;
      if (whereInsert == dropPosition.bottom) {
        if ((hoverBlock.children[2] as HTMLElement).style.opacity !== "0.5") {
          clearHoverStatus();
          (hoverBlock.children[2] as HTMLElement).style.opacity = "0.5";
        }
      } else if (whereInsert == dropPosition.center) {
        if ((hoverBlock.children[1] as HTMLElement).style.opacity !== "0.5") {
          clearHoverStatus();
          (hoverBlock.children[1] as HTMLElement).style.opacity = "0.5";
        }
      } else {
        if ((hoverBlock.children[0] as HTMLElement).style.opacity !== "0.5") {
          clearHoverStatus();
          (hoverBlock.children[0] as HTMLElement).style.opacity = "0.5";
        }
      }
    }
    targetId = targetIdTemp;
    targetRevId = targetIdRevTemp;
    whereInsert = whereInsert;
  }
  /*function resetTreeData(sourceData) {
    if (targetId === undefined) return;
    const newList: TABLERESULTARRAY = [];
    const curList = itemCollection.items;
    let curDragItem = null;
    let targetItem = null;
    function pushData(curList: TABLERESULTARRAY, needPushList: TABLERESULTARRAY) {
      for (let i = 0; i < curList.length; i++) {
        const item = curList[i];
        let obj = clone(item);
        const key = item['_key'];
        obj[0] = key; //obj[0][1] = [] // empty_logic
        if (targetId == key) {
          curDragItem = getItemById(itemCollection.items, sourceData.dragId);
          targetItem = getItemById(itemCollection.items, targetId);
          if (whereInsert === dropPosition.top) {
            //curDragItem['parent_id'] = item['parent_id']
            needPushList.push(curDragItem);
            needPushList.push(obj);
          } else if (whereInsert === dropPosition.center) {
            // Dont expand automatically:
            // if (!dragState.expandedRowsKeys[key as string) {
            //   dragState.expandedRowsKeys.push(key);
            // }
            // isExapndedModified = true;
            // Now not need this:
            // if (Array.isArray(obj[0])) {
            //   obj[0][1].push(curDragItem);
            // } else {
            //   obj[0] = [key, [curDragItem]];
            // }
            needPushList.push(obj);
          } else if (whereInsert === dropPosition.bottom) {
            //curDragItem['parent_id'] = item['parent_id']
            needPushList.push(obj);
            needPushList.push(curDragItem);
          } else {
            console.log('whereInsert must be valid: ', whereInsert);
            needPushList.push(curDragItem);
            needPushList.push(obj);
          }
        } else {
          if (sourceData.dragId != key) {
            needPushList.push(obj); // empty_logic
          }
        }
        // empty_logic
        if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
          pushData(item['_key'][1], obj[0][1]);
        }
      }
    }
    pushData(curList, newList);
    setItemsState({ items: newList }); // bug: From Child to parent set null row
    /////resetOrder(newList)
    ////onDrag(newList, curDragItem, targetItem, _this.whereInsert)
  }*/
  // Reset all data order
  // todo : fix this function
  // function resetOrder(list) {
  //     for (let i = 0; i < list.length; i++) {
  //         list[i]['order'] = i
  //         if (list[i]['lists'] && list[i]['lists'].length) {
  //             resetOrder(list[i]['lists'])
  //         }
  //     }
  // }
  // Get current row based on id
  /*function getItemById(lists: TABLERESULTARRAY, id: string) {
    let curItem = null;
    function getchild(curList: TABLERESULTARRAY) {
      for (let i = 0; i < curList.length; i++) {
        let item = curList[i];
        if (item['_key'] == id) {
          curItem = clone(item);
          break;
        } else if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
          getchild(item['_key'][1]);
        }
      }
    }
    getchild(lists);
    return curItem;
  }*/
  // not used function:
  /*// Get current row based on id
    function DelById(id: string) {
        const newList: TABLERESULTARRAY = [];
        const curList = itemCollection.items;
        function pushData(curList: TABLERESULTARRAY, needPushList: TABLERESULTARRAY) {
            //let order = 0
            for (let i = 0; i < curList.length; i++) {
                const item = curList[i];
                if (item['_key'] != id) {
                    let obj = clone(item);
                    //obj['order'] = order
                    obj[0] = [item['_key'], []]; //obj[0][1] = []
                    needPushList.push(obj);
                    order++;
                    if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                        pushData(item['_key'][1], obj[0][1]);
                    }
                }
            }
        }
        pushData(curList, newList);
        return newList;
    }*/

  // not used function:
  /*function GetLevelById(id: string) {
        row = table.querySelector('[tree-id="' + id + '"]');
        let level = row.getAttribute('data-level') * 1;
        return level;
    }
    function HighlightRow(id: string, isHighlight = true, deep = false) {
        let list = clone(itemCollection.items);
        let ids = [id];
        if (deep == true) {
            ids = ids.concat(GetChildIds(id, true));
        }
        deepSetAttr('highlight', isHighlight, list, ids);
        setState({ items: list });
    }
    function AddRow(pId, data) {
        // todo: note: data is json obj
        const deepList = clone(itemCollection.items);
        function deep(list) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item['_key'] == pId) {
                    item['open'] = true;
                    let newRow = Object.assign({}, data);
                    ////newRow['parent_id'] = pId
                    if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                        ////newRow['order'] = item['lists'].length
                        item['_key'][1].push(newRow);
                    } else {
                        item['_key'][1] = [];
                        ////newRow['order'] = 0
                        item['_key'][1].push(newRow);
                    }
                }
                if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                    deep(item['_key'][1]);
                }
            }
        }
        deep(deepList);
        setState({ items: deepList });
    }
    function EditRow(id, data) {
        const deepList = clone(itemCollection.items);
        function deep(list) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item['_key'] == id) {
                    let newRow = Object.assign({}, item, data);
                    item = newRow;
                }
                if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                    deep(item['_key'][1]);
                }
            }
        }
        deep(deepList);
        setState({ items: deepList });
    }
    function GetChildIds(id, deep = true) {
        let ids = [];
        function getChilds(list, id, addAll = false) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                let currentPid = '';
                let pid = item['_key']; // list[i]['parent_id']
                if (addAll || id == pid) {
                    currentPid = item['_key'];
                    ids.push(currentPid);
                } else {
                    currentPid = id;
                }
                if (deep == true || id == currentPid) {
                    if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                        getChilds(item['_key'][1], currentPid, true);
                    }
                }
            }
        }
        getChilds(itemCollection.items, id);
        return ids;
    }
    // Select button event
    function onCheckAll(evt, func) {
        setAllCheckData(itemCollection.items, !!evt.target.checked);
        const checkedList = getCheckedList(itemCollection.items);
        func && func(checkedList);
    }
    // Batch process data according to flag
    function setAllCheckData(curList, flag) {
        for (let i = 0; i < curList.length; i++) {
            let item = curList[i];
            // this.$set(item, 'checked', flag); // todo fix this
            if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                setAllCheckData(item['_key'][1], flag);
            }
        }
    }
    // Get all selected rows
    function getCheckedList(lists) {
        let checkedList = [];
        const deepList = clone(lists);
        function getchild(curList) {
            for (let i = 0; i < curList.length; i++) {
                let item = curList[i];
                if (item.checked && item.isShowCheckbox != false) {
                    checkedList.push(item);
                }
                if (Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length) {
                    getchild(item['_key'][1]);
                }
            }
        }
        getchild(deepList);
        return checkedList;
    }*/

  return {
    onDraggingOver,
    drop,
  };
}
