export const focusElement = (element?: HTMLElement | null) => element && element.focus();
export const hideElement = (element: HTMLElement) => (element.style.display = 'none');
export const showElement = (element: HTMLElement) => (element.style.display = 'block');
export const changeElementTopLeftPositionWhereMouse = (e: MouseEvent, element: HTMLElement) => {
  if (element) {
    const left = e.clientX;
    const top = e.clientY;
    element.style.left = left + 'px';
    element.style.top = top + 'px';
  }
};
