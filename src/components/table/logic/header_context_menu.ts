import { SortDirection } from '../../../utils/enums';
import { changeElementTopLeftPositionWhereMouse, hideElement, showElement } from './tableUtils';
import { TableDoms } from './table_types';
// 1. serve list from schema
// 2. show edit/delete links on  menu.
export function tableContextMenuFns(
  doms: TableDoms,
  onHandleSort: (col: number, order?: SortDirection | undefined) => (e: MouseEvent) => void,
) {
  let headerMenuDisplayed = false;
  let headerMenuColumn = 0;
  const globalOutSideContextMenuHandler = () => {
    closeHeaderMenu();
    window.removeEventListener('click', globalOutSideContextMenuHandler);
  };
  const onHeaderContext = (col: number) => (e: MouseEvent) => {
    e.preventDefault();
    if (doms.refContextMenuBox) {
      changeElementTopLeftPositionWhereMouse(e, doms.refContextMenuBox);
      showElement(doms.refContextMenuBox);
      headerMenuDisplayed = true;
      headerMenuColumn = col;
    }
    window.addEventListener('click', globalOutSideContextMenuHandler);
  };
  const closeHeaderMenu = () => headerMenuDisplayed && doms.refContextMenuBox && hideElement(doms.refContextMenuBox);
  const onHandleSortFromContext = (order: SortDirection) => (e: MouseEvent) => onHandleSort(headerMenuColumn, order)(e);
  return {
    onHeaderContext,
    closeHeaderMenu,
    onHandleSortFromContext,
  };
}
