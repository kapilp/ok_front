import { SetStoreFunction, Store } from 'solid-js/store';
import { ColumnState, ConfigState, FetchConfig, FetchState } from './table_types';

export const getInitialConfigState = (): ConfigState => ({
  config: false,
});
export default function configFns(
  configState: Store<ConfigState>,
  setConfigState: SetStoreFunction<ConfigState>,
  columnState: Store<ColumnState>,
  setColumnState: SetStoreFunction<ColumnState>,
  fetchState: Store<FetchState>,
  setFetchState: SetStoreFunction<FetchState>,
  fetchConfigState: Store<FetchConfig>,
  refresh: () => void,
) {
  const onConfigClicked = async () => setConfigState({ config: !configState.config });

  const onConfigApply = (columns: string[]) => {
    setColumnState('selectedColumns', columns);
    setConfigState({ config: false });
    // const e1 = [header_evt, setFetchState] // fix this(config)
    // Ws.trigger([e1]) // fix this
    // resetFilter_(); onHeaderGet() will do this.
    // setItemsState({ colSortRow: {} }); // fix this
    // refresh();
  };

  const onColumnHide = (id: string) => (e: MouseEvent & { target: HTMLInputElement }) => {
    if (e.target.checked) {
      if (!columnState.selectedColumns.includes(id)) {
        setColumnState('selectedColumns', l => {
          l.push(id);
        });
      }
    } else if (columnState.selectedColumns.includes(id)) {
      setColumnState('selectedColumns', l => {
        const idx = l.findIndex(x => x == id);
        if (idx > -1) {
          l.splice(idx, 1);
        }
      });
    }
    refresh();
  };

  return { onConfigClicked, onConfigApply, onColumnHide };
}
