import { SetStoreFunction, Store } from 'solid-js/store';
import { PaginationQury, PaginationState } from './table_types';
// full pagination component:
// keystone/packages/arch/packages/pagination/src/Pagination.js
//  Implement pagination button
export const getInitialPaginationState = (query?: PaginationQury): PaginationState => ({
  count: 0,
  limit: Number(query?.limit) || 0,
  pages: [1],
  current_page: Number(query?.page) || 1,
  total_pages: Math.max(Number(query?.page) || 1, 1),
});
export default function paginationFns(paginationState: Store<PaginationState>, setPaginationState: SetStoreFunction<PaginationState>, refresh: () => void) {
  const calcPagination = () => {
    if (paginationState.limit <= 0) {
      setPaginationState({
        limit: 0,
        total_pages: 1,
        pages: [1],
        current_page: 1,
      });
    } else {
      setPaginationState({ total_pages: Math.ceil(paginationState.count / paginationState.limit) });
      const arrPages = [];
      for (let i = 1; i <= paginationState.total_pages; i++) arrPages.push(i);
      setPaginationState({ pages: arrPages });
      !paginationState.pages.includes(paginationState.current_page) && setPaginationState({ current_page: 1 });
    }
  };
  const onLimitChange = (e: Event & { currentTarget: HTMLInputElement; target: HTMLInputElement }) => {
    e.target && setPaginationState({ limit: Number(e.target.value) });
    calcPagination();
    refresh();
    //note: after refresh calc_pagination() will be called again.
  };
  const onPageChange = (e: Event & { currentTarget: HTMLSelectElement; target: HTMLSelectElement }) => {
    e.target && setPaginationState({ current_page: Number(e.target.value) });
    refresh(); // refresh cause page change not work?
  };
  return {
    calcPagination,
    onLimitChange,
    onPageChange,
  };
}
