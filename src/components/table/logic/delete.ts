import { SetStoreFunction, Store } from 'solid-js/store';
import { toast } from '@zerodevx/svelte-toast';
import { ET, GeneralError, makeTableManyDeleteArgs, makeTableOneDeleteArgs, Success } from '../../../utils/enums';
import { Ws } from '../../../utils/ws_events_dispatcher';
import { FetchConfig, FetchState, MultiSelectState } from './table_types';

export function deleteFns(
  events: any[] | undefined,
  multiSelectState: Store<MultiSelectState>,
  fetchState: Store<FetchState>,
  setFetchState: SetStoreFunction<FetchState>,
  fetchConfigState: Store<FetchConfig>,
) {
  const onDeleteSvgKeyPress = (key: string) => (e: KeyboardEvent & { currentTarget: SVGSVGElement; target: SVGSVGElement }) =>
    (e.keyCode === 13 || e.which === 13) && onDeleteOneRow(key)();

  // const { addNotification } = getNotificationsContext();
  const onDeleteOneRow = (key: string, unsubEvt?: number[]) => async (e?: Event) => {
    if (e) e.stopPropagation();

    if (events) {
      const mutate_evt = [ET[ET.delete_], events[1], key];

      const d: [Success, string] = await new Promise((resolve, reject) => {
        // send unsubscribe event if edit is open
        const args = makeTableOneDeleteArgs().setKeyRev(key, '');
        if (unsubEvt) args.setUnsub(unsubEvt); // necessary to send unsubscribe event else it send update result two times.

        Ws.bindT(
          mutate_evt,
          d => {
            resolve(d);
          },
          { ...args, ...fetchConfigState },
        );
      });
      if (!d.error) {
        toast.push('Success', {
          pausable: true,
          theme: {
            '--toastBackground': '#48BB78',
            '--toastBarBackground': '#2F855A',
          },
        });
      } else {
        toast.push(d.description, {
          pausable: true,
          theme: {
            '--toastBackground': '#F56565',
            '--toastBarBackground': '#C53030',
          },
        });
      }
    }
  };
  const onDeleteSelected = async () => {
    if (events) {
      let filter: {};
      if (multiSelectState.multipleSelected) {
        const selectedKeys = [];
        for (const p in multiSelectState.selectedRowsKeys) {
          if (multiSelectState.selectedRowsKeys[p]) selectedKeys.push(p);
        }
        filter = { _key: JSON.stringify(selectedKeys) };
        const mutate_evt = [ET[ET.batchDelete], events[1], Ws.uid];
        const d: [Success, string] = await new Promise((resolve, reject) =>
          Ws.bindT(
            mutate_evt,
            d => {
              resolve(d);
            },
            makeTableManyDeleteArgs().setFilter(filter),
          ),
        );
        if (!d.error) {
          //
        } else {
          setFetchState({ er: d.description });
        }
      } else {
        let selectedKey = '';
        for (const p in multiSelectState.selectedRowsKeys) {
          if (multiSelectState.selectedRowsKeys[p]) {
            selectedKey = p;
            break;
          }
        }
        filter = { _key: selectedKey, _rev: '' }; // pass _rev later
        await onDeleteOneRow(selectedKey)();
      }
    }
  };
  return {
    onDeleteSvgKeyPress,
    onDeleteOneRow,
    onDeleteSelected,
  };
}
