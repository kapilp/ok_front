import {
  AllIdsLabelsObject,
  DropPosition,
  GroupByARRAY,
  IdsARRAY,
  SORT_DIRECTION,
  TableOptionButtonLabels,
  TableResult,
  TABLE_OPTIONS,
  WIDTH,
  TableSchema,
} from '../../../utils/enums';

export type FetchConfig = {
  parent?: string;
  project?: string;
} & { [key: string]: any };

export type FetchState = {
  er: string;
  isLoading: boolean;
  authorized: boolean;
  options: TABLE_OPTIONS;
};
export type ColumnState = {
  schema: TableSchema['columnSchema'];
  selectedColumns: IdsARRAY;
  allIdsRow: AllIdsLabelsObject;
  // showKeyRev: !IS_PRODUCTION,
  // colCount: number;
};
export type ItemsState = {
  items: TableResult;
};
export type ConfigState = {
  config: boolean;
  // selectedColumns: string[];
};
export type SortState = {
  colSortRow: SORT_DIRECTION;
  sortedByPosition: boolean;
};
export type FilterState = {
  filterSettings: { [key: string]: string | number | boolean };
  headerColCustomFilter: Array<{}>; // Todo Make it set by backend
};
export type PaginationState = {
  count: number;
  limit: number;
  pages: number[];
  current_page: number;
  total_pages: number;
};
export type GroupByState = {
  groupByRow: GroupByARRAY;
};
export type ViewState = {
  showRowNum: boolean;
  // Add Edit Row
  newFormVisible: boolean;
  addNewPos: 't' | 'b' | string;
  addNewType: 'button' | string;
  addNewLabels: TableOptionButtonLabels;
  rowType: string; // 'table' // unused
  showHeader: boolean;
  showButton: boolean;
};
export type ResizeState = {
  headerColWidthRow: WIDTH;
  resizingColId: string;
  tableWidth: number;
};
export type MultiSelectState = {
  selectedRowsKeys: { [key: string]: boolean };
  allSelected: boolean;
  multipleSelected: boolean;
};
export type EditState = {
  quickViewKeys: { [key: string]: boolean };
};
export type DragData = {
  type?: string;
  key?: string;
  dragId?: string | null;
  dragRevId?: string | null;
  dragPId?: string | null;
  dragParentNode?: HTMLElement;
  //
  dragX?: number;
  dragY?: number;
  targetId?: string | null;
  targetRevId?: string | null;
  whereInsert?: DropPosition;
};
export type DragState = {
  isTree: boolean;
  isDraggable: boolean;
  expandedRowsKeys: { [key: string]: boolean };
  isDragging: boolean;
  onlySameLevelCanDrag: boolean;
};
export type PaginationQury = { limit: 0; page: 1 };
export interface TableDoms {
  addButton?: HTMLButtonElement;
  table?: HTMLDivElement;
  refContextMenuBox?: HTMLDivElement;
  refContextMenuInputBox?: HTMLDivElement;
}
