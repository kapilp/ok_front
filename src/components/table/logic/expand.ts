import { SetStoreFunction, Store } from "solid-js/store";
import { TableResult } from "../../../utils/enums";
import { DragState, ItemsState } from "./table_types";

export function expandFns(itemCollection: Store<ItemsState>, dragState: Store<DragState>, setDragState: SetStoreFunction<DragState>) {
  // Set properties recursively, only allowed to set component built-in properties
  function deepSetAttr(key: string, val: boolean, list: TableResult, ids: string[] | undefined = undefined) {
    for (let i = 0; i < list.length; i++) {
      const item = list[i];
      const _key = item["_key"] as string;
      const changeAttribute = (k: string, v: boolean) => {
        if (key == "open") {
          if (v) !dragState.expandedRowsKeys[k] && setDragState("expandedRowsKeys", k, true);
          else dragState.expandedRowsKeys[k] && setDragState("expandedRowsKeys", k, false);
        } else if (key == "highlight") {
          // todo fix this
        }
      };
      if (ids !== undefined) ids.includes(_key) && changeAttribute(_key, val);
      else changeAttribute(_key, val);
      // Array.isArray(item['_key']) && item['_key'][1] && item['_key'][1].length && deepSetAttr(key, val, item['_key'][1], ids);
    }
  }
  const ZipAll = (e: MouseEvent) => deepSetAttr("open", false, itemCollection.items);
  const OpenAll = (e: MouseEvent) => deepSetAttr("open", true, itemCollection.items);
  const toggleExpandedRowsKeys = (key: string) => {
    if (dragState.expandedRowsKeys[key]) setDragState("expandedRowsKeys", key, false);
    else setDragState("expandedRowsKeys", key, true);
  };
  return { ZipAll, OpenAll, toggleExpandedRowsKeys };
}
