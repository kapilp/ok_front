import { SetStoreFunction, Store } from "solid-js/store";
import { ItemsState, MultiSelectState } from "./table_types";

// multi-select should be displayed properly
//  https://datatables.net/examples/api/select_row.html
export const getInitialMultiSelectState = (): MultiSelectState => ({
  selectedRowsKeys: {} as { [key: string]: boolean },
  allSelected: false,
  multipleSelected: false,
});
export default function multiSelectFns(itemCollection: Store<ItemsState>, multiSelectState: Store<MultiSelectState>, setMultiSelectState: SetStoreFunction<MultiSelectState>) {
  const selectHelper = () => {
    let allSelected = true;
    for (let i = 0; i < itemCollection.items.length; i++) {
      if (!multiSelectState.selectedRowsKeys[itemCollection.items[i]["_key"] as string]) {
        allSelected = false;
        break;
      }
    }
    let multipleSelected = false;
    for (const p in multiSelectState.selectedRowsKeys) {
      if (multiSelectState.selectedRowsKeys[p]) {
        multipleSelected = true;
        break;
      }
    }
    if (multiSelectState.allSelected != allSelected) setMultiSelectState({ allSelected: allSelected });
    if (multiSelectState.multipleSelected != multipleSelected) setMultiSelectState({ multipleSelected: multipleSelected });
  };
  const onSelectRowClick = (key: string) => (e: Event) => {
    e.stopPropagation();
    if (multiSelectState.selectedRowsKeys[key]) setMultiSelectState("selectedRowsKeys", key, false);
    else setMultiSelectState("selectedRowsKeys", key, true);
    selectHelper();
  };
  const onSelectAllClick = (e: MouseEvent & { currentTarget: HTMLInputElement; target: HTMLInputElement }) => {
    const v: boolean = e.target.checked;
    // multiSelectState({ allSelected: v }) // selectHelper() will do this
    if (v) {
      const allSelectObject: { [key: string]: boolean } = {};
      itemCollection.items.forEach((v) => (allSelectObject[v["_key"] as string] = true));
      setMultiSelectState({ selectedRowsKeys: allSelectObject });
    } else setMultiSelectState({ selectedRowsKeys: {} });
    selectHelper();
  };
  return { selectHelper, onSelectRowClick, onSelectAllClick };
}
