import {supportsPassiveEvents} from 'detect-passive-events';
import { SetStoreFunction } from 'solid-js/store';
import { ResizeState, TableDoms } from './table_types';
import { dragLineGlobal } from '../../UI/DragLine';
export const getInitialResizeState = (): ResizeState => ({
  headerColWidthRow: {},
  resizingColId: '',
  tableWidth: 0,
});
export const dragLineEvents = (
  doms: TableDoms,
  setResizeState: SetStoreFunction<ResizeState>,
) => {
  let mouse = {
    status: 0,
    startX: 0,
    curColWidth: 0,
    curId: '',
  };
  const onChange = (id: string, width: number) => {
    setResizeState('headerColWidthRow', id, width);
  };
  const mousedown = (id: string) => (e: MouseEvent & { currentTarget: HTMLDivElement; target: HTMLDivElement }) => {
    if (e.target) {
      const resizeBoxRect = e.target.getBoundingClientRect();
      const curColWidth = e.target.parentElement?.offsetWidth ?? 0;
      mouse = {
        status: 1,
        startX: resizeBoxRect.x + resizeBoxRect.width,
        curId: id,
        curColWidth,
      };
      setResizeState({ resizingColId: id });
      const passiveIfSupported = supportsPassiveEvents ? { passive: true } : false;
      document.addEventListener('mouseup', mouseup, passiveIfSupported);
      document.addEventListener('mousemove', mousemove, passiveIfSupported);
    }
  };
  const mouseup = (e: MouseEvent) => {
    if (!mouse.status) return;
    const curX = e.clientX;

    if (dragLineGlobal) {
      dragLineGlobal.style.left = '-10000px';
      mouse.status = 0;
      const subWidth = curX - mouse.startX;
      const lastWidth = mouse.curColWidth + subWidth;
      onChange(mouse.curId, lastWidth);
      setResizeState({ resizingColId: '' });
      document.addEventListener('mouseup', mouseup);
      document.addEventListener('mousemove', mousemove);
    }
  };
  const mousemove = (e: MouseEvent) => {
    if (!mouse.status) return;
    const endX = e.clientX;
    const tableLeft = doms.table ? doms.table.getBoundingClientRect().left : 0;
    if (dragLineGlobal) dragLineGlobal.style.left = endX - tableLeft + 'px';
  };
  return { mousedown };
};
