import { composeWith, tap } from 'ramda';
import { JSX } from 'solid-js';
import { SetStoreFunction, Store } from 'solid-js/store';
import { focusElement } from './tableUtils';
import { EditState } from './table_types';

export const getInitialEditState = (): EditState => ({
  quickViewKeys: {},
});
export function editFns(events: any[] | undefined, editState: Store<EditState>, setEditState: SetStoreFunction<EditState>) {
  const rowEditDoms: { [key: string]: HTMLElement } = {};
  const closeEditForm = (key: string) => editState.quickViewKeys[key] && setEditState('quickViewKeys', key, false);
  const editButtonFocus = async (key: string) => focusElement(document.querySelector(`button[key='${key}'][name='edit']`) as HTMLElement);
  const editSuccessSave = composeWith(tap, [editButtonFocus, closeEditForm]);
  const onEditSvgKeyPress = (key: string) => (e: KeyboardEvent) => (e.keyCode == 13 || e.which == 13) && setEditState('quickViewKeys', key, true);
  const onEditSvgClick = (key: string) => (e: MouseEvent) => {
    e.stopPropagation();
    setEditState('quickViewKeys', key, true);
  };
  return { closeEditForm, editSuccessSave, onEditSvgKeyPress, onEditSvgClick };
}
