import { SetStoreFunction, Store } from 'solid-js/store';
import { focusElement } from './tableUtils';
import { TableDoms, ViewState } from './table_types';
export function addFns(doms: TableDoms, viewState: Store<ViewState>, setViewState: SetStoreFunction<ViewState>) {
  const toggleAddForm = () => {
    if (viewState.addNewType === 'button') {
      setViewState({ newFormVisible: !viewState.newFormVisible });
      focusElement(doms.addButton);
    }
  };
  return { toggleAddForm };
}
