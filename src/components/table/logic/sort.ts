// sort-array-objects
// https://github.com/SanichKotikov/sort-array-objects

// sort by date funs:
// /home/kapili3/ok/drogonroot/packages/awaken-loot-management/lib/utils/sorter.ts

import { produce, SetStoreFunction, Store } from 'solid-js/store';
import { SortDirection, SORT_DIRECTION } from '../../../utils/enums';
import { ColumnState, SortState } from './table_types';
export const getInitialSortState = (): SortState => ({
  //headerColSortSettingsRow: (props?.query?.sort ? JSON.parse(query.sort) : []) as SORT_DIRECTIONARRAY,
  colSortRow: {} as SORT_DIRECTION,
  sortedByPosition: false,
});
export default function sortFns(columnState: Store<ColumnState>, sortState: Store<SortState>, setSortState: SetStoreFunction<SortState>, refresh: () => void) {
  const sortHelper = () => {
    const isSortedByPosition = sortState.colSortRow['position'] == SortDirection.Ascending;
    if (sortState.sortedByPosition != isSortedByPosition) setSortState({ sortedByPosition: isSortedByPosition });
  };
  const onHandleSort = (col: number, order?: SortDirection) => (e: MouseEvent) => {
    const sortOrder = sortState.colSortRow[columnState.selectedColumns[col]];
    if (!e.ctrlKey) {
      setSortState({ colSortRow: {} });
    }

    if (order !== undefined) {
      // when clicked from context menu
      setSortState('colSortRow', columnState.selectedColumns[col], order);
    } else {
      /*if(has(itemCollection.headerColselectedColumns[col],  itemCollection.colSortRow)){

      }*/

      if (sortOrder === null || sortOrder === undefined || sortOrder === SortDirection.None) {
        setSortState('colSortRow', columnState.selectedColumns[col], SortDirection.Ascending);
      } else if (sortOrder === SortDirection.Ascending) {
        setSortState('colSortRow', columnState.selectedColumns[col], SortDirection.Descending);
      } else {
        setSortState(
          'colSortRow',
          produce(l => {
            delete l[columnState.selectedColumns[col]];
          }),
        );
      }
    }
    sortHelper();

    refresh();
    // if (col.sortable === true && typeof col.value === "function") {
    //   if (sortKey === col.key) {
    //     sortOrder = sortOrder === 1 ? -1 : 1;
    //   } else {
    //     sortOrder = 1;
    //     sortKey = col.key;
    //     sortBy = r => col.value(r);
    //   }
    // }
  };
  return {
    sortHelper,
    onHandleSort,
  };
}
