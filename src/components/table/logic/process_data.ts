import { batch } from "solid-js";
import { produce, SetStoreFunction, Store } from "solid-js/store";
import { SortDirection, TableResult, TableResultWithSchema, TableSchema } from "../../../utils/enums";
import { ColumnState, EditState, FetchState, FilterState, ItemsState, MultiSelectState, PaginationState, SortState } from "./table_types";

export function dropNotExisting(removeFrom: { [key: string]: boolean }, notIn: string[]) {
  for (const p in removeFrom) {
    const idx = notIn.indexOf(p);
    if (idx == -1) removeFrom[p] = false;
  }
  return removeFrom;
}
export function updateModifiedRow(newData: TableResult, items: Store<ItemsState>, setItemsState: SetStoreFunction<ItemsState>) {
  newData.forEach((mod) => {
    const findIndex = items.items.findIndex((i) => i["_key"] == mod["_key"]);
    if (findIndex !== -1)
      setItemsState(
        "items",
        produce((l) => {
          // what is immutable way?
          l.splice(findIndex, 1, mod);
        })
      );
  });
}
export function removeRowsFromItems(keys: string[], items: Store<ItemsState>, setItemsState: SetStoreFunction<ItemsState>) {
  let count = 0;
  keys.forEach((k) => {
    const findIndex = items.items.findIndex((i) => i["_key"] == k);
    if (findIndex !== -1)
      setItemsState(
        "items",
        produce((l) => {
          l.splice(findIndex, 1);
        })
      );
    count++;
  });

  return count;
}
export function doLocalSorting(sortSetting: { [id: string]: SortDirection }, columns: string[], items: Store<ItemsState>, setItemsState: SetStoreFunction<ItemsState>) {
  if (Object.keys(sortSetting).length > 0) {
    const col = columns.findIndex((x) => sortSetting[x] == SortDirection.Ascending || sortSetting[x] == SortDirection.Descending);
    if (col == -1) return;
    setItemsState(
      "items",
      produce((l) => {
        const id = columns[col];
        if (sortSetting[id] == SortDirection.Ascending) {
          l.sort((first, second) => {
            if (first[id] < second[id]) return -1;
            if (first[id] > second[id]) return 1;
            return 0;
          });
        } else if (sortSetting[id] == SortDirection.Descending) {
          l.sort((first, second) => {
            if (first[id] < second[id]) return 1;
            if (first[id] > second[id]) return -1;
            return 0;
          });
        }
      })
    );
  }
}
export default function processHeaderAndDataReceived(
  setSchemaAndOptionsState: (d: TableSchema) => void,
  itemCollection: Store<ItemsState>,
  setItemsState: SetStoreFunction<ItemsState>,
  columnState: Store<ColumnState>,
  setColumnState: SetStoreFunction<ColumnState>,
  fetchState: Store<FetchState>,
  setFetchState: SetStoreFunction<FetchState>,
  paginationState: Store<PaginationState>,
  setPaginationState: SetStoreFunction<PaginationState>,
  sortState: Store<SortState>,
  setSortState: SetStoreFunction<SortState>,
  filterState: Store<FilterState>,
  setFilterState: SetStoreFunction<FilterState>,
  editState: Store<EditState>,
  setEditState: SetStoreFunction<EditState>,
  multiSelectState: Store<MultiSelectState>,
  setMultiSelectState: SetStoreFunction<MultiSelectState>,
  calcPagination: () => void,
  sortHelper: () => void,
  selectHelper: () => void,
  closeEditForm: (key: string) => void
) {
  const removeRows = (keys: string[]) => {
    keys.forEach((k) => {
      if (multiSelectState.selectedRowsKeys[k]) setMultiSelectState("selectedRowsKeys", k, false);
      if (editState.quickViewKeys[k]) setEditState("quickViewKeys", k, false);
    });
    keys.forEach((key) => closeEditForm(key));
    const removedCount = removeRowsFromItems(keys, itemCollection, setItemsState);
    if (removedCount) setPaginationState({ count: paginationState.count - removedCount });
  };
  function onDataGet(d: TableResultWithSchema) {
    batch(() => {
      if (fetchState.isLoading) setFetchState({ isLoading: false });

      if (d.error) {
        setFetchState({ authorized: false });
        if (typeof d === "string") setFetchState({ er: d.error });
        return;
      }
      setFetchState({ authorized: true });
      if (d.columnSchema) setSchemaAndOptionsState(d);
      if (d.r) {
        // reset quickViewKeys with empty array:
        // quickViewKeys = Array.from({length: d.length}, ()=>0);

        setItemsState("items", d.r.result ?? []);
        setPaginationState({ count: d.r.fullCount });
        setPaginationState({ current_page: d.r.pagination[2] }); // change page if not same.
        calcPagination();
        selectHelper(); // not perfect
        sortHelper();
        setMultiSelectState({
          selectedRowsKeys: dropNotExisting(
            multiSelectState.selectedRowsKeys,
            itemCollection.items.map((x) => x._key as string)
          ),
        });
        /// //////// setQueryParams(); // TODO FIX THIS
      } else if (d.n) {
        // do i set new data to state and then do sorting or do sorting and then set new data to the state?
        setItemsState("items", (prev) => [...prev, ...d.n.result]); // l.push(...d.n.result);
        /* .
            if (hightRowChange !== undefined) {
              const rowEle: HTMLElement | null = document.querySelector("[tree-id='" + dragData.dragId + "']");
              if (rowEle) {
                rowEle.style.backgroundColor = 'rgba(64,158,255,0.5)';
                setTimeout(() => {
                  rowEle.style.backgroundColor = 'rgba(64,158,255,0)';
                }, 2000);
              }
            }
          */
        setPaginationState({ count: paginationState.count + 1 });
      } else if (d.m) updateModifiedRow(d.m.result, itemCollection, setItemsState);
      else if (d.d) removeRows(d.d.result);
      if (d.n || d.m) doLocalSorting(sortState.colSortRow, columnState.selectedColumns, itemCollection, setItemsState);
    });
  }
  // Todo fix this:
  /* function setQueryParams() {
      if (syncQueryParams??true) {
        const q =
          '?limit=' +
          limit +
          '&page=' +
          itemCollection.current_page +
          '&sort=' +
          (all(x => x === SortDirection.None || x === null, itemCollection.headerColSortSettingsRow)
            ? JSON.stringify([])
            : JSON.stringify(itemCollection.headerColSortSettingsRow)) +
          '&filter=' +
          JSON.stringify(filterSettings);
        const pathAndSearch = window.location.pathname + q;
        if (pathAndSearch !== window.location.pathname + window.location.search && q + q !== window.location.search) {
          history.replaceState({ page: pathAndSearch }, '', pathAndSearch);
        }
      }
    } */
  /*= ======================================
    =            extra functions            =
    ======================================= */
  /* // can pass null to display nothing.
    const org_id_ctx = getContext('org_id');
    const org_id = org_id_ctx ? get(org_id_ctx) : '';
    const project_id_ctx = getContext('project_id');
    const project_id = project_id_ctx ? get(project_id_ctx) : ''; */
  /*= ====  End of extra functions  ====== */
  return {
    onDataGet,
  };
}
