import { produce, SetStoreFunction, Store } from 'solid-js/store';
import { changeElementTopLeftPositionWhereMouse, hideElement, showElement } from './tableUtils';
import { ColumnState, FilterState, PaginationState, TableDoms } from './table_types';
// Determine if a column is filterable
// Do Filters apply nested tables too?
//  make custom filter object fetch from database
//  make dynamic filter form for table too
// Make Filter Dialogs to easily input filter on table
// multiple filter rows support?
export const getInitialFilterState = (): FilterState => ({
  //filterSettings: props?.query?.filter ? JSON.parse(props.query.filter) : [],
  filterSettings: {} as { [key: string]: string | number | boolean; },
  headerColCustomFilter: [], // Todo Make it set by backend
});
export default function filterFns(
  doms: TableDoms,
  columnState: Store<ColumnState>,
  filterState: Store<FilterState>,
  setFilterState: SetStoreFunction<FilterState>,
  requiredFilter: any[],
  refresh: () => void,
  setPaginationState: SetStoreFunction<PaginationState>,
) {
  const onResetFilter = () => {
    const emptyObject: { [key: string]: string } = {};
    for (const key in requiredFilter) emptyObject[key] = requiredFilter[key];
    setFilterState({ filterSettings: emptyObject }); // clear filters
    refresh();
  };
  //store the timeout, cancel it on each change, then set a new one
  let filter_timeout: ReturnType<typeof window.setTimeout>;
  const onHandleFilterGeneral = (col: number, value: string | number | boolean) => {
    setFilterState(
      'filterSettings',
      produce(l => {
        if (value !== '') l[columnState.selectedColumns[col]] = value;
        else delete l[columnState.selectedColumns[col]]; // This not update parent
      }),
    );
    Object.keys(filterState.filterSettings).length == 0 && setFilterState({ filterSettings: {} }); // to update parent
    clearTimeout(filter_timeout);
    filter_timeout = setTimeout(()=>{
      // when filter applied, change current_page to 1 before fetching data, to prevent empty result
      setPaginationState({ current_page: 1 });
      refresh();
    }, 250);
  };
  const onHandleFilterText = (col: number) => (event: Event & { target: HTMLInputElement | HTMLSelectElement }) => event.target && onHandleFilterGeneral(col, event.target.value);
  const onHandleFilterBool = (col: number) => (event: Event & { target: HTMLInputElement }) => event.target && onHandleFilterGeneral(col, event.target.checked);
  //filter context-menu
  let inputheaderMenuDisplayed = false;
  let headerMenuColumn = 0;
  const globalOutSideInputContextMenuHandler = () => {
    closeInputMenu();
    window.removeEventListener('click', globalOutSideInputContextMenuHandler);
  };
  const onTextInputContext = (col: number) => (e: MouseEvent) => {
    e.preventDefault();
    if (doms.refContextMenuInputBox) {
      changeElementTopLeftPositionWhereMouse(e, doms.refContextMenuInputBox);
      showElement(doms.refContextMenuInputBox);
      inputheaderMenuDisplayed = true;
      headerMenuColumn = col;
    }
    window.addEventListener('click', globalOutSideInputContextMenuHandler);
  };
  const closeInputMenu = () => inputheaderMenuDisplayed && doms.refContextMenuInputBox && hideElement(doms.refContextMenuInputBox);
  return {
    onResetFilter,
    onHandleFilterText,
    onHandleFilterBool,
    onTextInputContext,
    closeInputMenu,
  };
}
