// Add Two modes on button press change classes
//  make two modes bordered / Overflow
//  use star component to show priorities.
// See very simple example: https://github.com/SanichKotikov/react-divtable
import clsx from "clsx";
import { isEmpty, mergeLeft } from "rambda";
import { isObj } from "ramda-adjunct";
import { JSX, Component, batch, createComputed, For, Match, onCleanup, Show, Switch, untrack, useContext } from "solid-js";
import { createStore, Store, produce, reconcile } from "solid-js/store";
import { Dynamic } from "solid-js/web";

import {
  DisplayType,
  DropPosition,
  ET,
  FailOrSuccessResult,
  GeneralError,
  getIsProduction,
  makeTableArgs,
  makeTableDropArgs,
  schemaEvents,
  SortDirection,
  TableResult,
  TableSchema,
  WsStatus,
} from "../../utils/enums";
import { getWsConnected, Ws } from "../../utils/ws_events_dispatcher";
import { FormServer } from "../formServer/FormServer";
import { ActionFooter } from "../UI/core/action-footer/src/ActionFooter";
import { ActionHeader } from "../UI/core/action-header/src/ActionHeader";
import { Alert } from "../UI/core/alert/src/Alert";
import { ButtonGroup } from "../UI/core/button-group/src/ButtonGroup";
import { Button, ButtonVariants } from "../UI/core/button/src/Button";
import { SvgIconAdd } from "../UI/core/icon/src/icon/IconAdd";
import { SvgIconClose } from "../UI/core/icon/src/icon/IconClose";
import { SvgIconCollapseRow } from "../UI/core/icon/src/icon/IconCollapseRow";
import { SvgIconEdit } from "../UI/core/icon/src/icon/IconEdit";
import { SvgIconExpandRow } from "../UI/core/icon/src/icon/IconExpandRow";
import { SvgIconKnurling } from "../UI/core/icon/src/icon/IconKnurling";
import { SvgIconList } from "../UI/core/icon/src/icon/IconList";
import { SvgIconTrash } from "../UI/core/icon/src/icon/IconTrash";
import { Spacer } from "../UI/core/spacer/src/Spacer";
import { Cell } from "../UI/core/table/src/subcomponents/_Cell";
import { HeaderCell } from "../UI/core/table/src/subcomponents/_HeaderCell";
// import { HeaderCheckMarkCell } from '../UI/core/table/src/subcomponents/_HeaderCheckMarkCell';
import { HeaderRow } from "../UI/core/table/src/subcomponents/_HeaderRow";
import { Row } from "../UI/core/table/src/subcomponents/_Row";
import { Toolbar } from "../UI/core/toolbar/src/Toolbar";
import { DialogModal } from "../UI/framework/dialog-modal/src/DialogModal";
import { Setting } from "../UI/Setting";
import { Skeleton } from "../UI/Skeleton";
import { Bool } from "./display/Bool";
import { Color } from "./display/Color";
import { TextView } from "./display/Text";
import { Time } from "./display/Time";
import { MakeUrl } from "./display/Url";
import { addFns } from "./logic/add";
import configFns, { getInitialConfigState } from "./logic/column_select";
import { deleteFns } from "./logic/delete";
import { dragData, DragItemTypes, getInitialDragState, setDragData, tableRowDrag } from "./logic/drag";
import { dragLineEvents, getInitialResizeState } from "./logic/drag_line";
import { editFns, getInitialEditState } from "./logic/edit";
import { expandFns } from "./logic/expand";
import filterFns, { getInitialFilterState } from "./logic/filter";
import { getInitialGroupByState } from "./logic/group_by";
import { tableContextMenuFns } from "./logic/header_context_menu";
import multiSelectFns, { getInitialMultiSelectState } from "./logic/mutli_select";
import paginationFns, { getInitialPaginationState } from "./logic/pagination";
import processHeaderAndDataReceived from "./logic/process_data";
import sortFns, { getInitialSortState } from "./logic/sort";
import { ColumnState, DragData, FetchConfig, FetchState, PaginationQury, ResizeState, TableDoms, ViewState } from "./logic/table_types";
import styles from "./Table.module.scss";
import { ProjectStackContext } from "../../layout/context";

document.body.ondrop = function (event) {
  event.preventDefault();
  // event.stopPropagation();
};
interface DraggableProps {
  isDraggable?: boolean;
  fixed?: string | boolean;
  height?: string | number;
  border?: boolean; // String // default true
  onlySameLevelCanDrag?: boolean;
  onHeightRowChange?: boolean; // String
  resize?: true; // String
  depth?: number;
  // beforeDragOver // Function
  // onDrag // Function
}

interface TableProps extends DraggableProps {
  uid: number;
  dontSendSubscribe: boolean;
  modelComponent: boolean;
  quickEditComponent: boolean;
  schemaKey: string;

  query?: PaginationQury; // To get arguments from ?limit=25&page=2
  requiredFilter?: []; // always add this filter when fetch // used when showing custom table
  fetchConfig: Omit<FetchConfig, "type" | "project">;
  syncQueryParams?: boolean; // default true
  buttonLabels?: {};
  tableBody?: (props: { rows: Store<TableResult> }) => JSX.Element;
  tableAction?: (props: { rowValue: { [key: string]: string | number | boolean } }) => JSX.Element;
  useLocalState?: boolean;
  deleteAction?: boolean;
  editAction?: boolean;
  showForm?: boolean;
}
interface EventsState {
  events: Array<string | null>;
  data_evt: Array<string | null>;
  unsub_evt: Array<string | null>;
  drag_evt: Array<string | null>;
}
export const Column = (props: { className: string; border?: boolean; width: number; flex: boolean; children?: any }) => {
  return (
    <div
      className={clsx(styles.treeColumn, { [styles.border]: props.border, [(styles as any)[props.className]]: props.className })}
      style={`width: ${props.width || 100}px; ${props.flex ? "flex: 1 1 0%;" : ""}`}
    >
      {props.children}
    </div>
  );
};
export const Table = (props: Readonly<TableProps>) => {
  const [getProject] = useContext(ProjectStackContext);
  const doms: TableDoms = {};

  const getInitialitemCollection = () => {
    return {
      items: [] as TableResult,
    };
  };
  const getInitialFetchConfigState = (): FetchConfig => ({
    project: getProject()?.[getProject().length - 1]?._key as string,
    ...props.fetchConfig,
  });
  const getInitialFetchState = (): FetchState => ({
    isLoading: true,
    authorized: true,
    er: "",
    options: {},
  });
  const getInitialColumnState = (): ColumnState => ({
    schema: {},
    selectedColumns: [],
    allIdsRow: {},
  });
  const getInitialViewState = (): ViewState => ({
    showRowNum: true,
    newFormVisible: false,
    addNewPos: "t",
    addNewType: "button",
    addNewLabels: { save: "Save", cancel: "Cancel" },
    rowType: "table", // unused
    showHeader: true,
    showButton: false,
  });

  const getInitialEventState = (): EventsState => ({ events: [], data_evt: [], unsub_evt: [], drag_evt: [] });
  const [fetchConfigState, setFetchConfigState] = createStore(getInitialFetchConfigState());
  const [fetchState, setFetchState] = createStore(getInitialFetchState());
  const [columnState, setColumnState] = createStore(getInitialColumnState());
  const [itemCollection, setitemCollection] = createStore(getInitialitemCollection());
  const [configState, setConfigState] = createStore(getInitialConfigState());
  const [sortState, setSortState] = createStore(getInitialSortState());
  const [filterState, setFilterState] = createStore(getInitialFilterState());
  const [paginationState, setPaginationState] = createStore(getInitialPaginationState(props.query));
  const [resizeState, setResizeState] = createStore(getInitialResizeState());
  const [, setGroupByState] = createStore(getInitialGroupByState());
  const [multiSelectState, setMultiSelectState] = createStore(getInitialMultiSelectState());
  const [editState, setEditState] = createStore(getInitialEditState());
  const [dragState, setDragState] = createStore(getInitialDragState(props.isDraggable));
  const [viewState, setViewState] = createStore(getInitialViewState());
  const [eventState, setEventState] = createStore(getInitialEventState());
  // let mutate_evt: number[] = [];
  const resetState = () => {
    batch(() => {
      setFetchConfigState(reconcile(getInitialFetchConfigState()));
      setFetchState(reconcile(getInitialFetchState()));
      setColumnState(reconcile(getInitialColumnState()));
      setitemCollection(reconcile(getInitialitemCollection()));
      setConfigState(reconcile(getInitialConfigState()));
      setSortState(reconcile(getInitialSortState()));
      setFilterState(reconcile(getInitialFilterState()));
      setPaginationState(reconcile(getInitialPaginationState(props.query)));
      setResizeState(reconcile(getInitialResizeState()));
      setGroupByState(reconcile(getInitialGroupByState()));
      setMultiSelectState(reconcile(getInitialMultiSelectState()));
      setEditState(reconcile(getInitialEditState()));
      setDragState(reconcile(getInitialDragState(props.isDraggable)));
      setViewState(reconcile(getInitialViewState()));
      setEventState(reconcile(getInitialEventState()));
    });
  };
  const refresh = () => {
    const args = makeTableArgs().setFilter(filterState.filterSettings).setSort(sortState.colSortRow).setPagination([paginationState.limit, 0, paginationState.current_page]);
    const e1 = [eventState.data_evt, { ...args, ...fetchConfigState, selectedColumns: columnState.selectedColumns }];
    Ws.trigger([e1]);
  };
  let previousSchema = "";
  createComputed(() => setFetchConfigState(reconcile(getInitialFetchConfigState())));
  createComputed(() => {
    if (!props.schemaKey) {
      setFetchState({ er: GeneralError.INVALID_SCHEMA });
    } else {
      if (previousSchema && previousSchema != props.schemaKey) {
        untrack(() => unSubscribeEvent(eventState.unsub_evt));
        untrack(resetState);
        previousSchema = props.schemaKey;
      }
      const events = schemaEvents(props.schemaKey);
      if (events) {
        const uid = props.uid || Ws.uid;
        setEventState({
          events,
          data_evt: [ET[ET.subscribe], events[0], uid],
          unsub_evt: [ET[ET.unsubscribe], events[0], uid],
          drag_evt: [ET[ET.changePosition], events[1], uid],
          // mutate_evt = [ET[ET.delete_], events[1], uid];
        });
        if (!props.dontSendSubscribe) untrack(refresh);
      } else {
        setFetchState({ er: GeneralError.NO_EVENTS });
      }
    }
  });
  const unSubscribeEvent = (e: Array<string | null>) => e.length && Ws.trigger([[e, {}]]); // TODO: unbind subscribe function
  onCleanup(() => unSubscribeEvent(eventState.unsub_evt));
  const calculateTableWidth = () => {
    // for (const r of columnState.selectedColumns) {
    // }
    // setResizeState({ tableWidth: width });
  };
  createComputed(calculateTableWidth);
  const addKeyToHiddenList = (key: string) => setColumnState("schema", key, { visible: false }); // Test this
  const setSchemaAndOptionsState = (d: TableSchema) => {
    if (!isObj(d)) {
      console.error("return value must be object");
      return;
    }
    batch(() => {
      setColumnState({
        schema: d.columnSchema ?? {},
        selectedColumns: d.selectedColumns ?? [],
        allIdsRow: d.allColumns ?? {},
      });
      setFetchState({ options: d.tableProps });
      if (getIsProduction() && !fetchState.options.showKeyRev) {
        addKeyToHiddenList("_key");
        addKeyToHiddenList("_rev");
      }
      setSortState({ colSortRow: d.sort ?? {} });
      setResizeState(
        "headerColWidthRow",
        produce<ResizeState["headerColWidthRow"]>((l) => {
          mergeLeft(l, d.width ?? {});
        })
      );
      // calculateTableWidth();
      // setFilterState({ filterSettings: {}}) if we can get filterSettings from server.
      setViewState({
        addNewPos: fetchState.options?.add?.pos ?? "t",
        addNewType: fetchState.options?.add?.type ?? "button",
        showHeader: fetchState.options?.table?.header ?? true,
        rowType: fetchState.options?.table?.row ?? "table",
      });
      if (viewState.addNewType == "button") setViewState({ showButton: true });
      else setViewState({ newFormVisible: true });
      const addNewLabels = fetchState.options?.add?.l;
      addNewLabels && setViewState({ addNewLabels });
      "tree" in fetchState.options && setDragState({ isTree: fetchState.options.tree });
      "drag" in fetchState.options && setDragState({ isDraggable: fetchState.options.drag });
    });
  };
  let firstTime = true;
  createComputed(() => {
    if (getWsConnected()) {
      if (!firstTime) {
        setFetchState({ er: GeneralError.NO_ERROR });
        untrack(refresh);
      } else {
        firstTime = false;
      }
    } else if (props.syncQueryParams ?? true) setFetchState({ er: WsStatus.RECONNECTION });
  });
  const { toggleAddForm } = addFns(doms, viewState, setViewState);
  const { closeEditForm, editSuccessSave, onEditSvgKeyPress, onEditSvgClick } = editFns(eventState.events, editState, setEditState);
  const { onDeleteSvgKeyPress, onDeleteOneRow, onDeleteSelected } = deleteFns(eventState.events, multiSelectState, fetchState, setFetchState, fetchConfigState);
  const { onResetFilter, onHandleFilterText, onHandleFilterBool, onTextInputContext, closeInputMenu } = filterFns(
    doms,
    columnState,
    filterState,
    setFilterState,
    props.requiredFilter ?? [],
    refresh,
    setPaginationState
  );
  const { calcPagination, onLimitChange, onPageChange } = paginationFns(paginationState, setPaginationState, refresh);
  const { sortHelper, onHandleSort } = sortFns(columnState, sortState, setSortState, refresh);
  const { selectHelper, onSelectRowClick, onSelectAllClick } = multiSelectFns(itemCollection, multiSelectState, setMultiSelectState);

  const getBindData = () => {
    const { onDataGet } = processHeaderAndDataReceived(
      setSchemaAndOptionsState,
      itemCollection,
      setitemCollection,
      columnState,
      setColumnState,
      fetchState,
      setFetchState,
      paginationState,
      setPaginationState,
      sortState,
      setSortState,
      filterState,
      setFilterState,
      editState,
      setEditState,
      multiSelectState,
      setMultiSelectState,
      calcPagination,
      sortHelper,
      selectHelper,
      closeEditForm
    );
    return onDataGet;
  };
  let prevUnbindFn: Array<() => void> = [];
  createComputed(() => {
    if (props.schemaKey) {
      const onDataGet = getBindData();
      prevUnbindFn.forEach((unbindFn) => unbindFn());
      prevUnbindFn = [
        Ws.bind$(
          (eventState.data_evt as string[]).map((e, i) => (i == 0 ? `${e}_confirm` : e)),
          onDataGet,
          1
        ),
        Ws.bind$(eventState.data_evt as string[], onDataGet, 1),
      ];
    }
  });
  onCleanup(() => prevUnbindFn.forEach((unbindFn) => unbindFn()));
  const { onConfigClicked, onConfigApply, onColumnHide } = configFns(
    configState,
    setConfigState,
    columnState,
    setColumnState,
    fetchState,
    setFetchState,
    fetchConfigState,
    refresh
  );
  const { onDraggingOver, drop } = tableRowDrag(eventState.drag_evt as string[], itemCollection, dragState, setDragState, props.onHeightRowChange);
  const { ZipAll, OpenAll, toggleExpandedRowsKeys } = expandFns(itemCollection, dragState, setDragState);
  const { onHeaderContext, closeHeaderMenu, onHandleSortFromContext } = tableContextMenuFns(doms, onHandleSort);
  const { mousedown } = dragLineEvents(doms, setResizeState);

  function onShowRowNum() {
    setViewState({ showRowNum: !viewState.showRowNum });
  }
  const operationsCount = 2;
  // components:================
  // width: ${resizeState.tableWidth ?? 500}px
  const TableWrapper: Component = (p) => <div style={`margin-left: ${(props.depth ?? 0) * 10}px;`}>{p.children}</div>;

  const AddNew = () => (
    <>
      <Show when={viewState.showButton && !viewState.newFormVisible}>
        <Button
          name="table_add"
          variant={ButtonVariants.EMPHASIS}
          className={clsx([{ [styles.pressed]: viewState.newFormVisible }])}
          ref={(el) => (doms.addButton = el)}
          onClick={toggleAddForm}
          title={!viewState.newFormVisible ? "Add New" : "Close"}
          isIconOnly
          icon={viewState.newFormVisible ? SvgIconClose : SvgIconAdd}
        />
      </Show>
      <Show when={viewState.newFormVisible && (props.sendFormSchemaRequest ?? true)}>
        <FormServer
          key=""
          schemaKey={props.schemaKey}
          fetchConfig={fetchConfigState}
          buttonLabels={viewState.addNewLabels}
          onCancel={toggleAddForm}
          handleSubmit={toggleAddForm}
          resetOnSubmit={fetchState.options?.add?.type === "open"}
        />
      </Show>
    </>
  );
  const FilterButtonGroup: Component = () => (
    <Show when={!!Object.keys(filterState.filterSettings).length}>
      <Spacer paddingRight="small">
        <ButtonGroup>
          <Button onClick={onResetFilter} text="Reset Filters" />
        </ButtonGroup>
      </Spacer>
    </Show>
  );
  const Config = () => (
    <>
      <Button onClick={onConfigClicked} isIconOnly text="Columns" icon={SvgIconKnurling} />

      <Show when={configState.config}>
        <ul>
          <For each={Object.keys(columnState.allIdsRow)}>
            {(id, i) => (
              <Show when={!["_key", "_rev", "children_count"].includes(id)}>
                <li>
                  <input type="checkbox" checked={columnState.selectedColumns.includes(id)} onClick={onColumnHide(id)} />
                  {columnState.allIdsRow[id]}
                </li>
              </Show>
            )}
          </For>
        </ul>
      </Show>
    </>
  );
  const Pagination = () => (
    <div>
      <span>
        {paginationState.count}
        {paginationState.count <= 1 ? " item " : " items "}
      </span>
      Page Size:
      <input className={styles.w60} type="number" value={paginationState.limit} onChange={onLimitChange} min="0" title="press Enter/Tab" />
      Page:
      <select value={paginationState.current_page} onChange={onPageChange}>
        <For each={paginationState.pages}>{(p) => <option value={p}>{p}</option>}</For>
      </select>
      {" / "} {paginationState.total_pages}
    </div>
  );
  const [state, setState] = createStore({
    isOpen: false,
  });
  const handleOpenModal = () => {
    setState({ isOpen: true });
  };
  const handleCloseModal = () => {
    setState({ isOpen: false });
  };

  const TableButtonsGroup = () => (
    <>
      <ButtonGroup>
        <Show when={multiSelectState.multipleSelected}>
          {state.isOpen && (
            <DialogModal
              ariaLabel="Delete Confirmation Dialog Modal"
              isOpen={state.isOpen}
              onRequestClose={handleCloseModal}
              header={<ActionHeader title="Confirmation" onClose={handleCloseModal} />}
              footer={<ActionFooter end={<Button icon={SvgIconTrash} text="Confirm Delete" name="delete" onClick={onDeleteSelected} />} />}
            >
              <p>The process is not reversible, Are You Sure to delete selected rows?</p>
            </DialogModal>
          )}
          <Button icon={SvgIconTrash} isIconOnly text="Delete" name="delete" onClick={handleOpenModal} />
        </Show>
        <Show when={!getIsProduction()}>
          <Button onClick={onShowRowNum} text="Row Numbers" isIconOnly icon={SvgIconList} />
        </Show>
        <Config />
      </ButtonGroup>

      <Setting
        schemaKey={props.schemaKey}
        getNewSaveSettings={() => {
          return { filter: filterState.filterSettings, sort: sortState.colSortRow };
        }}
        applyNewSetting={(v: { sort?: {}; filter?: {} }) => {
          batch(() => {
            if (v.sort) setSortState({ colSortRow: v.sort });
            if (v.filter) setFilterState({ filterSettings: v.filter });
            refresh();
          });
        }}
      />
      <Show when={dragState.isTree}>
        <ButtonGroup>
          <Button onClick={OpenAll} text="Expand All" isIconOnly icon={SvgIconExpandRow} />
          <Button onClick={ZipAll} text="Collapse All" isIconOnly icon={SvgIconCollapseRow} />
        </ButtonGroup>
      </Show>
    </>
  );
  const MyHeaderCell = (p: { index: () => number; id: string }) => {
    return (
      <div onClick={onHandleSort(p.index())} onContextMenu={onHeaderContext(p.index())}>
        {columnState.allIdsRow[p.id]}
        <Switch>
          <Match when={sortState.colSortRow[columnState.selectedColumns[p.index()]] === SortDirection.Ascending}>
            ▲{Object.keys(sortState.colSortRow).length > 1 && <sup>{Object.keys(sortState.colSortRow).findIndex((x) => x == columnState.selectedColumns[p.index()]) + 1}</sup>}
          </Match>
          <Match when={sortState.colSortRow[columnState.selectedColumns[p.index()]] === SortDirection.Descending}>
            ▼{Object.keys(sortState.colSortRow).length > 1 && <sup>{Object.keys(sortState.colSortRow).findIndex((x) => x == columnState.selectedColumns[p.index()]) + 1}</sup>}
          </Match>
        </Switch>
      </div>
    );
  };
  const TableHeader = () => (
    <HeaderRow
    // className={clsx(styles.dragTreeTableHeader)}
    >
      {/* <HeaderCheckMarkCell isSelectable={true} isSelected={multiSelectState.allSelected} onSelect={onSelectAllClick} */}
      <HeaderCell
        width={{ static: { value: 25, unit: "px" } }}
        // border={props.border ?? true} className={clsx([styles.alignCenter])}
      >
        <input type="checkbox" checked={multiSelectState.allSelected} onClick={onSelectAllClick} />
        {/* <div className={styles.resizeLine} onMouseDown={mousedown('0')} /> */}
      </HeaderCell>
      <Show when={viewState.showRowNum}>
        <HeaderCell
          width={{ static: { value: 50, unit: "px" } }}
          // border={props.border} className={clsx([styles.alignCenter])}
        >
          <span>No</span>
          <div className={styles.resizeLine} onMouseDown={mousedown("1")} />
        </HeaderCell>
      </Show>
      <For each={columnState.selectedColumns}>
        {(id, i) => (
          <Show when={columnState.schema[id]?.visible ?? true}>
            <HeaderCell
              width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
              // flex={false} border={props.border} className={clsx([styles.alignCenter])}
            >
              <MyHeaderCell index={i} id={id} />
              <div className={clsx([{ [styles.resizing]: resizeState.resizingColId === id }, "resizeLine"])} onMouseDown={mousedown(id)} />
            </HeaderCell>
          </Show>
        )}
      </For>
      <HeaderCell
        width={{ static: { value: operationsCount * 50, unit: "px" } }}
        flex={false}
        border={props.border}
        className={`align-${"center"} colIndex${2 + columnState.selectedColumns.length}`}
      >
        <span>Actions</span>
        <div className={styles.resizeLine} onMouseDown={mousedown("actions")} />
      </HeaderCell>
    </HeaderRow>
  );
  const TableFilterRow = () => (
    <HeaderRow
    // className={clsx(styles.dragTreeTableHeader)}
    >
      <HeaderCell
        width={{ static: { value: 25, unit: "px" } }} // flex={false} border={props.border} className={clsx([styles.alignCenter])}
      >
        <span />
        {/* <div className={styles.resizeLine} onMouseDown={mousedown('0')} /> */}
      </HeaderCell>
      <Show when={viewState.showRowNum}>
        <HeaderCell width={{ static: { value: 50, unit: "px" } }} flex={false} border={props.border} className={clsx([styles.alignCenter])}>
          <span />
          <div className={styles.resizeLine} onMouseDown={mousedown("1")} />
        </HeaderCell>
      </Show>
      <For each={columnState.selectedColumns}>
        {(id, i) => (
          <Show when={columnState.schema[id]?.visible ?? true}>
            <Switch fallback={<>!No Column Type Found!</>}>
              <Match when={filterState.headerColCustomFilter[i()]}>
                <HeaderCell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  flex={false}
                  border={props.border}
                  className={clsx([styles.alignCenter])}
                >
                  <select value={filterState.filterSettings[columnState.selectedColumns[i()]] as string} onChange={onHandleFilterText(i())}>
                    <For each={filterState.headerColCustomFilter[i()]}>{(f: string[]) => <option value={f[1]}>{f[0]}</option>}</For>
                  </select>
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </HeaderCell>
              </Match>
              <Match
                when={
                  columnState.schema[id]?.type === DisplayType.Number ||
                  columnState.schema[id]?.type === DisplayType.Text ||
                  columnState.schema[id]?.type === DisplayType.Double ||
                  columnState.schema[id]?.type === DisplayType.Url
                }
              >
                <HeaderCell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  flex={false}
                  border={props.border}
                  className={clsx([styles.alignCenter])}
                >
                  <input
                    type="search"
                    className={styles.search}
                    placeholder=" &#128269;"
                    value={(filterState.filterSettings[columnState.selectedColumns[i()]] as string) || ""}
                    onInput={onHandleFilterText(i())}
                    onContextMenu={onTextInputContext(i())}
                  />
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </HeaderCell>
              </Match>
              <Match when={columnState.schema[id]?.type === DisplayType.Checkbox}>
                <HeaderCell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  // flex={false} border={props.border} className={clsx([styles.alignCenter])}
                >
                  <input className={styles.checkbox} type="checkbox" checked={!!filterState.filterSettings[columnState.selectedColumns[i()]]} onChange={onHandleFilterBool(i())} />
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </HeaderCell>
              </Match>
              <Match when={columnState.schema[id]?.type === DisplayType.DateTime}>
                <HeaderCell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  // flex={false} border={props.border} className={clsx([styles.alignCenter])}
                >
                  <span>Date Fix Me</span>
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </HeaderCell>
              </Match>
              <Match when={columnState.schema[id]?.type === DisplayType.Url}>fix</Match>
              <Match when={columnState.schema[id]?.type === DisplayType.Color}>
                <Cell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  // flex={false} border={props.border} className={clsx([styles.alignCenter])}
                >
                  <span>Fix me</span>
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </Cell>
              </Match>
              <Match when={columnState.schema[id]?.type === DisplayType.Color}>
                <HeaderCell
                  width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }}
                  // flex={false} border={props.border} className={clsx([styles.alignCenter])}
                >
                  <span>Unknown Type {columnState.schema[id]?.type}</span>
                  <div className={styles.resizeLine} onMouseDown={mousedown(id)} />
                </HeaderCell>
              </Match>
            </Switch>
          </Show>
        )}
      </For>
      <HeaderCell width={operationsCount * 50} flex={false} border={props.border} className={clsx([styles.alignCenter])}>
        <div className={styles.resizeLine} onMouseDown={mousedown("actions")} />
      </HeaderCell>
    </HeaderRow>
  );
  const RowNew = (p: { rowValue: { [key: string]: string | number | boolean }; rowIndex: () => number; key: string }) => {
    const [rowState, setRowState] = createStore({ isFolder: false, dropState: DropPosition.none });
    createComputed(() => {
      setRowState({ isFolder: !!(dragState.isTree && p.rowValue && p.rowValue.children_count) });
    });
    const toggle = () => rowState.isFolder && toggleExpandedRowsKeys(p.key);
    let row: HTMLDivElement | null = null;
    const setRef = (el: HTMLDivElement) => (row = el);
    const dragstart = (e: DragEvent & { target: HTMLDivElement }) => {
      setDragData({
        type: DragItemTypes.TABLE_ROW,
        dragX: 0,
        dragY: 0,
        key: p.key,
        dragId: p.key,
        dragRevId: p.rowValue._rev as string,
        dragPId: p.parentKey || null,
        dragParentNode: e.target,
        targetId: "",
        targetRevId: "",
        whereInsert: DropPosition.none,
      });
      // The data is only available on drop, this is a security feature since a website could grab data when you happen to be dragging something across the webpage.
      // https://stackoverflow.com/questions/28487352/dragndrop-datatransfer-getdata-empty
      // Firefox drag have a bug // unused
      // Required to make it work in Firefox, but not used in this example.
      if (e.dataTransfer) e.dataTransfer.setData("source", p.key); // data should be string
      e.target.style.opacity = "0.2";
    };
    const onDraggingOverRow = (e: DragEvent): boolean => {
      // https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/Drag_operations#droptargets
      e.stopPropagation();
      e.preventDefault();
      if (e.dataTransfer) e.dataTransfer.dropEffect = "move";
      const isSourceData = e.dataTransfer ? e.dataTransfer.types.includes("source") : false;
      if (!isSourceData) return false;
      if (isEmpty(dragData)) return false;
      setDragState({ isDragging: false });
      if (e.pageX == dragData.dragX && e.pageY == dragData.dragY) return true;
      setDragData({
        dragX: e.pageX,
        dragY: e.clientY,
        targetId: p.key,
        targetRevId: p.rowValue._rev as string,
      });
      filter(e.pageX, e.clientY, dragData);
      if (e.clientY < 100) {
        window.scrollTo(0, scrollY - 6);
      } else if (e.clientY > document.body.clientHeight - 160) {
        window.scrollTo(0, scrollY + 6);
      }
      return true;
    };
    const dropRow = async (e: DragEvent) => {
      e.preventDefault();
      e.stopPropagation();
      // not work:
      // const isSourceData = e.dataTransfer.types.includes('source')
      // if (!isSourceData) return
      if (isEmpty(dragData) || dragData.type != DragItemTypes.TABLE_ROW) {
        return;
      }
      // fix it should return in some time limit.
      const d: FailOrSuccessResult = await new Promise((resolve, reject) => {
        Ws.bindT(
          eventState.drag_evt,
          (d) => {
            resolve(d);
          },
          makeTableDropArgs().setValue([dragData.dragId, dragData.dragRevId, dragData.targetId, dragData.targetRevId, dragData.whereInsert])
        );
      });
      if (d.error) {
        clearHoverStatus();
        setDragState({ isDragging: false });
        return;
      }
      clearHoverStatus();
      // resetTreeData(dragData); //Main Function // No need anymore
      setDragState({ isDragging: false });
      // no highlighRowChange // No need anymore
      setDragData({});
    };
    // todo: fix only one line, this function is unsed while draggging over itemCollection.items.
    //* 1. get all tree row
    //* 2. this just show hover position
    //*
    // Find matching lines, handle drag and drop styles
    function filter(x: number, y: number, sourceData: DragData) {
      if (!sourceData.dragParentNode) return;
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect
      const dragRect = sourceData.dragParentNode.getBoundingClientRect();
      const dragW = dragRect.left + sourceData.dragParentNode.clientWidth;
      const dragH = dragRect.top + sourceData.dragParentNode.clientHeight;
      if (x >= dragRect.left && x <= dragW && y >= dragRect.top && y <= dragH) {
        // The original block currently being dragged is not allowed to be inserted
        // console.log('same element !');
        return;
      }
      if (!row) return;
      const rect = row.getBoundingClientRect();
      const rx = rect.left;
      const ry = rect.top;
      const rw = row.clientWidth;
      const rh = row.clientHeight;
      if (x > rx && x < rx + rw && y > ry && y < ry + rh) {
        const diffY = y - ry;
        const pId = p.parentKey || null; // It is not allowed to change the hierarchical structure, only the upper and lower order logic
        if (dragState.onlySameLevelCanDrag && pId !== sourceData.dragPId) {
          return;
        }
        setDragData({ targetId: p.key });
        setDragData({ targetRevId: p.rowValue._rev as string });
        const rowHeight = row.offsetHeight;
        if (diffY / rowHeight > 3 / 4) {
          setDragData({ whereInsert: DropPosition.bottom });
        } else if (diffY / rowHeight > 1 / 4) {
          if (dragState.onlySameLevelCanDrag) {
            // It is not allowed to change the hierarchical structure, only the upper and lower order logic
            return;
          }
          setDragData({ whereInsert: DropPosition.center });
        } else {
          setDragData({ whereInsert: DropPosition.top });
        }
      }
      if (!dragData.targetId) {
        // Can't match to clear the previous state
        clearHoverStatus();
        setDragData({ whereInsert: DropPosition.none });
        return;
      }
      // let canDrag: boolean = true;
      // Todo can disable some row drag and drop. set canDrag == false.
      // if (beforeDragOver) { //todo fix this
      // const curRow = getItemById(itemCollection.items, sourceData.dragId)
      // const targetRow = getItemById(itemCollection.items, targetIdTemp)
      // canDrag = beforeDragOver(curRow, targetRow, whereInsert)
      // }
      // if (canDrag == false) return;
      // let rowHeight = row.offsetHeight;
      if (dragData.whereInsert == DropPosition.bottom) {
        setRowState({ dropState: DropPosition.bottom });
      } else if (dragData.whereInsert == DropPosition.center) {
        setRowState({ dropState: DropPosition.center });
      } else {
        setRowState({ dropState: DropPosition.top });
      }
    }
    const clearHoverStatus = () => setRowState({ dropState: DropPosition.none });
    function onDragEnd(e: DragEvent & { currentTarget: HTMLDivElement; target: HTMLDivElement }) {
      e.preventDefault();
      if (e.target) e.target.style.opacity = "1";
    }
    const onDragLeave = (e: DragEvent) => {
      // solution taken from: https://stackoverflow.com/a/42715014/4372670
      if (row) {
        const rect = row.getBoundingClientRect();
        if (e.clientY < rect.top || e.clientY >= rect.bottom || e.clientX < rect.left || e.clientX >= rect.right) {
          // real leave
          e.stopPropagation();
          e.preventDefault();
          clearHoverStatus();
        }
      }
    };
    const highlight = false; // todo: based on headerColPropsRow[id] logic
    const [state, setState] = createStore({
      isOpen: false,
    });
    const handleOpenModal = () => {
      setState({ isOpen: true });
    };
    const handleCloseModal = () => {
      setState({ isOpen: false });
    };
    return (
      <>
        <Row
          // className={styles.treeBlock}
          draggable={dragState.isDraggable && sortState.sortedByPosition}
          onDragStart={dragstart}
          onDragEnd={onDragEnd}
          onDragEnter={onDraggingOverRow}
          onDragOver={onDraggingOverRow}
          onDragLeave={onDragLeave}
          onDrop={dropRow}
        >
          <div ref={setRef} className={clsx(styles.treeRow, { [styles.highlightRow]: highlight })} onClick={toggle} data-level={props.depth ?? 0}>
            <Cell
              width={{ static: { value: 25, unit: "px" } }} // className={clsx([styles.alignCenter])} width={25} flex={false} border={props.border}
            >
              <Show when={!false}>
                <input type="checkbox" checked={multiSelectState.selectedRowsKeys[p.key]} onClick={onSelectRowClick(p.key)} />
              </Show>
            </Cell>
            <Show when={viewState.showRowNum}>
              <Cell
                width={{ static: { value: 50, unit: "px" } }} // className={clsx([styles.alignCenter])} width={50} flex={false} border={props.border}
              >
                <span>
                  {/* <Space {depth ?? 0} */}
                  <Switch fallback={<span className={clsx([styles.zipIcon, styles.arrowTransparent])} />}>
                    <Match when={rowState.isFolder}>
                      <span className={clsx([styles.zipIcon, dragState.expandedRowsKeys[p.key] ? "arrowBottom" : "arrowRight"])} />
                    </Match>
                  </Switch>
                  <span />
                </span>
                <span>{p.rowIndex() + 1}</span>
              </Cell>
            </Show>
            <For each={columnState.selectedColumns}>
              {(id) => (
                <Show when={columnState.schema[id]?.visible ?? true}>
                  <Cell
                    width={{ static: { value: resizeState.headerColWidthRow?.[id] ?? 100, unit: "px" } }} // className={clsx([styles.alignCenter])} width={resizeState.headerColWidthRow?.[id] ?? 100} flex={false} border={props.border}
                  >
                    <div>
                      <Switch>
                        <Match when={columnState.schema[id]?.editable ?? false}>GeneralForm Fix me</Match>
                        <Match when={p.rowValue[id] != null}>
                          <Switch fallback={<TextView value={p.rowValue[id] as string} props={{}} />}>
                            <Match when={columnState.schema[id]?.type === DisplayType.DateTime}>{new Date(p.rowValue[id] as number).toLocaleString()}</Match>
                            <Match when={columnState.schema[id]?.type === DisplayType.Url}>
                              <MakeUrl {...(columnState.schema[id]?.props ?? {})} id={p.rowValue[id] as string} value={p.rowValue[id] as string} />
                            </Match>
                            <Match when={columnState.schema[id]?.type === DisplayType.Checkbox}>
                              <Bool value={p.rowValue[id] as boolean} />
                            </Match>
                            <Match when={columnState.schema[id]?.type === DisplayType.Color}>
                              <Color value={p.rowValue[id] as string} props={{}} />
                            </Match>
                            <Match when={columnState.schema[id]?.type === DisplayType.Date}>
                              <Time value={p.rowValue[id] as string} props={{}} />
                            </Match>
                          </Switch>
                        </Match>
                      </Switch>
                    </div>
                  </Cell>
                </Show>
              )}
            </For>
            <Cell
              width={{ static: { value: 100, unit: "px" } }} // className={clsx([styles.alignCenter])} width={100} flex={false} border={props.border}
            >
              <Show when={(props.editAction ?? true) && !!props.quickEditComponent && !editState.quickViewKeys[p.rowValue._key as string]}>
                <Button icon={SvgIconEdit} isIconOnly text="Edit" name="edit" onClick={onEditSvgClick(p.key)} />
              </Show>
              <Show when={(props.deleteAction ?? true) && !editState.quickViewKeys[p.rowValue._key as string]}>
                {state.isOpen && (
                  <DialogModal
                    ariaLabel="Delete Confirmation Dialog Modal"
                    isOpen={state.isOpen}
                    onRequestClose={handleCloseModal}
                    header={<ActionHeader title="Confirmation" onClose={handleCloseModal} />}
                    footer={
                      <ActionFooter
                        end={
                          <Button
                            icon={SvgIconTrash}
                            text="Confirm Delete"
                            name="delete"
                            onClick={async (e) => {
                              await onDeleteOneRow(p.key)(e);
                              handleCloseModal();
                            }}
                          />
                        }
                      />
                    }
                  >
                    <p>The process is not reversible, Are you sure?</p>
                  </DialogModal>
                )}
                <Button icon={SvgIconTrash} isIconOnly text="Delete" name="delete" onClick={handleOpenModal} />
              </Show>
            </Cell>
            <Show when={props.tableAction}>
              <Cell>
                <Dynamic component={props.tableAction} rowValue={p.rowValue} />
              </Cell>
            </Show>
            <Show when={rowState.dropState !== DropPosition.none}>
              <div className={styles.hoverModel}>
                <Switch>
                  <Match when={rowState.dropState === DropPosition.top}>
                    <div className={clsx([styles.hoverBlock, styles.prevBlock])}>
                      <i className="el-icon-caret-top" />
                    </div>
                  </Match>
                  <Match when={rowState.dropState === DropPosition.center}>
                    <div className={clsx([styles.hoverBlock, styles.centerBlock])}>
                      <i className="el-icon-caret-right" />
                    </div>
                  </Match>
                  <Match when={rowState.dropState === DropPosition.bottom}>
                    <div className={clsx([styles.hoverBlock, styles.nextBlock])}>
                      <i className="el-icon-caret-bottom" />
                    </div>
                  </Match>
                </Switch>
              </div>
            </Show>
          </div>
        </Row>
        <Show when={editState.quickViewKeys[p.key]}>
          <Row>
            <div className={styles.treeBlock}>
              <div className={styles.treeRow}>
                <div>
                  <Show when={props.quickEditComponent}>
                    <FormServer
                      key={p.key}
                      schemaKey={props.schemaKey}
                      fetchConfig={fetchConfigState}
                      onCancel={editSuccessSave}
                      handleSubmit={editSuccessSave}
                      onDeleteOneRow={onDeleteOneRow}
                    />
                  </Show>
                </div>
              </div>
            </div>
          </Row>
        </Show>
        <Show when={rowState.isFolder}>
          <Show when={dragState.expandedRowsKeys[p.key]}>
            <Table
              depth={(props.depth ?? 0) + 1}
              schemaKey={props.schemaKey}
              fetchConfig={{ ...fetchConfigState, parent: p.rowValue._key as string }}
              syncQueryParams={false}
              modelComponent={props.quickEditComponent}
              quickEditComponent={props.quickEditComponent}
            />
          </Show>
        </Show>
      </>
    );
  };
  const TableBody = () => (
    <div
      className={clsx(styles.dragTreeTableBody, { [styles.isDragging]: dragState.isDragging })}
      style={`overflow: ${props.fixed !== undefined && props.fixed !== false ? "auto" : "hidden"}; height: ${
        props.fixed !== undefined && props.fixed !== false ? `${props.height || 400}px` : "auto"
      }`}
      // onDragOver={!props.depth ? onDraggingOver : undefined}
      // onDragEnd={!props.depth ? drop : undefined}
    >
      <For each={itemCollection.items}>{(r, i) => <RowNew rowValue={r} rowIndex={i} key={r._key as string} />}</For>
    </div>
  );
  const ContextMenu = () => (
    <>
      <div className={clsx([styles.ContextMenu])} ref={doms.refContextMenuBox}>
        <div className={clsx([styles.ContextMenuItem])} onClick={onHandleSortFromContext(SortDirection.Ascending)}>
          Sort Ascending
        </div>
        <div className={clsx([styles.ContextMenuItem])} onClick={onHandleSortFromContext(SortDirection.Descending)}>
          Sort Descending
        </div>
        <div className={clsx([styles.ContextMenuItem])} onClick={onHandleSortFromContext(SortDirection.None)}>
          No Sorting
        </div>
        <hr />
        <div className={clsx([styles.ContextMenuItem])} onClick={closeHeaderMenu}>
          Close
        </div>
      </div>
      <div className={clsx([styles.ContextMenuInput])} ref={doms.refContextMenuInputBox}>
        <div className={clsx([styles.ContextMenuItem])}>Is NULL</div>
        <div className={clsx([styles.ContextMenuItem])}>Is not NULL</div>
        <div className={clsx([styles.ContextMenuItem])}>Is empty</div>
        <div className={clsx([styles.ContextMenuItem])}>Is not empty</div>
        <hr />
        <div className={clsx([styles.ContextMenuItem])}>Equal to...</div>
        <div className={clsx([styles.ContextMenuItem])}>Not equal to...</div>
        <div className={clsx([styles.ContextMenuItem])}>Greater than...</div>
        <div className={clsx([styles.ContextMenuItem])}>Less than...</div>
        <div className={clsx([styles.ContextMenuItem])}>Greater or equal...</div>
        <div className={clsx([styles.ContextMenuItem])}>Less or equal...</div>
        <div className={clsx([styles.ContextMenuItem])}>In range...</div>
        <hr />
        <div className={clsx([styles.ContextMenuItem])} onClick={closeInputMenu}>
          Close
        </div>
      </div>
    </>
  );
  const TableWrap = (p: { children?: JSX.Element }) => (
    <div className={clsx(styles.dragTreeTable, { border: props.border })} ref={doms.table}>
      {p.children}
    </div>
  );
  // note : header and filter are loop through headerColselectedColumns, but items loops through rowValue

  return (
    <TableWrapper>
      {fetchState.er && <Alert type="error">{fetchState.er}</Alert>}
      <Show when={(props.showForm ?? true) && viewState.addNewPos === "t"}>
        <AddNew />
      </Show>
      <Show when={!!columnState.selectedColumns.length}>
        <Show when={viewState.showHeader}>
          <Toolbar align="start">
            <FilterButtonGroup />
            <TableButtonsGroup />
          </Toolbar>
        </Show>

        <TableWrap>
          <Show when={viewState.showHeader}>
            <TableHeader />
            <TableFilterRow />
          </Show>
          {props.tableBody ? <Dynamic component={props.tableBody} rows={itemCollection.items} /> : <TableBody />}
        </TableWrap>

        <Show when={viewState.showHeader}>
          <Pagination />
        </Show>
        <Show when={(props.showForm ?? true) && viewState.addNewPos === "b"}>
          <AddNew />
        </Show>
      </Show>
      <ContextMenu />
      <Show when={fetchState.isLoading}>
        <Skeleton />
      </Show>
      {/* {JSON.stringify(itemCollection.items)} */}
    </TableWrapper>
  );
};
//          <Show when={fetchState.authorized}>{props.tableBody ? <Dynamic component={props.tableBody} rows={itemCollection.items} /> : <TableBody />}</Show>
