// https://github.com/ccssmnn/lumino-react
// https://github.com/ccssmnn/lumino-react/blob/0babd3689e/src/features/widgets/Lumino.tsx
// https://jupyterlab.github.io/lumino/widgets/globals.html
// https://github.com/jupyterlab/lumino
import { BoxPanel, DockPanel, Widget } from '@lumino/widgets';
import './Lumino.css';
import { Accessor, Component, createComputed, createEffect, createSignal, onCleanup } from 'solid-js';
import { render } from 'solid-js/web';
import { Setter } from 'solid-js/types/reactive/signal';

export interface AppWidget {
  type: string;
  id: string;
  tabTitle: string;
  active: boolean;
  component: Component;
  onClose?: () => void;
}

/**
 * This is the type of the custom event we use to communicate from lumino to react/redux
 */
export interface LuminoEvent {
  detail: { widget: LuminoWidget; id: string; name: string; closable: boolean; onClose?: () => void };
}

/**
 * LuminoWidget allows us to fire custom events to the HTMLElement that is holding all
 * the widgets. This approach handles the plumbing between Lumino and React/Redux
 */
// https://lumino.readthedocs.io/en/latest/api/widgets/classes/widget.html
export class LuminoWidget extends Widget {
  name: string; // will be displayed in the tab

  closable: boolean; // make disable closing on some widgets if you want

  mainRef: HTMLDivElement; // reference to the element holding the widgets to fire events

  onClose: () => void;

  constructor(id: string, name: string, mainRef: HTMLDivElement, closable: boolean, onClose?: () => void) {
    super({ node: LuminoWidget.createNode(id) });

    this.id = id;
    this.name = name;
    this.mainRef = mainRef;
    this.closable = closable;
    this.onClose = onClose;

    this.setFlag(Widget.Flag.DisallowLayout);
    this.addClass('content');

    this.title.label = name; // this sets the tab name
    this.title.closable = closable;
  }

  static createNode(id: string) {
    const div = document.createElement('div');
    div.setAttribute('id', id);
    return div;
  }

  /**
   * this event is triggered when we click on the tab of a widget
   */
  onActivateRequest(msg: any) {
    // create custom event
    const event = new CustomEvent('lumino:activated', this.getEventDetails());
    // fire custom event to parent element
    this.mainRef?.dispatchEvent(event);
    // continue with normal Widget behaviour
    super.onActivateRequest(msg);
  }

  /**
   * this event is triggered when the user clicks the close button
   */
  onCloseRequest(msg: any) {
    // create custom event
    const event = new CustomEvent('lumino:deleted', this.getEventDetails());
    // fire custom event to parent element
    this.mainRef?.dispatchEvent(event);
    // continue with normal Widget behaviour
    super.onCloseRequest(msg);
    super.dispose();
  }

  /**
   * creates a LuminoEvent holding name/id to properly handle them in react/redux
   */
  private getEventDetails(): LuminoEvent {
    return {
      detail: {
        widget: this,
        id: this.id,
        name: this.name,
        closable: this.closable,
        onClose: this.onClose,
      },
    };
  }
}

/**
 * Props of any component that will be rendered inside a LuminoWidget
 */
export interface ReactWidgetProps {
  id: string;
  name: string;
}

/**
 * Type of any component that will be rendered inside a LuminoWidget
 */
// export type ReactWidget = React.FC<ReactWidgetProps>;

/**
 * Method to return the component corresponding to the widgettype
 */
/* const getComponent = (type: string): ReactWidget => {
  switch (type) {
    case 'WATCHER':
      return Watcher;
    case 'INCREMENTOR':
      return Incrementor;
    case 'DECREMENTOR':
      return Decrementor;
    default:
      return Watcher;
  }
}; */

/**
 * Initialize Boxpanel and Dockpanel globally once to handle future calls
 */
export let main = new BoxPanel({ direction: 'left-to-right', spacing: 0 });
export let dock = new DockPanel();

interface LuminoParams {
  widgets: Accessor<AppWidget[]>;
  setWidgets: Setter<AppWidget[]>;
  luminoWidgets: Accessor<LuminoWidget[]>;
  setLuminoWidgets: Setter<LuminoWidget[]>;
  renderedWidgetIds: Accessor<string[]>;
  setRenderedWidgetIds: Setter<string[]>;
  disposeFunctions: Accessor<{ [p: string]: () => void }>;
  setDisposeFunctions: Setter<{ [p: string]: () => void }>;
}

/**
 * This component watches the widgets redux state and draws them
 */
// note: treat props as readOnly. mutable props has problems
export const Lumino = function (props: LuminoParams) {
  const [attached, setAttached] = createSignal<boolean>(false); // avoid attaching DockPanel and BoxPanel twice
  let mainRef: undefined | HTMLDivElement; // reference for Element holding our Widgets
  mainRef = undefined;

  /**
   * creates a LuminoWidget and adds it to the DockPanel. Id of widget is added to renderedWidgets
   */
  const addWidget = (w: AppWidget) => {
    if (!mainRef) return;
    props.setRenderedWidgetIds(cur => [...cur, w.id]);
    const lum = new LuminoWidget(w.id, w.tabTitle, mainRef, !!w.onClose, w.onClose);
    dock.addWidget(lum, { ref: props.luminoWidgets()[props.luminoWidgets.length - 1] });
    props.setLuminoWidgets(prevState => {
      prevState.push(lum);
      return prevState;
    });
    dock.activateWidget(lum);
  };
  onCleanup(() => {
    Widget.detach(main);
    main.dispose(); // gives error
    main = new BoxPanel({ direction: 'left-to-right', spacing: 0 });
    dock = new DockPanel();
  });
  /**
   * watch widgets state and calls addWidget for Each. After addWidget is executed we look
   * for the element in the DOM and use React to render the Component into the widget
   * NOTE: We need to use Provider in order to access the Redux State inside the widgets.
   */
  createComputed(() => {
    if (!attached()) return;
    props.widgets().forEach(w => {
      if (props.renderedWidgetIds().includes(w.id)) return; // avoid drawing widgets twice
      addWidget(w); // addWidget to DOM
      const el = document.getElementById(w.id); // get DIV
      // const Component = getComponent(w.type); // get Component for TYPE
      if (el) {
        props.setDisposeFunctions(prevState => {
          prevState[w.id] = render(w.component, el);
          return prevState;
        });
      }
    });
  });

  /**
   * This effect initializes the BoxPanel and the Dockpanel and adds event listeners
   * to dispatch proper Redux Actions for our custom events
   */
  createEffect(() => {
    if (!mainRef || attached()) {
      return;
    }
    main.id = 'main';
    main.addClass('main');
    dock.id = 'dock';
    window.onresize = () => main.update();
    BoxPanel.setStretch(dock, 1);
    Widget.attach(main, mainRef);
    setAttached(true);
    main.addWidget(dock);
    // dispatch activated action
    mainRef.addEventListener('lumino:activated', (e: Event) => {
      const le = e as unknown as LuminoEvent;
      props.setWidgets(prevWidgets =>
        prevWidgets.map(w => {
          w.active = w.id === le.detail.id;
          return w;
        }),
      );
    });
    // dispatch deleted action
    mainRef.addEventListener('lumino:deleted', (e: Event) => {
      const le = e as unknown as LuminoEvent;
      const conversationKey = le.detail.id;
      le.detail.onClose?.();
      props.disposeFunctions()[conversationKey]();
    });
  });

  return <div ref={mainRef} className="main" />;
};
