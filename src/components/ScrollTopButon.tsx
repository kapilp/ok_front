import {  JSX, onCleanup  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNamesBind from 'classnames/bind';
import styles from './ScrollTopButton.module.scss';
import { Button } from './UI/core/button/src/Button';
import { SvgIconUp } from './UI/core/icon/src/icon/IconUp';
const cx = classNamesBind.bind(styles);
/** React components for scrolling back to the top of the page **/
// https://codepen.io/Qbrid/pen/GjVvwL
// Create a Scroll to Top Arrow Using React Hooks
// https://medium.com/better-programming/create-a-scroll-to-top-arrow-using-react-hooks-18586890fedc
interface IScrollButtonProps extends JSX.HTMLAttributes<Element> {
  delayInMs?: any;
  scrollStepInPx?: any;
}
type ScrollButtonState = {
  intervalId: number;
  isVisible: boolean;
};
export const ScrollButton = (props: IScrollButtonProps) => {
  const [state, setState] = createStore({ intervalId: 0, isVisible: false } as ScrollButtonState);

  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    scrollFunction();
  };
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      setState({ isVisible: true });
    } else {
      setState({ isVisible: false });
    }
  }
  onCleanup(() => {
    window.onscroll = null;
  });
  /*const scrollStep = () => {
    if (window.pageYOffset === 0) {
      clearInterval(state.intervalId);
    }
    window.scroll(0, window.pageYOffset - props.scrollStepInPx);
  };
  const scrollToTopSmooth = () => {
    let intervalId = window.setInterval(scrollStep, props.delayInMs);
    setState({ intervalId: intervalId });
  };*/
  // When the user clicks on the button, scroll to the top of the document
  function scrollToTopFast() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  return (
    <Button
      title="Back to top"
      className={cx('scrollTop')}
      style={`display: ${state.isVisible ? 'block' : 'none'}`}
      onClick={scrollToTopFast}
      isIconOnly
      icon={SvgIconUp}
      text={'Top'}
    />
  );
};
/*
// USAGE:(see scss from codepen)
const ColoredContainer = (props: { color: string }) => {
  let containerStyle = {
    backgroundColor: props.color
  };
  return <div className="container" style={containerStyle}></div>;
};

const ScrollApp = () => {
  const state = {
    colors: ['#044747', '#079191', '#38adad', '#90e3e3', '#d5f7f7']
  };

  return (
    <div className="long">
      {state.colors.map(function (color) {
        return <ColoredContainer color={color} />;
      })}
      <ScrollButton scrollStepInPx="50" delayInMs="16.66" />
    </div>
  );
};
*/
