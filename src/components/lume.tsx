import {
  Element, // A base class for LUME custom elements
  attribute, // A property decorator to map attributes to properties
  reactive, // A property decorator to make a property reactive
  css, // A no-op identity function (useful to enable CSS syntax highlighting in various text editors)
} from '@lume/element';

@element('greeting-card') // defines the element tag name
class GreetingCard extends Element {
  // Make the firstName property a reactive variable, and also map any value
  // from an attribute named 'first-name' back to this property (the attribute
  // name is the dash-case version of the property name).
  @reactive @attribute firstName = 'Roger';

  // Define the structure of the DOM tree that we want rendered on screen by
  // providing a template property. This template property should simply
  // reference a DOM element (which we can create with JSX), and that DOM
  // element will be, by default, appended into the ShadowRoot of our custom
  // element.
  //
  // To take advantage of reactivity in our template, simply use the same
  // technique here as we did in the section above titled "Manipulating and
  // composing trees of elements", by using reactive variables or properties
  // in the places where they should be "rendered".
  //
  // Any time the `.firstName` reactive property's value changes, the DOM will
  // be automatically updated, thanks how the JSX works (it compiles to reactive
  // computations).
  template = (
    <div>
      <span>
        Hello <i>{this.firstName}</i>
      </span>
    </div>
  );

  // Apply styling to your element and its content with the static `css` property.
  // Because the property is static, this style is re-used across all instances of the element.
  // Styles are by default scoped to your element's content.
  static css = css`
    :host {
      background: skyblue;
    } /* Give greeting-card a background. */
    div {
      color: pink;
    }
  `;

  // If you need instance-specific styling, use a non-static `css` property.
  // This style has higher specificity than styles in the static `css` property.
  // In this example, the divs in each instance of this element will have borders of random sizes.
  css = css`
    div {
      border: ${Math.random() * 5}px solid teal;
    }
  `;

  // connectedCallback is a method that fires any time this custom element is
  // connected into a web site's live DOM tree.
  connectedCallback() {
    super.connectedCallback(); // Don't forget to call the super method!

    // Once the element is connected, let's update the `.firstName` prop after a
    // couple of seconds, and we'll see the change on screen change.
    setTimeout(() => (this.firstName = 'Zaya'), 2000);

    // And show that it works with by setting HTML attributes too, two seconds later.
    setTimeout(() => this.setAttribute('first-name', 'Raquel'), 4000);
  }

  // Use the disconnectedCallback to clean anything up when the element is removed from the DOM.
  disconnectedCallback() {
    super.disconnectedCallback();
    // ... clean up ...
  }
}
export const Lume = () => <greeting-card first-name="Raynor"></greeting-card>;
