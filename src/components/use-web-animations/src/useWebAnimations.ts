// @ts-ignore
import loadPolyfill from './polyfill';

if (typeof window !== 'undefined') loadPolyfill();

import {  createEffect, untrack, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { RefObject } from 'react';

export const polyfillErr = '> 💡use-web-animations: please enable polyfill to use this hook. See https://github.com/wellyshen/use-web-animations#disable-polyfill';
export const eventErr = (type: string): string =>
  `> 💡use-web-animations: the browser doesn't support ${type} event, please use onUpdate to monitor the animation's state instead. See https://github.com/wellyshen/use-web-animations#basic-usage`;

type Keyframes = Keyframe[] | PropertyIndexedKeyframes;
type State = { playState: string | null; animation: Animation; ref: HTMLElement };
interface AnimConf {
  id?: string;
  playbackRate?: number;
  autoPlay?: boolean;
  timing?: number | KeyframeAnimationOptions;
}
interface Animate {
  (args: AnimConf & { keyframes: Keyframes }): void;
}
export interface Event {
  playState: PlayState;
  animate: Animate;
  animation: Animation;
}
export interface Callback {
  (event: Event): void;
}
export interface Options<T> extends AnimConf {
  ref?: RefObject<T>;
  keyframes?: Keyframes;
  onReady?: Callback;
  onUpdate?: Callback;
  onFinish?: Callback;
}
interface Return<T> {
  ref: RefObject<T>;
  readonly playState: State;
  readonly getAnimation: () => Animation;
  readonly animate: Animate;
}

export const useWebAnimations = <T extends HTMLElement>(props: Options<T> = {}): Return<T> => {
  const [extracted, customProps] = splitProps(props, ['ref', 'id', 'playbackRate', 'autoPlay', 'keyframes', 'timing', 'onReady', 'onUpdate', 'onFinish']);
  const [state, setState] = createStore<State>({ playState: null, ref: null });

  let prevPending: boolean;
  let prevPlayState: string;

  let refVar = (el: HTMLElement) => {
    setState({ ref: el });
  };
  const refFn = props.ref || refVar;

  const animate: Animate = args => {
    if (!state.ref || !args.keyframes) return;
    if (!state.ref.animate) {
      console.error(polyfillErr);
      return;
    }

    setState({ animation: state.ref.animate(args.keyframes, args.timing) });

    if (args.autoPlay === false) state.animation.pause();
    if (args.id) state.animation.id = args.id;
    if (args.playbackRate) state.animation.playbackRate = args.playbackRate;

    if (props.onReady) {
      if (state.animation.ready) {
        state.animation.ready.then(animation => {
          props.onReady &&
            props.onReady({
              playState: animation.playState,
              animate,
              animation,
            });
        });
      } else {
        console.error(eventErr('onReady'));
      }
    }

    if (props.onFinish) {
      if (state.animation.finished) {
        state.animation.finished.then(animation => {
          props.onFinish &&
            props.onFinish({
              playState: animation.playState,
              animate,
              animation,
            });
        });
      } else {
        console.error(eventErr('onFinish'));
      }
    }

    prevPlayState = undefined;
  };

  createEffect(() => {
    if ([state.ref, props.id, props.playbackRate, props.autoPlay, props.keyframes, props.timing]) {
    } // on update this, rerun Effect,
    // use setTimeout Hack, because on ref set, element is not instantly available:
    setTimeout(() => animate({ id: props.id, playbackRate: props.playbackRate, autoPlay: props.autoPlay, keyframes: props.keyframes, timing: props.timing }));
  });

  const update = () => {
    if (state.animation) {
      const { pending, playState: curPlayState } = state.animation;

      if (prevPlayState !== curPlayState) setState({ playState: curPlayState });

      if (props.onUpdate && (prevPending !== pending || prevPlayState !== curPlayState || curPlayState === 'running'))
        props.onUpdate({ playState: curPlayState, animate, animation: state.animation });

      prevPending = pending;
      prevPlayState = curPlayState;
    }

    requestAnimationFrame(update);
  };
  createEffect(() => {
    state.ref && state.animation && untrack(() => requestAnimationFrame(update));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  });

  return { refFn, animationState: state, animate };
};
