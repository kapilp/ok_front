import { useFormContext } from './FormContext';
import { JSX, splitProps } from 'solid-js';
// Formik 3 RFC: https://github.com/formium/formik/pull/2360
export type FormikFormProps = Omit<JSX.FormHTMLAttributes<HTMLFormElement>, 'onReset' | 'onSubmit'>;

type FormProps = JSX.ComponentPropsWithoutRef<'form'>;

// @todo tests
export const Form = (props: FormikFormProps) => {
  // iOS needs an "action" attribute for nice input: https://stackoverflow.com/a/39485162/406725
  // We default the action to "#" in case the preventDefault fails (just updates the URL hash)
  const [p, customProps] = splitProps(props, ['action']);
  const _action = p.action || '#';
  const { handleReset, handleSubmit } = useFormContext();
  return <form onSubmit={handleSubmit} onReset={handleReset} action={_action} {...customProps} />;
};
