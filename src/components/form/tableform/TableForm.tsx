import {
  For,
  batch,
  Show,
  mergeProps,
  createComputed,
  untrack
} from "solid-js";
import { createStore, produce, Store } from "solid-js/store";
import { isEr, TableResult, TableResultAll} from "../../../utils/enums";
import { Ws } from '../../../utils/ws_events_dispatcher';
import { Label } from '../Label';
import { Options } from './Options';

interface Properties {
  label?: string;
  dp: Array<string>; // display pattern
  keyIdx: string; // '_key'
  value: string[];
  e?: number[][]; //events array
  disabled?: boolean;
  // args: [ [], [], []]
  error?: string;
  data: TableResult;
  onChange?: (value: string[]) => void;
}

export const TableForm = (props: Readonly<Properties>) => {
  props = mergeProps({}, { data: [], value: [], disabled: false }, props);

  const [state, setState] = createStore({ value: [] as string[], data: [] as TableResult, newAvailableOps: [] as TableResult, options: [] as TableResult[], er: '' });

  const calculateAvailableOptions = () => {
    setState({ newAvailableOps: (state.data as Store<Array<{ [key: string]: string }>>).filter(x => !state.value.includes(x[props.keyIdx || '_key'])) });
  };

  const reCalculateAllOptions = () => {
    const newOptions = []
    for (let i = 0; i < state.value.length; i++) {
      newOptions[i] = ((state.data as Store<Array<{ [key: string]: string }>>).filter(x => !state.value.includes(x[props.keyIdx|| '_key']) || state.value[i] === x[props.keyIdx|| '_key']));
    }
    setState({options: newOptions});
  };

  createComputed(()=>{
    if(props.value && state.data.length) {
      untrack(()=>{
        setState({value:props.value})
        if (state.data.length) {
          calculateAvailableOptions()
          reCalculateAllOptions()
        }
      });
    }
  })

  createComputed(()=>{
    if(props.data && props.data.length) {
      setState({data: props.data})
    }
  })

  createComputed(()=>{
    if (state.data.length) {
      untrack(calculateAvailableOptions);
      untrack(reCalculateAllOptions);
    }
  })

  const fetch_evt = [...props.e?.[0] ?? []];
  if (fetch_evt.length) {
    fetch_evt.push(Ws.uid);
    Ws.bind$(fetch_evt, onFetchGet, 1);
    Ws.trigger([[fetch_evt, {}]]);
  } else {
    console.warn('cant get fetch_evt on TableForm component at e[0]', props.e);
  }

  function onFetchGet(d: TableResultAll) {
    if (isEr(d, state, setState)) {
      return;
    }

    setState({ data: d?.r?.result ?? [] });
  }

  function handleAdd() {
      if (state.newAvailableOps.length) {
        setState({ value: state.value.concat([state.newAvailableOps[0][props.keyIdx|| '_key'] as string]) });
      }
      if (props.onChange) props.onChange(state.value);
  }

  const handleDelete = (rowNumber: () => number) => () => {
      setState('value', produce<string[]>(l => {
        l.splice(rowNumber(), 1);
      }));
      if (props.onChange) props.onChange(state.value);
  };

  const onChange = (i: () => number) => (e: Event & { currentTarget: HTMLSelectElement; target: Element }) => {
      setState('value', produce<string[]>(l => {
        l[i()] = state.options[i()][(e.target as HTMLSelectElement).selectedIndex][props.keyIdx|| '_key'] as string;
      }));
      if (props.onChange) props.onChange(state.value);
  };

  return (
    <>
      <Label name={props.label || ''} />

      <Show when={!!state.value}>
        <table>
          <tbody>
            <For each={state.value}>
              {(v, i) => (
                <>
                  <tr>
                    <td>
                      <label />
                    </td>
                    <td>
                      <select value={String(v)} required disabled={props.disabled || i() < state.value.length - 1} onChange={onChange(i)}>
                        <Options options={state.options[i()]} dp={props.dp} selected={String(v)}/>
                      </select>
                    </td>
                    <td>
                      <button type="button" onClick={handleDelete(i)} disabled={props.disabled}>
                        delete
                      </button>
                    </td>
                  </tr>
                </>
              )}
            </For>
          </tbody>
        </table>
      </Show>
      <button type="button" onClick={handleAdd} disabled={props.disabled || !state.newAvailableOps.length}>
        Add
      </button>

      <span>{props.error}</span>
    </>
  );
};
