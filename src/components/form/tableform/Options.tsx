import { For } from 'solid-js';
import { TableResult } from '../../../utils/enums';
import { stringifyRawPattern } from '../../../utils/string_functions';

interface Properties {
  options: TableResult;
  selected: string;

  dp?: Array<string>;
}

export const Options = (props: Properties) => {

  function render(r: { [key: string]: string | number | boolean }) {
    return stringifyRawPattern(props.dp ?? ['_key', ' - ', 'name'], r);
  }

  return <For each={props.options}>{r => <option value={r['_key'] as string} selected={props.selected == r['_key'] ? true : false}>{render(r)}</option>}</For>;
};
