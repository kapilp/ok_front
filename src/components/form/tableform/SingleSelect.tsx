import { createComputed } from 'solid-js';
import { createStore } from 'solid-js/store';
import { isEr, TableResult, TableResultAll } from '../../../utils/enums';
import { Ws } from '../../../utils/ws_events_dispatcher';
import { Label } from '../Label';
import { Options } from './Options';

interface Properties {
  label?: string;
  dp?: Array<string>; // display pattern
  key?: string;
  value: string;
  e?: number[][]; // events array
  disabled?: boolean;
  error?: string;
  data?: TableResult;
  required?: boolean;
  onChange?: (v: string) => void;
}

export const SingleSelect = (props: Readonly<Properties>) => {
  const [state, setState] = createStore({ data: props.data ?? [], singleValue: props.value, newAvailableOps: [] as TableResult, er: '' });

  const fetch_evt = [...(props.e?.[0] ?? [])];
  if (fetch_evt.length) {
    fetch_evt.push(Ws.uid);
    Ws.bind$(fetch_evt, onFetchGet, 1);
    Ws.trigger([[fetch_evt, []]]);
  }

  const setFirstValueFromState = () => props.onChange && props.onChange(state.singleValue);

  const setFirstValueIfEmpty = (d?: TableResult) => {
    if (d && d.length && !state.singleValue) {
      setState({ singleValue: (d[0]?.[props.key || '_key'] ?? '') as string });
      setFirstValueFromState();
    }
  };
  setFirstValueIfEmpty(props?.data);

  createComputed(() => props.value && setState({ singleValue: props.value }));

  function onFetchGet(d: TableResultAll) {
    if (isEr(d, state, setState)) {
      return;
    }

    setState({ data: d?.r?.result ?? [] });

    setFirstValueIfEmpty(state.data);
  }

  const onChangeSingle = (e: Event & { currentTarget: HTMLSelectElement; target: Element }) => {
    setState({ singleValue: state.data[(e.target as HTMLSelectElement).selectedIndex][props.key || '_key'] as string });
    setFirstValueFromState();
  };

  return (
    <>
      <Label name={props.label || ''} />

      <select value={state.singleValue} required={props.required} disabled={props.disabled} onChange={onChangeSingle}>
        <Options options={state.data} dp={props.dp} />
      </select>

      <span>{props.error || state.er}</span>
    </>
  );
};
