import { find, has } from "rambda";
import { isObject } from "ramda-adjunct";
import {
  batch,
  createComputed,
  For,
  mergeProps,
  Show,
  untrack
} from "solid-js";
import { createStore, produce, Store } from "solid-js/store";
import { isEr, TableResult, TableResultWithSchema } from "../../../utils/enums";
import { Ws } from "../../../utils/ws_events_dispatcher";
import { Label } from "../Label";
import { Options } from "./Options";

type BoolProp = [string, { [key: string]: boolean }];

interface Properties {
  label?: string;
  dp?: Array<string>; // [1, ' - ', 2]; // display pattern
  keyIdx?: number; // 0
  value: BoolProp[];
  e?: number[][]; //events array
  disabled?: boolean;
  // args: [ [], [], []]
  boolprop?: boolean;
  bi?: ""; // boolPropIndex
  error?: string;
  data: TableResult;
  onChange?: (value: BoolProp[]) => void;
}

interface Properties2 {
  rowIndex: number;
  value: BoolProp;
  options: TableResult;
  disabled: boolean;
  dp?: Array<string>;
  bi?: string; // boolPropIndex
  form_disabled?: boolean;
  onChange?: (index: number, value: BoolProp) => void;
}

export const TableFormBoolProperties = (props: Readonly<Properties>) => {
  props = mergeProps({}, { data: [], value: [], disabled: false }, props);

  const [state, setState] = createStore({
    value: [] as BoolProp[],
    data: {} as TableResult,
    newAvailableOps: [] as TableResult,
    options: [] as TableResult[],
    er: ""
  });

  const calculateAvailableOptions = () => {
    const keys = state.value.map(x => x[0]);
    setState({ newAvailableOps: (state.data as Store<Array<{ [key: string]: string }>>).filter(x => !keys.includes(x[props.keyIdx || "_key"])) });
  };

  const reCalculateAllOptions = () => {
    batch(() => {
      const keys = state.value.map(x => x[0]);
      for (let i = 0; i < state.value.length; i++) {
        setState("options", produce<TableResult[]>(l => {
          // [['abc',{}],['def',{}]]
          l[i] = state.data.filter(x => !keys.includes(x[props.keyIdx || "_key"] as string) || keys[i] == x[props.keyIdx || "_key"]);
        }));
      }
    });
  };

  let updatePropsValue = true;
  createComputed(() => {
    if (props.value && updatePropsValue) {
      untrack(() => {
        setState({ value: props.value });
        if (state.data.length) {
          calculateAvailableOptions();
          reCalculateAllOptions();
        }
      });
    }
  });

  createComputed(() => {
    if (props.data && props.data.length) {
      setState({ data: props.data });
    }
  });

  createComputed(() => {
    if (state.data.length) {
      untrack(calculateAvailableOptions);
      untrack(reCalculateAllOptions);
    }
  });

  const fetch_evt = [...props.e?.[0] ?? []];
  if (fetch_evt.length) {
    fetch_evt.push(Ws.uid);
    Ws.bind$(fetch_evt, onFetchGet, 1);
    Ws.trigger([[fetch_evt, {}]]);
  } else {
    console.warn("cant get fetch_evt on TableForm component at e[0]", props.e);
  }

  function onFetchGet(d: TableResultWithSchema) {
    if (isEr(d, state, setState)) {
      return;
    }

    setState({ data: d?.r?.result ?? [] });
  }

  const onOneChange = (index: number, value: BoolProp) => {
    setState("value", index, value);
    updatePropsValue = false;
    props.onChange && props.onChange(state.value);
    updatePropsValue = true;
  };

  function handleAdd() {
    if (state.newAvailableOps.length) {
      setState({ value: state.value.concat([[state.newAvailableOps[0][props.keyIdx || "_key"] as string, {}]]) });
    }
    if (props.onChange) props.onChange(state.value);
  }

  const handleDelete = (rowNumber: () => number) => () => {
    setState("value", produce<BoolProp[]>(l => {
      l.splice(rowNumber(), 1);
    }));
    if (props.onChange) props.onChange(state.value);
  };

  const BoolProperties = (props: Properties2) => {
    const [state, setState] = createStore({
      boolkeys: [] as string[],
      value: props.value ?? [null, {}]
    });

    const fixBoolPropsValue = () => {
      if (!state.value[1]) {
        // this will not happen, parent component take care of it
        setState("value", produce<BoolProp>(l => {
          l[1] = state.value[1] ?? {};
        }));
        return true;
      }
      return false;
    };
    const onChange = (e: Event & { currentTarget: HTMLSelectElement; target: Element }) => {
      batch(() => {
        setState("value", produce<BoolProp>(l => {
          l[0] = props.options[(e.target as HTMLSelectElement).selectedIndex]["_key"] as string;
          l[1] = {};
        }));

        fixBoolPropsValue();
        setAllOptions();
        if (props.onChange) props.onChange(props.rowIndex, state.value);
      });
    };
    const onPropertyChange = (r: string) => (e: MouseEvent & { currentTarget: HTMLInputElement; target: Element }) => {
      batch(() => {
        setState("value", produce<BoolProp>(l => {
          l[1][r] = (e.target as HTMLInputElement).checked;
        }));
        if (props.onChange) props.onChange(props.rowIndex, state.value);
      });
    };
    const setAllOptions = () => {
      let stateChanged = false;
      const findPropertiesRow = find(x => x["_key"] == props.value[0], props.options ?? []); // hack! crash when props.options is undefined
      if (findPropertiesRow) {
        if (isObject(findPropertiesRow) && has(props.bi ?? "", findPropertiesRow) && Array.isArray(findPropertiesRow[props.bi ?? ""])) {
          setState({ boolkeys: (findPropertiesRow[props.bi ?? ""] as unknown) as string[] });
          stateChanged = true;
        } else {
          setState("value", produce<BoolProp>(l => {
            l[1] = {};
          }));
          stateChanged = true;
          setState({ boolkeys: [] });
        }
      }
      if (stateChanged) {
        if (props.onChange) props.onChange(props.rowIndex, state.value);
      }
    };
    createComputed(() => {
      untrack(setAllOptions);
    });

    return (
      <>
        <td>
          <Show when={!!props.options}>
            <select value={props.value[0]} required onChange={onChange}
                    disabled={props.disabled}>
              <Options options={props.options} dp={props.dp}
                       selected={props.value[0]} />
            </select>
          </Show>
        </td>
        <td>
          <For each={state.boolkeys}>
            {r => (
              <label>
                <input type="checkbox" checked={props.value[1][r]}
                       disabled={props.form_disabled}
                       onClick={onPropertyChange(r)} />
                {r}
              </label>
            )}
          </For>
        </td>
      </>
    );
  };

  return (
    <>
      <Label name={props.label || ""} />
      <Show when={!!state.value}>
        <table className={"permissions"}>
          <tbody>
          <For each={state.value}>
            {(v, i) => (
              <>
                <tr>
                  <td>
                    <label />
                  </td>

                  {state.options[i()] && state.options[i()].length &&
                  <BoolProperties
                    disabled={props.disabled || i() < state.value.length - 1}
                    value={v}
                    options={state.options[i()]}
                    form_disabled={props.disabled ?? false}
                    dp={props.dp}
                    bi={props.bi}
                    onChange={onOneChange}
                    rowIndex={i()}
                  />}

                  <td>
                    <button type="button" onClick={handleDelete(i)}
                            disabled={props.disabled}>
                      delete
                    </button>
                  </td>
                </tr>
              </>
            )}
          </For>
          </tbody>
        </table>
      </Show>
      <button type="button" onClick={handleAdd}
              disabled={props.disabled || !state.newAvailableOps.length}>
        Add
      </button>
      <span>{props.error}</span>
    </>
  );
};
