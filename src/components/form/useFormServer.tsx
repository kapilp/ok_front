import { JSX, batch, createEffect, onCleanup, untrack, useContext } from 'solid-js';
import { createStore, produce, reconcile } from 'solid-js/store';
import { clone, mergeLeft } from 'rambda';
import { mergeRight } from 'ramda';
import { getWsConnected, Ws, WSEvent } from '../../utils/ws_events_dispatcher';
import { ET, FormError, FormResultWithSchema, GeneralError, isEr, isNonEmptyObject, schemaEvents, TableResultAll, WsStatus } from '../../utils/enums';
import { Properties, useForm } from './useForm';
import { fetchHtml, getTemplateHtml } from '../UI/Html';
import { ProjectStackContext } from '../../layout/context';

export interface ServerFormProperties extends Properties {
  t?: string[];
  b?: string[];
  key: string;
  schemaKey: string;
  fetchConfig: { project?: string; parent?: string };
  selector?: string[];
  headerSchema?: FormResultWithSchema;
  onCancel?: (key: string) => void;
}

export const useFormServer = (props: ServerFormProperties) => {
  const [getProject] = useContext(ProjectStackContext);
  const submitOnServer = () => {
    // const filter = key ? [`="${fetchConfig.type == form._key}"`] : null;
    const filter = props.key
      ? {
          _key: state.values._key,
          _rev: state.values._rev,
        }
      : null;
    // Now auto unsubscribing no need to pass  , ...(key ?  {unsub: data_evt} : {})
    const saveConfig = makeBaseFetchConfig(); // , form: true, schema: schemaKey
    if (props.selector && props.selector.length) {
      // fix this to save it in state
      saveConfig.sel = props.selector;
    }
    const args = { value: state.values, f: filter, ...saveConfig };
    if (props.key) args.unSub = eventState.unsub_evt;

    Ws.trigger([[eventState.mutate_evt, args]]);
  };
  const { state, setState, optionsState, setOptionsState, handleChange, handleBlur, handleSubmit, resetState, resetOptionsState } = useForm({
    ...props,
    onSubmit: submitOnServer,
  });
  const getInitialEventState = () => {
    return { events: [], data_evt: [], unsub_evt: [], drag_evt: [], mutate_evt: [], schemaGetEvt: [] } as {
      events: number[];
      data_evt: number[];
      unsub_evt: number[];
      mutate_evt: WSEvent;
      schemaGetEvt: WSEvent;
    };
  };
  const getInitialServerState = () => {
    return {
      isFetchedTopTemplate: false,
      isFetchedBottomTemplate: false,
      topHtmlTemplate: '',
      bottomHtmlTemplate: '',
    };
  };

  const [eventState, setEventState] = createStore(getInitialEventState());
  const [serverState, setServerState] = createStore(getInitialServerState());
  const resetServerState = () => {
    batch(() => {
      resetState();
      resetOptionsState();
      setEventState(reconcile(getInitialEventState()));
      setServerState(reconcile(getInitialServerState()));
    });
  };
  function makeBaseFetchConfig() {
    const fetchConfig = {
      ...(props.fetchConfig || {}),
      project: getProject()?.[getProject().length - 1]?._key ?? undefined,
    };
    return fetchConfig;
  }

  function fetch() {
    const fetchConfig = makeBaseFetchConfig();

    const filter = props.key ? { _key: `="${props.key}"` } : {};
    if (eventState.schemaGetEvt && eventState.schemaGetEvt.length) {
      const args = { f: filter, ...fetchConfig, schema: props.schemaKey };
      const e1 = [eventState.schemaGetEvt, args];
      Ws.trigger([e1]);
    } else if (eventState.data_evt && eventState.data_evt.length) {
      const args = { f: filter, ...fetchConfig, form: true }; // level: ProjectStackContext[ProjectStackContext.length - 1]?._key ?? ""
      const e1 = [eventState.data_evt, args];
      Ws.trigger([e1]);
    }
  }
  createEffect(() => {
    // console.log('running effect11');
    if (getWsConnected()) {
      setState({ er: GeneralError.NO_ERROR });
      // sample(refresh); // todo: Refetch data and set if changed. but first save old data.
    } else {
      setState({ er: WsStatus.RECONNECTION });
    }
  });
  let previousSchema = '';
  createEffect(() => {
    // console.log('running effect22');
    if (!props.schemaKey) {
      setState({ er: GeneralError.INVALID_SCHEMA });
    } else {
      if (previousSchema && previousSchema != props.schemaKey) {
        untrack(() => unSubscribeEvent(eventState.unsub_evt));
        untrack(resetState);
      }
      previousSchema = props.schemaKey;
      const events = schemaEvents(props.schemaKey);
      if (events) {
        const { uid } = Ws;
        setEventState(
          produce(prevState => {
            prevState.events = events;
            if (events[0]) {
              if (props.key) {
                prevState.data_evt = [ET[ET.subscribe], events[0], uid];
                prevState.unsub_evt = [ET[ET.unsubscribe], events[0], uid];
              } else {
                prevState.data_evt = [ET[ET.get], events[0], uid];
                prevState.unsub_evt = [];
              }
            } else {
              prevState.schemaGetEvt = [ET[ET.get], 'form_schema_get', uid];
            }

            if (props.key) {
              prevState.mutate_evt = [ET[ET.update], events[1], uid];
            } else {
              prevState.mutate_evt = [ET[ET.insert], events[1], uid];
            }
          }),
        );

        if (props.headerSchema) {
          untrack(() => onDataGet(props.headerSchema));
        } else {
          untrack(fetch);
        }
      } else {
        setState({ er: GeneralError.NO_EVENTS });
      }
    }
  });
  fetchHtml(props.t || []).then(d => {
    setServerState({ isFetchedTopTemplate: true, topHtmlTemplate: getTemplateHtml(d) });
  });
  fetchHtml(props.b || []).then(d => {
    setServerState({ isFetchedBottomTemplate: true, bottomHtmlTemplate: getTemplateHtml(d) });
  });
  const unSubscribeEvent = (e: number[]) => e.length && Ws.trigger([[e, {}]]);
  onCleanup(() => props.key && unSubscribeEvent(eventState.unsub_evt)); // // on delete not necessary

  type Fn = () => void;
  let prevDataUnbindFn: Fn | null;
  let prevSchemaUnbindFn: Fn | null;
  let prevMutateUnbindFn: Fn | null;
  createEffect(() => {
    if (props.schemaKey) {
      if (prevDataUnbindFn) prevDataUnbindFn();
      if (prevSchemaUnbindFn) prevSchemaUnbindFn();
      if (prevMutateUnbindFn) prevMutateUnbindFn();
      prevDataUnbindFn = null;
      if (eventState.data_evt.length) {
        prevDataUnbindFn = Ws.bind$(eventState.data_evt, onDataGet, 1);
      }
      prevSchemaUnbindFn = null;
      if (eventState.schemaGetEvt.length) {
        prevSchemaUnbindFn = Ws.bind$(eventState.schemaGetEvt, onDataGet, 1);
      }
      prevMutateUnbindFn = Ws.bind$(eventState.mutate_evt, onMutateGet, 1);
    }
  });
  onCleanup(() => {
    prevDataUnbindFn && prevDataUnbindFn();
    prevSchemaUnbindFn && prevSchemaUnbindFn();
    prevMutateUnbindFn && prevMutateUnbindFn();
  });

  function onDataGet(d: FormResultWithSchema) {
    batch(() => {
      if (isEr(d, state, setState)) return;
      resetOptionsState();
      const options_new = d.formProps ?? {};
      setOptionsState(options_new);

      if (isNonEmptyObject(d.fields))
        setState({
          fields: props.value ? mergeLeft(state.fields, d.fields) : d.fields,
          layout: d.layout,
        });

      /// / We do these checks because initial value could be false or zero!!
      // todo ..

      if (optionsState.replace && props.key) {
        Ws.unbind(eventState.mutate_evt);
        setEventState(
          'mutate_evt',
          produce(e => {
            e[0] = ET[ET.replace];
          }),
        );
        Ws.bind$(eventState.mutate_evt, onMutateGet, 1);
      }
      const form_new = onFormDataGetStatic(d);
      if (!form_new) {
        setState({ er: FormError.INVALID_VALUE });
      } else {
        const mergeFn = props.value ? mergeLeft : mergeRight;
        // R.mergeDeepRight?
        const new_form = mergeFn(state.values || {}, form_new as {});
        setState({ values: new_form });
        setState({ initialValues: clone(new_form) });
        setOptionsState({ disabled: optionsState?.ds ?? false });
      }
    });
  }
  function onFormDataGetStatic(d: TableResultAll) {
    // can improve this helper function with runtime checking.
    if (d.r) {
      const r = d.r.result;
      if (r.length) {
        return r[0];
      }
      return {};
    }
    if (d.n) {
      // d.n.result
    } else if (d.m) {
      const r = d.m.result;
      if (r.length) {
        return r[0];
      }
      return {};

      // d.m.result
    } else if (d.d) {
      //
    }
  }
  let emitEvent = true;
  function onApply() {
    emitEvent = false;
    handleSubmit();
  }
  // const { addNotification } = getNotificationsContext();
  function onMutateGet(d: [Success, string | undefined, {} | undefined]) {
    setState({ isSubmitting: false });
    if (!isEr(d, state, setState)) {
      /* const save_msg = view(lensPath(['msg', 'save']), $translation);
      if (options.notify) {
        addNotification({
          text: save_msg,
          position: 'bottom-right',
          type: 'success',
          removeAfter: 4000
        });
      } */
      setState({ er: GeneralError.NO_ERROR });
      emitEvent && props.handleSubmit && props.handleSubmit(props.key); // can pass `d`
      onReset();
    } else {
      setState({ errors: d.errors || {} });
    }
  }
  const onReset = () => !props.key && setState({ values: clone(state.initialValues) });
  const onCancel = (e: Event) => props.onCancel && props.onCancel(props.key);
  const onDelete = (e: Event) => props.onDeleteOneRow && props.onDeleteOneRow(props.key, eventState.unsub_evt)(e);

  return { state, optionsState, serverState, handleChange, handleBlur, handleSubmit, onApply, onDelete, onCancel };
};
