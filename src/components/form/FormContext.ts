import { FormikContextType } from '../UI/validators/formik/packages/formik/src/types';
import invariant from 'tiny-warning';
import { createContext, useContext } from 'solid-js';

export const FormContext = createContext<FormikContextType<any>>(undefined as any);
export const FormProvider = FormContext.Provider;

export function useFormContext<Values>() {
  const form = useContext<FormikContextType<Values>>(FormContext);
  invariant(!!form, `Form context is undefined, please verify you are calling useFormikContext() as child of a <FormProvider> component.`);

  return form;
}
