import { Label } from '../Label';
import { JSX, splitProps } from 'solid-js';

interface Properties extends JSX.HTMLAttributes<HTMLInputElement> {
  disabled?: boolean;
  onChange: JSX.EventHandler<HTMLInputElement, Event>;
  //isSaving?: boolean
  required?: boolean;
  value: string;
  error?: string;
  placeholder?: string;
  label?: string;
  props?: {};
  dom?: HTMLInputElement;
  class?: string;
  type: string;
}

export const Input = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['disabled', 'onChange', 'required', 'value', 'error', 'placeholder', 'label', 'props', 'dom', 'class', 'type']);

  return (
    <>
      <Label name={props.label || ''} />
      <input
        {...customProps}
        name={p.label || ''}
        className={p.class}
        type={p.type}
        required={p.required}
        autocomplete={'false'}
        disabled={p.disabled}
        placeholder={p.placeholder}
        value={p.value}
        onChange={p.onChange}
        onBlur={p.onChange}
        ref={p.dom}
      />
      <span>{p.error}</span>
    </>
  );
};
// {...(p.props || {})}
