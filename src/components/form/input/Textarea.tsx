import { JSX } from 'solid-js';
import { Label } from '../Label';

interface Properties {
  disabled?: boolean;
  onChange: JSX.EventHandler<HTMLTextAreaElement, Event>;
  //isSaving?: boolean
  required?: boolean;
  value: string;
  error?: string;
  placeholder?: string;
  label?: string;
  props?: {};
  dom?: HTMLTextAreaElement;
  class?: string;
}

export const Textarea = (props: Properties) => {
  return (
    <>
      <Label name={props.label || ''} />
      <textarea
        name={props.label || ''}
        className={props.class}
        required={props.required}
        autocomplete={'false'}
        disabled={props.disabled}
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.onChange}
        onBlur={props.onChange}
        ref={props.dom}
        rows={5}
        cols={20}
        {...(props.props || {})}
      />
      <span>{props.error}</span>
    </>
  );
};
