/*
React-Quill
https://github.com/zenoamaro/react-quill
*/

import isEqual from 'lodash/isEqual';
import Quill, { BoundsStatic, DeltaStatic, QuillOptionsStatic, RangeStatic, Sources, StringMap } from 'quill';
import 'quill/dist/quill.snow.css';
import {  JSX, onCleanup  } from 'solid-js';
import { createStore } from 'solid-js/store';
// Merged namespace hack to export types along with default object
// See: https://github.com/Microsoft/TypeScript/issues/2719
export declare namespace ReactQuill {
  export type Value = string | DeltaStatic;
  export type Range = RangeStatic | null;

  export interface QuillOptions extends QuillOptionsStatic {
    tabIndex?: number;
  }

  export interface ReactQuillProps {
    bounds?: string | HTMLElement;
    children?: React.ReactElement<any>;
    className?: string;
    defaultValue?: Value;
    formats?: string[];
    id?: string;
    modules?: StringMap;
    onChange?(value: string, delta: DeltaStatic, source: Sources, editor: UnprivilegedEditor): void;
    onChangeSelection?(selection: Range, source: Sources, editor: UnprivilegedEditor): void;
    onFocus?(selection: Range, source: Sources, editor: UnprivilegedEditor): void;
    onBlur?(previousSelection: Range, source: Sources, editor: UnprivilegedEditor): void;
    onKeyDown?: React.EventHandler<any>;
    onKeyPress?: React.EventHandler<any>;
    onKeyUp?: React.EventHandler<any>;
    placeholder?: string;
    preserveWhitespace?: boolean;
    readOnly?: boolean;
    scrollingContainer?: string | HTMLElement;
    style?: React.CSSProperties;
    tabIndex?: number;
    theme?: string;
    value?: Value;
  }

  export interface UnprivilegedEditor {
    getLength(): number;
    getText(index?: number, length?: number): string;
    getHTML(): string;
    getBounds(index: number, length?: number): BoundsStatic;
    getSelection(focus?: boolean): RangeStatic;
    getContents(index?: number, length?: number): DeltaStatic;
  }
}

// Re-import everything from namespace into scope for comfort

interface ReactQuillState {
  generation: number;
}

export const ReactQuill = (props: ReactQuill.ReactQuillProps) => {
  const displayName = 'React Quill';

  /*
  Export Quill to be able to call `register`
  */
  //const Quill = Quill;

  /*
  Changing one of these props should cause a full re-render and a
  re-instantiation of the Quill editor.
  */
  const dirtyProps: (keyof ReactQuill.ReactQuillProps)[] = ['modules', 'formats', 'bounds', 'theme', 'children'];

  /*
  Changing one of these props should cause a regular update. These are mostly
  props that act on the container, rather than the quillized editing area.
  */
  const cleanProps: (keyof ReactQuill.ReactQuillProps)[] = [
    'id',
    'className',
    'style',
    'placeholder',
    'tabIndex',
    'onChange',
    'onChangeSelection',
    'onFocus',
    'onBlur',
    'onKeyPress',
    'onKeyDown',
    'onKeyUp',
  ];

  /*static defaultProps = {
    theme: 'snow',
    modules: {},
    readOnly: false,
  }*/

  const [state, setState] = createStore({
    generation: 0,
  });

  /*
  The Quill Editor instance.
  */
  let editor: Quill | undefined;

  /*
  Reference to the element holding the Quill editing area.
  */
  let editingArea: React.ReactInstance | null;

  /*
  Tracks the internal value of the Quill editor
  */
  let value: ReactQuill.Value;

  /*
  Tracks the internal selection of the Quill editor
  */
  let selection: ReactQuill.Range = null;

  /*
  Used to compare whether deltas from `onChange` are being used as `value`.
  */
  let lastDeltaChangeSet: DeltaStatic | undefined;

  /*
  Stores the contents of the editor to be restored after regeneration.
  */
  let regenerationSnapshot:
    | {
        delta: DeltaStatic;
        selection: ReactQuill.Range;
      }
    | undefined;

  /*
  A weaker, unprivileged proxy for the editor that does not allow accidentally
  modifying editor state.
  */
  let unprivilegedEditor: ReactQuill.UnprivilegedEditor | undefined;
  /*
  We consider the component to be controlled if `value` is being sent in props.
  */
  const isControlled = (): boolean => {
    return 'value' in props;
  };
  value = (isControlled() ? props.value : props.defaultValue) ?? '';

  const validateProps = (props: ReactQuill.ReactQuillProps): void => {
    if (props.children.length > 1) throw new Error('The Quill editing area can only be composed of a single React element.');

    if (props.children.length) {
      const child = props.children; //React.Children.only(
      if (child?.type === 'textarea') throw new Error('Quill does not support editing on a <textarea>. Use a <div> instead.');
    }

    if (lastDeltaChangeSet && props.value === lastDeltaChangeSet)
      throw new Error(
        'You are passing the `delta` object from the `onChange` event back ' +
          'as `value`. You most probably want `editor.getContents()` instead. ' +
          'See: https://github.com/zenoamaro/react-quill#using-deltas',
      );
  };

  /*shouldComponentUpdate(nextProps: ReactQuillProps, nextState: ReactQuillState) {
    validateProps(nextProps);

    // If the editor hasn't been instantiated yet, or the component has been
    // regenerated, we already know we should update.
    if (!editor || state.generation !== nextState.generation) {
      return true;
    }

    // Handle value changes in-place
    if ('value' in nextProps) {
      const prevContents = getEditorContents();
      const nextContents = nextProps.value ?? '';

      // NOTE: Seeing that Quill is missing a way to prevent edits, we have to
      //       settle for a hybrid between controlled and uncontrolled mode. We
      //       can't prevent the change, but we'll still override content
      //       whenever `value` differs from current state.
      // NOTE: Comparing an HTML string and a Quill Delta will always trigger a
      //       change, regardless of whether they represent the same document.
      if (!isEqualValue(nextContents, prevContents)) {
        setEditorContents(editor, nextContents);
      }
    }

    // Handle read-only changes in-place
    if (nextProps.readOnly !== props.readOnly) {
      setEditorReadOnly(editor, nextProps.readOnly!);
    }

    // Clean and Dirty props require a render
    return [...cleanProps, ...dirtyProps].some((prop) => {
      return !isEqual(nextProps[prop], props[prop]);
    });
  }*/

  /*shouldComponentRegenerate(nextProps: ReactQuillProps): boolean {
    // Whenever a `dirtyProp` changes, the editor needs reinstantiation.
    return dirtyProps.some((prop) => {
      return !isEqual(nextProps[prop], props[prop]);
    });
  }*/

  const createEditorInstance = () => {
    // Mount
    instantiateEditor();
    setEditorContents(editor!, getEditorContents());
  };

  onCleanup(() => {
    destroyEditor();
  });

  /*componentDidUpdate(prevProps: ReactQuillProps, prevState: ReactQuillState) {
    // If we're changing one of the `dirtyProps`, the entire Quill Editor needs
    // to be re-instantiated. Regenerating the editor will cause the whole tree,
    // including the container, to be cleaned up and re-rendered from scratch.
    // Store the contents so they can be restored later.
    if (editor && shouldComponentRegenerate(prevProps)) {
      const delta = editor.getContents();
      const selection = editor.getSelection();
      regenerationSnapshot = {delta, selection};
      setState({generation: state.generation + 1});
      destroyEditor();
    }

    // The component has been regenerated, so it must be re-instantiated, and
    // its content must be restored to the previous values from the snapshot.
    if (state.generation !== prevState.generation) {
      const {delta, selection} = regenerationSnapshot!;
      delete regenerationSnapshot;
      instantiateEditor();
      const editor = editor!;
      editor.setContents(delta);
      postpone(() => setEditorSelection(editor, selection));
    }
  }*/

  const instantiateEditor = (): void => {
    if (editor) return;
    editor = createEditor(getEditingArea(), getEditorConfig());
  };

  const destroyEditor = (): void => {
    if (!editor) return;
    unhookEditor(editor);
    // delete editor; // strict mode issue
  };

  const getEditorConfig = (): ReactQuill.QuillOptions => {
    return {
      bounds: props.bounds,
      formats: props.formats,
      modules: props.modules,
      placeholder: props.placeholder,
      readOnly: props.readOnly,
      scrollingContainer: props.scrollingContainer,
      tabIndex: props.tabIndex,
      theme: props.theme,
    };
  };

  const getEditor = (): Quill => {
    if (!editor) throw new Error('Accessing non-instantiated editor');
    return editor;
  };

  /**
  Creates an editor on the given element. The editor will be passed the
  configuration, have its events bound,
  */
  const createEditor = (element: Element, config: ReactQuill.QuillOptions) => {
    const editor = new Quill(element, config);
    if (config.tabIndex != null) {
      setEditorTabIndex(editor, config.tabIndex);
    }
    hookEditor(editor);
    return editor;
  };

  const hookEditor = (editor: Quill) => {
    // Expose the editor on change events via a weaker, unprivileged proxy
    // object that does not allow accidentally modifying editor state.
    unprivilegedEditor = makeUnprivilegedEditor(editor);
    // Using `editor-change` allows picking up silent updates, like selection
    // changes on typing.
    editor.on('editor-change', onEditorChange);
  };

  const unhookEditor = (editor: Quill) => {
    editor.off('editor-change', onEditorChange);
  };

  const getEditorContents = (): ReactQuill.Value => {
    return value;
  };

  const getEditorSelection = (): ReactQuill.Range => {
    return selection;
  };

  /*
  True if the value is a Delta instance or a Delta look-alike.
  */
  const isDelta = (value: any): boolean => {
    return value && value.ops;
  };

  /*
  Special comparison function that knows how to compare Deltas.
  */
  const isEqualValue = (value: any, nextValue: any): boolean => {
    if (isDelta(value) && isDelta(nextValue)) {
      return isEqual(value.ops, nextValue.ops);
    } else {
      return isEqual(value, nextValue);
    }
  };

  /*
  Replace the contents of the editor, but keep the previous selection hanging
  around so that the cursor won't move.
  */
  const setEditorContents = (editor: Quill, value: ReactQuill.Value) => {
    value = value;
    const sel = getEditorSelection();
    if (typeof value === 'string') {
      editor.setContents(editor.clipboard.convert(value));
    } else {
      editor.setContents(value);
    }
    postpone(() => setEditorSelection(editor, sel));
  };

  const setEditorSelection = (editor: Quill, range: ReactQuill.Range) => {
    selection = range;
    if (range) {
      // Validate bounds before applying.
      const length = editor.getLength();
      range.index = Math.max(0, Math.min(range.index, length - 1));
      range.length = Math.max(0, Math.min(range.length, length - 1 - range.index));
      editor.setSelection(range);
    }
  };

  const setEditorTabIndex = (editor: Quill, tabIndex: number) => {
    if (editor?.scroll?.domNode) {
      (editor.scroll.domNode as HTMLElement).tabIndex = tabIndex;
    }
  };

  const setEditorReadOnly = (editor: Quill, value: boolean) => {
    if (value) {
      editor.disable();
    } else {
      editor.enable();
    }
  };

  /*
  Returns a weaker, unprivileged proxy object that only exposes read-only
  accessors found on the editor instance, without any state-modifying methods.
  */
  const makeUnprivilegedEditor = (editor: Quill) => {
    const e = editor;
    return {
      getHTML: () => e.root.innerHTML,
      getLength: e.getLength.bind(e),
      getText: e.getText.bind(e),
      getContents: e.getContents.bind(e),
      getSelection: e.getSelection.bind(e),
      getBounds: e.getBounds.bind(e),
    };
  };

  const getEditingArea = (): Element => {
    if (!editingArea) {
      throw new Error('Instantiating on missing editing area');
    }
    const element = editingArea;
    if (!element) {
      throw new Error('Cannot find element for editing area');
    }
    if (element.nodeType === 3) {
      throw new Error('Editing area cannot be a text node');
    }
    return element as Element;
  };

  /*
  Renders an editor area, unless it has been provided one to clone.
  */
  const renderEditingArea = (): JSX.Element => {
    const { children, preserveWhitespace } = props;
    const { generation } = state;

    const properties = {
      key: generation,
      ref: (instance: React.ReactInstance | null) => {
        editingArea = instance;
        setTimeout(createEditorInstance, 150);
      },
    };
    // Todo Fix this
    /*if (children.length) {
      return React.cloneElement(
        React.Children.only(children)!,
        properties
      );
    }*/

    return preserveWhitespace ? <pre {...properties} /> : <div {...properties} />;
  };

  const onEditorChange = (
    eventName: 'text-change' | 'selection-change',
    rangeOrDelta: ReactQuill.Range | DeltaStatic,
    oldRangeOrDelta: ReactQuill.Range | DeltaStatic,
    source: Sources,
  ) => {
    if (eventName === 'text-change') {
      onEditorChangeText?.(editor!.root.innerHTML, rangeOrDelta as DeltaStatic, source, unprivilegedEditor!);
    } else if (eventName === 'selection-change') {
      onEditorChangeSelection?.(rangeOrDelta as RangeStatic, source, unprivilegedEditor!);
    }
  };

  const onEditorChangeText = (value: string, delta: DeltaStatic, source: Sources, editor: ReactQuill.UnprivilegedEditor): void => {
    if (!editor) return;

    // We keep storing the same type of value as what the user gives us,
    // so that value comparisons will be more stable and predictable.
    const nextContents = isDelta(value) ? editor.getContents() : editor.getHTML();

    if (nextContents !== getEditorContents()) {
      // Taint `delta` object, so we can recognize whether the user
      // is trying to send it back as `value`, preventing a likely loop.
      lastDeltaChangeSet = delta;

      value = nextContents;
      props.onChange?.(value, delta, source, editor);
    }
  };

  const onEditorChangeSelection = (nextSelection: RangeStatic, source: Sources, editor: ReactQuill.UnprivilegedEditor): void => {
    if (!editor) return;
    const currentSelection = getEditorSelection();
    const hasGainedFocus = !currentSelection && nextSelection;
    const hasLostFocus = currentSelection && !nextSelection;

    if (isEqual(nextSelection, currentSelection)) return;

    selection = nextSelection;
    props.onChangeSelection?.(nextSelection, source, editor);

    if (hasGainedFocus) {
      props.onFocus?.(nextSelection, source, editor);
    } else if (hasLostFocus) {
      props.onBlur?.(currentSelection, source, editor);
    }
  };

  const focus = (): void => {
    if (!editor) return;
    editor.focus();
  };

  const blur = (): void => {
    if (!editor) return;
    selection = null;
    editor.blur();
  };

  return (
    <div
      id={props.id}
      style={props.style}
      key={state.generation}
      className={`quill ${props.className ?? ''}`}
      onKeyPress={props.onKeyPress}
      onKeyDown={props.onKeyDown}
      onKeyUp={props.onKeyUp}
    >
      {renderEditingArea()}
    </div>
  );
};

/*
Small helper to execute a function in the next micro-tick.
*/
function postpone(fn: (value: void) => void) {
  Promise.resolve().then(fn);
}
