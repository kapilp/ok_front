import JSONEditor, { JSONEditorOptions } from 'jsoneditor';
import 'jsoneditor/dist/jsoneditor.css';
// TODO: move to new beta version of jsonEditor:
// https://github.com/josdejong/jsoneditor/issues/1223
import { mergeProps, onCleanup } from 'solid-js';
import { Label } from '../Label';
import { Field } from '../../form2';

interface Properties {
  disabled?: boolean;
  onChange: (value: string | number) => void;
  // isSaving?: boolean
  required?: boolean;
  value: {};
  error?: string;
  placeholder?: string;
  label?: string;
  props?: {};
  dom?: HTMLElement | ((e: HTMLElement) => void);
  class?: string;
  field: Field;
}

export const JsonEditor = (props: Properties) => {
  props = mergeProps({}, { value: {} }, props);
  let jsonEditor: JSONEditor;

  /* let updatePropsValue = true;
   createComputed(() => {
    // reset editor focus when editor value change
    if (typeof props.field.value === 'object' && jsonEditor && updatePropsValue) jsonEditor.set(props.field.value);
  }); */

  const options: JSONEditorOptions = {
    mode: props.disabled ? 'view' : 'text', // code
    modes: ['code', 'tree', 'text'],
    onChange() {
      try {
        const value = jsonEditor.get();
        if (props.onChange) {
          props.onChange(value);
          // updatePropsValue = false;
        }
      } catch (err) {
        console.warn('Error in onChange callback: ', err);
      }
    },
  };

  const createEditor = (el: HTMLDivElement) => {
    jsonEditor = new JSONEditor(el, options, props.field.value || {});
  };

  onCleanup(() => {
    if (jsonEditor) jsonEditor.destroy();
  });

  return (
    <>
      <Label name={props.label || ''} />
      <div ref={createEditor} style="width: 600px; height: 400px; display: inline-block;" />
      <span>{props.error}</span>
    </>
  );
};
