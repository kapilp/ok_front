import Codemirror from 'codemirror';
import { JSX, createEffect, onCleanup } from 'solid-js';
// cant import with vite: https://github.com/codemirror/CodeMirror/issues/5403
import './codeMirrorPlugins';

// copied from : https://github.com/scniro/react-codemirror2
export interface IDefineModeOptions {
  fn: () => Codemirror.Mode<any>;
  name: string;
}

export interface ISetScrollOptions {
  x?: number | null;
  y?: number | null;
}

export interface ISetSelectionOptions {
  anchor: Codemirror.Position;
  head: Codemirror.Position;
}

export interface DomEvent {
  (editor: Codemirror.Editor, event?: any): void;
}

export interface KeyHandledEvent {
  (editor: Codemirror.Editor, name: string, event: any): void;
}

export interface EditorChangeEvent {
  (editor: Codemirror.Editor, changeObj: Codemirror.EditorChange): void;
}

export interface ICodeMirror {
  autoCursor?: boolean; // default: true
  autoScroll?: boolean; // default: false
  className?: string;
  cursor?: Codemirror.Position;
  defineMode?: IDefineModeOptions;
  editorDidConfigure?: (editor: Codemirror.Editor) => void;
  editorDidMount?: (editor: Codemirror.Editor, value: string, cb: () => void) => void;
  editorWillUnmount?: (lib: any) => void;
  onBlur?: DomEvent;
  onChange?: (editor: Codemirror.Editor, data: Codemirror.EditorChange, value: string) => void;
  onContextMenu?: DomEvent;
  onCopy?: DomEvent;
  onCursor?: (editor: Codemirror.Editor, data: Codemirror.Position) => void;
  onCut?: DomEvent;
  onCursorActivity?: (editor: Codemirror.Editor) => void;
  onDblClick?: DomEvent;
  onDragEnter?: DomEvent;
  onDragLeave?: DomEvent;
  onDragOver?: DomEvent;
  onDragStart?: DomEvent;
  onDrop?: DomEvent;
  onFocus?: DomEvent;
  onGutterClick?: (editor: Codemirror.Editor, lineNumber: number, gutter: string, event: Event) => void;
  onInputRead?: EditorChangeEvent;
  onKeyDown?: DomEvent;
  onKeyHandled?: KeyHandledEvent;
  onKeyPress?: DomEvent;
  onKeyUp?: DomEvent;
  onMouseDown?: DomEvent;
  onPaste?: DomEvent;
  onRenderLine?: (editor: Codemirror.Editor, line: Codemirror.LineHandle, element: HTMLElement) => void;
  onScroll?: (editor: Codemirror.Editor, data: Codemirror.ScrollInfo) => void;
  onSelection?: (editor: Codemirror.Editor, data: any) => void;
  onTouchStart?: DomEvent;
  onUpdate?: (editor: Codemirror.Editor) => void;
  onViewportChange?: (editor: Codemirror.Editor, start: number, end: number) => void;
  options?: Codemirror.EditorConfiguration;
  selection?: { ranges: Array<ISetSelectionOptions>; focus?: boolean };
  scroll?: ISetScrollOptions;
}

export interface IControlledCodeMirror extends ICodeMirror {
  onBeforeChange: (editor: Codemirror.Editor, data: Codemirror.EditorChange, value: string) => void;
  value: string;
}

export interface IUnControlledCodeMirror extends ICodeMirror {
  detach?: boolean;
  editorDidAttach?: (editor: Codemirror.Editor) => void;
  editorDidDetach?: (editor: Codemirror.Editor) => void;
  onBeforeChange?: (editor: Codemirror.Editor, data: Codemirror.EditorChange, value: string, next: () => void) => void;
  value?: string;
}

declare interface ICommon {
  wire: (props: IControlledCodeMirror | IUnControlledCodeMirror) => void;
  apply: (props: IControlledCodeMirror | IUnControlledCodeMirror) => void;
  applyNext: (props: IControlledCodeMirror | IUnControlledCodeMirror, next?: IControlledCodeMirror | IUnControlledCodeMirror, preserved?: IPreservedOptions) => void;
  applyUserDefined: (props: IControlledCodeMirror | IUnControlledCodeMirror, preserved?: IPreservedOptions) => void;
}

declare interface IPreservedOptions {
  cursor?: Codemirror.Position;
}

abstract class Helper {
  public static equals(x: {}, y: {}) {
    const ok = Object.keys,
      tx = typeof x,
      ty = typeof y;
    return x && y && tx === 'object' && tx === ty ? ok(x).length === ok(y).length && ok(x).every(key => equals(x[key], y[key])) : x === y;
  }
}

class Shared implements ICommon {
  private readonly editor: Codemirror.Editor;
  private props: ICodeMirror;

  constructor(editor: Codemirror.Editor, props: ICodeMirror) {
    editor = editor;
    props = props;
  }

  delegateCursor(position: Codemirror.Position, scroll?: boolean, focus?: boolean) {
    const doc = editor.getDoc() as Codemirror.Doc;

    if (focus) {
      editor.focus();
    }

    scroll ? doc.setCursor(position) : doc.setCursor(position, null, { scroll: false });
  }

  delegateScroll(coordinates: ISetScrollOptions) {
    editor.scrollTo(coordinates.x, coordinates.y);
  }

  delegateSelection(ranges: Array<ISetSelectionOptions>, focus?: boolean) {
    const doc = editor.getDoc() as Codemirror.Doc;
    doc.setSelections(ranges);

    if (focus) {
      editor.focus();
    }
  }

  public apply(props: IControlledCodeMirror | IUnControlledCodeMirror) {
    // init ranges
    if (props && props.selection && props.selection.ranges) {
      delegateSelection(props.selection.ranges, props.selection.focus || false);
    }

    // init cursor
    if (props && props.cursor) {
      delegateCursor(props.cursor, props.autoScroll || false, editor.getOption('autofocus') || false);
    }

    // init scroll
    if (props && props.scroll) {
      delegateScroll(props.scroll);
    }
  }

  public applyNext(props: IControlledCodeMirror | IUnControlledCodeMirror, next?: IControlledCodeMirror | IUnControlledCodeMirror, preserved?: any) {
    // handle new ranges
    if (props && props.selection && props.selection.ranges) {
      if (next && next.selection && next.selection.ranges && !Helper.equals(props.selection.ranges, next.selection.ranges)) {
        delegateSelection(next.selection.ranges, next.selection.focus || false);
      }
    }

    // handle new cursor
    if (props && props.cursor) {
      if (next && next.cursor && !Helper.equals(props.cursor, next.cursor)) {
        delegateCursor(preserved.cursor || next.cursor, next.autoScroll || false, next.autoCursor || false);
      }
    }

    // handle new scroll
    if (props && props.scroll) {
      if (next && next.scroll && !Helper.equals(props.scroll, next.scroll)) {
        delegateScroll(next.scroll);
      }
    }
  }

  public applyUserDefined(props: IControlledCodeMirror | IUnControlledCodeMirror, preserved?: any) {
    if (preserved && preserved.cursor) {
      delegateCursor(preserved.cursor, props.autoScroll || false, editor.getOption('autofocus') || false);
    }
  }

  public wire(props: IControlledCodeMirror | IUnControlledCodeMirror) {
    Object.keys(props || {})
      .filter(p => /^on/.test(p))
      .forEach(prop => {
        switch (prop) {
          case 'onBlur':
            {
              editor.on('blur', (cm, event) => {
                props.onBlur && props.onBlur(editor, event);
              });
            }
            break;
          case 'onContextMenu': {
            editor.on('contextmenu', (cm, event) => {
              props.onContextMenu && props.onContextMenu(editor, event);
            });
            break;
          }
          case 'onCopy': {
            editor.on('copy', (cm, event?) => {
              props.onCopy && props.onCopy(editor, event);
            });
            break;
          }
          case 'onCursor':
            {
              editor.on('cursorActivity', cm => {
                props.onCursor && props.onCursor(editor, editor.getDoc().getCursor());
              });
            }
            break;
          case 'onCursorActivity':
            {
              editor.on('cursorActivity', cm => {
                props.onCursorActivity && props.onCursorActivity(editor);
              });
            }
            break;
          case 'onCut': {
            editor.on('cut', (cm, event?) => {
              props.onCut && props.onCut(editor, event);
            });
            break;
          }
          case 'onDblClick': {
            editor.on('dblclick', (cm, event) => {
              props.onDblClick && props.onDblClick(editor, event);
            });
            break;
          }
          case 'onDragEnter':
            {
              editor.on('dragenter', (cm, event) => {
                props.onDragEnter && props.onDragEnter(editor, event);
              });
            }
            break;
          case 'onDragLeave': {
            editor.on('dragleave', (cm, event) => {
              props.onDragLeave && props.onDragLeave(editor, event);
            });
            break;
          }
          case 'onDragOver':
            {
              editor.on('dragover', (cm, event) => {
                props.onDragOver && props.onDragOver(editor, event);
              });
            }
            break;
          case 'onDragStart': {
            editor.on('dragstart', (cm, event) => {
              props.onDragStart && props.onDragStart(editor, event);
            });
            break;
          }
          case 'onDrop':
            {
              editor.on('drop', (cm, event) => {
                props.onDrop && props.onDrop(editor, event);
              });
            }
            break;
          case 'onFocus':
            {
              editor.on('focus', (cm, event) => {
                props.onFocus && props.onFocus(editor, event);
              });
            }
            break;
          case 'onGutterClick':
            {
              editor.on('gutterClick', (cm, lineNumber, gutter, event) => {
                props.onGutterClick && props.onGutterClick(editor, lineNumber, gutter, event);
              });
            }
            break;
          case 'onInputRead':
            {
              editor.on('inputRead', (cm, EditorChangeEvent) => {
                props.onInputRead && props.onInputRead(editor, EditorChangeEvent);
              });
            }
            break;
          case 'onKeyDown':
            {
              editor.on('keydown', (cm, event) => {
                props.onKeyDown && props.onKeyDown(editor, event);
              });
            }
            break;
          case 'onKeyHandled':
            {
              editor.on('keyHandled', (cm, key, event) => {
                props.onKeyHandled && props.onKeyHandled(editor, key, event);
              });
            }
            break;
          case 'onKeyPress':
            {
              editor.on('keypress', (cm, event) => {
                props.onKeyPress && props.onKeyPress(editor, event);
              });
            }
            break;
          case 'onKeyUp':
            {
              editor.on('keyup', (cm, event) => {
                props.onKeyUp && props.onKeyUp(editor, event);
              });
            }
            break;
          case 'onMouseDown': {
            editor.on('mousedown', (cm, event) => {
              props.onMouseDown && props.onMouseDown(editor, event);
            });
            break;
          }
          case 'onPaste': {
            editor.on('paste', (cm, event?) => {
              props.onPaste && props.onPaste(editor, event);
            });
            break;
          }
          case 'onRenderLine': {
            editor.on('renderLine', (cm, line, element) => {
              props.onRenderLine && props.onRenderLine(editor, line, element);
            });
            break;
          }
          case 'onScroll':
            {
              editor.on('scroll', cm => {
                props.onScroll && props.onScroll(editor, editor.getScrollInfo());
              });
            }
            break;
          case 'onSelection':
            {
              editor.on('beforeSelectionChange', (cm, data) => {
                props.onSelection && props.onSelection(editor, data);
              });
            }
            break;
          case 'onTouchStart': {
            editor.on('touchstart', (cm, event) => {
              props.onTouchStart && props.onTouchStart(editor, event);
            });
            break;
          }
          case 'onUpdate':
            {
              editor.on('update', cm => {
                props.onUpdate && props.onUpdate(editor);
              });
            }
            break;
          case 'onViewportChange':
            {
              editor.on('viewportChange', (cm, from, to) => {
                props.onViewportChange && props.onViewportChange(editor, from, to);
              });
            }
            break;
        }
      });
  }
}

export const ControlledCodeMirror = (props: IControlledCodeMirror) => {
  let editor: Codemirror.Editor;

  let mirror: any;
  let ref: HTMLElement;
  let shared: Shared;

  let applied = false;
  // let appliedNext = false;
  // let appliedUserDefined = false;
  let deferred = null;
  const emulating = false;
  // let hydrated = false;
  const initCb = () => {
    if (props.editorDidConfigure) {
      props.editorDidConfigure(editor);
    }
  };
  let mounted = false;

  /** @internal */
  const mirrorChange = (deferred: Codemirror.EditorChangeCancellable) => {
    const doc = editor.getDoc();

    if (deferred.origin === 'undo') {
      doc.setHistory(mirror.getHistory());
      mirror.undo();
    } else if (deferred.origin === 'redo') {
      doc.setHistory(mirror.getHistory());
      mirror.redo();
    } else {
      mirror.replaceRange(deferred.text, deferred.from, deferred.to, deferred.origin);
    }

    return mirror.getValue();
  };

  const handleNodeChange = (node: HTMLElement) => {
    ref = node;
    createCodeMirrorInstance();
  };
  /** @internal */
  const createCodeMirrorInstance = () => {
    if (props.defineMode) {
      if (props.defineMode.name && props.defineMode.fn) {
        Codemirror.defineMode(props.defineMode.name, props.defineMode.fn);
      }
    }

    editor = Codemirror(ref, props.options) as Codemirror.Editor;
    editor.setValue(props.value || ''); // added (must not be null)

    shared = new Shared(editor, props);

    mirror = (Codemirror as any)(() => {}, props.options);

    editor.on('electricInput', () => {
      mirror.setHistory(editor.getDoc().getHistory());
    });

    editor.on('cursorActivity', () => {
      mirror.setCursor(editor.getDoc().getCursor());
    });

    editor.on('beforeChange', (cm, data) => {
      if (emulating) {
        return;
      }

      //data.cancel(); // when this line is uncommented i cant see font on editor

      deferred = data;

      const phantomChange = mirrorChange(deferred);

      if (props.onBeforeChange) props.onBeforeChange(editor, deferred, phantomChange);
    });

    editor.on('change', (cm, data) => {
      if (!mounted) {
        return;
      }

      if (props.onChange) {
        props.onChange(editor, data, editor.getValue());
      }
    });

    shared.apply(props);

    applied = true;

    mounted = true;

    shared.wire(props);

    if (editor.getOption('autofocus')) {
      editor.focus();
    }

    if (props.editorDidMount) {
      props.editorDidMount(editor, editor.getValue(), initCb);
    }
  };

  createEffect(() => {
    const preserved: IPreservedOptions = { cursor: undefined };

    if (!props.autoCursor && props.autoCursor !== undefined) {
      preserved.cursor = editor.getDoc().getCursor();
    }
  });

  onCleanup(() => {
    if (props.editorWillUnmount) {
      props.editorWillUnmount(Codemirror);
    }
  });
  const className = props.className ? `react-codemirror2 ${props.className}` : 'react-codemirror2';
  return <div className={className} ref={handleNodeChange} />;
};
