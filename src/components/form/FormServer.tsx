/**
* This is the Main Component for the form. it render form from the schema.
\todo make model form working
\todo make user able to add custom fields to any form
\todo make form work for multiple level modification by user
\todo make update & delete return preojection of inserted/updated members too
\todo schema can be made to not make projection for inserted/updated/deleted members
 */
// https://github.com/foxhound87/mobx-react-form
// make focus working too
import classNames from 'classnames';
import { JSX, For, Match, Show, Switch } from 'solid-js';
import { isObj } from 'ramda-adjunct';
import { css } from 'goober';
import { FieldType, FormFields, getIsProduction } from '../../utils/enums';

import { HtmlBase } from '../UI/Html';
import { Button, ButtonTypes, ButtonVariants } from '../UI/core/button/src/Button';
import { DynamicGrid } from '../UI/core/dynamic-grid/src/DynamicGrid';
import { Fieldset } from '../UI/core/form-fieldset/src/Fieldset';
import { useTranslation } from '../react-i18next/src/useTranslation';
import { ActionFooter } from '../UI/core/action-footer/src/ActionFooter';
import { Spacer } from '../UI/core/spacer/src/Spacer';
import { GeneralInput } from './GeneralInput';

import { ServerFormProperties, useFormServer } from './useFormServer';
import { Alert } from '../UI/core/alert/src/Alert';
import { Region } from '../UI/core/dynamic-grid/src/Region';
// For validation can use : https://github.com/formsy/formsy-react/blob/master/src/validationRules.ts
// todo set validation like: https://codesandbox.io/s/solid-form-validation-2cdti?file=/src/validation.js
// great from library: https://github.com/joeattardi/lwc-forms

export const FormServer = (props: Readonly<ServerFormProperties>) => {
  const { state, optionsState, serverState, handleChange, handleBlur, handleSubmit, onApply, onDelete, onCancel } = useFormServer(props);
  const { t, i18n } = useTranslation();
  const AllInputDisplay = (pInternal: { layout: {}; layoutCss: {} }) => {
    // Getting Region template from Props so only one layout is possible.
    const isHidden = id => css`
      ${state.fields[id].type == FieldType.hidden ? 'display: none' : ''};
    `;
    return (
      <DynamicGrid {...(pInternal.layoutCss ?? {})}>
        <For each={Object.keys(pInternal.layout)} fallback={<div>Loading Form Loop...</div>}>
          {id => (
            <Switch
              fallback={
                <Region defaultPosition={state.fields[id]?.props?.region ?? {}} className={isHidden(id)}>
                  <GeneralInput
                    showKeyRev={state.showKeyRev}
                    value={state.values[id]}
                    type={state.fields[id].type}
                    label={t(state.fields[id].label)}
                    required={state.fields[id].isRequired}
                    disabled={optionsState.disabled || state.fields[id].isDisabled}
                    props={state.fields[id].props}
                    error={t(state.errors[id])}
                    touched={t(state.touched[id])}
                    onChange={v => handleChange(id)(v)}
                    onBlur={handleBlur(id)}
                    help={t(state.fields[id].description)}
                  />
                </Region>
              }
            >
              <Match when={pInternal.layout[id].type == FieldType.fieldSet}>
                <Fieldset {...(pInternal.layout[id]?.props ?? {})}>
                  <AllInputDisplay layout={pInternal.layout[id].layout} layoutCss={pInternal.layout[id]?.layoutCss ?? {}} />
                </Fieldset>
              </Match>
              <Match when={pInternal.layout[id].type == FieldType.tab}>
                <Fieldset {...(pInternal.layout[id]?.props ?? {})}>
                  <AllInputDisplay layout={pInternal.layout[id].layout} layoutCss={pInternal.layout[id]?.layoutCss ?? {}} />
                </Fieldset>
              </Match>
            </Switch>
          )}
        </For>
      </DynamicGrid>
    );
  };
  return (
    <Show when={serverState.isFetchedTopTemplate && serverState.isFetchedBottomTemplate}>
      <Show when={props.t}>
        <HtmlBase value={serverState.topHtmlTemplate} />
      </Show>
      <Show when={Object.keys(state.fields).length > 0} fallback={<div>Loading Form...</div>}>
        <form className={props.schemaKey} onSubmit={handleSubmit}>
          <AllInputDisplay layoutCss={optionsState.layoutCss} layout={state.layout} />
          <ActionFooter
            end={
              <>
                <Spacer isInlineBlock marginRight="medium">
                  <Button
                    type={ButtonTypes.SUBMIT}
                    variant={ButtonVariants.EMPHASIS}
                    isDisabled={!state.isFormUpdated || state.isSubmitting}
                    className={classNames('submit', 'success', { loading: state.isSubmitting })}
                    text={state.buttonLabels?.save || 'Submit'}
                  />
                </Spacer>
                <Show when={state.buttonLabels?.apply}>
                  <Button
                    type={ButtonTypes.SUBMIT}
                    isDisabled={!state.isFormUpdated || state.isSubmitting}
                    className={classNames('submit', 'success', { loading: state.isSubmitting })}
                    text={state.buttonLabels.apply || 'Apply'}
                    onClick={onApply}
                  />
                </Show>
                <Show when={state.buttonLabels?.cancel ?? true}>
                  <Button
                    isDisabled={state.isSubmitting}
                    className={classNames('cancel', { loading: state.isSubmitting })}
                    text={state.buttonLabels?.cancel ?? 'Cancel'}
                    onClick={onCancel}
                  />
                </Show>
              </>
            }
          />
          <Show when={props.onDeleteOneRow}>
            <Button text="Delete" onClick={onDelete} />
          </Show>
          {/* <button type="button" on:click={onReset}>Reset</button> */}
        </form>
      </Show>
      {state.er && <Alert type="error">{state.er}</Alert>}
      {!getIsProduction() && JSON.stringify(state.values)}
      <Show when={props.b}>
        <HtmlBase value={serverState.bottomHtmlTemplate} />
      </Show>
    </Show>
  );
};
