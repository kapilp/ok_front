import { useFormContext } from './FormContext';
import { isFunction, isEmptyChildren, isObject } from '../../utils/enums';
import invariant from 'tiny-warning';
import { FormikProps, GenericFieldHTMLAttributes, FieldMetaProps, FieldHelperProps, FieldInputProps, FieldValidator } from '../UI/validators/formik/packages/formik/src/types';
import { JSX, createComponent, createEffect, createMemo, onCleanup, splitProps } from 'solid-js';
import { IS_PRODUCTION } from '../../utils/enums';
import { Dynamic } from 'solid-js/web';
// Add parse, format, formatOnBlur to getFieldProps()
//https://github.com/formium/formik/pull/2255
export interface FieldProps<V = any, FormValues = any> {
  field: FieldInputProps<V>;
  form: FormikProps<FormValues>; // if ppl want to restrict this for a given form, let them.
  meta: FieldMetaProps<V>;
}

export interface FieldConfig<V = any> {
  /**
   * Field component to render. Can either be a string like 'select' or a component.
   */
  component?: string | JSX.ComponentType<FieldProps<V>> | JSX.ComponentType | JSX.ForwardRefExoticComponent<any>;

  /**
   * Component to render. Can either be a string e.g. 'select', 'input', or 'textarea', or a component.
   */
  as?: JSX.ComponentType<FieldProps<V>['field']> | string | JSX.ComponentType | JSX.ForwardRefExoticComponent<any>;

  /**
   * Children render function <Field name>{props => ...}</Field>)
   */
  children?: ((props: FieldProps<V>) => JSX.Element) | JSX.Element;

  /**
   * Validate a single field value independently
   */
  validate?: FieldValidator;
  /**
   * Function to parse raw input value before setting it to state
   */
  parse?: (value: unknown, name: string) => any;

  /**
   * Function to transform value passed to input
   */
  format?: (value: any, name: string) => any;
  /**
   * Wait until blur event before formatting input value?
   * @default false
   */
  formatOnBlur?: boolean;

  /**
   * Field name
   */
  name: string;

  /** HTML input type */
  type?: string;

  /** Field value */
  value?: any;

  /** Inner ref */
  innerRef?: (instance: any) => void;
}

export type FieldAttributes<T> = GenericFieldHTMLAttributes & FieldConfig<T> & T & { name: string };

export type FieldHookConfig<T> = GenericFieldHTMLAttributes & FieldConfig<T>;
// deprecated
export function useField<Val = any>(propsOrFieldName: string | FieldHookConfig<Val>): [FieldInputProps<Val>, FieldMetaProps<Val>, FieldHelperProps<Val>] {
  const formik = useFormContext();
  const { getFieldProps, getFieldMeta, getFieldHelpers, registerField, unregisterField } = formik;

  const isAnObject = isObject(propsOrFieldName);

  // Normalize propsOrFieldName to FieldHookConfig<Val>
  const props: FieldHookConfig<Val> = isAnObject ? (propsOrFieldName as FieldHookConfig<Val>) : { name: propsOrFieldName as string };

  createEffect(() => {
    if (props.name) {
      registerField(props.name, {
        validate: props.validate,
      });
    }
  }); //, [registerField, unregisterField, props.name, props.validate]);
  onCleanup(() => {
    if (props.name) {
      unregisterField(props.name);
    }
  });

  if (!IS_PRODUCTION) {
    invariant(formik, 'useField() / <Field /> must be used underneath a <Formik> component or withFormik() higher order component');
  }

  invariant(props.name, 'Invalid field name. Either pass `useField` a string or an object containing a `name` key.');

  return [getFieldProps(props), getFieldMeta(props.name), getFieldHelpers(props.name)];
}

export function Field(props: FieldAttributes<any>) {
  const [p, customProps] = splitProps(props, [
    'validate',
    'name',
    'children',
    'as', // `as` is reserved in typescript lol
    'component',
  ]);

  const {
    validate: _validate,
    validationSchema: _validationSchema,

    ...formik
  } = useFormContext();

  if (!IS_PRODUCTION) {
    createEffect(() => {
      invariant(
        !(p.as && p.children && isFunction(p.children)),
        'You should not use <Field as> and <Field children> as a function in the same <Field> component; <Field as> will be ignored.',
      );

      invariant(
        !(p.component && p.children && isFunction(p.children)),
        'You should not use <Field component> and <Field children> as a function in the same <Field> component; <Field component> will be ignored.',
      );

      invariant(
        !(p.children && !isEmptyChildren(p.children)),
        'You should not use <Field render> and <Field children> in the same <Field> component; <Field children> will be ignored',
      );
    });
  }

  // Register field and field-level validation with parent <Formik>
  const { registerField, unregisterField } = formik;
  let previousRegistered;
  createEffect(() => {
    console.log('registerring field');
    if (previousRegistered) unregisterField(p.name);
    registerField(p.name, {
      validate: p.validate,
    });
    previousRegistered = p.name;
  }); //, [registerField, unregisterField, p.name, p.validate]);
  onCleanup(() => {
    console.log('unregistering field');
    unregisterField(p.name);
  });
  const field = formik.getFieldProps({ name: p.name, ...customProps });
  const meta = formik.getFieldMeta(p.name);
  const legacyBag = { field, form: formik };

  if (isFunction(p.children)) {
    return p.children({ ...legacyBag, meta });
  }

  if (p.component) {
    // This behavior is backwards compat with earlier Formik 0.9 to 1.x
    if (typeof p.component === 'string') {
      return (
        <Dynamic component={p.component} {...{ ...field(), ...customProps }}>
          {p.children}
        </Dynamic>
      );
    }
    // We don't pass `meta` for backwards compat
    return (
      <Dynamic component={p.component} {...{ ...field(), form: formik, ...customProps }}>
        {p.children}
      </Dynamic>
    );
  }

  // default to input here so we can check for both `as` and `children` above
  const asElement = createMemo(() => p.as || 'input');
  console.log('new Field Regisss...');
  return (
    <>
      {p.name}
      <Dynamic component={asElement()} {...{ ...field(), ...customProps }}>
        {p.children}
      </Dynamic>
    </>
  );
}
