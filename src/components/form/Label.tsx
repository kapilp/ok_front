export const Label = (props: { name?: string; class?: string }) => {
  return <span className="label"> {props.name}</span>;
};
