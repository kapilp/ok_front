import { FormikContextType } from '../UI/validators/formik/packages/formik/src/types';
import { isFunction } from '../../utils/enums';
import { JSX, createMemo, splitProps } from 'solid-js';
import invariant from 'tiny-warning';
import { Dynamic } from 'solid-js/web';
import { FormConsumer, useFormContext } from './FormContext';

export interface ErrorMessageProps {
  name: string;
  className?: string;
  component?: string | JSX.ComponentType;
  children?: (errorMessage: string) => JSX.Element;
  render?: (errorMessage: string) => JSX.Element;
}

export const ErrorMessage = (props: JSX.Component<ErrorMessageProps & { formik: FormikContextType<any> }>) => {
  const formik = useFormContext();
  invariant(
    !!formik,
    `Formik context is undefined, please verify you are rendering <Form>, <Field>, <FastField>, <FieldArray>, or your custom context-using component as a child of a <Formik> component. Component name: `,
  );
  // <FormConsumer> not worked so using context
  const [p, customProps] = splitProps(props, ['component', 'children', 'name']); //'render'
  const touch = () => formik.state.fieldRegistry[p.name].fieldState.touched; //getIn
  const error = () => formik.state.fieldRegistry[p.name].fieldState.error; //getIn // Because property is initially not available its not reactive. so use state
  return (
    <>
      {!!touch() && !!error() ? (
        p.children ? (
          isFunction(p.children) ? (
            p.children(error())
          ) : null
        ) : p.component ? (
          <Dynamic component={p.component} {...customProps} formik={formik}>
            {error}
          </Dynamic>
        ) : (
          <>{error}</>
        )
      ) : null}
    </>
  );
};
//<pre>{JSON.stringify(formik.state.fieldRegistry[p.name].fieldState, null, 2)}</pre>
//export const ErrorMessage = connect<ErrorMessageProps, ErrorMessageProps & { formik: FormikContextType<any> }>(ErrorMessageImpl);
