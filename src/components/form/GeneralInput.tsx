import { Hook } from "flatpickr/dist/types/options";
import { FieldType, FormValue } from "../../utils/enums";
import { ArrayInput } from "./input/Array";
// import {JsonEditor} from './input/JsonEditor';
// import {Flatpicker} from './input/Flatpicker';
import { DateTimePicker } from "./input/Flatpicker";
// import {DropZone} from './input/DropZone';
// import {DateRange} from './input/DateRange';
// import {Emoji} from './input/Emoji';
// import {CLEditor} from './input/CLEditor';
// import {TableForm} from './tableform/TableForm';
// import {ArrayForm} from './input/Array';
import { JsonEditor } from "./input/JsonEditor";
// import {Checkboxes} from './input/Checkboxes';
// import {File} from './input/File';
// import {Radio} from './input/Radio';
import { SingleSelect } from "./tableform/SingleSelect";
import { TableFormBoolProperties } from "./tableform/TODOBoolPropTableForm";
import { UUIDInput } from "./input/UUID";
import { InputField } from "../UI/core/form-input/src/InputField";
import { TextareaField } from "../UI/core/form-textarea/src/TextareaField";
import { CheckboxField } from "../UI/core/form-checkbox/src/CheckboxField";
import { RadioField } from "../UI/core/form-radio/src/RadioField";
import { JSX, Match, Switch } from "solid-js";
import { TableForm } from "./tableform/TableForm";

// export let value;
// export let type = FormType.text;
// export let label = '';
// export let required = false;
// export let disabled = false;
// export let description = '';
// export let error = '';
// export let props = {};
export const doms = {};
// export let showKeyRev;

export const InputWrapper = (props: { children: JSX.Element }) => {
  return <div className="form-item">{props.children}</div>;
};

interface Properties {
  disabled?: boolean; // done
  onChange: (value: string | number | string[]) => void;
  onBlur: (e: any) => void;
  //isSaving?: boolean
  required?: boolean; // done
  value: FormValue;
  error?: string; // done
  touched?: boolean;
  placeholder?: string;
  label?: string; // done
  description?: string; // done
  props?: {}; // done
  dom?: HTMLElement | ((e: HTMLElement) => void);
  class?: string;
  type: FieldType; // done
  showKeyRev?: boolean;
}

export const GeneralInput = (props: Properties) => {
  if (props.h) {
    return <div></div>;
  }
  if (!props.showKeyRev) {
    if (["Key", "Rev"].includes(props.label || "")) {
      return <div></div>;
    }
  }
  const getFirstValue: Hook | Hook[] = newValue => {
    const value = Array.isArray(newValue) && newValue.length === 1 ? newValue[0].getTime() : newValue[0].getTime();
    if (props.onChange) props.onChange(value);
  };
  /*const extraProps = {};
  if (type === FormType.select) {
      extraProps.multiSelect = false;
    } else if (type === FormType.multi_select) {
      extraProps.multiSelect = true;
    } else if (type === FormType.multi_select_bool_properties) {
      extraProps.multiSelect = true;
      extraProps.boolprop = true;
    }*/

  return (
    <Switch fallback={<div>Unknown Component type: {props.type}</div>}>
      <Match when={props.type == FieldType.color}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="color"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.email}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="email"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.hidden}>
        <InputField
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="hidden"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match
        when={props.type == FieldType.number || props.type == FieldType.serial}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(parseFloat(e.target.value))}
          onBlur={props.onBlur}
          type="number"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.password}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="password"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.range}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="range"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.search}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="search"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.text}>
        <InputField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="text"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.checkbox}>
        <CheckboxField
          label={props.label}
          value={props.value as string}
          checked={!!props.value}
          onChange={e => props.onChange(e.target.checked)}
          onBlur={props.onBlur}
          type="checkbox"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.radio}>
        <RadioField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          type="radio"
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match
        when={props.type == FieldType.textarea || props.type == FieldType.codemirror}>
        <TextareaField
          label={props.label}
          value={props.value as string}
          onChange={e => props.onChange(e.target.value)}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.select}>
        <SingleSelect
          label={props.label}
          value={props.value as string}
          onChange={props.onChange}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          {...(props.props ?? {})}
        />
      </Match>
      <Match when={props.type == FieldType.text_array}>
        <ArrayInput
          label={props.label}
          value={props.value as string[]}
          onChange={props.onChange}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          {...(props.props ?? {})}
        />
      </Match>
      <Match when={props.type == FieldType.multi_select}>
        <TableForm
          label={props.label}
          value={props.value as Array<{ _key: string; value: {} }>}
          onChange={props.onChange}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          {...(props.props ?? {})}
        />
      </Match>
      <Match when={props.type == FieldType.multi_select_bool_properties}>
        <TableFormBoolProperties
          label={props.label}
          value={props.value as Array<{ _key: string; value: {} }>}
          onChange={props.onChange}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          {...(props.props ?? {})}
        />
      </Match>
      <Match when={props.type == FieldType.jsonEditor}>
        <JsonEditor
          label={props.label}
          value={props.value}
          onChange={props.onChange}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          props={props.props}
          isInvalid={props.error}
        />
      </Match>
      <Match when={props.type == FieldType.flatpicker}>
        <DateTimePicker
          label={props.label}
          value={props.value}
          onChange={getFirstValue}
          onBlur={props.onBlur}
          required={props.required}
          disabled={props.disabled}
          error={props.touched && props.error}
          isInvalid={props.error}
          props={props.props}
        />
      </Match>
      <Match when={props.type == FieldType.uuid}>
        <UUIDInput value={props.value} onChange={props.onChange}
                   onBlur={props.onBlur} {...(props.props ?? {})} />
      </Match>
      <Match when={props.type == FieldType.url}>
        <>
          <span>http://</span>
          <InputField
            label={props.label}
            value={props.value as string}
            onChange={e => props.onChange(e.target.value)}
            onBlur={props.onBlur}
            type="text"
            required={props.required}
            disabled={props.disabled}
            error={props.touched && props.error}
            isInvalid={props.error}
            props={props.props}
          />
          <span>.{props.props?.domain ?? ""}</span>
        </>
      </Match>
    </Switch>
  );
};
