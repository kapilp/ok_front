import { JSX, batch, createEffect, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import cloneDeep from 'lodash/cloneDeep';
import { FormikContextType, FormikState, SharedRenderProps, FormikProps, FormikErrors } from '../UI/validators/formik/packages/formik/src/types';
import { isEmptyChildren, isFunction, isEmptyArray } from '../../utils/enums';
import isEqual from 'fast-deep-equal';
import get from 'lodash/get';
import { useFormContext } from './FormContext';
import toPath from 'lodash/toPath';

// to add optional `explicitValidation` parameter to all FieldArray Helpers:
// https://github.com/formium/formik/pull/1468/files
// solves performance problem
// helper functions concept:https://github.com/formium/formik/pull/1361
// add Callbacks in ArrayHelpers: https://github.com/formium/formik/pull/1323/files

export type FieldArrayRenderProps<FormValue = any, FieldArrayValue = any> = ArrayHelpers<FieldArrayValue> & {
  form: FormikProps<FormValue>;
  name: string;
  [key: string]: any;
};

export type FieldArrayConfig = {
  /** Really the path to the array field to be updated */
  name: string;
  /** Should field array validate the form AFTER array updates/changes? */
  validateOnChange?: boolean;
  /** Allow users to pass props through to component/render/children */
  [key: string]: any;
} & SharedRenderProps<FieldArrayRenderProps>;
export interface ArrayHelpers<FieldArrayValue = any> {
  /** Imperatively add a value to the end of an array */
  push: (obj: FieldArrayValue) => void;
  /** Curried fn to add a value to the end of an array */
  handlePush: (obj: FieldArrayValue) => () => void;
  /** Imperatively swap two values in an array */
  swap: (indexA: number, indexB: number) => void;
  /** Curried fn to swap two values in an array */
  handleSwap: (indexA: number, indexB: number) => () => void;
  /** Imperatively move an element in an array to another index */
  move: (from: number, to: number) => void;
  /** Imperatively move an element in an array to another index */
  handleMove: (from: number, to: number) => () => void;
  /** Imperatively insert an element at a given index into the array */
  insert: (index: number, value: FieldArrayValue) => void;
  /** Curried fn to insert an element at a given index into the array */
  handleInsert: (index: number, value: FieldArrayValue) => () => void;
  /** Imperatively replace a value at an index of an array  */
  replace: (index: number, value: FieldArrayValue) => void;
  /** Curried fn to replace an element at a given index into the array */
  handleReplace: (index: number, value: FieldArrayValue) => () => void;
  /** Imperatively sort an array using given comparator */
  sort: (comp: (a: any, b: any) => number) => void;
  /** Curried fn to sort an array */
  handleSort: (comp: (a: any, b: any) => number) => () => void;
  /** Imperatively add an element to the beginning of an array and return its length */
  unshift: (value: FieldArrayValue) => number;
  /** Curried fn to add an element to the beginning of an array */
  handleUnshift: (value: FieldArrayValue) => () => void;
  /** Curried fn to remove an element at an index of an array */
  handleRemove: (index: number) => () => void;
  /** Curried fn to remove a value from the end of the array */
  handlePop: () => () => void;
  /** Imperatively remove and element at an index of an array */
  remove: (index: number) => FieldArrayValue | undefined;
  /** Imperatively remove and return value from the end of the array */
  pop: () => FieldArrayValue | undefined;
  /** Imperatively get an element at the given index of an array */
  get: (index: number) => FieldArrayValue | undefined;
}

/**
 * Some array helpers!
 */
export const move = (array: any[], from: number, to: number) => {
  const copy = copyArrayLike(array);
  const value = copy[from];
  copy.splice(from, 1);
  copy.splice(to, 0, value);
  return copy;
};

export const swap = (arrayLike: ArrayLike<any>, indexA: number, indexB: number) => {
  const copy = copyArrayLike(arrayLike);
  const a = copy[indexA];
  // copy[indexA] = copy[indexB];
  // copy[indexB] = a;
  const b = copy[indexB];
  if (b === undefined) delete copy[indexA];
  else copy[indexA] = b;
  if (a === undefined) delete copy[indexB];
  else copy[indexB] = a;
  return copy;
};

export const insert = (arrayLike: ArrayLike<any>, index: number, value: any) => {
  const copy = copyArrayLike(arrayLike);
  copy.splice(index, 0, value);
  return copy;
};

export const replace = (arrayLike: ArrayLike<any>, index: number, value: any) => {
  const copy = copyArrayLike(arrayLike);
  copy[index] = value;
  return copy;
};
export const sort = (array: any[], comp: (a: any, b: any) => number) => {
  return Array.prototype.slice.call(array).sort(comp);
};
const copyArrayLike = (arrayLike: ArrayLike<any>) => {
  if (!arrayLike) {
    return [];
  } else if (Array.isArray(arrayLike)) {
    return [...arrayLike];
  } else {
    const maxIndex = Object.keys(arrayLike)
      .map(key => parseInt(key))
      .reduce((max, el) => (el > max ? el : max), 0);
    return Array.from({ ...arrayLike, length: maxIndex + 1 });
  }
};
// import isEqual from 'fast-deep-equal';
export const FieldArray = (props: FieldArrayConfig & { formik: FormikContextType<Values> }) => {
  // static defaultProps = {
  //   validateOnChange: true,
  // };
  const formik = useFormContext();

  // createEffect(prevProps: FieldArrayConfig & { formik: FormikContextType<Values> }) {
  //   if (!isEqual(get(prevProps.formik.values, prevProps.name), get(props.formik.values, props.name)) && props.formik.validateOnChange) {
  //     props.formik.validateFormWithLowPriority(props.formik.values);
  //   }
  // }

  const updateArrayField = (fn: Function, alterTouched: boolean | Function, alterErrors: boolean | Function) => {
    const { name } = props;
    batch(() => {
      // values fn should be executed before updateErrors and updateTouched,
      // otherwise it causes an error with unshift.
      formik.setState('values', name, fn(get(formik.state.values, name)));
      if (alterErrors) {
        let updateErrors = typeof alterErrors === 'function' ? alterErrors : (error: FormikErrors<Values>) => error;
        //let fieldError = updateErrors(formik.state.errors[name]);
        let fieldError = updateErrors(formik.state.fieldRegistry[name]?.fieldState?.error);
        if (isEmptyArray(fieldError)) {
          fieldError = undefined;
        }
        //formik.setState('errors', name, fieldError);
        formik.setFieldError(name, fieldError);
      }
      if (alterTouched) {
        let updateTouched = typeof alterTouched === 'function' ? alterTouched : fn;
        //let fieldTouched = updateTouched(formik.state.touched[name]);
        let fieldTouched = updateTouched(formik.state.fieldRegistry[name]?.fieldState?.touched);
        if (isEmptyArray(fieldTouched)) {
          fieldTouched = undefined;
        }
        //formik.setState('touched', name, fieldTouched);
        formik.setFieldTouched(name, fieldTouched);
      }
    });
  };

  const push = (value: any) => updateArrayField((arrayLike: ArrayLike<any>) => [...copyArrayLike(arrayLike), cloneDeep(value)], false, false);

  const handlePush = (value: any) => () => push(value);

  const swap_ = (indexA: number, indexB: number) => updateArrayField((array: any[]) => swap(array, indexA, indexB), true, true);

  const handleSwap = (indexA: number, indexB: number) => () => swap_(indexA, indexB);

  //https://github.com/formium/formik/pull/2399/files
  //const move = (from: number, to: number) => updateArrayField((array: any[]) => move(array, from, to), true, true);
  const move_ = (from: number, to: number) => {
    updateArrayField(
      (array: any[]) => move(array, from, to),
      (touched: Array<boolean>) => {
        let touchedArr = copyArrayLike(touched);
        touchedArr[to] = true;
        return touchedArr;
      },
      true,
    );
  };

  const handleMove = (from: number, to: number) => () => move_(from, to);

  const insert_ = (index: number, value: any) =>
    updateArrayField(
      (array: any[]) => insert(array, index, value),
      (array: any[]) => insert(array, index, null),
      (array: any[]) => insert(array, index, null),
    );

  const handleInsert = (index: number, value: any) => () => insert_(index, value);

  const replace_ = (index: number, value: any) => updateArrayField((array: any[]) => replace(array, index, value), false, false);

  const handleReplace = (index: number, value: any) => () => replace_(index, value);

  const sort = (comp: (a: any, b: any) => number) => updateArrayField((array: any[]) => sort(array, comp), false, false);

  const handleSort = (comp: (a: any, b: any) => number) => () => sort(comp);

  const unshift = (value: any) => {
    let length = -1;
    updateArrayField(
      (array: any[]) => {
        const arr = array ? [value, ...array] : [value];
        if (length < 0) {
          length = arr.length;
        }
        return arr;
      },
      (array: any[]) => {
        const arr = array ? [null, ...array] : [null];
        if (length < 0) {
          length = arr.length;
        }
        return arr;
      },
      (array: any[]) => {
        const arr = array ? [null, ...array] : [null];
        if (length < 0) {
          length = arr.length;
        }
        return arr;
      },
    );
    return length;
  };

  const handleUnshift = (value: any) => () => unshift(value);

  const remove: <T>(index: number) => T = index => {
    // We need to make sure we also remove relevant pieces of `touched` and `errors`
    let result: any;
    updateArrayField(
      // so this gets call 3 times
      (array?: any[]) => {
        const copy = array ? copyArrayLike(array) : [];
        if (!result) {
          result = copy[index];
        }
        if (isFunction(copy.splice)) {
          copy.splice(index, 1);
        }
        return copy;
      },
      true,
      true,
    );

    return result as T;
  };

  const handleRemove = (index: number) => () => remove<any>(index);

  const pop: <T>() => T = () => {
    // Remove relevant pieces of `touched` and `errors` too!
    let result: any;
    updateArrayField(
      // so this gets call 3 times
      (array: any[]) => {
        const tmp = array;
        if (!result) {
          result = tmp && tmp.pop && tmp.pop();
        }
        return tmp;
      },
      true,
      true,
    );

    return result as T;
  };

  const handlePop = () => () => pop<any>();

  // added:
  const getAt: <T>(index: number) => T = index => {
    let result: any = get(formik.state.values, props.name)[index];
    return result as T;
  };

  const arrayHelpers: ArrayHelpers = {
    push: push,
    pop: pop,
    swap: swap_,
    move: move_,
    insert: insert_,
    replace: replace_,
    sort,
    unshift: unshift,
    remove: remove,
    handlePush: handlePush,
    handlePop: handlePop,
    handleSwap: handleSwap,
    handleMove: handleMove,
    handleInsert: handleInsert,
    handleReplace: handleReplace,
    handleSort,
    handleUnshift: handleUnshift,
    handleRemove: handleRemove,
  };
  const [p, customProps] = splitProps(props, ['component', 'render', 'children', 'name', 'formik', 'validateOnChange']);

  const { validate: _validate, validationSchema: _validationSchema, ...restOfFormik } = formik;

  return p.component ? (
    <Dynamic component={p.component} {...customProps} form={restOfFormik} {...arrayHelpers} name={p.name} />
  ) : p.children ? ( // children come last, always called
    typeof p.children === 'function' ? (
      (p.children as any)({ ...customProps, form: restOfFormik, ...arrayHelpers, name: p.name })
    ) : (
      p.children
    )
  ) : null;
};

// export const FieldArray = connect<FieldArrayConfig, any>(FieldArrayInner);
