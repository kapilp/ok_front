// https://github.com/jaredpalmer/formik-persist/pulls
import { FormikProps } from '../../UI/validators/formik/packages/formik/src/types';
import debounce from 'lodash.debounce';
import isEqual from 'react-fast-compare';

export interface PersistProps {
  name: string;
  debounce?: number;
  isSessionStorage?: boolean;
}

export const PersistImpl = (props: PersistProps & { formik: FormikProps<any> }) => {
  const saveForm = debounce((data: FormikProps<{}>) => {
    if (this.props.isSessionStorage) {
      window.sessionStorage.setItem(this.props.name, JSON.stringify(data));
    } else {
      window.localStorage.setItem(this.props.name, JSON.stringify(data));
    }
  }, this.props.debounce ?? 300);

  // componentDidUpdate(prevProps: PersistProps & { formik: FormikProps<any> }) {
  //   if (!isEqual(prevProps.formik, this.props.formik)) {
  //     this.saveForm(this.props.formik);
  //   }
  // }

  // componentDidMount() {
  //   const maybeState = this.props.isSessionStorage
  //     ? window.sessionStorage.getItem(this.props.name)
  //     : window.localStorage.getItem(this.props.name);
  //   if (maybeState && maybeState !== null) {
  //     this.props.formik.setFormikState(JSON.parse(maybeState));
  //   }
  // }

  return null;
};

// export const Persist = connect<PersistProps, any>(PersistImpl);
