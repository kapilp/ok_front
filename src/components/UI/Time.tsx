import { JSX, onCleanup, createSignal } from 'solid-js';

export const Time = () => {
  const formatter = new Intl.DateTimeFormat('en', {
    hour12: true,
    hour: 'numeric',
    minute: '2-digit',
    second: '2-digit',
  });
  const [getState, setState] = createSignal(formatter.format(new Date()));
  const timer = setInterval(() => {
    setState(formatter.format(new Date()));
  }, 1000);

  onCleanup(() => {
    clearInterval(timer);
  });

  return <h1>The time is {getState()}</h1>;
};
