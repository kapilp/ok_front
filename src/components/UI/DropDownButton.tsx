import './DropDownButton.css';
export const DropDownMenu = () => {
  const toogleDropDown = () => {
    const menuButton = document.getElementById('myDropdown');
    menuButton && menuButton.classList.toggle('show');
  };
  const closeDropDown = () => {
    const dropdown = document.getElementById('myDropdown');
    if (dropdown && dropdown.classList.contains('show')) {
      dropdown.classList.remove('show');
    }
  };
  const removeDropDownEventListener = () => window.removeEventListener('click', outSideMenuClickListner);
  const outSideMenuClickListner = (event: MouseEvent) => {
    if (event.target && !(event.target as HTMLElement).matches('.dropbtn')) {
      closeDropDown();
    }
    removeDropDownEventListener();
  };
  const onClick = () => {
    removeDropDownEventListener();
    toogleDropDown();
    window.setTimeout(() => window.addEventListener('click', outSideMenuClickListner), 0);
  };
  return (
    <div className="dropdown">
      <button onClick={onClick}>Dropdown</button>
      <div id="myDropdown" className="dropdown-content">
        <a href="#">Link 1</a>
        <a href="#">Link 2</a>
        <a href="#">Link 3</a>
      </div>
    </div>
  );
};
