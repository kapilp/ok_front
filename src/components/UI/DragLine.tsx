import { JSX, createSignal } from 'solid-js';
import { styled } from 'solid-styled-components';

export let dragLineGlobal = null as HTMLDivElement | null;
const Div = styled('div')`
  position: absolute;
  top: 0;
  left: -1000px;
  height: 100%;
  width: 1px;
  background: #ccc;
`;
export const DragLine = (props: {}) => {
  const setRef = (el: HTMLDivElement) => (dragLineGlobal = el);
  return <Div className="drag-line" ref={setRef} />;
};
