import { createSignal, useContext } from 'solid-js';
import { ProjectDocumentContext } from '../../layout/context';
import { Table } from '../table/Table';
import { Button } from './core/button/src/Button';
import { ChatFullScreen } from './Chat';
import { SvgIconComment } from './core/icon/src/icon/IconComment';
import { AppWidget, dock, Lumino, LuminoWidget } from '../Lumino';

export const ChatAdmin = (props: Record<string, unknown>) => {
  const [getProjectDocumentContext] = useContext(ProjectDocumentContext);
  const [openConversations, setOpenConversations] = createSignal([] as string[]);
  const [widgets, setWidgets] = createSignal<AppWidget[]>([]);
  const [luminoWidgets, setLuminoWidgets] = createSignal([] as LuminoWidget[]); // used to get last tab reference
  const [renderedWidgetIds, setRenderedWidgetIds] = createSignal<string[]>([]); // tracker of components that have been rendered with LuminoWidget already
  const [disposeFunctions, setDisposeFunctions] = createSignal<{ [key: string]: () => void }>({});

  setWidgets([
    {
      id: 'conversation-table',
      type: 'root',
      active: true,
      tabTitle: 'Conversations',
      component: () => <Table fetchConfig={{ project: getProjectDocumentContext()?._key }} modelComponent quickEditComponent schemaKey="conversation" tableAction={TableAction} />,
    },
  ]);
  const onChatOpenClick = (conversationKey: string) => () => {
    if (!openConversations().includes(conversationKey)) {
      setOpenConversations(prevState => [...prevState, conversationKey]);
      setWidgets(prevState =>
        prevState.concat({
          id: conversationKey,
          type: 'root',
          active: true,
          tabTitle: conversationKey,
          onClose: closeForm(conversationKey),
          component: () => (
            <div style="overflow: scroll;">
              <ChatFullScreen projectKey={getProjectDocumentContext()?._key} conversationKey={conversationKey} closeForm={closeForm(conversationKey)} />
            </div>
          ),
        }),
      );
    } else {
      const lwg = luminoWidgets().find(lw => lw.id === conversationKey);
      if (lwg) dock.activateWidget(lwg);
    }
  };

  const closeForm = (conversationKey: string) => () => {
    if (openConversations().includes(conversationKey)) {
      setOpenConversations(prevState => prevState.filter(c => c !== conversationKey));
      const lwgIdx = luminoWidgets().findIndex(lw => lw.id === conversationKey);
      if (lwgIdx > -1) {
        // luminoWidgets()[lwgIdx].close(); // same as closing tab event
        setLuminoWidgets(prevState => {
          prevState.splice(lwgIdx, 1);
          return prevState;
        });
        setWidgets(prevState => prevState.filter(widget => widget.id !== conversationKey));
        setRenderedWidgetIds(cur => cur.filter(c => c !== conversationKey));
      }
    }
  };
  const TableAction = (tableActionProps: { rowValue: { [key: string]: string | number | boolean } }) => {
    return <Button icon={SvgIconComment} isIconOnly text="Open Chat" name="open chat window" onClick={onChatOpenClick(tableActionProps.rowValue.state._key)} />;
  };
  return (
    <div>
      <Lumino
        widgets={widgets}
        setWidgets={setWidgets}
        luminoWidgets={luminoWidgets}
        setLuminoWidgets={setLuminoWidgets}
        renderedWidgetIds={renderedWidgetIds}
        setRenderedWidgetIds={setRenderedWidgetIds}
        disposeFunctions={disposeFunctions}
        setDisposeFunctions={setDisposeFunctions}
      />
    </div>
  );
};
