import { Ws } from '../../utils/ws_events_dispatcher';
import { ET } from '../../utils/enums';
import { Redirect } from '@rturnq/solid-router';
import { createComputed, createSignal } from "solid-js";

export const NavigateWsBind = () => {
  const [path, setPath] = createSignal( '');
  const [renderRedirect, setRenderRedirect] = createSignal(false);

  // must use id =0
  Ws.bind$(
    [ET[ET.get], 'redirection_event', 0],
    function (data) {
      if (data[1]) {
        // GlobalMessageStore.set(data[2]); // fix if needed
        setTimeout(function () {
          setPath(data[0]);
        }, data[1]);
      } else {
        setPath(data[0]);
      }
    },
    1,
  );

  createComputed(()=>{
    if(path() !== '') setTimeout(()=>setPath(''))
    setRenderRedirect(path() !== '')
  })

  // Redirect only works once
  return <>{renderRedirect() && <Redirect href={path()} />}</>;
};
