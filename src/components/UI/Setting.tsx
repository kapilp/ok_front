import {  JSX, For, batch, onCleanup, Show  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ET, FieldType, makeTableArgs, makeTableOneDeleteArgs, Success, TableResult, TableResultWithSchema } from '../../utils/enums';
import { Ws } from '../../utils/ws_events_dispatcher';
import { FormServer } from '../formServer/FormServer';

import { doLocalSorting, removeRowsFromItems, updateModifiedRow } from '../table/logic/process_data';
import { Card } from './core/card/src/Card';
import { Button } from './core/button/src/Button';
import { SvgIconSave } from './core/icon/src/icon/IconSave';
import { SvgIconTrash } from './core/icon/src/icon/IconTrash';
import { DialogModal } from './framework/dialog-modal/src/DialogModal';
import { ActionHeader } from './core/action-header/src/ActionHeader';
import { ActionFooter } from './core/action-footer/src/ActionFooter';
import { Cell } from './core/table/src/subcomponents/_Cell';

// TODO: settings are public or private. public settings can be viewed by children members
export const Setting = (props: { schemaKey: string; getNewSaveSettings: () => {}; applyNewSetting: (v: {}) => void }) => {
  let selectElement: HTMLSelectElement | null = null;
  const uid = Ws.uid;
  const data_evt: string[] = [ET[ET.subscribe], 'preferences_list', uid];
  const unsub_evt: string[] = [ET[ET.unsubscribe], 'preferences_list', uid];

  const [state, setState] = createStore({ items: [] as TableResult, isLoading: true, authorized: false, showModal: false });
  onCleanup(Ws.bind$(data_evt, onDataGet, 1));
  onCleanup(() => {
    unsub_evt.length && Ws.trigger([[unsub_evt, {}]]);
  });
  function onDataGet(d: TableResultWithSchema) {
    batch(() => {
      if (state.isLoading) setState({ isLoading: false });
      if (d.error) {
        setState({ authorized: false });
        if (typeof d.error == 'string') setState({ er: d.error });
        return;
      }
      setState({ authorized: true });

      if (d.r) {
        setState({ items: d.r.result ?? [] });
        //sortHelper();
      }
      if (d.n)
        setState('items', (l: TableResult) => {
          l.push(...d.n.result);
        });

      if (d.m) updateModifiedRow(d.m.result, state.items, setState);

      if (d.d) removeRowsFromItems(d.d.result, state.items, setState);

      if (d.n || d.m) {
        const sortSetting = {};
        const columns: string[] = [];
        doLocalSorting(sortSetting, columns, setState);
      }
    });
  }
  const refresh = () => {
    const event1 = [data_evt, makeTableArgs().setFilter({ id1: 'table', id2: props.schemaKey }).setSort({ _key: 1 }).setHeader(false)];
    Ws.trigger([event1]);
  };
  refresh();

  const onChangeSingle = (e: Event & { currentTarget: HTMLSelectElement; target: HTMLSelectElement }) => {
    props.applyNewSetting(state.items[e.target.selectedIndex]['value']);
  };

  const value = [
    {
      s: {
        _key: { label: 'Key', type: FieldType.hidden, isRequired: false, isDisabled: false, description: '', props: {} },
        id1: { label: 'Id1', type: FieldType.hidden, isRequired: false, isDisabled: false, description: '', props: {} },
        id2: { label: 'Id2', type: FieldType.hidden, isRequired: false, isDisabled: false, description: '', props: {} },
        profile: { label: 'Profile', type: 20, isRequired: false, isDisabled: false, description: '', props: {} },
        value: { label: 'Value', type: FieldType.hidden, isRequired: false, isDisabled: false, description: '', props: {} },
      },
      p: {},
    },
    { r: { result: [{ _key: '', id1: 'table', id2: props.schemaKey, profile: '', value: {} }] } },
  ];
  const handleOpenModal = () => {
    const newSettings = props.getNewSaveSettings(); // todo add groupBy,
    if (value[1].r?.result[0].value) value[1].r.result[0].value = newSettings;
    setState({ showModal: true });
  };

  const handleCloseModal = () => setState({ showModal: false });
  const handleDelete = async () => {
    if (selectElement) {
      const mutate_evt = [ET[ET.delete_], 'preferences_mutate', Ws.uid];
      const key = state.items[selectElement.selectedIndex]['_key'] as string;
      if (!key) return;
      const r = confirm('Are You Sure?');
      if (r) {
        const d: [Success, string] = await new Promise((resolve, reject) => {
          const args = makeTableOneDeleteArgs().setKeyRev(key, '');
          Ws.bindT(
            mutate_evt,
            d => {
              resolve(d);
            },
            args,
          );
        });
        if (!d.error) {
          /*const delete_msg = view(lensPath(['msg', 'delete']), $translation);
                      addNotification({
                        text: delete_msg,
                        position: 'bottom-right',
                        type: 'danger',
                        removeAfter: 4000
                      });*/
          // removeRows([key]);
        } else {
          alert(d.error);
        }
      }
    }
  };

  return (
    <>
      <Show when={state.items.length}>
        <select ref={selectElement} onChange={onChangeSingle}>
          <For each={state.items}>{(r, i) => <option value={r['_key'] as string}> {r['profile']}</option>}</For>
        </select>
      </Show>
      <Button onClick={handleOpenModal} isIconOnly icon={SvgIconSave} text="Save Profile" />

      {state.items.length && <Button onClick={handleDelete} isIconOnly icon={SvgIconTrash} text="Delete Profile" />}

      <DialogModal
        ariaLabel="Delete Confirmation Dialog Modal"
        isOpen={state.showModal}
        onRequestClose={handleCloseModal}
        header={<ActionHeader title="Save Profile" onClose={handleCloseModal} />}
        footer={''}
      >
        <Card>
          <h2 style="text-align: center">Save Profile</h2>
          <FormServer key="" schemaKey={'preferences'} fetchConfig={{}} buttonlabels={{ save: 'Save' }} handleSubmit={handleCloseModal} onCancel={handleCloseModal} value={value} />
        </Card>
      </DialogModal>
    </>
  );
};
