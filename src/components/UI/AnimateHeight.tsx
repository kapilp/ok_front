// TODO fix infinite loop on css class names
import cx from 'classnames';
import {  JSX, createEffect, onCleanup, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
const ANIMATION_STATE_CLASSES = {
  animating: 'rah-animating',
  animatingUp: 'rah-animating--up',
  animatingDown: 'rah-animating--down',
  animatingToHeightZero: 'rah-animating--to-height-zero',
  animatingToHeightAuto: 'rah-animating--to-height-auto',
  animatingToHeightSpecific: 'rah-animating--to-height-specific',
  static: 'rah-static',
  staticHeightZero: 'rah-static--height-zero',
  staticHeightAuto: 'rah-static--height-auto',
  staticHeightSpecific: 'rah-static--height-specific',
};
const PROPS_TO_OMIT = [
  'animateOpacity',
  'animationStateClasses',
  'applyInlineTransitions',
  'children',
  'contentClassName',
  'delay',
  'duration',
  'easing',
  'height',
  'onAnimationEnd',
  'onAnimationStart',
];

// Start animation helper using nested requestAnimationFrames
function startAnimationHelper(callback: () => void) {
  const requestAnimationFrameIDs = [];
  requestAnimationFrameIDs[0] = requestAnimationFrame(() => {
    requestAnimationFrameIDs[1] = requestAnimationFrame(() => {
      callback();
    });
  });
  return requestAnimationFrameIDs;
}
function cancelAnimationFrames(requestAnimationFrameIDs: number[]) {
  requestAnimationFrameIDs.forEach(id => cancelAnimationFrame(id));
}
function isNumber(n: number | string) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function isPercentage(height: number | string) {
  // Percentage height
  return typeof height === 'string' && height.search('%') === height.length - 1 && isNumber(height.substr(0, height.length - 1));
}
function runCallback(callback: (p: {}) => void, params: {}) {
  if (callback && typeof callback === 'function') {
    callback(params);
  }
}

type OmitTypeProps<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

type DivWithNoAnimationCallbacks = OmitTypeProps<OmitTypeProps<JSX.HTMLAttributes<HTMLDivElement>, 'onAnimationStart'>, 'onAnimationEnd'>;

export type AnimateHeightProps = DivWithNoAnimationCallbacks & {
  animateOpacity?: boolean;
  animationStateClasses?: AnimationStateClasses;
  applyInlineTransitions?: boolean;
  children: JSX.Element | JSX.Element[];
  className?: string;
  contentClassName?: string;
  delay?: number;
  duration?: number;
  easing?: 'ease' | 'linear' | 'ease-in' | 'ease-out' | 'ease-in-out' | string;
  height: string | number;
  onAnimationEnd?(props: { newHeight: number }): void;
  onAnimationStart?(props: { newHeight: number }): void;
  style?: JSX.CSSProperties;
};
export type AnimationStateClasses = {
  animating?: string;
  animatingUp?: string;
  animatingDown?: string;
  animatingToHeightZero?: string;
  animatingToHeightAuto?: string;
  animatingToHeightSpecific?: string;
  static?: string;
  staticHeightZero?: string;
  staticHeightAuto?: string;
  staticHeightSpecific?: string;
};
export const AnimateHeight = (props: AnimateHeightProps) => {
  let contentElement: HTMLDivElement;
  let timeoutID: number | undefined;
  props = mergeProps(
    {},
    {
      animateOpacity: false,
      animationStateClasses: ANIMATION_STATE_CLASSES,
      applyInlineTransitions: true,
      duration: 250,
      delay: 0,
      easing: 'ease',
      style: {},
    },
    props,
  );

  let animationFrameIDs = [] as number[];

  let height = 'auto' as number | string;
  let overflow = 'visible';
  if (isNumber(props.height)) {
    // If value is string "0" make sure we convert it to number 0
    height = props.height < 0 || props.height === '0' ? 0 : props.height;
    overflow = 'hidden';
  } else if (isPercentage(props.height)) {
    // If value is string "0%" make sure we convert it to number 0
    height = props.height === '0%' ? 0 : props.height;
    overflow = 'hidden';
  }
  let animationStateClasses = {
    ...ANIMATION_STATE_CLASSES,
    ...props.animationStateClasses,
  };
  let animationClassesTimeoutID: number | undefined;

  const [state, setState] = createStore({
    animationStateClasses: getStaticStateClasses(height),
    height,
    overflow,
    shouldUseTransitions: false,
  });

  function getStaticStateClasses(height: number | string) {
    return cx({
      [animationStateClasses.static]: true,
      [animationStateClasses.staticHeightZero]: height === 0,
      [animationStateClasses.staticHeightSpecific]: height > 0,
      [animationStateClasses.staticHeightAuto]: height === 'auto',
    });
  }

  function setRef(el: HTMLDivElement) {
    contentElement = el;
    // Hide content if height is 0 (to prevent tabbing into it)
    // Check for contentElement is added cause this would fail in tests (react-test-renderer)
    // Read more here: https://github.com/Stanko/react-animate-height/issues/17
    if (contentElement && contentElement.style) {
      hideContent(state.height);
    }
  }
  createEffect(() => {
    // Remove display: none from the content div
    // if it was hidden to prevent tabbing into it
    if (!contentElement) return;
    showContent(state.height);
    // Cache content height
    contentElement.style.overflow = 'hidden';
    const contentHeight = contentElement.offsetHeight;
    contentElement.style.overflow = '';
    // set total animation time
    const totalDuration = props.duration + props.delay;
    let newHeight = null;
    const timeoutState = {
      height: null,
      overflow: 'hidden',
    };
    const isCurrentHeightAuto = state.height === 'auto';
    if (isNumber(props.height)) {
      // If value is string "0" make sure we convert it to number 0
      newHeight = props.height < 0 || props.height === '0' ? 0 : props.height;
      timeoutState.height = newHeight;
    } else if (isPercentage(props.height)) {
      // If value is string "0%" make sure we convert it to number 0
      newHeight = props.height === '0%' ? 0 : props.height;
      timeoutState.height = newHeight;
    } else {
      // If not, animate to content height
      // and then reset to auto
      newHeight = contentHeight; // TODO solve contentHeight = 0
      timeoutState.height = 'auto';
      timeoutState.overflow = null;
    }
    if (isCurrentHeightAuto) {
      // This is the height to be animated to
      timeoutState.height = newHeight;
      // If previous height was 'auto'
      // set starting height explicitly to be able to use transition
      newHeight = contentHeight;
    }
    // Animation classes
    const animationStateClasses_ = cx({
      [animationStateClasses.animating]: true,
      [animationStateClasses.animatingUp]: props.height === 'auto' || props.height < props.height,
      [animationStateClasses.animatingDown]: props.height === 'auto' || props.height > props.height,
      [animationStateClasses.animatingToHeightZero]: timeoutState.height === 0,
      [animationStateClasses.animatingToHeightAuto]: timeoutState.height === 'auto',
      [animationStateClasses.animatingToHeightSpecific]: timeoutState.height > 0,
    });
    // Animation classes to be put after animation is complete
    const timeoutAnimationStateClasses = getStaticStateClasses(timeoutState.height);
    // Set starting height and animating classes
    // We are safe to call set state as it will not trigger infinite loop
    // because of the "height !== props.height" check
    setState({
      // eslint-disable-line react/no-did-update-set-state
      animationStateClasses: animationStateClasses_,
      height: newHeight,
      overflow: 'hidden',
      // When animating from 'auto' we first need to set fixed height
      // that change should be animated
      shouldUseTransitions: !isCurrentHeightAuto,
    });
    // Clear timeouts
    clearTimeout(timeoutID);
    clearTimeout(animationClassesTimeoutID);
    if (isCurrentHeightAuto) {
      // When animating from 'auto' we use a short timeout to start animation
      // after setting fixed height above
      timeoutState.shouldUseTransitions = true;
      cancelAnimationFrames(animationFrameIDs);
      animationFrameIDs = startAnimationHelper(() => {
        setState(timeoutState);
        // ANIMATION STARTS, run a callback if it exists
        runCallback(props.onAnimationStart, { newHeight: timeoutState.height });
      });
      // Set static classes and remove transitions when animation ends
      animationClassesTimeoutID = setTimeout(() => {
        setState({
          animationStateClasses: timeoutAnimationStateClasses,
          shouldUseTransitions: false,
        });
        // ANIMATION ENDS
        // Hide content if height is 0 (to prevent tabbing into it)
        hideContent(timeoutState.height);
        // Run a callback if it exists
        runCallback(props.onAnimationEnd, { newHeight: timeoutState.height });
      }, totalDuration);
    } else {
      // ANIMATION STARTS, run a callback if it exists
      runCallback(props.onAnimationStart, { newHeight });
      // Set end height, classes and remove transitions when animation is complete
      timeoutID = setTimeout(() => {
        timeoutState.animationStateClasses = timeoutAnimationStateClasses;
        timeoutState.shouldUseTransitions = false;
        setState(timeoutState);
        // ANIMATION ENDS
        // If height is auto, don't hide the content
        // (case when element is empty, therefore height is 0)
        if (props.height !== 'auto') {
          // Hide content if height is 0 (to prevent tabbing into it)
          hideContent(newHeight); // TODO solve newHeight = 0
        }
        // Run a callback if it exists
        runCallback(props.onAnimationEnd, { newHeight });
      }, totalDuration);
    }
  });
  onCleanup(() => {
    cancelAnimationFrames(animationFrameIDs);
    window.clearTimeout(timeoutID);
    window.clearTimeout(animationClassesTimeoutID);
    timeoutID = undefined;
    animationClassesTimeoutID = undefined;
    animationStateClasses = null;
  });
  function showContent(height: number | string) {
    if (height === 0) {
      contentElement.style.display = '';
    }
  }
  function hideContent(newHeight: number | string) {
    if (newHeight === 0) {
      contentElement.style.display = 'none';
    }
  }

  const [sty, setSty] = createStore({
    componentStyle: {
      height: state.height,
      overflow: state.overflow || props.style.overflow,
    },
    contentStyle: {},
  });

  createEffect(() => {
    if (state.shouldUseTransitions && props.applyInlineTransitions) {
      setSty('componentStyle', 'transition', `height ${props.duration}ms ${props.easing} ${props.delay}ms`);
      // Include transition passed through styles
      if (props.style.transition) {
        setSty('componentStyle', 'transition', `${props.style.transition}, ${sty.componentStyle.transition}`);
      }
    }
  });

  createEffect(() => {
    if (props.animateOpacity) {
      setSty('contentStyle', 'transition', `opacity ${props.duration}ms ${props.easing} ${props.delay}ms`);
      if (state.height === 0) {
        setSty('contentStyle', 'opacity', 0);
      }
    }
  });
  const [extracted, customProps] = splitProps(props, PROPS_TO_OMIT);
  return (
    <div
      {...customProps}
      aria-hidden={state.height === 0}
      className={cx({
        [state.animationStateClasses]: true,
        [props.className]: props.className,
      })}
      style={{ ...props.style, ...sty.componentStyle }}
    >
      <div className={props.contentClassName} style={sty.contentStyle} ref={setRef}>
        {props.children}
      </div>
    </div>
  );
};
/*const heightPropType = (props, propName, componentName) => {
  const value = props[propName];
  if ((typeof value === 'number' && value >= 0) || isPercentage(value) || value === 'auto') {
    return null;
  }
  return new TypeError(
    `value "${value}" of type "${typeof value}" is invalid type for ${propName} in ${componentName}. ` +
      'It needs to be a positive number, string "auto" or percentage string (e.g. "15%").',
  );
};*/
