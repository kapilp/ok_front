import { JSX, For, createSignal, onCleanup, Show, Switch, Match, createComputed } from "solid-js";
import { createStore, Store } from "solid-js/store";
import classNamesBind from "classnames/bind";
import { SingeResult, TableResult } from "../../utils/enums";
import { Table } from "../table/Table";
import { Button } from "./core/button/src/Button";
import { Card } from "./core/card/src/Card";
import styles from "./Chat.module.scss";
import { ActionHeader } from "./core/action-header/src/ActionHeader";
// import { SvgIconEnvelope } from './core/icon/src/icon/IconEnvelope';
import { SvgIconChatBubble } from "./terra-consumer/packages/terra-consumer-icon/src/icon/IconChatBubble";
import { getIsLoggedIn, getMember, Ws } from "../../utils/ws_events_dispatcher";
import { FormServer } from "../formServer/FormServer";

const cx = classNamesBind.bind(styles);

interface ChatMessageParams {
  rowValue: { [key: string]: string | number | boolean };
  rowIndex: () => number;
  key: string;
  currentParticipantKey: string;
}

interface ChatFullScreenParams {
  projectKey: string;
  closeForm?: () => void;
  conversationKey?: string;
}
// props.conversationKey means admin
export const ChatFullScreen = function (props: ChatFullScreenParams): JSX.Element {
  const [conversation, setConversation] = createSignal(props.conversationKey ? props.conversationKey : localStorage.getItem(`conversation${props.projectKey || ""}`));
  const [currentParticipantKey, setCurrentParticipantKey] = createSignal();
  const [isChatEnd, setIsChatEnd] = createSignal(false);
  const [isEditMyDetails, setIsEditMyDetails] = createSignal(false);
  const [participantList, setParticipantList] = createSignal([] as TableResult);
  const [participantListByKey, setParticipantListByKey] = createSignal({} as { [key: string]: SingeResult });
  const [isParticipantsOpen, setIsParticipantsOpen] = createSignal(false);
  const [typingParticipants, setTypingParticipants] = createSignal([] as string[]);
  // store the timeout, cancel it on each change, save a new one
  const typingTimeout = {} as { [key: string]: ReturnType<typeof window.setTimeout> };
  const [aliasList, setAliasList] = createSignal([] as TableResult);
  const [isParticipant, setIsParticipant] = createSignal(false);
  const [selectedAlias, setSelectedAlias] = createSignal("");

  const [fetchConfig, setFetchConfig] = createSignal({});

  createComputed(() => {
    setFetchConfig({ project: props.projectKey, conversation: props.conversationKey || conversation(), ...(props.conversationKey ? { isAdmin: true } : {}) });
  });
  const { uid } = Ws;
  const conversationSubscribeEvent = ["subscribe", "conversation", uid];
  const conversationUnsubscribeEvent = ["unsubscribe", "conversation", uid];
  const conversationEndEvent = ["update", "conversation_end", uid];
  const aliasListEvent = ["subscribe", "alias_list", Ws.uid];
  const joinParticipantEvent = ["insert", "join_participant", uid];

  onCleanup(() => Ws.trigger([[conversationUnsubscribeEvent, fetchConfig()]]));

  onCleanup(
    Ws.bindT(
      conversationSubscribeEvent,
      (d) => {
        if (d.error) {
          console.log("error occurred: ", d.description);
        } else if (d.conversation) {
          if (props.conversationKey) {
            console.error("this should never happen");
          } else {
            setConversation(d.conversation);
            localStorage.setItem(`conversation${props.projectKey || ""}`, d.conversation);
          }
        } else if (d.participant === false) {
          setIsParticipant(false);
          setSelectedAlias(`member/${getMember()._key}`);
          if (props.conversationKey) Ws.trigger([[aliasListEvent, fetchConfig()]]); // only projectKey is required in the arguments
          // show join button
        } else if (d.participant) {
          setIsParticipant(true);
          // participant is just used to find name, backend set participant automatically
          setCurrentParticipantKey(d.participant);
          setSelectedAlias(`participant/${d.participant}`);
        } else if (d.typing) {
          if (!typingParticipants().includes(d._key)) setTypingParticipants((prevState) => [...prevState, d._key]);
          if (typingTimeout[d._key]) clearTimeout(typingTimeout[d._key]);
          typingTimeout[d._key] = setTimeout(() => {
            if (typingParticipants().includes(d._key)) setTypingParticipants((prevState) => prevState.filter((k) => k !== d._key));
          }, 500);
        }
      },
      fetchConfig(),
      1
    )
  );
  onCleanup(
    Ws.bind$(
      ["subscribe", "conversation_list", uid],
      (d) => {
        if (d.error) {
          // Todo
        }
      },
      1
    )
  );
  onCleanup(
    Ws.bind$(
      ["subscribe", "participant_list", uid],
      (d) => {
        if (d.error) {
          // Todo
        } else if (d.r) {
          setParticipantList(d.r.result ?? []);
        } else if (d.n) {
          setParticipantList((prevState) => [...prevState, ...d.n.result]);
        } else if (d.m) {
          setParticipantList((prevState) => {
            const idx = prevState.findIndex((p) => p._key === d.m.result[0]._key);
            prevState[idx] = d.m.result[0];
            return [...prevState];
          });
        } else if (d.d) {
          setParticipantList((prevState) => {
            d.d.result.each((key) => {
              const idx = prevState.findIndex((p) => p._key === key);
              prevState.splice(idx, 1);
            });
            return prevState;
          });
        }
      },
      1
    )
  );
  onCleanup(
    Ws.bind$(
      conversationUnsubscribeEvent,
      (d) => {
        // Todo
      },
      1
    )
  );
  onCleanup(
    Ws.bind$(
      conversationEndEvent,
      (d) => {
        if (!d.error) {
          if (!props.conversationKey) {
            localStorage.removeItem(`conversation${props.projectKey || ""}`);
            setConversation("");
          } else {
            props.closeForm();
          }
          setIsChatEnd(true);
        }
      },
      1
    )
  );
  onCleanup(
    Ws.bind$(
      aliasListEvent,
      (d) => {
        if (d.error) {
          // Todo
        } else if (d.r) {
          setAliasList(d.r.result ?? []);
        } else if (d.n) {
          setAliasList((prevState) => [...prevState, ...d.n.result]);
        } else if (d.m) {
          setAliasList((prevState) => {
            const idx = prevState.findIndex((p) => p._key === d.m.result[0]._key);
            prevState[idx] = d.m.result[0];
            return [...prevState];
          });
        } else if (d.d) {
          setAliasList((prevState) => {
            d.d.result.each((key) => {
              const idx = prevState.findIndex((p) => p._key === key);
              prevState.splice(idx, 1);
            });
            return prevState;
          });
        }
      },
      1
    )
  );
  createComputed(() => {
    setParticipantListByKey(
      participantList().reduce((prev, curr) => {
        prev[curr._key as string] = curr;
        return prev;
      }, {} as { [key: string]: SingeResult })
    );
  });

  const onEndChat = () => {
    Ws.trigger([[conversationEndEvent, fetchConfig()]]);
  };
  const onStartChat = () => {
    setIsChatEnd(false);
    Ws.trigger([[conversationSubscribeEvent, fetchConfig()]]);
  };
  const onEditMyDetails = () => {
    setIsEditMyDetails(true);
  };
  const onJoin = () => {
    Ws.trigger([[joinParticipantEvent, { ...fetchConfig(), alias: selectedAlias() }]]);
  };
  const onSelectAlias = (e: JSX.EventHandlerUnion<HTMLSelectElement, Event>) => {
    setSelectedAlias(e.target.value);
  };
  const getParticipantName = (key) => participantListByKey()[key]?.name;
  // https://www.w3schools.com/howto/howto_js_popup_chat.asp
  const ChatMessage = function (chatMessageProps: ChatMessageParams) {
    return (
      <>
        {chatMessageProps.rowValue.state.from === currentParticipantKey() ? (
          <div className={cx("rowContainer", "darker")}>
            <span>{getParticipantName(chatMessageProps.rowValue.state.from)} (You) </span>
            <span className={cx("right")}>{new Date(chatMessageProps.rowValue.state.time).toLocaleString()}: </span>
            <span>{chatMessageProps.rowValue.state.message}</span>
            {chatMessageProps.rowValue.state.media && chatMessageProps.rowValue.state.media.map((key) => <img src={`/api/download/1/${key}`} />)}
          </div>
        ) : (
          <div className={cx("rowContainer")}>
            <span>{getParticipantName(chatMessageProps.rowValue.state.from)}</span>
            <span className={cx("right")}>{new Date(chatMessageProps.rowValue.state.time).toLocaleString()}: </span>
            <span>{chatMessageProps.rowValue.state.message}</span>
            {chatMessageProps.rowValue.state.media && chatMessageProps.rowValue.state.media.map((key) => <img src={`/api/download/1/${key}`} />)}
          </div>
        )}
      </>
    );
  };
  const ChatMessages = function (chatMessagesProps: { rows: Store<TableResult> }) {
    return (
      <Card style="height: 300px; overflow-y: scroll;">
        <For each={chatMessagesProps.rows}>{(r, i) => <ChatMessage rowValue={r} rowIndex={i} key={r._key as string} />}</For>
      </Card>
    );
  };
  /* if (!localUUID) {
    fetchUUID().then(d => {
      setState({ UUID: d.uuid });
      localStorage.setItem('chatUUID', d.uuid);
    });
  } else {
    setState({ UUID: localUUID });
  } */
  // <label htmlFor="msg"><b>Message</b></label>
  // <textarea placeholder="Type message.." name="msg" required />
  // <button type="submit" className="btn"> Send </button>
  // {"_key":"","from":"","to":"admin","message":""}
  return (
    <>
      <ActionHeader title="Support">
        <Show when={(props.conversationKey || !!conversation()) && !isChatEnd()} fallback={<div />}>
          <Show when={!isEditMyDetails()}>
            <Button style="white-space: nowrap;" text="Edit My Details" onClick={onEditMyDetails} />
          </Show>
          <Button
            style="white-space: nowrap;"
            text={`${isParticipantsOpen() ? "Hide Participants" : "Participants"}`}
            onClick={() => setIsParticipantsOpen((prevState) => !prevState)}
          />
          <Button style="white-space: nowrap;" text="End Chat" onClick={onEndChat} />
        </Show>
      </ActionHeader>

      <Show when={isEditMyDetails()}>
        <FormServer
          schemaKey="participant"
          key={currentParticipantKey()}
          fetchConfig={fetchConfig()}
          buttonLabels={{}}
          onCancel={() => setIsEditMyDetails(false)}
          handleSubmit={() => setIsEditMyDetails(false)}
        />
      </Show>

      <Show when={isParticipantsOpen()}>
        <Table
          uid={uid}
          useLocalState
          sendFormSchemaRequest={props.conversationKey || !!conversation()}
          fetchConfig={{ ...fetchConfig(), header: true }}
          modelComponent
          quickEditComponent
          schemaKey="participant"
          deleteAction={false}
          editAction={false}
        />
      </Show>

      <Switch fallback={<div>Not Found</div>}>
        <Match when={!isChatEnd()}>
          <Table
            uid={uid}
            dontSendSubscribe
            useLocalState
            showForm={isParticipant()}
            sendFormSchemaRequest={props.conversationKey || !!conversation()}
            fetchConfig={fetchConfig()}
            modelComponent
            quickEditComponent
            schemaKey="message"
            tableBody={ChatMessages}
          />
          <Show when={props.conversationKey && !isParticipant()}>
            <select onChange={onSelectAlias}>
              <Show when={props.conversationKey && getIsLoggedIn}>
                <optgroup label="Member">
                  <option value={`member/${getMember()._key}`}>
                    {getMember().firstName} {getMember().lastName}
                  </option>
                </optgroup>
              </Show>
              <Show when={aliasList().length > 0}>
                <optgroup label="Alias">
                  <For each={aliasList()}>{(alias, index) => <option value={`alias/${alias._key as string}`}>{alias.name}</option>}</For>
                </optgroup>
              </Show>
              <Show when={participantList().length > 0}>
                <optgroup label="Participant">
                  <For each={participantList()}>{(participant, index) => <option value={`participant/${participant._key as string}`}>{participant.name}</option>}</For>
                </optgroup>
              </Show>
            </select>

            <Button style="white-space: nowrap;" text="Join" onClick={onJoin} />
          </Show>
          <For each={typingParticipants()}>
            {(participantKey, index) => (
              <span>
                {index() > 0 ? "," : ""}
                {getParticipantName(participantKey)}
              </span>
            )}
          </For>
          <Show when={typingParticipants().length}> typing</Show>
        </Match>
        <Match when={isChatEnd()}>
          Chat is Ended
          <br />
          <Button style="white-space: nowrap;" text="Start Chat" onClick={onStartChat} />
        </Match>
      </Switch>

      {props.closeForm && <Button className="btn cancel" onClick={props.closeForm} text="Close" />}
    </>
  );
};
export const Chat = function (): JSX.Element {
  const [state, setState] = createStore({ show: false });
  const openForm = () => {
    setState({ show: true });
  };

  const closeForm = () => {
    setState({ show: false });
  };

  return (
    <>
      <Button
        style={`display: ${state.show ? "none" : "block"}`}
        className={cx(["openButton"])}
        onClick={openForm}
        icon={() => <SvgIconChatBubble width="2em" height="2em" />}
        isIconOnly
        text="Contact"
      />

      <div className={cx(["popup"])} style={`display: ${state.show ? "block" : "none"}; overflow: scroll; max-height: 100vh;`}>
        <div className={cx(["container"])}>
          <ChatFullScreen closeForm={closeForm} projectKey="17123813" />
        </div>
      </div>
    </>
  );
};
