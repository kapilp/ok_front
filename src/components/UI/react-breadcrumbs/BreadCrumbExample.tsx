import {NavLink} from '@rturnq/solid-router';
import { splitProps, Suspense, Switch } from "solid-js";
import { Dynamic } from "solid-js/web";
import { MatchRoute } from '@rturnq/solid-router';
import { Breadcrumbs } from './src/breadcrumbs';
import {Router} from '@rturnq/solid-router';
import {pathIntegration} from '../../solid-router/integration';
import { Breadcrumb } from './src/breadcrumb';


export const CrumbRoute = (props) => {
  // includeSearch = false, render,
  const [p, customProps] = splitProps(props, ['component', 'includeSearch']
  return (
    <MatchRoute {...customProps}>
      <Breadcrumb
        data={{
          title: props.title,
          path: props.path,
          //search: p.includeSearch ? routeProps.location.search : null,
        }}
      >
        {p.component ? <Dynamic component={p.component} {...customProps} /> : customProps.children}
      </Breadcrumb></MatchRoute>
  );
}


const Friend = props => (
  <div className="friend">
    <h3>{props.name}</h3>
    <p>More information about {props.name}...</p>
  </div>
);
// Create and export the component
const Friends = () => (
  <div className="friends">
    <h2>Your Friends</h2>
    <p>Here are your friends...</p>
    <ul>
      <li>
        <NavLink href={`alice`}>Alice</NavLink>
      </li>
      <li>
        <NavLink href={`frank`}>Frank</NavLink>
      </li>
      <li>
        <NavLink href={`jane`}>Jane</NavLink>
      </li>
      <li>
        <NavLink href={`matt`}>Matt</NavLink>
      </li>
    </ul>

    <Switch fallback={'no matched freiend'}>
      <CrumbRoute title="Alice" path={`alice`}>
        <Friend name="Alice" />
      </CrumbRoute>
      <CrumbRoute title="Frank" path={`frank`}>
        <Friend name="Frank" />
      </CrumbRoute>
      <CrumbRoute title="Jane" path={`jane`}>
        <Friend name="Jane" />
      </CrumbRoute>
      <CrumbRoute title="Matt" path={`matt`}>
        <Friend name="Matt" />
      </CrumbRoute>
    </Switch>
  </div>
);
const Event = props => (
  <div className="event">
    <h3>{props.name}</h3>
    <p>More information about the {props.name}...</p>
  </div>
);
// Create and export the component
const Locations = () => (
  <div className="friends">
    <h2>Upcoming Events</h2>
    <p>These events are coming up soon...</p>
    <ul>
      <li>
        <NavLink href={`dance`}>Dance</NavLink>
      </li>
      <li>
        <NavLink href={`cookout`}>Cookout</NavLink>
      </li>
    </ul>

    <Switch fallback={'no matched location'}>
      <CrumbRoute title="Dance" path={`dance`}>
        <Event name="Dance" />
      </CrumbRoute>
      <CrumbRoute title="Cookout" path={`cookout`}>
        <Event name="Cookout" />
      </CrumbRoute>
    </Switch>
  </div>
);
const Location = props => (
  <div className="location">
    <h3>{props.name}</h3>
    <p>More information about {props.name}...</p>
  </div>
);
// Create and export the component
const Events = () => (
  <div className="friends">
    <h2>Locations</h2>
    <p>Some locations...</p>
    <ul>
      <li>
        <NavLink href={`mexico`}>Mexico</NavLink>
      </li>
      <li>
        <NavLink href={`china`}>China</NavLink>
      </li>
    </ul>

    <Switch fallback={'no matched event'}>
      <CrumbRoute title="Mexico" path={`mexico`}>
        <Location name="Mexico" />
      </CrumbRoute>
      <CrumbRoute title="China" path={`china`}>
        <Location name="China" />
      </CrumbRoute>
    </Switch>
  </div>
);

export const BreadCrumbExample1 = () => {
  return (
    <>
      <Router integration={pathIntegration()}>
        <div className="demo">
          <Breadcrumbs className="demo__crumbs" />
          <main className="demo__main">
            <h1>Breadcrumbs Demo</h1>
            <p>Use the links below to jump around the site and watch the breadcrumbs update...</p>
            <ul className="demo__links">
              <li>
                <NavLink href="/friends">Friends</NavLink>
              </li>
              <li>
                <NavLink href="/events">Events</NavLink>
              </li>
              <li>
                <NavLink href="/locations">Locations</NavLink>
              </li>
            </ul>

            <div className="demo__content">
              <Switch>
                <MatchRoute path="/" end>
                  <span>Home content...</span>
                </MatchRoute>
                <CrumbRoute title='Friends' path="/friends">
                  <Friends />
                </CrumbRoute>
                <CrumbRoute title="Events" path="/events">
                  <Events />
                </CrumbRoute>
                <CrumbRoute title="Locations" path="/locations">
                  <Locations />
                </CrumbRoute>
                <MatchRoute title="404 Not Found">
                  <span>Page not found...</span>
                </MatchRoute>
              </Switch>
            </div>
          </main>
        </div>
      </Router>
    </>
  );
};
