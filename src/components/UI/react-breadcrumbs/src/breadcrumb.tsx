import UUID from 'uuid';
import isEqual from 'lodash/isEqual';

import {  JSX, mergeProps, createEffect, onCleanup, untrack  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { rootStore } from '../../../../stores/rootStore';
interface IBreadcrumbProps extends JSX.HTMLAttributes<Element> {
  data: any;
  hidden?: boolean;
}

// Create and export the component
const defaultProps = {
  hidden: false,
  children: null,
};
export const Breadcrumb = (props: IBreadcrumbProps) => {
  props = mergeProps({}, defaultProps, props);
  const [state, setState] = createStore({
    id: UUID.v4(),
  });
  const crumbsState = rootStore.crumbsStore.crumbsState;

  createEffect(() => {
    if (!props.hidden) {
      console.log('added');
      untrack(() => crumbsState.add({ id: state.id, ...props.data }));
    }
  });
  createEffect(() => {
    if (props.hidden) untrack(() => crumbsState.remove({ id: state.id, ...props.data }));
  });
  let previousData = props.data;
  createEffect(() => {
    if (!isEqual(previousData, props.data)) {
      untrack(() => crumbsState.update({ id: state.id, ...props.data }));
      previousData = props.data;
    }
  });
  onCleanup(() => {
    crumbsState.remove({ id: state.id, ...props.data });
  });
  return <>{props.children}</>;
};
