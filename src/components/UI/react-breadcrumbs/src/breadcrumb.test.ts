import isEqual from 'lodash/isEqual';

describe('My Test', () => {
  it('isEqual', () => {
    const a = { a: 1, b: { c: { d: { f: 10 } } } };
    const b = { a: 1, b: { c: { d: { f: 10 } } } };
    expect(isEqual(a, b)).toBe(true);
  });
});
