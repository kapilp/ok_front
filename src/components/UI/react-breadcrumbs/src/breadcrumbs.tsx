import { JSX, mergeProps, For } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { NavLink } from '@rturnq/solid-router';
import { rootStore } from '../../../../stores/rootStore';
// Specify BEM block name
const block = 'breadcrumbs';
interface IBreadcrumbsProps extends JSX.HTMLAttributes<Element> {
  className?: string;
  hidden?: boolean;
  separator?: React.ReactNode;
  setCrumbs?: (...args: any[]) => any;
  wrapper?: ((...args: any[]) => any) | any;
}
// Create and export the component
const defaultProps = {
  className: '',
  hidden: false,
  separator: '>',
  setCrumbs: undefined,
  wrapper: props => <nav {...props}>{props.children}</nav>, // A Component
};
export const Breadcrumbs = (props: IBreadcrumbsProps) => {
  props = mergeProps({}, defaultProps, props);
  let hiddenMod = () => (props.hidden ? `${block}--hidden` : '');

  const crumbsState = rootStore.crumbsStore.crumbsState;
  //crumbs = crumbs.sort((a, b) => {return a.pathname.length - b.pathname.length; }); // why sorting?
  //if (props.setCrumbs) crumbsState.setCrumbs(crumbs);
  return (
    <div className={props.className}>
      {JSON.stringify(crumbsState)}
      <Dynamic component={props.wrapper} className={`${block} ${hiddenMod()}`}>
        <div className={`${block}__inner`}>
          <For each={crumbsState.crumbs}>
            {(crumb, i) => (
              <span key={crumb.id} className={`${block}__section`}>
                <NavLink end className={`${block}__crumb`} activeClassName={`${block}__crumb--active`} href={crumb.path}>
                  {crumb.title}
                </NavLink>

                {i < crumbsState.crumbs.length - 1 ? <span className={`${block}__separator`}>{props.separator}</span> : null}
              </span>
            )}
          </For>
        </div>
      </Dynamic>

      {props.children}
    </div>
  );
};
