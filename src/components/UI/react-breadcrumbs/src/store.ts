import {    } from 'solid-js';
import { createStore } from 'solid-js/store';
export const CrumbsStore = crumbs => {
  const [crumbsState, setCrumbsState] = createStore({
    crumbs: crumbs ?? [],
    add: payload => {
      setCrumbsState({ crumbs: [...crumbsState.crumbs, payload] });
    },
    update: payload => {
      setCrumbsState({
        crumbs: crumbsState.crumbs.map(crumb => {
          return crumb.id === payload.id ? payload : crumb;
        }),
      });
    },
    remove: payload => {
      setCrumbsState({
        crumbs: crumbsState.crumbs.filter(crumb => {
          return crumb.id !== payload.id;
        }),
      });
    },
  });
  return { crumbsState };
};
