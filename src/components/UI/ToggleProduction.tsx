import { JSX, Show } from 'solid-js';
import { getIsProduction, IS_PRODUCTION, setIsProduction } from '../../utils/enums';
import { Button } from './core/button/src/Button';

export const ToggleProduction = () => (
  <Show when={!IS_PRODUCTION}>
    <Button onClick={_ => setIsProduction(!getIsProduction())} text={`Toggle Production ${getIsProduction() ? 'off': 'on'}`} />
  </Show>
);
