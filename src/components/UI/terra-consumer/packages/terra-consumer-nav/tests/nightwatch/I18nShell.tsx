import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
// eslint-disable-next-line import/no-extraneous-dependencies
import { I18nProvider, i18nLoader } from 'terra-i18n';
interface Properties {
  children: PropTypes.node.isRequired;
}
type DefaultLayoutState = {
  areTranslationsLoaded?: boolean;
  locale?: string;
  messages?: {};
};
class DefaultLayout extends React.Component<{}, DefaultLayoutState> {
  constructor(props) {
    super(props);
    this.state = {
      areTranslationsLoaded: false,
      locale: 'en-US',
      messages: {},
    };
  }
  componentDidMount() {
    i18nLoader(this.state.locale, this.setState, this);
  }
  render() {
    if (!this.state.areTranslationsLoaded) {
      return null;
    }
    const { children } = this.props;
    return (
      <I18nProvider locale={this.state.locale} messages={this.state.messages}>
        <div style={{ background: 'cadetblue', height: '100vh' }}>{children}</div>
      </I18nProvider>
    );
  }
}
export default DefaultLayout;
