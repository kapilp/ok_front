export type navItemShape = {
  /**
   * Text to be displayed in profile link.
   */
  text: string;
  /**
   * The path the link would redirect to.
   */
  url?: string;
  /**
   * Optional Icon to be displayed in profile link.
   */
  icon?: JSX.Element;
  /**
   *  Specifies where to open the linked document.
   */
  target?: string;
  /**
   * Whether or not the link should be styled as active or not.
   */
  isActive?: boolean;
  /**
   * Whether or not the link is external.
   */
  isExternal?: boolean;
  /**
   * An optional badge. When supplied, displays the value inline, styled alongside the text.
   */
  badgeValue?: number | string;
};
export const navItemShapeDefaults = {
  url: '',
  icon: null,
  target: '_self',
  isActive: false,
  badgeValue: 0,
};
