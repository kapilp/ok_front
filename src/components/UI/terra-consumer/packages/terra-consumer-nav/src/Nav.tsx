import { JSX, match, mergeProps, splitProps, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Button, ButtonVariants } from '../../../../core/button/src/Button';
import { SvgIconClose } from '../../../../core/icon/src/icon/IconClose';
import { Popup } from '../../../../framework/popup/src/Popup';
import { ResponsiveElement } from '../../../../core/responsive-element/src/ResponsiveElement';
import { NavItems } from './components/nav-items/NavItems';
import { Modal } from './components/modal/Modal';
import { NavLogo } from './components/nav-logo/NavLogo';
import { UserProfile } from './components/user-profile/UserProfile';
import { NavHelp } from './components/nav-help/NavHelp';
import { NavBurgerButton } from './components/nav-burger-button/NavBurgerButton';
import { breakpoints } from '../../../../core/breakpoints/src/breakpoints';
import styles from './Nav.module.scss';
import { onBack, onNext, stackState } from '../../../../../../layout/adminLayoutState';
import { Layout } from '../../terra-consumer-layout/src/Layout';
import { getIsLoggedIn } from '../../../../../../utils/ws_events_dispatcher';
import { Icon } from '@amoutonbrady/solid-heroicons';
import { arrowNarrowLeft } from '@amoutonbrady/solid-heroicons/outline';
import { arrowNarrowRight } from '@amoutonbrady/solid-heroicons/outline';
const cx = classNames.bind(styles);
interface INavProps extends JSX.HTMLAttributes<Element> {
  /**
   * An array of objects to be displayed as nav link options.
   */
  navItems?: [];
  /**
   * Object representing all the profile information
   */
  profile?: {
    signinUrl?: string;
    avatar?: JSX.Element;
  };
  /**
   * An object defining the logo to be displayed
   */
  logo?: {
    /**
     * The location where the image to be displayed is stored.
     */
    path?: string;
    /**
     * Alternate text used be screen readers.
     */
    altText?: string;
    /**
     * Whether or not the logo should be placed on top of a white card.
     */
    isCard?: boolean;
  };
  /**
   * Callback function?: should be used to close the nav on mobile devices.
   */
  onRequestClose: () => void;
}
const defaultProps = {
  navItems: [],
  logo: {},
};

export const Nav = (props: INavProps) => {
  props = mergeProps({}, defaultProps, props);
  const [state, setState] = createStore({
    isModalOpen: false,
    modalContent: {
      title: '',
      content: <div />,
    },
  });
  const handleOpenProfile = (modalContent, numberOfLinks) => {
    toggleModal(modalContent, numberOfLinks);
  };
  const handleProfileLinkClick = () => {
    // close our modal so they can see the new page
    setState({ isModalOpen: false });
    // Close the navigation panel as well
    props.onRequestClose();
  };
  const toggleModal = (modalObject?: { title: string; content: JSX.Element }) => {
    if (modalObject?.title && modalObject?.content) {
      setState({
        modalContent: modalObject || {
          title: '',
          content: <div />,
        },
      });
    }
    setState({ isModalOpen: !state.isModalOpen });
  };
  const [p, customProps] = splitProps(props, ['navItems', 'profile', 'logo', 'onRequestClose', 'className', 'stackState', 'onBack', 'onNext']);
  const profileId = 'profile-popup-button';
  const DefaultElement = () => (
    <Modal isModalOpen={state.isModalOpen} title={state.modalContent.title} onRequestClose={toggleModal}>
      {state.modalContent.content}
    </Modal>
  );
  const MyPopup = () => (
    <Popup
      isOpen={state.isModalOpen}
      onRequestClose={toggleModal}
      targetRef={() => document.getElementById(profileId)}
      contentWidth="240"
      contentHeight="auto"
      contentAttachment="top right"
      isArrowDisplayed
    >
      {state.modalContent.content}
    </Popup>
  );
  const [breakPointState, setBreakPointState] = createStore({ width: 0 });
  return (
    <div {...customProps} id="terra-consumer-nav" className={cx('nav', { 'modal-open': state.isModalOpen }, p.className)}>
      <div className={cx('close-button-container')}>
        <Button
          icon={SvgIconClose}
          className={cx('close-button')}
          onClick={() => {
            p.onRequestClose();
          }}
          variant={ButtonVariants.UTILITY}
          text="Close"
        />
      </div>
      <NavLogo {...p.logo} />
      <Button
        className={'back_button'}
        isCompact
        isDisabled={stackState.isBackDisabled}
        icon={() => <Icon path={arrowNarrowLeft} outline style={{ height: '1.5em' }} />}
        isIconOnly
        onClick={p.onBack}
      />
      <Button
        className={'forward_button'}
        isCompact
        isDisabled={stackState.isNextDisabled}
        icon={() => <Icon path={arrowNarrowRight} outline style={{ height: '1.5em' }} />}
        isIconOnly
        onClick={p.onNext}
      />

      <NavItems navItems={p.stackState.stack[p.stackState.activeIndex]} handleClick={p.onRequestClose} />
      <ResponsiveElement responsiveTo="window" onResize={value => setBreakPointState({ width: value })}>
        <Switch fallback={<DefaultElement />}>
          <Match when={breakPointState.width > breakpoints.medium}>
            <MyPopup />
          </Match>
        </Switch>
      </ResponsiveElement>
      {p.profile && (
        <div className={cx('profile')}>
          <UserProfile
            {...p.profile}
            id={profileId}
            handleClick={handleOpenProfile}
            onLinkClick={handleProfileLinkClick}
            //isSignIn={p.profile.signinUrl && !(p.profile.avatar || p.profile.userName)}
            isSignIn={!getIsLoggedIn()}
          />
        </div>
      )}
    </div>
  );
};
Nav.Help = NavHelp;
Nav.Burger = NavBurgerButton;
Nav.UserProfile = UserProfile;
// export default Nav;
//  <NavItems navItems={p.navItems
