import classNames from 'classnames/bind';
import { NavItem } from './NavItem';
import { navItemShape } from '../../NavPropShapes';
import styles from './NavItem.module.scss';
import {  JSX,  createEffect, createMemo, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
const cx = classNames.bind(styles);
interface Properties {
  navItems?: (navItemShape & {
    /**
     * An optional array of objects to be displayed as sub navs toggled by the main nav.
     */
    subItems: navItemShape[];
  })[];

  /**
   * Function to be applied on all nav links, excluding toggle headers.
   */
  handleClick?: () => void;
}
const defaultProps = {
  navItems: [],
};
/**
 * Returns the index of Navitems for an isActive subItem.
 * @param  {object[]} navItems - An array of objects displayed as nav link options.
 * @param  {number} - The index.
 */
const findNavItemsIndex = navItems => navItems.findIndex(item => item.subItems && item.subItems.some(subItem => subItem.isActive));
interface INavItemsProps extends React.HTMLAttributes<Element> {
  navItems?: any;
  handleClick?: any;
  customProps?: any;
}
export const NavItems = (props: INavItemsProps) => {
  props = mergeProps({}, defaultProps, props)
  const initialIndex = findNavItemsIndex(props.navItems);
  const [state, setState] = createStore({
    openToggle: initialIndex,
  });
  createEffect(() => {
    const index = findNavItemsIndex(props.navItems);
    if (findNavItemsIndex(props.navItems) === index) {
      return;
    }
    setState({
      openToggle: index,
    });
  });

  function handleToggle(toggleId, isOpen) {
    setState({ openToggle: isOpen ? toggleId : null });
  }
  const [p, customProps] = splitProps(props, ['navItems', 'handleClick', 'className']);

  const content = createMemo(() =>
    p.navItems.map((element, i) => {
      let toggleProps = {};
      let subNavs = [];
      if (element.subItems) {
        // eslint-disable-next-line react/no-array-index-key
        subNavs = element.subItems.map((item, index) => <NavItem key={index} {...item} handleClick={p.handleClick} />);
        toggleProps = {
          isOpen: state.openToggle === i,
          handleToggle: handleToggle,
          toggleId: i,
        };
      }
      return (
        <NavItem
          key={element.text}
          url={element.url}
          text={element.text}
          icon={element.icon}
          target={element.target}
          isActive={element.isActive}
          isExternal={element.isExternal}
          badgeValue={element.badgeValue}
          handleClick={p.handleClick}
          {...toggleProps}
        >
          {subNavs}
        </NavItem>
      );
    }),
  );

  return (
    <div {...customProps} className={cx(p.className)}>
      {content}
    </div>
  );
};
