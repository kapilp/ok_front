import { JSX, mergeProps, splitProps } from 'solid-js';
import { createStore } from 'solid-js/store';
import { NavLink, Link } from '@rturnq/solid-router';

interface Properties {
  url: string;
  target?: string;
  isExternal?: boolean;
  activeClass?: string;
  handleClick?: () => void;
  children: JSX.Element;
}
const defaultProps = {
  isExternal: false,
  target: '_self',
  handleClick: () => {},
};
export const SmartLink = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  const [p, customProps] = splitProps(props, ['url', 'target', 'isExternal', 'activeClass', 'handleClick', 'children']);
  if (p.isExternal || p.target === '_blank') {
    return (
      <a {...customProps} onClick={p.handleClick} target={p.target} href={p.url}>
        {p.children}
      </a>
    );
  }
  const a = document.createElement('a');
  a.href = p.url;
  // fix for pathname quirk in IE : http://stackoverflow.com/questions/956233/javascript-pathname-ie-quirk
  const linkPath = a.pathname[0] === '/' ? a.pathname : `/${a.pathname}`;
  return (
    <Link {...customProps} activeClassName={p.activeClass} href={linkPath} onClick={p.handleClick} end>
      {p.children}
    </Link>
  );
};
//  <NavLink {...customProps} exact activeClassName={p.activeClass} to={linkPath} onClick={p.handleClick}>
