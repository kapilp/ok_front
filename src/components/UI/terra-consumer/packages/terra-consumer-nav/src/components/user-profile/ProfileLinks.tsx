import {  JSX,  createMemo, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import { SvgIconChevronDown } from '../../../../../../core/icon/src/icon/IconChevronDown';
import { SvgIconChevronUp } from '../../../../../../core/icon/src/icon/IconChevronUp';
import { Toggle } from '../../../../../../core/toggle/src/Toggle';
import { navItemShape } from '../../NavPropShapes';
import { SafeHtml } from '../safe-html/SafeHtml';
import { ProfileLink } from './ProfileLink';
import styles from './UserProfile.module.scss';

const cx = classNames.bind(styles);
interface IProfileLinksProps extends JSX.HTMLAttributes<Element> {
  /**
   * An array of nav items to be displayed on the user profile/ settings menu/popup.
   */
  linkItems?: (navItemShape & { subItems: navItemShape[] })[];

  /**
   * An optional function. When supplied, gets triggered on link click.
   */
  handleClick?: () => void;
}
const defaultProps = {
  linkItems: [],
};

export const ProfileLinks = (props: IProfileLinksProps) => {
  const [state, setState] = createStore({ togglers: {} });

  function handleToggle(toggleIndex) {
    setState('togglers', toggleIndex, !state.togglers[toggleIndex]);
  }
  const [p, customProps] = splitProps(props, ['linkItems', 'handleClick']);

  const profileLinks = p.linkItems.map((linkItem: navItemShape & { subItems: navItemShape[] }, index: number) => {
    if (linkItem.subItems && linkItem.subItems.length > 0) {
      const isOpen = createMemo(() => state.togglers[index]);
      const toggleIcon = createMemo(() => (isOpen() ? <SvgIconChevronUp /> : <SvgIconChevronDown />));
      return (
        <div key={linkItem.text} className={cx(isOpen && 'open')}>
          <button
            onClick={() => {
              handleToggle(index);
            }}
            className={cx('link', 'toggler-wrapper')}
          >
            <Arrange align="stretch" fill={<SafeHtml text={linkItem.text} />} fitEnd={<div>{toggleIcon}</div>} />
          </button>
          <Toggle isOpen={isOpen()} isAnimated={false} className={cx('toggler')}>
            {linkItem.subItems.map(subItem => (
              <ProfileLink key={subItem.text} {...subItem} handleClick={p.handleClick} />
            ))}
          </Toggle>
        </div>
      );
    }
    return <ProfileLink key={linkItem.text} url={linkItem.url} text={linkItem.text} target={linkItem.target} isExternal={linkItem.isExternal} handleClick={p.handleClick} />;
  });

  return <div {...customProps}>{profileLinks}</div>;
};
