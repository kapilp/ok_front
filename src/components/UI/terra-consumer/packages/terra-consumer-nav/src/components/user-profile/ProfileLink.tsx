import {  JSX,  splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { SafeHtml } from '../safe-html/SafeHtml';
import { navItemShape } from '../../NavPropShapes';
import { SmartLink } from '../smart-link/SmartLink';
import styles from './UserProfile.module.scss';
const cx = classNames.bind(styles);
interface Properties extends navItemShape {
  /**
   * An optional function. When supplied, gets triggered on link click.
   */
  handleClick?: () => void;
  customOnClick?: () => void;
}
export const ProfileLink = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['text', 'url', 'target', 'isExternal', 'handleClick', 'customOnClick', 'className']);
  const onClick = () => {
    p.handleClick();
    p.customOnClick && p.customOnClick();
  };
  return (
    <SmartLink {...customProps} url={p.url} target={p.target} isExternal={p.isExternal} className={cx('link', 'profile-item-border', p.className)} handleClick={onClick}>
      <SafeHtml text={p.text} />
    </SmartLink>
  );
};
