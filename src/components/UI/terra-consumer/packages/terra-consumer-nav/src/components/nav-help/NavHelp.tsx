import { JSX, match, mergeProps, splitProps, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import { SvgIconOutlineQuestionMark } from '../../../../terra-consumer-icon/src/icon/IconOutlineQuestionMark';
// import { injectIntl, intlShape } from "react-intl";
import { ResponsiveElement } from '../../../../../../core/responsive-element/src/ResponsiveElement';
import { Modal } from '../modal/Modal';
import { navItemShape } from '../../NavPropShapes';
import { NavHelpContent } from './NavHelpContent';
import { NavHelpPopup } from './NavHelpPopup';
import styles from './NavHelp.module.scss';
import { breakpoints } from '../../../../../../core/breakpoints/src/breakpoints';
const cx = classNames.bind(styles);
interface INavHelpProps extends JSX.HTMLAttributes<Element> {
  /**
   * An array of items to be displayed as help menu/popup.
   */
  helpNavs?: (navItemShape & {
    children: (navItemShape & {
      children: JSX.Element[];
    })[];
  })[];

  /**
   * A unique id set to the help button that will be referred in help menu/popup .
   */
  id?: string;
  /**
   * Injected react-intl formatting api
   */
  //intl: intlShape.isRequired //todo fix this
}
const defaultProps = {
  id: 'terra-consumer-nav-help-button',
  helpNavs: [],
};

export const NavHelp = (props: INavHelpProps) => {
  props = mergeProps({}, defaultProps, props);
  const [state, setState] = createStore({ isOpen: false });
  const togglePopup = () => setState({ isOpen: !state.isOpen });

  const [p, customProps] = splitProps(props, ['helpNavs', 'id', 'intl']);
  const helpText = 'Info';
  const helpButton = (
    <button id={p.id} onClick={togglePopup} className={cx('nav-help')}>
      <Arrange fill={<SvgIconOutlineQuestionMark />} fitEnd={<div className={cx('button-text-padding')}>{helpText}</div>} align="stretch" />
    </button>
  );
  const PopupContent = () => <NavHelpContent helpContent={p.helpNavs} onLinkClick={togglePopup} />;
  const DefaultElement = () => (
    <Modal isModalOpen={state.isOpen} title={helpText} onRequestClose={togglePopup}>
      <PopupContent />
    </Modal>
  );
  const Popup = () => (
    <NavHelpPopup
      title={helpText}
      hasHeader
      isOpen={state.isOpen}
      targetRef={() => document.getElementById(p.id)}
      closePopup={togglePopup}
      contentWidth="320"
      contentHeight="240"
      classNameContent={cx('popup-content')}
      contentAttachment="middle center"
      popupContent={<PopupContent />}
      rowCount={p.helpNavs.length}
    />
  );
  const [breakPointState, setBreakPointState] = createStore({ width: 0 });

  return (
    <div {...customProps}>
      <ResponsiveElement responsiveTo="window" onResize={value => setBreakPointState({ width: value })}>
        <Switch fallback={<DefaultElement />}>
          <Match when={breakPointState.width > breakpoints.medium}>
            <Popup />
          </Match>
        </Switch>
      </ResponsiveElement>
      {helpButton}
    </div>
  );
};
// <ResponsiveElement responsiveTo="window" medium={popup}>
// export default injectIntl(NavHelp);
