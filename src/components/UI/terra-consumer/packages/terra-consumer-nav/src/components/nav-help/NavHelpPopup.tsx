import { JSX,  mergeProps, splitProps  } from 'solid-js';
import classNames from 'classnames/bind';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import { Button, ButtonVariants } from '../../../../../../core/button/src/Button';
import { SvgIconClose } from '../../../../../../core/icon/src/icon/IconClose';
import { Popup } from '../../../../../../framework/popup/src/Popup';
import { Spacer } from '../../../../../../core/spacer/src/Spacer';
import { SafeHtml } from '../safe-html/SafeHtml';
import styles from './NavHelpPopup.module.scss';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Title to be rendered on top of the popup content
   */
  title?: string;
  /**
   * Boolean value to hide/display popup header with title and close button
   */
  hasHeader?: boolean;
  /**
   * Content to be displayed in a dialog
   */
  popupContent?: JSX.Element;
  /**
   * Boolean value to render the popup
   */
  isOpen?: boolean;
  /**
   * Required callback function for use by parent component to update state.
   */
  closePopup?: () => void;
  /**
   * Number of rows in the help pop up to know what height to use
   */
  rowCount: number;
}
const defaultProps = {
  hasHeader: false,
  isOpen: false,
  closePopup: null,
  contentHeight: 'auto',
};
export const NavHelpPopup = (props: Properties) => {
  props = mergeProps({}, defaultProps, props)
  const [p, customProps] = splitProps(props, ['title', 'hasHeader', 'popupContent', 'isOpen', 'closePopup', 'rowCount', 'contentHeight']);

  const PopupHeader = () => {
    return (
      <Spacer className={cx('popup-title')} paddingTop="small-1" paddingRight="small" paddingBottom="small-1" paddingLeft="large+1">
        <Arrange
          fill={<SafeHtml text={p.title} />}
          fitEnd={<Button className={cx('close-button')} icon={SvgIconClose} onClick={p.closePopup} text="Close Popup" variant={ButtonVariants.UTILITY} />}
        />
      </Spacer>
    );
  };
  return (
    <Popup {...customProps} isOpen={p.isOpen} onRequestClose={p.closePopup} isHeaderDisabled contentHeight="auto">
      <div>
        {p.hasHeader && <PopupHeader />}
        <div>{p.popupContent}</div>
      </div>
    </Popup>
  );
};
