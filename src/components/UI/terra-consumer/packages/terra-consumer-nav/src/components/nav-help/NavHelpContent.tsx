import {  JSX,  createMemo,    } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import classNames from 'classnames/bind';
import { SvgIconChevronUp } from '../../../../../../core/icon/src/icon/IconChevronUp';
import { SvgIconChevronDown } from '../../../../../../core/icon/src/icon/IconChevronDown';
import { Toggle } from '../../../../../../core/toggle/src/Toggle';
import { SafeHtml } from '../safe-html/SafeHtml';
import { SmartLink } from '../smart-link/SmartLink';
import styles from './NavHelp.module.scss';
const cx = classNames.bind(styles);
interface INavHelpContentProps extends JSX.HTMLAttributes<Element> {
  helpContent: {
    text: string;
    children?: JSX.Element[];
    url?: string;
    isExternal?: boolean;
    target?: string;
  }[];

  onLinkClick: () => void;
}

export const NavHelpContent = (props: INavHelpContentProps) => {
  const [state, setState] = createStore({ togglers: {} });

  function handleToggle(toggleIndex: number) {
    setState('togglers', toggleIndex, !state.togglers[toggleIndex]);
  }

  const contentList = createMemo(() =>
    props.helpContent.map((content, index) => {
      return (
        <>
          {content.children && Array.isArray(content.children) && content.children.length > 0 ? (
            <div className={cx('help-item-wrapper')} key={content.text}>
              <button className={cx('help-item', { 'toggler-open': state.togglers[index] })} onClick={() => handleToggle(index)}>
                <Arrange
                  className={cx('help-item-text')}
                  align="stretch"
                  fitStart={<div className={cx('icon-text-padding')}>{content.icon}</div>}
                  fill={
                    <div>
                      <SafeHtml text={content.text} />
                    </div>
                  }
                  fitEnd={<div>{state.togglers[index] ? <SvgIconChevronUp /> : <SvgIconChevronDown />}</div>}
                />
              </button>
              <Toggle isOpen={state.togglers[index]} isAnimated={false} className={cx('toggler-padding')}>
                {content.children.map(element => (
                  <p key={element.text} className={cx('toggler-content-alignment')}>
                    <span className={cx('help-subitem')}>
                      <SafeHtml text={element.text} />
                    </span>
                  </p>
                ))}
              </Toggle>
            </div>
          ) : (
            <SmartLink className={cx('help-item')} key={content.text} url={content.url} target={content.target} isExternal={content.isExternal} handleClick={props.onLinkClick}>
              <Arrange
                className={cx('help-item-text')}
                align="center"
                fitStart={<div className={cx('icon-text-padding')}>{content.icon}</div>}
                fill={
                  <div>
                    <SafeHtml text={content.text} />
                  </div>
                }
              />
            </SmartLink>
          )}
        </>
      );
    }),
  );

  return <div>{contentList}</div>;
};
