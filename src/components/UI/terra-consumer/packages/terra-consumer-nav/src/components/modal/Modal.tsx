import { JSX,  mergeProps, splitProps  } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { Button, ButtonVariants } from '../../../../../../core/button/src/Button';
import { SvgIconClose } from '../../../../../../core/icon/src/icon/IconClose';
import { Overlay } from '../../../../../../core/overlay/src/Overlay';
import { SafeHtml } from '../safe-html/SafeHtml';
import styles from './Modal.module.scss';

const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Title to be rendered on top of the modal content
   */
  title?: string;
  /**
   * Boolean value to render modal.
   */
  isModalOpen?: boolean;
  /**
   * Required callback function for use by parent component to update state.
   */
  onRequestClose: (modalObject: { title: string; content: JSX.Element }) => void;
  /**
   * Content to be rendered in the body of the dialog box.
   */
  children?: JSX.Element;
}
const defaultProps = {
  isModalOpen: false,
};
export const Modal = (props: Properties) => {
  props = mergeProps({}, defaultProps, props)
  const [p, customProps] = splitProps(props, ['title', 'isModalOpen', 'onRequestClose', 'children', 'className']);

  const modalHeader = (
    <div className={cx('modal-title')}>
      <SafeHtml text={p.title} />
      <div className={cx('close-button-container')}>
        <Button icon={SvgIconClose} className={cx('close-button')} onClick={p.onRequestClose} variant={ButtonVariants.UTILITY} text="Close" />
      </div>
    </div>
  );
  return (
    <Overlay {...customProps} isOpen={p.isModalOpen} className={classNames(cx('overlay'), p.className)}>
      {modalHeader}
      <div className={cx('modal')}>{p.children}</div>
    </Overlay>
  );
};
