import {  JSX,  Component, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Dynamic } from 'solid-js/web';
export const SafeHtml: Component<{ text: string; tag?: string } & JSX.HTMLAttributes<HTMLSpanElement>> = props => {
  const [customProps, otherProps] = splitProps(props, ['text', 'tag']);
  return () => <Dynamic component={props.tag || 'span'} innerHTML={customProps.text} {...otherProps} />;
};
