import {  JSX,  createMemo, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames/bind';
import { Card } from '../../../../../../core/card/src/Card';
import { CardBody } from '../../../../../../core/card/src/CardBody';
import { SmartLink } from '../smart-link/SmartLink';
import styles from './NavLogo.module.scss';

interface Properties {
  /**
   *  The url of the logo to be shown.
   */
  url?: string;
  /**
   *  The alternate text that is read by screen readers are displayed if the image fails to load.
   */
  altText?: string;
  /**
   * Whether or not the logo should be placed on top of a white card.
   */
  isCard?: boolean;
  /**
   * Props for a smartlink
   */
  link?: {
    url: string;
    isExternal?: boolean;
  };
}
const defaultProps = {
  altText: 'Client Logo',
  isCard: false,
};
const cx = classNames.bind(styles);
export const NavLogo = (props: Properties) => {
  props = mergeProps({}, defaultProps, props)
  const [p, customProps] = splitProps(props, ['url', 'altText', 'isCard', 'link', 'className']);

  const imgProps = createMemo(() => {
    return {
      className: cx('img'),
      ...(p.url
        ? {
            src: p.url,
            alt: p.altText,
          }
        : {
            children: p.altText,
          }),
    };
  });
  const LogoElement = createMemo(() => (p.url ? 'img' : 'p'));

  const imgTag = <Dynamic component={LogoElement()} {...imgProps()} />;
  const imgContent = <>{p.link ? <SmartLink {...p.link}>{imgTag} </SmartLink> : imgTag}</>;
  const body = <>{p.isCard && !!p.url ? <CardBody> {imgContent} </CardBody> : imgContent}</>;

  const domNode = createMemo(() => (p.isCard && !!p.url ? Card : 'div'));
  const logoClassNames = createMemo(() => cx('logo-container', p.className));

  return (
    <Dynamic component={domNode()} {...customProps} className={logoClassNames()}>
      {body}
    </Dynamic>
  );
};
