import {  JSX,  splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { SvgIconMenu } from '../../../../../../core/icon/src/icon/IconMenu';
import styles from './NavBurgerButton.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  handleClick: () => void;
}
export const NavBurgerButton = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['handleClick', 'className']);

  return (
    <button {...customProps} aria-label="Open Navbar" className={cx('burger', 'button', p.className)} onClick={p.handleClick}>
      <span className={cx('icon')}>
        <SvgIconMenu />
      </span>
    </button>
  );
};
