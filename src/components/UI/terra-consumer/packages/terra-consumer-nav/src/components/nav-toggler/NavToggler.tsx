import {  JSX,  createMemo, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Toggle } from '../../../../../../core/toggle/src/Toggle';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import { SvgIconChevronDown } from '../../../../../../core/icon/src/icon/IconChevronDown';
import { SvgIconChevronUp } from '../../../../../../core/icon/src/icon/IconChevronUp';
import styles from './NavToggler.module.scss';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Element to be displayed on top of the button that opens the toggle content.
   */
  header?: JSX.Element;
  /**
   * Function callback when the toggle component is clicked.
   */
  handleToggle: (isOpen: boolean) => void;
  /**
   * Whether or not the content should be toggled open or not.
   */
  isOpen?: boolean;
  /**
   * Items to be displayed within the toggler.
   */
  children?: JSX.Element;
}
const defaultProps = {
  isOpen: false,
};
export const NavToggler = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['header', 'handleToggle', 'isOpen', 'children']);
  props = mergeProps({}, defaultProps, props)

  const toggleIcon = createMemo(() => (p.isOpen ? <SvgIconChevronUp /> : <SvgIconChevronDown />));
  return (
    <div {...customProps}>
      <button className={cx('toggle-header')} onClick={() => p.handleToggle(!p.isOpen)}>
        <Arrange alignFill="stretch" alignFitEnd="center" fill={p.header} fitEnd={toggleIcon()} />
      </button>
      <Toggle isOpen={p.isOpen} isAnimated className={cx('toggler')}>
        {p.children}
      </Toggle>
    </div>
  );
};
