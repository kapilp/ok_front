import {  JSX,  createMemo, createSignal, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { navItemShape } from '../../NavPropShapes';
import { NavToggler } from '../nav-toggler/NavToggler';
import { SafeHtml } from '../safe-html/SafeHtml';
import { SmartLink } from '../smart-link/SmartLink';
import styles from './NavItem.module.scss';
const cx = classNames.bind(styles);
interface Properties extends navItemShape {
  /**
   * The id of the toggle component to be toggled.
   */
  toggleId?: number;
  /**
   * Function callback when the toggle component is clicked.
   */
  handleToggle?: () => void;
  /**
   * Whether or not the content should be toggled open or not.
   */
  isOpen?: boolean;
  /**
   * Function to be applied to the generated link.
   */
  handleClick?: () => void;
  /**
   * Items to be displayed within the toggler.
   */
  children?: JSX.Element;
}
const defaultProps = {
  // eslint-disable-next-line react/default-props-match-prop-types
  isActive: false,
  handleToggle: () => {},
};
export const NavItem = (props: Properties) => {
  props = mergeProps({}, defaultProps, props)
  const [p, customProps] = splitProps(props, [
    'url',
    'text',
    'icon',
    'target',
    'isActive',
    'badgeValue',
    'toggleId',
    'handleToggle',
    'isOpen',
    'isExternal',
    'handleClick',
    'children',
    'className',
  ]);
  const activeClass = cx('active');

  const badgeString = createMemo(() => `${p.badgeValue}`);
  const itemLabel = (
    <div>
      <SafeHtml text={p.text} />
      {p.badgeValue !== undefined && p.badgeValue !== null && badgeString().trim() && <SafeHtml className={cx('badge')} text={badgeString()} />}
    </div>
  );
  const itemText = (
    <div className={cx('item')}>
      {p.icon && <div className={cx('icon')}>{p.icon}</div>}
      <div className={cx('label')}>{itemLabel}</div>
    </div>
  );
  const [isActive, setActive] = createSignal();
  return (
    <>
      {p.children && Array.isArray(p.children) && p.children.length > 0 ? (
        <NavToggler {...customProps} header={itemText} handleToggle={open => p.handleToggle(p.toggleId, open)} isOpen={p.isOpen} className={cx('nested', p.className)}>
          {p.children}
        </NavToggler>
      ) : (
        <div className={isActive() ? activeClass : ''}>
          <SmartLink
            {...customProps}
            url={p.url}
            target={p.target}
            isExternal={p.isExternal}
            activeClass={activeClass}
            handleClick={p.handleClick}
            className={cx('link', p.className)}
            onActive={setActive}
          >
            {itemText}
          </SmartLink>
        </div>
      )}
    </>
  );
};
