import { JSX, mergeProps, splitProps } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Arrange } from '../../../../../../core/arrange/src/Arrange';
import classNames from 'classnames/bind';
// import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { SvgIconEllipses } from '../../../../../../core/icon/src/icon/IconEllipses';
import { SvgIconOutlineUserCircle } from '../../../../terra-consumer-icon/src/icon/IconOutlineUserCircle';
import { ProfileLinks } from './ProfileLinks';
import { SafeHtml } from '../safe-html/SafeHtml';
import { Link, NavLink } from '@rturnq/solid-router';
import { getMember } from '../../../../../../../../utils/ws_events_dispatcher';
import styles from './UserProfile.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * User name to be displayed in profile if user is signed in.
   */
  userName?: string;
  /**
   * Avatar to be displayed in profile.
   */
  avatar?: JSX.Element;
  /**
   * A unique id set to the profile popup button that will be referred in profile popup.
   */
  id?: string;
  /**
   * The path signout button would redirect to.
   */
  signoutUrl: string;
  /**
   * The path to the login page.
   */
  signinUrl?: string;
  /**
   * Determiniate of whether profile should render as signin link or profile button.
   */
  isSignIn?: boolean;
  /**
   * The content of the each profile items.
   */
  profileLinks?: [];
  /**
   * A function used as a callback to render modal and popup content.
   */
  handleClick: (title: string, content: JSX.Element) => void;
  /**
   * Injected react-intl formatting api
   */
  // intl?: intlShape.isRequired; // todo fix
  /**
   * A function used as a callback when user clicks on any link in the content.
   */
  onLinkClick: () => void;
}
const defaultProps = {
  avatar: <SvgIconOutlineUserCircle />,
  profileLinks: [],
  isSignIn: false,
  id: 'terra-conumser-nav-profile-button',
};
export const UserProfile = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['userName', 'avatar', 'id', 'signoutUrl', 'signinUrl', 'isSignIn', 'profileLinks', 'handleClick', 'onLinkClick', 'intl']);
  props = mergeProps({}, defaultProps, props);

  const ProfileContent = () => {
    return (
      <>
        {p.isSignIn ? (
          <Link className={cx('popup-button')} href={p.signinUrl}>
            <Arrange fitStart={<div className={cx('avatar')}>{p.avatar}</div>} fill={'Sign In'} align="center" />
          </Link>
        ) : (
          () => {
            const content = (
              <div>
                <ProfileLinks linkItems={p.profileLinks} handleClick={p.onLinkClick} />
                <Link className={cx('link', 'signout-border')} href={p.signoutUrl}>
                  {'Sign Out'}
                </Link>
              </div>
            );
            const title = 'Settings';
            return (
              <button className={cx('popup-button')} onClick={() => p.handleClick({ title, content }, p.profileLinks.length + 1)}>
                <Arrange
                  fitStart={<div className={cx('avatar')}>{p.avatar}</div>}
                  fill={<SafeHtml text={getMember()?.email} />}
                  fitEnd={<SvgIconEllipses className={cx('icon')} id={p.id} />}
                  alignFill="center"
                />
              </button>
            );
          }
        )}
      </>
    );
  };

  return (
    <div {...customProps} className={cx('profile')}>
      <ProfileContent />
    </div>
  );
};

// export default injectIntl(UserProfile);
// fill={<SafeHtml text={p.userName} />}
// Both <Link component was <a>
