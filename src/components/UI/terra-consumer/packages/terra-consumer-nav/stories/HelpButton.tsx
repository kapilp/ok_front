import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import { SvgIconOutlineQuestionMark } from '../../terra-consumer-icon/src/icon/IconOutlineQuestionMark';
import { NavHelp } from '../src/components/nav-help/NavHelp';
const helpItems = [
  {
    text: 'Technical Questions',
    icon: <SvgIconOutlineQuestionMark />,
    children: [
      {
        text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
      },
    ],
  },
  {
    text: 'Get Support ID',
    icon: <SvgIconOutlineQuestionMark />,
    children: [
      {
        text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
      },
    ],
  },
  {
    isExternal: true,
    text: 'Link',
    url: 'http://localhost:8080/',
    icon: <SvgIconOutlineQuestionMark />,
  },
  {
    isExternal: true,
    text: 'Another link 1',
    url: 'http://localhost:8080/',
  },
];
export const HelpButtonStory = () => <NavHelp helpNavs={helpItems} />;
