import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import Markdown from "terra-markdown";
import ReadMe from "../README.md";
const Home = () => <Markdown src={ReadMe} />;
export default Home;
