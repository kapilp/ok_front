/* eslint-disable import/no-extraneous-dependencies */
import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import Modal from "./SimpleModal";
const PortalNav = () => (
  <Modal text="Portal Nav">
    <h1>This is PortalNav placeholder</h1>
  </Modal>
);
export default PortalNav;
