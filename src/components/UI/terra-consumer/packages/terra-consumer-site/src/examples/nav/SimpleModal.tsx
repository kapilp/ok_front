/* eslint-disable import/no-extraneous-dependencies */
import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from "terra-button";
interface ISimpleModalProps extends React.HTMLAttributes<Element> {
  text?: string;
}
type SimpleModalState = {
  isOpen?: boolean
};
class SimpleModal extends React.Component<ISimpleModalProps, SimpleModalState> {
  constructor() {
    super();
    this.state = { isOpen: false };
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleToggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }
  render() {
    return (
      <div>
        <Button text={this.props.text} onClick={this.handleToggle} />
        {this.state.isOpen && (
          <div
            style={{
              position: "fixed",
              left: 0,
              right: 0,
              bottom: 0,
              top: 0,
              backgroundColor: "#fff"
            }}
          >
            <Button
              variant="link"
              text="Close"
              onClick={this.handleToggle}
              style={{ position: "fixed", top: 0, right: 0 }}
            />
            {this.props.children}
          </div>
        )}
      </div>
    );
  }
}
export default SimpleModal;
