import { JSX, match, splitProps, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Nav } from '../../terra-consumer-nav/src/Nav';
import { ResponsiveElement } from '../../../../core/responsive-element/src/ResponsiveElement';
import { Overlay } from '../../../../core/overlay/src/Overlay';
// import { injectIntl, intlShape } from "react-intl";
import { breakpoints } from '../../../../core/breakpoints/src/breakpoints';
// import 'terra-base/lib/baseStyles';
import styles from './Layout.module.scss';
import { NavBurgerButton } from '../../terra-consumer-nav/src/components/nav-burger-button/NavBurgerButton';
import { NavHelp } from '../../terra-consumer-nav/src/components/nav-help/NavHelp';

const cx = classNames.bind(styles);

interface ILayoutProps extends JSX.HTMLAttributes<Element> {
  /**
   * Object of configuration for the side navigation and profile.
   */
  nav: {};
  /**
   * Array of links to show for the content of the help button
   */
  helpItems?: [];
  /**
   * Alert banner
   */
  siteAlert?: JSX.Element;
  /**
   * A center justified logo in header for mobile.
   */
  logo?: {
    /**
     * A center justified logo in header for mobile.
     */
    mobileLogo?: {
      url: string;
      altText: string;
    };
    navLogo?: {};
  };
  /**
   * Injected react-intl formatting api
   */
  // intl?: intlShape.isRequired, // TODO FIX
  children: JSX.Element;
}

export const Layout = (props: ILayoutProps) => {
  const [state, setState] = createStore({
    isMobileNavOpen: false,
  });
  const toggleNav = () => {
    setState({
      isMobileNavOpen: !state.isMobileNavOpen,
    });
  };
  const [p, customProps] = splitProps(props, ['nav', 'helpItems', 'logo', 'siteAlert', 'intl', 'stackState', 'onBack', 'onNext', 'className']);
  const overlay = <Overlay onRequestClose={toggleNav} isOpen={state.isMobileNavOpen} backgroundStyle="clear" isRelativeToContainer />;
  const [breakPointState, setBreakPointState] = createStore({ width: 0 });
  return (
    <div className={cx('wrap')}>
      <div className={cx('skip-container')}>
        <a className={cx('skip-to-main-content')} href="#main-container" id="skip-maincontent-link">
          Skip to Main Content
        </a>
      </div>
      {p.siteAlert}
      <div {...customProps} className={cx('layout', { open: state.isMobileNavOpen }, p.className)}>
        <nav className={cx('nav')}>
          <Nav {...p.nav} stackState={p.stackState} onBack={p.onBack} onNext={p.onNext} logo={p.logo.navLogo} onRequestClose={toggleNav} />
        </nav>
        <main id="main-container" className={cx('main-container')}>
          <ResponsiveElement responsiveTo="window" onResize={value => setBreakPointState({ width: value })}>
            <Switch fallback={<div />}>
              <Match when={breakPointState.width < breakpoints.medium}>{overlay}</Match>
            </Switch>
          </ResponsiveElement>

          <div className={cx('main-container-inner')}>
            <div className={cx('nav-burgerbar')}>
              <NavBurgerButton handleClick={toggleNav} />
              <div className={cx('mobile-logo')}>{p.logo && p.logo.mobileLogo && <img src={p.logo.mobileLogo.url} alt={p.logo.mobileLogo.altText} />}</div>
            </div>
            <div className={cx('main-content')}>{props.children}</div>
            {p.helpItems && <NavHelp className={cx('help-button')} helpNavs={p.helpItems} id="nav-help-button" />}
          </div>
        </main>
      </div>
    </div>
  );
};
// export default injectIntl(Layout);
// <Nav {...p.nav} logo={p.logo.navLogo} onRequestClose={toggleNav} />
