import { JSX, Switch } from 'solid-js';
import { createStore } from 'solid-js/store';
// import { Route, BrowserRouter } from "react-router-dom";
import { SvgIconPerson } from '../../../../core/icon/src/icon/IconPerson';
import { Layout } from '../src/Layout';
import './DexTheme.scss';
import { Router } from '@rturnq/solid-router';
import { pathIntegration } from '@rturnq/solid-router';
import { MatchRoute } from '@rturnq/solid-router';
import { Base } from '../../../../core/base/src/Base';
const data = {
  nav: {
    navItems: [
      {
        url: '/',
        text: 'Dashboard',
        badgeValue: 1,
      },
      {
        text: 'Messaging',
        badgeValue: 2,
        subItems: [
          {
            url: '/inbox',
            text: 'Inbox',
            badgeValue: 1,
          },
          {
            url: '/sent',
            text: 'Sent',
            badgeValue: 1,
          },
        ],
      },
      {
        text: 'Health Record',
        badgeValue: 0,
        subItems: [
          {
            url: '/health',
            text: 'Health',
          },
          {
            url: '/record',
            text: 'Record',
          },
        ],
      },
      {
        text: 'Test Data',
        subItems: [
          {
            url: '/test',
            text: 'Tests test test Tests test testa Tes test test ',
            icon: <SvgIconPerson />,
            badgeValue: 2,
          },
          {
            url: '/data',
            text: 'Data',
            badgeValue: 2,
          },
        ],
      },
      {
        text: 'More Tests',
        subItems: [
          {
            url: '/more',
            text: 'More',
          },
          {
            url: '/tests',
            text: 'Tests',
            icon: <SvgIconPerson />,
          },
        ],
      },
      {
        url: 'http://google.com',
        text: 'Google External',
        isExternal: true,
        target: '_blank',
      },
    ],
    profile: {
      profileLinks: [
        {
          text: 'Account',
          url: '/account',
        },
        {
          text: 'Notifications',
          url: '/notifications',
        },
        {
          url: 'http://google.com',
          text: 'Google External',
          isExternal: true,
          target: '_blank',
        },
      ],
      // comment out userName to see signin
      userName: 'Martin O&#39;Neil',
      signinUrl: 'http://localhost:8080/',
      signoutUrl: 'http://localhost:8080/',
    },
  },
  helpItems: [
    {
      text: 'Technical Questions',
      children: [
        {
          text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
        },
      ],
    },
    {
      text: 'Get Support ID',
      children: [
        {
          text: 'Need help using this portal or need to report an issue? Contact the support team at 123-xxx-xxxx',
        },
      ],
    },
    {
      isExternal: true,
      url: 'http://google.com',
      text: 'Google External',
      target: '_blank',
    },
  ],
  logo: {
    mobileLogo: {
      url: 'https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/2014/02/Olympic-logo.png',
      altText: 'Placeholder logo',
    },
    navLogo: {
      url: 'https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/2014/02/Olympic-logo.png',
      altText: 'Placeholder logo',
      isCard: false,
    },
  },
};
export const DexLayoutStory = () => (
  <Base>
    <main className="dex-theme">
      <Router integration={pathIntegration()}>
        <Layout {...data}>
          <Switch>
            <MatchRoute path="/inbox">
              <h1>You have millions of unread messages</h1>{' '}
            </MatchRoute>
            <MatchRoute path="/sent">
              <h1>Opps, it is empty</h1>{' '}
            </MatchRoute>
            <MatchRoute path="/health">
              <h1>Game</h1>
            </MatchRoute>
            <MatchRoute path="/record">
              <h1 style={{ color: 'white' }}>
                of Thrones more content more content more content more content more content more content more content more content more content more content more content more
                content more content more content more content more content more content more content more content more content more content more content more content more content
                more content more content more content more content more content more content more content more content more content more content more content more content more
                content more content more content more content more content more content more content more content
              </h1>
            </MatchRoute>
            <MatchRoute path="/">
              <h1 style={{ color: 'white' }}>
                of Thrones more content more content more content more content more content more content more content more content more content more content more content more
                content more content more content more content more content more content more content more content more content more content more content more content more content
                more content more content more content more content more content more content more content more content more content more content more content more content more
                content more content more content more content more content more content more content more content
              </h1>
            </MatchRoute>
          </Switch>
        </Layout>
      </Router>
    </main>
  </Base>
);
