import { IconPathDetails } from "../config";
class csvObject {
  isBidi: boolean;
  isSpin: boolean;
  isThemeable: boolean;
  name: any;
  svgDest: string;
  svgSrc: string;
  constructor(name, filepath, themeable, bidi) {
    this.name = name;
    // use current directory svg path
    this.svgSrc = `../../../tmp/svg/${filepath}`;
    this.svgDest = `${IconPathDetails.svgDir}${name}.svg`;
    this.isThemeable = !!themeable;
    this.isBidi = bidi === "bi-directional";
    this.isSpin = name === "spinner";
  }
}
export default csvObject;
