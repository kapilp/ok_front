export const IconRepo = {
  csvFile: "../../../tmp/ConsumerTerraIconMapping.csv",
  // TODO: replace this path to use tera_consumer_icon csv from node modules
  svgDir: "../../../tmp/svg/"
};
// export terra consumer Icon with correct path
export const IconPathDetails = {
  csvFile: "src/consumer-icons.csv",
  svgDir: "src/svg/",
  iconDir: "src/icon/",
  exampleDir: "example/"
};
