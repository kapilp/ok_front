/* eslint-disable import/no-extraneous-dependencies */
import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Link } from "react-router";
const IconTests = () => (
  <div>
    <ul>
      <li>
        <Link to="/tests/icon-tests/default">Icon Default</Link>
      </li>
    </ul>
  </div>
);
export default IconTests;
