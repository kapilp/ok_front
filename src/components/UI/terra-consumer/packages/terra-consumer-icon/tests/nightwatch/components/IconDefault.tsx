import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
import IconChatBubble from "../../../lib/icon/IconChatBubble";
const IconDefault = () => (
  <div>
    <h3>Default Icon</h3>
    <IconChatBubble id="icon-default" />
  </div>
);
export default IconDefault;
