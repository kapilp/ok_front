/* eslint-disable */
import { JSX,  mergeProps  } from 'solid-js';
import { IconBase, IconBaseProperties } from '../../../../../core/icon/src/IconBase';
export const SvgIconOutlineQuestionMark = (customProps: IconBaseProperties) => {
  customProps = mergeProps({},  {
    viewBox: '0 0 48 48',
    xmlns: 'http://www.w3.org/2000/svg',
  }, customProps);
  return (
    <IconBase {...customProps}>
      <circle fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" cx="24" cy="24" r="22"></circle>
      <circle data-color="color-2" fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" cx="24" cy="36" r="1"></circle>
      <path
        data-color="color-2"
        fill="none"
        stroke="#444"
        strokeWidth="2"
        strokeLinecap="square"
        strokeMiterlimit="10"
        d="M24 29v-5c3.218 0 6-2.782 6-6s-2.782-6-6-6c-2.389 0-4.685 1.785-5.584 3.843"
      ></path>
      <circle data-color="color-2" data-stroke="none" fill="#444" cx="24" cy="36" r="1"></circle>
    </IconBase>
  );
};
