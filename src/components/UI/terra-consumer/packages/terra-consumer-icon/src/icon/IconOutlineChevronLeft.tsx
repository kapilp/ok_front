/* eslint-disable */
import { JSX,  mergeProps  } from 'solid-js';
import { IconBase, IconBaseProperties } from '../../../../../core/icon/src/IconBase';
export const SvgIconOutlineChevronLeft = (customProps: IconBaseProperties) => {
  customProps = mergeProps({},  {
    className: '',
    viewBox: '0 0 48 48',
    xmlns: 'http://www.w3.org/2000/svg',
    isBidi: true,
  }, customProps);
  return (
    <IconBase {...customProps}>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M34 4L14 24l20 20"></path>
    </IconBase>
  );
};
