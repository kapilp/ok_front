/* eslint-disable */
import { JSX,  mergeProps  } from 'solid-js';
import { IconBase, IconBaseProperties } from '../../../../../core/icon/src/IconBase';
export const SvgIconOutlineMartini = (customProps: IconBaseProperties) => {
  customProps = mergeProps({},  {
    viewBox: '0 0 48 48',
    xmlns: 'http://www.w3.org/2000/svg',
  }, customProps);
  return (
    <IconBase {...customProps}>
      <path
        data-cap="butt"
        data-color="color-2"
        fill="none"
        stroke="#444"
        strokeWidth="2"
        strokeMiterlimit="10"
        d="M30 12c0-4.4 3.6-8 8-8s8 3.6 8 8-3.6 8-8 8c-2 0-3.9-.8-5.3-2"
      ></path>
      <path data-cap="butt" data-color="color-2" fill="none" stroke="#444" strokeWidth="2" strokeMiterlimit="10" d="M14.9 22h14.2"></path>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M38 12L22 30 6 12z"></path>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M22 30v14"></path>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M12 44h20"></path>
    </IconBase>
  );
};
