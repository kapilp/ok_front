/* eslint-disable */
import { JSX,  mergeProps  } from 'solid-js';
import { IconBase, IconBaseProperties } from '../../../../../core/icon/src/IconBase';
export const SvgIconDataSync = (customProps: IconBaseProperties) => {
  customProps = mergeProps({},  {
    viewBox: '0 0 48 48',
    xmlns: 'http://www.w3.org/2000/svg',
  }, customProps);
  return (
    <IconBase {...customProps}>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M22 46H9a4 4 0 0 1-4-4V6a4 4 0 0 1 4-4h24a4 4 0 0 1 4 4v15"></path>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M5 10h32"></path>
      <path fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M5 36h17"></path>
      <path
        data-cap="butt"
        data-color="color-2"
        fill="none"
        stroke="#444"
        strokeWidth="2"
        strokeMiterlimit="10"
        d="M28.223 34c.91-4.008 4.494-7 8.777-7 3.534 0 6.527 2.037 8 5"
      ></path>
      <path data-color="color-2" fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M45 26v6h-6"></path>
      <path
        data-cap="butt"
        data-color="color-2"
        fill="none"
        stroke="#444"
        strokeWidth="2"
        strokeMiterlimit="10"
        d="M45.777 38c-.91 4.008-4.494 7-8.777 7-3.534 0-6.527-2.037-8-5"
      ></path>
      <path data-color="color-2" fill="none" stroke="#444" strokeWidth="2" strokeLinecap="square" strokeMiterlimit="10" d="M29 46v-6h6"></path>
    </IconBase>
  );
};
