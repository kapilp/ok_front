/* eslint-disable */
import { JSX,  mergeProps  } from 'solid-js';
import { IconBase, IconBaseProperties } from '../../../../../core/icon/src/IconBase';
export const SvgIconOutlineNav = (customProps: IconBaseProperties) => {
  customProps = mergeProps({},  {
    viewBox: '0 0 48 48',
    xmlns: 'http://www.w3.org/2000/svg',
  }, customProps);
  return (
    <IconBase {...customProps}>
      <path fill="#EA9860" d="M45 26H3a1 1 0 0 1-1-1v-2a1 1 0 0 1 1-1h42a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1z"></path>
      <path fill="#EA9860" d="M45 12H3a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h42a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1z"></path>
      <path fill="#EA9860" d="M45 40H3a1 1 0 0 1-1-1v-2a1 1 0 0 1 1-1h42a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1z"></path>
    </IconBase>
  );
};
