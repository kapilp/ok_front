import {  JSX,     } from 'solid-js';
import { createStore } from 'solid-js/store';
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from "@storybook/react";
import IconStatic from "./IconStatic";
import IconThemeable from "./IconThemeable";
storiesOf("Icon", module)
  .add("Static", () => <IconStatic />)
  .add("Themeable", () => <IconThemeable />);
