import {  JSX, Show  } from 'solid-js';
import { createStore } from 'solid-js/store';
// import './deleteModal.css'; //temporary locked
// https://www.w3schools.com/howto/howto_css_delete_modal.asp
export const DeleteModal = (props: { onCancel: () => void; onDelete: () => void }) => {
  const [state, setState] = createStore({ show: false });
  const showModal = () => setState({ show: true });
  const hideModal = () => setState({ show: false });
  return (
    <>
      <button onClick={showModal}>Open Modal</button>
      <Show when={state.show}>
        <div class="modal" style={`display: ${state.show ? 'block' : 'none'}`}>
          <span onClick={hideModal} class="close" title="Close Modal">
            &times;
          </span>

          <form class="modal-content" action="/action_page.php">
            <div class="container">
              <h1>Delete Account</h1>
              <p>Are you sure you want to delete your account?</p>

              <div class="clearfix">
                <button type="button" class="cancelbtn">
                  Cancel
                </button>
                <button type="button" class="deletebtn">
                  Delete
                </button>
              </div>
            </div>
          </form>
        </div>
      </Show>
    </>
  );
};
