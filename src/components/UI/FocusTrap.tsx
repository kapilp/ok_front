import { createFocusTrap } from 'focus-trap';
import { Options as FocusTrapOptions, FocusTrap as FocusTrapReturn } from 'focus-trap';
import { JSX, onCleanup, mergeProps } from 'solid-js';
interface IFocusTrapProps extends JSX.HTMLAttributes<Element> {
  // returnFocusOnDeactivate?: any;
  children: JSX.Element; // must not be array
  active?: boolean;
  paused?: boolean;
  focusTrapOptions?: FocusTrapOptions;
}
// TODO: Cant convert this to solid js
/* eslint-disable react/no-find-dom-node */
export const FocusTrap = (props: IFocusTrapProps) => {
  props = mergeProps(
    {},
    {
      active: true,
      paused: false,
      focusTrapOptions: {},
    },
    props,
  );

  let focusTrap: FocusTrapReturn;
  let focusTrapElement: HTMLElement | null;
  let previouslyFocusedElement: Element | null;

  if (typeof document !== 'undefined') {
    previouslyFocusedElement = document.activeElement;
  }

  const setFocusTrapElement = (el: HTMLElement) => {
    focusTrapElement = el;
    setTimeout(() => {
      if (focusTrapElement) {
        focusTrap = createFocusTrap(focusTrapElement, props.focusTrapOptions);

        if (props.active) {
          focusTrap.activate();
        }
        if (props.paused) {
          focusTrap.pause();
        }
      }
    });
  };
  /*const componentDidUpdate = prevProps => {
    if (prevProps.active && !props.active) {
      const { returnFocusOnDeactivate } = props.focusTrapOptions;
      const returnFocus = returnFocusOnDeactivate || false;
      const config = { returnFocus };
      focusTrap.deactivate(config);
    } else if (!prevProps.active && props.active) {
      focusTrap.activate();
    }
    if (prevProps.paused && !props.paused) {
      focusTrap.unpause();
    } else if (!prevProps.paused && props.paused) {
      focusTrap.pause();
    }
  };*/
  onCleanup(() => {
    focusTrap.deactivate();
    if (props.focusTrapOptions && props.focusTrapOptions.returnFocusOnDeactivate !== false && previouslyFocusedElement && previouslyFocusedElement.focus) {
      previouslyFocusedElement.focus();
    }
  });
  return <div ref={setFocusTrapElement}>{props.children}</div>;
};
