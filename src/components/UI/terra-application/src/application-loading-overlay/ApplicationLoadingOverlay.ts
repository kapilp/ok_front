import { createEffect, mergeProps, useContext } from 'solid-js';

import { v4 as uuidv4 } from 'uuid';
import { ApplicationLoadingOverlayContext } from './ApplicationLoadingOverlayContext';

interface Properties {
  /**
   * A boolean value indicating whether the loading overlay should be visible or not.
   */
  isOpen?: boolean;
  /**
   * A string indicating the background style for the overlay. One of: `dark`, `light`, `clear`.
   */
  backgroundStyle?: 'dark' | 'light' | 'clear';
}
const defaultProps = {
  backgroundStyle: 'clear',
};
export const ApplicationLoadingOverlay = (props: Properties) => {
  props = mergeProps({}, defaultProps, props)
  const idRef = uuidv4();
  const applicationLoadingOverlay = useContext(ApplicationLoadingOverlayContext);
  createEffect(() => {
    const overlayId = idRef;
    if (props.isOpen) {
      applicationLoadingOverlay.show(overlayId, { backgroundStyle });
    } else {
      applicationLoadingOverlay.hide(overlayId);
    }
    return () => {
      applicationLoadingOverlay.hide(overlayId);
    };
  });
  return null;
};
