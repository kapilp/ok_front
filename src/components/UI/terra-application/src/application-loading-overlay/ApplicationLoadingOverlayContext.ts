import { createContext } from 'solid-js';
export type contextShape = {
  /**
   * A function that will present a loading overlay for the given key. The key should
   * be unique (namespace appropriately). If an overlay is already presented for the
   * key, no action is performed.
   * Ex. show(String key, Object data)
   */
  show: (key: string, data: {}) => void;
  /**
   * A function that will remove the loading overlay for the given key. If no overlay
   * is presented, no action is performed.
   * Ex. hide(String key)
   */
  hide: (key: string) => void;
};

export const ApplicationLoadingOverlayContext = createContext<contextShape>();
