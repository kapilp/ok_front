import {  JSX, createEffect, createMemo, createSignal, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Scroll } from '../../../core/scroll/src/Scroll';
import { LoadingOverlay } from '../../../core/overlay/src/LoadingOverlay';
import { OverlayContainer } from '../../../core/overlay/src/OverlayContainer';
import { ApplicationLoadingOverlayContext } from './ApplicationLoadingOverlayContext';
import styles from './ApplicationLoadingOverlayProvider.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The components to be rendered within the context of the ApplicationLoadingOverlayProvider.
   * Components rendered here are able to interact with ApplicationLoadingOverlayProvider through
   * the ApplicationLoadingOverlayContext.
   */
  children?: JSX.Element;
  /**
   * A function to be called with the current ref of the scrollable element rendered within the
   * ApplicationLoadingOverlayProvider.
   */
  ref?: (el: HTMLDivElement) => void;
}
export const ApplicationLoadingOverlayProvider = ({ children, ref, ...customProps }) => {
  const [registeredLoadingOverlays, setRegisteredLoadingOverlays] = createStore<{ [key: string]: any }>({});
  const contextValue = {
    show: (key: string, data: {}) => {
      setRegisteredLoadingOverlays(state => ({
        ...state,
        [`${key}`]: data,
      }));
    },
    hide: (key: string) => {
      setRegisteredLoadingOverlays(state => {
        const newRegisteredLoadingOverlays = { ...state };
        delete newRegisteredLoadingOverlays[key];
        return newRegisteredLoadingOverlays;
      });
    },
  };
  const registeredOverlayKeys = Object.keys(registeredLoadingOverlays);
  const registeredBackgroundStyles = registeredOverlayKeys.map(key => registeredLoadingOverlays[key] && registeredLoadingOverlays[key].backgroundStyle);
  /**
   * If multiple styles of overlay are requested, the strongest requested style is used.
   */
  let overlayBackgroundStyle = 'clear';
  if (registeredBackgroundStyles.includes('dark')) {
    overlayBackgroundStyle = 'dark';
  } else if (registeredBackgroundStyles.includes('light')) {
    overlayBackgroundStyle = 'light';
  }
  const overlay = <LoadingOverlay isRelativeToContainer isAnimated isOpen={!!registeredOverlayKeys.length} backgroundStyle={overlayBackgroundStyle} />;
  let className = cx('container');
  if (customProps.className) {
    className = [className, customProps.className].join(' ');
  }
  return (
    <OverlayContainer {...customProps} className={className} overlay={overlay}>
      <Scroll ref={ref}>
        <ApplicationLoadingOverlayContext.Provider value={contextValue}>{children}</ApplicationLoadingOverlayContext.Provider>
      </Scroll>
    </OverlayContainer>
  );
};
