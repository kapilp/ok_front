import { JSX, splitProps } from 'solid-js';
import { ModalManager } from '../../../framework/modal-manager/src/ModalManager';
import { navigationPromptResolutionOptionsShape } from '../navigation-prompt';
import DisclosureContainer from '../disclosure-manager/_DisclosureContainer';

interface Properties {
  /**
   * The components to be rendered in the body of the ModalManager. These components will receive the
   * disclosure capabilities through the DisclosureManger's context API.
   */
  children: JSX.Element;
  /**
   * The component to render within the Modal above the disclosed content.
   */
  disclosureAccessory: JSX.Element;
  /**
   * The Object (or function that returns an Object) that specifies the messages
   * used to prompt the user when disclosure dismissal occurs when pending state
   * is present. If not provided, the default messaging will be used.
   */
  navigationPromptResolutionOptions: navigationPromptResolutionOptionsShape;
}
export const ModalManagerWithDC = (props: Properties) => {
  const [p, terraModalManagerProps] = splitProps(props, ['navigationPromptResolutionOptions']);
  return (
    <ModalManager
      {...terraModalManagerProps}
      withDisclosureContainer={disclosureContent => (
        <DisclosureContainer navigationPromptResolutionOptions={p.navigationPromptResolutionOptions}>{disclosureContent}</DisclosureContainer>
      )}
    />
  );
};
