/**
 * Generates an Object containing properties used to define messages for NavigationPromptCheckpoint-based user prompting.
 * @param {Object} intl - The react-intl provided intl object used to look up translated string definitions.
 */
export default intl => prompts => {
  let startMessage;
  let content;
  if (prompts.length === 1) {
    startMessage = `Unsaved changes were made to ${prompts[0].description}.`;
  } else {
    startMessage = 'Unsaved changes were made to the following:';
    // We do not currently have access to the prompts internal identifiers.
    /* eslint-disable react/no-array-index-key */
    content = (
      <ul>
        {prompts.map((prompt, index) => (
          <li key={index}>{prompt.description}</li>
        ))}
      </ul>
    );
    /* eslint-enable react/no-array-index-key */
  }
  const endMessage = "Changes will be lost if you don't save them. How do you want to proceed?";
  return {
    title: 'Unsaved Changes',
    startMessage,
    content,
    endMessage,
    buttonOrder: 'rejectFirst',
    emphasizedAction: 'reject',
    acceptButtonText: "Don't Save",
    rejectButtonText: 'Continue Editing',
  };
};
