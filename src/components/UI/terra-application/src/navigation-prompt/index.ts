import getUnsavedChangesPromptOptions from './getUnsavedChangesPromptOptions';
type ObjectShape = {
  /**
   * The title string for the NotificationDialog.
   */
  title?: string;
  /**
   * The startMessage string for the NotificationDialog.
   */
  startMessage?: string;
  /**
   * The content element for the NotificationDialog.
   */
  content?: JSX.Element;
  /**
   * The endMessage string for the NotificationDialog.
   */
  endMessage?: string;
  /**
   * The string for the NotificationDialog's accept button text.
   */
  acceptButtonText?: string;
  /**
   * The string for the NotificationDialog's reject button text.
   */
  rejectButtonText?: string;
  /**
   * The string specifying the emphasized action.
   */
  emphasizedAction?: 'accept' | 'reject' | 'none';
  /**
   * The string specifying the button order.
   */
  buttonOrder?: 'acceptFirst' | 'rejectFirst';
};
type FunctionType = () => {};
type navigationPromptResolutionOptionsShape = ObjectShape | FunctionType;
export { navigationPromptResolutionOptionsShape, getUnsavedChangesPromptOptions };
