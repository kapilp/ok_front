import { JSX, splitProps } from 'solid-js';
import SlidePanelManager from '../../../framework/slide-panel-manager/src/SlidePanelManager';
import { navigationPromptResolutionOptionsShape } from '../navigation-prompt';
import DisclosureContainer from '../disclosure-manager/_DisclosureContainer';
interface Properties {
  /**
   * The components to be rendered in the body of the SlidePanelManager. These components will receive the
   * disclosure capabilities through the DisclosureManger's context API.
   */
  children: JSX.Element;
  /**
   * The desired panel behavior. Either 'squish' or 'overlay'.
   */
  panelBehavior: 'overlay' | 'squish';
  /**
   * The component to render within the panel above the disclosed content.
   */
  disclosureAccessory: JSX.Element;
  /**
   * The Object (or function that returns an Object) that specifies the messages
   * used to prompt the user when disclosure dismissal occurs when pending state
   * is present. If not provided, the default messaging will be used.
   */
  navigationPromptResolutionOptions: navigationPromptResolutionOptionsShape;
}
export const SlidePanelManagerWithDC = (props: Properties) => {
  const [p, terraSlidePanelManagerProps] = splitProps(props, ['navigationPromptResolutionOptions']);

  return (
    <SlidePanelManager
      {...terraSlidePanelManagerProps}
      withDisclosureContainer={disclosureContent => (
        <DisclosureContainer navigationPromptResolutionOptions={p.navigationPromptResolutionOptions}>{disclosureContent}</DisclosureContainer>
      )}
    />
  );
};
// SlidePanelManager.defaultProps = SlidePanelManager.defaultProps;
