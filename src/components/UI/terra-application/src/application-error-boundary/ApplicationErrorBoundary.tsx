import { StatusView, StatusViewVariants } from '../../../core/status-view/src/StatusView';
// import { injectIntl, intlShape } from 'react-intl';
import { Logger } from '../utils/logger/logger';
import {  JSX, createEffect,   } from 'solid-js';
import { createStore } from 'solid-js/store';

interface IApplicationErrorBoundaryProps extends JSX.HTMLAttributes<Element> {
  /**
   * Components to render within the context of the ApplicationErrorBoundary. Exceptions thrown
   * by these components during their render lifecycle will be caught by the ApplicationErrorBoundary.
   */
  children: JSX.Element;
  /**
   * @private
   * Intl object for translations.
   */
  // intl: intlShape, //Todo fix this
}
/**
 * The ApplicationErrorBoundary is designed to catch exceptions that are thrown
 * by its children during render lifecycle. In the event an exception is thrown,
 * a styled status component will be rendered to communicate the exception to the
 * user.
 *
 * Unlike a standard error boundary, the error is not persisted within the
 * ApplicationErrorBoundary's state. The ApplicationErrorBoundary will attempt to
 * render its children each time it is updated. Resetting the ApplicationErrorBoundary
 * by using a key is not necessary.
 */
export const ApplicationErrorBoundary = (props: IApplicationErrorBoundaryProps) => {
  const [state, setState] = createStore({ error: undefined as { message: string } | undefined });
  const resetError = () => {
    if (state.error) {
      setState({ error: undefined });
    }
  };
  // static getDerivedStateFromError(error) {
  //   /**
  //    * When an exception occurs while rendering children, getDerivedStateFromError gets executed. The error
  //    * is returned here and stored within the ApplicationErrorBoundary's state, triggering an update.
  //    */
  //   return { error };
  // }
  createEffect(() => {
    /**
     * After mounting (and each subsequent update), the error state within the ApplicationErrorBoundary
     * is reset.
     *
     * If the ApplicationErrorBoundary was updated due to a caught exception, the componentDidCatch method will
     * execute and the error will be stored in the errorRef. Otherwise, resetting the error state will trigger another update that
     * will  result in the children being rendered (if they were not already successfully rendered).
     */
    resetError();
  });

  function componentDidCatch(error: { message: string }) {
    /**
     * If the ApplicationErrorBoundary was updated due to a caught exception, this lifecycle method will be
     * executed. componentDidCatch executes after componentDidMount/componentDidUpdate, so this is executing
     * after the error state has been reset.
     *
     * The error that was caught is stored in a ref and the error within state is cleared. This causes
     * the ApplicationErrorBoundary to update again to ensure that the StatusView remains presented until the
     * next update occurs.
     */
    Logger.error(error);
    setState({ error: undefined });
  }

  return <>{state.error ? <StatusView variant={StatusViewVariants.ERROR} message={`The system encountered an error: ${state.error.message.toString()}`} /> : props.children}</>;
};
// export default injectIntl(ApplicationErrorBoundary);
