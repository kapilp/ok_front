import { createContext } from "react";
const ActiveMainPageContext = createContext();
const contextShape = {
  parentNavigationKeys: PropTypes.array,
  pageKey: PropTypes.string,
  pageLabel: PropTypes.string,
  pageMetaData: PropTypes.object
};
export default ActiveMainPageContext;
export { contextShape };
