import React from "react";
import ReactDOM from "react-dom";
import WorkspaceContext from "./WorkspaceContext";
import TabContext from "./subcomponents/_TabContext";
const propTypes = {
  /**
   * Key to match with the activeItemKey to handle the display of selection.
   */
  itemKey: PropTypes.string.isRequired,
  /**
   * Text to be displayed on the tab or as it's aria-label.
   */
  label: PropTypes.string.isRequired,
  /**
   * Object to be returned in the onRequestActive.
   */
  metaData: PropTypes.object,
  /**
   * @private
   * The id of the tab.
   */
  id: PropTypes.string,
  /**
   * @private
   * The id of the panel associated to the tab.
   */
  associatedPanelId: PropTypes.string,
  /**
   * @private
   * The indicator whether or not the tab content is active.
   */
  isActive: PropTypes.bool,
  /**
   * @private
   * The html element to be hold the portaled item content.
   */
  portalElement: PropTypes.instanceOf(HTMLElement)
};
const WorkspaceItem: React.SFC<{}> = ({
  id,
  associatedPanelId,
  isActive,
  label,
  render,
  portalElement,
  /**
   * The itemKey and metaData props are not used by the WorkspaceItem component.
   * However, the Workspace component will read those prop values and use those values to
   * build its tab structure.
   */
  itemKey, // eslint-disable-line no-unused-vars
  metaData
}) => {
  const tabContextValue = React.useMemo(
    () => ({
      tabId: id,
      panelId: associatedPanelId,
      label
    }),
    [associatedPanelId, id, label]
  );
  const workspaceContextValue = React.useMemo(
    () => ({
      isActive
    }),
    [isActive]
  );
  return ReactDOM.createPortal(
    <TabContext.Provider value={tabContextValue}>
      <WorkspaceContext.Provider value={workspaceContextValue}>
        {render()}
      </WorkspaceContext.Provider>
    </TabContext.Provider>,
    portalElement
  );
};
export default WorkspaceItem;
