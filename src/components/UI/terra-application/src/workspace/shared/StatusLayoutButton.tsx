import React from "react";
import Button from "terra-button";
const propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func
};
const StatusLayoutButton: React.SFC<{}> = ({ text, onClick }) => (
  <Button onClick={onClick} text={text} variant="neutral" />
);
export default StatusLayoutButton;
