import { JSX, createEffect, createMemo, createSignal, ErrorBoundary, onCleanup, Suspense } from 'solid-js';
import classNames from 'classnames/bind';
import { Base } from '../../../core/base/src/Base';
import { ThemeProvider } from '../../../framework/theme-provider/src/ThemeProvider';
import { ActiveBreakpointProvider } from '../../../core/breakpoints/src';
import { ThemeContextProvider } from '../../../framework/theme-context/src/ThemeContextProvider';
// import { ApplicationErrorBoundary } from '../application-error-boundary/ApplicationErrorBoundary';
import { StatusView, StatusViewVariants } from '../../../core/status-view/src/StatusView';
// import { ApplicationLoadingOverlayProvider } from '../application-loading-overlay/ApplicationLoadingOverlayProvider';
import { ApplicationLoadingOverlay } from '../application-loading-overlay/ApplicationLoadingOverlay';
// import { ApplicationStatusOverlayProvider } from '../application-status-overlay/ApplicationStatusOverlayProvider';
// import { NavigationPromptCheckpoint } from '../../../framework/navigation-prompt/src/NavigationPromptCheckpoint';
import { I18nContext } from '../../../../react-i18next/src/context';
import i18n from './i18nConfig';
import { getBrowserLocale } from './private/getBrowserLocale';
import { useTestOverrides } from './private/useTestOverrides';
import styles from './ApplicationBase.module.scss';
/* global TERRA_THEME_CONFIG */
const cx = classNames.bind(styles);
// const browserLocale = getBrowserLocale();
// We only need to retrieve the root theme and root theme name once for the life of the application.
const themeConfig = typeof TERRA_THEME_CONFIG !== 'undefined' ? TERRA_THEME_CONFIG : undefined;
const rootThemeName = themeConfig?.theme ? themeConfig.theme : 'terra-default-theme';

interface Properties {
  /**
   * The components to render within ApplicationBase.
   */
  children?: JSX.Element | JSX.Element[];
  /**
   * The locale name to be used to load translated messages.
   * If the `locale` prop is not provided, the preferred language from the browser will be used.
   */
  locale?: string;
  /**
   * Custom translations for the current locale.
   */

  customTranslatedMessages?: (
    props: {},
    propName: string,
    componentName: string,
  ) => {
    // TODO
    // if (!props[propName]) {
    //   return null;
    // }
    // if (
    //   Object.keys(props[propName]).length !== 0 &&
    //   props.locale === undefined
    // ) {
    //   return new Error(
    //     `Missing locale prop for ${propName} in ${componentName} props`
    //   );
    // }
    // return null;
  };
  /**
   * The component to render while the translation files are being retrieved.
   * NOTE?: Absolutely no locale-dependent logic should be
   * utilized in this placeholder.
   */
  translationsLoadingPlaceholder?: JSX.Element;
  /**
   * The name of the theme to apply to the application using terra-theme-provider.
   */
  themeName?: string;
  /**
   * By default, the elements rendered by ApplicationBase are fit to the Application's parent using 100% height.
   * If `fitToParentIsDisabled` is provided, the Application will render at its intrinsic content height and
   * overflow potentially overflow its parent.
   */
  fitToParentIsDisabled?: boolean;
  /**
   * By default, NavigationPrompts rendered within ApplicationBase will cause the user to be prompted during
   * the window's beforeUnload event. If `unloadPromptIsDisabled` is provided, the user will **not** be prompted
   * before continuing with the unload event, even if NavigationPrompts are present.
   */
  unloadPromptIsDisabled?: boolean;
}
export const ApplicationBase = (props: Properties) => {
  const [registeredPromptsRef, setRegisteredPromptsRef] = createSignal<[]>();

  function onBeforeUnload(event: Event) {
    if (registeredPromptsRef() && registeredPromptsRef().length) {
      event.preventDefault();
      // Chrome requires returnValue to be set to present the confirmation dialog
      event.returnValue = ''; // eslint-disable-line no-param-reassign
      // For this prompt, ApplicationBase is limited to browser-defaulted messaging.
      return '';
    }
    return undefined;
  }
  createEffect(() => {
    if (props.unloadPromptIsDisabled && registeredPromptsRef()) {
      return undefined;
    }
    window.addEventListener('beforeunload', onBeforeUnload);
  });
  onCleanup(() => {
    window.removeEventListener('beforeunload', onBeforeUnload);
  });
  const { localeOverride } = useTestOverrides(); // Allows us to test deployed applications in different locales.
  const theme = () => ({
    // If the theme class name is undefined or an empty string, that indicates we have the root theme and should apply the root theme name.
    name: props.themeName || rootThemeName,
    className: props.themeName,
  });
  return (
    <div data-terra-application-base className={cx('application-base', { fill: !props.fitToParentIsDisabled })}>
      <ThemeProvider themeName={props.themeName}>
        <ThemeContextProvider theme={theme}>
          <Base
            // customMessages={props.customTranslatedMessages}
            // translationsLoadingPlaceholder={props.translationsLoadingPlaceholder}
            //locale={localeOverride || props.locale || browserLocale}
          >
            {/*<ErrorBoundary fallback={(err: Error) => <StatusView variant={StatusViewVariants.ERROR} message={`The system encountered an error: ${err.message}`} />}>*/}
            {/* <I18nContext.Provider value={i18n}> */}
              <ActiveBreakpointProvider>
                {/*<NavigationPromptCheckpoint onPromptChange={registeredPrompts => {setRegisteredPromptsRef(registeredPrompts);}}>*/}
                {/*<ApplicationLoadingOverlayProvider>*/}
                {/*<ApplicationStatusOverlayProvider>*/}
                {/*<Suspense fallback={<ApplicationLoadingOverlay isOpen />}>{props.children}</Suspense>*/}
                {props.children}
                {/*</ApplicationStatusOverlayProvider>*/}
                {/*</ApplicationLoadingOverlayProvider>*/}
                {/*</NavigationPromptCheckpoint>*/}
              </ActiveBreakpointProvider>
            {/* </I18nContext.Provider> */}
            {/*</ErrorBoundary>*/}
          </Base>
        </ThemeContextProvider>
      </ThemeProvider>
    </div>
  );
};
