import { JSX, useContext } from 'solid-js';
import React, { useEffect, useRef, useMemo } from 'react';
// import { injectIntl } from 'react-intl';
import { DisclosureManagerContext } from '../../../framework/disclosure-manager/src/DisclosureManagerContext';
import { DisclosureManagerDelegate } from '../../../framework/disclosure-manager/src/DisclosureManagerDelegate';
import { ApplicationLoadingOverlayProvider } from '../application-loading-overlay/ApplicationLoadingOverlayProvider';
import { ApplicationStatusOverlayProvider } from '../application-status-overlay/ApplicationStatusOverlayProvider';
import { navigationPromptResolutionOptionsShape, getUnsavedChangesPromptOptions } from '../navigation-prompt';
import { NavigationPromptCheckpoint } from '../../../framework/navigation-prompt/src/NavigationPromptCheckpoint';
import { ApplicationErrorBoundary } from '../application-error-boundary/ApplicationErrorBoundary';
import { addCallback, removeCallback } from './_disclosureCallbacks';

interface Properties {
  /**
   * The components to render within the context of the DisclosureContainer.
   */
  children?: JSX.Element;
  /**
   * The Object (or function that returns an Object) that specifies the messages
   * used to prompt the user when disclosure dismissal occurs when pending state
   * is present.
   */
  navigationPromptResolutionOptions?: navigationPromptResolutionOptionsShape;
  //intl
}
//injectIntl
export const DisclosureContainer = (props: Properties) => {
  const disclosureManager = useContext(DisclosureManagerContext);
  const promptCheckpointRef = useRef();
  const customRegisterDismissCheckRef = useRef();

  const overrideDisclosureManagerContext = useMemo(
    () =>
      DisclosureManagerDelegate.clone(disclosureManager, {
        registerDismissCheck: check => {
          customRegisterDismissCheckRef.current = check;
          /**
           * Return Promise to align with DisclosureManager's default implementation.
           */
          return Promise.resolve();
        },
      }),
    [disclosureManager],
  );
  const defaultPromptOptions = useMemo(() => getUnsavedChangesPromptOptions(intl), [intl]);
  useEffect(() => {
    const callback = disclosureManager.goBack || disclosureManager.closeDisclosure;
    addCallback(callback);
    return () => {
      removeCallback(callback);
    };
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    disclosureManager.registerDismissCheck(() => {
      if (customRegisterDismissCheckRef.current) {
        return customRegisterDismissCheckRef.current();
      }
      return new Promise((resolve, reject) => {
        if (!promptCheckpointRef.current) {
          resolve();
          return;
        }
        promptCheckpointRef.current.resolvePrompts(props.navigationPromptResolutionOptions || defaultPromptOptions).then(resolve, reject);
      });
    });
  }, [defaultPromptOptions, disclosureManager, props.navigationPromptResolutionOptions]);
  return (
    <DisclosureManagerContext.Provider value={overrideDisclosureManagerContext}>
      <ApplicationErrorBoundary>
        <ApplicationLoadingOverlayProvider>
          <ApplicationStatusOverlayProvider>
            <NavigationPromptCheckpoint ref={promptCheckpointRef}>{props.children}</NavigationPromptCheckpoint>
          </ApplicationStatusOverlayProvider>
        </ApplicationLoadingOverlayProvider>
      </ApplicationErrorBoundary>
    </DisclosureManagerContext.Provider>
  );
};
