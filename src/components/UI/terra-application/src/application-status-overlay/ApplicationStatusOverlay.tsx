import { JSX, createEffect, mergeProps, useContext } from 'solid-js';
import { IButtonProps } from '../../../core/button/src/Button';
import { v4 as uuidv4 } from 'uuid';
import { ApplicationStatusOverlayContext } from './ApplicationStatusOverlayContext';
interface Properties {
  /**
   * An array of objects containing terra-button properties. A key attribute is required for each object.
   * This array is used to render buttons in the bottom section.
   * Example:`[{ text: 'Refresh Application', key: 1, variant: 'neutral', onClick: myRefreshFunction}]`
   */
  // eslint-disable-next-line react/forbid-foreign-prop-types
  buttonAttrs?: IButtonProps[];
  /**
   * The descriptive text, displayed under the title.
   */
  message?: string;
  /**
   * Sets the glyph and title using a pre-baked variant. One of the following: `no-data`,
   * `no-matching-results`, `not-authorized`, or `error`
   */
  variant: 'no-data' | 'no-matching-results' | 'not-authorized' | 'error';
}
const defaultProps = {
  buttonAttrs: [],
  message: undefined,
  variant: undefined,
};
export const ApplicationStatusOverlay = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  const idRef = uuidv4();
  const applicationStatusOverlay = useContext(ApplicationStatusOverlayContext);

  createEffect(() => {
    applicationStatusOverlay.show(idRef, {
      buttonAttrs: props.buttonAttrs,
      message: props.message,
      variant: props.variant,
    });
  });

  createEffect(() => () => {
    applicationStatusOverlay.hide(idRef);
  });
  return null;
};
