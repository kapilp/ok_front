import {  JSX, createEffect, createMemo, createSignal, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Scroll } from '../../../core/scroll/src/Scroll';
import { StatusView } from '../../../core/status-view/src/StatusView';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { ApplicationStatusOverlayContext } from './ApplicationStatusOverlayContext';
import styles from './ApplicationStatusOverlayProvider.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The components to be rendered within the context of the ApplicationStatusOverlayProvider.
   * Components rendered here are able to interact with ApplicationStatusOverlayProvider through
   * the ApplicationStatusOverlayContext.
   */
  children?: JSX.Element;
  /**
   * A function to be called with the current ref of the scrollable element rendered within the
   * ApplicationStatusOverlayProvider.
   */
  ref?: (el: HTMLDivElement) => void;
}
export const ApplicationStatusOverlayProvider = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['children', 'ref']);

  const [registeredStatusOverlay, setRegisteredStatusOverlay] = createStore<{ [key: string]: any }>({});
  const [containerRef, setContainerRef] = createSignal<HTMLDivElement>();
  const contextValue = {
    show: (key: string, data: {}) => {
      setRegisteredStatusOverlay({ [`${key}`]: data });
    },
    hide: (key: string) => {
      setRegisteredStatusOverlay({ key: undefined });
    },
  };
  const disableContainerChildrenFocus = () => {
    if (containerRef()) {
      containerRef().setAttribute('inert', '');
    }
  };
  const enableContainerChildrenFocus = () => {
    if (containerRef()) {
      containerRef().removeAttribute('inert');
      containerRef().removeAttribute('aria-hidden');
    }
  };
  const registeredStatusOverlayKeys = createMemo(() => Object.keys(registeredStatusOverlay));
  // In the event when multiple ApplicationStatusOverlay's are provided, the last rendered wins
  const lastRegisteredStatusOverlayKey = createMemo(() => registeredStatusOverlayKeys().length && registeredStatusOverlayKeys()[registeredStatusOverlayKeys().length - 1]);
  const registeredButtonAttrs = createMemo(() => registeredStatusOverlay[lastRegisteredStatusOverlayKey()]?.buttonAttrs);
  const registeredMessage = createMemo(() => registeredStatusOverlay[lastRegisteredStatusOverlayKey()]?.message);
  const registeredVariant = createMemo(() => registeredStatusOverlay[lastRegisteredStatusOverlayKey()]?.variant);

  createEffect(() => {
    if (registeredStatusOverlayKeys().length) {
      disableContainerChildrenFocus();
    } else {
      enableContainerChildrenFocus();
    }
  });

  const theme = useContext(ThemeContext);
  const statusView = (
    <>
      {registeredStatusOverlayKeys.length > 0 && (
        <StatusView buttonAttrs={registeredButtonAttrs()} className={cx('status-view', theme.className)} message={registeredMessage()} variant={registeredVariant()} />
      )}
    </>
  );
  return (
    <div {...customProps} className={classNames(cx('container'), customProps.className)}>
      {statusView}
      <div data-status-overlay-container-content ref={setContainerRef} className={cx('container-content')}>
        <Scroll ref={p.ref}>
          <ApplicationStatusOverlayContext.Provider value={contextValue}>{p.children}</ApplicationStatusOverlayContext.Provider>
        </Scroll>
      </div>
    </div>
  );
};
