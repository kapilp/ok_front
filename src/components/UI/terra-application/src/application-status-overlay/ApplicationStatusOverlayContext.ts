import { createContext } from 'solid-js';
export type contextShape = {
  /**
   * A function that will present a status view for the given key.
   * The key should be unique (namespace appropriately).
   * If a status view is already presented for the key, no action is performed.
   * Ex. show(String key, Object data)
   */
  show: (key: string, data: {}) => void;
  /**
   * A function that will remove the status view for the given key.
   * If no status view is presented, no action is performed.
   * Ex. hide(String key)
   */
  hide: (key: string) => void;
};
export const ApplicationStatusOverlayContext = createContext<contextShape>();
