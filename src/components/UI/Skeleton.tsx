// https://github.com/dylanblokhuis/svelte-loading-skeleton
import { JSX, onCleanup } from 'solid-js';
import classNamesBind from 'classnames/bind';
import styles from './Skeleton.module.scss';
const cx = classNamesBind.bind(styles);
export const Skeleton = (props: { width?: string; height?: string; borderRadius?: string; baseColor?: string; highlightColor?: string; animationLength?: string }) => {
  return (
    <div
      className={styles.skeleton}
      style={`height: ${props.height || '50px'};
          width: ${props.width || '100%'};
          border-radius: ${props.borderRadius || '4px'};
          --baseColor: ${props.baseColor || 'rgb(238, 238, 238)'};
          --highlightColor: ${props.highlightColor || 'rgb(245, 245, 245)'};
          --animationLength: ${props.animationLength || '1.2s'}`}
    />
  );
};
