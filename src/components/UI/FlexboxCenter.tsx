// https://github.com/aush/react-center
// His all Repositories is various css utilities e.g. react-stack is great utilty
import { JSX, Component, createMemo, mergeProps, splitProps } from 'solid-js';
// import { prefix } from 'inline-style-prefixer'; // goober automatically adding vendor prefixes
import { css } from 'goober';
import classNames from 'classnames';

interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  direction: 'row' | 'column';
}
export const FlexboxCenter: Component<Properties> = props => {
  props = mergeProps({}, { direction: 'row' }, props);
  const [p, customProps] = splitProps(props, ['children', 'className', 'direction']);

  const flexContainerStyle = createMemo(
    () => css`
      align-content: center;
      align-items: center;
      box-sizing: border-box;
      display: flex;
      flex-direction: ${p.direction};
      flex-wrap: nowrap;
      justify-content: center;
    `,
  );

  return (
    <div {...customProps} className={classNames(flexContainerStyle(), p.className)}>
      {p.children}
    </div>
  );
};
