// https://github.com/wellyshen/react-cool-onclickoutside
// His all Repositories is cool like this component.
// very helpful:
// https://github.com/wellyshen/react-cool-dimensions
// https://github.com/wellyshen/use-web-animations
import canUsePassiveEvents from './canUsePassiveEvents';
import {  createEffect, onCleanup  } from 'solid-js';
import { createStore } from 'solid-js/store';

export const DEFAULT_IGNORE_CLASS = 'ignore-onclickoutside';

export interface Callback<T extends Event = Event> {
  (event: T): void;
}

type El = HTMLElement;
type Refs = El[];
export interface Options {
  refs?: Refs;
  disabled?: boolean;
  eventTypes?: string[];
  excludeScrollbar?: boolean;
  ignoreClass?: string;
}
interface Return {
  (element: El | null): void;
}

const hasIgnoreClass = (e: any, ignoreClass: string): boolean => {
  let el = e.target;

  while (el) {
    if (el.classList?.contains(ignoreClass)) return true;
    el = el.parentElement;
  }

  return false;
};

const clickedOnScrollbar = (e: MouseEvent): boolean => document.documentElement.clientWidth <= e.clientX || document.documentElement.clientHeight <= e.clientY;

const getEventOptions = (type: string): { passive: boolean } | boolean => (type.includes('touch') && canUsePassiveEvents() ? { passive: true } : false);

export const useOnclickOutside = (
  callback: Callback,
  { refs: refsOpt, disabled = false, eventTypes = ['mousedown', 'touchstart'], excludeScrollbar = false, ignoreClass = DEFAULT_IGNORE_CLASS }: Options = {},
): Return => {
  const [state, setState] = createStore({ refs: [] as Refs });

  const ref: Return = el => {
    setState('refs', 0, el);
  };

  const listener = (e: any): void => {
    if (hasIgnoreClass(e, ignoreClass)) return;

    const refs = refsOpt || state.refs;
    const els: El[] = [];
    refs.forEach(el => {
      if (el) els.push(el);
    });

    if (excludeScrollbar && clickedOnScrollbar(e)) return;
    if (!els.length || !els.every(el => !el.contains(e.target))) return;

    callback(e);
  };

  const removeEventListener = (): void => {
    eventTypes.forEach(type => {
      // @ts-ignore
      document.removeEventListener(type, listener, getEventOptions(type));
    });
  };
  createEffect(() => {
    if (!refsOpt?.length && !state.refs.length) return;

    if (disabled) {
      removeEventListener();
      return;
    }

    eventTypes.forEach(type => {
      document.addEventListener(type, listener, getEventOptions(type));
    });
  });
  onCleanup(() => {
    removeEventListener();
  });

  return ref;
};
