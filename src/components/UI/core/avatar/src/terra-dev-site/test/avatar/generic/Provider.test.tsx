import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Generic } from '../../../../index';
export default () => <Generic variant="provider" id="provider" alt="provider" color="seven" />;
