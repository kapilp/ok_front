import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Avatar from '../../../../index';
import exampleAvatarImage from '../../../assets/170x251.jpg';
export default () => <Avatar image={exampleAvatarImage} alt="User" id="image-avatar" initials="JS" />;
