import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Avatar from '../../../../index';
export default () => <Avatar id="one-initial-avatar" initials="J" alt="John" />;
