import { JSX, Component, mergeProps, Show, splitProps } from 'solid-js';
import { createStore } from 'solid-js/store';
import Avatar from "../../../../index";
import exampleAvatarImage from "../../../assets/150x150.jpg";
import exampleAvatarImage2 from "../../../assets/200x133.jpg";
import exampleAvatarImage3 from "../../../assets/170x251.jpg";
let initials = "JD";
let image = exampleAvatarImage;
let alt = "placeholder";
const variant = "user";
const ariaLabel = "user";
let id = "image-avatar";
export const UpdateAvatar: Component = (props: {}) => {
  forceUpdate: any;
  constructor(props) {
    super(props);
    altButtonHandle = altButtonHandle.bind(this);
    imageButtonHandle = imageButtonHandle.bind(this);
    initialsButtonHandle = initialsButtonHandle.bind(this);
    idButtonHandle = idButtonHandle.bind(this);
    imageToggleButtonHandle = imageToggleButtonHandle.bind(this);
  }
  altButtonHandle() {
    if (alt === "placeholder") {
      alt = "standin";
    } else {
      alt = "placeholder";
    }
    forceUpdate();
  }
  imageButtonHandle() {
    if (image === exampleAvatarImage) {
      image = exampleAvatarImage2;
    } else if (image === exampleAvatarImage2) {
      image = exampleAvatarImage3;
    } else {
      image = exampleAvatarImage;
    }
    forceUpdate();
  }
  initialsButtonHandle() {
    if (initials === "JD") {
      initials = "LI";
    } else {
      initials = "JD";
    }
    forceUpdate();
  }
  idButtonHandle() {
    if (id === "image-avatar") {
      id = "avatar-image";
    } else {
      id = "image-avatar";
    }
    forceUpdate();
  }
  imageToggleButtonHandle() {
    if (image === null) {
      image = exampleAvatarImage;
    } else {
      image = null;
    }
    forceUpdate();
  }
  render() {
    return (
      <div>
        <Avatar
          id={id}
          image={image}
          alt={alt}
          variant={variant}
          aria-label={ariaLabel}
          initials={initials}
        />
        <br />
        <button type="button" onClick={altButtonHandle} id="alt">
          Alt Text
        </button>
        <button type="button" onClick={imageButtonHandle} id="image">
          Image
        </button>
        <button type="button" onClick={initialsButtonHandle} id="initials">
          Initials
        </button>
        <button type="button" onClick={idButtonHandle} id="id">
          Custom Prop (id)
        </button>
        <br />
        <p>
          The buttons update one of the Avatar&apos;s props then force it to
          reload
        </p>
        <button
          type="button"
          onClick={imageToggleButtonHandle}
          id="image-toggle"
        >
          Toggle Image
        </button>
      </div>
    );
  }
}
export default UpdateAvatar;
