import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Facility } from '../../../../index';
import exampleAvatarImage from '../../../assets/150x150.jpg';
export default () => <Facility id="image-facility" image={exampleAvatarImage} alt="facility" />;
