import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Generic } from '../../../../index';
export default () => <Generic id="shared-user" variant="shared-user" alt="shared user" color="eight" />;
