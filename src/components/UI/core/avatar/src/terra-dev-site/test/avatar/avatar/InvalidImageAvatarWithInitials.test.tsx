import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Avatar from '../../../../index';
export default () => <Avatar image="invalid-image-url" initials="JD" alt="John Doe" id="invalid-image-avatar" />;
