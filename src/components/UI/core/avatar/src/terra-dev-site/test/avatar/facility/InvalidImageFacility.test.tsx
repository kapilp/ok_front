import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Facility } from '../../../../index';
export default () => <Facility image="invalid-image-url" alt="Facility" id="invalid-image-facility" />;
