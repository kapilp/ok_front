import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Avatar from '../../../../index';
export default () => <Avatar id="two-initials-avatar" initials="JSS" alt="Joe S Shane" color="two" />;
