import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import image from '../../../assets/200x133.jpg';
import { Facility } from '../../../../index';
export const FacilitySize = () => <Facility image={image} alt="Thailand" size="2em" />;
