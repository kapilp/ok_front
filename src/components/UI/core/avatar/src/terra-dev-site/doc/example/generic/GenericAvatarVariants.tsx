import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { Generic } from '../../../../index';
import { GenericAvatarVariantsWrapper } from './GenericAvatarVariantsWrapper';
interface Properties {
  color: string;
  size: string;
  variant: string;
}
export const GenericAvatarVariants = () => (
  <GenericAvatarVariantsWrapper>
    {(props: Properties) => <Generic variant={props.variant} alt={props.variant} color={props.color} size={props.size} />}
  </GenericAvatarVariantsWrapper>
);
