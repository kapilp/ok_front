import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Avatar } from '../../../../variants/Avatar';

export const AvatarIsDeceased = () => <Avatar alt="Patient #1" isDeceased initials="JS" />;
