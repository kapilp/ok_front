import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Facility } from '../../../../index';
import { ColorVariantsWrapper } from '../common/ColorVariantsWrapper';

export const FacilityColorVariants = () => <ColorVariantsWrapper>{props => <Facility alt="Lima" color={props.color} />}</ColorVariantsWrapper>;
