import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Avatar } from '../../../../variants/Avatar';

export const AvatarUser = () => <Avatar alt="The Last Airbender" initials="JS" />;
