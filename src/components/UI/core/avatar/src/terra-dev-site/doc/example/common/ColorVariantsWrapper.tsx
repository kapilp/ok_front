import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

export const ColorVariantsWrapper = (props: {}) => {
  const [state, setState] = createStore({ colorVariant: 'auto' });

  function handleOnSelect(event) {
    setState({ [event.target.name]: event.target.value });
  }

  return (
    <div>
      {/*<WrappedComponent color={state.colorVariant} {...props} />*/}
      {props.children({ color: state.colorVariant })}
      <p>
        <label htmlFor="colorVariant">Select a color variant:</label>
      </p>
      <select id="colorVariant" name="colorVariant" value={state.colorVariant} onChange={handleOnSelect}>
        <option value="auto">Auto</option>
        <option value="neutral">Neutral</option>
        <option value="one">One</option>
        <option value="two">Two</option>
        <option value="three">Three</option>
        <option value="four">Four</option>
        <option value="five">Five</option>
        <option value="six">Six</option>
        <option value="seven">Seven</option>
        <option value="eight">Eight</option>
        <option value="nine">Nine</option>
        <option value="ten">Ten</option>
      </select>
    </div>
  );
};
