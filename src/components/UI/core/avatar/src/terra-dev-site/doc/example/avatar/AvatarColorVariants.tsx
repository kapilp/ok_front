import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ColorVariantsWrapper } from '../common/ColorVariantsWrapper';
import { Avatar } from '../../../../variants/Avatar';

export const AvatarColorVariants = () => <ColorVariantsWrapper>{props => <Avatar alt="Joe Shane" initials="JS" color={props.color} />}</ColorVariantsWrapper>;
