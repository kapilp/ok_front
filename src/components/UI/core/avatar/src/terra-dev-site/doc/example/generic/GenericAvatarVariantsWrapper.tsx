import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
export const GenericAvatarVariantsWrapper = (props: { children: (p: {}) => JSX.Element }) => {
  const [state, setState] = createStore({
    colorVariant: 'auto',
    size: undefined,
    variant: 'single-user',
  });

  function handleOnSelect(event) {
    setState({ [event.target.name]: event.target.value });
  }

  return (
    <div>
      {/*<WrappedComponent color={state.colorVariant} variant={state.variant} size={state.size}  {...props} />*/}
      {props.children({ color: state.colorVariant, variant: state.variant, size: state.size })}
      <p>
        <label htmlFor="variant">Select a generic variant type:</label>
      </p>
      <select id="variant" name="variant" value={state.variant} onChange={handleOnSelect}>
        <option value="single-user">Single-User</option>
        <option value="shared-user">Shared-User</option>
        <option value="provider">Provider</option>
      </select>
      <p>
        <label htmlFor="colorVariant">Select a color variant:</label>
      </p>
      <select id="colorVariant" name="colorVariant" value={state.colorVariant} onChange={handleOnSelect}>
        <option value="auto">Auto</option>
        <option value="neutral">Neutral</option>
        <option value="one">One</option>
        <option value="two">Two</option>
        <option value="three">Three</option>
        <option value="four">Four</option>
        <option value="five">Five</option>
        <option value="six">Six</option>
        <option value="seven">Seven</option>
        <option value="eight">Eight</option>
        <option value="nine">Nine</option>
        <option value="ten">Ten</option>
      </select>
      <p>
        <label htmlFor="size">Select a size:</label>
      </p>
      <select id="size" name="size" value={state.size} onChange={handleOnSelect}>
        <option value="1em">Default</option>
        <option value="2em">2em</option>
        <option value="3em">3em</option>
      </select>
    </div>
  );
};
