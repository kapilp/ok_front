import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import exampleAvatarImage from '../../../assets/150x150.jpg';
import { Avatar } from '../../../../variants/Avatar';
export const AvatarImage = () => <Avatar image={exampleAvatarImage} initials="JS" alt="Deep Space" />;
