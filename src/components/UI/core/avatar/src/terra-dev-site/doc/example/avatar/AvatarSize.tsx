import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import exampleAvatarImage from '../../../assets/150x150.jpg';
import { Avatar } from '../../../../variants/Avatar';
export const AvatarSize = () => <Avatar alt="User" image={exampleAvatarImage} size="2em" initials="JS" />;
