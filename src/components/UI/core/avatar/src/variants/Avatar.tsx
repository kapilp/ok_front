import { AVATAR_VARIANTS, GenerateImage, GenerateInitials, setColor } from '../common/AvatarUtils';
import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../../framework/theme-context/src/ThemeContext';
import styles from '../common/Avatar.module.scss';
const cx = classNamesBind.bind(styles);

interface IAvatarProps extends JSX.HTMLAttributes<Element> {
  /**
   * Specifies the alternative text for the image.
   */
  alt: string;
  /**
   * Sets the background color. Defaults to `auto`. Accepted color variants are theme specific.
   * One of: `'auto'`, `'neutral'`, `'one'`, `'two'`, `'three'`, `'four'`, `'five'`, `'six'`, `'seven'`, `'eight'`, `'nine'`, `'ten'`.
   */
  color?: 'auto' | 'neutral' | 'one' | 'two' | 'three' | 'four' | 'five' | 'six' | 'seven' | 'eight' | 'nine' | 'ten';

  /**
   * Value used for the hash function when color is set to `auto`. If not provided, hash function utilizes alt.
   */
  hashValue?: string;
  /**
   * The image to display.
   */
  image?: string;
  /**
   * One or two letters to display.
   */
  initials: string;
  /**
   * Whether to hide avatar from the accessibility tree.
   */
  isAriaHidden?: boolean;
  /**
   * Whether the person is deceased. Overrides any color variant.
   */
  isDeceased?: boolean;
  /**
   * Overrides the default size.
   */
  size?: string;
}
const defaultProps = {
  color: 'auto',
  hashValue: undefined,
  image: undefined,
  isAriaHidden: false,
  isDeceased: false,
  size: undefined,
};

type AvatarState = {
  fallback: boolean;
};
export const Avatar = (props: IAvatarProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  //context: any;
  const [state, setState] = createStore({
    fallback: false,
  } as AvatarState);

  const handleFallback = () => {
    setState({ fallback: true });
  };

  //const theme = context;

  const avatarParams = {
    image: props.image,
    alt: props.alt,
    isAriaHidden: props.isAriaHidden,
    variant: AVATAR_VARIANTS.USER,
    handleFallback: handleFallback,
    initials: props.initials.length > 2 ? props.initials.slice(0, 2) : props.initials,
  };

  const [extracted, customProps] = splitProps(props, ['alt', 'color', 'hashValue', 'image', 'initials', 'isAriaHidden', 'isDeceased', 'size']);
  /* eslint-disable react/forbid-dom-props */
  return (
    <div
      {...customProps}
      className={classNames(
        cx(
          'avatar',
          setColor(props.alt, props.color, props.hashValue),
          { 'fallback-icon': state.fallback },
          { image: Boolean(props.image) },
          { 'is-deceased': props.isDeceased },
          theme.className,
        ),
        customProps.className,
      )}
      style={props.size ? { ['font-size']: props.size, ...customProps.style } : customProps.style}
    >
      {props.image ? <GenerateImage {...avatarParams} /> : <GenerateInitials {...avatarParams} />}
    </div>
  );
  /* eslint-enable react/forbid-dom-props */
};
