import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { setColor } from '../common/AvatarUtils';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../../framework/theme-context/src/ThemeContext';
import styles from '../common/Avatar.module.scss';
const cx = classNamesBind.bind(styles);
enum GENERIC_VARIANTS {
  SINGLE_USER = 'single-user',
  SHARED_USER = 'shared-user',
  PROVIDER = 'provider',
}
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Specifies the alternative text for the image.
   */
  alt: string;
  /**
   * Sets the background color. Defaults to `auto`. Accepted color variants are theme specific.
   * One of: `'auto'`, `'neutral'`, `'one'`, `'two'`, `'three'`, `'four'`, `'five'`, `'six'`, `'seven'`, `'eight'`, `'nine'`, `'ten'`.
   */
  color?: 'auto' | 'neutral' | 'one' | 'two' | 'three' | 'four' | 'five' | 'six' | 'seven' | 'eight' | 'nine' | 'ten';

  /**
   * Value used for the hash function when color is set to `auto`. If not provided, hash function utilizes alt.
   */
  hashValue?: string;
  /**
   * Whether to hide avatar from the accessibility tree.
   */
  isAriaHidden?: boolean;
  /**
   * Overrides the default size.
   */
  size?: string;
  /**
   * Sets the Generic Avatar type to One of the following variants `single-user`, `shared-user`, or `provider`.
   */
  variant: GENERIC_VARIANTS;
}
const defaultProps = {
  color: 'auto',
  hashValue: undefined,
  isAriaHidden: false,
  size: undefined,
  variant: GENERIC_VARIANTS.SINGLE_USER,
};
export const Generic = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['alt', 'color', 'hashValue', 'isAriaHidden', 'size', 'variant']);
  const theme = useContext(ThemeContext);
  const colorVariant = () => setColor(props.alt, props.color, props.hashValue);
  const customStyles = () => (props.size ? { ['font-size']: p.size, ...customProps.style } : customProps.style);
  const GenericUserClassNames = () => classNames(cx('avatar', `${colorVariant()}`, theme.className), customProps.className);
  const genericIconClassNames = () => {
    let genericIconClassNames = cx(['icon', 'user']);
    if (props.variant === GENERIC_VARIANTS.SHARED_USER) {
      genericIconClassNames = cx(['icon', GENERIC_VARIANTS.SHARED_USER]);
    } else if (props.variant === GENERIC_VARIANTS.PROVIDER) {
      genericIconClassNames = cx(['icon', GENERIC_VARIANTS.PROVIDER]);
    }
    return genericIconClassNames;
  };

  /* eslint-disable react/forbid-dom-props */
  return (
    <div {...customProps} className={GenericUserClassNames()} style={customStyles()}>
      <span className={genericIconClassNames()} role="img" aria-label={props.alt} alt={p.alt} aria-hidden={p.isAriaHidden} />
    </div>
  );
  /* eslint-enable react/forbid-dom-props */
};
export { GENERIC_VARIANTS };
