import { JSX, Component, mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import Card from './Card';
import { DynamicGrid } from '../../../DynamicGrid';
import { Region } from '../../../Region';

const template = {
  'grid-template-columns': '3fr 1fr 3fr',
  'grid-template-rows': 'auto auto auto auto',
};
const leftGutter = {
  'grid-column-start': 1,
  'grid-row-start': 1,
  'grid-row-end': 5,
};
const rightGutter = {
  'grid-column-start': 3,
  'grid-row-start': 1,
  'grid-row-end': 5,
};
const notification = {
  'grid-column-start': 2,
  'grid-column-end': 3,
  'grid-row-start': 1,
};
const region1 = {
  'grid-column-start': 2,
  'grid-row-start': 2,
  style: {
    padding: '0 5px 5px 5px',
  },
};
const region2 = {
  'grid-column-start': 2,
  'grid-row-start': 3,
  style: {
    padding: '5px',
  },
};
const region3 = {
  name: 'r3',
  'grid-column-start': 2,
  'grid-row-start': 4,
  style: {
    padding: '5px 5px 0',
  },
};
export const DashboardLayout = () => (
  <DynamicGrid defaultTemplate={template}>
    <Region defaultPosition={leftGutter}>
      <Card>left gutter</Card>
    </Region>
    <Region defaultPosition={notification}>
      <Card>notification</Card>
    </Region>
    <Region defaultPosition={rightGutter}>
      <Card>right gutter</Card>
    </Region>
    <Region defaultPosition={region1}>
      <Card>r1</Card>
    </Region>
    <Region defaultPosition={region2}>
      <Card>r2</Card>
    </Region>
    <Region defaultPosition={region3}>
      <Card>r3</Card>
    </Region>
  </DynamicGrid>
);
