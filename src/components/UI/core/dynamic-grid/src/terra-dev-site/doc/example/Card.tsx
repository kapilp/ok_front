import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Card.module.scss';
const cx = classNames.bind(styles);
export default props => <div className={cx('card')} {...props} />;
