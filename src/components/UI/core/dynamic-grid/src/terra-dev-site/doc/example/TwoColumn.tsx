import Card from './Card';
import { DynamicGrid } from '../../../DynamicGrid';
import { Region } from '../../../Region';

const template = {
  'grid-template-columns': '1fr 1fr',
  'grid-template-rows': 'auto',
  'grid-gap': '10px',
};
const region1 = {
  'grid-column-start': 1,
  'grid-row-start': 1,
};
const region2 = {
  'grid-column-start': 2,
  'grid-row-start': 1,
};
export const TwoColumn = () => (
  <DynamicGrid defaultTemplate={template}>
    <Region defaultPosition={region1}>
      <Card>Region 1</Card>
    </Region>
    <Region defaultPosition={region2}>
      <Card>Region 2</Card>
    </Region>
  </DynamicGrid>
);
