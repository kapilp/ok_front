import Card from './Card';
import { DynamicGrid } from '../../../DynamicGrid';
import { Region } from '../../../Region';

const template = {
  'grid-template-columns': '1fr',
  'grid-template-rows': 'auto',
};
const region1 = {
  'grid-column-start': 1,
  'grid-column-end': 1,
  'grid-row-start': 1,
  'grid-row-end': 1,
};
export const OneColumn = () => (
  <DynamicGrid defaultTemplate={template}>
    <Region defaultPosition={region1}>
      <Card>Region 1</Card>
    </Region>
  </DynamicGrid>
);
