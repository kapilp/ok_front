import { JSX, Component, mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import Card from './Card';
import { DynamicGrid } from '../../../DynamicGrid';
import { Region } from '../../../Region';

const template = {
  'grid-template-columns': '1fr 1fr 1fr 1fr 1fr',
  'grid-template-rows': '100px',
  'grid-gap': '10px',
};
const region1 = {
  defaultPosition: {
    'grid-column-start': 1,
    'grid-row-start': 1,
  },
  small: {
    'grid-column-start': 2,
  },
  medium: {
    'grid-column-start': 3,
  },
  large: {
    'grid-column-start': 4,
  },
};
const region2 = {
  defaultPosition: {
    'grid-column-start': 2,
    'grid-row-start': 1,
  },
  small: {
    'grid-column-start': 3,
  },
  medium: {
    'grid-column-start': 4,
  },
  large: {
    'grid-column-start': 5,
  },
};
const region3 = {
  defaultPosition: {
    'grid-column-start': 3,
    'grid-row-start': 1,
  },
  small: {
    'grid-column-start': 4,
  },
  medium: {
    'grid-column-start': 5,
  },
  large: {
    'grid-column-start': 1,
  },
};
const region4 = {
  defaultPosition: {
    'grid-column-start': 4,
    'grid-row-start': 1,
  },
  small: {
    'grid-column-start': 5,
  },
  medium: {
    'grid-column-start': 1,
  },
  large: {
    'grid-column-start': 2,
  },
};
const region5 = {
  defaultPosition: {
    'grid-column-start': 5,
    'grid-row-start': 1,
  },
  small: {
    'grid-column-start': 1,
  },
  medium: {
    'grid-column-start': 2,
  },
  large: {
    'grid-column-start': 3,
  },
};
export const ResponsiveGrid = () => (
  <DynamicGrid defaultTemplate={template}>
    <Region {...region1}>
      <Card>1</Card>
    </Region>
    <Region {...region2}>
      <Card>2</Card>
    </Region>
    <Region {...region3}>
      <Card>3</Card>
    </Region>
    <Region {...region4}>
      <Card>4</Card>
    </Region>
    <Region {...region5}>
      <Card>5</Card>
    </Region>
  </DynamicGrid>
);
