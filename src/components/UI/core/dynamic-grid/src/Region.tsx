import {  JSX, mergeProps, splitProps  } from 'solid-js';
import { css, StyleSheet } from 'aphrodite';
import classNames from 'classnames/bind';
import styles from './DynamicGrid.module.scss';
import style from './Region.module.scss';
import { region } from './styles';

const cx = classNames.bind(style);

export type DynamicGridPositionShape = {
  /**
   * The starting column line for the region. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-column-start.
   */
  'grid-column-start'?: number;
  /**
   * The ending column line for the region. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-column-end.
   */
  'grid-column-end'?: number;
  /**
   * The starting row line for the region. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row-start.
   */
  'grid-row-start'?: number;
  /**
   * The ending row line for the region. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-row-end.
   */
  'grid-row-end'?: number;
  /**
   * Additional CSS properties to apply to the region.
   */
  // eslint-disable-next-line react/forbid-prop-types
  style?: {};
};

interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The children of the region.
   */
  children?: JSX.Element;
  /**
   * The default position to use.
   */
  defaultPosition?: DynamicGridPositionShape;
  /**
   *  The template to be used at tiny breakpoints
   */
  tiny?: DynamicGridPositionShape;
  /**
   *  The template to be used at small breakpoints
   */
  small?: DynamicGridPositionShape;
  /**
   *  The template to be used at medium breakpoints
   */
  medium?: DynamicGridPositionShape;
  /**
   *  The template to be used at large breakpoints
   */
  large?: DynamicGridPositionShape;
  /**
   *  The template to be used at huge breakpoints
   */
  huge?: DynamicGridPositionShape;
}

const defaultProps = {
  defaultPosition: {},
  tiny: {},
  small: {},
  medium: {},
  large: {},
  huge: {},
};

export const Region = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'defaultPosition', 'tiny', 'small', 'medium', 'large', 'huge']);
  const media = (breakpoint: string, props: DynamicGridPositionShape) => (Object.keys(props).length ? { [`@media screen and (min-width: ${breakpoint})`]: region(props) } : {});

  const stylesheet = StyleSheet.create({
    region: {
      ...region(p.defaultPosition!),
      ...media(styles.tiny, p.tiny!),
      ...media(styles.small, p.small!),
      ...media(styles.medium, p.medium!),
      ...media(styles.large, p.large!),
      ...media(styles.huge, p.huge!),
    },
  });

  const regionClasses = () => classNames(customProps.className, css(stylesheet.region));

  return (
    <div {...customProps} className={regionClasses()}>
      <div className={cx('region-child-container')}>{p.children}</div>
    </div>
  );
};
