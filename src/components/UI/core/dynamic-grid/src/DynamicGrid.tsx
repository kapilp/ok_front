import { JSX, mergeProps, splitProps } from 'solid-js';
import { StyleSheet, css } from 'aphrodite';
import classNames from 'classnames';
import { grid } from './styles';
import { Region } from './Region';
import styles from './DynamicGrid.module.scss';

export type DynamicGridTemplateShape = {
  /**
   * The column definitions of the grid. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-columns.
   * For IE support, verify syntax is supported here https://msdn.microsoft.com/en-us/library/hh772246(v=vs.85).aspx.
   */
  'grid-template-columns'?: string;
  /**
   * The row definitions of the grid. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-template-rows.
   * For IE support, verify syntax is supported here https://msdn.microsoft.com/en-us/library/hh772258(v=vs.85).aspx.
   */
  'grid-template-rows'?: string;
  /**
   * The gap to place between the columns and rows. See https://developer.mozilla.org/en-US/docs/Web/CSS/grid-gap.
   */
  'grid-gap'?: string;
  /**
   * Additional CSS properties to apply to the grid.
   */
  // eslint-disable-next-line react/forbid-prop-types
  style?: {};
};

interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The child Region components that make up the grid.
   */
  children?: JSX.Element;
  /**
   * The default grid layout template to use.
   */
  defaultTemplate?: DynamicGridTemplateShape;
  /**
   *  The template to be used at tiny breakpoints
   */
  tiny?: DynamicGridTemplateShape;
  /**
   *  The template to be used at small breakpoints
   */
  small?: DynamicGridTemplateShape;
  /**
   *  The template to be used at medium breakpoints
   */
  medium?: DynamicGridTemplateShape;
  /**
   *  The template to be used at large breakpoints
   */
  large?: DynamicGridTemplateShape;
  /**
   *  The template to be used at huge breakpoints
   */
  huge?: DynamicGridTemplateShape;
}

const defaultProps = {
  defaultTemplate: {},
  tiny: {},
  small: {},
  medium: {},
  large: {},
  huge: {},
};

export const DynamicGrid = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['defaultTemplate', 'tiny', 'small', 'medium', 'large', 'huge', 'children', 'className']);
  const media = (breakpoint: string, props: DynamicGridTemplateShape) => (Object.keys(props).length ? { [`@media screen and (min-width: ${breakpoint})`]: grid(props) } : {});

  const stylesheet = () =>
    StyleSheet.create({
      grid: {
        ...grid(p.defaultTemplate!),
        ...media(styles.tiny, p.tiny!),
        ...media(styles.small, p.small!),
        ...media(styles.medium, p.medium!),
        ...media(styles.large, p.large!),
        ...media(styles.huge, p.huge!),
      },
    });

  const gridClasses = () => classNames(p.className, css(stylesheet().grid));

  return (<div {...customProps} className={gridClasses()}>{p.children}</div>);
};

DynamicGrid.Region = Region;
