import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import ReadMe from '../../../../docs/README.md';
import styles from './Markdown.module.scss';

import { Markdown } from '../../../Markdown';
const cx = classNames.bind(styles);
// We're not using the doc template here to avoid circular dependencies.
export const MarkdownExample = () => (
  <div className={cx('markdown-wrapper')}>
    <Markdown src={ReadMe} />
  </div>
);
