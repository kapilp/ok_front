import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Markdown from '../../../../lib/Markdown';
import markdown from './html.md';
const HtmlTest = () => <Markdown src={markdown} />;
export default HtmlTest;
