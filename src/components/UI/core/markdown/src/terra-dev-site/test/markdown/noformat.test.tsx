import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Markdown from '../../../../lib/Markdown';
import markdown from './noformat.md';
const NoformatTest = () => <Markdown src={markdown} />;
export default NoformatTest;
