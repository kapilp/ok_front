import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Markdown from '../../../../lib/Markdown';
import markdown from './javascript.md';
const JavascriptTest = () => <Markdown src={markdown} />;
export default JavascriptTest;
