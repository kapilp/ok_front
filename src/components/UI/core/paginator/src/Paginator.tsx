import * as KeyCode from 'keycode-js';
import classNamesBind from 'classnames/bind';
//import { injectIntl, intlShape } from 'react-intl';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { calculatePages, pageSet } from './_paginationUtils';
import { PaginatorButton } from './_PaginatorButton';
import styles from './Paginator.module.scss';
import index from '../../../framework/date-picker/src/react-datepicker';
import { render } from 'solid-js/web';
import {  JSX, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
const cx = classNamesBind.bind(styles);
interface IPaginatorProps extends JSX.HTMLAttributes<Element> {
  /**
   * Function to be executed when a navigation element is selected.
   */
  onPageChange: () => void;
  /**
   * The active/selected page. Used to set the default selected page or maintain selection across renders.
   * Required when using totalCount and itemCountPerPage.
   */
  selectedPage?: number;
  /**
   * Total number of all items being paginated.
   * Required when using itemCountPerPage and selectedPage.
   */
  totalCount?: number;
  /**
   * Total number of items per page.
   * Required when using selectedPage and totalCount.
   */
  itemCountPerPage?: number;
  /**
   * @private
   * The intl object to be injected for translations.
   */
  // intl: intlShape.isRequired; //tood
}

type PaginatorState = {
  selectedPage?: any;
  pageSequence?: any[];
  showReducedPaginator?: boolean;
};
export const Paginator = (props: IPaginatorProps) => {
  const theme = useContext(ThemeContext);

  const [state, setState] = createStore({
    selectedPage: props.selectedPage && props.totalCount ? props.selectedPage : undefined,
    pageSequence: props.selectedPage && props.totalCount ? pageSet(props.selectedPage, calculatePages(props.totalCount, props.itemCountPerPage)) : undefined,
    showReducedPaginator: false,
  });
  const setPaginator = event => {
    const showReducedPaginator = event === 'tiny';
    if (state.showReducedPaginator !== showReducedPaginator) {
      setState({ showReducedPaginator });
    }
  };
  const handlePageChange = index => {
    return event => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        event.preventDefault();
      }
      if (Number.isNaN(Number(index))) {
        props.onPageChange(event.currentTarget.attributes['aria-label'].value);
        return false;
      }
      props.onPageChange(index);
      setState({
        selectedPage: index,
        pageSequence: pageSet(index, calculatePages(props.totalCount, props.itemCountPerPage)),
      });
      return false;
    };
  };
  const buildPageButtons = (totalPages, onClick) => {
    const pageButtons = [];
    state.pageSequence.forEach(val => {
      const paginationLinkClassNames = cx(['nav-link', val === state.selectedPage ? 'is-selected' : null]);
      if (val > totalPages) {
        return;
      }
      pageButtons.push(
        <PaginatorButton
          ariaLabel={`Page ${val}`}
          ariaCurrent={val === state.selectedPage}
          className={paginationLinkClassNames}
          tabIndex={val === state.selectedPage ? '-1' : '0'}
          key={`pageButton_${val}`}
          onClick={onClick(val)}
          onKeyDown={onClick(val)}
        >
          {val}
        </PaginatorButton>,
      );
    });
    return pageButtons;
  };
  const hasNavContext = () => {
    return props.totalCount && props.itemCountPerPage;
  };
  const defaultPaginator = () => {
    const theme = useContext(ThemeContext);

    const totalPages = calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = () => (state.selectedPage === 1 ? 1 : state.selectedPage - 1);
    const nextPageIndex = () => (state.selectedPage === totalPages ? totalPages : state.selectedPage + 1);
    const fullView = (
      <div className={cx('tPaginator', 'paginator', !hasNavContext() && 'pageless', theme.className)}>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', 'left-controls', state.selectedPage === 1 && 'is-disabled'])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
        )}
        <PaginatorButton
          ariaDisabled={state.selectedPage === 1}
          ariaLabel={'Previous'}
          className={cx(['nav-link', 'left-controls', 'previous', state.selectedPage === 1 && 'is-disabled'])}
          disabled={state.selectedPage === 1}
          onClick={handlePageChange(previousPageIndex())}
          onKeyDown={handlePageChange(previousPageIndex())}
        >
          <span className={cx('icon')} />
          {'Previous'}
        </PaginatorButton>
        {hasNavContext() && buildPageButtons(totalPages, handlePageChange)}
        <PaginatorButton
          ariaDisabled={state.selectedPage === totalPages}
          ariaLabel={'Next'}
          className={cx(['nav-link', 'right-controls', 'next', state.selectedPage === totalPages && 'is-disabled'])}
          disabled={state.selectedPage === totalPages}
          onClick={handlePageChange(nextPageIndex())}
          onKeyDown={handlePageChange(nextPageIndex())}
        >
          {'Next'}
          <span className={cx('icon')} />
        </PaginatorButton>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages}
            ariaLabel={'Last'}
            className={cx(['nav-link', 'right-controls', state.selectedPage === totalPages && 'is-disabled'])}
            disabled={state.selectedPage === totalPages}
            onClick={handlePageChange(totalPages)}
            onKeyDown={handlePageChange(totalPages)}
          >
            {'Last'}
          </PaginatorButton>
        )}
      </div>
    );
    return fullView;
  };
  const reducedPaginator = () => {
    const theme = useContext(ThemeContext);

    const totalPages = () => calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = () => (state.selectedPage === 1 ? 1 : state.selectedPage - 1);
    const nextPageIndex = () => (state.selectedPage === totalPages() ? totalPages() : state.selectedPage + 1);
    const reducedView = (
      <div className={cx('tPaginator', 'paginator', !hasNavContext() && 'pageless', theme.className)} role="navigation" aria-label="pagination">
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', 'left-controls', state.selectedPage === 1 && 'is-disabled'])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
        )}
        <PaginatorButton
          ariaDisabled={state.selectedPage === 1}
          ariaLabel={'Previous'}
          className={cx(['nav-link', 'left-controls', 'previous', 'icon-only', state.selectedPage === 1 && 'is-disabled'])}
          disabled={state.selectedPage === 1}
          onClick={handlePageChange(previousPageIndex())}
          onKeyDown={handlePageChange(previousPageIndex())}
        >
          <VisuallyHiddenText text={'Previous'} />
          <span className={cx('icon')} />
        </PaginatorButton>
        {hasNavContext() && `Page ${state.selectedPage}`}
        <PaginatorButton
          ariaDisabled={state.selectedPage === totalPages()}
          ariaLabel={'Next'}
          className={cx(['nav-link', 'right-controls', 'next', 'icon-only', state.selectedPage === totalPages() && 'is-disabled'])}
          disabled={state.selectedPage === totalPages()}
          onClick={handlePageChange(nextPageIndex())}
          onKeyDown={handlePageChange(nextPageIndex())}
        >
          <VisuallyHiddenText text={'Next'} />
          <span className={cx('icon')} />
        </PaginatorButton>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages()}
            ariaLabel={'Last'}
            className={cx(['nav-link', 'right-controls', state.selectedPage === totalPages() && 'is-disabled'])}
            disabled={state.selectedPage === totalPages()}
            onClick={handlePageChange(totalPages())}
            onKeyDown={handlePageChange(totalPages())}
          >
            {'Last'}
          </PaginatorButton>
        )}
      </div>
    );
    return reducedView;
  };

  return <ResponsiveElement onChange={setPaginator}>{state.showReducedPaginator ? reducedPaginator() : defaultPaginator()}</ResponsiveElement>;
};

// export default injectIntl(Paginator);
