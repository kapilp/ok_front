import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Dialog } from '../../../../../dialog/src/Dialog';
import { Paginator } from '../../../Paginator';
import styles from './PaginatorExampleCommon.module.scss';
const cx = classNames.bind(styles);
const totalCount = 450;
const fillArray = (value, len) => {
  const arr = [];
  for (let i = 0; i < len; i += 1) {
    arr.push(<p key={Math.floor(Math.random() * Math.floor(100000))}>{value}</p>);
  }
  return arr;
};
const buildPage = () => {
  let fullContent = [];
  const content =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
    'Fusce porttitor ullamcorper nisi, vel tincidunt dui pharetra vel. ' +
    'Morbi eu rutrum nibh, sit amet placerat libero. Integer vel dapibus nibh. ' +
    'Donec tempor mi vitae lorem congue, ut ultrices metus feugiat. Sed non commodo felis. ' +
    'Aliquam eget maximus dui, ut rhoncus augue.';
  fullContent = fillArray(content, 10);
  return fullContent;
};
type PaginatorExampleState = {
  content?: any[];
  currentPage?: number;
};
export const PaginatorExample = props => {
  const [state, setState] = createStore({
    content: buildPage(),
    currentPage: 1,
  });
  const changePages = index => {
    setState({ currentPage: index });
  };

  return (
    <div className={cx('paginator-wrapper')}>
      <Dialog
        header={
          <h1>
            Page
            {state.currentPage}
          </h1>
        }
        footer={<Paginator onPageChange={changePages} selectedPage={1} totalCount={totalCount} itemCountPerPage={10} />}
      >
        {state.content}
      </Dialog>
    </div>
  );
};
