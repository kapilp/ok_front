import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Dialog } from '../../../../../dialog/src/Dialog';
import { Paginator } from '../../../Paginator';
import classNames from 'classnames/bind';
import styles from './PaginatorExampleCommon.module.scss';
const cx = classNames.bind(styles);
const maxPages = 5;
const fillArray = (value, len) => {
  const arr = [];
  for (let i = 0; i < len; i += 1) {
    arr.push(<p key={Math.floor(Math.random() * Math.floor(100000))}>{value}</p>);
  }
  return arr;
};
const buildPage = () => {
  let fullContent = [];
  const content =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
    'Fusce porttitor ullamcorper nisi, vel tincidunt dui pharetra vel. ' +
    'Morbi eu rutrum nibh, sit amet placerat libero. Integer vel dapibus nibh. ' +
    'Donec tempor mi vitae lorem congue, ut ultrices metus feugiat. Sed non commodo felis. ' +
    'Aliquam eget maximus dui, ut rhoncus augue.';
  fullContent = fillArray(content, 10);
  return fullContent;
};
type PaginatorNoPagesExampleState = {
  content?: JSX.Element;
  currentPage?: number;
};
export const PaginatorNoPagesExample = (props: {}) => {
  const [state, setState] = createStore({
    content: <h2>Welcome!</h2>,
    currentPage: 1,
  });
  const changePages = direction => {
    const index = direction === 'next' ? state.currentPage + 1 : state.currentPage - 1;
    if (index >= maxPages) {
      setState({
        content: <h2>No more pages...</h2>,
        currentPage: maxPages,
      });
    } else if (index <= 1) {
      setState({ content: <h2>Welcome!</h2>, currentPage: 1 });
    } else {
      setState({ content: buildPage(), currentPage: index });
    }
  };

  return (
    <div className={cx('paginator-wrapper')}>
      <Dialog
        header={
          <h1>
            Page
            {state.currentPage}
          </h1>
        }
        footer={<Paginator onPageChange={changePages} />}
      >
        {state.content}
      </Dialog>
    </div>
  );
};
