import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import classNames from 'classnames/bind';
import ControlledPaginator from '../../../ControlledPaginator';
import styles from './ControlledPaginatorTestCommon.module.scss';
const cx = classNames.bind(styles);
const totalCount = 450;
type PaginatorExampleState = {
  currentPage?: number;
};
class PaginatorExample extends React.Component<{}, PaginatorExampleState> {
  constructor(props) {
    super(props);
    state = {
      currentPage: 1,
    };
    changePages = changePages.bind(this);
  }
  changePages(index) {
    setState({ currentPage: index });
  }
  render() {
    return (
      <div className={cx('paginator-wrapper')}>
        <Button
          id="button-9"
          text="Set Page to 9"
          onClick={() => {
            setState({ currentPage: 9 });
          }}
        />
        <Button
          id="button-15"
          text="Set Page to 15"
          onClick={() => {
            setState({ currentPage: 15 });
          }}
        />
        <Button
          id="button-45"
          text="Set Page to 45"
          onClick={() => {
            setState({ currentPage: 45 });
          }}
        />
        <ControlledPaginator onPageChange={changePages} selectedPage={state.currentPage} totalCount={totalCount} itemCountPerPage={10} />
      </div>
    );
  }
}
export default PaginatorExample;
