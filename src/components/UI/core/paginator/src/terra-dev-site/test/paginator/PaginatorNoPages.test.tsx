import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Paginator from '../../../Paginator';
const PaginatorNoPagesExample = () => (
  // eslint-disable-next-line no-console
  <Paginator onPageChange={i => console.log(i)} />
);
export default PaginatorNoPagesExample;
