import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import classNames from 'classnames/bind';
import ControlledProgressivePaginator from '../../../ControlledProgressivePaginator';
import styles from './ControlledPaginatorTestCommon.module.scss';
const cx = classNames.bind(styles);
const totalCount = 450;
type ProgressivePaginatorExampleState = {
  currentPage?: number;
};
class ProgressivePaginatorExample extends React.Component<{}, ProgressivePaginatorExampleState> {
  constructor(props) {
    super(props);
    state = {
      currentPage: 1,
    };
    changePages = changePages.bind(this);
  }
  changePages(index) {
    setState({ currentPage: index });
  }
  render() {
    return (
      <div className={cx('paginator-wrapper')}>
        <Button
          id="button-9"
          text="Set Page to 9"
          onClick={() => {
            setState({ currentPage: 9 });
          }}
        />
        <Button
          id="button-15"
          text="Set Page to 15"
          onClick={() => {
            setState({ currentPage: 15 });
          }}
        />
        <Button
          id="button-45"
          text="Set Page to 45"
          onClick={() => {
            setState({ currentPage: 45 });
          }}
        />
        <ControlledProgressivePaginator onPageChange={changePages} selectedPage={state.currentPage} totalCount={totalCount} itemCountPerPage={10} />
      </div>
    );
  }
}
export default ProgressivePaginatorExample;
