import * as KeyCode from 'keycode-js';
// import { injectIntl, intlShape } from 'react-intl';
import classNamesBind from 'classnames/bind';
import { JSX, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { calculatePages } from './_paginationUtils';
import { PaginatorButton } from './_PaginatorButton';
import styles from './Paginator.module.scss';

const cx = classNamesBind.bind(styles);
interface IProgressivePaginatorProps extends JSX.HTMLAttributes<Element> {
  /**
   * Function to be executed when a navigation element is selected.
   */
  onPageChange: () => void;
  /**
   * The active/selected page.
   */
  selectedPage: number;
  /**
   * Total number of all items being paginated.
   */
  totalCount: number;
  /**
   * Total number of items per page.
   */
  itemCountPerPage: number;
  /**
   * @private
   * The intl object to be injected for translations.
   */
  // intl: intlShape; //todo
}

export const ProgressivePaginator = (props: IProgressivePaginatorProps) => {
  const theme = useContext(ThemeContext);

  const [state, setState] = createStore({
    selectedPage: props.selectedPage && props.totalCount ? props.selectedPage : undefined,
    showReducedPaginator: false,
  });
  const setPaginator = event => {
    const showReducedPaginator = event === 'tiny';
    if (state.showReducedPaginator !== showReducedPaginator) {
      setState({ showReducedPaginator });
    }
  };
  const handlePageChange = index => {
    return event => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        event.preventDefault();
      }
      props.onPageChange(index);
      setState({ selectedPage: index });
    };
  };
  const defaultProgressivePaginator = () => {
    const theme = useContext(ThemeContext);

    const totalPages = () => calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = () => (state.selectedPage === 1 ? 1 : state.selectedPage - 1);
    const nextPageIndex = () => (state.selectedPage === totalPages() ? totalPages() : state.selectedPage + 1);
    return (
      <div className={cx('paginator', 'progressive', theme.className)} role="navigation" aria-label="pagination">
        <div>{`Page ${state.selectedPage} of ${totalPages()}`}</div>
        <div>
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel="First"
            className={cx(['nav-link', state.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            First
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel="Previous"
            className={cx(['nav-link', 'previous', state.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(previousPageIndex())}
            onKeyDown={handlePageChange(previousPageIndex())}
          >
            <span className={cx('icon')} />
            Previous
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages()}
            ariaLabel="Next"
            className={cx(['nav-link', 'next', state.selectedPage === totalPages() ? 'is-disabled' : null])}
            disabled={state.selectedPage === totalPages()}
            onClick={handlePageChange(nextPageIndex())}
            onKeyDown={handlePageChange(nextPageIndex())}
          >
            Next
            <span className={cx('icon')} />
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages()}
            ariaLabel="Last"
            className={cx(['nav-link', state.selectedPage === totalPages() ? 'is-disabled' : null])}
            disabled={state.selectedPage === totalPages()}
            onClick={handlePageChange(totalPages())}
            onKeyDown={handlePageChange(totalPages())}
          >
            Last
          </PaginatorButton>
        </div>
      </div>
    );
  };
  const reducedProgressivePaginator = () => {
    const theme = useContext(ThemeContext);

    const totalPages = () => calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = () => (state.selectedPage === 1 ? 1 : state.selectedPage - 1);
    const nextPageIndex = () => (state.selectedPage === totalPages() ? totalPages() : state.selectedPage + 1);
    return (
      <div className={cx('tPaginator', 'paginator', theme.className)} role="navigation" aria-label="pagination">
        <div>
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel="First"
            className={cx(['nav-link', state.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            First
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={state.selectedPage === 1}
            ariaLabel="Previous"
            className={cx(['nav-link', 'previous', 'icon-only', state.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={state.selectedPage === 1}
            onClick={handlePageChange(previousPageIndex())}
            onKeyDown={handlePageChange(previousPageIndex())}
          >
            <VisuallyHiddenText text="Previous" />
            <span className={cx('icon')} />
          </PaginatorButton>
        </div>
        <div>{`Page ${state.selectedPage} of ${totalPages()}`}</div>
        <div>
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages()}
            ariaLabel="Next"
            className={cx(['nav-link', 'next', 'icon-only', state.selectedPage === totalPages() ? 'is-disabled' : null])}
            disabled={state.selectedPage === totalPages()}
            onClick={handlePageChange(nextPageIndex())}
            onKeyDown={handlePageChange(nextPageIndex())}
          >
            <VisuallyHiddenText text="Next" />
            <span className={cx('icon')} />
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={state.selectedPage === totalPages()}
            ariaLabel="Last"
            className={cx(['nav-link', state.selectedPage === totalPages() ? 'is-disabled' : null])}
            disabled={state.selectedPage === totalPages()}
            onClick={handlePageChange(totalPages())}
            onKeyDown={handlePageChange(totalPages())}
          >
            Last
          </PaginatorButton>
        </div>
      </div>
    );
  };

  return <ResponsiveElement onChange={setPaginator}>{state.showReducedPaginator ? reducedProgressivePaginator() : defaultProgressivePaginator()}</ResponsiveElement>;
};

// export default injectIntl(ProgressivePaginator);
