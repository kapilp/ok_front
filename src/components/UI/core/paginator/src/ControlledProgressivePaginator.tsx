import * as KeyCode from 'keycode-js';
import classNamesBind from 'classnames/bind';
// import { injectIntl, intlShape } from 'react-intl';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { calculatePages } from './_paginationUtils';
import { PaginatorButton } from './_PaginatorButton';
import styles from './Paginator.module.scss';
import {  JSX, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import index from '../../../framework/date-picker/src/react-datepicker';
import value from '*.svg';
import { render } from 'solid-js/web';
const cx = classNamesBind.bind(styles);
interface IProgressivePaginatorProps extends JSX.HTMLAttributes<Element> {
  /**
   * Function to be executed when a navigation element is selected.
   */
  onPageChange: () => void;
  /**
   * The active/selected page.
   */
  selectedPage: number;
  /**
   * Total number of all items being paginated.
   */
  totalCount: number;
  /**
   * Total number of items per page.
   */
  itemCountPerPage: number;
  /**
   * @private
   * The intl object to be injected for translations.
   */
  // intl: intlShape.isRequired; //todo
}

export const ProgressivePaginator = (props: IProgressivePaginatorProps) => {
  const theme = useContext(ThemeContext);

  const [state, setState] = createStore({
    showReducedPaginator: false,
  });

  const setPaginator = value => {
    const showReducedPaginator = value === 'tiny';
    if (state.showReducedPaginator !== showReducedPaginator) {
      setState({ showReducedPaginator });
    }
  };
  const handlePageChange = index => {
    return event => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        event.preventDefault();
      }
      props.onPageChange(index);
    };
  };
  const defaultProgressivePaginator = () => {
    const theme = useContext(ThemeContext);
    const totalPages = () => calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = props.selectedPage === 1 ? 1 : props.selectedPage - 1;
    const nextPageIndex = props.selectedPage === totalPages ? totalPages : props.selectedPage + 1;
    return (
      <div className={cx('paginator', 'progressive', theme.className)} role="navigation" aria-label="pagination">
        <div>{`Page ${props.selectedPage} of ${totalPages}`}</div>
        <div>
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', props.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'Previous'}
            className={cx(['nav-link', 'previous', props.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(previousPageIndex)}
            onKeyDown={handlePageChange(previousPageIndex)}
          >
            <span className={cx('icon')} />
            {'Previous'}
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Next'}
            className={cx(['nav-link', 'next', props.selectedPage === totalPages ? 'is-disabled' : null])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(nextPageIndex)}
            onKeyDown={handlePageChange(nextPageIndex)}
          >
            {'Next'}
            <span className={cx('icon')} />
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Last'}
            className={cx(['nav-link', props.selectedPage === totalPages ? 'is-disabled' : null])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(totalPages)}
            onKeyDown={handlePageChange(totalPages)}
          >
            {'Last'}
          </PaginatorButton>
        </div>
      </div>
    );
  };
  const reducedProgressivePaginator = () => {
    const theme = useContext(ThemeContext);
    const totalPages = calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = props.selectedPage === 1 ? 1 : props.selectedPage - 1;
    const nextPageIndex = props.selectedPage === totalPages ? totalPages : props.selectedPage + 1;
    return (
      <div className={cx('paginator', theme.className)} role="navigation" aria-label="pagination">
        <div>
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', props.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'Previous'}
            className={cx(['nav-link', 'previous', 'icon-only', props.selectedPage === 1 ? 'is-disabled' : null])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(previousPageIndex)}
            onKeyDown={handlePageChange(previousPageIndex)}
          >
            <VisuallyHiddenText text={'Previous'} />
            <span className={cx('icon')} />
          </PaginatorButton>
        </div>
        <div>{`Page ${props.selectedPage} of ${totalPages}`}</div>
        <div>
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Next'}
            className={cx(['nav-link', 'next', 'icon-only', props.selectedPage === totalPages ? 'is-disabled' : null])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(nextPageIndex)}
            onKeyDown={handlePageChange(nextPageIndex)}
          >
            <VisuallyHiddenText text={'Next'} />
            <span className={cx('icon')} />
          </PaginatorButton>
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Last'}
            className={cx(['nav-link', props.selectedPage === totalPages ? 'is-disabled' : null])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(totalPages)}
            onKeyDown={handlePageChange(totalPages)}
          >
            {'Last'}
          </PaginatorButton>
        </div>
      </div>
    );
  };

  return <ResponsiveElement onChange={setPaginator}>{state.showReducedPaginator ? reducedProgressivePaginator() : defaultProgressivePaginator()}</ResponsiveElement>;
};

// export default injectIntl(ProgressivePaginator);
