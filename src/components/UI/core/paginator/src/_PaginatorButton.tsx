import classNames from 'classnames/bind';
import * as KeyCode from 'keycode-js';
import styles from './Paginator.module.scss';
import { JSX, createComputed, createSignal, splitProps } from 'solid-js';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Sets aria-current attribute.
   */
  ariaCurrent: boolean;
  /**
   * Sets aria-disabled attribute.
   */
  ariaDisabled: boolean;
  /**
   * Sets aria-label attribute.
   */
  ariaLabel: string;
  /**
   * Child elements passed in to buttons.
   */
  children: JSX.Element;
  /**
   * Passed in CSS className.
   */
  className: string;
  /**
   * Sets the disabled attribute based on the selected page..
   */
  disabled: boolean;
  /**
   * Callback function triggered when clicked.
   */
  onClick: (e: MouseEvent) => void;
  /**
   * Tab Index of selected button.
   */
  tabIndex: string;
}
const setFocusActiveStyles = (setActive, setFocused, event) => {
  if (event.keyCode === KeyCode.KEY_TAB) {
    setFocused(true);
  }
  if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
    if (event.type === 'keydown') {
      setActive(true);
      setFocused(false);
    } else {
      setActive(false);
      setFocused(true);
    }
  }
};
const handleOnBlur = (setActive, setFocused) => {
  setActive(false);
  setFocused(false);
};
const handleMouseDown = event => {
  // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button#Clicking_and_focus
  // Button on Firefox, Safari and IE running on OS X does not receive focus when clicked.
  // This will put focus on the button when clicked if it is not currently the active element.
  if (document.activeElement !== event.currentTarget) {
    event.currentTarget.focus();
  }
  event.preventDefault();
};
export const PaginatorButton = (props: Properties) => {
  const [isActive, setActive] = createSignal(false);
  const [isFocused, setFocused] = createSignal(false);
  const buttonFocused = isFocused() ? 'is-focused' : undefined;
  const buttonActive = isActive() ? 'is-active' : undefined;
  const [p, customProps] = splitProps(props, ['ariaCurrent', 'ariaDisabled', 'ariaLabel', 'children', 'className', 'disabled', 'onClick', 'tabIndex']);

  createComputed(() => {
    if (p.disabled) setActive(false);
  });
  return (
    <button
      aria-current={p.ariaCurrent}
      aria-disabled={p.ariaDisabled}
      aria-label={p.ariaLabel}
      className={cx([buttonActive, buttonFocused, p.className])}
      disabled={p.disabled}
      onBlur={() => handleOnBlur(setActive, setFocused)}
      onClick={p.onClick}
      onKeyDown={e => setFocusActiveStyles(setActive, setFocused, e)}
      onKeyUp={e => setFocusActiveStyles(setActive, setFocused, e)}
      onMouseDown={handleMouseDown}
      tabIndex={p.tabIndex}
      type="button"
    >
      {p.children}
    </button>
  );
};
