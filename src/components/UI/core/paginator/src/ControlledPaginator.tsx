import {  JSX, createMemo, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import * as KeyCode from 'keycode-js';
import classNamesBind from 'classnames/bind';
//import { injectIntl, intlShape } from 'react-intl';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { calculatePages, pageSet } from './_paginationUtils';
import { PaginatorButton } from './_PaginatorButton';
import styles from './Paginator.module.scss';
const cx = classNamesBind.bind(styles);
interface IPaginatorProps extends JSX.HTMLAttributes<Element> {
  /**
   * Function to be executed when a navigation element is selected.
   */
  onPageChange: () => void;
  /**
   * The active/selected page. Used to set the default selected page or maintain selection across renders.
   * Required when using totalCount and itemCountPerPage.
   */
  selectedPage: number;
  /**
   * Total number of all items being paginated.
   * Required when using itemCountPerPage and selectedPage.
   */
  totalCount: number;
  /**
   * Total number of items per page.
   * Required when using selectedPage and totalCount.
   */
  itemCountPerPage: number;
  /**
   * @private
   * The intl object to be injected for translations.
   */
  // intl: intlShape.isRequired; //todo fix this
}

export const Paginator = (props: IPaginatorProps) => {
  const theme = useContext(ThemeContext);

  const [state, setState] = createStore({
    showReducedPaginator: false,
  });
  const setPaginator = value => {
    const showReducedPaginator = value === 'tiny';
    if (state.showReducedPaginator !== showReducedPaginator) {
      setState({ showReducedPaginator });
    }
  };
  const handlePageChange = (index: number) => {
    return event => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        event.preventDefault();
      }
      if (Number.isNaN(Number(index))) {
        props.onPageChange(event.currentTarget.attributes['aria-label'].value);
        return false;
      }
      props.onPageChange(index);
      return false;
    };
  };
  const buildPageButtons = (totalPages, onClick) => {
    const pageSequence = pageSet(props.selectedPage, calculatePages(props.totalCount, props.itemCountPerPage));
    const pageButtons = [];
    pageSequence.forEach(val => {
      const paginationLinkClassNames = cx(['nav-link', val === props.selectedPage ? 'is-selected' : null]);
      if (val > totalPages) {
        return;
      }
      pageButtons.push(
        <PaginatorButton
          ariaLabel={`Page ${props.val}`}
          ariaCurrent={val === props.selectedPage}
          className={paginationLinkClassNames}
          key={`pageButton_${val}`}
          onClick={onClick(val)}
          onKeyDown={onClick(val)}
          tabIndex={val === props.selectedPage ? '-1' : '0'}
        >
          {val}
        </PaginatorButton>,
      );
    });
    return pageButtons;
  };
  const hasNavContext = () => {
    return props.totalCount && props.itemCountPerPage;
  };
  const defaultPaginator = () => {
    const theme = useContext(ThemeContext);
    const totalPages = () => calculatePages(props.totalCount, props.itemCountPerPage); // use memo?

    const previousPageIndex = () => (props.selectedPage === 1 ? 1 : props.selectedPage - 1);
    const nextPageIndex = () => (props.selectedPage === totalPages() ? totalPages() : props.selectedPage + 1);
    const fullView = (
      <div className={cx('paginator', !hasNavContext() && 'pageless', theme.className)}>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', 'left-controls', props.selectedPage === 1 && 'is-disabled'])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
        )}
        <PaginatorButton
          ariaDisabled={props.selectedPage === 1}
          ariaLabel={'Previous'}
          className={cx(['nav-link', 'left-controls', 'previous', props.selectedPage === 1 && 'is-disabled'])}
          disabled={props.selectedPage === 1}
          onClick={handlePageChange(previousPageIndex())}
          onKeyDown={handlePageChange(previousPageIndex())}
        >
          <span className={cx('icon')} />
          {'Previous'}
        </PaginatorButton>
        {hasNavContext() && buildPageButtons(totalPages, handlePageChange)}
        <PaginatorButton
          ariaDisabled={props.selectedPage === totalPages}
          ariaLabel={'Next'}
          className={cx(['nav-link', 'right-controls', 'next', props.selectedPage === totalPages && 'is-disabled'])}
          disabled={props.selectedPage === totalPages}
          onClick={handlePageChange(nextPageIndex())}
          onKeyDown={handlePageChange(nextPageIndex())}
        >
          {'Next'}
          <span className={cx('icon')} />
        </PaginatorButton>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Last'}
            className={cx(['nav-link', 'right-controls', props.selectedPage === totalPages && 'is-disabled'])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(totalPages)}
            onKeyDown={handlePageChange(totalPages)}
          >
            {'Last'}
          </PaginatorButton>
        )}
      </div>
    );
    return fullView;
  };
  const reducedPaginator = () => {
    const theme = useContext(ThemeContext);
    const totalPages = calculatePages(props.totalCount, props.itemCountPerPage);

    const previousPageIndex = props.selectedPage === 1 ? 1 : props.selectedPage - 1;
    const nextPageIndex = props.selectedPage === totalPages ? totalPages : props.selectedPage + 1;
    const reducedView = (
      <div className={cx('tPaginator', 'paginator', !hasNavContext() && 'pageless', theme.className)} role="navigation" aria-label="pagination">
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={props.selectedPage === 1}
            ariaLabel={'First'}
            className={cx(['nav-link', 'left-controls', props.selectedPage === 1 && 'is-disabled'])}
            disabled={props.selectedPage === 1}
            onClick={handlePageChange(1)}
            onKeyDown={handlePageChange(1)}
          >
            {'First'}
          </PaginatorButton>
        )}
        <PaginatorButton
          ariaDisabled={props.selectedPage === 1}
          ariaLabel={'Previous'}
          className={cx(['nav-link', 'left-controls', 'previous', props.selectedPage === 1 && 'is-disabled'])}
          disabled={props.selectedPage === 1}
          onClick={handlePageChange(previousPageIndex)}
          onKeyDown={handlePageChange(previousPageIndex)}
        >
          <span className={cx('icon')} />
          {'Previous'}
        </PaginatorButton>
        {hasNavContext() && `Page ${props.selectedPage}`}
        <PaginatorButton
          ariaDisabled={props.selectedPage === totalPages}
          ariaLabel={'Next'}
          className={cx(['nav-link', 'right-controls', 'next', 'icon-only', props.selectedPage === totalPages && 'is-disabled'])}
          disabled={props.selectedPage === totalPages}
          onClick={handlePageChange(nextPageIndex)}
          onKeyDown={handlePageChange(nextPageIndex)}
        >
          <VisuallyHiddenText text={'Next'} />
          <span className={cx('icon')} />
        </PaginatorButton>
        {hasNavContext() && (
          <PaginatorButton
            ariaDisabled={props.selectedPage === totalPages}
            ariaLabel={'Last'}
            className={cx(['nav-link', 'right-controls', props.selectedPage === totalPages && 'is-disabled'])}
            disabled={props.selectedPage === totalPages}
            onClick={handlePageChange(totalPages)}
            onKeyDown={handlePageChange(totalPages)}
          >
            {'Last'}
          </PaginatorButton>
        )}
      </div>
    );
    return reducedView;
  };

  return <ResponsiveElement onChange={setPaginator}>{state.showReducedPaginator ? reducedPaginator() : defaultPaginator()}</ResponsiveElement>;
};

// export default injectIntl(Paginator);
