import { JSX, createMemo, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Checkbox.module.scss';
import { isConsideredMobileDevice } from './CheckboxUtil';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Whether or not the checkbox element is checked. Use this to generate a controlled Checkbox Element.
   */
  checked?: boolean;
  /**
   * The checked property of the Input element. Use this to generate an uncontrolled Checkbox Element.
   */
  defaultChecked?: boolean;
  /**
   * The id of the checkbox.
   */
  id?: string;
  /**
   * Additional attributes for the input object.
   */
  // eslint-disable-next-line react/forbid-prop-types
  inputAttrs?: {};
  /**
   * Whether the checkbox element is disabled.
   */
  disabled?: boolean;
  /**
   * Whether the checkbox element is inline.
   */
  isInline?: boolean;
  /**
   * Whether the label is hidden.
   */
  isLabelHidden?: boolean;
  /**
   * Text of the label.
   */
  labelText: string;
  /**
   * Additional attributes for the text object.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelTextAttrs?: {};
  /**
   * Name attribute of the input.
   */
  name?: string;
  /**
   * Function to trigger when focus is lost from the checkbox.
   */
  onBlur?: () => void;
  /**
   * Function to trigger when user clicks on the checkbox. Provide a function to create a controlled input.
   */
  onChange?: () => void;
  /**
   *  Function to trigger when user focuses on the checkbox.
   */
  onFocus?: () => void;
  /**
   * The value of the input element.
   */
  value?: string;
}
const defaultProps = {
  checked: undefined,
  defaultChecked: undefined,
  id: undefined,
  inputAttrs: {},
  disabled: false,
  isInline: false,
  isLabelHidden: false,
  labelTextAttrs: {},
  name: null,
  onBlur: undefined,
  onChange: undefined,
  onFocus: undefined,
  value: undefined,
};
export const Checkbox = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'checked',
    'defaultChecked',
    'inputAttrs',
    'id',
    'disabled',
    'isInline',
    'isLabelHidden',
    'labelText',
    'labelTextAttrs',
    'name',
    'onBlur',
    'onChange',
    'onFocus',
    'value',
    'className',
  ]);
  const theme = useContext(ThemeContext);
  /*const controlInputAttrs = { ...p.inputAttrs };
  if (p.checked !== undefined) {
    controlInputAttrs.checked = p.checked;
  } else {
    controlInputAttrs.defaultChecked = p.defaultChecked;
  }*/
  const checkboxClasses = createMemo(() => classNames(cx('checkbox', { 'is-inline': p.isInline }, theme.className), p.className));
  const labelClasses = createMemo(() => cx(['label', { 'is-disabled': p.disabled }, { 'is-mobile': isConsideredMobileDevice() }, p.labelTextAttrs.className]));
  const inputClasses = createMemo(() => cx(['native-input', p.inputAttrs.className]));
  const labelTextClasses = createMemo(() => cx(['label-text', { 'is-hidden': p.isLabelHidden }, { 'is-mobile': isConsideredMobileDevice() }]));
  const controlInputAttrs = createMemo(() => (p.isLabelHidden ? { 'aria-label': p.labelText } : {}));
  const labelTextContainer = (
    <>
      {p.isLabelHidden ? (
        <span {...p.labelTextAttrs} className={labelTextClasses()} />
      ) : (
        <span {...p.labelTextAttrs} className={labelTextClasses()}>
          {p.labelText}
        </span>
      )}
    </>
  );

  return (
    <div {...customProps} className={checkboxClasses()}>
      <label htmlFor={p.id} className={labelClasses()}>
        <input
          {...controlInputAttrs()}
          checked={p.checked}
          type="checkbox"
          id={p.id}
          disabled={p.disabled}
          name={p.name}
          value={p.value}
          onChange={p.onChange}
          onFocus={p.onFocus}
          onBlur={p.onBlur}
          className={inputClasses()}
        />
        {labelTextContainer}
      </label>
    </div>
  );
};

Checkbox.isCheckbox = true;
