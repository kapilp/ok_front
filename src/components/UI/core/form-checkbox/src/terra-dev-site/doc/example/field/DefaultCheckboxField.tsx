import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Checkbox } from '../../../../Checkbox';
import { CheckboxField } from '../../../../CheckboxField';
export const DefaultCheckboxFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
  });
  const handleOnClick = () => {
    setState({ isInvalid: !state.isInvalid });
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <div>
        <CheckboxField error={errorMessage} isInvalid={state.isInvalid} legend="Do you have experience with any of the following?">
          <Checkbox id="experience-indesign" name="experience[]" labelText="InDesign" />
          <Checkbox id="experience-photoshop" name="experience[]" labelText="Photoshop" />
          <Checkbox id="experience-illustrator" name="experience[]" labelText="Illustrator" />
        </CheckboxField>
      </div>
      <hr />
      <button type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
