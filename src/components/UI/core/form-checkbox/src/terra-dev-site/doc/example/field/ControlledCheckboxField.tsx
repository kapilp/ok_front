import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Checkbox } from '../../../../Checkbox';
import { CheckboxField } from '../../../../CheckboxField';
export const ControlledCheckboxFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
    selectedAnswers: [],
  });
  const handleOnChange = (e: Event) => {
    setState(state => {
      if (e.currentTarget.checked) {
        state.selectedAnswers.push(e.currentTarget.value);
      } else {
        state.selectedAnswers.splice(state.selectedAnswers.indexOf(e.currentTarget.value), 1);
      }
    });
  };
  const handleOnClick = () => {
    setState({ isInvalid: !state.isInvalid });
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <div>
        <CheckboxField
          legend="What departments are you looking to work for?"
          help="These are the core areas we need for graphic designers"
          isInvalid={state.isInvalid || state.selectedAnswers.length <= 0}
          error={state.isInvalid ? errorMessage : 'You must be willing to work in one of these departments'}
          required
        >
          <Checkbox id="ux-dept" name="dept[]" labelText="UX/Interaction Design" onChange={handleOnChange} value="ux" />
          <Checkbox id="magazine-dept" name="dept[]" labelText="Magazine Advertisements" onChange={handleOnChange} value="magazine" />
          <Checkbox id="website-dept" name="dept[]" labelText="Website Advertisements" onChange={handleOnChange} value="website" />
          <Checkbox id="events-dept" name="dept[]" labelText="Event Promotions" onChange={handleOnChange} value="events" />
        </CheckboxField>
      </div>
      <hr />
      <button type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
