import { Checkbox } from '../../../../Checkbox';

export const MobileCheckboxExample = () => {
  return <Checkbox id="mobileCheckbox" labelText="Toggle to trigger resize. Window size needs to be less than or equal to 1024px." />;
};
