import { Checkbox } from '../../../../Checkbox';

export const DefaultCheckboxExample = () => <Checkbox id="defaultCheckbox" labelText="Default Checkbox" />;
