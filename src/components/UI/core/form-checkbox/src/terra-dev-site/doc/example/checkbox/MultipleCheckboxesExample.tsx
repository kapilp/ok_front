import { Checkbox } from '../../../../Checkbox';
export const MultipleCheckboxesExample = () => (
  <div>
    <Checkbox id="first" labelText="First Checkbox" checked />
    <Checkbox id="second" labelText="Second Checkbox" />
    <Checkbox id="third" labelText="Third Checkbox" />
  </div>
);
