import { Checkbox } from '../../../../Checkbox';
export const InlineCheckboxesExample = () => (
  <div>
    <Checkbox id="firstInline" labelText="First Checkbox" isInline />
    <Checkbox id="secondInline" labelText="Second Checkbox" isInline />
    <Checkbox id="thirdInline" labelText="Third Checkbox" isInline />
  </div>
);
