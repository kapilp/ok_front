import { Checkbox } from '../../../../Checkbox';
export const HiddenLabelCheckboxExample = () => (
  <div>
    <Checkbox labelText="Can you see me?" isLabelHidden />
  </div>
);
