import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Checkbox } from '../../../../Checkbox';
import { CheckboxField } from '../../../../CheckboxField';
import classNames from 'classnames/bind';
import styles from './InlineCheckboxField.module.scss';
const cx = classNames.bind(styles);
export const InlineCheckboxFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
    selectedAnswers: [],
    toggleInline: false,
  });
  const handleOnChange = e => {
    setState(state => {
      if (e.currentTarget.checked) {
        state.selectedAnswers.push(e.currentTarget.value);
      } else {
        state.selectedAnswers.splice(state.selectedAnswers.indexOf(e.currentTarget.value), 1);
      }
    });
  };
  const handleOnClick = e => {
    if (e.currentTarget.id === 'inline') {
      setState({ toggleInline: !state.toggleInline });
    } else {
      setState({ isInvalid: !state.isInvalid });
    }
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <button className={cx('checkbox-button-wrapper')} id="inline" type="button" aria-label="Toggle Inline" onClick={handleOnClick}>
        Toggle Inline
      </button>
      <button className={cx('checkbox-button-wrapper')} id="invalid" type="button" aria-label="Toggle Inline" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
      <hr />
      <div>
        <CheckboxField error={errorMessage} isInline={state.toggleInline} isInvalid={state.isInvalid} legend="Do you have experience with any of the following?">
          <Checkbox id="inline-experience-indesign" name="experience[]" labelText="InDesign" />
          <Checkbox id="inline-experience-photoshop" name="experience[]" labelText="Photoshop" />
          <Checkbox id="inline-experience-illustrator" name="experience[]" labelText="Illustrator" />
        </CheckboxField>
        <CheckboxField
          error={errorMessage}
          isInline={state.toggleInline}
          isInvalid={state.isInvalid}
          legend="Do you also have experience with any of the cutting edge technologies?"
        >
          <Checkbox id="experience-ie9" name="experience[]" labelText="IE9" />
          <Checkbox id="experience-flase" name="experience[]" labelText="Flash" />
          <Checkbox id="experience-punchcards" name="experience[]" labelText="Punch Cards" />
        </CheckboxField>
      </div>
    </div>
  );
};
