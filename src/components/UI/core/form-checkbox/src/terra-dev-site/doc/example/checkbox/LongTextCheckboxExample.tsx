import { Checkbox } from '../../../../Checkbox';
export const LongTextCheckboxExample = () => (
  <Checkbox
    id="longTextCheckbox"
    labelText={
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ' +
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
    }
  />
);
