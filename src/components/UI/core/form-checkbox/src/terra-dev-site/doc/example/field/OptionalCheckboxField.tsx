import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Checkbox } from '../../../../Checkbox';
import { CheckboxField } from '../../../../CheckboxField';
type undefinedState = {
  isInvalid?: boolean;
  selectedAnswers?: undefined[];
};
export const OptionalCheckboxFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
    selectedAnswers: [],
  });
  const handleOnChange = e => {
    if (e.currentTarget.checked) {
      state.selectedAnswers.push(e.currentTarget.value);
    } else {
      state.selectedAnswers.splice(state.selectedAnswers.indexOf(e.currentTarget.value), 1);
    }
  };
  const handleOnClick = () => {
    setState({ isInvalid: !state.isInvalid });
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <div>
        <CheckboxField
          error={errorMessage}
          isInvalid={state.isInvalid}
          legend="Do you wish to use any of our services for new hires?"
          help="These are not required, but make it easier to transition to our city"
          showOptional
        >
          <Checkbox id="roommate-service" name="service[]" labelText="Roommate Search" onChange={handleOnChange} value="roommate" />
          <Checkbox id="location-service" name="service[]" labelText="Relocation Assistance" onChange={handleOnChange} value="relocation" />
          <Checkbox id="city-tour-service" name="service[]" labelText="City Tours" onChange={handleOnChange} value="city_tours" />
        </CheckboxField>
      </div>
      <hr />
      <button type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
