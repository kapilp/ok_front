import { Checkbox } from '../../../../Checkbox';
export const DisabledCheckboxExample = () => (
  <div>
    <Checkbox id="disabledCheckbox" labelText="Disabled Checkbox" disabled />
    <Checkbox id="disabledcheckedCheckbox" labelText="Disabled and Checked Checkbox" checked disabled />
  </div>
);
