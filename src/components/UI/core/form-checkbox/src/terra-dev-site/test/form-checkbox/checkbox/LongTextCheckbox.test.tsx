import {JSX} from "solid-js";
import Checkbox from "../../../../Checkbox";
const checkbox = () => (
  <Checkbox
    id="longText"
    labelText={
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua " +
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua"
    }
  />
);
export default checkbox;
