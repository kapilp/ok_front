import {JSX} from "solid-js";
import Checkbox from "../../../../Checkbox";
const checkbox = () => (
  <Checkbox id="hidden" labelText="can you see me?" isLabelHidden />
);
export default checkbox;
