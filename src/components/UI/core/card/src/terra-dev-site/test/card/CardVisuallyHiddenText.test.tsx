import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Card from '../../../Card';
const CardVisuallyHiddenText = () => (
  <div>
    <Card visuallyHiddenText="This is a Hello World Card Introduction">Hello World!!</Card>
  </div>
);
export default CardVisuallyHiddenText;
