import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Card from '../../../Card';
import CardBody from '../../../CardBody';
export default () => (
  <Card>
    <CardBody isContentCentered>Centered content</CardBody>
  </Card>
);
