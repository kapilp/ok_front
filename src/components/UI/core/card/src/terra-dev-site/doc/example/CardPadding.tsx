import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Card } from '../../../Card';
import { CardBody } from '../../../CardBody';
export const CardPadding = () => (
  <div>
    <Card>
      <CardBody>Hello World!!</CardBody>
    </Card>
  </div>
);
