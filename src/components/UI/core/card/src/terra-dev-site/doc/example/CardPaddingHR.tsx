import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Card } from '../../../Card';
import styles from './CardPaddingHR.module.scss';
import { CardBody } from '../../../CardBody';
const cx = classNames.bind(styles);
export const CardPaddingHR = () => (
  <div>
    <Card>
      <CardBody>Hello World Above The Line!!</CardBody>
      <hr className={cx('horizontal-rule')} />
      <CardBody>Hello World Below The Line!!</CardBody>
    </Card>
  </div>
);
