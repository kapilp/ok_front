import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Card } from '../../../Card';
export const CardDefault = () => (
  <div>
    <Card>Hello World!!</Card>
  </div>
);
