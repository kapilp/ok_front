import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Card } from '../../../Card';
export const CardRaised = () => (
  <div>
    <Card variant="raised">Hello World!!</Card>
  </div>
);
