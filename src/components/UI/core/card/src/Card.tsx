import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Card.module.scss';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
const cx = classNamesBind.bind(styles);
enum CardVariants {
  DEFAULT = 'default',
  RAISED = 'raised',
}
interface Properties extends JSX.HTMLAttributes<HTMLElement> {
  /**
   * Child Nodes
   */
  children: JSX.Element; // required
  /**
   * Sets the card variant to change the style for different use cases. One of `default`,  `raised`.
   */
  variant?: CardVariants;
  /**
   * Text that describes the badge to a screen reader. Use this
   * if more information is needed to accurately describe
   * this card to screen reader users.
   */
  visuallyHiddenText?: string;
}

export const Card = (props: Properties) => {
  props = mergeProps(
    {},
    {
      variant: CardVariants.DEFAULT,
    },
    props,
  );
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, ['children', 'variant', 'visuallyHiddenText']);

  return (
    <article {...customProps} className={classNames(cx(['card', props.variant, theme.className]), customProps.className)}>
      {props.visuallyHiddenText && <VisuallyHiddenText text={props.visuallyHiddenText} />}
      {props.children}
    </article>
  );
};
export { CardVariants };
