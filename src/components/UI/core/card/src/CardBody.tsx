import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './CardBody.module.scss';
const cx = classNamesBind.bind(styles);

interface Properties {
  /**
   * Child Nodes
   */
  children?: JSX.Element;
  /**
   * Provides themeable padding vertical
   */
  hasPaddingVertical?: boolean;
  /**
   * Provides themeable padding horizontal
   */
  hasPaddingHorizontal?: boolean;
  /**
   * Sets the content of the card to be centered
   */
  isContentCentered?: boolean;
}

export const CardBody = (props: Properties) => {
  props = mergeProps(
    {},
    {
      hasPaddingHorizontal: true,
      hasPaddingVertical: true,
      isContentCentered: false,
    },
    props,
  );

  const theme = useContext(ThemeContext);
  const [extracted, customProps] = splitProps(props, ['children', 'hasPaddingVertical', 'hasPaddingHorizontal', 'isContentCentered']);
  return (
    <div
      {...customProps}
      className={classNames(
        cx([
          'tCardBody',
          { 'vertical-padding': props.hasPaddingVertical },
          { 'horizontal-padding': props.hasPaddingHorizontal },
          { 'center-content': props.isContentCentered },
          theme.className,
        ]),
        customProps.className,
      )}
    >
      {props.children}
    </div>
  );
};
