import { ButtonGroupButton } from './ButtonGroupButton';
// import ButtonGroupUtils from './ButtonGroupUtils';
import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './ButtonGroup.module.scss';
const cx = classNamesBind.bind(styles);

interface IButtonGroupProps extends JSX.HTMLAttributes<Element> {
  /**
   * Child nodes
   */
  children?: Node;
  /**
   * Allows user to set button group to fill container width.
   */
  isBlock?: boolean;
  /**
   * Callback function when the state changes. Parameters are (event, key).
   */
  onChange?: (e: Event, key: string) => void;
  /**
   * A list of keys of the buttons that should be selected.
   */
  selectedKeys?: string[];
}
const defaultProps = {
  children: [],
  isBlock: false,
  selectedKeys: [],
};

export const ButtonGroup = (props: IButtonGroupProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  //context: any;

  /*const handleOnChange = (event, key) => {
    if (props.onChange) {
      props.onChange(event, key);
    }
  };
  const wrapOnClick = item => {
    const { onClick } = item.props;
    return event => {
      handleOnChange(event, item.key);
      if (onClick) {
        onClick(event);
      }
    };
  };*/

  //const theme = context;

  //todo it looks impossible to convert this to solidjs
  /*const allButtons = props.children ? [] : undefined;
  React.Children.forEach(props.children, child => {
    const isSelected = props.selectedKeys.indexOf(child.key) > -1;
    const cloneChild = React.cloneElement(child, {
      onClick: wrapOnClick(child),
      className: cx([{ 'is-selected': isSelected && !child.props.isDisabled }, child.props.className]),
      'aria-pressed': isSelected || null,
    });
    allButtons.push(cloneChild);
  });*/
  const [extracted, customProps] = splitProps(props, ['children', 'isBlock', 'onChange', 'selectedKeys']);
  return (
    <div {...customProps} className={classNames(cx('button-group', { 'is-block': props.isBlock }, theme.className), customProps.className)}>
      {props.children}
    </div>
  );
};

// ButtonGroup.Utils = ButtonGroupUtils;

//      <For each={props.children}>{i=><div onClick={event=>handleOnChange(event,)}>{i}</div>}</For>
