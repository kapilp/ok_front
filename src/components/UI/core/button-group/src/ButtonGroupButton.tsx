import * as KeyCode from 'keycode-js';
import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Button, ButtonVariants } from '../../button/src/Button';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
//import ThemeContext from 'terra-theme-context';
import styles from './ButtonGroup.module.scss';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);

interface IButtonGroupButtonProps extends JSX.HTMLAttributes<Element> {
  /**
   * An optional icon. If an icon is provided, it will be an icon only button and the provided text is set as the aria-label for accessibility.
   */
  icon?: JSX.Element;
  /**
   * Whether or not the button should be disabled.
   */
  isDisabled?: boolean;
  /**
   * Callback function triggered when button loses focus.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when clicked.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * Callback function triggered when button gains focus.
   */
  onFocus?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when key is pressed.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * Callback function triggered when key is released.
   */
  onKeyUp?: (e: KeyboardEvent) => void;
  /**
   * Sets the button text. If an icon is provided, it will be an icon only button and this text is set as the aria-label for accessibility.
   */
  text: string;
}
const defaultProps = {
  isDisabled: false,
};

type ButtonGroupButtonState = {
  focused: boolean;
};
export const ButtonGroupButton = (props: IButtonGroupButtonProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  //context: any;
  let shouldShowFocus = false;

  const [state, setState] = createStore({
    focused: false,
  } as ButtonGroupButtonState);

  const handleOnBlur = (event: FocusEvent) => {
    if (document.activeElement === event.currentTarget && state.focused) {
      shouldShowFocus = true;
    } else {
      shouldShowFocus = false;
      setState({ focused: false });
    }
    if (props.onBlur) {
      props.onBlur(event);
    }
  };
  const handleKeyDown = (event: KeyboardEvent) => {
    // Add focus styles for keyboard navigation.
    // The onFocus event doesn't get triggered in some browsers, hence, the focus state needs to be managed here.
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
    }
    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  };
  const handleKeyUp = (event: KeyboardEvent) => {
    // Apply focus styles for keyboard navigation.
    // The onFocus event doesn't get triggered in some browsers, hence, the focus state needs to be managed here.
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
      shouldShowFocus = true;
    }
    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  };
  const handleFocus = (event: FocusEvent) => {
    if (shouldShowFocus) {
      setState({ focused: true });
    }
    if (props.onFocus) {
      props.onFocus(event);
    }
  };

  //const theme = context;
  const [extracted, customProps] = splitProps(props, ['icon', 'isDisabled', 'onFocus']);
  return (
    <Button
      {...customProps}
      icon={props.icon}
      isDisabled={props.isDisabled}
      isIconOnly={props.icon != null}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
      onFocus={handleFocus}
      variant={ButtonVariants.NEUTRAL}
      className={classNames(
        cx('button-group-button', { 'is-disabled': props.isDisabled }, { 'is-focused': state.focused && !props.isDisabled }, theme.className),
        customProps.className,
      )}
    />
  );
};
