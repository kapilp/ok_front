import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ButtonGroup } from '../../../ButtonGroup';

import { handleMultiSelectedKeys } from '../../../ButtonGroupUtils';
import { ButtonGroupButton } from '../../../ButtonGroupButton';

type ButtonGroupIsBlockState = {
  selectedKeys?: undefined[];
};
export const ButtonGroupIsBlock = (props: {}) => {
  const [state, setState] = createStore({ selectedKeys: [] });

  const handleSelection = (event: Event, key: string) => {
    event.preventDefault();
    setState({
      selectedKeys: handleMultiSelectedKeys(state.selectedKeys, key),
    });
  };

  return (
    <ButtonGroup id="button-block-group" isBlock onChange={handleSelection} selectedKeys={state.selectedKeys}>
      <ButtonGroupButton text="Button 1" key="block-select1" />
      <ButtonGroupButton text="Button 2" key="block-select2" />
      <ButtonGroupButton text="Button 3" key="block-select3" />
    </ButtonGroup>
  );
};
