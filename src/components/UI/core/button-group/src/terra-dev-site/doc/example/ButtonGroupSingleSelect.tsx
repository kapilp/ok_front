import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ButtonGroup } from '../../../ButtonGroup';

import { shouldHandleSingleSelection } from '../../../ButtonGroupUtils';
import { ButtonGroupButton } from '../../../ButtonGroupButton';
type ButtonGroupSingleSelectState = {
  selectedKey?: string[];
};
export const ButtonGroupSingleSelect = () => {
  const [state, setState] = createStore({ selectedKey: ['single-select1'] });
  const handleSelection = (event: Event, selectedKey: string) => {
    if (shouldHandleSingleSelection(state.selectedKey, selectedKey)) {
      event.preventDefault();
      setState({ selectedKey: [selectedKey] });
    }
  };

  return (
    <ButtonGroup id="controlled-button-group" onChange={handleSelection} selectedKeys={state.selectedKey}>
      <ButtonGroupButton text="Single-Select 1" key="single-select1" />
      <ButtonGroupButton text="Single-Select 2" key="single-select2" />
      <ButtonGroupButton text="Single-Select 3" key="single-select3" />
    </ButtonGroup>
  );
};
