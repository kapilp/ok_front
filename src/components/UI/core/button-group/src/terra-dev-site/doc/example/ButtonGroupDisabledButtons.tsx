import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ButtonGroup } from '../../../ButtonGroup';
import { ButtonGroupButton } from '../../../ButtonGroupButton';

export const ButtonGroupDisabledButtons = () => (
  <ButtonGroup id="button-group-not-selectable">
    <ButtonGroupButton text="Button 1" key="1" />
    <ButtonGroupButton text="Button 2" key="2" isDisabled />
    <ButtonGroupButton text="Button 3" key="3" />
    <ButtonGroupButton text="Button 4" key="4" isDisabled />
  </ButtonGroup>
);
