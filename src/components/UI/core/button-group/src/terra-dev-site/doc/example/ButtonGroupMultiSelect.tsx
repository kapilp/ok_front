import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ButtonGroup } from '../../../ButtonGroup';
import { handleMultiSelectedKeys } from '../../../ButtonGroupUtils';
import { ButtonGroupButton } from '../../../ButtonGroupButton';

type ButtonGroupMultiSelectState = {
  selectedKeys?: string[];
};
export const ButtonGroupMultiSelect = () => {
  const [state, setState] = createStore({ selectedKeys: ['multi-select1', 'multi-select3'] });

  const handleSelection = (event: Event, key: string) => {
    event.preventDefault();
    setState(
      produce(prevState => ({
        selectedKeys: handleMultiSelectedKeys(prevState.selectedKeys, key),
      })),
    );
  };

  return (
    <ButtonGroup id="button-group-multi-select" onChange={handleSelection} selectedKeys={state.selectedKeys}>
      <ButtonGroupButton text="Multi-Select 1" key="multi-select1" />
      <ButtonGroupButton text="Multi-Select 2" key="multi-select2" />
      <ButtonGroupButton text="Multi-Select 3" key="multi-select3" />
    </ButtonGroup>
  );
};
