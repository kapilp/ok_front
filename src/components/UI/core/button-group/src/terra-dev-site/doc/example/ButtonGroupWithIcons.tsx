import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ButtonGroup } from '../../../ButtonGroup';

import { SvgIconCaretDown } from '../../../../../icon/src/icon/IconCaretDown';
import { SvgIconCaretUp } from '../../../../../icon/src/icon/IconCaretUp';
import { ButtonGroupButton } from '../../../ButtonGroupButton';

export const ButtonGroupWithIcons = () => (
  <div>
    <div>
      <ButtonGroup>
        <ButtonGroupButton text="Up" key="upButton" />
        <ButtonGroupButton text="Down" key="downButton" />
      </ButtonGroup>
    </div>
    <br />
    <div>
      <ButtonGroup>
        <ButtonGroupButton text="Up" icon={SvgIconCaretUp} key="upIcon" />
        <ButtonGroupButton text="Down" icon={SvgIconCaretDown} key="downIcon" />
      </ButtonGroup>
    </div>
  </div>
);
