import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
type ButtonGroupIsBlockState = {
  selectedKeys?: undefined[];
};
class ButtonGroupIsBlock extends React.Component<{}, ButtonGroupIsBlockState> {
  constructor(props) {
    super(props);
    state = { selectedKeys: [] };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, key) {
    event.preventDefault();
    setState(prevState => ({
      selectedKeys: ButtonGroup.Utils.handleMultiSelectedKeys(prevState.selectedKeys, key),
    }));
  }
  render() {
    return (
      <ButtonGroup id="button-block-group" isBlock onChange={handleSelection} selectedKeys={state.selectedKeys}>
        <ButtonGroup.Button text="Button 1" key="single-select1" />
        <ButtonGroup.Button text="Button 2" key="single-select2" />
        <ButtonGroup.Button text="Button 3" key="single-select3" />
      </ButtonGroup>
    );
  }
}
export default ButtonGroupIsBlock;
