import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
const TextButtonGroup = () => (
  <ButtonGroup id="button-group-text">
    <ButtonGroup.Button text="Text1" key="text1" />
    <ButtonGroup.Button text="Text2" key="text2" />
  </ButtonGroup>
);
export default TextButtonGroup;
