import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
type ButtonGroupMultiSelectState = {
  selectedKeys?: undefined[];
  join?: any;
};
class ButtonGroupMultiSelect extends React.Component<{}, ButtonGroupMultiSelectState> {
  constructor(props) {
    super(props);
    state = { selectedKeys: [] };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, key) {
    event.preventDefault();
    setState(prevState => ({
      selectedKeys: ButtonGroup.Utils.handleMultiSelectedKeys(prevState.selectedKeys, key),
    }));
  }
  render() {
    return (
      <div>
        <h3>
          Selected Button(s): <span id="selected-keys">{state.selectedKeys.join(', ')}</span>
        </h3>
        <ButtonGroup id="button-group-multi-select" onChange={handleSelection} selectedKeys={state.selectedKeys}>
          <ButtonGroup.Button text="Multi-Select 1" key="1" />
          <ButtonGroup.Button text="Multi-Select 2" key="2" />
          <ButtonGroup.Button text="Multi-Select 3" key="3" />
          <ButtonGroup.Button text="Multi-Select 4" key="4" />
        </ButtonGroup>
      </div>
    );
  }
}
export default ButtonGroupMultiSelect;
