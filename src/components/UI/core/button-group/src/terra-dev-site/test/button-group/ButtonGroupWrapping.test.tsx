import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
const ButtonGroupWrapping = () => (
  <ButtonGroup id="button-group-text">
    <ButtonGroup.Button text="A-Really-Long Text" key="text1" />
    <ButtonGroup.Button text="A-Really-Long Text" key="text2" />
    <ButtonGroup.Button text="A-Really-Long Text" key="text3" />
    <ButtonGroup.Button text="A-Really-Long Text" key="text4" />
    <ButtonGroup.Button text="A-Really-Long Text" key="text5" />
  </ButtonGroup>
);
export default ButtonGroupWrapping;
