import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
type ButtonGroupIsBlockLongTextState = {
  selectedKeys?: undefined[];
};
class ButtonGroupIsBlockLongText extends React.Component<{}, ButtonGroupIsBlockLongTextState> {
  constructor(props) {
    super(props);
    state = { selectedKeys: [] };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, key) {
    event.preventDefault();
    setState(prevState => ({
      selectedKeys: ButtonGroup.Utils.handleMultiSelectedKeys(prevState.selectedKeys, key),
    }));
  }
  render() {
    return (
      <ButtonGroup id="button-block-group-long-text" isBlock onChange={handleSelection} selectedKeys={state.selectedKeys}>
        <ButtonGroup.Button text="Is-Block Single-Select 1" key="single-select1" />
        <ButtonGroup.Button text="Is-Block Single-Select 2 Longer Text" key="single-select2" />
        <ButtonGroup.Button
          text="Is-Block Single-Select 3 Extremely Long Text to Test Text Wrapping Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"
          key="single-select3"
        />
      </ButtonGroup>
    );
  }
}
export default ButtonGroupIsBlockLongText;
