import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ButtonGroup from '../../../ButtonGroup';
type ButtonGroupSingleSelectState = {
  selectedKey?: string;
};
class ButtonGroupSingleSelect extends React.Component<{}, ButtonGroupSingleSelectState> {
  constructor(props) {
    super(props);
    state = { selectedKey: '1' };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, selectedKey) {
    if (ButtonGroup.Utils.shouldHandleSingleSelection(state.selectedKey, selectedKey)) {
      event.preventDefault();
      setState({ selectedKey });
    }
  }
  render() {
    return (
      <div>
        <h3>
          Selected Button: <span id="selected-key">{state.selectedKey}</span>
        </h3>
        <ButtonGroup id="button-group-single-select" onChange={handleSelection} selectedKeys={[state.selectedKey]}>
          <ButtonGroup.Button text="Single-Select 1" key="1" />
          <ButtonGroup.Button text="Single-Select 2" key="2" />
          <ButtonGroup.Button text="Single-Select 3" key="3" />
          <ButtonGroup.Button text="Single-Select 4" key="4" />
        </ButtonGroup>
      </div>
    );
  }
}
export default ButtonGroupSingleSelect;
