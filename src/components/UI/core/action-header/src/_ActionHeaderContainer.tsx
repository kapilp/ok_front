import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './ActionHeaderContainer.module.scss';
const cx = classNamesBind.bind(styles);

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Child element to be displayed on the right end of the header.
   * The element passed as children will be decorated with flex attributes.
   */
  children?: JSX.Element;
  /**
   * Content to be displayed at the start of the header, placed before the title.
   */
  startContent?: JSX.Element;
  /**
   * Text to be displayed as the title in the header bar.
   */
  title?: string;
  /**
   * Content to be displayed at the end of the header.
   * The element passed as endContent will be wrapped in a div with flex attributes.
   */
  endContent?: JSX.Element;
  /**
   * Sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`. This helps screen readers to announce appropriate heading levels.
   * Changing 'level' will not visually change the style of the content.
   */
  level: 1 | 2 | 3 | 4 | 5 | 6;
}
const defaultProps = {
  title: undefined,
  startContent: undefined,
  endContent: undefined,
};
export const ActionHeaderContainer = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  // Todo fix this
  const [e, customProps] = splitProps(props, ['children', 'title', 'startContent', 'endContent', 'level', 'className']);
  const content = () => e.children;
  // todo fix this:
  /*const content = React.Children.map(children, child =>
    React.cloneElement(child, {
      className: cx(["flex-collapse", children.props.className])
    })
  );*/
  const titleElement = () =>
    e.title ? (
      <div className={cx('title-container')}>
        <Dynamic component={`h${e.level}`} className={cx('title')}>
          {e.title}
        </Dynamic>
      </div>
    ) : undefined;
  return (
    <div {...customProps} className={classNames(cx(['flex-header', theme.className]), e.className)}>
      {e.startContent && <div className={cx('flex-end')}>{e.startContent}</div>}
      <div className={cx('flex-fill')}>{titleElement}</div>
      {content()}
      {e.endContent && <div className={cx('flex-end')}>{e.endContent}</div>}
    </div>
  );
};
