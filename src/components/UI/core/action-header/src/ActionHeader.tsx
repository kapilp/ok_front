import {  JSX, createMemo, Match, mergeProps, Show, splitProps, Switch, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
// import { FormattedMessage } from 'react-intl';

import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { ActionHeaderContainer } from './_ActionHeaderContainer';

import { Button, ButtonVariants } from '../../button/src/Button';
import styles from './ActionHeader.module.scss';

const cx = classNames.bind(styles);

interface Properties {
  /**
   * Displays a single terra `Collapsible Menu View` (_Not provided by `Action Header`_) child element on the right end of the header.
   */
  children?: JSX.Element;
  /**
   * Optionally sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`. Default `level=1`. This helps screen readers to announce appropriate heading levels.
   * Changing 'level' will not visually change the style of the content.
   */
  level?: 1 | 2 | 3 | 4 | 5 | 6;
  /**
   * Callback function for when the close button is clicked.
   * On small viewports, this will be triggered by a back button if onBack is not set.
   */
  onClose?: () => void;
  /**
   * Callback function for when the back button is clicked. The back button will not display if this is not set.
   */
  onBack?: () => void;
  /**
   * Callback function for when the expand button is clicked.
   * The expand button will not display if this is not set or on small viewports.
   * Only the expand button will be rendered if onMaximize and onMinimize are set.
   *
   * *Note: If `onBack` is set, the maximize button will not appear and a custom maximize button must be provided
   * as a child inside a `Collapsible Menu View`.*
   */
  onMaximize?: () => void;
  /**
   * Callback function for when the minimize button is clicked.
   * The minimize button will not display if this is not set or on small viewports.
   * Only the expand button will be rendered if both onMaximize and onMinimize are set.
   *
   * *Note: If `onBack` is set, the minimize button will not appear and a custom minimize button must be provided
   * as a child inside a `Collapsible Menu View`.*
   */
  onMinimize?: () => void;
  /**
   * Callback function for when the next button is clicked. The previous-next button group will display if either this or onPrevious is set but the button for the one not set will be disabled.
   */
  onNext?: () => void;
  /**
   * Callback function for when the previous button is clicked. The previous-next button group will display if either this or onNext is set but the button for the one not set will be disabled.
   */
  onPrevious?: () => void;
  /**
   * Text to be displayed as the title in the header bar.
   */
  title?: string;
}
const defaultProps = {
  title: undefined,
  level: 1,
  onClose: undefined,
  onBack: undefined,
  onMaximize: undefined,
  onMinimize: undefined,
  onNext: undefined,
  onPrevious: undefined,
  children: undefined,
};
export const ActionHeader = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [e, customProps] = splitProps(props, ['title', 'level', 'onClose', 'onBack', 'onMaximize', 'onMinimize', 'onPrevious', 'onNext', 'children']);

  const isCloseButton = () => e.onClose;
  const closeButton = () =>
    e.onClose ? (
      <Button
        className={cx(['header-button', 'close-button'])}
        data-terra-action-header="close-button"
        isIconOnly
        icon={() => <span className={cx(['header-icon', 'close'])} />}
        text={'Close'}
        onClick={e.onClose}
        variant={ButtonVariants.UTILITY}
      />
    ) : null;
  const isBackButton = () => e.onBack;
  const backButton = e.onBack ? (
    <Button
      className={cx(['header-button', 'back-button'])}
      data-terra-action-header="back-button"
      isIconOnly
      icon={() => <span className={cx(['header-icon', 'back'])} />}
      text={'Back'}
      onClick={e.onBack}
      variant={ButtonVariants.UTILITY}
    />
  ) : null;
  const isExpandButton = () => !e.onBack && (e.onMaximize || e.onMinimize);
  const expandButton = (
    <>
      {!e.onBack && (
        <Switch>
          <Match when={!!e.onMaximize}>
            <Button
              className={cx(['header-button', 'maximize-button'])}
              data-terra-action-header="maximize-button"
              isIconOnly
              icon={() => <span className={cx(['header-icon', 'maximize'])} />}
              text={'Maximize'}
              onClick={e.onMaximize}
              variant={ButtonVariants.UTILITY}
            />
          </Match>
          <Match when={!!e.onMinimize}>
            <Button
              className={cx(['header-button', 'minimize-button'])}
              data-terra-action-header="minimize-button"
              isIconOnly
              icon={() => <span className={cx(['header-icon', 'minimize'])} />}
              text={'Minimize'}
              onClick={e.onMinimize}
              variant={ButtonVariants.UTILITY}
            />
          </Match>
        </Switch>
      )}
    </>
  );
  const isPreviousNextButtonGroup = () => e.onPrevious || e.onNext;
  const previousNextButtonGroup = () =>
    e.onPrevious || e.onNext ? (
      <div className={cx('previous-next-button-group')}>
        <Button
          className={cx(['header-button', 'previous-button'])}
          data-terra-action-header="previous-button"
          isIconOnly
          icon={() => <span className={cx(['header-icon', 'previous'])} />}
          text={'Previous'}
          onClick={e.onPrevious}
          isDisabled={e.onPrevious === undefined}
          variant={ButtonVariants.UTILITY}
        />

        <Button
          className={cx(['header-button', 'next-button'])}
          data-terra-action-header="next-button"
          isIconOnly
          icon={() => <span className={cx(['header-icon', 'next'])} />}
          text={'Next'}
          onClick={e.onNext}
          isDisabled={e.onNext === undefined}
          variant={ButtonVariants.UTILITY}
        />
      </div>
    ) : null;
  const leftButtons = () =>
    isBackButton() || isExpandButton() || isPreviousNextButtonGroup() ? (
      <div className={cx('left-buttons', theme.className)}>
        {backButton}
        {expandButton}
        {previousNextButtonGroup()}
      </div>
    ) : null;

  const rightButtons = () => (isCloseButton() ? <div className={cx('right-buttons', theme.className)}>{closeButton}</div> : null);

  return (
    <ActionHeaderContainer {...customProps} startContent={leftButtons()} title={e.title} endContent={rightButtons()} level={e.level}>
      {e.children}
    </ActionHeaderContainer>
  );
};
