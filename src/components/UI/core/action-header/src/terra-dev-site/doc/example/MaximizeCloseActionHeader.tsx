/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ActionHeader } from '../../../ActionHeader';
export const MaximizeCloseActionHeader = () => (
  <div>
    <br />
    <ActionHeader title="Maximize Close Action Header" onClose={() => alert('You clicked close!')} onMaximize={() => alert('You clicked maximize!')} />
    <br />
  </div>
);
