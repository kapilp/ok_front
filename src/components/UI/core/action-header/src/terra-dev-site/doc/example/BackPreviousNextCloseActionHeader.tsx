/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ActionHeader } from '../../../ActionHeader';
export const BackPreviousNextCloseActionHeader = () => (
  <div>
    <br />
    <ActionHeader
      title="Back Close Previous Next Action Header"
      onClose={() => alert('You clicked close!')}
      onBack={() => alert('You clicked back!')}
      onPrevious={() => alert('You clicked previous!')}
      onNext={() => alert('You clicked next!')}
    />
    <br />
  </div>
);
