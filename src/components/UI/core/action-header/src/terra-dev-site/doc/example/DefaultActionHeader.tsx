import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ActionHeader } from '../../../ActionHeader';
export const DefaultActionHeader = () => (
  <div>
    <br />
    <ActionHeader title="Default Action Header" />
    <br />
  </div>
);
