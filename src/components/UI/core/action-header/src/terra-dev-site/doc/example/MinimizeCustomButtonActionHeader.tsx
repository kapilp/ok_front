/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ActionHeader } from '../../../ActionHeader';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import styles from './ActionHeaderDocCommon.module.scss';

const cx = classNames.bind(styles);
export const MinimizeCustomButtonActionHeader = () => (
  <div>
    <br />
    <ActionHeader title="Minimize Custom Button Action Header" onMinimize={() => alert('You clicked minimize!')} onClose={() => alert('You clicked close!')}>
      <Placeholder className={cx('placeholder-wrapper')} title="Collapsible Menu View" />
    </ActionHeader>
    <br />
  </div>
);
