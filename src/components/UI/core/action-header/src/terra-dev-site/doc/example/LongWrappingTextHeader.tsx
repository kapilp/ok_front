/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ActionHeader } from '../../../ActionHeader';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import styles from './ActionHeaderDocCommon.module.scss';

const cx = classNames.bind(styles);
export const LongWrappingTextHeader = () => (
  <div>
    <br />
    <ActionHeader
      title={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla orci dolor, dignissim vitae risus vel, tristique egestas sapien.
      Vivamus blandit augue justo, id tincidunt justo luctus et. Morbi lacinia porttitor lacus, ac fermentum mauris tempus dapibus. Donec fringilla est ut ullamcorper consequat.
      Aliquam ornare efficitur ornare. Curabitur facilisis urna a congue gravida.
      Nulla accumsan non nisl sed elementum.
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.`}
      onMinimize={() => alert('You clicked minimize!')}
      onClose={() => alert('You clicked close!')}
    >
      <Placeholder className={cx('placeholder-wrapper')} title="Collapsible Menu View" />
    </ActionHeader>
    <br />
  </div>
);
