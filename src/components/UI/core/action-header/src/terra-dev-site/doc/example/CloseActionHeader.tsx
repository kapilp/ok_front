/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ActionHeader } from '../../../ActionHeader';
export const CloseActionHeader = () => (
  <div>
    <br />
    <ActionHeader title="Close Action Header" onClose={() => alert('You clicked close!')} />
    <br />
  </div>
);
