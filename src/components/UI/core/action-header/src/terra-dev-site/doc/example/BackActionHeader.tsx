/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ActionHeader } from '../../../ActionHeader';

export const BackActionHeader = () => (
  <div>
    <br />
    <ActionHeader title="Back Action Header" onBack={() => alert('You clicked back!')} />
    <br />
  </div>
);
