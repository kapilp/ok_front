/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ActionHeader from '../../../ActionHeader';
const ActionHeaderExample = () => <ActionHeader title="Back Action Header" onBack={() => alert('You clicked back!')} />;
export default ActionHeaderExample;
