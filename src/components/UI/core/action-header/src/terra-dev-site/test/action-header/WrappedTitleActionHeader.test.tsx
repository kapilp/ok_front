/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ActionHeader from '../../../ActionHeader';
export default () => (
  <ActionHeader
    title="Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Long Title Action Header"
    onBack={() => alert('You clicked back!')}
    onClose={() => alert('You clicked close!')}
  />
);
