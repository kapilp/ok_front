/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ActionHeader from '../../../ActionHeader';
const ActionHeaderExample = () => <ActionHeader title="Next Enabled Previous Disabled Action Header" onNext={() => alert('You clicked next!')} />;
export default ActionHeaderExample;
