/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import ActionHeader from '../../../ActionHeader';
const ActionHeaderExample = () => (
  <div>
    <ActionHeader title="Minimize Custom Button Action Header" onMaximize={() => alert('You clicked maximize!')}>
      <Button text="Custom Button" onClick={() => alert('You clicked me!')} />
    </ActionHeader>
  </div>
);
export default ActionHeaderExample;
