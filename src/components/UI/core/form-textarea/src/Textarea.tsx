import {
  createSignal,
  JSX,
  mergeProps,
  splitProps,
  useContext
} from "solid-js";
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import { ThemeContext } from "../../../framework/theme-context/src/ThemeContext";
import styles from "./Textarea.module.scss";

const cx = classNamesBind.bind(styles);
const IsMobileDevice = () =>
  window.matchMedia("(max-width: 1024px)").matches &&
  ("ontouchstart" in window ||
    // eslint-disable-next-line no-undef
    (window.DocumentTouch && document instanceof DocumentTouch) ||
    navigator.maxTouchPoints > 0 || // eslint-disable-line compat/compat
    navigator.msMaxTouchPoints > 0);
export const TextareaSize = {
  SMALL: "small",
  MEDIUM: "medium",
  LARGE: "large",
  FULL: "full"
};
const TEXTAREA_ROW_SIZES = {
  small: 2,
  medium: 5,
  large: 10
};

interface ITextareaProps extends JSX.HTMLAttributes<Element> {
  /**
   * String that labels the current element. 'aria-label' must be present,
   * for accessibility.
   */
  ariaLabel?: string;
  /**
   * The defaultValue of the textarea. Use this to create an uncontrolled textarea.
   */
  defaultValue?: string;
  /**
   * Whether the textarea is disabled.
   */
  disabled?: boolean;
  /**
   * Whether the textarea can be auto-resized vertically. _(Will be ignored if size attribute is set to "full".)_
   */
  isAutoResizable?: boolean;
  /**
   * Whether the text area displays as Incomplete. Use when no value has been provided. _(usage note?: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the text area displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Name of the input.
   */
  name?: string;
  /**
   * Function to trigger when user changes the textarea value. Provide a function to create a controlled input.
   */
  onChange?: (e: Event) => void;
  /**
   * Function to trigger when user focuses on this textarea.
   */
  onFocus?: (e: Event) => void;
  /**
   * Whether the input is required or not.
   */
  required?: boolean;
  /**
   * Value to set for the rows attribute of the textarea. This takes presidence over size when
   * setting the height of the textarea.
   */
  rows?: number;
  /**
   * The size of the textarea. _(Sizes the textarea by setting the rows attribute a corresponding preset value.)_
   */
  size?: "small" | "medium" | "large" | "full";
  /**
   * The value of the textarea. Use this to create a controlled textarea.
   */
  value?: string;
  /**
   * Function callback for the ref of the textarea. Useful for advanced
   * functionality such as managing focus, selection, or animations.
   */
  ref?: (el: HTMLElement) => void;
  /**
   * Whether or not the textarea is resizable.
   * `Disabled` textarea and the ones with size `full` are always non-resizable.
   */
  disableResize?: boolean;
}

const defaultProps = {
  defaultValue: undefined,
  disabled: false,
  name: null,
  isAutoResizable: false,
  isIncomplete: false,
  isInvalid: false,
  onChange: undefined,
  required: false,
  rows: null,
  size: "small",
  value: undefined,
  ref: undefined,
  disableResize: false
};

export const Textarea = (props: ITextareaProps) => {
  props = mergeProps({}, defaultProps, props);

  let isMobileDevice = IsMobileDevice();
  const [textarea, setTextArea] = createSignal<HTMLTextAreaElement>();
  // componentDidMount() {
  //   const lineHeight = Math.ceil(
  //     parseInt(window.getComputedStyle(textarea).lineHeight, 0)
  //   );
  //   textarea().currentLineHeight = lineHeight;
  //   setBaseHeights();
  //   if (props.isAutoResizable && !isMobileDevice) {
  //     resizeTextarea();
  //   }
  // }
  // Addresses https://github.com/cerner/terra-core/issues/2496.
  // min-height manipulation exists outside the virtual dom. React reconcilliation will fail to update this inline style, because it is not a prop.
  // This is apparent when swapping different sized TextArea components. To alleviate this, first reset the min-height.
  // This allows us to capture the correct scrollHeight, and use this correct value for min-height.
  // componentDidUpdate(prevProps) {
  //   if (
  //     props.size !== prevProps.size ||
  //     props.rows !== prevProps.rows
  //   ) {
  //     setBaseHeights();
  //   }
  // }
  const onFocus = (event: FocusEvent) => {
    if (props.isAutoResizable && !isMobileDevice) {
      const lineHeight = Math.ceil(parseInt(window.getComputedStyle(textarea).lineHeight, 0));
      if (textarea().currentLineHeight !== lineHeight) {
        textarea().currentLineHeight = lineHeight;
        setBaseHeights();
      }
    }
    if (props.onFocus) {
      props.onFocus(event);
    }
  };
  const onChange = (event: Event) => {
    if (props.isAutoResizable && !isMobileDevice) {
      resizeTextarea();
    }
    if (props.onChange) {
      props.onChange(event);
    }
  };
  const setBaseHeights = () => {
    // To Properly resize the textarea vertically, we need to record the initial height
    // to help with the resizing calculation.
    const savedValue = textarea().value;
    textarea().value = "";
    textarea().baseScrollHeight = textarea().scrollHeight;
    // For terra textareas, we want the gripper to not have the ability to resize the textarea to
    // be a tiny square. Setting the minHeight restricts the area the gripper can be shrunk too
    textarea().style.minHeight = "0px";
    textarea().style.minHeight = `${textarea().scrollHeight}px`;
    textarea().value = savedValue;
  };
  const resizeTextarea = () => {
    const minRows = props.rows || TEXTAREA_ROW_SIZES[props.size];
    textarea().rows = minRows;
    const rows = Math.ceil((textarea().scrollHeight - textarea().baseScrollHeight) / textarea().currentLineHeight);
    textarea().rows = minRows + rows;
  };
  const [p, customProps] = splitProps(props, [
    "name",
    "required",
    "onChange",
    "onFocus",
    "isAutoResizable",
    "isIncomplete",
    "isInvalid",
    "value",
    "defaultValue",
    "rows",
    "size",
    "ariaLabel",
    "ref",
    "disableResize",
    "className"
  ]);
  const theme = useContext(ThemeContext);
  const textareaClasses = () =>
    classNames(
      cx(
        "textarea",
        { "form-error": p.isInvalid },
        { "form-incomplete": p.isIncomplete && p.required && !p.isInvalid },
        { "full-size": p.size === "full" },
        { resizable: p.isAutoResizable && !isMobileDevice },
        { "no-resize": p.disableResize },
        theme.className
      ),
      customProps.className
    );
  const getAriaLabelText = () => {
    let ariaLabelText;
    // Handle case of users setting aria-label as a custom prop
    if (customProps && Object.prototype.hasOwnProperty.call(customProps, "aria-label")) {
      // If they've set aria-label and ariaLabel, use the ariaLabel value,
      // otherwise, fallback to using the aria-label value passed in.
      ariaLabelText = !p.ariaLabel ? customProps["aria-label"] : p.ariaLabel;
    } else if (p.ariaLabel) {
      // If users only set ariaLabel prop, use that value
      ariaLabelText = p.ariaLabel;
    }
    return ariaLabelText;
  };

  const textareaRows = () => p.rows || TEXTAREA_ROW_SIZES[p.size];

  const getInputAttributes = () => {
    const additionalTextareaProps = { ...customProps };

    additionalTextareaProps["aria-label"] = getAriaLabelText();
    if (p.required) {
      additionalTextareaProps["aria-required"] = "true";
    }

    if (p.value !== undefined) {
      additionalTextareaProps.value = p.value;
    } else {
      additionalTextareaProps.defaultValue = p.defaultValue;
    }
    if (additionalTextareaProps.placeholder) {
      additionalTextareaProps.placeholder = null;
    }
    return additionalTextareaProps;
  };

  return (
    <textarea
      {...getInputAttributes()}
      ref={textarea => {
        setTextArea(textarea);
        if (p.ref) p.ref(textarea);
      }}
      name={name}
      onFocus={onFocus}
      onChange={onChange}
      required={p.required}
      rows={textareaRows()}
      className={textareaClasses()}
    />
  );
};

Textarea.isTextarea = true;
