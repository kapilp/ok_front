import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Textarea from "../../../Textarea";
import styles from "./common/Textarea.test.module.scss";
const cx = classNames.bind(styles);
export const textarea: Component = (props: {}) => {
  resetDocumentTouch: any;
  constructor() {
    super();
    if (!window.DocumentTouch) {
      resetDocumentTouch = true;
      window.DocumentTouch = () => {};
      window.DocumentTouch.prototype = Document.prototype;
    }
  }
  componentDidUpdate() {
    if (!window.DocumentTouch) {
      resetDocumentTouch = true;
      window.DocumentTouch = () => {};
      window.DocumentTouch.prototype = Document.prototype;
    }
  }
  componentWillUnmount() {
    if (resetDocumentTouch) {
      delete window.DocumentTouch;
    }
  }
  render() {
    return (
      <div className={cx("content-wrapper")}>
        <Textarea
          id="auto-resizable"
          cols="2"
          isAutoResizable
          defaultValue="Default Value"
          ariaLabel="label"
        />
      </div>
    );
  }
}
