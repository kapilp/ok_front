import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Textarea from "../../../Textarea";
import styles from "./common/Textarea.test.module.scss";
const cx = classNames.bind(styles);
export const textarea: Component = (props: {}) => {
  previousMsMaxTouchPoints: number;
  resetMsMaxTouchPoints: any;
  constructor() {
    super();
    if (!navigator.msMaxTouchPoints || navigator.msMaxTouchPoints === 0) {
      resetMsMaxTouchPoints = true;
      previousMsMaxTouchPoints = navigator.msMaxTouchPoints;
      Object.defineProperty(navigator, "msMaxTouchPoints", {
        value: 1,
        configurable: true
      });
    }
  }
  componentDidUpdate() {
    if (!navigator.msMaxTouchPoints || navigator.msMaxTouchPoints === 0) {
      resetMsMaxTouchPoints = true;
      previousMsMaxTouchPoints = navigator.msMaxTouchPoints;
      Object.defineProperty(navigator, "msMaxTouchPoints", {
        value: 1,
        configurable: true
      });
    }
  }
  componentWillUnmount() {
    if (resetMsMaxTouchPoints) {
      Object.defineProperty(navigator, "msMaxTouchPoints", {
        value: previousMsMaxTouchPoints,
        configurable: true
      });
    }
  }
  render() {
    return (
      <div className={cx("content-wrapper")}>
        <Textarea
          id="auto-resizable"
          cols="2"
          isAutoResizable
          defaultValue="Default Value"
          ariaLabel="label"
        />
      </div>
    );
  }
}
