import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Textarea from "../../../Textarea";
import styles from "./common/Textarea.test.module.scss";
const cx = classNames.bind(styles);
export const textarea: Component = (props: {}) => {
  resetontouchstart: any;
  constructor() {
    super();
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = "true";
    }
  }
  componentDidUpdate() {
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = "true";
    }
  }
  componentWillUnmount() {
    if (resetontouchstart) {
      delete window.ontouchstart;
    }
  }
  render() {
    return (
      <div className={cx("content-wrapper")}>
        <Textarea
          id="auto-resizable"
          cols="2"
          isAutoResizable
          defaultValue="Default Value"
          ariaLabel="label"
        />
      </div>
    );
  }
}
