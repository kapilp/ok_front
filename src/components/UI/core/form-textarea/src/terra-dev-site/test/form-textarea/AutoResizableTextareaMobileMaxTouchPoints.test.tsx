import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Textarea from "../../../Textarea";
import styles from "./common/Textarea.test.module.scss";
const cx = classNames.bind(styles);
export const textarea: Component = (props: {}) => {
  previousMaxTouchPoints: number;
  resetMaxTouchPoints: any;
  constructor() {
    super();
    /* eslint-disable compat/compat */
    if (!navigator.maxTouchPoints || navigator.maxTouchPoints === 0) {
      resetMaxTouchPoints = true;
      previousMaxTouchPoints = navigator.maxTouchPoints;
      Object.defineProperty(navigator, "maxTouchPoints", {
        value: 1,
        configurable: true
      });
    }
  }
  componentDidUpdate() {
    if (!navigator.maxTouchPoints || navigator.maxTouchPoints === 0) {
      resetMaxTouchPoints = true;
      previousMaxTouchPoints = navigator.maxTouchPoints;
      Object.defineProperty(navigator, "maxTouchPoints", {
        value: 1,
        configurable: true
      });
    }
    /* eslint-enable compat/compat */
  }
  componentWillUnmount() {
    if (resetMaxTouchPoints) {
      Object.defineProperty(navigator, "maxTouchPoints", {
        value: previousMaxTouchPoints,
        configurable: true
      });
    }
  }
  render() {
    return (
      <div className={cx("content-wrapper")}>
        <Textarea
          id="auto-resizable"
          cols="2"
          isAutoResizable
          defaultValue="Default Value"
          ariaLabel="label"
        />
      </div>
    );
  }
}
