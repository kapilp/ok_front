import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Textarea from '../../../Textarea';
import styles from './common/Textarea.test.module.scss';
const cx = classNames.bind(styles);
const textarea = () => (
  <div className={cx('content-wrapper')}>
    <Textarea ariaLabel="label" />
  </div>
);
export default textarea;
