import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Textarea from '../../../Textarea';
import styles from './common/Textarea.test.module.scss';
const cx = classNames.bind(styles);
type textareaState = {
  swap?: boolean;
};
class textarea extends React.Component<{}, textareaState> {
  constructor(props) {
    super(props);
    state = {
      swap: false,
    };
    handleSwap = handleSwap.bind(this);
  }
  handleSwap() {
    setState(prevState => ({
      swap: !prevState.swap,
    }));
  }
  render() {
    const smallTextArea = <Textarea isAutoResizable size="small" ariaLabel="label" />;
    const largeTextArea = <Textarea isAutoResizable size="large" ariaLabel="label" />;
    const button = (
      <button id="swap-button" type="button" onClick={handleSwap}>
        Swap
      </button>
    );
    if (state.swap) {
      return (
        <div className={cx('content-wrapper')}>
          {smallTextArea}
          {largeTextArea}
          {button}
        </div>
      );
    }
    return (
      <div className={cx('content-wrapper')}>
        {largeTextArea}
        {smallTextArea}
        {button}
      </div>
    );
  }
}
export default textarea;
