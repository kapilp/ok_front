import { JSX, Component, mergeProps, Show, splitProps, useContext, createSignal } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const IncompleteTextAreaExample = () => {
  const [area, setArea] = createSignal('');
  return (
    <Field label="Incomplete Textarea When Empty" htmlFor="incomplete">
      <Textarea value={area()} required isIncomplete={area() === ''} onChange={event => setArea(event.target.value)} size="small" id="incomplete" />
    </Field>
  );
};
