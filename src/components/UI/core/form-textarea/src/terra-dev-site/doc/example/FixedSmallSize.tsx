import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const FixedSmallSizeTextAreaExample = () => (
  <Field label="Fixed Textarea - Small Size" htmlFor="small">
    <Textarea size="small" id="small" disableResize />
  </Field>
);
