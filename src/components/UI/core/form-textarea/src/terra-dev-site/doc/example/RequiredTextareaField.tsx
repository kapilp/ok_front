import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { TextareaField } from '../../../TextareaField';

export const RequiredTextAreaFieldExample = () => (
  <TextareaField
    inputId="requiredTextarea"
    label="Required Textarea"
    help="Note: This is help text"
    required
    inputAttrs={{
      name: 'requiredTextarea',
    }}
  />
);
