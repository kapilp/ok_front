import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const ResizableTextAreaExample = () => (
  <Field label="Resizable Textarea" htmlFor="resizable">
    <Textarea isAutoResizable size="small" defaultValue="I automatically resize as you type in more information (except in mobile)." id="resizable" />
  </Field>
);
