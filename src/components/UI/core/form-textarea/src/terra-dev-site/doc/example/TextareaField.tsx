import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { TextareaField } from '../../../TextareaField';

export const TextAreaFieldExample = () => (
  <TextareaField
    inputId="textarea"
    label="Textarea"
    help="Note: This is help text"
    inputAttrs={{
      name: 'textarea',
    }}
  />
);
