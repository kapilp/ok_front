import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { TextareaField } from '../../../TextareaField';

export const IncompleteTextAreaFieldExample = () => (
  <TextareaField
    inputId="incompleteTextarea"
    label="Incomplete Textarea"
    help="Note: This is help text"
    required
    isIncomplete
    inputAttrs={{
      name: 'requiredTextarea',
    }}
  />
);
