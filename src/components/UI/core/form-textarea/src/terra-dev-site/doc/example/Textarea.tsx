import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const DefaultTextAreaExample = () => (
  <Field label="Default Textarea">
    <Textarea size="small" id="default" ariaLabel="Default Textarea" />
  </Field>
);
