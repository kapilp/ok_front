import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';

export const InvalidTextAreaExample = () => {
  const [state, setState] = createStore({
    area: '',
  });

  const handleAreaChange = event => {
    setState({ area: event.target.value });
  };

  return (
    <Field label="Invalid Textarea When Empty" htmlFor="invalid">
      <Textarea value={state.area} isInvalid={state.area === ''} onChange={handleAreaChange} size="small" id="invalid" />
    </Field>
  );
};
