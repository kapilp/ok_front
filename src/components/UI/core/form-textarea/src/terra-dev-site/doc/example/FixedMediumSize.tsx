import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const FixedMediumSizeTextAreaExample = () => (
  <Field label="Fixed Textarea - Medium Size" htmlFor="medium">
    <Textarea size="medium" id="medium" disableResize />
  </Field>
);
