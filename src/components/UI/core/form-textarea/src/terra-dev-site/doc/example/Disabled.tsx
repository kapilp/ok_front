import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';

export const DisabledTextAreaExample = () => (
  <Field label="Disabled Textarea" htmlFor="disabled">
    <Textarea disabled size="small" value="I'm disabled." id="disabled" />
  </Field>
);
