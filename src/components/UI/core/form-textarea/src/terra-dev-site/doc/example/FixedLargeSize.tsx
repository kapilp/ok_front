import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const FixedLargeSizeTextAreaExample = () => (
  <Field label="Fixed Textarea - Large Size" htmlFor="large">
    <Textarea size="large" id="large" disableResize />
  </Field>
);
