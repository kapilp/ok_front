import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Textarea } from '../../../Textarea';
export const FillContainerTextAreaExample = () => (
  <Field label="Fill Container" htmlFor="fill-container">
    <Textarea size="full" defaultValue="I am full container width and height, making me non-resizable." id="fill-container" />
  </Field>
);
