import * as KeyCode from 'keycode-js';
import classNamesBind from 'classnames/bind';
import ThemeContext from 'terra-theme-context';
import Util from './_DropdownListUtil';
import styles from './_DropdownList.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The Options that should be displayed in the dropdown
   */
  children: JSX.Element.isRequired;
  /**
   * Callback to tell the parent it should close the dropdown
   */
  requestClose: PropTypes.func.isRequired;
  /**
   * @private
   * Width to set the dropdown to. Used when `isBlock` is true. Unset means to autosize.
   */
  width: string;
  /**
   * Ref callback for the dropdown list DOM element.
   */
  ref: PropTypes.func;
}
interface IDropdownListProps extends JSX.HTMLAttributes<Element> {
  width?: any;
  ref?: any;
  requestClose?: any;
}
type DropdownListState = {
  focused?: number;
  active?: number;
};
export const  DropdownList extends React.Component<IDropdownListProps, DropdownListState> {
  const theme = useContext(ThemeContext);
  context: any;
  listRef: any;
  pressed: boolean;
  searchString: string;
  searchTimeout: null;
  constructor(props) {
    super(props);
    cloneChildren = cloneChildren.bind(this);
    handleKeyDown = handleKeyDown.bind(this);
    handleKeyUp = handleKeyUp.bind(this);
    handleFocus = handleFocus.bind(this);
    clearSearch = clearSearch.bind(this);
    changeFocusState = changeFocusState.bind(this);
    state = { focused: 0, active: -1 };
    searchString = '';
    pressed = false;
    listRef = null;
  }
  changeFocusState(index) {
    // the div inside the li is what is actually focusable so need to go 2 layers down
    listRef.childNodes[index].childNodes[0].focus();
    setState({ focused: index });
  }
  handleKeyDown(event) {
    const { keyCode } = event;
    const { focused } = state;
    if (keyCode === KeyCode.KEY_RETURN || keyCode === KeyCode.KEY_SPACE) {
      /*
              Prevent the callback from being called repeatedly if key is held down.
              The close dropdown request had to be moved to handleKeyUp to fix a firefox bug
              where choosing an item with spacebar if the dropdown caret was focused when opening the dropdown
              would cause the dropdown to reopen itself.
            */
      if (!pressed) {
        pressed = true;
        setState({ active: focused });
      }
      event.preventDefault();
    } else if (keyCode === KeyCode.KEY_DOWN) {
      if (!pressed) {
        changeFocusState(Util.findNext(this, state.focused));
      }
      event.preventDefault();
    } else if (keyCode === KeyCode.KEY_UP) {
      if (!pressed) {
        changeFocusState(Util.findPrevious(this, state.focused));
      }
      event.preventDefault();
    } else if (keyCode === KeyCode.KEY_HOME) {
      if (!pressed) {
        changeFocusState(0);
      }
      event.preventDefault();
    } else if (keyCode === KeyCode.KEY_END) {
      if (!pressed) {
        changeFocusState(Util.getChildArray(this).length - 1);
      }
      event.preventDefault();
    } else if (keyCode === KeyCode.KEY_TAB) {
      props.requestClose();
      event.preventDefault();
    } else if (keyCode >= 48 && keyCode <= 90) {
      searchString = searchString.concat(String.fromCharCode(keyCode));
      clearTimeout(searchTimeout);
      searchTimeout = setTimeout(clearSearch, 500);
      let newFocused = Util.findWithStartString(this, searchString);
      if (newFocused === -1) {
        newFocused = state.focused;
      }
      changeFocusState(newFocused);
    }
    event.stopPropagation();
  }
  handleKeyUp(event) {
    const { keyCode } = event;
    if (keyCode === KeyCode.KEY_RETURN || keyCode === KeyCode.KEY_SPACE) {
      setState({ active: undefined });
      event.preventDefault();
      if (pressed) {
        const item = Util.findByIndex(this, state.focused);
        props.requestClose(() => {
          item.props.onSelect(event, item.props.metaData);
        });
      }
      pressed = false;
    }
  }
  /**
   * Keeps internal state synced with what is focused on the page incase
   * something such as a screen reader moves focus
   * @param {React.FocusEvent} event the focus event
   */
  handleFocus(event) {
    const index = Util.findIndexByValue(this, event.target.innerText);
    if (index !== -1) {
      setState({ focused: index });
    }
  }
  /**
   * Clears the keyboard search.
   */
  clearSearch() {
    searchString = '';
    clearTimeout(searchTimeout);
    searchTimeout = null;
  }
  /**
   * Tells children whether or not they are active and gives them a function to close the dropdown
   * @return {Array<JSX.Element>} the array of children
   */
  cloneChildren() {
    return React.Children.map(props.children, (child, index) =>
      React.cloneElement(child, {
        isActive: index === state.active,
        requestClose: props.requestClose,
        'data-terra-dropdown-list-item': true,
      }),
    );
  }
  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
  render() {
    const { width, ref } = props;
    const theme = context;
    return (
      <ul
        className={cx('dropdown-list', theme.className)}
        // eslint-disable-next-line react/forbid-dom-props
        style={{ width }}
        ref={ref => {
          if (ref) {
            ref(ref);
          }
          listRef = ref;
        }}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
        onKeyUp={handleKeyUp}
        role="menu"
      >
        {cloneChildren()}
      </ul>
    );
  }
}


