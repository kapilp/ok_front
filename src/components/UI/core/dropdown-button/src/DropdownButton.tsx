import * as KeyCode from 'keycode-js';
import classNamesBind from 'classnames/bind';
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import Item from './Item';
import SplitButton, { Variants as SplitButtonVariants } from './SplitButton';
import { DropdownButtonBase } from './_DropdownButtonBase';
import styles from './DropdownButton.module.scss';
const cx = classNamesBind.bind(styles);
const Variants = {
  NEUTRAL: 'neutral',
  EMPHASIS: 'emphasis',
  GHOST: 'ghost',
};
interface Properties {
  /**
   * The options to display in the dropdown. Should be comprised of the subcomponent `Item`.
   */
  children: JSX.Element.isRequired;
  /**
   * Determines whether the button should have block styling applied. The dropdown list will match the component's width.
   */
  isBlock: boolean;
  /**
   * Whether or not the button has reduced padding for use in tables and single-row lists.
   */
  isCompact: boolean;
  /**
   * Determines whether the button should be disabled.
   */
  isDisabled: boolean;
  /**
   * Sets the text that will be shown on the dropdown button.
   */
  label: string.isRequired;
  /**
   * Sets the styles of the component, one of `neutral`, `emphasis`, or `ghost`.
   */
  variant: 'neutral' | 'emphasis' | 'ghost';
}
const defaultProps = {
  isBlock: false,
  isCompact: false,
  isDisabled: false,
  variant: 'neutral',
};
interface IDropdownButtonProps extends JSX.HTMLAttributes<Element> {
  isBlock?: any;
  isCompact?: any;
  isDisabled?: any;
  label?: any;
  variant?: any;
  customProps?: any;
}
type DropdownButtonState = {
  isOpen?: boolean;
  isActive?: boolean;
  openedViaKeyboard?: boolean;
};
export const DropdownButton extends React.Component<IDropdownButtonProps, DropdownButtonState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);

  buttonNode: any;
  context: any;
  dropdownList: any;
  constructor(props) {
    super(props);
    handleDropdownButtonClick = handleDropdownButtonClick.bind(this);
    handleDropdownRequestClose = handleDropdownRequestClose.bind(this);
    handleKeyDown = handleKeyDown.bind(this);
    handleKeyUp = handleKeyUp.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setListNode = setListNode.bind(this);
    toggleDropDown = toggleDropDown.bind(this);
    state = { isOpen: false, isActive: false, openedViaKeyboard: false };
  }
  setListNode(element) {
    dropdownList = element;
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  toggleDropDown(event) {
    setState(prevState => ({ isOpen: !prevState.isOpen }));
    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button#Clicking_and_focus
    // Button on Firefox, Safari and IE running on OS X does not receive focus when clicked.
    // This will put focus on the button when clicked.
    event.currentTarget.focus();
  }
  handleDropdownButtonClick(event) {
    if (state.isOpen) {
      setState({ openedViaKeyboard: false });
    }
    toggleDropDown(event);
  }
  handleDropdownRequestClose(callback) {
    const onSelectCallback = typeof callback === 'function' ? callback : undefined;
    setState({ isOpen: false, openedViaKeyboard: false, isActive: false }, onSelectCallback);
  }
  handleKeyDown(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      // In FireFox active styles don't get applied on space
      setState({ isActive: true, openedViaKeyboard: true });
      /*
              Prevent the callback from being called repeatedly if the RETURN or SPACE key is held down.
              The keyDown event of native html button triggers Onclick() event on RETURN or SPACE key press.
              where holding RETURN key for longer time will call dropdownClick() event repeatedly which would cause
              the dropdown to open and close itself.
            */
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_DOWN && state.isOpen && !state.openedViaKeyboard) {
      // set focus to first list element on down arrow key press only when dropdown is opened by mouse click.
      const listOptions = dropdownList.querySelectorAll('[data-terra-dropdown-list-item]');
      listOptions[0].focus();
      // prevent handleFocus() callback of DropdownList.
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_UP && state.isOpen && !state.openedViaKeyboard) {
      // set focus to last list element on up arrow key press only when dropdown is opened by mouse click
      const listOptions = dropdownList.querySelectorAll('[data-terra-dropdown-list-item]');
      listOptions[listOptions.length - 1].focus();
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_TAB) {
      handleDropdownRequestClose();
    }
  }
  handleKeyUp(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      event.preventDefault();
      setState({ isActive: false });
      toggleDropDown(event);
    }
  }
  render() {
    const { children, isBlock, isCompact, isDisabled, label, variant, ...customProps } = props;
    const theme = context;
    const { isOpen, isActive, openedViaKeyboard } = state;
    const classnames = cx(
      'tDropdownButton',
      'dropdown-button',
      variant,
      { 'is-active': isOpen || isActive },
      { 'is-block': isBlock },
      { 'is-compact': isCompact },
      /* This needs to match terra-hookshot's react-onclickoutside ignore classname or clicking the caret with
          the dropdown open will cause the dropdown to close and reopen
        */
      { 'ignore-react-onclickoutside': isOpen },
      theme.className,
    );
    return (
      <DropdownButtonBase
        {...customProps}
        items={children}
        isOpen={isOpen}
        isBlock={isBlock}
        isCompact={isCompact}
        isDisabled={isDisabled}
        requestClose={handleDropdownRequestClose}
        openedViaKeyboard={openedViaKeyboard}
        buttonRef={getButtonNode}
        ref={setListNode}
      >
        <button
          type="button"
          className={classnames}
          onClick={handleDropdownButtonClick}
          onKeyDown={handleKeyDown}
          onKeyUp={handleKeyUp}
          disabled={isDisabled}
          tabIndex={isDisabled ? '-1' : undefined}
          aria-disabled={isDisabled}
          aria-expanded={isOpen}
          aria-haspopup="menu"
          ref={setButtonNode}
        >
          <span className={cx('dropdown-button-text')}>{label}</span>
          <span className={cx('caret-icon')} />
        </button>
      </DropdownButtonBase>
    );
  }
}


export { Item, Variants, SplitButton, SplitButtonVariants };
