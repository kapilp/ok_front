/* eslint-disable no-console */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SplitButton, { Item } from '../../../SplitButton';
export default () => (
  <SplitButton
    primaryOptionLabel="Primary option"
    onSelect={() => {
      console.log('primary option');
    }}
    isBlock
  >
    <Item
      label="1st Option"
      onSelect={() => {
        console.log('hi');
      }}
    />
    <Item
      label="2nd Option"
      onSelect={() => {
        console.log('bye');
      }}
    />
    <Item
      label="3rd Option"
      onSelect={() => {
        console.log('eyb');
      }}
    />
    <Item
      label="4th Option"
      onSelect={() => {
        console.log('ih');
      }}
    />
  </SplitButton>
);
