import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import { Dropdown } from './_Dropdown';
import styles from './_DropdownButtonBase.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The button or buttons that are the primary option and caret
   */
  children: JSX.Element.isRequired;
  /**
   * The options to display in the dropdown. Should be of type `Button`.
   */
  items: JSX.Element.isRequired;
  /**
   * Whether or not the dropdown is open
   */
  isOpen: boolean.isRequired;
  /**
   * Callback to tell the parent it should close the dropdown
   */
  requestClose: PropTypes.func.isRequired;
  /**
   * Determines whether the component should have block styles applied. The dropdown will match the component's width.
   */
  isBlock: boolean;
  /**
   * Whether or not the button has reduced padding for use in tables and single-row lists.
   */
  isCompact: boolean;
  /**
   * Determines whether the primary button and expanding the dropdown should be disabled.
   */
  isDisabled: boolean;
  /**
   * Whether or not dropdown is opened using keyboard.
   */
  openedViaKeyboard: boolean;
  /**
   * Callback for reference of the dropdown button
   */
  buttonRef: PropTypes.func;
  /**
   * Ref callback for the dropdown list DOM element.
   */
  ref: PropTypes.func;
}
const defaultProps = {
  isBlock: false,
  isCompact: false,
  isDisabled: false,
  openedViaKeyboard: false,
};
interface IDropdownButtonBaseProps extends JSX.HTMLAttributes<Element> {
  items?: any;
  isOpen?: any;
  requestClose?: any;
  isBlock?: any;
  isCompact?: any;
  isDisabled?: any;
  openedViaKeyboard?: any;
  buttonRef?: any;
  ref?: any;
  customProps?: any;
}
export const  DropdownButtonBase extends React.Component<IDropdownButtonBaseProps, {}> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  buttonWrapperRef: any;
  context: any;
  constructor(props) {
    super(props);
    setButtonWrapperRef = setButtonWrapperRef.bind(this);
    getButtonWrapperRef = getButtonWrapperRef.bind(this);
    buttonWrapperRef = null;
  }
  setButtonWrapperRef(ref) {
    buttonWrapperRef = ref;
  }
  getButtonWrapperRef() {
    return buttonWrapperRef;
  }
  render() {
    const { children, items, isOpen, requestClose, isBlock, isCompact, isDisabled, openedViaKeyboard, buttonRef, ref, ...customProps } = props;
    const theme = context;
    const DropdownButtonClassNames = classNames(
      cx('dropdown-button-base', { 'is-block': isBlock }, { 'is-compact': isCompact }, { disabled: isDisabled }, theme.className),
      customProps.className,
    );
    let calcWidth;
    if (buttonWrapperRef && isBlock) {
      calcWidth = `${buttonWrapperRef.getBoundingClientRect().width}px`;
    }
    return (
      <div {...customProps} className={DropdownButtonClassNames} ref={setButtonWrapperRef}>
        {children}
        <Dropdown
          targetRef={getButtonWrapperRef}
          isOpen={isOpen}
          requestClose={requestClose}
          width={calcWidth}
          openedViaKeyboard={openedViaKeyboard}
          buttonRef={buttonRef}
          ref={ref}
        >
          {items}
        </Dropdown>
      </div>
    );
  }
}


