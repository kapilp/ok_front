
import FocusTrap from "focus-trap-react";
import Hookshot from "terra-hookshot";
import DropdownList from "./_DropdownList";
interface Properties {
  /**
   * Callback to tell the parent it should close the dropdown
   */
  requestClose: PropTypes.func.isRequired,
  /**
   * Ref to attach the dropdown to
   */
  targetRef: PropTypes.func.isRequired,
  /**
   * Whether or not the dropdown should be open
   */
  isOpen: boolean.isRequired,
  /**
   * The Options that should be displayed in the dropdown
   */
  children: JSX.Element.isRequired,
  /**
   * @private
   * Width to set the dropdown to. Used when `isBlock` is true. Unset means to autosize.
   */
  width: string,
  /**
   * Whether or not dropdown is opened using keyboard.
   */
  openedViaKeyboard: boolean,
  /**
   * Callback for reference of the dropdown button
   */
  buttonRef: PropTypes.func,
  /**
   * Ref callback for the dropdown list DOM element.
   */
  ref: PropTypes.func
};
export const Dropdown = (props: Properties {
  requestClose,
  isOpen,
  targetRef,
  children,
  width,
  openedViaKeyboard,
  buttonRef,
  ref
}) => (
  <Hookshot
    isOpen={isOpen}
    isEnabled
    targetRef={targetRef}
    attachmentBehavior="flip"
    contentAttachment={{ vertical: "top", horizontal: "start" }}
    targetAttachment={{ vertical: "bottom", horizontal: "start" }}
  >
    <Hookshot.Content onEsc={requestClose} onOutsideClick={requestClose}>
      <FocusTrap
        focusTrapOptions={{
          returnFocusOnDeactivate: true,
          initialFocus: openedViaKeyboard ? "" : buttonRef,
          clickOutsideDeactivates: true
        }}
      >
        <DropdownList
          requestClose={requestClose}
          width={width}
          ref={ref}
        >
          {children}
        </DropdownList>
      </FocusTrap>
    </Hookshot.Content>
  </Hookshot>
);

