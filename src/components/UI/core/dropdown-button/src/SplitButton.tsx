import classNamesBind from 'classnames/bind';
import * as KeyCode from 'keycode-js';
import { injectIntl, intlShape } from 'react-intl';
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import Item from './Item';
import DropdownButtonBase from './_DropdownButtonBase';
import styles from './SplitButton.module.scss';
const cx = classNamesBind.bind(styles);
const Variants = {
  NEUTRAL: 'neutral',
  // EMPHASIS: 'emphasis', // Wait to add in future enhancement
  GHOST: 'ghost',
};
interface Properties {
  /**
   * The options to display in the dropdown. Should be comprised of the subcomponent `Item`.
   */
  children: JSX.Element.isRequired;
  /**
   * Determines whether the component should have block styles applied. The dropdown will match the component's width.
   */
  isBlock: boolean;
  /**
   * Whether or not the button has reduced padding for use in tables and single-row lists.
   */
  isCompact: boolean;
  /**
   * Determines whether the primary button and expanding the dropdown should be disabled.
   */
  isDisabled: boolean;
  /**
   * Sets the text that will be shown on the primary button which is outside the dropdown.
   */
  primaryOptionLabel: string.isRequired;
  /**
   * What will be called when the primary button is pressed.
   */
  onSelect: PropTypes.func.isRequired;
  /**
   * Sets the styles of the component, one of `neutral`, or `ghost`.
   */
  variant: 'neutral' | 'ghost';
  /**
   * @private
   * The intl object to be injected for translations.
   */
  intl: intlShape.isRequired;
  /**
   * @private
   * Callback to tell the parent it should close the dropdown
   */
  requestClose: PropTypes.func;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData: {};
}
const defaultProps = {
  isBlock: false,
  isCompact: false,
  isDisabled: false,
  variant: 'neutral',
};
interface ISplitButtonProps extends JSX.HTMLAttributes<Element> {
  isBlock?: any;
  isCompact?: any;
  isDisabled?: any;
  primaryOptionLabel?: any;
  onSelect?: any;
  variant?: any;
  intl?: any;
  requestClose?: any;
  metaData?: any;
  customProps?: any;
}
type SplitButtonState = {
  isOpen?: boolean;
  caretIsActive?: boolean;
  primaryIsActive?: boolean;
  openedViaKeyboard?: boolean;
};
export const  SplitButton extends React.Component<ISplitButtonProps, SplitButtonState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);

  buttonNode: any;
  context: any;
  dropdownList: any;
  constructor(props) {
    super(props);
    handleDropdownButtonClick = handleDropdownButtonClick.bind(this);
    handlePrimaryButtonClick = handlePrimaryButtonClick.bind(this);
    handleDropdownRequestClose = handleDropdownRequestClose.bind(this);
    handlePrimaryKeyDown = handlePrimaryKeyDown.bind(this);
    handlePrimaryKeyUp = handlePrimaryKeyUp.bind(this);
    handleCaretKeyDown = handleCaretKeyDown.bind(this);
    handleCaretKeyUp = handleCaretKeyUp.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setListNode = setListNode.bind(this);
    toggleDropDown = toggleDropDown.bind(this);
    state = {
      isOpen: false,
      caretIsActive: false,
      primaryIsActive: false,
      openedViaKeyboard: false,
    };
  }
  setListNode(element) {
    dropdownList = element;
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  toggleDropDown(event) {
    setState(prevState => ({ isOpen: !prevState.isOpen }));
    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button#Clicking_and_focus
    // Button on Firefox, Safari and IE running on OS X does not receive focus when clicked.
    // This will put focus on the button when clicked.
    event.currentTarget.focus();
  }
  handleDropdownButtonClick(event) {
    if (state.isOpen) {
      setState({ openedViaKeyboard: false });
    }
    toggleDropDown(event);
  }
  handlePrimaryButtonClick(event) {
    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button#Clicking_and_focus
    // Button on Firefox, Safari and IE running on OS X does not receive focus when clicked.
    // This will put focus on the button when clicked.
    event.currentTarget.focus();
    props.onSelect(event, props.metaData);
    handleDropdownRequestClose();
  }
  handleDropdownRequestClose(callback) {
    const onSelectCallback = typeof callback === 'function' ? callback : undefined;
    setState({ isOpen: false, openedViaKeyboard: false, caretIsActive: false }, onSelectCallback);
  }
  /*
      In FireFox active styles don't get applied onKeyDown
     */
  handlePrimaryKeyDown(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ primaryIsActive: true });
    }
  }
  handlePrimaryKeyUp(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ primaryIsActive: false });
    }
  }
  handleCaretKeyDown(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      // In FireFox active styles don't get applied onKeyDown
      setState({ caretIsActive: true, openedViaKeyboard: true });
      /*
              Prevent the callback from being called repeatedly if the RETURN or SPACE key is held down.
              The keyDown event of native html button triggers Onclick() event on RETURN or SPACE key press.
              where holding RETURN key for longer time will call dropdownClick() event repeatedly which would cause
              the dropdown to open and close itself.
            */
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_DOWN && state.isOpen && !state.openedViaKeyboard) {
      // set focus to first list element on down arrow key press only when dropdown is opened by mouse click.
      const listOptions = dropdownList.querySelectorAll('[data-terra-dropdown-list-item]');
      listOptions[0].focus();
      // prevent handleFocus() callback of DropdownList.
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_UP && state.isOpen && !state.openedViaKeyboard) {
      // set focus to last list element on up arrow key press only when dropdown is opened by mouse click
      const listOptions = dropdownList.querySelectorAll('[data-terra-dropdown-list-item]');
      listOptions[listOptions.length - 1].focus();
      event.preventDefault();
    } else if (event.keyCode === KeyCode.KEY_TAB) {
      handleDropdownRequestClose();
    }
  }
  handleCaretKeyUp(event) {
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      event.preventDefault();
      setState({ caretIsActive: false });
      toggleDropDown(event);
    }
  }
  render() {
    const { children, isBlock, isCompact, isDisabled, primaryOptionLabel, onSelect, variant, intl, requestClose, metaData, ...customProps } = props;
    const { isOpen, primaryIsActive, caretIsActive, openedViaKeyboard } = state;
    const theme = context;
    const caretLabel = intl.formatMessage({
      id: 'Terra.dropdownButton.moreOptions',
    });
    const primaryClassnames = cx('split-button-primary', variant, { 'is-block': isBlock }, { 'is-compact': isCompact }, { 'is-active': primaryIsActive }, theme.className);
    const caretClassnames = cx(
      'split-button-caret',
      variant,
      { 'is-compact': isCompact },
      { 'is-active': isOpen || caretIsActive },
      /* This needs to match terra-hookshot's react-onclickoutside ignore classname or clicking the caret with
          the dropdown open will cause the dropdown to close and reopen
        */
      { 'ignore-react-onclickoutside': isOpen },
      theme.className,
    );
    return (
      <DropdownButtonBase
        {...customProps}
        items={children}
        isOpen={isOpen}
        requestClose={handleDropdownRequestClose}
        isBlock={isBlock}
        isCompact={isCompact}
        isDisabled={isDisabled}
        openedViaKeyboard={openedViaKeyboard}
        buttonRef={getButtonNode}
        ref={setListNode}
      >
        <button
          type="button"
          className={primaryClassnames}
          onClick={handlePrimaryButtonClick}
          onKeyDown={handlePrimaryKeyDown}
          onKeyUp={handlePrimaryKeyUp}
          disabled={isDisabled}
          tabIndex={isDisabled ? '-1' : undefined}
          aria-disabled={isDisabled}
        >
          {primaryOptionLabel}
        </button>
        <button
          type="button"
          onClick={handleDropdownButtonClick}
          onKeyDown={handleCaretKeyDown}
          onKeyUp={handleCaretKeyUp}
          className={caretClassnames}
          disabled={isDisabled}
          tabIndex={isDisabled ? '-1' : undefined}
          aria-disabled={isDisabled}
          aria-expanded={isOpen}
          aria-haspopup="menu"
          aria-label={caretLabel}
          ref={setButtonNode}
        >
          <span className={cx('caret-icon')} />
        </button>
      </DropdownButtonBase>
    );
  }
}

export default injectIntl(SplitButton);
export { Item, Variants };
