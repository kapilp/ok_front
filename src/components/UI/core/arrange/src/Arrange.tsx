import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Arrange.module.scss';
const cx = classNames.bind(styles);
enum AlignmentTypes {
  CENTER = 'center',
  BOTTOM = 'bottom',
  STRETCH = 'stretch',
}

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * The content to display in the body of the fitStart.
   */
  fitStart?: JSX.Element;
  /**
   * The content to display in the body of the fill.
   */
  fill: JSX.Element;
  /**
   * The content to display in the body of the fitEnd.
   */
  fitEnd?: JSX.Element;
  /**
   * The vertical orientation of all three containers. It will override the alignment of alignFitStart, alignFill and alignFitEnd if given. One of: `center`, `bottom`, `stretch`.
   */
  align?: AlignmentTypes;
  /**
   * The vertical orientation of fitStart. One of: `center`, `bottom`, `stretch`.
   */
  alignFitStart?: AlignmentTypes;
  /**
   * The vertical orientation of fitEnd. One of: `center`| `bottom`, `stretch`.
   */
  alignFitEnd?: AlignmentTypes;
  /**
   * The vertical orientation of fill. One of: `center`, `bottom`, `stretch`.
   */
  alignFill?: AlignmentTypes;
  /**
   * The attributes to be set on the fitStart wrapper element
   */
  // eslint-disable-next-line react/forbid-prop-types
  fitStartAttributes?: {};
  /**
   * The attributes to be set on the fill wrapper element
   */
  // eslint-disable-next-line react/forbid-prop-types
  fillAttributes?: {};
  /**
   * The attributes to be set on the fitEnd wrapper element
   */
  // eslint-disable-next-line react/forbid-prop-types
  fitEndAttributes?: {};
}
export const Arrange = (props: Properties) => {
  props = mergeProps({}, { fitStartAttributes: {}, fillAttributes: {}, fitEndAttributes: {} }, props);
  const [extracted, customProps] = splitProps(props, [
    'fitStart',
    'fill',
    'fitEnd',
    'align',
    'alignFitStart',
    'alignFill',
    'alignFitEnd',
    'fitStartAttributes',
    'fillAttributes',
    'fitEndAttributes',
  ]);
  return (
    <div {...customProps} className={cx('arrange', customProps.className)}>
      <div {...props.fitStartAttributes} className={cx('fit', props.align || props.alignFitStart, props.fitStartAttributes.className)}>
        {props.fitStart}
      </div>
      <div {...props.fillAttributes} className={cx('fill', props.align || props.alignFill, props.fillAttributes.className)}>
        {props.fill}
      </div>
      <div {...props.fitEndAttributes} className={cx('fit', props.align || props.alignFitEnd, props.fitEndAttributes.className)}>
        {props.fitEnd}
      </div>
    </div>
  );
};
export { AlignmentTypes };
