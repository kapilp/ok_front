import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Arrange from '../../../Arrange';
import { ArrangeWrapper, IconWrapper, image, simpleText } from './common/examplesetup';
const IndividuallyAlignedFitEnd = () => (
  <div>
    <ArrangeWrapper>
      <Arrange id="default" fitStart={image} fill={simpleText} fitEnd={<IconWrapper />} />
    </ArrangeWrapper>

    <ArrangeWrapper>
      <Arrange id="center" alignFitEnd="center" fitStart={image} fill={simpleText} fitEnd={<IconWrapper />} />
    </ArrangeWrapper>

    <ArrangeWrapper>
      <Arrange id="bottom" alignFitEnd="bottom" fitStart={image} fill={simpleText} fitEnd={<IconWrapper />} />
    </ArrangeWrapper>

    <ArrangeWrapper>
      <Arrange id="stretch" alignFitEnd="stretch" fitStart={image} fill={simpleText} fitEnd={<IconWrapper />} />
    </ArrangeWrapper>
  </div>
);
export default IndividuallyAlignedFitEnd;
