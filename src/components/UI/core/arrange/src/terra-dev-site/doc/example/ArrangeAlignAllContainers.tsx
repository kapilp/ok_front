import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Arrange } from '../../../Arrange';
import { alignExampleDivBlue, textWithBlueBorder } from '../common/examplesetup';
import styles from './ArrangeExamples.module.scss';

const cx = classNames.bind(styles);
export const ArrangeAlignAllContainers = () => (
  <div>
    <h3>Align - Default</h3>
    <Arrange className={cx('arrange')} fitStart={alignExampleDivBlue} fill={textWithBlueBorder} fitEnd={alignExampleDivBlue} />
    <br />
    <hr />
    <h3>Align - Center</h3>
    <Arrange align="center" className={cx('arrange')} fitStart={alignExampleDivBlue} fill={textWithBlueBorder} fitEnd={alignExampleDivBlue} />
    <br />
    <hr />
    <h3>Align - Bottom</h3>
    <Arrange align="bottom" className={cx('arrange')} fitStart={alignExampleDivBlue} fill={textWithBlueBorder} fitEnd={alignExampleDivBlue} />
  </div>
);
