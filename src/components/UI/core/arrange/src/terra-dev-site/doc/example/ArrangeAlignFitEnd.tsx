import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Arrange } from '../../../Arrange';
import { alignExampleDivBlue, alignExampleDiv, simpleText } from '../common/examplesetup';
import styles from './ArrangeExamples.module.scss';
const cx = classNames.bind(styles);
export const ArrangeAlignFitEnd = () => (
  <div>
    <h3>Align FitEnd - Default</h3>
    <Arrange className={cx('arrange')} fitStart={alignExampleDiv} fill={simpleText} fitEnd={alignExampleDivBlue} />
    <br />
    <hr />
    <h3>Align FitEnd - Center</h3>
    <Arrange alignFitEnd="center" className={cx('arrange')} fitStart={alignExampleDiv} fill={simpleText} fitEnd={alignExampleDivBlue} />
    <br />
    <hr />
    <h3>Align FitEnd - Bottom</h3>
    <Arrange alignFitEnd="bottom" className={cx('arrange')} fitStart={alignExampleDiv} fill={simpleText} fitEnd={alignExampleDivBlue} />
  </div>
);
