import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Arrange } from '../../../Arrange';
import { alignExampleDiv, textWithBlueBorder } from '../common/examplesetup';
import styles from './ArrangeExamples.module.scss';
const cx = classNames.bind(styles);
export const ArrangeAlignFill = () => (
  <div>
    <h3>Align Fill - Default</h3>
    <Arrange className={cx('arrange')} fitStart={alignExampleDiv} fill={textWithBlueBorder} fitEnd={alignExampleDiv} />
    <br />
    <hr />
    <h3>Align Fill - Center</h3>
    <Arrange alignFill="center" className={cx('arrange')} fitStart={alignExampleDiv} fill={textWithBlueBorder} fitEnd={alignExampleDiv} />
    <br />
    <hr />
    <h3>Align Fill - Bottom</h3>
    <Arrange alignFill="bottom" className={cx('arrange')} fitStart={alignExampleDiv} fill={textWithBlueBorder} fitEnd={alignExampleDiv} />
  </div>
);
