import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './examplesetup.module.scss';
const cx = classNames.bind(styles);
const ipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
export const simpleText = () => <div>{ipsum}</div>;
export const textWithBlueBorder = () => <div className={cx('outlined-placeholder')}>{ipsum}</div>;
export const alignExampleDiv = () => <div className={cx('placeholder')} />;
export const alignExampleDivBlue = () => <div className={cx('highlighted-placeholder')} />;
