import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Toolbar } from '../../../Toolbar';
import { SvgIconAttachment } from '../../../../../icon/src/icon/IconAttachment';
import { SvgIconEdit } from '../../../../../icon/src/icon/IconEdit';
import { SvgIconAdd } from '../../../../../icon/src/icon/IconAdd';
import { Button, ButtonVariants } from '../../../../../button/src/Button';

export const DefaultToolbar = () => {
  const [state, setState] = createStore({ align: '' });
  const handleOnChange = event => {
    setState({ align: event.target.value });
  };
  return (
    <div>
      <label>
        Alignment:
        <select onChange={handleOnChange}>
          <option value="start">start</option>
          <option value="center">center</option>
          <option value="end">end</option>
        </select>
      </label>
      <br />
      <Toolbar align={state.align}>
        <Button text="Edit" variant={ButtonVariants.UTILITY} icon={SvgIconEdit} />
        <Button text="Add" variant={ButtonVariants.UTILITY} icon={SvgIconAdd} />
        <Button text="Attachment" variant={ButtonVariants.UTILITY} icon={SvgIconAttachment} />
      </Toolbar>
    </div>
  );
};
