import { JSX, For, mergeProps, splitProps, useContext } from 'solid-js';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import styles from './Toolbar.module.scss';
const cx = classNamesBind.bind(styles);

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Content alignment. Align to start, end, or center.
   */
  align: 'start' | 'end' | 'center';
  /**
   * Items to be displayed in toolbar such as buttons, button groups, and links.
   */
  children: JSX.Element[];
}
const defaultProps = {
  align: 'start',
};
export const Toolbar = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, ['align', 'children']);
  return (
    <div {...customProps} className={classNames(cx('toolbar', `${props.align}-align`, theme.className), customProps.className)}>
      <For each={props.children}>{item => <div className={cx('item')}>{item}</div>}</For>
    </div>
  );
};
