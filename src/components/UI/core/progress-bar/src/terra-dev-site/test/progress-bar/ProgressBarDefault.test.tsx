import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ProgressBar from '../../../ProgressBar';
const ProgressBarDefault = () => <ProgressBar id="progressbar" valueText="15%" value={15} />;
export default ProgressBarDefault;
