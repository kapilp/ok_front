import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ProgressBar } from '../../../ProgressBar';

export const ProgressBarDefault = () => <ProgressBar valueText="15%" value={15} />;
