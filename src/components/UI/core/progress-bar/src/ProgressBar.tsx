import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './ProgressBar.module.scss';

const cx = classNamesBind.bind(styles);
export const ProgressBarHeightSize = {
  TINY: 'tiny',
  SMALL: 'small',
  MEDIUM: 'medium',
  LARGE: 'large',
  HUGE: 'huge',
};
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Sets the size of the progress-bar from the following values; `tiny`, `small`, `medium`, `large` and `huge`
   */
  heightSize?: 'tiny' | 'small' | 'medium' | 'large' | 'huge';
  /**
   * Sets the fill-value of the progress bar with respect to the `max` prop.
   */
  value: number;
  /**
   * Sets the maximum possible fill-value.
   */
  max?: number;
  /**
   * Value passed to aria-valuetext for accessibility. You can view more about this attribute
   * at https://www.w3.org/WAI/PF/aria/states_and_properties#aria-valuetext.
   */
  valueText?: string;
  /**
   * Sets an author defined class, to control the colors of the progress bar and override the base color styles.
   *
   * ![IMPORTANT](https://badgen.net/badge//IMPORTANT/blue?icon=github)
   * Adding `var(--my-app...` CSS variables is required for proper re-themeability when creating custom color styles _(see included examples)_.
   */
  colorClass?: string;
}
const defaultProps = {
  heightSize: 'small',
  max: 100,
  valueText: undefined,
  colorClass: 'default-color',
};
export const ProgressBar = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const normalizedValue = (props.value / props.max) * 100;
  const [extracted, customProps] = splitProps(props, ['heightSize', 'value', 'max', 'valueText', 'colorClass']);
  return (
    <progress
      {...customProps}
      className={classNames(cx('progress-bar', props.heightSize, props.colorClass, theme.className), customProps.className)}
      max={100}
      value={normalizedValue}
      aria-valuemax={100}
      aria-valuemin={0}
      aria-valuenow={normalizedValue}
      aria-valuetext={props.valueText}
      tabIndex="-1"
    />
  );
};
