import {JSX} from "solid-js";
import { Placeholder } from "@cerner/terra-docs";
export default () => <Placeholder title="Placeholder" />;
