import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Docs.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Content to be displayed as the name
   */
  name: string;
}
const defaultProps = {
  name: 'default',
};
export const Docs: Component = ({ name, ...customProps }) => {
  const theme = React.useContext(ThemeContext);
  const DocsClassNames = classNames(cx(['docs', theme.className]), customProps.className);
  return (
    <div {...customProps} className={DocsClassNames}>
      {name}
    </div>
  );
};
props = mergeProps({}, defaultProps, props);
