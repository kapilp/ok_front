import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './colors.module.scss';
import { Text } from '../../../Text';
const cx = classNames.bind(styles);
export const TextColors = () => (
  <div>
    <Text fontSize={18}>{"Font Color: 'default' class"}</Text>
    <br />
    <Text fontSize={18} colorClass={cx(['attention'])}>
      {"Font Color: 'attention' class"}
    </Text>
    <br />
    <Text fontSize={18} colorClass={cx(['success'])}>
      {"Font Color: 'success' class"}
    </Text>
    <br />
    <Text fontSize={18} colorClass={cx(['info'])}>
      {"Font Color: 'info' class"}
    </Text>
  </div>
);
