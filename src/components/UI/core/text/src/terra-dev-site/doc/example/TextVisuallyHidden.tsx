import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Text } from '../../../Text';
export const TextVisuallyHidden = () => (
  <div>
    <p>
      The example below includes a<code> &lt;Text /&gt;</code> component using the
      <code> isVisuallyHidden</code> prop.
    </p>
    <Text isVisuallyHidden>This text is not visible, however it is accessible to screen readers.</Text>
  </div>
);
