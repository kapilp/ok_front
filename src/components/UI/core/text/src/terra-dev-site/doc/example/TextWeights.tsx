import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Text } from '../../../Text';
export const TextWeights = () => (
  <div>
    <Text fontSize={18} weight={200}>
      Font Weight: 200
    </Text>
    <br />
    <Text fontSize={18} weight={300}>
      Font Weight: 300
    </Text>
    <br />
    <Text fontSize={18} weight={400}>
      Font Weight: 400
    </Text>
    <br />
    <Text fontSize={18} weight={700}>
      Font Weight: 700
    </Text>
  </div>
);
