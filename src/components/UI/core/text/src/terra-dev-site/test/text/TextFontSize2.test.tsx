import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Text from '../../../Text';
const TextFontSize = () => (
  <div id="fontSize2">
    <Text id="textFontSize36" fontSize={36}>
      Font Size 36
    </Text>
    <br />
    <Text id="textFontSize46" fontSize={46}>
      Font Size 46
    </Text>
    <br />
    <Text id="textFontSize50" fontSize={50}>
      Font Size 50
    </Text>
    <br />
    <Text id="textFontSize56" fontSize={56}>
      Font Size 56
    </Text>
    <br />
    <Text id="textFontSize64" fontSize={64}>
      Font Size 64
    </Text>
  </div>
);
export default TextFontSize;
