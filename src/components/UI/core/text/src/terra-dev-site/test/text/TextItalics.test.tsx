import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Text from '../../../Text';
const TextItalics = () => (
  <div>
    <Text id="textItalic" isItalic>
      Italics
    </Text>
  </div>
);
export default TextItalics;
