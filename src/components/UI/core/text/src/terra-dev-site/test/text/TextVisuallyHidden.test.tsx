import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Text from '../../../Text';
const TextVisuallyHidden = () => (
  <div>
    <span>Text below is visually hidden but available to screen readers</span>
    <Text id="textVisuallyHidden" isVisuallyHidden>
      Visually Hidden
    </Text>
  </div>
);
export default TextVisuallyHidden;
