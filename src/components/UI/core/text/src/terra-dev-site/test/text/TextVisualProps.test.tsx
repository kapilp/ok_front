import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Text from '../../../Text';
import styles from '../../doc/example/colors.module.scss';
const cx = classNames.bind(styles);
const TextVisualProps = () => (
  <div>
    <Text id="textVisualProps" fontSize={24} colorClass={cx(['info'])} isItalic weight={200}>
      All Visual Props Set
    </Text>
  </div>
);
export default TextVisualProps;
