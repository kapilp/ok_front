import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Text from '../../../Text';
import styles from '../../doc/example/colors.module.scss';
const cx = classNames.bind(styles);
const TextColor = () => (
  <div>
    <Text id="textColor" colorClass={cx(['info'])}>
      Color
    </Text>
  </div>
);
export default TextColor;
