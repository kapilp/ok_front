import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Text.module.scss';
const cx = classNamesBind.bind(styles);
/* eslint-disable quote-props */
export const TextFontSize = {
  '10': 10,
  '12': 12,
  '14': 14,
  '16': 16,
  '18': 18,
  '20': 20,
  '24': 24,
  '32': 32,
  '36': 36,
  '46': 46,
  '50': 50,
  '56': 56,
  '64': 64,
  '72': 72,
  '92': 92,
  '100': 100,
};
export const TextWeight = {
  '200': 200,
  '300': 300,
  '400': 400,
  '700': 700,
};
/* eslint-enable quote-props */
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Child nodes
   */
  children: JSX.Element;
  /**
   * Sets the text to display in italics.
   */
  isItalic?: boolean;
  /**
   * Sets the text to appear visually hidden. The text will still be available to screen readers.
   */
  isVisuallyHidden?: boolean;
  /**
   * Indicates if word wrapping styles should be applied.
   */
  isWordWrapped?: boolean;
  /**
   * Sets the text size. One of `10`, `12`, `14`, `16`, `18`, `20`, `24`, `32`, `36`, `46`, `50`, `56`, `64`, `72`, `92`, `100`.
   */
  fontSize?: 10 | 12 | 14 | 16 | 18 | 20 | 24 | 32 | 36 | 46 | 50 | 56 | 64 | 72 | 92 | 100;

  /**
   * The prop can be used to specify the weight or boldness of the Text component. One of `200`, `300`, `400`, `700`.
   */
  weight?: 200 | 300 | 400 | 700;
  /**
   * Sets an author defined class, to control the colors of the text and override the base color styles.
   *
   * ![IMPORTANT](https://badgen.net/badge//IMPORTANT/blue?icon=github)
   * Adding `var(--my-app...` CSS variables is required for proper re-themeability when creating custom color styles _(see included examples)_.
   */
  colorClass?: string;
}
const defaultProps = {
  isItalic: false,
  isVisuallyHidden: false,
  isWordWrapped: false,
};
export const Text = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, ['children', 'isVisuallyHidden', 'isItalic', 'fontSize', 'weight', 'isWordWrapped', 'colorClass']);
  return (
    <span
      {...customProps}
      className={cx(
        [
          'text',
          { 'inherit-color': !props.colorClass },
          { italic: props.isItalic },
          { 'word-wrap': props.isWordWrapped },
          { 'visually-hidden': props.isVisuallyHidden },
          { [`font-size-${props.fontSize}`]: props.fontSize },
          { [`font-weight-${props.weight}`]: props.weight },
          props.colorClass,
          theme.className,
        ],
        customProps.className,
      )}
    >
      {props.children}
    </span>
  );
};
