// import { FormattedMessage } from "react-intl";
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Alert.module.scss';
import { SvgIconDiamondSymbol } from '../../icon/src/icon/IconDiamondSymbol';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { SvgIconAlert } from '../../icon/src/icon/IconAlert';
import { SvgIconInformation } from '../../icon/src/icon/IconInformation';
import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { SvgIconWarning } from '../../icon/src/icon/IconWarning';
import { SvgIconGapChecking } from '../../icon/src/icon/IconGapChecking';
import { SvgIconError } from '../../icon/src/icon/IconError';
import { SvgIconSuccess } from '../../icon/src/icon/IconSuccess';
import { Button } from '../../button/src/Button';
const cx = classNamesBind.bind(styles);

export enum AlertTypes {
  ALERT = 'alert',
  ERROR = 'error',
  WARNING = 'warning',
  UNSATISFIED = 'unsatisfied',
  UNVERIFIED = 'unverified',
  ADVISORY = 'advisory',
  INFO = 'info',
  SUCCESS = 'success',
  CUSTOM = 'custom',
}
interface Properties {
  /**
   * An action element to be added to the action section of the alert to give the user an easy way
   * to accomplish a task to resolve the notification.
   */
  action?: JSX.Element;
  /**
   * Child Nodes providing the message content for the alert. Can contain text and HTML.
   */
  children?: JSX.Element;
  /**
   * The icon to be used for an alert of type custom. This will not be used for any other alert types.
   */
  customIcon?: JSX.Element;
  /**
   * Sets an author-defined class, to control the status bar color to be used for an alert of type custom.
   *
   * ![IMPORTANT](https://badgen.net/badge//IMPORTANT/CSS?icon=github)
   * Adding `var(--my-app...` CSS variables is required for proper re-themeability when creating custom color styles _(see included examples)_.
   */
  customColorClass?: string;
  /**
   * Callback function triggered when Dismiss button is clicked. The presence of this prop will cause the Dismiss button to be included on the alert.
   */
  onDismiss?: () => void;
  /**
   * The title for the alert which will be bolded.
   */
  title?: string;
  /**
   * The type of alert to be rendered. One of `alert`, `error`, `warning`, `unsatisfied`, `unverified`, `advisory`,
   * `info`, `success`, or `custom`.
   */
  type?: AlertTypes;
}
const defaultProps = {
  customColorClass: 'custom-default-color',
  type: AlertTypes.ALERT,
};
const getAlertIcon = (type: AlertTypes, customIcon) => {
  switch (type) {
    case AlertTypes.ALERT:
      return (
        <span className={cx('icon')}>
          <SvgIconAlert />
        </span>
      );
    case AlertTypes.ERROR:
      return (
        <span className={cx('icon')}>
          <SvgIconError />
        </span>
      );
    case AlertTypes.WARNING:
      return (
        <span className={cx('icon')}>
          <SvgIconWarning />
        </span>
      );
    case AlertTypes.UNSATISFIED:
      return (
        <span className={cx('icon', 'unsatisfied-icon')}>
          <SvgIconGapChecking />
        </span>
      );
    case AlertTypes.UNVERIFIED:
      return (
        <span className={cx('icon', 'unverified-icon')}>
          <SvgIconDiamondSymbol />
        </span>
      );
    case AlertTypes.ADVISORY:
      return null;
    case AlertTypes.INFO:
      return (
        <span className={cx('icon')}>
          <SvgIconInformation />
        </span>
      );
    case AlertTypes.SUCCESS:
      return (
        <span className={cx('icon')}>
          <SvgIconSuccess />
        </span>
      );
    case AlertTypes.CUSTOM:
      return <span className={cx('icon')}>{customIcon}</span>;
    default:
      return null;
  }
};
export const getStrHack = (v: AlertTypes) => {
  switch (v) {
    case AlertTypes.ALERT:
      return 'Alert';
    case AlertTypes.ERROR:
      return 'Error';
    case AlertTypes.WARNING:
      return 'Warning';
    case AlertTypes.UNSATISFIED:
      return 'Required Action.';
    case AlertTypes.UNVERIFIED:
      return 'Outside Records.';
    case AlertTypes.ADVISORY:
      return 'Advisory';
    case AlertTypes.INFO:
      return 'Information';
    case AlertTypes.SUCCESS:
      return 'Success';
    case AlertTypes.CUSTOM:
      return '';
  }
};
export const Alert = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [state, setState] = createStore({ isNarrow: false });
  const [e, customProps] = splitProps(props, ['action', 'children', 'customIcon', 'customColorClass', 'onDismiss', 'title', 'type', 'className']);
  const defaultTitle = () => (e.type === AlertTypes.CUSTOM ? '' : getStrHack(e.type)); // <FormattedMessage id={`Terra.alert.${e.type}`} />;
  const alertClassNames = () =>
    classNames(cx('alert-base', e.type, { narrow: e.isNarrow }, { wide: !e.isNarrow }, theme.className), e.className, {
      [`${e.customColorClass}`]: e.customColorClass && e.type === AlertTypes.CUSTOM,
    });
  const bodyClassNameForParent = () =>
    cx('body', { 'body-std': !e.isNarrow || (e.isNarrow && !e.onDismiss && !e.action) }, { 'body-narrow': e.isNarrow && (e.onDismiss || e.action) });

  const dismissButton = () => e.onDismiss && <Button text={'Dismiss'} onClick={e.onDismiss} />;
  /*if (e.onDismiss) {
    dismissButton = <FormattedMessage id="Terra.alert.dismiss">{buttonText => <Button text={buttonText} onClick={e.onDismiss} />}</FormattedMessage>;
  }*/
  const actionsSection = () =>
    e.onDismiss || e.action ? (
      <div
        className={cx('actions', {
          'actions-custom': e.type === AlertTypes.CUSTOM,
        })}
      >
        {e.action}
        {dismissButton()}
      </div>
    ) : null;
  const alertSectionClassName = () =>
    cx('section', {
      'section-custom': e.type === AlertTypes.CUSTOM,
    });
  const alertMessageContent = () => (
    <div className={alertSectionClassName()}>
      {(e.title || e.type != AlertTypes.CUSTOM) && <strong className={cx('title')}>{e.title || getStrHack(e.type)}</strong>}
      {e.children}
    </div>
  );
  return (
    <ResponsiveElement
      onChange={value => {
        const showNarrowAlert = value === 'tiny';
        if (showNarrowAlert !== state.isNarrow) {
          setState({ isNarrow: showNarrowAlert });
        }
      }}
    >
      <div role="alert" {...customProps} className={alertClassNames()}>
        <div className={bodyClassNameForParent()}>
          {getAlertIcon(e.type, e.customIcon)}
          {alertMessageContent()}
        </div>
        {actionsSection()}
      </div>
    </ResponsiveElement>
  );
};

// Alert.Opts = {};
// Alert.Opts.Types = AlertTypes;
