import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Alert from '../../../Alert';
export default () => (
  <Alert id="errorAlert" type={Alert.Opts.Types.ERROR}>
    a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j
    k l m n o p q r s t u v w x y z
  </Alert>
);
