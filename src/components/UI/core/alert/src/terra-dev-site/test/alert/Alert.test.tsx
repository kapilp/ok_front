import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import Alert from '../../../Alert';
type AlertActionButtonState = {
  actionButtonClickCount?: number;
  showAlert?: boolean;
};
class AlertActionButton extends React.Component<{}, AlertActionButtonState> {
  alert: any;
  constructor(props) {
    super(props);
    state = {
      actionButtonClickCount: 0,
      showAlert: false,
    };
    actionFunc = actionFunc.bind(this);
    popAlert = popAlert.bind(this);
  }
  actionFunc() {
    setState(prevState => ({
      actionButtonClickCount: prevState.actionButtonClickCount + 1,
    }));
    setState({ showAlert: false });
  }
  popAlert() {
    setState(prevState => ({ showAlert: !prevState.showAlert }));
  }
  render() {
    alert = (
      <Alert id="actionAlert" type="warning" action={<Button text="Action" onClick={actionFunc} />}>
        This is a warning. It is configured with a custom Action button. Action button has been clicked <span id="actionButtonClickCount">{state.actionButtonClickCount}</span>{' '}
        times.
      </Alert>
    );
    return (
      <div>
        <p> This Test has been added to test the functionality on JAWS and Voice Over </p>
        <Button text="Trigger Alert" onClick={popAlert} />
        {state.showAlert && alert}
      </div>
    );
  }
}
export default AlertActionButton;
