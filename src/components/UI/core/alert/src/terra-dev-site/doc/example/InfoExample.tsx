import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Alert } from '../../../Alert';
export const AlertInfoExample = () => <Alert type="info">This is an information alert</Alert>;
