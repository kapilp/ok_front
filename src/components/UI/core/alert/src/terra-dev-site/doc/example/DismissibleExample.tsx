import { Button } from '../../../../../button/src/Button';
import { Alert } from '../../../Alert';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';

export const AlertDismissibleExample = () => {
  const [state, setState] = createStore({ isOpen: true });

  return (
    <>
      {!state.isOpen ? (
        <>
          <div id="dismissed">Alert was dismissed</div>
          <Button
            text="Trigger Alert"
            onClick={() => {
              setState({ isOpen: true });
            }}
          />
        </>
      ) : (
        <Alert id="dismissibleAlert" type="success" onDismiss={() => setState({ isOpen: false })}>
          This is a dismissable Alert.
        </Alert>
      )}
    </>
  );
};
