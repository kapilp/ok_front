import { Alert } from '../../../Alert';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Button } from '../../../../../button/src/Button';

export const AlertActionExample = () => {
  const [state, setState] = createStore({ actionButtonClickCount: 0 });
  return (
    <Alert
      id="actionAlert"
      type="warning"
      action={
        <Button
          text="Action"
          variant="emphasis"
          onClick={() => {
            setState({ actionButtonClickCount: state.actionButtonClickCount + 1 });
          }}
        />
      }
    >
      {`This is a warning. It is configured with a custom Action button. Action button has been clicked ${state.actionButtonClickCount} times.`}
    </Alert>
  );
};
