import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './colors.module.scss';
import { Alert } from '../../../Alert';
import { SvgIconHelp } from '../../../../../icon/src/icon/IconHelp';
const cx = classNames.bind(styles);
export const AlertCustomExample = () => (
  <Alert type="custom" title="Help!" customColorClass={cx(['my-app-alert-help-example'])} customIcon={<SvgIconHelp />}>
    <span>
      This is a<b> custom</b> alert
    </span>
  </Alert>
);
