import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Alert } from '../../../Alert';
export const AlertWarningExample = () => <Alert type="warning">This is a warning</Alert>;
