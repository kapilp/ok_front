import classNames from 'classnames/bind';

import styles from './colors.module.scss';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Alert } from '../../../Alert';
import { Button } from '../../../../../button/src/Button';
const cx = classNames.bind(styles);
export const AlertActionAndDismissibleExample = () => {
  const [state, setState] = createStore({ actionButtonClickCount: 0, isOpen: true });

  return (
    <>
      {state.isOpen && (
        <Alert
          type="custom"
          onDismiss={() => setState({ isOpen: false })}
          customColorClass={cx(['my-app-alert-dismiss-example'])}
          action={
            <Button
              text="Action"
              variant="emphasis"
              onClick={() => {
                const updatedCount = state.actionButtonClickCount + 1;
                setState({ actionButtonClickCount: updatedCount });
              }}
            />
          }
        >
          This is a custom alert with no icon or title. It is configured to be dismissible and with a custom action button. Click on the Dismiss button to dismiss the alert.
        </Alert>
      )}
      <br />
      <p>{`Action button has been clicked ${state.actionButtonClickCount} times.`}</p>
      {!state.isOpen && (
        <Button
          text="Trigger Alert"
          onClick={() => {
            setState({ isOpen: true });
          }}
        />
      )}
    </>
  );
};
