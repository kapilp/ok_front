import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Alert } from '../../../Alert';
export const AlertSuccessExample = () => <Alert type="success">This is a success alert</Alert>;
