import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Placeholder.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The themed variant of the text and border. Example, use the light variant when against a dark background.
   */
  variant: 'light' | 'dark';
  /**
   * The placeholder text to be displayed.
   */
  title: string;
}
const defaultProps = {
  variant: 'dark',
  title: '',
};
export const Placeholder = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [extracted, customProps] = splitProps(props, ['variant', 'title']);
  return (
    <div {...customProps} className={cx(['placeholder', customProps.className])}>
      <div className={cx(['inner', `is-${props.variant}`])}>
        <h2>{props.title}</h2>
      </div>
    </div>
  );
};
