import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import TestComponent from './TestComponent';
const TestComponentExample = () => <TestComponent text="Hey" otherText="Seeya" />;
export default TestComponentExample;
