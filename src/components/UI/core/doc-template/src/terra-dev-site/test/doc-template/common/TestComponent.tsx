import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
interface Properties {
  /**
   * A string of text to be displayed first
   */
  text: string;
  /**
   * A string of text to be displayed second
   */
  otherText: string;
}
const defaultProps = {
  text: 'Hi!',
  otherText: 'Bye.',
};
export const TestComponent = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  return (
    <p>
      {props.text}, {props.otherText}
    </p>
  );
};
