import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { NpmBadge } from '../../../NpmBadge';
const name = 1;
const version = 2;
const packageUrl = 'https://engineering.cerner.com/terra-ui/components/terra-doc-template/doc-template/doc-template';
export const BadgeExamples = () => (
  <div>
    <p>Dynamically generated npm badge:</p>
    <NpmBadge packageName={name} />
    <p>Custom Badge with packageVersion prop set:</p>
    <NpmBadge packageName={name} packageVersion={version} />
    <p>Custom Badge with packageUrl and packageVersion props set:</p>
    <NpmBadge packageName={name} packageUrl={packageUrl} packageVersion={version} />
  </div>
);
