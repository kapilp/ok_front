import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import DocTemplate from '../../../DocTemplate';
import ChangeLog from '../../../../CHANGELOG.md';
const DocPage = () => <DocTemplate packageName="terra-doc-template" srcPath="https://github.com/cerner/terra-core/tree/main/packages/terra-doc-template" readme={ChangeLog} />;
export default DocPage;
