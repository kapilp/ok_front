import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './DocTemplateDocCommon.module.scss';
import { Placeholder } from '../../../Placeholder';
const cx = classNames.bind(styles);
export const PlaceholderDefault = () => (
  <div className={cx('placeholder-default')}>
    <Placeholder title="Dark Placeholder" />
  </div>
);
