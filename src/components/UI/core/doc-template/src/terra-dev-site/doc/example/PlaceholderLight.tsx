import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './DocTemplateDocCommon.module.scss';
import { Placeholder } from '../../../Placeholder';
const cx = classNames.bind(styles);
export const PlaceholderLight = () => (
  <div className={cx('placeholder-light')}>
    <Placeholder title="Light Placeholder" variant="light" />
  </div>
);
