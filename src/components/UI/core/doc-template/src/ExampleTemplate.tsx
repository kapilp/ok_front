import {  JSX,  mergeProps, Show, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import jsx from 'react-syntax-highlighter/dist/cjs/languages/prism/jsx';
import SyntaxHighlighter from 'react-syntax-highlighter/dist/cjs/prism-light';
import theme from './syntax-highlighter-theme';
import classNames from 'classnames/bind';
SyntaxHighlighter.registerLanguage('jsx', jsx);
const cx = classNames.bind(styles);

interface Properties {
  /**
   * The example component.
   */
  example: JSX.Element;
  /**
   * The example source code.
   */
  exampleSrc: string;
  /**
   * The example title.
   */
  title: string;
  /**
   * The example description.
   */
  description: JSX.Element;
  /**
   * Additional content.
   */
  children: JSX.Element;
}
const defaultProps = {
  example: undefined,
  exampleSrc: undefined,
  title: undefined,
  description: undefined,
  children: undefined,
};
interface IExampleTemplateProps extends JSX.HTMLAttributes<Element> {
  title?: any;
  example?: any;
  exampleSrc?: any;
  description?: any;
  customProps?: any;
}
type ExampleTemplateState = {
  isExpanded?: boolean;
  isBackgroundTransparent?: boolean;
};
export const ExampleTemplate extends React.Component<IExampleTemplateProps, ExampleTemplateState> {
  props = mergeProps({}, defaultProps, props)
  constructor(props) {
    super(props);
    state = {
      isExpanded: false,
      isBackgroundTransparent: false,
    };
    handleBgToggle = handleBgToggle.bind(this);
    handleCodeToggle = handleCodeToggle.bind(this);
  }
  handleBgToggle() {
    setState(prevState => ({
      isBackgroundTransparent: !prevState.isBackgroundTransparent,
    }));
  }
  handleCodeToggle() {
    setState(prevState => ({ isExpanded: !prevState.isExpanded }));
  }
  render() {
    const { title, example, exampleSrc, children, description, ...customProps } = props;
    const { isExpanded, isBackgroundTransparent } = state;
    let dynamicContentStyle = {};
    if (isBackgroundTransparent) {
      dynamicContentStyle = {
        backgroundColor: 'rgba(0, 0, 0, 0)',
      };
    }
    return (
      <div {...customProps} className={cx('tNpmBadge', 'template', customProps.className)}>
        {title && (
          <div className={cx('header')}>
            <h2 className={cx('title')}>{title}</h2>
          </div>
        )}

        <div className={cx('content')} style={dynamicContentStyle}>
          {description && <div className={cx('description')}>{description}</div>}
          {example}
          {children}
        </div>

        {exampleSrc && (
          <div className={cx('footer')}>
            <div className={cx('button-container')}>
              <button type="button" className={cx('bg-toggle')} onClick={handleBgToggle}>
                Toggle Background
              </button>
              <button type="button" className={cx('code-toggle')} onClick={handleCodeToggle}>
                <span className={cx('chevron-left')} />
                <span>Code</span>
                <span className={cx('chevron-right')} />
              </button>
            </div>
            <div className={cx('code', { 'is-expanded': isExpanded })} aria-hidden={!isExpanded}>
              {isExpanded ? (
                <SyntaxHighlighter language="jsx" style={theme} customStyle={{ margin: '0', borderRadius: '0' }}>
                  {exampleSrc}
                </SyntaxHighlighter>
              ) : undefined}
            </div>
          </div>
        )}
      </div>
    );
  }
}


