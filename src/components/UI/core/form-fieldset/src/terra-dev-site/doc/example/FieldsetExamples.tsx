import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Input } from '../../../../../form-input/src/Input';
import { Fieldset } from '../../../Fieldset';
import classNames from 'classnames/bind';
import styles from './FieldsetExamples.module.scss';
const cx = classNames.bind(styles);
export const FieldsetExamples = () => {
  const [state, setState] = createStore({
    first: '',
    middle: '',
    last: '',
  });

  const handleFirstChange = (event: Event) => {
    setState({ first: event.target.value });
  };
  const handleMiddleChange = (event: Event) => {
    setState({ middle: event.target.value });
  };
  const handleLastChange = (event: Event) => {
    setState({ last: event.target.value });
  };

  return (
    <div>
      <Fieldset
        // type="checkbox"
        legend="Give your full name here"
        name="children_present"
        value="children_present"
        error="All fields must be filled out"
        help="Families are eligible for family package plans"
        required
      >
        <Field label="First" isInline required htmlFor="first">
          <Input id="first" type="text" name="first" defaultValue="" onChange={handleFirstChange} />
        </Field>
        <Field label="Middle" isInline required htmlFor="middle">
          <Input id="middle" type="text" name="middle" defaultValue="" onChange={handleMiddleChange} />
        </Field>
        <Field label="Last" isInline required htmlFor="last">
          <Input id="last" type="text" name="last" defaultValue="" onChange={handleLastChange} />
        </Field>
      </Fieldset>
      <hr />
      <p>
        Full Name Provided:
        <span className={cx('fieldset-wrapper')}>
          {state.first} {state.middle} {state.last}
        </span>
      </p>
      <br />
    </div>
  );
};
