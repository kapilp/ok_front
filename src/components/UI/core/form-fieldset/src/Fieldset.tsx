/* eslint-disable react/jsx-boolean-value, jsx-a11y/label-has-for */
import { JSX, createMemo, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Fieldset.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Children the Field contains.
   */
  children?: JSX.Element;
  /**
   * Help element to display with other elements.
   */
  help?: JSX.Element;
  /**
   * Legend for the input group.
   */
  legend?: string;
  /**
   * Attributes to attach to the legend.
   */
  // eslint-disable-next-line react/forbid-prop-types
  legendAttrs?: {};
  /**
   * Determines whether the fieldset is required.
   */
  required?: boolean;
  /**
   * Whether or not the legend is visible. Use this prop to hide a legend while still creating it on the DOM for accessibility.
   */
  isLegendHidden?: boolean;
}
const defaultProps = {
  legendAttrs: {},
  required: false,
  isLegendHidden: false,
};
export const Fieldset = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'help', 'legend', 'legendAttrs', 'isLegendHidden', 'required', 'className']);
  const theme = useContext(ThemeContext);
  const fieldsetClasses = createMemo(() => classNames(cx(['fieldset', { 'fieldset-required': props.required }, theme.className]), p.className));
  const legendClasses = createMemo(() => cx(['legend', props.legendAttrs.className, { 'legend-visually-hidden': props.isLegendHidden }]));

  return (
    <fieldset {...customProps} className={fieldsetClasses()}>
      {props.legend && (
        <legend {...props.legendAttrs} className={legendClasses()}>
          {props.legend}
        </legend>
      )}
      {props.help && (
        <small className={cx('help-text')} tabIndex="-1">
          {props.help}
        </small>
      )}
      <div className={cx('fieldset-children')}>{props.children}</div>
    </fieldset>
  );
};
