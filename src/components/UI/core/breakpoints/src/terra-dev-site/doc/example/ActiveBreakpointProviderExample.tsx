import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ActiveBreakpointContext } from '../../../ActiveBreakpointContext';
import { ActiveBreakpointProvider } from '../../../ActiveBreakpointProvider';
import { WithActiveBreakpoint } from '../../../withActiveBreakpoint';

const ActiveBreakpointConsumer1 = () => (
  <WithActiveBreakpoint>
    {state => (
      <div>
        {JSON.stringify(state)}
        <p>This component uses the withActiveBreakpoint() component generator to interface with the ActiveBreakpointProvider.</p>
        // Bug this not display value:
        <p>The active breakpoint is: {state.activeBreakpoint}</p>
      </div>
    )}
  </WithActiveBreakpoint>
);

const ActiveBreakpointConsumer2 = () => {
  const state = useContext(ActiveBreakpointContext);
  return (
    <div>
      <p>This component interfaces with the ActiveBreakpointContext directly.</p>
      <p>The active breakpoint is: {state.activeBreakpoint}</p>
    </div>
  );
};
export const ActiveBreakpointProviderExample = () => (
  <ActiveBreakpointProvider>
    <ActiveBreakpointConsumer1 />
    <hr />
    <ActiveBreakpointConsumer2 />
  </ActiveBreakpointProvider>
);
