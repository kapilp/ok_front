import { ActiveBreakpointContext } from './ActiveBreakpointContext';
import { activeBreakpointForSize } from './breakpoints';
import {  JSX, onCleanup  } from 'solid-js';
import { isServer } from 'solid-js/web'
import { createStore } from 'solid-js/store';

interface Properties {
  /**
   * The component(s) that will be wrapped by `<ActiveBreakpointContext.Provider />`.
   */
  children: JSX.Element;
}
type ActiveBreakpointProviderState = {
  activeBreakpoint?: any;
};
export const ActiveBreakpointProvider = (props: Properties) => {
  const [state, setState] = createStore({
    activeBreakpoint: activeBreakpointForSize(isServer ? 1440: window.innerWidth),
  });

  !isServer && window.addEventListener('resize', setActiveBreakpoint);
  // This event is needed to handle mobile device rotation since the resize event is not triggered.
  !isServer && window.addEventListener('orientationchange', setActiveBreakpoint);

  onCleanup(() => {
    if(!isServer){
    window.removeEventListener('resize', setActiveBreakpoint);
    window.removeEventListener('orientationchange', setActiveBreakpoint);
    }
  });

  function setActiveBreakpoint() {
    const newBreakpoint = activeBreakpointForSize(window.innerWidth);

    if (state.activeBreakpoint !== newBreakpoint) {
      setState({
        activeBreakpoint: newBreakpoint,
      });
    }
  }

  return <ActiveBreakpointContext.Provider value={state}>{props.children}</ActiveBreakpointContext.Provider>;
};
