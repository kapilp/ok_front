import { createContext } from 'solid-js';
import { breakpoints } from './breakpoints';
export const ActiveBreakpointContext = createContext<typeof breakpoints>();
