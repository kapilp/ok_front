import { ActiveBreakpointContext } from './ActiveBreakpointContext';
import { JSX, useContext } from 'solid-js';
export const WithActiveBreakpoint = (props: { children: JSX.Element }) => {
  const state = useContext(ActiveBreakpointContext);
  return () => props.children({ state });
};
