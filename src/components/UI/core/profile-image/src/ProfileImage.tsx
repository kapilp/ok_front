import { JSX, mergeProps, useContext } from 'solid-js';
import { Image } from '../../image/src/Image';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './ProfileImage.module.scss';

const cx = classNamesBind.bind(styles);
/* eslint react/no-unused-prop-types: [0] */
interface Properties {
  /**
   * The source for the image which will be displayed.
   */
  src: string;
  /**
   * The text content that specifies an alternative text for an image.
   */
  alt: string;
  /**
   * Sets the `object-fit` style of the image from the following values: `cover`, `contain`, `scale-down`, `none`.
   * ![IMPORTANT](https://badgen.net/badge/UX/Design-Standards/blue) Anywhere the terra-profile-image is used to show images of People, _only_ `cover` and `contain` are acceptable.
   */
  fit: 'cover' | 'scale-down' | 'contain' | 'none';
  /**
   * Sets the height of the image.
   */
  height: string;
  /**
   * Sets the width of the image.
   */
  width: string;
  /**
   * Function to be executed when the profile image load is successful.
   */
  onLoad: () => void;
  /**
   * Function to be executed when the profile image load errors.
   */
  onError: () => void;
}
const defaultProps = {
  fit: 'cover',
};
const isOnlyNumbers = toTest => !/\D/.test(toTest);
export const ProfileImage = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  // img tags assume a height attribute of only numbers is in px but CSS does not
  /* eslint-disable react/forbid-dom-props */
  const PlaceholderImage = () => (
    <span
      className={cx('placeholder-images', theme.className)}
      title={props.alt}
      style={{ height: isOnlyNumbers(props.height) ? `${props.height}px` : props.height, width: isOnlyNumbers(props.width) ? `${props.width}px` : props.width }}
    />
  );

  return (
    <>
      {props.src ? (
        <div>
          <Image placeholder={<PlaceholderImage />} {...props} />
        </div>
      ) : (
        <PlaceholderImage />
      )}
    </>
  );
};
