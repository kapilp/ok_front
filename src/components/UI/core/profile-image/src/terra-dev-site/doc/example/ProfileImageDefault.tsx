import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import exampleProfileImage from '../../assets/150x150.jpg';
import { ProfileImage } from '../../../ProfileImage';
export const ProfileImageDefault = () => (
  <div>
    <h2>Successful Profile Image</h2>
    <ProfileImage alt="profile image" src={exampleProfileImage} width="75" height="75" />
    <br />
    <h2>Failed Profile Image</h2>
    <ProfileImage alt="profile image" src="invalid.jpg" width="75" height="75" />
  </div>
);
