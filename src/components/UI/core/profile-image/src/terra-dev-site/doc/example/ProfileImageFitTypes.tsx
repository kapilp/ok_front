import { JSX, Component, mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import exampleProfileImage from '../../assets/170x251.jpg';
import { ProfileImage } from '../../../ProfileImage';
import { FitTypesWrapper } from './common/FitTypesWrapper';

export const ProfileImageFitTypes = props => (
  <FitTypesWrapper>{p => <ProfileImage alt="Toggle fit style" src={exampleProfileImage} fit={p.fit} height="100" width="100" />}</FitTypesWrapper>
);
