import { createStore } from 'solid-js/store';

export const FitTypesWrapper = props => {
  const [state, setState] = createStore({ fitType: 'cover' });
  debugger;
  const handleOnSelect = event => {
    setState({ fitType: event.target.value });
  };

  return (
    <div>
      {props.children({ fit: state.fitType })}
      <p>
        <label htmlFor="fitType">Select a Fit Type:</label>
      </p>
      <select id="fitType" name="fitType" value={state.fitType} onChange={handleOnSelect}>
        <option value="cover">cover</option>
        <option value="contain">contain</option>
        <option value="scale-down">scale-down</option>
        <option value="none">none</option>
      </select>
    </div>
  );
};
