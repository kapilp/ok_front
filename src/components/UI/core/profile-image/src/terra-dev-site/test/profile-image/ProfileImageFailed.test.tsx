import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ProfileImage from '../../../ProfileImage';
const ProfileImageFailed = () => (
  <div>
    <h2>Failed Profile Image</h2>
    <ProfileImage src="invalid.jpg" alt="could not load profile image" width="75" height="75" />
  </div>
);
export default ProfileImageFailed;
