import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import Button from "terra-button";
import * as KeyCode from "keycode-js";
import IconSearch from "terra-icon/lib/icon/IconSearch";
import { injectIntl, intlShape } from "react-intl";
import styles from "./SearchField.module.scss";
const cx = classNamesBind.bind(styles);
const Icon = <IconSearch />;
interface Properties {
  /**
   * The defaultValue of the search field. Use this to create an uncontrolled search field.
   */
  defaultValue: string,
  /**
   * When true, will disable the auto-search.
   */
  disableAutoSearch: boolean,
  /**
   * Callback ref to pass into the inner input component.
   */
  inputRefCallback: PropTypes.func,
  /**
   * Custom input attributes to apply to the input field such as aria-label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  inputAttributes: PropTypes.object,
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  intl: intlShape.isRequired,
  /**
   * Whether or not the field should display as a block.
   */
  isBlock: boolean,
  /**
   * When true, will disable the field.
   */
  isDisabled: boolean,
  /**
   * The minimum number of characters to perform a search.
   */
  minimumSearchTextLength: PropTypes.number,
  /**
   * Function to trigger when user changes the input value. Provide a function to create a controlled input.
   */
  onChange: PropTypes.func,
  /**
   * Function to trigger when user inputs a value. Use when programmatically setting a value. Sends parameter {Event} event.
   */
  onInput: PropTypes.func,
  /**
   * A callback to indicate an invalid search. Sends parameter {String} searchText.
   */
  onInvalidSearch: PropTypes.func,
  /**
   * A callback to perform search. Sends parameter {String} searchText.
   */
  onSearch: PropTypes.func,
  /**
   * Placeholder text to show while the search field is empty.
   */
  placeholder: string,
  /**
   * How long the component should wait (in milliseconds) after input before performing an automatic search.
   */
  searchDelay: PropTypes.number,
  /**
   * The value of search field.  Use this to create a controlled search field.
   */
  value: string
};
const defaultProps = {
  defaultValue: undefined,
  disableAutoSearch: false,
  isBlock: false,
  isDisabled: false,
  minimumSearchTextLength: 2,
  placeholder: "",
  searchDelay: 250,
  value: undefined,
  inputAttributes: undefined
};
interface ISearchFieldProps extends JSX.HTMLAttributes<Element> {
  defaultValue?: any;
  disableAutoSearch?: any;
  inputRefCallback?: any;
  inputAttributes?: any;
  intl?: any;
  isBlock?: any;
  isDisabled?: any;
  minimumSearchTextLength?: any;
  onChange?: any;
  onInput?: any;
  onInvalidSearch?: any;
  onSearch?: any;
  placeholder?: any;
  searchDelay?: any;
  value?: any;
  customProps?: any;
}
export const SearchField extends React.Component<ISearchFieldProps, {}> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  context: any;
  forceUpdate: any;
  inputRef: any;
  searchText: string;
  searchTimeout: any;
  constructor(props) {
    super(props);
    handleClear = handleClear.bind(this);
    handleTextChange = handleTextChange.bind(this);
    handleSearch = handleSearch.bind(this);
    handleKeyDown = handleKeyDown.bind(this);
    handleInput = handleInput.bind(this);
    setInputRef = setInputRef.bind(this);
    updateSearchText = updateSearchText.bind(this);
    searchTimeout = null;
    searchText = props.defaultValue || props.value;
  }
  componentDidUpdate() {
    // if consumer updates the value prop with onChange, need to update variable to match
    updateSearchText(props.value);
  }
  componentWillUnmount() {
    clearSearchTimeout();
  }
  setInputRef(node) {
    inputRef = node;
    if (props.inputRefCallback) {
      props.inputRefCallback(node);
    }
  }
  handleClear(event) {
    // Pass along changes to consuming components using associated props
    if (props.onChange) {
      props.onChange(event, "");
    }
    if (props.onInvalidSearch) {
      props.onInvalidSearch("");
    }
    updateSearchText("");
    // Clear input field
    if (inputRef) {
      inputRef.value = "";
      inputRef.focus();
    }
  }
  handleTextChange(event) {
    const textValue = event.target.value;
    updateSearchText(textValue);
    if (props.onChange) {
      props.onChange(event, textValue);
    }
    if (!searchTimeout && !props.disableAutoSearch) {
      searchTimeout = setTimeout(
        handleSearch,
        props.searchDelay
      );
    }
  }
  updateSearchText(searchText) {
    if (typeof searchText !== "undefined" && searchText !== searchText) {
      searchText = searchText;
      // Forcing update for clearButton rerender.
      forceUpdate();
    }
  }
  handleInput(event) {
    const textValue = event.target.value;
    updateSearchText(textValue);
    if (props.onInput) {
      props.onInput(event);
    }
  }
  handleKeyDown(event) {
    if (event.keyCode === KeyCode.KEY_RETURN) {
      handleSearch();
    }
    if (event.keyCode === KeyCode.KEY_ESCAPE) {
      handleClear(event);
    }
  }
  handleSearch() {
    clearSearchTimeout();
    const searchText = searchText || "";
    if (
      searchText.length >= props.minimumSearchTextLength &&
      props.onSearch
    ) {
      props.onSearch(searchText);
    } else if (props.onInvalidSearch) {
      props.onInvalidSearch(searchText);
    }
  }
  clearSearchTimeout() {
    if (searchTimeout) {
      clearTimeout(searchTimeout);
      searchTimeout = null;
    }
  }
  render() {
    const {
      defaultValue,
      disableAutoSearch,
      inputRefCallback,
      inputAttributes,
      intl,
      isBlock,
      isDisabled,
      minimumSearchTextLength,
      onChange,
      onInput,
      onInvalidSearch,
      onSearch,
      placeholder,
      searchDelay,
      value,
      ...customProps
    } = props;
    const theme = context;
    const searchFieldClassNames = classNames(
      cx("search-field", { block: isBlock }, theme.className),
      customProps.className
    );
    const inputText =
      inputAttributes &&
      Object.prototype.hasOwnProperty.call(inputAttributes, "aria-label")
        ? inputAttributes["aria-label"]
        : intl.formatMessage({ id: "Terra.searchField.search" });
    const buttonText = intl.formatMessage({
      id: "Terra.searchField.submit-search"
    });
    const clearText = intl.formatMessage({ id: "Terra.searchField.clear" });
    const additionalInputAttributes = { ...inputAttributes };
    const clearIcon = <span className={cx("clear-icon")} />;
    const inputClass = classNames(
      cx(
        'input',
      ),
      additionalInputAttributes.className,
    );
    if (value !== undefined) {
      additionalInputAttributes.value = value;
    } else {
      additionalInputAttributes.defaultValue = defaultValue;
    }
    const clearButton =
      searchText && !isDisabled ? (
        <Button
          className={cx("clear")}
          onClick={handleClear}
          text={clearText}
          variant="utility"
          icon={clearIcon}
          isIconOnly
        />
      ) : (
        undefined
      );
    return (
      <div {...customProps} className={searchFieldClassNames}>
        <div className={cx("input-group")}>
          <input
            {...additionalInputAttributes}
            className={inputClass}
            type="search"
            placeholder={placeholder}
            onChange={handleTextChange}
            disabled={isDisabled}
            aria-label={inputText}
            aria-disabled={isDisabled}
            onKeyDown={handleKeyDown}
            onInput={handleInput}
            ref={setInputRef}
          />
          {clearButton}
        </div>
        <Button
          className={cx("button")}
          text={buttonText}
          onClick={handleSearch}
          isDisabled={isDisabled}
          icon={Icon}
          isIconOnly
          isCompact
        />
      </div>
    );
  }
}

export default injectIntl(SearchField);
