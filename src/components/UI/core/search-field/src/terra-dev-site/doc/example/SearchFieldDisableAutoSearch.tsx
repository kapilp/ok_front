import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from 'terra-search-field';
const SearchFieldDisableAutoSearch = () => (
  <div>
    <SearchField disableAutoSearch />
  </div>
);
export default SearchFieldDisableAutoSearch;
