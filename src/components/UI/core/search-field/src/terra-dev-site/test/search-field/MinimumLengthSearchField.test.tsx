import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type MinimumLengthSearchFieldState = {
  searchText?: string;
};
class MinimumLengthSearchField extends React.Component<{}, MinimumLengthSearchFieldState> {
  constructor(props) {
    super(props);
    state = {
      searchText: '',
    };
  }
  render() {
    return (
      <div>
        <div id="search-callback-text">Search Text: {state.searchText}</div>
        <SearchField
          id="searchfieldWithMinimumLength"
          minimumSearchTextLength={5}
          onSearch={searchText => {
            setState({ searchText });
          }}
        />
      </div>
    );
  }
}
export default MinimumLengthSearchField;
