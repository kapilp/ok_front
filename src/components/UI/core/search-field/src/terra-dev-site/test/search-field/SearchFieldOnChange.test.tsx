import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type SearchFieldOnChangeState = {
  searchText?: string;
  callCount?: number;
};
class SearchFieldOnChange extends React.Component<{}, SearchFieldOnChangeState> {
  constructor(props) {
    super(props);
    onChange = onChange.bind(this);
    state = { searchText: '', callCount: 0 };
  }
  onChange(event, text) {
    let searchText = text;
    if (text && text.length > 0 && /\d/.test(text)) {
      searchText = text.substring(0, text.length - 1);
    }
    setState(prevState => ({
      searchText,
      callCount: prevState.callCount + 1,
    }));
  }
  render() {
    return (
      <div>
        <p id="searchOnChangeCallCount">{state.callCount}</p>
        <p id="searchOnChangeText">{state.searchText}</p>
        <SearchField id="searchfield" onChange={onChange} value={state.searchText} />
      </div>
    );
  }
}
export default SearchFieldOnChange;
