import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type CallbackSearchFieldState = {
  searchText?: string;
  message?: string;
};
class CallbackSearchField extends React.Component<{}, CallbackSearchFieldState> {
  constructor(props) {
    super(props);
    state = {
      searchText: '',
      message: '',
    };
    handleSearch = handleSearch.bind(this);
    handleInvalidSearch = handleInvalidSearch.bind(this);
  }
  handleSearch(searchText) {
    setState({ searchText, message: 'Search Text: ' });
  }
  handleInvalidSearch(searchText) {
    setState({ searchText, message: 'INVALID Search Text: ' });
  }
  render() {
    return (
      <div>
        <div> Minimum Search Length is 3 </div>
        <SearchField id="searchfield" onSearch={handleSearch} onInvalidSearch={handleInvalidSearch} minimumSearchTextLength={3} />
        <div id="search-callback-text">
          {state.message}
          {state.searchText}
        </div>
      </div>
    );
  }
}
export default CallbackSearchField;
