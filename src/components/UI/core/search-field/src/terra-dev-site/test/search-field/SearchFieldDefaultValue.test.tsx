import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
export default () => <SearchField id="searchfield" defaultValue="Default" />;
