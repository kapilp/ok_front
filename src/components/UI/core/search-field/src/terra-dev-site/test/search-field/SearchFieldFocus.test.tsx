import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button/lib/Button';
import SearchField from '../../../SearchField';
type SearchFieldFocusState = {
  searchText?: string;
};
class SearchFieldFocus extends React.Component<{}, SearchFieldFocusState> {
  searchInput: any;
  constructor(props) {
    super(props);
    onChange = onChange.bind(this);
    handleButtonClick = handleButtonClick.bind(this);
    state = { searchText: '' };
  }
  onChange(event, text) {
    setState({ searchText: text });
  }
  handleButtonClick() {
    if (searchInput) {
      searchInput.focus();
    }
  }
  render() {
    return (
      <>
        <Button text="Focus Me" onClick={handleButtonClick} id="search-field-focus-button" />
        <SearchField
          onChange={onChange}
          value={state.searchText}
          inputRefCallback={inputRef => {
            searchInput = inputRef;
          }}
        />
      </>
    );
  }
}
export default SearchFieldFocus;
