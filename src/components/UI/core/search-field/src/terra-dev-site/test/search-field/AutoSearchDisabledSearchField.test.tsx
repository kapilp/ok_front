import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type AutoSearchDisabledSearchFieldState = {
  searchText?: string;
  message?: string;
};
class AutoSearchDisabledSearchField extends React.Component<{}, AutoSearchDisabledSearchFieldState> {
  constructor(props) {
    super(props);
    state = {
      searchText: '',
      message: '',
    };
    handleSearch = handleSearch.bind(this);
    handleInvalidSearch = handleInvalidSearch.bind(this);
  }
  handleSearch(searchText) {
    setState({ searchText, message: 'Search Text: ' });
  }
  handleInvalidSearch(searchText) {
    setState({ searchText, message: 'INVALID Search Text: ' });
  }
  render() {
    return (
      <div>
        <div> Auto Searching is Disabled </div>
        <SearchField id="searchfield" onSearch={handleSearch} onInvalidSearch={handleInvalidSearch} disableAutoSearch />
        <div id="search-callback-text">
          {state.message}
          {state.searchText}
        </div>
      </div>
    );
  }
}
export default AutoSearchDisabledSearchField;
