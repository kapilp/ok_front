import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type AutoSearchDisabledSearchFieldState = {
  searchCount?: number;
  searchText?: string;
  message?: string;
};
class AutoSearchDisabledSearchField extends React.Component<{}, AutoSearchDisabledSearchFieldState> {
  constructor(props) {
    super(props);
    state = {
      searchCount: 0,
      searchText: '',
      message: '',
    };
    handleSearch = handleSearch.bind(this);
    handleInvalidSearch = handleInvalidSearch.bind(this);
  }
  handleSearch(searchText) {
    setState(prevState => ({
      searchCount: prevState.searchCount + 1,
      searchText,
      message: 'Search Text: ',
    }));
  }
  handleInvalidSearch(searchText) {
    setState(prevState => ({
      searchCount: prevState.searchCount + 1,
      searchText,
      message: 'INVALID Search Text: ',
    }));
  }
  render() {
    return (
      <div>
        <h3> Auto Searching is Disabled </h3>
        <SearchField id="searchfield" onSearch={handleSearch} onInvalidSearch={handleInvalidSearch} />
        <div id="search-callback-text">
          {state.message}
          {state.searchText}
        </div>
        <div id="search-count">Search Count: {state.searchCount}</div>
      </div>
    );
  }
}
export default AutoSearchDisabledSearchField;
