import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SearchField from '../../../SearchField';
type DelayedSearchFieldState = {
  searchText?: string;
};
class DelayedSearchField extends React.Component<{}, DelayedSearchFieldState> {
  constructor(props) {
    super(props);
    state = {
      searchText: '',
    };
  }
  render() {
    return (
      <div>
        <div id="search-callback-text">Search Text: {state.searchText}</div>
        <SearchField
          searchDelay={1000}
          onSearch={searchText => {
            setState({ searchText });
          }}
        />
      </div>
    );
  }
}
export default DelayedSearchField;
