import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Scroll.module.scss';

const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * The child content to present in the scrollable region.
   */
  children: JSX.Element;
  /**
   * Function callback for the ref of the scrollable container.
   */
  ref?: (el: HTMLDivElement) => void;
}
export const Scroll = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['children', 'ref']);
  return (
    <div {...customProps} className={cx(['scroll', customProps.className])} ref={p?.ref}>
      {props.children}
    </div>
  );
};
