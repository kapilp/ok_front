import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import classNames from 'classnames/bind';
import styles from './Status.module.scss';
const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Child node. Component to display next to the status indicator.
   */
  children: JSX.Element | string;
  /**
   * Visually hidden text used to convey the meaning of the status indicator to screen readers.
   */
  visuallyHiddenText?: string;
  /**
   * Sets an author defined class, to control the colors of the status indicator.
   *
   * ![IMPORTANT](https://badgen.net/badge//IMPORTANT/blue?icon=github)
   * Adding `var(--my-app...` CSS variables is required for proper re-themeability when creating custom color styles _(see included examples)_.
   */
  colorClass?: string;
}
export const Status = (props: Properties) => {
  if (process.env.NODE_ENV !== 'production' && !props.visuallyHiddenText) {
    // eslint-disable-next-line no-console
    console.warn(
      "'visuallyHiddenText' should be added to convey the meaning of the status indicator for screen readers accessibility. This prop will be required in the next major version bump of terra-status.",
    );
  }
  const [extracted, customProps] = splitProps(props, ['children', 'visuallyHiddenText', 'colorClass']);
  return (
    <div {...customProps} className={cx('status', props.colorClass, customProps.className)}>
      {props.visuallyHiddenText && <VisuallyHiddenText text={props.visuallyHiddenText} />}
      {props.children}
    </div>
  );
};
