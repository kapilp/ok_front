import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import placeholderPic150x150 from './150x150.jpg';
import classNames from 'classnames/bind';
import styles from './colors.module.scss';
import { Status } from '../../../Status';
import { Arrange } from '../../../../../arrange/src/Arrange';
const cx = classNames.bind(styles);
const image = <img className={cx('image-wrapper')} height="150" width="150" src={placeholderPic150x150} alt="placeholder" />;
const simpleText = <div className={cx('text-wrapper')}>Sample text</div>;
export const StatusArrange = () => (
  <div>
    <Status colorClass={cx(['attention'])} visuallyHiddenText="Status Attention">
      <Arrange fitStart={image} fill={simpleText} alignFill="center" />
    </Status>
  </div>
);
