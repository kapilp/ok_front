import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Status } from '../../../Status';
import { SvgIconDue } from '../../../../../icon/src/icon/IconDue';
import classNames from 'classnames/bind';
import styles from './colors.module.scss';

const cx = classNames.bind(styles);
const icon = <SvgIconDue height="60" width="60" />;
export const StatusIcon = () => (
  <Status colorClass={cx(['info'])} visuallyHiddenText="Status Info">
    {icon}
  </Status>
);
