import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Status } from '../../../Status';
import placeholderPic150x150 from './150x150.jpg';
import classNames from 'classnames/bind';
import styles from './colors.module.scss';

const cx = classNames.bind(styles);
const image = <img className={cx('image-wrapper')} height="150" width="150" src={placeholderPic150x150} alt="placeholder" />;
export const StatusImage = () => (
  <Status colorClass={cx(['attention'])} visuallyHiddenText="Status Attention">
    {image}
  </Status>
);
