import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledDefaultInvalidExample = () => (
  <div>
    <Field label="Default" htmlFor="uncontrolled-default-invalid">
      <Input name="default error input" id="uncontrolled-default-invalid" defaultValue="Default Error Input – Uncontrolled" isInvalid />
    </Field>
  </div>
);
