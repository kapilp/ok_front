import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { Field } from '../../../../../form-field/src/Field';
import { Input } from '../../../Input';

export const BlankExample = () => (
  <div>
    <Field label="Blank" htmlFor="blank">
      <Input name="default blank input" id="blank" ariaLabel="Blank" />
    </Field>
  </div>
);
