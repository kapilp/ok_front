import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Input } from '../../../Input';

const PlaceholderExample = () => (
  <div>
    <Field label="Placeholder Text" htmlFor="placeholder">
      <Input name="default placeholder input" placeholder="Placeholder Text" id="placeholder" ariaLabel="Placeholder Text" />
    </Field>
  </div>
);
export default PlaceholderExample;
