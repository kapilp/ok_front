import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledDisabledExample = () => (
  <div>
    <Field label="Disabled" htmlFor="uncontrolled-disabled">
      <Input disabled defaultValue="Disabled Example input – Uncontrolled" name="disabled input" id="uncontrolled-disabled" />
    </Field>
  </div>
);
