import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledInvalidWithIncompleteExample = () => (
  <div>
    <Field label="Default" htmlFor="uncontrolled-invalid-with-incomplete">
      <Input name="invalid wins input" id="uncontrolled-invalid-with-incomplete" required isIncomplete isInvalid />
    </Field>
  </div>
);
