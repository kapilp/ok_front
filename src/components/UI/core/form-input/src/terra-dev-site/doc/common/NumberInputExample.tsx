import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../form-field/src/Field';
import { Input } from '../../../Input';

export const NumberInputExample = () => (
  <div>
    <Field label="Numeric Input" htmlFor="numeric">
      <Input name="number input" id="numeric" type="number" ariaLabel="Numeric Input" />
    </Field>
  </div>
);
