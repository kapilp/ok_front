import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledDisabledInvalidExample = () => (
  <div>
    <Field label="Disabled" htmlFor="uncontrolled-disabled-invalid">
      <Input disabled defaultValue="Disabled Error Example input – Uncontrolled" name="disabled input" id="uncontrolled-disabled-invalid" isInvalid />
    </Field>
  </div>
);
