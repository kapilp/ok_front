import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const ControlledDefaultIncompleteExample = () => {
  const [state, setState] = createStore({ input: '', isIncomplete: true });
  const handleChange = event => {
    setState({ input: event.target.value, isIncomplete: event.target.value.length === 0 });
  };
  return (
    <div>
      <Field label="Default" htmlFor="default-controlled-incomplete">
        <Input name="default incomplete input" id="default-controlled-incomplete" value={state.input} onChange={handleChange} required isIncomplete={state.isIncomplete} />
      </Field>
      <p>
        Input Provided:
        {state.input}
      </p>
    </div>
  );
};
