import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { InputField } from '../../../InputField';
export const NumberInputField = () => (
  <InputField
    inputId="numeric-input"
    label="Numeric Value"
    type="number"
    help="Enter Digits"
    inputAttrs={{
      name: 'numeric',
    }}
  />
);
