import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledFileExample = () => (
  <div>
    <Field label="File">
      <Input name="file input" type="file" ariaLabel="File" />
    </Field>
  </div>
);
