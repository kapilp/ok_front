import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';

export const ControlledDisabledInvalidExample = () => {
  const [state, setState] = createStore({ input: 'Disabled Error Example input' });
  const handleChange = event => {
    setState({ input: event.target.value });
  };

  return (
    <div>
      <Field label="Disabled" htmlFor="controlled-disabled-invalid">
        <Input disabled value={state.input} name="disabled input" id="controlled-disabled-invalid" onChange={handleChange} isInvalid />
      </Field>
      <p>
        Input Provided:
        {state.input}
      </p>
    </div>
  );
};
