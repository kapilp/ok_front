import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';

export const ControlledDefaultExample = () => {
  const [state, setState] = createStore({ input: 'Disabled Input' });
  const handleChange = event => {
    setState({ input: event.target.value });
  };
  return (
    <div>
      <Field label="Default">
        <Input name="default input" value={state.input} onChange={handleChange} ariaLabel="Default" />
      </Field>
      <p>
        Input Provided:
        {state.input}
      </p>
    </div>
  );
};
