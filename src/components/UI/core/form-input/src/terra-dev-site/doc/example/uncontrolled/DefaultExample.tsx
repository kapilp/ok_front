import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledDefaultExample = () => (
  <div>
    <Field label="Default">
      <Input name="default input" defaultValue="Default Input – Uncontrolled" ariaLabel="Default" />
    </Field>
  </div>
);
