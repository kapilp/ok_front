import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { InputField } from '../../../InputField';

export const RequiredInputField = () => (
  <InputField
    inputId="incompleteField"
    label="Incomplete Input Field"
    help="Note: This is help text"
    type="text"
    required
    isIncomplete
    inputAttrs={{
      name: 'requiredField',
    }}
  />
);
