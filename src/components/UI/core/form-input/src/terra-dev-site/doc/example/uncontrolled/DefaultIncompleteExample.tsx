import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';
export const UncontrolledDefaultIncompleteExample = () => (
  <div>
    <Field label="Default" htmlFor="uncontrolled-default-incomplete">
      <Input name="default incomplete input" id="uncontrolled-default-incomplete" required isIncomplete />
    </Field>
  </div>
);
