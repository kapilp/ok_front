import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../../../../form-field/src/Field';
import { Input } from '../../../../Input';

export const ControlledDefaultInvalidExample = () => {
  const [state, setState] = createStore({ input: 'Disabled Error Input' });
  const handleChange = event => {
    setState({ input: event.target.value });
  };
  return (
    <div>
      <Field label="Default" htmlFor="default-controlled-invalid">
        <Input name="default error input" id="default-controlled-invalid" value={state.input} onChange={handleChange} isInvalid />
      </Field>
      <p>
        Input Provided:
        {state.input}
      </p>
    </div>
  );
};
