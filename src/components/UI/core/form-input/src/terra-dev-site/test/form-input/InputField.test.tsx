import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import InputField from '../../../InputField';
import styles from './common/Input.test.module.scss';
const cx = classNames.bind(styles);
type InputFieldExampleState = {
  isInvalid?: boolean;
  isIncomplete?: boolean;
  required?: boolean;
};
class InputFieldExample extends React.Component<{}, InputFieldExampleState> {
  constructor() {
    super();
    state = {
      isInvalid: false,
      isIncomplete: false,
      required: false,
    };
    toggleInvalid = toggleInvalid.bind(this);
    toggleIncomplete = toggleIncomplete.bind(this);
  }
  toggleInvalid() {
    setState(prevState => ({
      isInvalid: !prevState.isInvalid,
    }));
  }
  toggleIncomplete() {
    setState(prevState => ({
      isIncomplete: !prevState.isIncomplete,
      required: !prevState.required,
    }));
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <button type="button" id="validity-toggle" onClick={toggleInvalid}>
          Toggle Validity
        </button>
        <button type="button" id="incomplete-toggle" onClick={toggleIncomplete}>
          Toggle Incomplete
        </button>
        <InputField
          inputId="test-input"
          defaultValue="Value"
          error="Error message."
          help="Help message."
          hideRequired
          inputAttrs={{
            name: 'test',
            type: 'text',
          }}
          isInvalid={state.isInvalid}
          isIncomplete={state.isIncomplete}
          required={state.required}
          label="Label Text"
          labelAttrs={{
            className: 'label',
          }}
          showOptional
        />
      </div>
    );
  }
}
export default InputFieldExample;
