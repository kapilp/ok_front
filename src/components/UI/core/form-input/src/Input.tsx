// added to allow this component to be class based for the input ref callback below
/* eslint-disable react/prefer-stateless-function */
import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Input.module.scss';
import { any } from 'rambda';
import { render } from 'solid-js/web';
const cx = classNamesBind.bind(styles);
interface IInputProps extends JSX.HTMLAttributes<Element> {
  /**
   * The defaultValue of the input field. Use this to create an uncontrolled input.
   */
  defaultValue?: string | number;
  /**
   * Whether the input is disabled.
   */
  disabled?: boolean;
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note?: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Function to trigger when the input loses focus.
   */
  onBlur?: () => void;
  /**
   * Function to trigger when user changes the input value. Provide a function to create a controlled input.
   */
  onChange?: (e: Event & { target: HTMLInputElement }) => void;
  /**
   * Function to trigger when the input gains focus.
   */
  onFocus?: () => void;
  /**
   * Content to be displayed as the name.
   */
  name?: string;
  /**
   * The regular expression that the input's value is checked against.
   * NOTE?: The pattern attribute works with the following input types?: text, date, search, url, tel, email, and password.
   */
  pattern?: string;
  /**
   * Callback ref to pass into the input dom element.
   */
  ref?: (el: HTMLInputElement) => void;
  /**
   * Whether the input is required.
   */
  required?: boolean;
  /**
   * Specifies the type of input element to display.
   */
  type?: string;
  /**
   * The value of the input field. Use this to create a controlled input.
   */
  value?: string | number;
  /**
   * String that labels the current element. 'aria-label' must be present,
   * for accessibility.
   */
  ariaLabel?: string;
}
const defaultProps = {
  defaultValue: undefined,
  disabled: false,
  isIncomplete: false,
  isInvalid: false,
  onBlur: undefined,
  onChange: undefined,
  onFocus: undefined,
  name: null,
  pattern: undefined,
  required: false,
  ref: undefined,
  type: undefined,
  value: undefined,
};

export const Input = (props: IInputProps) => {
  props = mergeProps({}, defaultProps, props);

  const [p, customProps] = splitProps(props, [
    'defaultValue',
    'disabled',
    'isIncomplete',
    'isInvalid',
    'onBlur',
    'onChange',
    'onFocus',
    'name',
    'pattern',
    'ref',
    'required',
    'type',
    'ariaLabel',
    'value',
  ]);
  const theme = useContext(ThemeContext);
  function getAttributes() {
    const attributes = {};

    let ariaLabelText;
    // Handle case of users setting aria-label as a custom prop
    if (attributes && Object.prototype.hasOwnProperty.call(attributes, 'aria-label')) {
      // If they've set aria-label and ariaLabel, use the ariaLabel value,
      // otherwise, fallback to using the aria-label value passed in.
      ariaLabelText = !p.ariaLabel ? attributes['aria-label'] : p.ariaLabel;
    } else if (p.ariaLabel) {
      // If users only set ariaLabel prop, use that value
      ariaLabelText = p.ariaLabel;
    }
    attributes['aria-label'] = ariaLabelText;
    if (p.required) {
      attributes['aria-required'] = 'true';
    }
    if (p.value !== undefined) {
      attributes.value = p.value;
    } else if (p.defaultValue !== undefined) {
      attributes.defaultValue = p.defaultValue;
    }
    if (attributes.placeholder) {
      attributes.placeholder = null;
    }
    return attributes;
  }

  return (
    <input
      {...{ ...customProps, ...getAttributes() }}
      ref={(inputRef: HTMLInputElement) => {
        if (p.ref) p.ref(inputRef);
      }}
      name={p.name}
      type={p.type}
      pattern={p.pattern}
      onBlur={p.onBlur}
      onChange={p.onChange}
      onFocus={p.onFocus}
      disabled={p.disabled}
      required={p.required}
      className={classNames(
        cx('form-input', { 'form-error': p.isInvalid }, { 'form-incomplete': p.isIncomplete && p.required && !p.isInvalid }, theme.className),
        customProps.className,
      )}
    />
  );
};

// Input.isInput = true;
