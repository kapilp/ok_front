import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Field } from '../../form-field/src/Field';
import { Input } from './Input';
import { SvgIconError } from '../../icon/src/icon/IconError';

interface Properties {
  /**
   * Id of the input. Populates both the input and the htmlFor prop of the wrapper Field.
   */
  inputId: string;
  /**
   * The label of the form control children.
   */
  label: string;
  /**
   * The defaultValue of the input field. Use this to create an uncontrolled input.
   */
  defaultValue?: string | number;
  /**
   * Whether the input is disabled.
   */
  disabled?: boolean;
  /**
   * Error message for when the input is invalid.
   */
  error?: JSX.Element;
  /**
   * Error Icon to display when the input is invalid.
   */
  errorIcon?: JSX.Element;
  /**
   * Help element to display with the input.
   */
  help?: JSX.Element;
  /**
   * Whether or not to hide the required indicator on the label.
   */
  hideRequired?: boolean;
  /**
   * Attributes to attach to the input object
   */
  // eslint-disable-next-line react/forbid-prop-types
  inputAttrs?: {};
  /**
   * Whether the field and input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether or not the field is an inline field.
   */
  isInline?: boolean;
  /**
   * Whether the field and input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Whether or not the label is visible. Use this props to hide a label while still creating it on the DOM for accessibility.
   */
  isLabelHidden?: boolean;
  /**
   * Attributes to attach to the label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelAttrs?: {};
  /**
   * Set the max-width of a field using `length` or `%`.  Best practice recommendation to never exceed
   * a rendered value of 1020px. _(Note?: Providing custom inline styles will take precedence.)_
   */
  maxWidth?: string;
  /**
   * Function to trigger when user changes the input value. Provide a function to create a controlled input.
   */
  onChange?: (e: Event & { target: HTMLInputElement }) => void;
  onBlur?: (e: Event & { target: HTMLInputElement }) => void;
  onFocus?: (e: Event & { target: HTMLInputElement }) => void;
  /**
   * Ref callback to pass into the ref attribute of the html input element.
   */
  ref?: (el: HTMLInputElement) => void;
  /**
   * Whether or not the field is required.
   */
  required?: boolean;
  /**
   * Whether or not to append the 'optional' label to a non-required field label.
   */
  showOptional?: boolean;
  /**
   * Specifies the type of input element to display.
   */
  type?: string;
  /**
   * The value of the input field. Use this to create a controlled input.
   */
  value?: string | number;
}
const defaultProps = {
  defaultValue: undefined,
  disabled: false,
  error: null,
  errorIcon: () => <SvgIconError />,
  help: null,
  hideRequired: false,
  inputAttrs: {},
  isIncomplete: false,
  isInline: false,
  isInvalid: false,
  isLabelHidden: false,
  labelAttrs: {},
  onChange: undefined,
  maxWidth: undefined,
  ref: undefined,
  required: false,
  showOptional: false,
  type: undefined,
  value: undefined,
};
export const InputField: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'defaultValue',
    'disabled',
    'error',
    'errorIcon',
    'help',
    'hideRequired',
    'inputAttrs',
    'inputId',
    'isIncomplete',
    'isInline',
    'isInvalid',
    'isLabelHidden',
    'label',
    'labelAttrs',
    'maxWidth',
    'onChange',
    'onBlur',
    'onFocus',
    'ref',
    'required',
    'showOptional',
    'type',
    'value',
  ]);

  const ariaDescriptionIds = createMemo(() => {
    let ariaDescriptionIds;
    if (p.help && p.error && p.isInvalid) {
      ariaDescriptionIds = `${p.inputId}-error ${p.inputId}-help`;
    } else {
      if (p.help) {
        ariaDescriptionIds = `${p.inputId}-help`;
      }
      if (p.error && p.isInvalid) {
        ariaDescriptionIds = `${p.inputId}-error`;
      }
    }
    return ariaDescriptionIds;
  });

  const inputType = createMemo(() => p.type || p.inputAttrs.type);

  if (customProps.placeholder) {
    customProps.placeholder = null;
  }

  return (
    <Field
      label={p.label}
      labelAttrs={p.labelAttrs}
      error={p.error}
      errorIcon={p.errorIcon}
      help={p.help}
      hideRequired={p.hideRequired}
      required={p.required}
      showOptional={p.showOptional}
      isInvalid={p.isInvalid}
      isInline={p.isInline}
      isLabelHidden={p.isLabelHidden}
      htmlFor={p.inputId}
      maxWidth={p.maxWidth}
      {...customProps}
    >
      <Input
        {...p.inputAttrs}
        disabled={p.inputAttrs.disabled || p.disabled}
        id={p.inputId}
        isIncomplete={p.isIncomplete}
        type={inputType()} // p.inputAttrs pass type, and also second time it makes crash
        onChange={p.onChange}
        onBlur={p.onBlur}
        onFocus={p.onFocus}
        value={p.value}
        defaultValue={p.defaultValue}
        ref={p.ref}
        aria-describedby={ariaDescriptionIds()}
      />
    </Field>
  );
};
