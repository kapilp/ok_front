import { SvgIconEdit } from '../../../../../icon/src/icon/IconEdit';
import { Button } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonIcon = () => (
  <div>
    <Button icon={SvgIconEdit} text="icon" className={cx('button')} />
    <Button icon={SvgIconEdit} text="icon" isReversed className={cx('button')} />
    <Button icon={SvgIconEdit} isIconOnly text="Icon Only Button" className={cx('button')} />
  </div>
);
