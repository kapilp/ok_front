import { Button } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonBlock = () => <Button text="Neutral Block" isBlock className={cx('button')} />;
