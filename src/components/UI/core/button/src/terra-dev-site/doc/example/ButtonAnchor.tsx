import { Button } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonAnchor = () => <Button href="#" text="I am rendered with an anchor tag" className={cx('button')} />;
