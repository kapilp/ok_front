import { Button, ButtonVariants } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonCompact = () => (
  <div>
    <Button text="Neutral Standard" className={cx('button')} />
    <Button text="Neutral Compact" isCompact className={cx('button')} />
    <Button text="Emphasis Standard" variant={ButtonVariants.EMPHASIS} className={cx('button')} />
    <Button text="Emphasis Compact" variant={ButtonVariants.EMPHASIS} isCompact className={cx('button')} />
  </div>
);
