import { SvgIconEdit } from '../../../../../icon/src/icon/IconEdit';
import { Button, ButtonVariants } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonDisabled = () => (
  <div>
    <Button text="Disabled Button" isDisabled className={cx('button')} />
    <Button text="Disabled Emphasis" variant={ButtonVariants.EMPHASIS} isDisabled className={cx('button')} />
    <Button text="Disabled Ghost" variant={ButtonVariants.GHOST} isDisabled className={cx('button')} />
    <Button text="Disabled De-emphasis" variant={ButtonVariants['DE-EMPHASIS']} isDisabled className={cx('button')} />
    <Button text="Disabled Action" variant={ButtonVariants.ACTION} isDisabled icon={SvgIconEdit} className={cx('button')} />
    <Button text="Disabled Utility" variant={ButtonVariants.UTILITY} isDisabled icon={SvgIconEdit} className={cx('button')} />
  </div>
);
