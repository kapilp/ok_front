import { SvgIconEdit } from '../../../../../icon/src/icon/IconEdit';
import { Button, ButtonVariants } from '../../../Button';
import classNames from 'classnames/bind';
import styles from './ButtonDocCommon.module.scss';

const cx = classNames.bind(styles);

export const ButtonVariant = () => (
  <div>
    <Button text="Neutral" className={cx('button')} />
    <Button text="Emphasis" variant={ButtonVariants.EMPHASIS} className={cx('button')} />
    <Button text="Ghost" variant={ButtonVariants.GHOST} className={cx('button')} />
    <Button text="De-emphasis" variant={ButtonVariants['DE-EMPHASIS']} className={cx('button')} />
    <Button text="Action" variant={ButtonVariants.ACTION} icon={SvgIconEdit} className={cx('button')} />
    <Button text="Utility" variant={ButtonVariants.UTILITY} icon={SvgIconEdit} className={cx('button')} />
  </div>
);
