import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Button from '../../../../Button';
import styles from '../ButtonTestCommon.module.scss';
const cx = classNames.bind(styles);
const EmphasisButton = () => (
  <div id="emphasis" className={cx('button-wrapper')}>
    <Button id="emphasisButton" text="Emphasis" variant="emphasis" />
    <Button id="emphasisButtonDisabled" text="Emphasis Disabled" variant="emphasis" isDisabled />
  </div>
);
export default EmphasisButton;
