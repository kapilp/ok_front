import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from '../../../Button';
const CompactButton = () => <Button id="compactButton" text="Compact" isCompact />;
export default CompactButton;
