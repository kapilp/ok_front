import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from '../../../Button';
const ProgrammaticDisabled = () => {
  const [isDisabled, setIsDisabled] = useState(false);
  return (
    <Button
      isDisabled={isDisabled}
      text={isDisabled ? 'Disabled' : 'Click to Disable'}
      id="programmaticDisabledButton"
      key="test-example-button"
      onClick={() => {
        setIsDisabled(!isDisabled);
      }}
    />
  );
};
export default ProgrammaticDisabled;
