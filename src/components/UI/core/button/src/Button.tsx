import classNames from 'classnames';
import * as KeyCode from 'keycode-js';
import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import classNamesBind from 'classnames/bind';
import styles from './Button.module.scss';

const cx = classNamesBind.bind(styles);

export enum ButtonVariants {
  NEUTRAL = 'neutral',
  EMPHASIS = 'emphasis',
  GHOST = 'ghost',
  // TODO= this should be removed on the next major version bump
  'DE-EMPHSASIS' = 'de-emphasis',
  'DE-EMPHASIS' = 'de-emphasis',
  ACTION = 'action',
  UTILITY = 'utility',
}

export enum ButtonTypes {
  BUTTON = 'button',
  SUBMIT = 'submit',
  RESET = 'reset',
}

export interface IButtonProps extends JSX.HTMLAttributes<HTMLButtonElement> {
  /**
   * Sets the href. When set will render the component as an anchor tag.
   */
  href?: string;
  /**
   * An optional icon. Nested inline with the text when provided.
   */
  icon?: Component;
  /**
   * Whether or not the button should only display as an icon.
   */
  isIconOnly?: boolean;
  /**
   * Whether or not the button should display as a block.
   */
  isBlock?: boolean;
  /**
   * Whether or not the button has reduced padding
   */
  isCompact?: boolean;
  /**
   * Whether or not the button should be disabled.
   */
  isDisabled?: boolean;
  /**
   * Reverses the position of the icon and text.
   */
  isReversed?: boolean;
  /**
   * Callback function triggered when mouse is pressed.
   * @param e
   */
  onMouseDown?: (e: MouseEvent) => void;
  /**
   * Callback function triggered when clicked.
   * @param e
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * Callback function triggered when button loses focus.
   * @param e
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when button gains focus.
   * @param e
   */
  onFocus?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when key is pressed.
   * @param e
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * Callback function triggered when key is released.
   * @param e
   */
  onKeyUp?: (e: KeyboardEvent) => void;
  /**
   * Callback ref to pass into the dom element.
   * @param el
   */
  ref?: (el: HTMLButtonElement) => void;
  /**
   * Sets the button text.
   * If the button is `isIconOnly` or variant `utility` this text is set as the aria-label and title for accessibility.
   */
  text: string;
  /**
   * Additional information to display as a native tooltip on hover.
   * Buttons declared as `isIconOnly` or `utility` will fallback to using `text` if not provided.
   */
  title?: string;
  /**
   * Sets the button type. One of `button`, `submit`, or `reset`.
   */
  type?: ButtonTypes;
  /**
   * Sets the button variant. One of `neutral`,  `emphasis`, `ghost`, `de-emphasis`, `action` or `utility`.
   */
  variant?: ButtonVariants;
}
type ButtonState = {
  active: boolean;
  focused: boolean;
};

export const Button: Component<IButtonProps> = props => {
  props = mergeProps(
    {},
    {
      isBlock: false,
      isCompact: false,
      isDisabled: false,
      isIconOnly: false,
      isReversed: false,
      ref: undefined,
      title: undefined,
      type: ButtonTypes.BUTTON,
      variant: ButtonVariants.NEUTRAL,
      text: '',
    },
    props,
  );
  const theme = useContext(ThemeContext);

  const [state, setState] = createStore({ active: false, focused: false } as ButtonState);

  let shouldShowFocus = true;

  const handleOnBlur = (event: FocusEvent) => {
    setState({ focused: false });

    if (props.onBlur) {
      props.onBlur(event);
    }
  };
  const handleClick = (event: MouseEvent) => {
    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Button#Clicking_and_focus
    // Button on Firefox, Safari and IE running on OS X does not receive focus when clicked.
    // This will put focus on the button when clicked if it is not currently the active element.
    if (document.activeElement !== event.currentTarget) {
      shouldShowFocus = false;
      (event.currentTarget as HTMLSpanElement).focus();
      shouldShowFocus = true;
    }

    if (props.onClick) {
      props.onClick(event);
    }
  };
  const handleKeyDown = (event: KeyboardEvent) => {
    // Add active state to FF browsers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: true });

      // Follow href on space keydown when rendered as an anchor tag
      if (props.href) {
        // Prevent window scrolling
        event.preventDefault();
        window.location.href = props.href;
      }
    }

    // Add focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
    }

    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  };

  const handleKeyUp = (event: KeyboardEvent) => {
    // Remove active state from FF browsers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: false });
    }

    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }

    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  };

  const handleFocus = (event: FocusEvent) => {
    if (shouldShowFocus) {
      setState({ focused: true });
    }

    if (props.onFocus) {
      props.onFocus(event);
    }
  };

  const handleMouseDown = (event: MouseEvent) => {
    if (props.onMouseDown) {
      props.onMouseDown(event);
    }

    // See https://developer.mozilla.org/en-US/docs/Web/API/HTMLOrForeignElement/focus#Notes
    // If you call HTMLElement.focus() from a mousedown event handler, you must call event.preventDefault() to keep the focus from leaving the HTMLElement.
    // Otherwise, when you click on the button again, focus would leave the button and onBlur would get called causing the document.activeElement would no longer be the focused button.
    event.preventDefault();
  };

  const [p, customProps] = splitProps(props, [
    'icon',
    'isBlock',
    'isCompact',
    'isDisabled',
    'isIconOnly',
    'isReversed',
    'text',
    'type',
    'variant',
    'href',
    'onClick',
    'onMouseDown',
    'onBlur',
    'onFocus',
    'onKeyDown',
    'onKeyUp',
    'ref',
    'title',
    'className',
    'children', // when passed children its causing error, very hard to debug
  ]);

  const buttonClasses = () =>
    classNames(
      cx([
        'button',
        p.variant,
        { 'is-disabled': p.isDisabled },
        { block: p.isBlock },
        { compact: p.isCompact },
        { 'is-active': state.active && !p.isDisabled },
        { 'is-focused': state.focused && !p.isDisabled },
        theme.className,
      ]),
      p.className,
    );

  const buttonLabelClasses = () =>
    cx([
      'button-label',
      { 'text-and-icon': p.icon && !p.isIconOnly && p.variant !== 'utility' },
      { 'icon-only': p.isIconOnly || p.variant === 'utility' },
      { 'text-only': !p.icon },
    ]);

  const buttonTextClasses = () => cx([{ 'text-first': props.icon && props.isReversed }]);

  const iconClasses = () => cx(['icon', { 'icon-first': !props.isIconOnly && props.variant !== 'utility' && !props.isReversed }]);

  const ButtonText = () => {
    return (
      <Show when={!props.isIconOnly && props.variant !== 'utility'}>
        <span className={buttonTextClasses()}>{props.text}</span>
      </Show>
    );
  };

  const ButtonIcon = () => {
    return (
      <Show when={props.icon}>
        <span className={iconClasses()}>
          <props.icon className={cx('icon-svg')} />
        </span>
      </Show>
    );
  };

  const buttonTitle = () => (p.isIconOnly || p.variant === 'utility' ? p.title || p.text : p.title);

  const ButtonLabel = () => (
    <span className={buttonLabelClasses()}>
      {p.isReversed ? <ButtonText /> : <ButtonIcon />}
      {p.isReversed ? <ButtonIcon /> : <ButtonText />}
    </span>
  );

  const ariaLabel = () => (p.isIconOnly || p.variant === 'utility' ? p['aria-label'] || p.text : p['aria-label']);

  // <Dynamic component={props.href ? 'a' : 'button'} gives a console warning so used <button> only

  return (
    <button
      // component={props.href ? 'a' : 'button'}
      {...customProps}
      className={buttonClasses()}
      type={p.type}
      disabled={p.isDisabled}
      tabIndex={p.isDisabled ? '-1' : undefined}
      aria-disabled={p.isDisabled}
      aria-label={ariaLabel()}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
      title={buttonTitle()}
      onClick={handleClick}
      onMouseDown={handleMouseDown}
      onFocus={handleFocus}
      // href={p.href}
      ref={p.ref}
    >
      <ButtonLabel />
    </button>
  );
};
