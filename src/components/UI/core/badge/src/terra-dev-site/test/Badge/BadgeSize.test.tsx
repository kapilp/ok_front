import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Badge from '../../../Badge';
const BadgeSize = () => (
  <div>
    <Badge size="tiny" text="Tiny" id="tiny-badge" /> <Badge size="small" text="Small" id="small-badge" /> <Badge size="medium" text="Medium" id="medium-badge" />{' '}
    <Badge size="large" text="Large" id="large-badge" /> <Badge size="huge" text="Huge" id="huge-badge" />
  </div>
);
export default BadgeSize;
