import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Badge from '../../../Badge';
const BadgeDefault = () => <Badge text="Default" id="default-badge" />;
export default BadgeDefault;
