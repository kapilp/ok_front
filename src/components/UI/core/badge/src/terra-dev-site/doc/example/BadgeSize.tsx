import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Badge } from '../../../Badge';

export const BadgeSize = () => (
  <div>
    <Badge size="tiny" text="Tiny" /> <Badge size="small" text="Small" /> <Badge size="medium" text="Medium" /> <Badge size="large" text="Large" />{' '}
    <Badge size="huge" text="Huge" />
  </div>
);
