import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Badge } from '../../../Badge';
import { SvgIconBookmark } from '../../../../../icon/src/icon/IconBookmark';

const Icon = () => <SvgIconBookmark />;
export const BadgeIcon = () => (
  <div>
    <Badge icon={Icon} text="icon" /> <Badge icon={Icon} text="icon" isReversed /> <Badge icon={Icon} />
  </div>
);
