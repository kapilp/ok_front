import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Badge } from '../../../Badge';

export const BadgeIntent = () => (
  <div>
    <Badge text="Default" /> <Badge intent="primary" text="Primary" /> <Badge intent="secondary" text="Secondary" /> <Badge intent="positive" text="Positive" />{' '}
    <Badge intent="negative" text="Negative" /> <Badge intent="warning" text="Warning" /> <Badge intent="info" text="Info" />
  </div>
);
