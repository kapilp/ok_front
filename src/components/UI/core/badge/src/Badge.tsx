import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { Dynamic } from 'solid-js/web';

import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Badge.module.scss';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';

const cx = classNamesBind.bind(styles);

enum BadgeIntent {
  DEFAULT = 'default',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  INFO = 'info',
  WARNING = 'warning',
  POSITIVE = 'positive',
  NEGATIVE = 'negative',
}
enum BadgeSize {
  TINY = 'tiny',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  HUGE = 'huge',
}
interface Properties {
  /**
   * Child Nodes.
   */
  children: JSX.Element;
  /**
   * An optional icon. Nested inline with the text when provided.
   */
  icon: JSX.Element;
  /**
   * Sets the badge color scheme. One of `default`, `primary`, `secondary`, `positive`, `negative`, `warning`, `info`.
   */
  intent: BadgeIntent;

  /**
   * Reverses the position of the icon and text.
   */
  isReversed: boolean;
  /**
   * Sets the badge size. One of `tiny`, `small`, `medium`, `large`, `huge`.
   */
  size: BadgeSize;
  /**
   * Sets the badge text.
   */
  text: string;
  /**
   * Text that describes the badge to a screen reader. Use this
   * for creating an accessible badge.
   */
  visuallyHiddenText: string;
}
const defaultProps = {
  children: null,
  icon: null,
  intent: 'default',
  isReversed: false,
  size: 'small',
  text: null,
  visuallyHiddenText: undefined,
};
export const Badge = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [p, customProps] = splitProps(props, ['size', 'intent', 'isReversed', 'text', 'icon', 'visuallyHiddenText', 'className']);
  const badgeClassNames = () => classNames(cx('badge', { 'has-icon': props.icon }, { 'is-reversed': props.isReversed }, props.size, props.intent, theme.className), p.className);

  const textContent = () =>
    props.text ? (
      <span className="text" aria-hidden="true">
        {props.text}
      </span>
    ) : null;

  const visuallyHiddenTextContent = () => (p.visuallyHiddenText ? <VisuallyHiddenText text={props.visuallyHiddenText} /> : null);
  const content = props.isReversed ? [visuallyHiddenTextContent(), textContent(), props.icon, p.children] : [props.icon, visuallyHiddenTextContent(), textContent(), p.children];

  return (
    <Dynamic component="span" {...customProps} className={badgeClassNames()}>
      {content}
    </Dynamic>
  );
};
export { BadgeIntent, BadgeSize };
