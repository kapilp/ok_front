import { JSX,  createMemo, mergeProps, splitProps, useContext  } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { SpacerClassMappings, SpacerSizes } from './_spacerConstants';
import { mapShorthandToObject } from './_spacerShorthandUtils';

import styles from './Spacer.module.scss';

const cx = classNamesBind.bind(styles);
/*
  NOTE: this is needed in order to ensure that the types `marginTop`, `marginRight`, `paddingTop`, `paddingRight`, etc. are recognized by terra-props-table
  as defined. Currently, simply putting `Object.values(SpacerSizes)` causes all other `marginX` and `paddingX` props to be recognized as `undefined`
  in the PropsTable.
*/
type arrayOfSpacerSizes = 'none' | 'small-2' | 'small-1' | 'small' | 'medium' | 'large' | 'large+1' | 'large+2' | 'large+3' | 'large+4';

interface SpacerProperties extends JSX.HTMLAttributes<Element> {
  /**
   * Child Nodes.
   */
  children: JSX.Element;
  /**
   * Sets margin in a syntax similar to the standard CSS spec f. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  margin?: shorthandValidator;
  /**
   * Sets top margin. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  marginTop?: arrayOfSpacerSizes;
  /**
   * Sets bottom margin. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  marginBottom?: arrayOfSpacerSizes;
  /**
   * Sets left margin One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  marginLeft?: arrayOfSpacerSizes;
  /**
   * Sets right margin. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  marginRight?: arrayOfSpacerSizes;
  /**
   * Sets padding in a syntax similar to the standard CSS spec https://developer.mozilla.org/en-US/docs/Web/CSS/padding. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  padding?: shorthandValidator;
  /**
   * Sets top padding. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  paddingTop?: arrayOfSpacerSizes;
  /**
   * Sets bottom padding. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  paddingBottom?: arrayOfSpacerSizes;
  /**
   * Sets left padding. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  paddingLeft?: arrayOfSpacerSizes;
  /**
   * Sets right padding. One of 'none', 'small-2', 'small-1', 'small', 'medium', 'large', 'large+1', 'large+2', 'large+3', 'large+4'.
   */
  paddingRight?: arrayOfSpacerSizes;
  /**
   * Sets the display to be inline-block.
   */
  isInlineBlock?: boolean;
}
const defaultProps = {
  margin: undefined,
  marginTop: SpacerSizes.NONE,
  marginBottom: SpacerSizes.NONE,
  marginRight: SpacerSizes.NONE,
  marginLeft: SpacerSizes.NONE,
  padding: undefined,
  paddingTop: SpacerSizes.NONE,
  paddingBottom: SpacerSizes.NONE,
  paddingLeft: SpacerSizes.NONE,
  paddingRight: SpacerSizes.NONE,
  isInlineBlock: false,
};
export const Spacer = (props: SpacerProperties) => {
  props = mergeProps({}, defaultProps, props);

  const theme = useContext(ThemeContext);
  const marginShorthand = createMemo(() => (props.margin ? mapShorthandToObject('margin', props.margin) : {}));
  const paddingShorthand = createMemo(() => (props.padding ? mapShorthandToObject('padding', props.padding) : {}));
  const marginAttributes = createMemo(() => {
    return {
      marginTop: props.marginTop,
      marginRight: props.marginRight,
      marginBottom: props.marginBottom,
      marginLeft: props.marginLeft,
      // ...marginShorthand(),
    };
  });
  const paddingAttributes = createMemo(() => {
    return {
      paddingTop: props.paddingTop,
      paddingRight: props.paddingRight,
      paddingBottom: props.paddingBottom,
      paddingLeft: props.paddingLeft,
      // ...paddingShorthand(),
    };
  });

  const [p, customProps] = splitProps(props, [
    'margin',
    'marginTop',
    'marginBottom',
    'marginLeft',
    'marginRight',
    'padding',
    'paddingTop',
    'paddingBottom',
    'paddingLeft',
    'paddingRight',
    'isInlineBlock',
    'children',
    'className',
  ]);

  return (
    <div
      {...customProps}
      className={classNames(
        cx(
          `margin-top-${SpacerClassMappings[marginAttributes().marginTop]}`,
          `margin-bottom-${SpacerClassMappings[marginAttributes().marginBottom]}`,
          `ml-${SpacerClassMappings[marginAttributes().marginLeft]}`,
          `mr-${SpacerClassMappings[marginAttributes().marginRight]}`,
          `padding-top-${SpacerClassMappings[paddingAttributes().paddingTop]}`,
          `padding-bottom-${SpacerClassMappings[paddingAttributes().paddingBottom]}`,
          `pl-${SpacerClassMappings[paddingAttributes().paddingLeft]}`,
          `pr-${SpacerClassMappings[paddingAttributes().paddingRight]}`,
          { 'is-inline-block': p.isInlineBlock },
          theme.className,
        ),
        p.className,
      )}
    >
      {p.children}
    </div>
  );
};
const opts = {
  Sizes: SpacerSizes,
};

Spacer.Opts = opts;
