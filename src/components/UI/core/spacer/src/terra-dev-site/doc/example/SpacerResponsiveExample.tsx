import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import placeholderPic150x150 from './150x150.jpg';
import placeholderPic1000x200 from './1000x200.png';
import { Spacer } from '../../../Spacer';
import { Image } from '../../../../../image/src/Image';

export const SpacerResponsiveExample = () => (
  <div>
    <Image src={placeholderPic150x150} alt={'placeholderPic1000x200'} />
    <Spacer marginTop="small-1" bps={{ 768: { marginTop: 'large+4' } }}>
      <Image src={placeholderPic150x150} alt={'placeholderPic1000x200'} />
    </Spacer>
  </div>
);
