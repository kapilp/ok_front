import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import styles from './Spacer.module.scss';
import { Spacer } from '../../../Spacer';
import { Button } from '../../../../../button/src/Button';
export const SpacerShortVsLongHandExample = () => (
  <div>
    <Spacer className={styles.spacerdemoprimary} padding="large small" margin="medium large+1" isInlineBlock>
      <Button text="Shorthand" />
    </Spacer>
    <Spacer
      className={styles.spacerdemodefault}
      paddingTop="large"
      paddingBottom="large"
      paddingLeft="small"
      paddingRight="small"
      marginTop="medium"
      marginBottom="medium"
      marginLeft="large+1"
      marginRight="large+1"
      isInlineBlock
    >
      <Button text="Longhand" />
    </Spacer>
  </div>
);
