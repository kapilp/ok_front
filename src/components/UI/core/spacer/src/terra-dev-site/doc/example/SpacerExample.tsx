import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Spacer } from '../../../Spacer';
import { Button } from '../../../../../button/src/Button';
import styles from './Spacer.module.scss';
export const SpacerExample = () => (
  <div>
    <Spacer className={styles.spacerdemodefault} paddingTop="large+4" paddingBottom="large+4" paddingLeft="large+4" paddingRight="large+4" isInlineBlock>
      <Button text="Default" />
    </Spacer>
    <Spacer className={styles.spacerdemoprimary} paddingTop="large+2" paddingBottom="large+2" paddingLeft="large+2" paddingRight="large+2" isInlineBlock>
      <Button text="Emphasis" variant="emphasis" />
    </Spacer>
  </div>
);
