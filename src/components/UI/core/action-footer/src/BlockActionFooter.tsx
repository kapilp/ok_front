import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './BlockActionFooter.module.scss';

const cx = classNamesBind.bind(styles);

interface Properties extends JSX.HTMLAttributes<Element> {
  children: JSX.Element;
}

const defaultProps = {
  children: undefined,
};
export const BlockActionFooter = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [e, customProps] = splitProps(props, ['children', 'className']);
  const isEmpty = false; // () => !Array.isArray(props.children); // not work on link properly so i set false
  const blockActionFooterClassNames = () => classNames(cx(['block-action-footer', { 'with-actions': !isEmpty }, theme.className]), e.className);
  return (
    <div {...customProps} className={blockActionFooterClassNames()}>
      {e.children}
    </div>
  );
};
