import classNames from 'classnames/bind';
import { JSX, mergeProps, splitProps } from 'solid-js';
import { BlockActionFooter } from './BlockActionFooter';
import styles from './ActionFooter.module.scss';
const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Actions to be displayed in the start socket
   */
  start: JSX.Element;
  /**
   * Actions to be displayed in the end socket
   */
  end: JSX.Element;
}
const defaultProps = {
  start: undefined,
  end: undefined,
};
export const ActionFooter = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [e, customProps] = splitProps(props, ['start', 'end', 'className']);

  const actionFooterClassnames = () => cx([{ 'action-footer': e.start }, { 'action-footer-end': !e.start }, e.className]);

  return (
    <BlockActionFooter {...customProps} className={actionFooterClassnames()}>
      {e.start && <div className={cx('start-actions')}>{e.start}</div>}
      {e.end && <div className={cx('end-actions')}>{e.end}</div>}
    </BlockActionFooter>
  );
};
