import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Button, ButtonVariants } from '../../../../../../button/src/Button';
import { Hyperlink } from '../../../../../../hyperlink/src/Hyperlink';
import { Spacer } from '../../../../../../spacer/src/Spacer';
import { ActionFooter } from '../../../../ActionFooter';
import { ExampleTemplate } from '../../common/ExampleTemplate';
export const ActionFooterStandardMultipleStartEndActionsExample = () => (
  <ExampleTemplate>
    <ActionFooter
      start={<Hyperlink href="#">Start Action</Hyperlink>}
      end={
        <>
          <Spacer isInlineBlock marginRight="medium">
            <Button text="Submit" variant={ButtonVariants.EMPHASIS} />
          </Spacer>
          <Button text="Cancel" />
        </>
      }
    />
  </ExampleTemplate>
);
