import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { Spacer } from '../../../../../../spacer/src/Spacer';
import { BlockActionFooter } from '../../../../BlockActionFooter';
import { Button } from '../../../../../../button/src/Button';
export const ActionFooterBlockMultipleActionsExample = () => (
  <ExampleTemplate>
    <BlockActionFooter>
      <Spacer paddingBottom="medium">
        <Button isBlock text="First Action" />
      </Spacer>
      <Button isBlock text="Second Action" />
    </BlockActionFooter>
  </ExampleTemplate>
);
