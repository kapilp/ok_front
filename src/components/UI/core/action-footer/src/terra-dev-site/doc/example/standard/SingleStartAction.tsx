import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { ActionFooter } from '../../../../ActionFooter';
import { Hyperlink } from '../../../../../../hyperlink/src/Hyperlink';
export const ActionFooterStandardSingleStartActionExample = () => (
  <ExampleTemplate>
    <ActionFooter start={<Hyperlink href="#">Start Action</Hyperlink>} />
  </ExampleTemplate>
);
