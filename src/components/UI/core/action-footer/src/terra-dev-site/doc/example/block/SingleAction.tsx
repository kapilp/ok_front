import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { BlockActionFooter } from '../../../../BlockActionFooter';
export const ActionFooterBlockSingleActionExample = () => (
  <ExampleTemplate>
    <BlockActionFooter>
      <a href="/#/site/action-footer/block">A link</a>
    </BlockActionFooter>
  </ExampleTemplate>
);
