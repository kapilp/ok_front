import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { CenteredActionFooter } from '../../../../CenteredActionFooter';
import { Spacer } from '../../../../../../spacer/src/Spacer';
import { Button } from '../../../../../../button/src/Button';
export const ActionFooterCenteredMultipleActionsExample = () => (
  <ExampleTemplate>
    <CenteredActionFooter
      center={
        <>
          <Spacer isInlineBlock marginRight="medium">
            <Button text="Action One" />
          </Spacer>
          <Button text="Action Two" />
        </>
      }
    />
  </ExampleTemplate>
);
