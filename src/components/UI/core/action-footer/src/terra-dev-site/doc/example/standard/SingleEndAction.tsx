import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { ActionFooter } from '../../../../ActionFooter';
import { Button } from '../../../../../../button/src/Button';
export const ActionFooterStandardSingleEndActionExample = () => (
  <ExampleTemplate>
    <ActionFooter end={<Button text="End Action" />} />
  </ExampleTemplate>
);
