import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { ExampleTemplate } from '../../common/ExampleTemplate';
import { CenteredActionFooter } from '../../../../CenteredActionFooter';
import { Hyperlink } from '../../../../../../hyperlink/src/Hyperlink';
export const ActionFooterCenteredSingleActionExample = () => (
  <ExampleTemplate>
    <CenteredActionFooter center={<Hyperlink href="#">A link</Hyperlink>} />
  </ExampleTemplate>
);
