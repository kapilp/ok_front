import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './ExampleTemplate.scss';
const cx = classNames.bind(styles);
interface Properties {
  children: JSX.Element;
}
export const ExampleTemplate = (props: Properties) => <div className={cx('content-wrapper')}>{props.children}</div>;
