import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import ActionFooter from '../../../ActionFooter';
export default () => <ActionFooter start={<Button text="Start Action" />} end="FakeLink End Action" />;
