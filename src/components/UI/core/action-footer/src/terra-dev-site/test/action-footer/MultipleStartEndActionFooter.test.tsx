import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import ActionFooter from '../../../ActionFooter';
export default () => (
  <ActionFooter
    start={
      <div>
        Start Text
        <Button text="Start Action" />
      </div>
    }
    end={
      <div>
        End Text
        <Button text="End Action" />
      </div>
    }
  />
);
