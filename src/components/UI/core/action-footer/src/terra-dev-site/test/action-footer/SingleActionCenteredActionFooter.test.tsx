import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import CenteredActionFooter from '../../../CenteredActionFooter';
export default () => <CenteredActionFooter center={<Button text="Action" />} />;
