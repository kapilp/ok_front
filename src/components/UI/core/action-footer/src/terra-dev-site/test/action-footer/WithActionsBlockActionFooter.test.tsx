import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import BlockActionFooter from '../../../BlockActionFooter';
export default () => (
  <BlockActionFooter>
    FakeLink Action
    <Button text="Button Action" />
  </BlockActionFooter>
);
