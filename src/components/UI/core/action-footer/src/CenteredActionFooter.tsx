import { JSX, mergeProps, splitProps } from 'solid-js';
import classNames from 'classnames/bind';
import { BlockActionFooter } from './BlockActionFooter';
import styles from './CenteredActionFooter.module.scss';
const cx = classNames.bind(styles);

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Actions to be displayed in the center socket
   */
  center: JSX.Element;
}
const defaultProps = {
  center: undefined,
};
export const CenteredActionFooter = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [e, customProps] = splitProps(props, ['center', 'className']);
  const centeredActionFooterClassNames = () => cx(['centered-action-footer', e.className]);
  return (
    <BlockActionFooter {...customProps} className={centeredActionFooterClassNames()}>
      {e.center}
    </BlockActionFooter>
  );
};
