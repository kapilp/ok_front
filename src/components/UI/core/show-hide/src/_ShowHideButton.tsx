/* eslint-disable react/button-has-type */
import * as KeyCode from 'keycode-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './_ShowHideButton.module.scss';
const cx = classNamesBind.bind(styles);
import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';

interface IButtonProps extends JSX.HTMLAttributes<Element> {
  /**
   * Callback function triggered when clicked.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * Callback function triggered when button loses focus.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when button gains focus.
   */
  onFocus?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when key is pressed.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * Callback function triggered when key is released.
   */
  onKeyUp?: (e: KeyboardEvent) => void;
  /**
   * Callback ref to pass into the dom element.
   */
  ref?: (el: HTMLButtonElement) => void;
  /**
   * Sets the button text.
   */
  text: string;
}
const defaultProps = {
  ref: undefined,
};
type ButtonState = {
  active: boolean;
  focused: boolean;
};
export const Button = (props: IButtonProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  //context: any;
  const [state, setState] = createStore({ active: false, focused: false } as ButtonState);

  const handleOnBlur = (event: FocusEvent) => {
    setState({ focused: false });
    if (props.onBlur) {
      props.onBlur(event);
    }
  };
  const handleKeyDown = (event: KeyboardEvent) => {
    // Add active state to FF browser
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: true });
      setState({ focused: true });
    } else if (event.keyCode === KeyCode.KEY_RETURN) {
      // Add focus styles for keyboard navigation
      setState({ focused: true });
    }
    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  };
  const handleKeyUp = (event: KeyboardEvent) => {
    // Remove active state from FF broswers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: false });
    }
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }
    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  };
  const [extracted, customProps] = splitProps(props, ['text', 'onClick', 'onBlur', 'onFocus', 'onKeyDown', 'onKeyUp', 'ref']);
  //const theme = context;

  return (
    <button
      {...customProps}
      className={classNames(cx(['button', { 'is-active': state.active }, { 'is-focused': state.focused }, theme.className]), customProps.className)}
      type="button"
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
      onClick={props.onClick}
      onFocus={props.onFocus}
      ref={props.ref}
    >
      <span className={cx('inner')}>
        <span className={cx('text')}>{props.text}</span>
        <span className={cx('icon-holder')}>
          <span className={cx('icon')} />
        </span>
      </span>
    </button>
  );
};
