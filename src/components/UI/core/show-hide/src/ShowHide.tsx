import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
// import { injectIntl, intlShape } from 'react-intl'; // Todo fix this

import { Button } from './_ShowHideButton';
import classNames from 'classnames/bind';
import styles from './ShowHide.module.scss';
import { Toggle } from '../../toggle/src/Toggle';
const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Content in the body of the component that will be shown or hidden.
   */
  children: JSX.Element;
  /**
   * The intl object to be injected for translations. Provided by the injectIntl function.
   */
  // intl: intlShape; // todo fix this
  /**
   * Callback function triggered when the component is expanded or collapsed.
   */
  onChange: (e: Event) => void;
  /**
   * Applies style for which side of the container the button aligns to. Available options are start, center and end
   */
  buttonAlign?: string;
  /**
   * Button text that will be displayed.
   */
  buttonText?: string;
  /**
   * Allows parent to toggle the component. True for open and false for close.
   */
  isOpen?: boolean;
  /**
   * Elements(s) that will be visible to the user when component is collapsed
   */
  preview?: JSX.Element;
}
const defaultProps = {
  buttonAlign: 'start',
  isOpen: false,
  preview: undefined,
};
export const ShowHide = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  /*let intlButtonText = '';
  if (!props.buttonText) {
    if (props.isOpen) {
      intlButtonText = intl.formatMessage({ id: 'Terra.showhide.hide' });
    } else {
      intlButtonText = intl.formatMessage({ id: 'Terra.showhide.showmore' });
    }
  }*/
  const [extracted, customProps] = splitProps(props, ['buttonAlign', 'buttonText', 'children', 'onChange', 'preview', 'intl', 'isOpen']);
  return (
    <div {...customProps}>
      {!props.isOpen && props.preview}
      <Toggle isOpen={props.isOpen}>{props.children}</Toggle>
      <div className={cx('show-hide')}>
        <Button
          aria-expanded={props.isOpen}
          text={props.buttonText || (props.isOpen ? 'Hide' : 'Show More')}
          onClick={props.onChange}
          className={cx(['show-hide', 'button', props.buttonAlign, customProps.className])}
        />
      </div>
    </div>
  );
};

// export default injectIntl(ShowHide);
