import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ShowHide } from '../../../ShowHide';

const sentences = [
  <p key="lorem1">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem2">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem3">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem4">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem5">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem6">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem7">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem8">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem9">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
  <p key="lorem10">Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>,
];
type CustomButtonTextShowHideState = {
  isOpen?: boolean;
};
export const CustomButtonTextShowHide = (props: {}) => {
  const [state, setState] = createStore({ isOpen: false });

  const toggleShowHide = () => {
    setState({
      isOpen: !state.isOpen,
    });
  };

  return (
    <ShowHide
      preview={[sentences[0], sentences[1], sentences[2]]}
      onChange={toggleShowHide}
      isOpen={state.isOpen}
      buttonText={state.isOpen ? `Hide ${sentences.length - 3} Sentences` : `Show ${sentences.length - 3} More Sentences`}
    >
      {sentences}
    </ShowHide>
  );
};
