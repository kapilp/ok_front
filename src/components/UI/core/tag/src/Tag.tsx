import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import * as KeyCode from 'keycode-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import styles from './Tag.module.scss';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);
// import ThemeContext from 'terra-theme-context';

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Sets the href. When set will render the component as an anchor tag.
   */
  href?: string;
  /**
   * An optional icon.
   */
  icon?: JSX.Element;
  /**
   * Callback function triggered when clicked.
   */
  onClick?: (e?: MouseEvent) => void;
  /**
   * Callback function triggered when tag loses focus.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when tag gains focus.
   */
  onFocus?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when key is released.
   */
  onKeyUp?: (e: KeyboardEvent) => void;
  /**
   * Sets the tag text.
   */
  text: string;
}

type TagState = {
  focused: boolean;
};
export const Tag = (props: Properties) => {
  const theme = useContext(ThemeContext);
  //context: any;
  const [state, setState] = createStore({ focused: false } as TagState);

  const handleOnBlur = (event: FocusEvent) => {
    setState({ focused: false });
    if (props.onBlur) {
      props.onBlur(event);
    }
  };
  const handleKeyUp = (event: KeyboardEvent) => {
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }
    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  };

  //const theme = context;

  const tagIcon = <>{props.icon ? <span className={cx('icon')}>{props.icon}</span> : null}</>;
  const isClickable = props.href || props.onClick;
  const clickableComponent = props.href ? 'a' : 'button';
  const ComponentType = isClickable ? clickableComponent : 'span';
  const [extracted, customProps] = splitProps(props, ['icon', 'text', 'href', 'onClick', 'onBlur', 'onFocus', 'onKeyUp']);
  return (
    <Dynamic
      component={ComponentType}
      {...customProps}
      className={classNames(cx('tag', { 'is-focused': state.focused }, { 'is-interactive': props.href || props.onClick }, theme.className), customProps.className)}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
      onClick={props.onClick}
      onFocus={props.onFocus}
      href={props.href}
    >
      {tagIcon}
      {props.text}
    </Dynamic>
  );
};
