import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SvgIconTag } from '../../../../../icon/src/icon/IconTag';
import { Tag } from '../../../Tag';
export const TagFallbacks = () => (
  <div>
    <Tag text="No OnClick/HREF Tag" />
    <Tag icon={<SvgIconTag />} text="Icon & Text - No OnClick/HREF Tag" />
  </div>
);
