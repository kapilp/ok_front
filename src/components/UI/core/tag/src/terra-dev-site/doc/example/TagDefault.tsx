import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { Tag } from '../../../Tag';
import { SvgIconTag } from '../../../../../icon/src/icon/IconTag';
export const TagDefault = () => (
  <div>
    <Tag text="Default OnClick Tag" onClick={() => window.alert('Tag has been clicked!')} />
    <Tag icon={<SvgIconTag />} onClick={() => window.alert('Tag has been clicked!')} text="Icon & Text OnClick Tag" />
  </div>
);
