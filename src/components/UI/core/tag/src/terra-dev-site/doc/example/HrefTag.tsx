import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SvgIconTag } from '../../../../../icon/src/icon/IconTag';
import { Tag } from '../../../Tag';

export const TagHref = () => (
  <div>
    <Tag href="http://google.com" text="HREF Tag" />
    <Tag icon={<SvgIconTag />} href="http://google.com" text="Icon & Text HREF Tag" />
  </div>
);
