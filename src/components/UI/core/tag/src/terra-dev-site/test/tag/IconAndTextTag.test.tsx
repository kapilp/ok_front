import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import IconBookmark from 'terra-icon/lib/icon/IconTile';
import Tag from '../../../Tag';
const icon = <IconBookmark />;
const IconTag = () => <Tag icon={icon} text="Icon Tag" id="iconTag" />;
export default IconTag;
