import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Tag from '../../../Tag';
const HrefTag = () => <Tag id="href" text="Href Tag" href="www.google.com" />;
export default HrefTag;
