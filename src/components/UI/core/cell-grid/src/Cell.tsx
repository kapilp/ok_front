import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { styleFromWidth } from './_CellGridUtils';
import classNames from 'classnames/bind';
import styles from './Cell.module.scss';
const cx = classNames.bind(styles);

interface Properties {
  /**
   * Content to be displayed for the cell.
   */
  children: JSX.Element;
  /**
   * Function callback to get the ref of the cell.
   */
  ref: (el: HTMLElement) => void;
  /**
   * Composed width for the cell. Can be provided as either a static, percentage, or scalar value.
   */
  width: {
    static: {
      value: number;
      unit: string;
    };
    percentage?: number;
    scalar?: number;
  };
}

export const Cell = (props: Properties) => {
  const [extracted, customProps] = splitProps(props, ['children', 'ref', 'width']);
  return (
    <div
      {...customProps}
      style={styleFromWidth(props.width)} // eslint-disable-line react/forbid-dom-props
      className={cx(['cell'], customProps.className)}
      ref={props.ref}
    >
      {props.children}
    </div>
  );
};
