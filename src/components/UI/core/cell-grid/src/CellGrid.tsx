import { JSX, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './CellGrid.module.scss';
const cx = classNamesBind.bind(styles);

interface Properties {
  /**
   * The child Cells to passed to the cell grid.
   */
  children: JSX.Element;
  /**
   * Indicates the desired divider styles to apply to a cell grid and its children.
   * One of `'vertical'`, `'horizontal'`, `'both'`,
   */
  dividerStyle?: 'vertical' | 'horizontal' | 'both';
  /**
   * Function callback for the ref of the cell grid.
   */
  ref?: (el: HTMLDivElement) => void;
}
export const CellGrid = (props: Properties) => {
  const theme = useContext(ThemeContext);
  const divider = props.dividerStyle ? `divider-${props.dividerStyle}` : undefined;

  const [extracted, customProps] = splitProps(props, ['children', 'dividerStyle', 'ref']);
  return (
    <div {...customProps} className={classNames(cx('cell-grid', divider, theme.className), customProps.className)} ref={props.ref}>
      {props.children}
    </div>
  );
};
