import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { CellGrid } from '../../../CellGrid';
import { Cell } from '../../../Cell';
export const CellGridHorizontal = () => (
  <CellGrid dividerStyle="horizontal">
    <Cell key="cell-0" width={{ static: { value: 80, unit: 'px' } }}>
      80px
    </Cell>
    <Cell key="cell-1" width={{ percentage: 10 }}>
      10%
    </Cell>
    <Cell key="cell-2">Scalar 1</Cell>
    <Cell key="cell-3" width={{ scalar: 3 }}>
      Scalar 3
    </Cell>
  </CellGrid>
);
