// import ResizeObserver from 'resize-observer-polyfill';

import {  JSX, createSignal, onCleanup, mergeProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { activeBreakpointForSize } from '../../breakpoints/src/breakpoints';
// import { breakpoints } from '../../breakpoints/src/breakpoints';
// import { ActiveBreakpointContext } from '../../breakpoints/src/ActiveBreakpointContext';
export const DependentViewport = {
  WINDOW: 'window',
  PARENT: 'parent',
};
interface IResponsiveElementProps extends JSX.HTMLAttributes<Element> {
  /**
   * Child nodes.
   */
  children: JSX.Element;
  /**
   * A callback function invoked when the breakpoint size changes. ```onChange(<string>breakpoint)```
   */
  onChange: (e: Event) => void;
  /**
   * A callback function invoked when the responsive target resizes. ```onResize(<number>width)```
   */
  onResize: (width: number) => void;
  /**
   * The viewport the element will be responsive to. One of `window` or `parent`.
   */
  responsiveTo: 'window' | 'parent';
}
const defaultProps = {
  responsiveTo: 'parent',
};

type ResponsiveElementState = {
  element?: null;
};
export const ResponsiveElement = (props: IResponsiveElementProps) => {
  props = mergeProps({}, defaultProps, props);

  let breakpoint: any;
  const [parentContainer, setContainer_] = createSignal<HTMLDivElement>();
  let resizeObserver: any;

  let animationFrameID: number;
  const onMount = () => {
    if (parentContainer()) {
      handleResize(parentContainer().getBoundingClientRect().width);
      resizeObserver = new ResizeObserver(entries => {
        animationFrameID = window.requestAnimationFrame(() => {
          animationFrameID = null;
          handleResize(entries[0].target.getBoundingClientRect().width);
        });
      });
      resizeObserver.observe(parentContainer());
    } else {
      handleResize(window.innerWidth);
      window.addEventListener('resize', handleWindowResize);
    }
  };
  if (props.responsiveTo != 'parent') {
    onMount();
  }
  function setParentContainer(el: HTMLDivElement) {
    if (props.responsiveTo === 'parent') {
      setTimeout(() => {
        setContainer_(el.parentNode);
        onMount();
      });
    }
  }

  onCleanup(() => {
    if (parentContainer()) {
      window.cancelAnimationFrame(animationFrameID);
      resizeObserver.disconnect(parentContainer());
      setContainer_(null);
    } else {
      window.removeEventListener('resize', handleWindowResize);
    }
  });

  function handleResize(width: number) {
    if (props.onResize) {
      props.onResize(width);
    }
    const activeBreakpoint = activeBreakpointForSize(width);
    if (props.onChange && activeBreakpoint !== breakpoint) {
      props.onChange(activeBreakpoint);
    }
    breakpoint = activeBreakpoint;
  }
  function handleWindowResize() {
    handleResize(window.innerWidth);
  }

  return (
    <>
      {props.responsiveTo === 'parent' && <div ref={setParentContainer} />}
      {props.children}
    </>
  );
};
