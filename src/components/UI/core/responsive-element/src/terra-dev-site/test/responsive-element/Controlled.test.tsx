/**
 * This test file is intended to test that a controlled implementation of the responsive element
 * renders the correct component at every breakpoint.
 */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ResponsiveElement from 'terra-responsive-element';
type ControlledState = {
  breakpoint?: undefined;
};
class Controlled extends React.Component<{}, ControlledState> {
  constructor() {
    super();
    state = { breakpoint: undefined };
    handleOnChange = handleOnChange.bind(this);
  }
  handleOnChange(breakpoint) {
    setState({ breakpoint });
  }
  render() {
    const { breakpoint } = state;
    let children = null;
    if (breakpoint === 'tiny') {
      children = <div>Tiny</div>;
    } else if (breakpoint === 'small') {
      children = <div>Small</div>;
    } else if (breakpoint === 'medium') {
      children = <div>Medium</div>;
    } else if (breakpoint === 'large') {
      children = <div>Large</div>;
    } else if (breakpoint === 'huge') {
      children = <div>Huge</div>;
    } else if (breakpoint === 'enormous') {
      children = <div>Enormous</div>;
    }
    return <ResponsiveElement onChange={handleOnChange}>{children}</ResponsiveElement>;
  }
}
export default Controlled;
