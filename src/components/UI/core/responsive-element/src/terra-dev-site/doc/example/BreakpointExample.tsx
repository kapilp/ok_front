import { ResponsiveElement } from '../../../ResponsiveElement';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';

export const BreakpointExample = () => {
  const [state, setState] = createStore({ breakpoint: '' });
  return (
    <ResponsiveElement onChange={value => setState({ breakpoint: value })}>
      <Placeholder title={state.breakpoint} />
    </ResponsiveElement>
  );
};
