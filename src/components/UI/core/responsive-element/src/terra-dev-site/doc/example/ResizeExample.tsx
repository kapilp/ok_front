import { ResponsiveElement } from '../../../ResponsiveElement';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';

export const ResizeExample = () => {
  const [state, setState] = createStore({ width: 0 });
  return (
    <ResponsiveElement onResize={value => setState({ width: value })}>
      <Placeholder title={state.width} />
    </ResponsiveElement>
  );
};
