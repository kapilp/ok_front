import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ResponsiveElement } from '../../../ResponsiveElement';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';

export const ResizeUncontrolledExample = () => (
  <ResponsiveElement
    tiny={<Placeholder title="tiny" />}
    small={<Placeholder title="small" />}
    medium={<Placeholder title="medium" />}
    large={<Placeholder title="large" />}
    huge={<Placeholder title="huge" />}
    enormous={<Placeholder title="enormous" />}
  />
);
