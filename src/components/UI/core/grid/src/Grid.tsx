import { JSX, splitProps } from 'solid-js';
import { GridRow } from './GridRow';
import { GridColumn } from './GridColumn';

interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The component that will be displayed at row level.
   */
  children: JSX.Element;
}
// export const Grid = (props: Properties) => <div {...props} />  // not work for ssr
export const Grid = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['children']);
  return <div {...customProps}>{p.children}</div>;
};
Grid.Row = GridRow;
Grid.Column = GridColumn;
