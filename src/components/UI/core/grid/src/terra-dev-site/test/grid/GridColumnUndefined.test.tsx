import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
import './GridExample.module.scss';
const GridColumnUndefined = () => (
  <Grid id="grid">
    <GridRow id="row">
      <GridColumn id="column-1">
        <div className="terra-grid-example">Column content</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridColumnUndefined;
