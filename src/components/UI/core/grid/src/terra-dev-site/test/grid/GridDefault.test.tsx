import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
import './GridExample.module.scss';
const GridDefault = () => (
  <Grid id="grid">
    <GridRow id="row">
      <GridColumn id="column-1" tiny={6}>
        <div className="terra-grid-example">First Column content</div>
      </GridColumn>
      <GridColumn id="column-2" tiny={6}>
        <div className="terra-grid-example">Second Column content</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridDefault;
