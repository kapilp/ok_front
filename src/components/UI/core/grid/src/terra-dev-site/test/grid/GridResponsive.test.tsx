import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
const GridResponsive = () => (
  <Grid id="grid">
    <GridRow id="row">
      <GridColumn id="column-1" tiny={12} small={4} medium={8} large={6}>
        <div>First Column content</div>
      </GridColumn>
      <GridColumn id="column-2" tiny={12} small={8} medium={4} large={6}>
        <div>Second Column content</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridResponsive;
