import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
import './GridExample.module.scss';
const GridNested = () => (
  <Grid id="grid">
    <GridRow id="row">
      <GridColumn id="column-1" tiny={4}>
        <div>4 Columns</div>
      </GridColumn>
      <GridColumn id="column-2" tiny={8}>
        <div>8 Columns</div>
        <GridRow id="nested-row">
          <GridColumn id="nested-column-1" tiny={5}>
            <div id="nested-column-1-text">5 Nested Columns</div>
          </GridColumn>
          <GridColumn id="nested-column-2" tiny={7}>
            <div id="nested-column-2-text">7 Nested Columns</div>
          </GridColumn>
        </GridRow>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridNested;
