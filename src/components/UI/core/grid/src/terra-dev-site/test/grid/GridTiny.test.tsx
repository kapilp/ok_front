import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
import './GridExample.module.scss';
const GridTiny = () => (
  <Grid id="grid">
    <GridRow>
      <GridColumn id="column-1" tiny={1}>
        <div className="terra-grid-example">1 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-2" tiny={2}>
        <div className="terra-grid-example">2 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-3" tiny={3}>
        <div className="terra-grid-example">3 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-4" tiny={4}>
        <div className="terra-grid-example">4 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-5" tiny={5}>
        <div className="terra-grid-example">5 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-6" tiny={6}>
        <div className="terra-grid-example">6 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-7" tiny={7}>
        <div className="terra-grid-example">7 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-8" tiny={8}>
        <div className="terra-grid-example">8 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-9" tiny={9}>
        <div className="terra-grid-example">9 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-10" tiny={10}>
        <div className="terra-grid-example">10 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-11" tiny={11}>
        <div className="terra-grid-example">11 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-12" tiny={12}>
        <div className="terra-grid-example">12 Column Span</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridTiny;
