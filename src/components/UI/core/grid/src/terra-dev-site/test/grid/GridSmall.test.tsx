import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Grid from '../../../../lib/Grid';
import './GridExample.module.scss';
const GridSmall = () => (
  <Grid id="grid">
    <GridRow>
      <GridColumn id="column-1" small={1}>
        <div className="terra-grid-example">1 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-2" small={2}>
        <div className="terra-grid-example">2 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-3" small={3}>
        <div className="terra-grid-example">3 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-4" small={4}>
        <div className="terra-grid-example">4 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-5" small={5}>
        <div className="terra-grid-example">5 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-6" small={6}>
        <div className="terra-grid-example">6 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-7" small={7}>
        <div className="terra-grid-example">7 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-8" small={8}>
        <div className="terra-grid-example">8 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-9" small={9}>
        <div className="terra-grid-example">9 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-10" small={10}>
        <div className="terra-grid-example">10 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-11" small={11}>
        <div className="terra-grid-example">11 Column Span</div>
      </GridColumn>
    </GridRow>
    <GridRow>
      <GridColumn id="column-12" small={12}>
        <div className="terra-grid-example">12 Column Span</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
export default GridSmall;
