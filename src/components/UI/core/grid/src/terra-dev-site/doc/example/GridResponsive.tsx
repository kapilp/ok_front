import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Grid } from '../../../Grid';
import { GridColumn } from '../../../GridColumn';
import { GridRow } from '../../../GridRow';
import styles from './GridExample.module.scss';
export const GridResponsive = () => (
  <Grid>
    <GridRow>
      <GridColumn tiny={12} small={4} medium={8} large={6}>
        <div className={styles['terra-grid-example']}>First Column content</div>
      </GridColumn>
      <GridColumn tiny={12} small={8} medium={4} large={6}>
        <div className={styles['terra-grid-example']}>Second Column content</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
