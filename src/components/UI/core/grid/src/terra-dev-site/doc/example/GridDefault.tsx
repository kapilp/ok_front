import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import styles from './GridExample.module.scss';
import { Grid } from '../../../Grid';
import { GridColumn } from '../../../GridColumn';
import { GridRow } from '../../../GridRow';

export const GridDefault = () => (
  <Grid>
    <GridRow>
      <GridColumn tiny={6}>
        <div className={styles['terra-grid-example']}>First Column content</div>
      </GridColumn>
      <GridColumn tiny={6}>
        <div className={styles['terra-grid-example']}>Second Column content</div>
      </GridColumn>
    </GridRow>
  </Grid>
);
