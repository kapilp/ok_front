import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import styles from './GridExample.module.scss';
import { Grid } from '../../../Grid';
import { GridColumn } from '../../../GridColumn';
import { GridRow } from '../../../GridRow';
export const GridNested = () => (
  <Grid>
    <GridRow>
      <GridColumn tiny={4}>
        <div className={styles['terra-grid-example']}>4 Columns</div>
      </GridColumn>
      <GridColumn tiny={8}>
        <div className={styles['terra-grid-example']}>8 Columns</div>
        <GridRow>
          <GridColumn tiny={5}>
            <div className={styles['terra-grid-example']}>5 Nested Columns</div>
          </GridColumn>
          <GridColumn tiny={7}>
            <div className={styles['terra-grid-example']}>7 Nested Columns</div>
          </GridColumn>
        </GridRow>
      </GridColumn>
    </GridRow>
  </Grid>
);
