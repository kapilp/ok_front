import { JSX, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Grid.module.scss';
const cx = classNamesBind.bind(styles);

interface Properties {
  /**
   * The component that will be displayed at column level.
   */
  children: JSX.Element;
  /**
   * Custom class name that can be assigned to grid row.
   */
  className?: string;
}
export const GridRow = (props: Properties) => {
  const theme = useContext(ThemeContext);
  const [extracted, customProps] = splitProps(props, ['className', 'children']);

  return (
    <div {...customProps} className={classNames(cx('grid', theme.className), props.className)}>
      {props.children}
    </div>
  );
};
