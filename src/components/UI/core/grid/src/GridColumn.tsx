import { JSX, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Grid.module.scss';
const cx = classNamesBind.bind(styles);

const columnRange = (props, propName) => {
  if (props[propName]) {
    const val = props[propName];
    return val >= 1 && val <= 12 ? null : new Error(`${propName} must be in range 1 to 12 inclusively`);
  }
  return null;
};
type ColumnRange = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
interface Properties {
  /**
   * The component that will be displayed in cell level.
   */
  children?: JSX.Element;
  /**
   * Custom class name that can be assigned to grid column.
   */
  className?: string;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `tiny` breakpoint and wider screens.
   */
  tiny?: ColumnRange;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `enormous` breakpoint and wider screens.
   */
  enormous?: ColumnRange;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `huge` breakpoint and wider screens.
   */
  huge?: ColumnRange;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `large` breakpoint and wider screens.
   */
  large?: ColumnRange;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `medium` breakpoint and wider screens.
   */
  medium?: ColumnRange;
  /**
   * Defines the number of columns this container will span in the 12 column grid. It is applied at the `small` breakpoint and wider screens.
   */
  small?: ColumnRange;
}
export const GridColumn = (props: Properties) => {
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, ['className', 'children', 'tiny', 'small', 'medium', 'large', 'huge', 'enormous']);
  return (
    <div
      {...customProps}
      className={classNames(
        cx([
          'column',
          {
            [`column-tiny-${props.tiny}`]: props.tiny,
            [`column-small-${props.small}`]: props.small,
            [`column-medium-${props.medium}`]: props.medium,
            [`column-large-${props.large}`]: props.large,
            [`column-huge-${props.huge}`]: props.huge,
            [`column-enormous-${props.enormous}`]: props.enormous,
          },
          theme.className,
        ]),
        props.className,
      )}
    >
      {props.children}
    </div>
  );
};
