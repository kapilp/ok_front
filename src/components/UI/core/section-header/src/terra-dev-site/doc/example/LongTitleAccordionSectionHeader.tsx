import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';

export const LongTitleAccordionSectionHeader = (props: {}) => {
  const [state, setState] = createStore({ isOpen: false });

  const handleClick = () => {
    setState({ isOpen: !state.isOpen });
  };

  return (
    <SectionHeaderExampleTemplate
      title="Long Title Accordion Section Header"
      exampleProps={{
        title:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteu',
      }}
      isOpen={state.isOpen}
      onClick={handleClick}
    />
  );
};
