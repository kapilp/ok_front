import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';
export const sectionHeaderProps = {
  title: "I'm clickable, click me",
};
export const OnClickSectionHeader = () => (
  <SectionHeaderExampleTemplate
    title="OnClick Section Header"
    exampleProps={sectionHeaderProps}
    onClick={() => {
      // eslint-disable-next-line no-alert
      window.alert('The accordion has been clicked!');
    }}
  />
);
