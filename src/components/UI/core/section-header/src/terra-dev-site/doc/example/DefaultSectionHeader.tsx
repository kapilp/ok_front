import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';
export const DefaultSectionHeader = () => <SectionHeaderExampleTemplate title="Default" exampleProps={{ title: 'Default Section Header' }} />;
