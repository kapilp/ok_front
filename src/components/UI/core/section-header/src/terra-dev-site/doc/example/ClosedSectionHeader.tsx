import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';
const sectionHeaderProps = {
  title: 'Closed',
};
export const ClosedSectionHeader = () => <SectionHeaderExampleTemplate title="Closed Section Header" exampleProps={sectionHeaderProps} onClick={() => {}} />;
