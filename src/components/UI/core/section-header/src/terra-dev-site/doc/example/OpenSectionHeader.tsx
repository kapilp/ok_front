import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';
const sectionHeaderProps = {
  title: 'Open',
};
export const OpenSectionHeader = () => <SectionHeaderExampleTemplate title="Open Section Header" exampleProps={sectionHeaderProps} isOpen={true} onClick={() => {}} />;
