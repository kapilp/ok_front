import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeaderExampleTemplate } from './SectionHeaderExampleTemplate';

export const AccordionSectionHeader = (props: {}) => {
  const [state, setState] = createStore({ isOpen: false });

  const handleClick = () => {
    setState({ isOpen: !state.isOpen });
  };

  const sectionHeaderProps = {
    title: 'I can accordion, click me',
  };
  return <SectionHeaderExampleTemplate title="Accordion Section Header" exampleProps={sectionHeaderProps} isOpen={state.isOpen} onClick={handleClick} />;
};
