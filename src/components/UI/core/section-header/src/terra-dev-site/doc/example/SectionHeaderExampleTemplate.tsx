import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeader } from '../../../SectionHeader';

interface Properties {
  /**
   * Title of the example
   */
  title: string;
  /**
   * Props to be spread onto SectionHeader
   */
  // eslint-disable-next-line react/forbid-prop-types
  exampleProps: {};
  onClick: () => void;
  isOpen: boolean;
}
export const SectionHeaderExampleTemplate = (props: Properties) => (
  <div>
    <h2>{props.title}</h2>
    <SectionHeader {...props.exampleProps} onClick={props.onClick} isOpen={props.isOpen} />
  </div>
);
