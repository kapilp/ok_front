import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import SectionHeader from '../../../SectionHeader';
export default () => <SectionHeader title="Closed Section Header" onClick={() => {}} />;
