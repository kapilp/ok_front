import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import * as KeyCode from 'keycode-js';
import { Arrange } from '../../arrange/src/Arrange';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './SectionHeader.module.scss';
const cx = classNamesBind.bind(styles);
interface ISectionHeaderProps extends JSX.HTMLAttributes<Element> {
  /**
   * Text to be displayed on the SectionHeader.
   */
  title: string;
  /**
   * Callback function triggered when the accordion icon is clicked.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * Whether the accordion icon should be displayed in its 'open' or 'closed' position.
   */
  isOpen?: boolean;
  /**
   * Optionally sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`. Default `level=2`.
   */
  level?: 1 | 2 | 3 | 4 | 5 | 6;
  /**
   * Sets the background of the section header to transparent.
   */
  isTransparent?: boolean;
}
const defaultProps = {
  onClick: undefined,
  isOpen: false,
  isTransparent: false,
  level: 2,
};
const isRecognizedKeyPress = (event: KeyboardEvent) => event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE;

type SectionHeaderState = {
  isActive?: boolean;
};
export const SectionHeader = (props: ISectionHeaderProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  // let context: any;

  const [state, setState] = createStore({ isActive: false });

  const wrapOnKeyDown = (onKeyDown: (e: KeyboardEvent) => void) => {
    return (event: KeyboardEvent) => {
      if (isRecognizedKeyPress(event) && !state.isActive) {
        setState({ isActive: true });
        // Call the same function the user provides for a click event
        if (props.onClick) {
          props.onClick(event);
        }
      }
      if (onKeyDown) {
        onKeyDown(event);
      }
    };
  };
  const wrapOnKeyUp = (onKeyUp: (e: KeyboardEvent) => void) => {
    return (event: KeyboardEvent) => {
      if (isRecognizedKeyPress(event) && state.isActive) {
        setState({ isActive: false });
      }
      if (onKeyUp) {
        onKeyUp(event);
      }
    };
  };
  const [extracted, customProps] = splitProps(props, ['title', 'onClick', 'isOpen', 'isTransparent', 'level']);
  // const theme = context;
  if (process.env.NODE_ENV !== 'production' && !props.onClick && props.isOpen) {
    // eslint-disable-next-line no-console
    console.warn("'isOpen' are intended to be used only when 'onClick' is provided.");
  }
  const attributes = { ...customProps };
  if (props.onClick) {
    attributes.tabIndex = '0';
    attributes.onKeyDown = wrapOnKeyDown(attributes.onKeyDown);
    attributes.onKeyUp = wrapOnKeyUp(attributes.onKeyUp);
    attributes.role = 'button';
  }
  const accordionIcon = () => (
    <div className={cx('accordion-icon-wrapper')}>
      <span className={cx(['accordion-icon', { 'is-openHack': props.onClick && props.isOpen }])} />
    </div>
  );

  const Element = `h${props.level}`;
  // allows us to set an onClick on the div
  // We set key events and role conditionally set if onClick is set
  // eslint doesn't know about this and so it marks this as a lint error
  /* eslint-disable jsx-a11y/click-events-have-key-events */
  /* eslint-disable jsx-a11y/no-static-element-interactions */
  return (
    <div
      {...attributes}
      onClick={props.onClick}
      className={classNames(
        cx('section-header', { 'is-interactable': props.onClick }, { 'is-active': state.isActive }, { 'is-transparent': props.isTransparent }, theme.className),
        customProps.className,
      )}
    >
      <Arrange
        fitStart={props.onClick && accordionIcon}
        fill={
          <Dynamic component={Element} className={cx('title')}>
            {props.title}
          </Dynamic>
        }
      />
    </div>
  );
  /* eslint-enable jsx-a11y/no-static-element-interactions */
};
