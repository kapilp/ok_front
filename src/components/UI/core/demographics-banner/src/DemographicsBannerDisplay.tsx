import { SmallDemographicsBannerDisplay } from './_SmallDemographicsBannerDisplay';
import { LargeDemographicsBannerDisplay } from './_LargeDemographicsBannerDisplay';
import { ResponsiveElement } from '../../responsive-element/src/ResponsiveElement';
import { JSX, mergeProps } from 'solid-js';
interface Properties {
  /**
   * Application content to display in the banner.
   */
  applicationContent?: JSX.Element;
  /**
   * Age of the person.
   */
  age?: string;
  /**
   * The persons date of birth
   */
  dateOfBirth?: string;
  /**
   * Label to display for the date of birth
   */
  dateOfBirthLabel?: string;
  /**
   * Text Description of the date of birth label that is read by screen readers
   */
  dateOfBirthFullText?: string;
  /**
   * The person's deceased date. Will display the banner as deceased if this value is provided
   */
  deceasedDate?: string;
  /**
   * Label to display for the deceased date
   */
  deceasedDateLabel?: string;
  /**
   * Gender of the Person
   */
  gender?: string;
  /**
   * The persons gestational age.
   */
  gestationalAge?: string;
  /**
   * Label to display for the Gestational Age
   */
  gestationalAgeLabel?: string;
  /**
   * Text Description of the Gestational Age label that is read by screen readers
   */
  gestationalAgeFullText?: string;
  /**
   * Additional key value identifiers of a person's demographic information
   */
  // eslint-disable-next-line react/forbid-prop-types
  identifiers?: {};
  /**
   * Full Name of the person
   */
  personName?: string;
  /**
   * Photo to display in the banner
   */
  photo?: JSX.Element;
  /**
   * The column layout in which to present the displays.
   */
  postMenstrualAge?: string;
  /**
   * Label to display for the PostMenstrualAgeLabel
   */
  postMenstrualAgeLabel?: string;
  /**
   * Text Description of the Post Menstrural Age label that is read by screen readers
   */
  postMenstrualAgeFullText?: string;
  /**
   * The persons preferred first name if they have one.
   */
  preferredFirstName?: string;
}
const defaultProps = {
  applicationContent: null,
  age: '--',
  dateOfBirth: '--',
  dateOfBirthLabel: 'DOB',
  dateOfBirthFullText: 'Date of Birth',
  deceasedDate: null,
  deceasedDateLabel: 'Deceased',
  gender: '--',
  gestationalAge: null,
  gestationalAgeLabel: 'GA',
  gestationalAgeFullText: 'Gestational Age',
  identifiers: {},
  personName: '--',
  photo: null,
  postMenstrualAge: null,
  postMenstrualAgeLabel: 'PMA',
  postMenstrualAgeFullText: 'Post Menstrual Age',
  preferredFirstName: null,
};
export const DemographicsBannerDisplay = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  return <ResponsiveElement tiny={<SmallDemographicsBannerDisplay {...props} />} small={<LargeDemographicsBannerDisplay {...props} />} />;
};
