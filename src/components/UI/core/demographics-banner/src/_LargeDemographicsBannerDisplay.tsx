/* eslint-disable react/prop-types */
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './DemographicsBanner.module.scss';
import DemographicsBannerUtils from './_sharedObjects';
import { JSX, splitProps, useContext } from 'solid-js';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);
export const LargeDemographicsBannerDisplay = (props: {}) => {
  const [extracted, customProps] = splitProps(props, [
    'className',
    'age',
    'applicationContent',
    'dateOfBirth',
    'dateOfBirthFullText',
    'dateOfBirthLabel',
    'deceasedDate',
    'deceasedDateLabel',
    'gender',
    'gestationalAge',
    'gestationalAgeFullText',
    'gestationalAgeLabel',
    'identifiers',
    'personName',
    'photo',
    'postMenstrualAge',
    'postMenstrualAgeFullText',
    'postMenstrualAgeLabel',
    'preferredFirstName',
  ]);

  const theme = useContext(ThemeContext);
  // delete customProps.className;
  return (
    <section className={classNames(cx('demographics-banner', 'large-demographics-banner', { deceased: props.deceasedDate }, theme.className), props.className)} {...customProps}>
      {props.photo && <div className={cx('profile-photo')}>{props.photo}</div>}
      <div className={cx('content')}>
        <div className={cx('row')}>
          <h1 className={cx('person-name')}>
            {props.personName}
            {props.preferredFirstName && <span className={cx('preferred-first-name')}>{props.preferredFirstName}</span>}
          </h1>
          <div className={cx('application-content')}>{props.applicationContent}</div>
        </div>
        <div className={cx('row')}>
          <div className={cx('person-details')}>{DemographicsBannerUtils.personDetails(props)}</div>
          <div className={cx('identifiers')}>{DemographicsBannerUtils.applicationIdentifiers(props)}</div>
        </div>
      </div>
    </section>
  );
};
