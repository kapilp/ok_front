/* eslint-disable react/prop-types */
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import DemographicsBannerUtils from './_sharedObjects';
import styles from './DemographicsBanner.module.scss';
import { JSX, splitProps, useContext } from 'solid-js';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);
export const SmallDemographicsBannerDisplay = (props: {}) => {
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, [
    'className',
    'age',
    'applicationContent',
    'dateOfBirth',
    'dateOfBirthFullText',
    'dateOfBirthLabel',
    'deceasedDate',
    'deceasedDateLabel',
    'gender',
    'gestationalAge',
    'gestationalAgeFullText',
    'gestationalAgeLabel',
    'identifiers',
    'personName',
    'photo',
    'postMenstrualAge',
    'postMenstrualAgeFullText',
    'postMenstrualAgeLabel',
    'preferredFirstName',
  ]);
  //delete customProps.className;
  return (
    <section className={classNames(cx('demographics-banner', { deceased: props.deceasedDate }, theme.className), props.className)} {...customProps}>
      <h1 className={cx('person-name')}>
        <span>
          {props.personName}
          {props.preferredFirstName && <span className={cx('preferred-first-name')}>{props.preferredFirstName}</span>}
        </span>
      </h1>
      <div className={cx('person-details')}>
        {DemographicsBannerUtils.personDetails(props)}
        {DemographicsBannerUtils.applicationIdentifiers(props)}
      </div>
      <div className={cx('application-content')}>{props.applicationContent}</div>
    </section>
  );
};
