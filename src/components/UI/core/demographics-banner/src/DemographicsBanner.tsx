// import { injectIntl, intlShape } from "react-intl";
import { DemographicsBannerDisplay } from './DemographicsBannerDisplay';
import { JSX, mergeProps, splitProps } from 'solid-js';
interface Properties {
  /**
   * Application content to display in the banner.
   */
  applicationContent?: JSX.Element;
  /**
   * Age of the person.
   */
  age?: string;
  /**
   * The persons date of birth
   */
  dateOfBirth?: string;
  /**
   * The person's deceased date. Will display the banner as deceased if this value is provided
   */
  deceasedDate?: string;
  /**
   * Gender of the Person
   */
  gender?: string;
  /**
   * The persons gestational age.
   */
  gestationalAge?: string;
  /**
   * Additional key value identifiers of a person's demographic information
   */
  // eslint-disable-next-line react/forbid-prop-types
  identifiers?: {};
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  // intl:intlShape.isRequired, // Todo fix this
  /**
   * Full Name of the person
   */
  personName?: string;
  /**
   * Photo to display in the banner
   */
  photo?: JSX.Element;
  /**
   * The column layout in which to present the displays.
   */
  postMenstrualAge?: string;
  /**
   * The persons preferred first name if they have one.
   */
  preferredFirstName?: string;
}
const defaultProps = {
  applicationContent: null,
  age: undefined,
  dateOfBirth: undefined,
  deceasedDate: null,
  gender: undefined,
  gestationalAge: null,
  identifiers: {},
  personName: undefined,
  photo: null,
  postMenstrualAge: null,
  preferredFirstName: null,
};
export const DemographicsBanner = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [extracted, customProps] = splitProps(props, ['age', 'dateOfBirth', 'gender', 'intl', 'personName']);
  /*const noDataProvided = intl.formatMessage({
    id: "Terra.demographicsBanner.noDataProvided"
  });*/
  const noDataProvided = '--';
  return (
    <DemographicsBannerDisplay
      {...customProps}
      age={props.age || noDataProvided}
      dateOfBirth={props.dateOfBirth || noDataProvided}
      gender={props.gender || noDataProvided}
      personName={props.personName || noDataProvided}
      dateOfBirthLabel={'DOB'}
      dateOfBirthFullText={'Date of Birth'}
      deceasedDateLabel={'Deceased'}
      gestationalAgeLabel={'GA'}
      gestationalAgeFullText={'Gestational Age'}
      postMenstrualAgeLabel={'PMA'}
      postMenstrualAgeFullText={'Post Menstrual Age'}
    />
  );
};

//export default injectIntl(DemographicsBanner);
/*
    <DemographicsBannerDisplay
      {...customProps}
      age={props.age || noDataProvided}
      dateOfBirth={props.dateOfBirth || noDataProvided}
      gender={props.gender || noDataProvided}
      personName={props.personName || noDataProvided}
      dateOfBirthLabel={intl.formatMessage({
        id: "Terra.demographicsBanner.dateOfBirth"
      })}
      dateOfBirthFullText={intl.formatMessage({
        id: "Terra.demographicsBanner.dateOfBirthFullText"
      })}
      deceasedDateLabel={intl.formatMessage({
        id: "Terra.demographicsBanner.deceased"
      })}
      gestationalAgeLabel={intl.formatMessage({
        id: "Terra.demographicsBanner.gestationalAge"
      })}
      gestationalAgeFullText={intl.formatMessage({
        id: "Terra.demographicsBanner.gestationalAgeFullText"
      })}
      postMenstrualAgeLabel={intl.formatMessage({
        id: "Terra.demographicsBanner.postMenstrualAge"
      })}
      postMenstrualAgeFullText={intl.formatMessage({
        id: "Terra.demographicsBanner.postMenstrualAgeFullText"
      })}
    />
 */
