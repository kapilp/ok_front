import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import DemographicsBanner from '../../../DemographicsBanner';
export default () => <DemographicsBanner id="blank-demographics-banner" />;
