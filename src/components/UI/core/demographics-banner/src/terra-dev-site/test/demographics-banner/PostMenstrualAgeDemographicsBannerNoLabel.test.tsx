import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import DemographicsBanner from '../../../DemographicsBanner';
export default () => <DemographicsBanner id="post-menstrual-no-label" postMenstrualAge="April 5, 2016" />;
