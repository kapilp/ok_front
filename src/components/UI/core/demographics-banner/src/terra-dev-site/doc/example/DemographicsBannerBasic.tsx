import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { DemographicsBanner } from '../../../DemographicsBanner';

export const DemographicsBannerBasic = () => <DemographicsBanner age="25 Years" dateOfBirth="May 9, 1993" gender="Male" personName="Johnathon Doe" preferredFirstName="John" />;
