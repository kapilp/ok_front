import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import { DividerExampleTemplate } from './DividerExampleTemplate';
import { Divider } from '../../../Divider';
export const DividerExample = () => (
  <DividerExampleTemplate>
    <Divider />
  </DividerExampleTemplate>
);
