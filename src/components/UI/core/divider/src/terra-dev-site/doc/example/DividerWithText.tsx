import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { DividerExampleTemplate } from './DividerExampleTemplate';
import { Divider } from '../../../Divider';
const currentDate = 'November 12, 1955';
export const DividerWithText = () => (
  <DividerExampleTemplate>
    <Divider text={currentDate} />
  </DividerExampleTemplate>
);
