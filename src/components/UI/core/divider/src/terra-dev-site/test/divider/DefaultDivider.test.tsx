import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Divider from '../../../Divider';
import styles from './DividerTestCommon.module.scss';
const cx = classNames.bind(styles);
export default () => (
  <div>
    <div className={cx('separator')} />
    <Divider id="divider" />
    <div className={cx('separator')} />
  </div>
);
