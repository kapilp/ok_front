import { JSX, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Divider.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * String to be displayed inline with the divider.
   */
  text: string;
}

export const Divider = (props: Properties) => {
  const theme = useContext(ThemeContext);

  const [extracted, customProps] = splitProps(props, ['text']);
  if (!props.text) {
    return <hr {...customProps} className={cx(['divider', theme.className], customProps.className)} aria-hidden="true" />;
  }

  return (
    <div className={cx(['tDivider', 'divider-container', theme.className])}>
      <span className={cx(['divider-text'])}>{props.text}</span>
    </div>
  );
};
