import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import PropsTable from '../../../PropsTable';
// eslint-disable-next-line import/no-webpack-loader-syntax, import/first
import MockSrc from '!raw-loader!../../../../src/terra-dev-site/test/props-table-test/MockPrivatePropsComponent';
const MockPrivatePropsTable = () => (
  <div>
    <PropsTable id="PrivatePropsTable" src={MockSrc} />
  </div>
);
export default MockPrivatePropsTable;
