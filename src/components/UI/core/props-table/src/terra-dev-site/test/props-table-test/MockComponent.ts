import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
// These props are used to test react-doc-gen, but they only need to be mocked
/* eslint-disable react/no-unused-prop-types */
interface Properties {
  /**
   * An example of type string markup.
   * `This component contains a very long section of code in it's description column. This component contains a very long section of code in it's description column.`
   */
  string: string.isRequired,
  /**
   * An example of type number markup.
   */
  number: PropTypes.number,
  /**
   * An example of type boolean markup.
   * ```This component contains a very long preformatted section in it's description column. This component contains a very long preformatted section in it's description column.```
   */
  bool: boolean,
  /**
   * An example of type element markup.
   */
  element: JSX.Element,
  /**
   * An example of type node markup.
   */
  node: JSX.Element,
  /**
   * An example of type array markup.
   */
  // eslint-disable-next-line react/forbid-prop-types
  array: PropTypes.array,
  /**
   * A default example of type arrayOf markup.
   */
  arrayOfDefault: PropTypes.arrayOf(JSX.Element),
  /**
   * An example of type arrayOf(shapes)  markup.
   */
  arrayOfShapes: PropTypes.arrayOf(
    PropTypes.shape({
      stringProp: string
    })
  ),
  /**
   * An example of type oneOfType markup with the option of a string, number or shape type.
   */
  oneOfType: PropTypes.oneOfType([
    string,
    PropTypes.number,
    PropTypes.shape({
      stringProp: string
    })
  ]),
  /**
   * An example of type shape markup.
   */
  shape: PropTypes.shape({
    stringProp: string,
    numberProp: PropTypes.number
  })
};
const MockComponent: Component = props =>
  React.createElement("span", JSON.stringify({ props }));
export default MockComponent;
