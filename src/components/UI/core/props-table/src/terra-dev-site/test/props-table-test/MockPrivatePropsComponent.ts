import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
// These props are used to test react-doc-gen, but they only need to be mocked
/* eslint-disable react/no-unused-prop-types */
interface Properties {
  /**
   * An example of type string markup.
   * `This component contains a very long section of code in it's description column. This component contains a very long section of code in it's description column.`
   */
  string: string.isRequired,
  /**
   * @private
   * A private prop that should not show up in the rendered markup.
   */
  privateProp: string,
  /**
   * @private Testing inline private declaration
   */
  anotherPrivateProp: string
};
const MockComponent: Component = props =>
  React.createElement("span", JSON.stringify({ props }));
export default MockComponent;
