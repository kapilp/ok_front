import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from '../Table.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The children passed to the component
   */
  children?: JSX.Element;
  /**
   * Function callback for the ref of the tr.
   */
  ref?: (el: HTMLElement) => void;
}
const defaultProps = {
  children: [],
};
export const HeaderRow = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['className', 'children', 'ref']);

  return (
    <div className={p.className ? `${cx('header')} ${p.className}` : cx('header')} role="rowgroup">
      <div {...customProps} className={cx(['header-content'])} role="row">
        {p.children}
      </div>
    </div>
  );
};
