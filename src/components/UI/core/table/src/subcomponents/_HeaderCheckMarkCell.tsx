import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { VisuallyHiddenText } from '../../../visually-hidden-text/src/VisuallyHiddenText';
import styles from './HeaderCheckMarkCell.module.scss';
import { wrappedOnClickForItem, wrappedOnKeyDownForItem, wrappedEventCallback } from './utils';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Aria label for the content.
   */
  label: string;
  /**
   * Whether or not the check should be visually hidden.
   */
  isHidden?: boolean;
  /**
   * The bottom padding to be used for the HeaderCheckMarkCell.
   * To used in conjunction with a cellPaddingStyle of none. Allowing for consumers to set their own padding.
   */
  alignmentPadding?: string;
  /**
   * Whether or not the checkmark displays as disabled. Dependent on `'isSelectable'`.
   */
  isDisabled?: boolean;
  /**
   * Whether or not a selected state should display as partially selected.
   */
  isIndeterminate?: boolean;
  /**
   * Whether or not row is selected
   */
  isSelected?: boolean;
  /**
   * Whether or not row is selectable, this will dictate whether or not a checkmark is present.
   */
  isSelectable?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: (e: Event, metaData: {}) => void;
  /**
   * Function callback for the ref of the td.
   */
  ref?: (el: HTMLElement) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: Event) => void;
}
const defaultProps = {
  isHidden: false,
  isDisabled: false,
  isIndeterminate: false,
  isSelected: false,
  isSelectable: false,
};
export const HeaderCheckMarkCell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'label',
    'alignmentPadding',
    'isHidden',
    'isDisabled',
    'isIndeterminate',
    'isSelected',
    'isSelectable',
    'metaData',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onSelect',
    'ref',
    'className',
  ]);

  const attrPadding = () => {
    let attrPadding = {};
    if (p.alignmentPadding) {
      attrPadding = { style: { 'padding-bottom': p.alignmentPadding } };
    }
    return attrPadding;
  };

  const attrSpread = () => {
    let attrSpread = {};
    if (p.isSelectable) {
      if (!p.isHidden) {
        attrSpread = {
          onClick: wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData),
          onKeyDown: wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData),
          onBlur: wrappedEventCallback(p.onBlur, event => event.currentTarget.setAttribute('data-cell-show-focus', 'true')),
          onMouseDown: wrappedEventCallback(p.onMouseDown, event => event.currentTarget.setAttribute('data-cell-show-focus', 'false')),
          tabIndex: '0',
          'data-cell-show-focus': true,
        };
      }
    }
    return attrSpread;
  };

  const attrCheck = () => {
    let attrCheck = {};
    if (p.isSelectable) {
      attrCheck = {
        role: 'checkbox',
        'aria-checked': p.isSelected && p.isIndeterminate ? 'mixed' : p.isSelected,
      };
      if (p.isDisabled) {
        attrCheck['aria-disabled'] = true;
      }
    }
    return attrCheck;
  };

  const headerCheckMarkCellClasses = () => cx('header-cell', { 'hide-cell': p.isHidden }, { 'is-interactable': !p.isDisabled && p.isSelectable });

  return (
    <div
      {...{ ...customProps, ...attrSpread() }}
      className={p.className ? `${headerCheckMarkCellClasses()} ${p.className}` : headerCheckMarkCellClasses()}
      ref={p?.ref}
      role="columnheader"
    >
      <div {...attrPadding()} className={cx({ container: !p.isHidden })}>
        <VisuallyHiddenText className={cx('label')} {...attrCheck} text={p.label} />
        <div
          aria-hidden
          focusable="false"
          className={cx(
            'checkmark',
            { 'is-selected': p.isSelectable && p.isSelected },
            { 'is-intermediate': p.isSelectable && p.isIndeterminate },
            { 'is-disabled': p.isSelectable && p.isDisabled },
            { 'is-hidden': p.isHidden },
          )}
        />
      </div>
    </div>
  );
};
