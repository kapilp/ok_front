import { JSX, Component, mergeProps, Show, splitProps, useContex,createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { VisuallyHiddenText } from '../../../visually-hidden-text/src/VisuallyHiddenText';
import styles from './Cell.module.scss';
import { styleFromWidth } from './utils';
import { widthShape } from '../proptypes/widthShape';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Child content to be displayed for the row cell.
   */
  children?: JSX.Element;
  /**
   * Data to attach with the link role of the cell.
   */
  disclosureData?: {
    /**
     * Aria label to be applied when the cell is marked as primary.
     */
    label: string;
    /**
     * Whether or not the link role should be marked as current.
     */
    isCurrent?: boolean;
  };
  /**
   * Function callback returning the html node for the cell.
   */
  ref?: (el: HTMLDivElement) => void;
  /**
   * Whether or not the cell's inner containing element responsible for handling table's default padding is removed.
   * This is useful to optimize the DOM for either a table without padding or to optimize a cell whose custom content is providing its own padding.
   */
  removeInner?: boolean;
  /**
   * Width of the cell. Should match header cell counter-part.
   */
  width?: widthShape;
}
const defaultProps = {
  children: [],
  removeInner: false,
};
export const Cell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'ref', 'removeInner', 'width', 'disclosureData', 'className']);

  const cellClassNames = cx('cell');
  const contentClass = createMemo(() => (!p.removeInner ? cx('container') : undefined));

  const ariaAttr = createMemo(() => (p.disclosureData ? { role: 'link', 'aria-current': p.disclosureData.isCurrent } : undefined));

  const ariaElement = createMemo(() => (p.disclosureData ? <VisuallyHiddenText text={p.disclosureData.label} /> : undefined));

  // In both the case of disclosure and default presentation we need a containing div.
  // For disclosure it requires the role of link and for default it's the only way to consistently apply padding.
  const content = (
    <>
      {ariaAttr() || contentClass() ? (
        <div {...ariaAttr()} className={contentClass() || cx('content-width')}>
          {p.children}
          {ariaElement}
        </div>
      ) : (
        p.children
      )}
    </>
  );

  return (
    <div
      {...customProps}
      style={styleFromWidth(p.width)} // eslint-disable-line react/forbid-dom-props
      className={p.className ? `${cellClassNames} ${p.className}` : cellClassNames}
      ref={p?.ref}
      role="gridcell"
      tabIndex={p.disclosureData ? '-1' : undefined}
    >
      {content}
    </div>
  );
};
