import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './HeaderCell.module.scss';
import { styleFromWidth, wrappedOnClickForItem, wrappedOnKeyDownForItem, wrappedEventCallback } from './utils';
import { widthShape } from '../proptypes/widthShape';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Content to be displayed for the column header.
   */
  children?: JSX.Element;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback returning the html node for the header cell.
   */
  ref?: (el: HTMLElement) => void;
  /**
   * Whether or not the cell's inner containing element responsible for handling table's default padding is removed.
   * This is useful to optimize the DOM for either a table without padding or to optimize a cell whose custom content is providing its own padding.
   */
  removeInner?: boolean;
  /**
   * Whether or not the sort direction is descending. False indicates ascending.
   */
  isSortDesc?: boolean;
  /**
   * Whether or not the header cell should display as an actively sorted cell.
   */
  isSortActive?: boolean;
  /**
   * Function callback associated to a pure cell click/action, potentially for selection, etc.
   * Callback contains the javascript event and prop metadata, e.g. onCellAction(event, metaData)
   */
  onCellAction?: (e: Event) => void;
  /**
   * Function callback associated to the sort click/action.
   * Callback contains the javascript event and prop metadata, e.g. onSortAction(event, metaData)
   */
  onSortAction?: (e: Event) => void;
  /**
   * Width of the header cell. Should match row cell counter-part.
   */
  width?: widthShape;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: Event) => void;
}
const defaultProps = {
  children: [],
  removeInner: false,
  isSortDesc: false,
  isSortActive: false,
};
export const HeaderCell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'children',
    'isSortDesc',
    'isSortActive',
    'metaData',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onCellAction',
    'onSortAction',
    'ref',
    'removeInner',
    'width',
  ]);

  const sortIndicator = (
    <>
      {(p.onSortAction || p.isSortActive) && (
        <div
          aria-hidden="true"
          key="sort"
          className={cx(`sort-indicator-${p.isSortDesc ? 'desc' : 'asc'}`, {
            'sort-is-active': p.isSortActive,
          })}
        />
      )}
    </>
  );

  const onAction = createMemo(() => p.onSortAction || p.onCellAction);

  // Only apply sort if it's an active sort column.
  const attrSpread = createMemo(() => {
    const attrSpread = {};
    if (p.isSortActive) {
      attrSpread['aria-sort'] = p.isSortDesc ? 'descending' : 'ascending';
    }

    if (onAction()) {
      attrSpread.onClick = wrappedOnClickForItem(p.onClick, onAction(), p.metaData);
      attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, onAction(), p.metaData);
      attrSpread.tabIndex = '0';
      attrSpread['data-header-show-focus'] = 'true';
      attrSpread.onBlur = wrappedEventCallback(p.onBlur, event => event.currentTarget.setAttribute('data-header-show-focus', 'true'));
      attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, event => event.currentTarget.setAttribute('data-header-show-focus', 'false'));
      attrSpread['aria-selected'] = false;
    }

    return attrSpread;
  });

  const contentBase = (
    <>
      <div className={cx('cell-content')} key="content">
        {p.children}
      </div>
      {sortIndicator}
    </>
  );
  const content = <>{!p.removeInner ? <div className={cx('container')}>{contentBase}</div> : contentBase}</>;

  const headerCellClasses = createMemo(() => cx('header-cell', { 'is-interactable': onAction() }));

  return (
    <div
      {...{ ...customProps, ...attrSpread() }}
      style={styleFromWidth(p.width)} // eslint-disable-line react/forbid-dom-props
      className={customProps.className ? `${headerCellClasses()} ${customProps.className}` : headerCellClasses()}
      ref={p?.ref}
      role="columnheader"
    >
      {content}
    </div>
  );
};
