import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SectionHeader } from './_SectionHeader';
interface Properties {
  /**
   * The children list items passed to the component.
   */
  children?: JSX.Element;
  /**
   * The numberOfColumns to be used as a descriptor for assistive technology.
   */
  numberOfColumns: number;
  /**
   * Whether or not the section is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * Whether or not the section can be collapsed.
   */
  isCollapsible?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback pass-through for the ref of the section header.
   */
  ref?: (el: HTMLElement) => void;
  /**
   * Title text to be placed within the section header.
   */
  title: string;
}
const defaultProps = {
  children: [],
  isCollapsed: false,
  isCollapsible: false,
};
export const Section = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'isCollapsed', 'isCollapsible']);

  return (
    <>
      <SectionHeader {...customProps} isCollapsible={p.isCollapsible} isCollapsed={p.isCollapsed} />
      {(!p.isCollapsible || !p.isCollapsed) && p.children}
    </>
  );
};
