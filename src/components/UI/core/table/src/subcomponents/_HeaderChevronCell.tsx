import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './HeaderChevronCell.module.scss';
const cx = classNames.bind(styles);
export const HeaderChevronCell = (props: {}) => {
  const [p, customProps] = splitProps(props, ['className']);
  return (
    <div {...customProps} className={p.className ? `${cx('cell')} ${p.className}` : cx('cell')} role="none">
      <div className={cx('container')}>
        <div className={cx('chevron')} />
      </div>
    </div>
  );
};
