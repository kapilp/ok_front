import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { VisuallyHiddenText } from '../../../visually-hidden-text/src/VisuallyHiddenText';
import styles from './CheckMarkCell.module.scss';
import { wrappedOnClickForItem, wrappedOnKeyDownForItem, wrappedEventCallback } from './utils';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Aria label for the check mark.
   */
  label: string;
  /**
   * Whether or not the check mark cell should be read only.
   */
  isIcon?: boolean;
  /**
   * Whether or not the check mark cell should be visually hidden.
   */
  isHidden?: boolean;
  /**
   * The top padding to be used for the CheckMarkCell.
   * To used in conjunction with a cellPaddingStyle of none. Allowing for consumers to set their own padding.
   * The presence of this property will also change alignment to a fixed value, rather then centered.
   */
  alignmentPadding?: string;
  /**
   * Whether or not the cell displays as disabled.
   */
  isDisabled?: boolean;
  /**
   * Whether or not the check should be it's own click target.
   */
  isSelectable?: boolean;
  /**
   * Whether or not the cell should display as selected with check mark.
   */
  isSelected?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: (e: Event) => void;
  /**
   * Function callback returning the html node of the check mark cell.
   */
  ref?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: Event) => void;
}
const defaultProps = {
  isIcon: false,
  isHidden: false,
  isDisabled: false,
  isSelected: false,
  isSelectable: false,
};
export const CheckMarkCell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'alignmentPadding',
    'isIcon',
    'isHidden',
    'isDisabled',
    'isSelected',
    'isSelectable',
    'label',
    'metaData',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onSelect',
    'ref',
    'className',
  ]);

  const attrSpread = createMemo(() => {
    const attrSpread = { 'aria-selected': p.isSelected };
    if ((p.isHidden || p.isIcon) && !p.isDisabled) {
      // A user of a screenreader still need a keyboard accessible method of selection, so providing -1 index and kydown.
      attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
      attrSpread.tabIndex = '-1';
    } else if (p.isSelectable && !p.isDisabled) {
      attrSpread.onClick = wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData);
      attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
      attrSpread.tabIndex = '0';
      attrSpread['data-cell-show-focus'] = 'true';
      attrSpread.onBlur = wrappedEventCallback(p.onBlur, event => {
        event.stopPropagation();
        event.currentTarget.setAttribute('data-cell-show-focus', 'true');
      });
      attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, event => {
        event.stopPropagation();
        event.currentTarget.setAttribute('data-cell-show-focus', 'false');
      });
    }
    return attrSpread;
  });

  const attrCheck = createMemo(() => (p.isDisabled ? { 'aria-disabled': true } : {}));

  let attrPadding = createMemo(() => (p.alignmentPadding ? { style: { 'padding-top': p.alignmentPadding } } : undefined));

  const checkMarkClasses = createMemo(() => cx('cell', { 'hide-cell': p.isHidden }, { 'is-interactable': !p.isDisabled && p.isSelectable }, { 'is-top-align': attrPadding() }));

  return (
    <div {...{ ...customProps, ...(attrSpread() ?? {}) }} className={p.className ? `${checkMarkClasses()} ${p.className}` : checkMarkClasses()} ref={p?.ref} role="gridcell">
      <div {...attrPadding} className={cx({ container: !p.isHidden })}>
        <VisuallyHiddenText aria-checked={p.isSelected} role="checkbox" {...attrCheck()} text={p.label} />
        <div
          aria-hidden
          focusable="false"
          className={cx('checkmark', { 'is-selectable': p.isSelectable }, { 'is-selected': p.isSelected }, { 'is-disabled': p.isDisabled }, { 'is-hidden': p.isHidden })}
        />
      </div>
    </div>
  );
};
