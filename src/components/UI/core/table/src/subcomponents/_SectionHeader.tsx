import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { wrappedOnClickForItem, wrappedOnKeyDownForItem, wrappedEventCallback } from './utils';
import styles from './SectionHeader.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The colSpan to be used as a descriptor for assistive technology.
   */
  colSpan?: number;
  /**
   * The id to be place on the column header.
   */
  id?: string;
  /**
   * @private Whether or not the section is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * @private Whether or not the section can be collapsed.
   */
  isCollapsible?: boolean;
  /**
   * @private The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: object;
  /**
   * The numberOfColumns to be used as a descriptor for assistive technology.
   */
  numberOfColumns: number;
  /**
   * @private Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: (e: Event) => void;
  /**
   * Function callback returning the html node of the header.
   */
  ref?: (e: Event) => void;
  /**
   * Title text to be placed within the section header.
   */
  title: string;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: Event) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: Event) => void;
}
const defaultProps = {
  isCollapsed: false,
  isCollapsible: false,
};
export const SectionHeader = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'isCollapsed',
    'isCollapsible',
    'metaData',
    'numberOfColumns',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onSelect',
    'ref',
    'title',
    'id',
    'className',
  ]);

  const titleElementBase = <span className={cx('title')}>{p.title}</span>;
  const titleElement = <>{p.isCollapsible ? <div className={cx('fill')}>{titleElementBase}</div> : titleElementBase}</>;

  const accordionIcon = (
    <>
      {p.isCollapsible && (
        <div aria-hidden className={cx('start')}>
          <span className={cx(['accordion-icon', { 'is-open': !p.isCollapsed }])} />
        </div>
      )}
    </>
  );

  const attrSpread = createMemo(() => {
    const attrSpread = {};
    if (p.isCollapsible) {
      attrSpread.onClick = wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData);
      attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
      attrSpread.tabIndex = '0';
      attrSpread['aria-expanded'] = !p.isCollapsed;
      attrSpread['data-row-show-focus'] = 'true';
      attrSpread.onBlur = wrappedEventCallback(p.onBlur, event => event.currentTarget.setAttribute('data-row-show-focus', 'true'));
      attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, event => event.currentTarget.setAttribute('data-row-show-focus', 'false'));
    }
    return attrSpread;
  });

  const sectionHeaderClasses = createMemo(() =>
    cx('section-header', {
      'is-collapsible': p.isCollapsible,
    }),
  );

  return (
    <div {...customProps} className={p.className ? `${sectionHeaderClasses()} ${p.className}` : sectionHeaderClasses()} ref={p?.ref} role="row">
      <div {...attrSpread()} id={p.id} colSpan={p.numberOfColumns} headers="" role="columnheader" className={cx('section-content')}>
        {accordionIcon}
        {titleElement}
      </div>
    </div>
  );
};
