import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './ChevronCell.module.scss';
const cx = classNames.bind(styles);
export const ChevronCell = (props: {}) => {
  const [p, customProps] = splitProps(props, ['className']);
  return (
    <div {...customProps} className={p.className ? `${cx('cell')} ${p.className}` : cx('cell')} role="none">
      <div className={cx('container')}>
        <span className={cx('chevron')} />
      </div>
    </div>
  );
};
