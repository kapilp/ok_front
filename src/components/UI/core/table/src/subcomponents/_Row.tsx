import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Row.module.scss';
import { wrappedOnClickForItem, wrappedOnKeyDownForItem, wrappedEventCallback } from './utils';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The children to be passed as row content.
   */
  children: JSX.Element;
  /**
   * Indicates the desired divider style to apply to the row and its children.
   */
  dividerStyle?: 'vertical' | 'horizontal' | 'both';
  /**
   * Whether or not the rows interaction is disabled.
   */
  isDisabled?: boolean;
  /**
   * Whether or not row should display as selected to match the link primary cell or toggle state of the row.
   */
  isSelected?: boolean;
  /**
   * Whether or not row can be selected.
   */
  isSelectable?: boolean;
  /**
   * Whether or not row should display as a striped row.
   */
  isStriped?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback returning the html node for the row.
   */
  ref?: () => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: () => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: () => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: () => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: () => void;
}
const defaultProps = {
  isDisabled: false,
  isSelected: false,
  isSelectable: false,
  isStriped: false,
};
export const Row = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'children',
    'dividerStyle',
    'isDisabled',
    'isSelected',
    'isSelectable',
    'isStriped',
    'metaData',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onSelect',
    'ref',
    'className',
  ]);

  const attrSpread = createMemo(() => {
    const attrSpread = {};
    if (p.isSelectable) {
      if (p.isDisabled) {
        attrSpread['aria-disabled'] = true;
      } else {
        attrSpread.onClick = wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData);
        attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
        attrSpread.tabIndex = '0';
        attrSpread['data-row-show-focus'] = 'true';
        attrSpread.onBlur = wrappedEventCallback(p.onBlur, event => event.currentTarget.setAttribute('data-row-show-focus', 'true'));
        attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, event => event.currentTarget.setAttribute('data-row-show-focus', 'false'));
      }
    }
    return attrSpread;
  });

  const divider = createMemo(() => (p.dividerStyle ? `divider-${p.dividerStyle}` : undefined));
  const rowClasses = createMemo(() =>
    cx({ 'is-selected': p.isSelected && p.isSelectable }, { 'is-selectable': !p.isDisabled && p.isSelectable }, { 'is-striped': p.isStriped }, divider(), 'row'),
  );

  return (
    <div {...{ ...customProps, ...attrSpread() }} className={p.className ? `${rowClasses()} ${p.className}` : rowClasses()} ref={p?.ref} role="row">
      {p.children}
    </div>
  );
};
