import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Table from 'terra-table';
const PaddingTable = () => (
  <>
    <Table
      summaryId="compact-table"
      summary="This table has compact row padding."
      cellPaddingStyle="compact"
      numberOfColumns={1}
      headerData={{
        cells: [{ id: 'header-name', key: 'name', children: 'Name' }],
      }}
      bodyData={[
        {
          rows: [
            {
              key: 'row-0',
              cells: [{ key: 'cell-0', children: 'John Smith' }],
            },
          ],
        },
      ]}
    />
    <Table
      summaryId="standard-table"
      summary="This table has standard row padding."
      cellPaddingStyle="standard"
      numberOfColumns={1}
      headerData={{
        cells: [{ id: 'header-name', key: 'name', children: 'Name' }],
      }}
      bodyData={[
        {
          rows: [
            {
              key: 'row-0',
              cells: [{ key: 'cell-0', children: 'John Smith' }],
            },
          ],
        },
      ]}
    />
  </>
);
export default PaddingTable;
