import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
import headerCellShape from "../../../../proptypes/headerCellShape";
import selecAllColumnShape from "../../../../proptypes/selectAllColumnShape";
/**
 * The props table parser continues to have trouble detecting the propTypes of a component that
 * returns `null`. I've duplicated the propType definition here so that a table could be parsed for it.
 */
interface Properties {
  /**
   * The cells to be displayed within the header.
   */
  cells: PropTypes.arrayOf(headerCellShape),
  /**
   * Function callback returning the html node for the header.
   */
  ref: PropTypes.func,
  /**
   * The select all column header's properties.
   */
  selectAllColumn: selecAllColumnShape
};
const PropTypesExample: Component = ({ ...customProps }) => <div />;
export default PropTypesExample;
