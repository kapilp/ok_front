import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import TableUtils from '../../../TableUtils';
import mockData, { RowDataType } from './mock-data/mock-select';
const createCell = cell => ({ key: cell.key, children: cell.title });
const createCellsForRow = cells => cells.map(cell => createCell(cell));
export const CheckMarkTableGuide = () => {
  const [state, setState] = createStore({ checkedKeys: [], allChecked: false });

  const rowCount = mockData.length; // This value needs to exclude or account for section headers
  const handleRowCheckAction = (event: Event, metaData: { key: string }) => {
    event.preventDefault();
    const newKeys = TableUtils.toggleArrayValue(state.checkedKeys, metaData.key);
    const isMax = newKeys.length === rowCount;
    setState({ allChecked: state.allChecked ? !isMax : isMax, checkedKeys: isMax ? [] : newKeys });
  };
  const handleHeaderCheckAction = () => {
    setState({ allChecked: !!state.checkedKeys.length || !state.allChecked, checkedKeys: [] });
  };
  const getIsRowChecked = (key: string) => {
    if (state.checkedKeys.length) {
      const isPresent = state.checkedKeys.indexOf(key) >= 0;
      return state.allChecked ? !isPresent : isPresent;
    }
    return state.allChecked;
  };
  const createRow = (rowData: RowDataType) => ({
    key: rowData.key,
    cells: createCellsForRow(rowData.cells),
    toggleAction: {
      metaData: { key: rowData.key },
      onToggle: handleRowCheckAction,
      toggleLabel: rowData.toggleText,
      isToggled: createMemo(() => getIsRowChecked(rowData.key)),
    },
    discloseAction: {
      discloseLabel: rowData.discloseText,
      discloseCellIndex: rowData.primaryIndex,
    },
  });
  const createRows = (data: RowDataType[]) => data.map(childItem => createRow(childItem));
  let status = 'empty';
  if (state.checkedKeys.length) {
    status = 'indeterminate';
  } else if (state.allChecked) {
    status = 'checked';
  }
  return (
    <Table
      summaryId="check-table"
      summary="This table has rows that can be batch selected with the checkbox or disclosed for further details."
      numberOfColumns={4}
      cellPaddingStyle="standard"
      rowStyle="disclose"
      checkStyle="toggle"
      dividerStyle="horizontal"
      headerData={{
        selectAllColumn: {
          checkStatus: status,
          checkLabel: 'Batch Selection',
          onCheckAction: handleHeaderCheckAction,
        },
        cells: [
          { key: 'cell-0', id: 'toggle-0', children: 'Column 0' },
          { key: 'cell-1', id: 'toggle-1', children: 'Column 1' },
          { key: 'cell-2', id: 'toggle-2', children: 'Column 2' },
          { key: 'cell-3', id: 'toggle-3', children: 'Column 3' },
        ],
      }}
      bodyData={[
        {
          rows: createRows(mockData),
        },
      ]}
    />
  );
};
