import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import styles from './FixedNode.module.scss';
const cx = classNames.bind(styles);
/* eslint-disable */
export const FixedNode = props => (
  <div className={cx('node')}>
    <Placeholder title={props.title} />
  </div>
);
