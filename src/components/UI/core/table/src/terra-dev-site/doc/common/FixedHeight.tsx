import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './FixedHeight.module.scss';
const cx = classNames.bind(styles);
/* eslint-disable */
export const FixedHeight = props => <div className={cx('container')}>{props.children}</div>;
