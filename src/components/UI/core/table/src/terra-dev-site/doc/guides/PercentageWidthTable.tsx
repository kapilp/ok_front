import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import TableUtils from '../../../TableUtils';
import mockData, { RowDataType } from './mock-data/mock-select';
const createCell = cell => ({ key: cell.key, children: cell.title });
const createCellsForRow = cells => cells.map(cell => createCell(cell));
const createRow = (rowData: RowDataType) => ({
  key: rowData.key,
  cells: createCellsForRow(rowData.cells),
});
const createRows = (data: RowDataType[]) => data.map(childItem => createRow(childItem));
export const PercentageWidthTable = () => (
  <Table
    summaryId="example-percentage-table"
    summary="This table shows an implementation of percentage width table columns."
    numberOfColumns={4}
    cellPaddingStyle="standard"
    columnWidths={[{ percentage: 20 }, { percentage: 40 }, { percentage: 10 }, { percentage: 30 }]}
    dividerStyle="horizontal"
    headerData={{
      cells: [
        { key: 'cell-0', id: 'unique-cell-0', children: '20%' },
        { key: 'cell-1', id: 'unique-cell-1', children: '40%' },
        { key: 'cell-2', id: 'unique-cell-2', children: '10%' },
        { key: 'cell-3', id: 'unique-cell-3', children: '30%' },
      ],
    }}
    bodyData={[
      {
        rows: createRows(mockData),
      },
    ]}
  />
);
