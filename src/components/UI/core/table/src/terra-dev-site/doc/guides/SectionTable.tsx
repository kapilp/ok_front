import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import TableUtils from '../../../TableUtils';
import mockData from './mock-data/mock-section';
import { RowDataType } from './mock-data/mock-select';
const createCell = cell => ({ key: cell.key, children: [cell.title] });
const createCellsForRow = cells => cells.map(cell => createCell(cell));
export const SectionTable = () => {
  const [state, setState] = createStore({ collapsedKeys: [] });
  const handleSectionToggle = (event, metaData) => {
    event.preventDefault();
    setState({ collapsedKeys: TableUtils.toggleArrayValue(state.collapsedKeys, metaData.key) });
  };
  const createRow = (rowData: RowDataType) => ({
    key: rowData.key,
    cells: createCellsForRow(rowData.cells),
  });
  const createSection = sectionData => ({
    sectionHeader: {
      id: `sub-${sectionData.key}`,
      key: sectionData.key,
      title: sectionData.title,
      onToggle: handleSectionToggle,
      metaData: { key: sectionData.key },
      isCollapsed: createMemo(() => state.collapsedKeys.indexOf(sectionData.key) >= 0),
    },
    rows: sectionData.childItems.map(childItem => createRow(childItem)),
  });
  return (
    <Table
      summaryId="example-sorted-table"
      summary="This table shows an implementation of sections."
      numberOfColumns={3}
      cellPaddingStyle="standard"
      dividerStyle="horizontal"
      headerData={{
        cells: [
          { key: 'cell-0', id: 'toggle-0', children: 'Column 0' },
          { key: 'cell-1', id: 'toggle-1', children: 'Column 1' },
          { key: 'cell-2', id: 'toggle-2', children: 'Column 2' },
        ],
      }}
      bodyData={mockData.map(section => createSection(section))}
    />
  );
};
