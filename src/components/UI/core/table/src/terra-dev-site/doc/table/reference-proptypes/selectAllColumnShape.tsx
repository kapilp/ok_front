import { JSX, Component, mergeProps, Show, splitProps, useContext } from 'solid-js';
import { createStore } from 'solid-js/store';
/**
 * The props table parser continues to have trouble detecting the propTypes of a component that
 * returns `null`. I've duplicated the propType definition here so that a table could be parsed for it.
 */
interface Properties {
  /**
   * The status of the select all checkbox.
   */
  checkStatus: PropTypes.oneOf(["empty", "checked", "indeterminate"]),
  /**
   * The alignment prop sets the bottom spacing of the check mar, standard units are valid. This is used when providing your own padding.
   */
  checkAlignment: string,
  /**
   * The text label for the column header's interaction.
   */
  checkLabel: string.isRequired,
  /**
   * The function callback triggering when the checkbox within the column header has an interaction.
   */
  onCheckAction: PropTypes.func,
  /**
   * Whether or not interaction should be disabled.
   */
  isDisabled: boolean
};
const PropTypesExample: Component = ({ ...customProps }) => <div />;
export default PropTypesExample;
