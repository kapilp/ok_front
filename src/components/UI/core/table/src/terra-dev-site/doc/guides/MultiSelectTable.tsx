import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import TableUtils from '../../../TableUtils';
import mockData, { RowDataType } from './mock-data/mock-select';
const maxSectionCount = 3;
const createCell = cell => ({ key: cell.key, children: cell.title });
const createCellsForRow = cells => cells.map(cell => createCell(cell));
export const MultiSelectTable = () => {
  const [state, setState] = createStore({ selectedKeys: [] });
  const handleRowToggle = (event: Event, metaData: {}) => {
    event.preventDefault();
    setState({ selectedKeys: TableUtils.toggleArrayValue(state.selectedKeys, metaData.key) });
  };
  const createRow = (rowData: RowDataType) => ({
    key: rowData.key,
    cells: createCellsForRow(rowData.cells),
    isDisabled: !TableUtils.canToggleArrayValue(maxSectionCount, state.selectedKeys, rowData.key),
    toggleAction: {
      metaData: { key: rowData.key },
      onToggle: handleRowToggle,
      isToggled: createMemo(() => state.selectedKeys.indexOf(rowData.key) >= 0),
      toggleLabel: rowData.toggleText,
    },
  });
  const createRows = (data: {}[]) => data.map(childItem => createRow(childItem));
  return (
    <Table
      summaryId="example-multi-select"
      summary="This table shows an implementation of multiple row selection."
      aria-multiselectable
      rowStyle="toggle"
      numberOfColumns={4}
      cellPaddingStyle="standard"
      dividerStyle="both"
      headerData={{
        selectAllColumn: {
          checkLabel: 'Multi Selection',
        },
        cells: [
          { key: 'cell-0', id: 'toggle-0', children: 'Column 0' },
          { key: 'cell-1', id: 'toggle-1', children: 'Column 1' },
          { key: 'cell-2', id: 'toggle-2', children: 'Column 2' },
          { key: 'cell-3', id: 'toggle-3', children: 'Column 3' },
        ],
      }}
      bodyData={[
        {
          rows: createRows(mockData),
        },
      ]}
    />
  );
};
