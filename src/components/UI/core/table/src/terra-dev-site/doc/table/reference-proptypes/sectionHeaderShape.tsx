import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
/**
 * The props table parser continues to have trouble detecting the propTypes of a component that
 * returns `null`. I've duplicated the propType definition here so that a table could be parsed for it.
 */
interface Properties {
  /**
   * The react key to apply to the section header.
   */
  key: string.isRequired;
  /**
   * The id to apply to the header in order to provide structure for assistive technologies.
   */
  id: string.isRequired;
  /**
   * Whether or not the section is collapsed.
   */
  isCollapsed: boolean;
  /**
   * Whether or not the section can be collapsed.
   */
  isCollapsible: boolean;
  /**
   * The associated metaData to be provided in the onToggle(event, metaData) callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData: PropTypes.object;
  /**
   * Function callback for when the appropriate click or key action is performed to expand or collapse the section.
   * Callback contains the javascript event and prop metadata, e.g. onToggle(event, metaData)
   * If present, will enable the interaction of the header.
   */
  onToggle: PropTypes.func;
  /**
   * Function callback pass-through for the ref of the section header.
   */
  ref: PropTypes.func;
  /**
   * Title text to be placed within the section header.
   */
  title: string.isRequired;
  /**
   * Additional attributes to be passed to the section header.
   */
  // eslint-disable-next-line react/forbid-prop-types
  attrs: PropTypes.object;
}
const PropTypesExample: Component = ({ ...customProps }) => <div />;
export default PropTypesExample;
