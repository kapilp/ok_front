import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { ContentContainer } from '../../content-container/src/ContentContainer';
import { VisuallyHiddenText } from '../../visually-hidden-text/src/VisuallyHiddenText';
import { sectionShape } from './proptypes/sectionShape';
import { headerShape } from './proptypes/headerShape';
import { widthShape } from './proptypes/widthShape';
import { Row } from './subcomponents/_Row';
import { Cell } from './subcomponents/_Cell';
import { Section } from './subcomponents/_Section';
import { HeaderRow } from './subcomponents/_HeaderRow';
import { HeaderCell } from './subcomponents/_HeaderCell';
import { ChevronCell } from './subcomponents/_ChevronCell';
import { CheckMarkCell } from './subcomponents/_CheckMarkCell';
import { HeaderChevronCell } from './subcomponents/_HeaderChevronCell';
import { HeaderCheckMarkCell } from './subcomponents/_HeaderCheckMarkCell';
import { isFunction } from 'ramda-adjunct';
import styles from './Table.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * An array of sections containing rows.
   */
  bodyData?: sectionShape[];
  /**
   * The check mark styling to apply.
   */
  checkStyle?: 'icon' | 'toggle';
  /**
   * The divider styling to apply to the child rows.
   */
  dividerStyle?: 'vertical' | 'horizontal' | 'both';
  /**
   * The width value structures associated to each column.
   */
  columnWidths?: widthShape[];
  /**
   * Whether or not the rows should have chevrons applied.
   */
  hasChevrons?: boolean;
  /**
   * The data to build header cells and columns.
   */
  headerData?: headerShape;
  /**
   * Element to append to the top of the table. i.e. toolbars etc.
   */
  headerNode?: JSX.Element;
  /**
   * Whether or not the table should expanded to fill its parent element.
   */
  fill?: boolean;
  /**
   * Element to append to the bottom of the table. i.e. toolbars etc.
   */
  footerNode?: JSX.Element;
  /**
   * The numberOfColumns to be used as a descriptor for assistive technology.
   */
  numberOfColumns: number;
  /**
   * This value is used for accessibility when paged/virtualized rows are used.
   * By default this value is derived from the number of rows passed within the section.
   */
  numberOfRows?: number;
  /**
   * The padding styling to apply to the cell content.
   */
  cellPaddingStyle?: 'standard' | 'compact';
  /**
   * The interaction styling to apply to the row.
   * `'toggle'` relates to the toggling of state as a means of input. `'disclose'` relates to the presentation or disclosure of another component.
   * Both variants can ultimately display as "selected", but the interaction and structure are different for accessibility.
   */
  rowStyle?: 'disclose' | 'toggle';
  /**
   * Function callback returning the html node of the table's inner body element.
   */
  ref?: () => void;
  /**
   * Whether or not a display only footer should be affixed to the table.
   */
  showSimpleFooter?: boolean;
  /**
   * The summary text to describe the table's content and interactions.
   */
  summary: string;
  /**
   * The element id to associate to the descriptive text.
   */
  summaryId: string;
}
const defaultProps = {
  fill: false,
  showSimpleFooter: false,
};
const createCell = (cell, sectionId, columnId, colWidth, discloseData) => (
  <Cell
    {...(cell.attrs ?? {})}
    // The headers attribute is a string that gives the cell a column reading order.
    headers={sectionId && columnId ? [sectionId, columnId].join(' ') : sectionId || columnId}
    key={cell.key}
    ref={cell.ref}
    removeInner={cell.removeInner}
    width={colWidth}
    disclosureData={discloseData}
  >
    {cell.children}
  </Cell>
);
const createCheckCell = (rowData, rowStyle, checkStyle) => {
  // Check style takes priority over the row styling. If a check is set to toggle or icon we know that it is face up.
  if (checkStyle === 'toggle' || checkStyle === 'icon') {
    return (
      <CheckMarkCell
        alignmentPadding={rowData.checkAlignment}
        metaData={rowData.toggleAction && rowData.toggleAction.metaData}
        onSelect={rowData.toggleAction && rowData.toggleAction.onToggle}
        label={rowData.toggleAction && rowData.toggleAction.toggleLabel}
        isSelectable={checkStyle === 'toggle'}
        isSelected={rowData.toggleAction && SignalHelper(rowData.toggleAction.isToggled)}
        isDisabled={rowData.isDisabled}
        isIcon={checkStyle === 'icon'}
      />
    );
  }
  // When the rowstyle is toggle we still to create a checkmark, but a hidden one.
  // This allows someone with a screenreader to view selection
  if (rowStyle === 'toggle') {
    return (
      <CheckMarkCell
        metaData={rowData.toggleAction && rowData.toggleAction.metaData}
        onSelect={rowData.toggleAction && rowData.toggleAction.onToggle}
        label={rowData.toggleAction && rowData.toggleAction.toggleLabel}
        isSelected={rowData.toggleAction && SignalHelper(rowData.toggleAction.isToggled)}
        isHidden
        isDisabled={rowData.isDisabled}
      />
    );
  }
  return undefined;
};
const createChevronCell = (rowStyle, hasChevrons) => {
  if (rowStyle === 'disclose' && hasChevrons) {
    return <ChevronCell />;
  }
  return undefined;
};
const createHeaderCheckCell = (columnData, rowStyle, checkStyle) => {
  let cellAlignment;
  let cellOnAction;
  let cellStatus;
  let cellLabel;
  let cellDisabled;
  if (columnData) {
    cellAlignment = columnData.checkAlignment;
    cellOnAction = columnData.onCheckAction;
    cellStatus = columnData.checkStatus;
    cellLabel = columnData.checkLabel;
    cellDisabled = columnData.isDisabled;
  }
  // Check style takes priority over the row styling. If a check is set to toggle or icon we know that it is face up.
  if (checkStyle === 'toggle' || checkStyle === 'icon') {
    return (
      <HeaderCheckMarkCell
        alignmentPadding={cellAlignment}
        isSelectable={checkStyle === 'toggle' && !!cellOnAction}
        isSelected={cellStatus === 'checked' || cellStatus === 'indeterminate'}
        isIndeterminate={cellStatus === 'indeterminate'}
        isDisabled={cellDisabled}
        onSelect={cellOnAction}
        label={cellLabel}
      />
    );
  }
  // When the row style is toggle we still to create a check mark, but a hidden one.
  // This allows someone with a screen reader to view selection
  if (rowStyle === 'toggle') {
    return (
      <HeaderCheckMarkCell
        label={cellLabel}
        isHidden
        isDisabled={cellDisabled}
        isSelected={cellStatus === 'checked' || cellStatus === 'indeterminate'}
        isIndeterminate={cellStatus === 'indeterminate'}
      />
    );
  }
  return undefined;
};
const createHeaderChevronCell = (rowStyle, hasChevrons) => {
  if (rowStyle === 'disclose' && hasChevrons) {
    return <HeaderChevronCell />;
  }
  return undefined;
};
const SignalHelper = v => (isFunction(v) ? v() : v);
const createRow = (tableData, rowData, rowIndex, sectionId) => {
  let rowMetaData;
  let rowOnAction;
  let rowActiveState = () => false;
  let primaryData;
  let primaryIndex;

  if (tableData.rowStyle === 'disclose' && rowData.discloseAction) {
    rowMetaData = rowData.discloseAction.metaData;
    rowOnAction = rowData.discloseAction.onDisclose; // The disclosure action will trigger from the entire row.
    rowActiveState = createMemo(() => rowData.discloseAction.isDisclosed); // Disclosure will show row selection, but only the link will show to a screen reader as current.
    primaryIndex = rowData.discloseAction.discloseCellIndex; // The index of the cell that will be converted to a link for disclosure.
    primaryData = {
      label: rowData.discloseAction.discloseLabel,
      isCurrent: rowData.discloseAction.isDisclosed,
    };
  } else if (tableData.checkStyle === 'toggle' && rowData.toggleAction) {
    // If check style is present the row should also be an actionable item, but only trigger the check mark selection state.
    rowMetaData = rowData.toggleAction.metaData;
    rowOnAction = rowData.toggleAction.onToggle;
  } else if (tableData.rowStyle === 'toggle' && rowData.toggleAction) {
    rowMetaData = rowData.toggleAction.metaData;
    rowOnAction = rowData.toggleAction.onToggle;
    // We only want to enable a selected state is check style isn't icon.
    // If icon a check mark is displayed to show selection rather than row highlight.
    rowActiveState = createMemo(() => tableData.checkStyle !== 'icon' && SignalHelper(rowData.toggleAction.isToggled));
  }
  return (
    <Row
      {...(rowData.attrs ?? {})}
      key={rowData.key}
      aria-rowindex={rowData.index || rowIndex}
      metaData={rowMetaData}
      isSelectable={tableData.rowStyle === 'toggle' || tableData.rowStyle === 'disclose' || tableData.checkStyle === 'toggle'}
      isSelected={rowActiveState()}
      onSelect={rowOnAction}
      isDisabled={rowData.isDisabled}
      isStriped={rowData.isStriped}
      dividerStyle={tableData.dividerStyle}
      ref={rowData.ref}
    >
      {createCheckCell(rowData, tableData.rowStyle, tableData.checkStyle)}
      {rowData.cells.map((cell, colIndex) => {
        const columnId = tableData.headerData && tableData.headerData.cells ? tableData.headerData.cells[colIndex].id : undefined;
        const columnWidth = tableData.columnWidths ? tableData.columnWidths[colIndex] : undefined;
        const discloseData = colIndex === primaryIndex ? primaryData : undefined;
        return createCell(cell, sectionId, columnId, columnWidth, discloseData);
      })}
      {createChevronCell(tableData.rowStyle, tableData.hasChevrons)}
    </Row>
  );
};
const createSections = (tableData, headerIndex) => {
  if (!tableData.bodyData) {
    return { sections: undefined, sectionIndex: headerIndex };
  }
  let rowIndex = headerIndex;
  const sections = tableData.bodyData.map(section => {
    if (section.sectionHeader) {
      const header = section.sectionHeader;
      rowIndex += 1;
      return (
        <Section
          id={header.id}
          key={header.key}
          aria-rowindex={header.index || rowIndex}
          title={header.title}
          isCollapsed={SignalHelper(header.isCollapsed)}
          isCollapsible={!!header.onToggle}
          metaData={header.metaData}
          numberOfColumns={tableData.checkStyle !== 'toggle' && tableData.rowStyle === 'toggle' ? tableData.numberOfColumns + 1 : tableData.numberOfColumns}
          onSelect={header.onToggle}
        >
          {section.rows
            ? section.rows.map(rowData => {
                rowIndex += 1;
                return createRow(tableData, rowData, rowIndex, header.id);
              })
            : undefined}
        </Section>
      );
    }
    if (section.rows) {
      return section.rows.map(rowData => {
        rowIndex += 1;
        return createRow(tableData, rowData, rowIndex, null);
      });
    }
    return undefined;
  });
  return { sections, sectionIndex: rowIndex };
};
const createHeader = tableData => {
  if (!tableData.headerData || !tableData.headerData.cells) {
    return { headerIndex: 0, header: undefined };
  }
  return {
    headerIndex: 1,
    header: (
      <HeaderRow
        aria-rowindex={1} // Row count begins with the header.
      >
        {createHeaderCheckCell(tableData.headerData.selectAllColumn, tableData.rowStyle, tableData.checkStyle)}
        {tableData.headerData.cells.map((cellData, colIndex) => (
          <HeaderCell
            {...(cellData.attrs ?? {})}
            key={cellData.key}
            ref={cellData.ref}
            metaData={cellData.metaData}
            isSortDesc={SignalHelper(cellData.isSortDesc)}
            isSortActive={SignalHelper(cellData.isSortActive)}
            onCellAction={cellData.onCellAction}
            onSortAction={cellData.onSortAction}
            removeInner={cellData.removeInner}
            width={tableData.columnWidths ? tableData.columnWidths[colIndex] : undefined}
          >
            {cellData.children}
          </HeaderCell>
        ))}
        {createHeaderChevronCell(tableData.rowStyle, tableData.hasChevrons)}
      </HeaderRow>
    ),
  };
};
const unpackTableData = tableData => {
  const { headerIndex, header } = createHeader(tableData);
  const { sectionIndex, sections } = createSections(tableData, headerIndex);
  return { rowCount: sectionIndex, header, sections };
};

export const Table = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'dividerStyle',
    'hasChevrons',
    'rowStyle',
    'checkStyle',
    'columnWidths',
    'headerData',
    'bodyData',
    'fill',
    'footerNode',
    'headerNode',
    'cellPaddingStyle',
    'numberOfColumns',
    'numberOfRows',
    'ref',
    'showSimpleFooter',
    'summary',
    'summaryId',
    'className',
  ]);
  const theme = useContext(ThemeContext);
  // If all column widths are using static sizing alter the table style to display inline.
  const makeInline = createMemo(() => (p.columnWidths && p.columnWidths.length ? p.columnWidths.every(width => !!width.static) : undefined));
  const hasEndNodes = createMemo(() => p.headerNode || p.footerNode || p.showSimpleFooter);
  const tableClasses = createMemo(() => classNames(cx('table', { fill: p.fill }, { 'is-inline': makeInline() }, { outer: !hasEndNodes() }, theme.className), p.className));

  const tableData = {
    headerData: p.headerData,
    bodyData: p.bodyData,
    columnWidths: p.columnWidths,
    rowStyle: p.rowStyle,
    checkStyle: p.checkStyle,
    hasChevrons: p.hasChevrons,
    dividerStyle: p.dividerStyle,
    numberOfColumns: p.numberOfColumns,
  };
  const { rowCount, header, sections } = unpackTableData(tableData);
  const attrSpread = createMemo(() => (p.cellPaddingStyle ? { 'data-table-padding': p.cellPaddingStyle } : {}));
  const rows = (
    <div {...{ ...customProps, ...attrSpread() }} className={tableClasses()} role="grid" aria-rowcount={p.numberOfRows || rowCount} aria-describedby={p.summaryId}>
      <VisuallyHiddenText id={p.summaryId} text={p.summary} />
      {header}
      {sections ? (
        /* eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex */
        <div className={cx(['body'])} role="rowgroup" ref={p?.ref} tabIndex={p.fill ? 0 : undefined}>
          {sections}
        </div>
      ) : undefined}
    </div>
  );

  const footerElement = createMemo(() => {
    const footerElement = [];
    if (p.footerNode) {
      footerElement.push(p.footerNode);
    }
    if (p.showSimpleFooter) {
      footerElement.push(<div className={cx('simple-footer')} />);
    }
    return footerElement;
  });

  return (
    <>
      {!hasEndNodes() ? (
        rows
      ) : (
        <ContentContainer fill={p.fill} footer={footerElement()} header={p.headerNode} className={cx('outer')}>
          {rows}
        </ContentContainer>
      )}
    </>
  );
};
