export type cellShape ={
  /**
   * The react key to apply to the cell.
   */
  key: string
  /**
   * Child content to be displayed for the row cell.
   */
  children?: JSX.Element,
  /**
   * Function callback returning the html node for the cell.
   */
  ref?: (el:HTMLElement)=>void,
  /**
   * Whether or not the cell's inner containing element responsible for handling table's default padding is removed.
   * This is useful to optimize the DOM for either a table without padding or to optimize a cell whose custom content is providing its own padding.
   */
  removeInner?: boolean,
  /**
   * Additional attributes to be passed to the cell.
   */
  // eslint-disable-next-line react/forbid-prop-types
  attrs?: {}
});
