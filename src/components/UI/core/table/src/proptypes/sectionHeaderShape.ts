export type sectionHeaderShape = {
  /**
   * The react key to apply to the section header.
   */
  key: string;
  /**
   * The id to apply to the header in order to provide structure for assistive technologies.
   */
  id: string;
  /**
   * Whether or not the section is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed to expand or collapse the section.
   * Callback contains the javascript event and prop metadata, e.g. onToggle(event, metaData)
   * If present, will enable the interaction of the header.
   */
  onToggle?: () => void;
  /**
   * Function callback pass-through for the ref of the section header.
   */
  ref?: () => void;
  /**
   * Title text to be placed within the section header.
   */
  title: string;
  /**
   * Additional attributes to be passed to the section header.
   */
  // eslint-disable-next-line react/forbid-prop-types
  attrs?: object;
};
