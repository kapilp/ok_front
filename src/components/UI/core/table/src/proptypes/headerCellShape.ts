export type headerCellShape = {
  /**
   * The id of the header cell for the row cells to set within their "headers" prop.
   */
  id: string;
  /**
   * The react key to apply to the cell.
   */
  key: string;
  /**
   * Content to be displayed for the column header.
   */
  children?: JSX.Element;
  /**
   * Whether or not the sort indicator is descending.
   */
  isSortDesc?: boolean;
  /**
   * Whether or not the column is to be marked as a sorted column.
   */
  isSortActive?: boolean;
  /**
   * The associated metaData to be provided in the onCellAction and onSortAction callbacks.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onCellAction(event, metaData)
   * The presence of this func will indicate that the cell can be interacted with for actions or selections.
   */
  onCellAction?: () => void;
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSortAction(event, metaData)
   * The presence of this func will indicate that the cell can be interacted with for sorting.
   */
  onSortAction?: () => void;
  /**
   * Function callback returning the html node for the header cell.
   */
  ref?: () => void;
  /**
   * Whether or not the cell's inner containing element responsible for handling table's default padding is removed.
   * This is useful to optimize the DOM for either a table without padding or to optimize a cell whose custom content is providing its own padding.
   */
  removeInner?: boolean;
  /**
   * Additional attributes to be passed to the cell.
   */
  // eslint-disable-next-line react/forbid-prop-types
  attrs?: object;
};
