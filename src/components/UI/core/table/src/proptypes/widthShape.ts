export type widthShape = {
  /**
   * Static width that for the cell.
   */
  static?: {
    /**
     * Numerical width value.
     */
    value: number;
    /**
     * Valid css units are supported (i.e. 'px', 'rem', etc).
     */
    unit: string;
  };
  /**
   * Percentage width of the row for the header cell.
   */
  percentage?: number;
  /**
   * Relative scalar value of the cell's width compared to its sibling cells.
   */
  scalar?: number;
};
