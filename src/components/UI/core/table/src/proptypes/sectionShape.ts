import { rowShape } from './rowShape';
import { sectionHeaderShape } from './sectionHeaderShape';
export type sectionShape = {
  /**
   * The children list items passed to the component.
   */
  rows?: rowShape[];
  /**
   * The section header of the provided rows.
   */
  sectionHeader?: sectionHeaderShape;
};
