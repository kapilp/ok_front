export type selectAllColumnShape = {
  /**
   * The status of the select all checkbox.
   */
  checkStatus?: 'empty' | 'checked' | 'indeterminate';
  /**
   * The alignment prop sets the bottom spacing of the check mar, standard units are valid. This is used when providing your own padding.
   */
  checkAlignment?: string;
  /**
   * The text label for the column header's interaction.
   */
  checkLabel: string;
  /**
   * The function callback triggering when the checkbox within the column header has an interaction.
   */
  onCheckAction?: () => void;
  /**
   * Whether or not interaction should be disabled.
   */
  isDisabled?: boolean;
};
