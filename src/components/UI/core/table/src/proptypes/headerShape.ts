import {headerCellShape} from "./headerCellShape";
import {selectAllColumnShape} from "./selectAllColumnShape";
export  type headerShape = {
  /**
   * The cells to be displayed within the header.
   */
  cells?: headerCellShape[],
  /**
   * Function callback returning the html node for the header.
   */
  ref?: (el:HTMLElement)=>void,
  /**
   * The select all column header's properties.
   */
  selectAllColumn?: selectAllColumnShape
});
