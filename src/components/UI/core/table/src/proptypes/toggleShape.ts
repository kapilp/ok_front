export type toggleShape = {
  /**
   * The label associated to the toggle action for the row.
   */
  toggleLabel: string;
  /**
   * Whether or not the row is in a togged state, this covers both `'checkStyle'` and `'rowStyle'` toggle states.
   */
  isToggled?: boolean;
  /**
   * The associated metaData to be return within row toggle callbacks.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onCheckAction(event, metaData)
   */
  onToggle?: () => void;
};
