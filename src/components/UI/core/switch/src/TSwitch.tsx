import { JSX, Component, mergeProps, Show, splitProps, useContext, createSignal } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
//import { FormattedMessage } from "react-intl";
import { KEY_SPACE, KEY_RETURN } from 'keycode-js';
import { removeFocusStyles, restoreFocusStyles } from './_SwitchUtils';
import styles from './Switch.module.scss';
import { useCallback, useRef } from 'react';
const cx = classNamesBind.bind(styles);
const SWITCH_STATE = Object.freeze({
  ON: 'On',
  OFF: 'Off',
});
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Whether or not the Switch is checked ("ON").
   */
  isChecked?: boolean;
  /**
   * Whether or not the Switch is disabled.
   */
  isDisabled?: boolean;
  /**
   * The label text of the Switch component.
   */
  labelText: string;
  /**
   * Callback function when switch value changes from ON / OFF.
   * Returns Parameters: 1. switch value 2. event.
   */
  onChange: (isChecked: boolean, event: Event) => void;
}
const defaultProps = {
  isChecked: false,
  isDisabled: false,
  onChange: undefined,
};
export const TSwitch = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['isChecked', 'isDisabled', 'onChange', 'labelText']);

  const theme = useContext(ThemeContext);
  const [sliderButton, setSliderButton] = createSignal<HTMLButtonElement>();
  const handleOnClick = (event: MouseEvent) => {
    // Need this for focus styles on IE running on OS X since it does not receive focus when clicked.
    sliderButton().focus();
    if (p.onChange) {
      p.onChange(!p.isChecked, event);
    }
  };
  const handleOnKeyDown = event => {
    if (event.keyCode === KEY_RETURN || event.keyCode === KEY_SPACE) {
      event.preventDefault();
      restoreFocusStyles(sliderButton());
      if (p.onChange) {
        p.onChange(!p.isChecked, event);
      }
    }
  };
  const handleOnMouseDown = event => {
    event.preventDefault();
    removeFocusStyles(sliderButton());
  };
  const handleOnBlur = () => {
    restoreFocusStyles(sliderButton());
  };
  const statusLabelText = () => (p.isChecked ? SWITCH_STATE.ON : SWITCH_STATE.OFF);
  const switchClassNames = () =>
    classNames(cx('switch', { 'is-enabled': !p.isDisabled }, { 'is-disabled': p.isDisabled }, { 'is-selected': p.isChecked }, theme.className), customProps.className);

  function getSwitchAttrs() {
    let switchAttrs;
    if (!p.isDisabled) {
      switchAttrs = {
        tabIndex: '0',
        onBlur: handleOnBlur,
        onClick: handleOnClick,
        onMouseDown: handleOnMouseDown,
        onKeyDown: handleOnKeyDown,
      };
    } else {
      switchAttrs = {
        'aria-disabled': true,
      };
    }
    return switchAttrs;
  }

  return (
    <div
      {...customProps}
      {...getSwitchAttrs()}
      aria-label={p.labelText}
      aria-checked={p.isChecked}
      role="switch"
      className={switchClassNames()}
      data-terra-switch-show-focus-styles
      ref={setSliderButton}
    >
      <div aria-hidden className={cx('label-container')}>
        <div className={cx('label-text')}>{p.labelText}</div>
        <div className={cx('status-label-text')}>{statusLabelText()}</div>
      </div>
      <div aria-hidden className={cx('tray')}>
        <div className={cx('slider')} />
      </div>
    </div>
  );
};
