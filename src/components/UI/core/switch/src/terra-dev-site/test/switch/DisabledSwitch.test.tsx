import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Switch from '../../../TSwitch';
const DisabledSwitch = () => <Switch id="disabledSwitch" isChecked labelText="Label" isDisabled />;
export default DisabledSwitch;
