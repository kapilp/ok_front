import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Switch from '../../../TSwitch';
const DefaultSwitch = () => {
  const [value, setValue] = useState(false);
  return <Switch id="defaultSwitch" isChecked={value} labelText="Label" onChange={setValue} />;
};
export default DefaultSwitch;
