import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Switch from '../../../TSwitch';
const SwitchWithLongLabelText = () => {
  const [value, setValue] = useState(true);
  return (
    <Switch
      id="switchWithLongLabel"
      isChecked={value}
      labelText="Long Label Text Long Label Text  Long Label Text  Long Label Text  Long Label Text  Long Label Text  Long Label Text  Long Label TextLong Label Text Long Label TextLong Label Text Long Label TextLong Label Text Long Label TextLong Label Text Long Label TextLong Label Text Long Label TextLong Label Text Long Label TextLong Label Text Long Label Text"
      onChange={setValue}
    />
  );
};
export default SwitchWithLongLabelText;
