import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { TSwitch } from '../../../TSwitch';

export const DefaultSwitch = () => <TSwitch labelText="Label" className="default-switch" />;
