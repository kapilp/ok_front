import { JSX, createSignal } from 'solid-js';

import { TSwitch } from '../../../TSwitch';

export const SwitchWithOnChange = () => {
  const [value, setValue] = createSignal(true); // makes switch on by default
  return <TSwitch isChecked={value()} labelText="Label" onChange={setValue} />;
};
