import { JSX, createMemo, mergeProps, splitProps, useContext } from "solid-js";
import { FormattedMessage } from "react-intl";
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import { ThemeContext } from "../../../framework/theme-context/src/ThemeContext";
import { v4 as uuidv4 } from "uuid";
import { VisuallyHiddenText } from "../../visually-hidden-text/src/VisuallyHiddenText";
import styles from "./RadioField.module.scss";
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The form control elements the Field contains.
   */
  children?: JSX.Element;
  /**
   * Error message for when the input is invalid. This will only be displayed if isInvalid is true.
   */
  error?: JSX.Element;
  /**
   * Help element to display with the input.
   */
  help?: JSX.Element;
  /**
   * Whether or not to hide the required indicator on the legend.
   */
  hideRequired?: boolean;
  /**
   * Whether or not the field is an inline field.
   */
  isInline?: boolean;
  /**
   * Whether the field displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Whether or not the legend is visible. Use this props to hide a legend while still creating it on the DOM for accessibility.
   */
  isLegendHidden?: boolean;
  /**
   * The legend of the form control children.
   */
  legend: string;
  /**
   * Attributes to attach to the legend.
   */
  // eslint-disable-next-line react/forbid-prop-types
  legendAttrs?: {};
  /**
   * Whether or not the field is required.
   */
  required?: boolean;
  /**
   * Whether or not to append the 'optional' legend to a non-required field legend.
   */
  showOptional?: boolean;
}
const defaultProps = {
  children: null,
  error: null,
  help: null,
  hideRequired: false,
  isInline: false,
  isInvalid: false,
  isLegendHidden: false,
  legendAttrs: {},
  required: false,
  showOptional: false,
};
export const RadioField = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [p, customProps] = splitProps(props, [
    "children",
    "error",
    "help",
    "hideRequired",
    "isInvalid",
    "isInline",
    "isLegendHidden",
    "legend",
    "legendAttrs",
    "required",
    "showOptional",
    "className",
  ]);

  const radioFieldClasses = createMemo(() => classNames(cx("radio-field", { "is-inline": p.isInline }, theme.className), p.className));
  const legendClassNames = createMemo(() => cx(["legend", p.legendAttrs.className]));
  const legendAriaDescriptionId = `terra-radio-field-description-${uuidv4()}`;
  const helpAriaDescriptionId = createMemo(() => (p.help ? `terra-radio-field-description-help-${uuidv4()}` : ""));
  const errorAriaDescriptionId = createMemo(() => (p.error ? `terra-radio-field-description-error-${uuidv4()}` : ""));
  const ariaDescriptionIds = createMemo(() => `${legendAriaDescriptionId} ${errorAriaDescriptionId()} ${helpAriaDescriptionId()}`);
  const legendGroup = (
    <legend id={legendAriaDescriptionId} className={cx(["legend-group", { "legend-group-hidden": p.isLegendHidden }])}>
      <div {...p.legendAttrs} className={legendClassNames()}>
        {p.isInvalid && <span className={cx("error-icon")} />}
        {p.required && (p.isInvalid || !p.hideRequired) && (
          <>
            <div aria-hidden="true" className={cx("required")}>
              *
            </div>
            <VisuallyHiddenText text={"Required"} />
          </>
        )}
        {p.legend}
        {p.required && !p.isInvalid && p.hideRequired && <span className={cx("required-hidden")}>*</span>}
        {p.showOptional && !p.required && <span className={cx("optional")}>{"(optional)"}</span>}
        {!p.isInvalid && <span className={cx("error-icon-hidden")} />}
      </div>
    </legend>
  );
  // Todo fix this:
  /*const content = React.Children.map(p.children, child => {
    if (child && child.type.isRadio) {
      return React.cloneElement(child, {
        inputAttrs: { 'aria-describedby': ariaDescriptionIds() },
      });
    }
    return child;
  });*/
  return (
    <fieldset {...customProps} aria-required={p.required} required={p.required} className={radioFieldClasses()}>
      {legendGroup}
      {/*content*/ p.children}
      {p.isInvalid && p.error && (
        <div id={errorAriaDescriptionId()} className={cx("error-text")}>
          {p.error}
        </div>
      )}
      {p.help && (
        <div id={helpAriaDescriptionId()} className={cx("help-text")}>
          {p.help}
        </div>
      )}
    </fieldset>
  );
};
