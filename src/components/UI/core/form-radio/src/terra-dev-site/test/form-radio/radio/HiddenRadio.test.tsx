import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Radio from '../../../../Radio';
export default () => <Radio id="hidden" labelText="can you see me?" isLabelHidden />;
