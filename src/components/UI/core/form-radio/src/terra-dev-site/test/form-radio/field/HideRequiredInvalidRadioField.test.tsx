import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Radio from '../../../../Radio';
import RadioField from '../../../../RadioField';
type undefinedState = {
  selectedAnswer?: any;
};
export default class extends React.Component<{}, undefinedState> {
  constructor(props) {
    super(props);
    state = {};
    handleOnChange = handleOnChange.bind(this);
  }
  handleOnChange(e) {
    setState({ selectedAnswer: e.currentTarget.value });
  }
  render() {
    return (
      <RadioField
        id="testing-radio-field"
        legend="Which Department do you work for?"
        help="Your work falls inline with one of these departments"
        isInvalid={state.selectedAnswer === undefined}
        error="One must be selected"
        hideRequired
        isInline
        required
      >
        <Radio id="ux-dept" name="dept" labelText="UX/Interaction Design" onChange={handleOnChange} value="ux" />
        <Radio id="magazine-dept" name="dept" labelText="Magazine Advertisements" onChange={handleOnChange} value="magazine" />
        <Radio id="website-dept" name="dept" labelText="Website Advertisements" onChange={handleOnChange} value="website" />
        <Radio id="events-dept" name="dept" labelText="Event Promotions" onChange={handleOnChange} value="events" />
      </RadioField>
    );
  }
}
