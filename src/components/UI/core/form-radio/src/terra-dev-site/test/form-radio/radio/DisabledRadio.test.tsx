import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Radio from '../../../../Radio';
export default () => (
  <div>
    <Radio id="disabled" labelText="Disabled Radio" disabled />
    <Radio id="disabledchecked" labelText="Disabled and Checked Radio" defaultChecked disabled />
  </div>
);
