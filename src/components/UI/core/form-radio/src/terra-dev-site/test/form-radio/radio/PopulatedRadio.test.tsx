import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Radio from '../../../../Radio';
const radio = () => (
  <Radio
    id="populated"
    labelText="Do you have any Children?"
    labelTextAttrs={{ className: 'healtheintent-control-label-text' }}
    name="children-present"
    value="children-present"
    inputAttrs={{ className: 'healtheintent-application' }}
    isInline
  />
);
export default radio;
