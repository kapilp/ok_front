import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
export const MultipleRadiosExample = () => (
  <div>
    <Radio id="first" labelText="First Radio" name="multiple-group" checked />
    <Radio id="second" labelText="Second Radio" name="multiple-group" />
    <Radio id="third" labelText="Third Radio" name="multiple-group" />
  </div>
);
