import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';

export const DefaultRadioExample = () => <Radio id="default-radio" labelText="Default Radio" name="default" />;
