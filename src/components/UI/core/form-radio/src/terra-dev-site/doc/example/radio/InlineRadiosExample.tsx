import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
export const InlineRadiosExample = () => (
  <div>
    <Radio id="first-inline" labelText="First Radio" name="inline-example" isInline />
    <Radio id="second-inline" labelText="Second Radio" name="inline-example" isInline />
    <Radio id="third-inline" labelText="Third Radio" name="inline-example" isInline />
  </div>
);
