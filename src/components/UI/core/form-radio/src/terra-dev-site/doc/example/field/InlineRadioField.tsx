import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
import { RadioField } from '../../../../RadioField';
import classNames from 'classnames/bind';
import styles from './RadioFieldCommon.module.scss';
const cx = classNames.bind(styles);
export const InlinedRadioFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
    selectedAnswer: undefined,
    toggleInline: false,
  });
  const handleOnChange = e => {
    setState({ selectedAnswer: e.currentTarget.value });
  };
  const handleOnClick = e => {
    if (e.currentTarget.id === 'inline') {
      setState({
        toggleInline: !state.toggleInline,
      });
    } else {
      setState({
        isInvalid: !state.isInvalid,
      });
    }
  };

  const getMessageAndIsInvalid = createMemo(() => {
    let errorMessage = 'An answer must be chosen';
    let isInvalid;
    if (state.isInvalid) {
      errorMessage = 'All options are now invalid';
    } else if (state.selectedAnswer === 'mcdonalds') {
      errorMessage = 'Invalid option selected.';
      isInvalid = true;
    } else if (state.selectedAnswer === undefined) {
      isInvalid = true;
    }
    return { errorMessage, isInvalid };
  });

  return (
    <div>
      <button className={cx('radio-button-wrapper')} id="inline" type="button" aria-label="Toggle Inline" onClick={handleOnClick}>
        Toggle Inline
      </button>
      <button className={cx('radio-button-wrapper')} id="invalid" type="button" aria-label="Toggle Inline" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
      <hr />
      <div>
        <RadioField
          legend="Which Type of Meal are you looking for?"
          help="This cannot be changed when submitted"
          isInline={state.toggleInline}
          isInvalid={state.isInvalid || getMessageAndIsInvalid().isInvalid}
          error={getMessageAndIsInvalid().errorMessage}
          required
        >
          <Radio id="inline-chicken-meal" name="meal" labelText="Chicken" onChange={handleOnChange} value="chicken" />
          <Radio id="inline-salmon-meal" name="meal" labelText="Salmon" onChange={handleOnChange} value="salmon" />
          <Radio id="inline-mcdonalds-meal" name="meal" labelText="McDonalds (Not a valid choice)" onChange={handleOnChange} value="mcdonalds" />
        </RadioField>
        <RadioField
          error={getMessageAndIsInvalid().errorMessage}
          legend="Which Type of Sides do you want?"
          help="This cannot be changed when submitted"
          isInline={state.toggleInline}
          isInvalid={state.isInvalid}
          required
        >
          <Radio id="fries" name="side" labelText="Fries" value="fries" />
          <Radio id="fruit" name="side" labelText="Fruit" value="fruit" />
          <Radio id="salad" name="side" labelText="Salad" value="salad" />
        </RadioField>
      </div>
    </div>
  );
};
