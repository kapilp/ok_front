import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
import { RadioField } from '../../../../RadioField';

export const DefaultRadioFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
  });
  const handleOnClick = () => {
    setState({ isInvalid: !state.isInvalid });
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <div>
        <RadioField error={errorMessage} isInvalid={state.isInvalid} legend="Which Type of Account are you looking for?">
          <Radio id="user-account" name="account" labelText="Base User" value="user" />
          <Radio id="team-account" name="account" labelText="Team Owner" value="team" />
          <Radio id="admin-account" name="account" labelText="Administrator" value="admin" />
        </RadioField>
      </div>
      <hr />
      <button type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
