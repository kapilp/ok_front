import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
import { RadioField } from '../../../../RadioField';

export const OptionalRadioFieldExample = () => {
  const [state, setState] = createStore({
    isInvalid: false,
  });
  const handleOnClick = () => {
    setState({ isInvalid: !state.isInvalid });
  };

  const errorMessage = 'All options are now invalid';
  return (
    <div>
      <div>
        <RadioField error={errorMessage} isInvalid={state.isInvalid} legend="Which Track are you looking for?" help="This cannot be changed when submitted" showOptional>
          <Radio id="frontend-track" name="track" labelText="Frontend Development" value="backend" />
          <Radio id="backend-track" name="track" labelText="Backend Development" value="frontend" />
          <Radio id="devops-track" name="track" labelText="DevOps" value="devops" />
        </RadioField>
      </div>
      <hr />
      <button type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
