import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
export const DisabledRadioExample = () => (
  <div>
    <Radio id="disabled-radio" labelText="Disabled Radio" disabled name="disabled" />
    <Radio id="disabled-checked-radio" labelText="Disabled and Checked Radio" checked disabled name="disabled" />
  </div>
);
