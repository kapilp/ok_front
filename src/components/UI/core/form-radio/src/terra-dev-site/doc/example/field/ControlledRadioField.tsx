import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Radio } from '../../../../Radio';
import { RadioField } from '../../../../RadioField';
import classNames from 'classnames/bind';
import styles from './RadioFieldCommon.module.scss';
const cx = classNames.bind(styles);

export const ControlledRadioFieldExample = () => {
  const [state, setState] = createStore({
    selectedAnswer: undefined,
    toggleInvalid: false,
  });
  const handleOnChange = e => {
    setState({ selectedAnswer: e.currentTarget.value });
  };
  const handleOnClick = () => {
    setState({
      toggleInvalid: !state.toggleInvalid,
    });
  };

  let errorMessage = createMemo(() => {
    let errorMessage = state.toggleInvalid ? 'All options are now invalid' : undefined;
    if (state.selectedAnswer === 'monday') {
      errorMessage = 'Invalid option selected.';
    }
    return errorMessage;
  });

  const isInvalid = createMemo(() => {
    let isInvalid = false;
    if (state.selectedAnswer === 'monday') {
      isInvalid = true;
    } else if (state.selectedAnswer === undefined) {
      isInvalid = true;
    }
    return isInvalid;
  });

  return (
    <div>
      <div>
        <RadioField
          legend="What is your favorite day of the week?"
          help="This cannot be changed when submitted"
          isInvalid={state.toggleInvalid || isInvalid()}
          error={errorMessage()}
          required
        >
          <Radio id="sunday" name="weekday" labelText="Sunday" onChange={handleOnChange} value="sunday" />
          <Radio id="monday" name="weekday" labelText="Monday (Not a valid choice)" onChange={handleOnChange} value="monday" />
          <Radio id="tuesday" name="weekday" labelText="Tuesday" onChange={handleOnChange} value="tuesday" />
          <Radio id="wednesday" name="weekday" labelText="Wednesday" onChange={handleOnChange} value="wednesday" />
          <Radio id="thursday" name="weekday" labelText="Thursday" onChange={handleOnChange} value="thursday" />
          <Radio id="friday" name="weekday" labelText="Friday" onChange={handleOnChange} value="friday" />
          <Radio id="saturday" name="weekday" labelText="Saturday" onChange={handleOnChange} value="saturday" />
        </RadioField>
      </div>
      <hr />
      <button className={cx('radio-button-wrapper')} type="button" aria-label="Toggle Invalid Status" onClick={handleOnClick}>
        Toggle Invalid Status
      </button>
    </div>
  );
};
