import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Button } from '../../../../../../button/src/Button';

export const MobileButtonRadioExample = (props: {}) => {
  return <Button id="mobileButton" text="Toggle" onClick={props.onChange} />;
};
