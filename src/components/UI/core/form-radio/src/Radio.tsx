import { JSX, createMemo, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Radio.module.scss';
import RadioUtil from './_RadioUtil';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Whether or not the radio button is checked. Use this to generate a controlled radio button.
   */
  checked?: boolean;
  /**
   * The checked property of the Input element. Use this to generate an uncontrolled radio button.
   */
  // defaultChecked?: boolean; // not available in solid
  /**
   * The id of the radio button.
   */
  id?: string;
  /**
   * Additional attributes for the input object.
   */
  // eslint-disable-next-line react/forbid-prop-types
  inputAttrs?: {};
  /**
   * Whether the radio button is disabled.
   */
  disabled?: boolean;
  /**
   * Whether the radio button is inline.
   */
  isInline?: boolean;
  /**
   * Whether the label is hidden.
   */
  isLabelHidden?: boolean;
  /**
   * Text of the label.
   */
  labelText: string;
  /**
   * Additional attributes for the text object.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelTextAttrs?: {};
  /**
   * Name attribute of the input.
   */
  name?: string;
  /**
   * Function to trigger when focus is lost from the radio button.
   */
  onBlur?: (e: Event) => void;
  /**
   * Function to trigger when user clicks on the radio button. Provide a function to create a controlled input.
   */
  onChange?: (e: Event) => void;
  /**
   *  Function to trigger when user focuses on the radio button.
   */
  onFocus?: (e: Event) => void;
  /**
   * The value of the input element.
   */
  value?: string;
}
const defaultProps = {
  checked: undefined,
  // defaultChecked: undefined,
  id: undefined,
  inputAttrs: {},
  disabled: false,
  isInline: false,
  isLabelHidden: false,
  labelTextAttrs: {},
  name: null,
  onBlur: undefined,
  onChange: undefined,
  onFocus: undefined,
  value: undefined,
};
export const Radio = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'checked',
    'defaultChecked',
    'inputAttrs',
    'id',
    'disabled',
    'isInline',
    'isLabelHidden',
    'labelText',
    'labelTextAttrs',
    'name',
    'onBlur',
    'onChange',
    'onFocus',
    'value',
    'className',
  ]);
  const theme = useContext(ThemeContext);

  /*const controlInputAttrs = { ...p.inputAttrs };
  if (p.checked !== undefined) {
    controlInputAttrs.checked = p.checked;
  } else {
    controlInputAttrs.defaultChecked = p.defaultChecked;
  }*/

  const radioClasses = createMemo(() => classNames(cx('radio', { 'is-inline': p.isInline }, theme.className), p.className));
  const labelClasses = createMemo(() =>
    cx(['label', { 'is-disabled': p.disabled }, { 'is-hidden': p.isLabelHidden }, { 'is-mobile': RadioUtil.isConsideredMobileDevice() }, p.labelTextAttrs.className]),
  );
  const inputClasses = createMemo(() => cx(['native-input', p.inputAttrs.className]));
  const labelTextClasses = cx(['label-text']);
  const outerRingClasses = cx(['outer-ring', { 'is-mobile': RadioUtil.isConsideredMobileDevice() }]);
  const innerRingClasses = cx(['inner-ring']);
  const controlInputAttrs = createMemo(() => (p.isLabelHidden ? { 'aria-label': p.labelText } : {}));
  const labelTextContainer = (
    <>
      {p.isLabelHidden ? (
        <span {...p.labelTextAttrs} className={labelTextClasses} />
      ) : (
        <span {...p.labelTextAttrs} className={labelTextClasses}>
          {p.labelText}
        </span>
      )}
    </>
  );
  return (
    <div {...customProps} className={radioClasses()}>
      <label htmlFor={p.id} className={labelClasses()}>
        <input
          {...controlInputAttrs()}
          checked={p.checked}
          type="radio"
          id={p.id}
          disabled={p.disabled}
          name={p.name}
          value={p.value}
          onChange={p.onChange}
          onFocus={p.onFocus}
          onBlur={p.onBlur}
          className={inputClasses()}
        />
        <span className={outerRingClasses}>
          <span className={innerRingClasses} />
        </span>
        {labelTextContainer}
      </label>
    </div>
  );
};

Radio.isRadio = true;
