import { JSX, mergeProps, Show, splitProps } from 'solid-js';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './ToggleButton.module.scss';
import { SvgIconChevronRight } from '../../icon/src/icon/IconChevronRight';
import { Toggle } from '../../toggle/src/Toggle';
import { Button } from '../../button/src/Button';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * Content in the body of the toggle-button component that will be expanded or toggle-buttond
   */
  children: JSX.Element;
  /**
   * Sets the text inside of the button when the toggle-button component is closed
   */
  closedButtonText: string;
  /**
   * Used to set props and HTML attributes on the toggle-button button
   */
  // eslint-disable-next-line react/forbid-prop-types
  buttonAttrs?: {};
  /**
   * Icon displayed next to text content within the toggle-button button
   */
  icon?: JSX.Element;
  /**
   * Sets the toggle-button to be animated when it is opened or closed
   */
  isAnimated?: boolean;
  /**
   * Used to turn on animation on toggle-button button icon
   */
  isIconAnimated?: boolean;
  /**
   * Sets the toggle-button to only display and icon. Uses closedButtonText prop as an aria-label on the button.
   */
  isIconOnly?: boolean;
  /**
   * Sets the toggle-button initial state to open
   */
  isInitiallyOpen?: boolean;
  /**
   * Callback function triggered when toggle-button is closed
   */
  onClose?: () => void;
  /**
   * Callback function triggered when toggle-button is opened
   */
  onOpen?: () => void;
  /**
   * Sets the text inside of the button when the toggle-button component is open
   */
  openedButtonText?: string;
}
const defaultProps = {
  isAnimated: false,
  isIconAnimated: false,
  isIconOnly: false,
  isInitiallyOpen: false,
  icon: <SvgIconChevronRight />,
};
interface IToggleButtonProps extends JSX.HTMLAttributes<Element> {
  buttonAttrs?: any;
  closedButtonText?: any;
  icon?: any;
  isAnimated?: any;
  isIconAnimated?: any;
  isIconOnly?: any;
  isInitiallyOpen?: any;
  onClose?: any;
  onOpen?: any;
  openedButtonText?: any;
  customProps?: any;
}
type ToggleButtonState = {
  isOpen?: any;
};
export const ToggleButton = (props: IToggleButtonProps) => {
  props = mergeProps({}, defaultProps, props);

  const [state, setState] = createStore({
    isOpen: props.isInitiallyOpen,
  });

  function handleOnClick(e: MouseEvent) {
    e.preventDefault();
    // Fire event from toggle handlers
    if (!state.isOpen && props.onOpen) {
      props.onOpen();
    } else if (state.isOpen && props.onClose) {
      props.onClose();
    }
    setState({ isOpen: !state.isOpen });
  }

  const [extracted, customProps] = splitProps(props, [
    'buttonAttrs',
    'children',
    'closedButtonText',
    'icon',
    'isAnimated',
    'isIconAnimated',
    'isIconOnly',
    'isInitiallyOpen',
    'onClose',
    'onOpen',
    'openedButtonText',
  ]);
  // Set openHeaderText to the same value as closedHeaderText if its not already set

  return (
    <div {...customProps} className={cx(['button', { 'is-open': state.isOpen }, { 'is-icon-animated': extracted.isIconAnimated }, customProps.className])}>
      <Button
        {...(extracted.buttonAttrs ?? {})}
        isIconOnly={extracted.isIconOnly}
        icon={() => <span className={cx('icon')}>{extracted.icon}</span>}
        aria-expanded={state.isOpen}
        text={extracted.isIconOnly ? extracted.closedButtonText : !state.isOpen ? extracted.closedButtonText : extracted.openedButtonText || extracted.closedButtonText}
        onClick={handleOnClick}
      />
      <Toggle isOpen={state.isOpen} isAnimated={props.isAnimated}>
        {props.children}
      </Toggle>
    </div>
  );
};
