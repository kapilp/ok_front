import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './OpenCloseEventToggleButton.module.scss';
import { ToggleButton } from '../../../ToggleButton';
const cx = classNames.bind(styles);
type OpenCloseEventToggleButtonState = {
  timesOpened?: number;
  timesClosed?: number;
};
export const OpenCloseEventToggleButton = (props: {}) => {
  const [state, setState] = createStore({
    timesOpened: 0,
    timesClosed: 0,
  });
  function handleOnOpen() {
    setState({ timesOpened: state.timesOpened + 1 });
  }
  function handleOnClose() {
    setState({ timesClosed: state.timesClosed + 1 });
  }
  return (
    <div>
      <div id="on-open-event">
        <p>
          Times Opened:
          <span className={cx('times-wrapper')}>{state.timesOpened}</span>
        </p>
        <p>
          Times Closed:
          <span className={cx('times-wrapper')}>{state.timesClosed}</span>
        </p>
      </div>
      <ToggleButton closedButtonText="ToggleButton" onOpen={handleOnOpen} onClose={handleOnClose}>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </ToggleButton>
    </div>
  );
};
