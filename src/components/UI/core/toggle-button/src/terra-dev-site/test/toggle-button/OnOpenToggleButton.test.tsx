import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ToggleButton from '../../../ToggleButton';
type OnOpenToggleButtonState = {
  timesOpened?: number;
};
class OnOpenToggleButton extends React.Component<{}, OnOpenToggleButtonState> {
  constructor(props) {
    super(props);
    state = { timesOpened: 0 };
    handleOnOpen = handleOnOpen.bind(this);
  }
  handleOnOpen() {
    setState(prevState => ({ timesOpened: prevState.timesOpened + 1 }));
  }
  render() {
    return (
      <div>
        <div id="on-open-event">
          <h3>{`Times Opened: ${state.timesOpened}`}</h3>
        </div>
        <ToggleButton id="onOpenToggleButton" closedButtonText="ToggleButton" onOpen={handleOnOpen}>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </ToggleButton>
      </div>
    );
  }
}
export default OnOpenToggleButton;
