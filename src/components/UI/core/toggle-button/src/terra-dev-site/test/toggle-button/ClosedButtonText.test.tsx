import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ToggleButton from '../../../ToggleButton';
const ClosedButtonTextToggleButton = () => (
  <ToggleButton id="closedButtonText" closedButtonText="Custom Text">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
  </ToggleButton>
);
export default ClosedButtonTextToggleButton;
