import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ToggleButton from '../../../ToggleButton';
const InitiallyOpenToggleButton = () => (
  <ToggleButton id="initiallyOpenToggleButton" closedButtonText="Custom Text" isInitiallyOpen>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
  </ToggleButton>
);
export default InitiallyOpenToggleButton;
