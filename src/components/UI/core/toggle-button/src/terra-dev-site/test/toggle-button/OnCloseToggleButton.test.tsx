import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ToggleButton from '../../../ToggleButton';
type OnCloseToggleButtonState = {
  timesClosed?: number;
};
class OnCloseToggleButton extends React.Component<{}, OnCloseToggleButtonState> {
  constructor(props) {
    super(props);
    state = { timesClosed: 0 };
    handleOnClose = handleOnClose.bind(this);
  }
  handleOnClose() {
    setState(prevState => ({ timesClosed: prevState.timesClosed + 1 }));
  }
  render() {
    return (
      <div>
        <div id="on-close-event">
          <h3>{`Times Closed: ${state.timesClosed}`}</h3>
        </div>
        <ToggleButton id="onCloseToggleButton" closedButtonText="ToggleButton" onClose={handleOnClose}>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </ToggleButton>
      </div>
    );
  }
}
export default OnCloseToggleButton;
