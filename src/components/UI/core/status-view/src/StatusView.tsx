// import { FormattedMessage } from 'react-intl'; // Todo fix this

import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { Button, IButtonProps } from '../../button/src/Button';
import { Divider } from '../../divider/src/Divider';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './StatusView.module.scss';

const cx = classNamesBind.bind(styles);
enum StatusViewVariants {
  NODATA = 'no-data',
  NOMATCHINGRESULTS = 'no-matching-results',
  NOTAUTHORIZED = 'not-authorized',
  ERROR = 'error',
}
/* eslint-disable react/forbid-foreign-prop-types */
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * An array of objects containing terra-button properties. A key attribute is required for each object.
   * This array is used to render buttons in the bottom section.
   * Example:`[{ text: 'Button 1', key: 1, variant: 'neutral', onClick: onClickFunction}]`
   */
  buttonAttrs?: IButtonProps[];
  /**
   * Display a custom glyph. Overrides a variant's default glyph.
   */
  customGlyph?: JSX.Element;
  /**
   *  Aligns the component at the top of the container rather than "centered"
   */
  isAlignedTop?: boolean;
  /**
   * Determines if the glyph should be displayed.
   */
  isGlyphHidden?: boolean;
  /**
   * The descriptive text, displayed under the title.
   */
  message?: string;
  /**
   * The title displayed under the glyph. Variants contain default titles that can be overriden by this prop.
   */
  title?: string;
  /**
   * Sets the glyph and title using a pre-baked variant. One of the following?: `no-data`,
   * `no-matching-results`, `not-authorized`, or `error`
   */
  variant?: StatusViewVariants;
}
/* eslint-enable react/forbid-foreign-prop-types */
const defaultProps = {
  buttonAttrs: [],
  customGlyph: undefined,
  isAlignedTop: false,
  isGlyphHidden: false,
  message: undefined,
  title: undefined,
  variant: undefined,
};
const generateButtons = buttonAttrsArray => {
  if (!buttonAttrsArray.length) {
    return undefined;
  }
  return buttonAttrsArray.map(button => <Button {...button} className={cx(['button', button.className])} />);
};
const getTitleHack = (v: StatusViewVariants) => {
  switch (v) {
    case StatusViewVariants.ERROR:
      return 'Error';
    case StatusViewVariants.NODATA:
      return 'No Results';
    case StatusViewVariants.NOMATCHINGRESULTS:
      return 'No Matching Results';
    case StatusViewVariants.NOTAUTHORIZED:
      return 'Not Authorized';
  }
};
export const StatusView = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  let glyphSection;
  if (props.customGlyph && !props.isGlyphHidden) {
    glyphSection = <div className={cx('glyph')}>{props.customGlyph}</div>;
  } else if (props.variant && !props.isGlyphHidden) {
    glyphSection = (
      <div className={cx('glyph')}>
        <svg className={cx(props.variant)} />
      </div>
    );
  }

  let defaultTitle;
  if (props.variant) {
    //defaultTitle = <FormattedMessage id={`Terra.status-view.${props.variant}`} />;
    defaultTitle = getTitleHack(props.variant);
  }
  // Custom title takes precedence
  let titleSection;
  if (props.title || defaultTitle) {
    titleSection = <p className={cx('title')}>{props.title || defaultTitle}</p>;
  }
  let messageSection;
  if (props.message) {
    messageSection = <p className={cx('message')}>{props.message}</p>;
  }
  let actionSection;
  const buttons = generateButtons(props.buttonAttrs);
  if (buttons) {
    actionSection = <div className={cx('actions')}>{buttons}</div>;
  }
  let dividerSection;
  if (titleSection && (messageSection || actionSection)) {
    dividerSection = (
      <div className={cx('divider')}>
        <Divider />
      </div>
    );
  }

  const [extracted, customProps] = splitProps(props, ['buttonAttrs', 'customGlyph', 'isAlignedTop', 'isGlyphHidden', 'message', 'title', 'variant']);
  return (
    <div {...customProps} className={classNames(cx('outer-view', { 'is-aligned-top': props.isAlignedTop }, theme.className), customProps.className)}>
      <div className={cx('top-space')} />
      <div className={cx(['inner-view'])}>
        {glyphSection}
        {titleSection}
        {dividerSection}
        {messageSection}
        {actionSection}
      </div>
      <div className={cx('bottom-space')} />
    </div>
  );
};
export { StatusViewVariants };
