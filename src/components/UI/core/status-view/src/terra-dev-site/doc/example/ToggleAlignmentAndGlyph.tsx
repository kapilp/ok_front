import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { StatusView } from '../../../StatusView';

export const ToggleAlignmentAndGlyph = () => {
  const [state, setState] = createStore({ isAlignedTop: false, isGlyphHidden: false });
  const handleOnAlignChange = () => {
    setState({ isAlignedTop: !state.isAlignedTop });
  };
  const handleOnGlyphChange = () => {
    setState({ isGlyphHidden: !state.isGlyphHidden });
  };
  /* eslint-disable react/forbid-dom-props */
  return (
    <div>
      <div style={{ height: '400px', border: '1px dashed black' }}>
        <StatusView variant="error" isAlignedTop={state.isAlignedTop} isGlyphHidden={state.isGlyphHidden} />
      </div>
      <fieldset>
        <legend>Toggle alignment and glyph</legend>
        <div>
          <input id="isAlignedTop" type="checkbox" onChange={handleOnAlignChange} />
          <label htmlFor="isAlignedTop">isAlignedTop</label>
        </div>
        <div>
          <input id="isGlyphHidden" type="checkbox" onChange={handleOnGlyphChange} />
          <label htmlFor="isGlyphHidden">isGlyphHidden</label>
        </div>
      </fieldset>
    </div>
  );
};
