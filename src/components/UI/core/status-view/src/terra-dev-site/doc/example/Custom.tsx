import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { StatusView } from '../../../StatusView';
import { SvgIconDiamond } from '../../../../../icon/src/icon/IconDiamond';

const StatusViewButtons = [
  {
    text: 'Action 1',
    key: 1,
  },
  {
    text: 'Action 2',
    key: 2,
  },
];
export const CustomStatusView = () => <StatusView message="Custom Message" customGlyph={<SvgIconDiamond />} title="Custom Title" buttonAttrs={StatusViewButtons} />;
