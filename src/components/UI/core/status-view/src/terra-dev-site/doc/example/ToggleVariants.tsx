import { StatusView } from '../../../StatusView';
import {  JSX,   } from 'solid-js';
import { createStore } from 'solid-js/store';

export const ToggleVariantsStatusView = () => {
  const [state, setState] = createStore({ variant: 'error' });
  const handleOnSelect = event => {
    setState({ variant: event.target.value });
  };
  return (
    <div>
      <StatusView variant={state.variant} />
      <fieldset>
        <legend>Change Variant</legend>
        <select aria-label="changeVariant" id="statusViewVariant" name="statusViewVariant" value={state.variant} onChange={handleOnSelect}>
          <option value="error">Error</option>
          <option value="no-data">No Data</option>
          <option value="no-matching-results">No Matching Results</option>
          <option value="not-authorized">Not Authorized</option>
        </select>
      </fieldset>
    </div>
  );
};
