import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import StatusView from '../../../StatusView';
import styles from './StatusViewTestCommon.module.scss';
const cx = classNames.bind(styles);
export default () => <StatusView id="statusView" variant="no-data" className={cx('status-view-1')} />;
