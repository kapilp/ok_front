import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import StatusView from '../../../StatusView';
export default () => <StatusView id="statusView" variant="not-authorized" />;
