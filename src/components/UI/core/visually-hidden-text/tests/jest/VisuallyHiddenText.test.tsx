
import VisuallyHiddenText from "../../src/VisuallyHiddenText";
describe("VisuallyHiddenText", () => {
  const defaultRender = (
    <VisuallyHiddenText text="This is screen reader only text" />
  );
  // Snapshot Tests
  it("should render a default component", () => {
    const wrapper = shallow(defaultRender);
    expect(wrapper).toMatchSnapshot();
  });
  it("should pass in ref as the ref prop of the input element", () => {
    const ref = jest.fn();
    const wrapper = mount(<VisuallyHiddenText ref={ref} />);
    expect(ref).toBeCalled();
    expect(wrapper).toMatchSnapshot();
  });
});
