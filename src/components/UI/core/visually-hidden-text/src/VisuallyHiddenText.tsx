import { JSX, mergeProps, Show, splitProps } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './VisuallyHiddenText.module.scss';

const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Callback ref to pass into the dom element. This is useful when using terra-visually-hidden-text as an aria-live container.
   */
  ref: (el: HTMLSpanElement) => void;
  /**
   * Text to be read to the screen reader
   */
  text: string;
}
const defaultProps = {
  text: undefined,
  // ref: undefined,
};
export const VisuallyHiddenText = (props: Properties) => {
  console.log(props);
  props = mergeProps({}, defaultProps, props);
  // const [p, customProps] = splitProps(props, ['text', 'className']);
  const VisuallyHiddenTextClassNames = () => cx(['visually-hidden-text', props.className]);
  return (
    // ref={props.ref}
    <span className={VisuallyHiddenTextClassNames()}>{props.text}</span>
  );
};
