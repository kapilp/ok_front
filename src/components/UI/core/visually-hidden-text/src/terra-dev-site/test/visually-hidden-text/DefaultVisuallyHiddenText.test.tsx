import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import VisuallyHiddenText from '../../../VisuallyHiddenText';
export default () => (
  <p>
    No Screen Reader text is added on this page, and should be blank other than this line.
    <VisuallyHiddenText />
  </p>
);
