import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { VisuallyHiddenText } from '../../../VisuallyHiddenText';

export const RefCallbackVisuallyHiddenText = () => {
  let visuallyHiddenText: any;
  const setRef = (el: HTMLSpanElement) => {
    visuallyHiddenText = el;
    setTimeout(() => (visuallyHiddenText.innerText = 'Text written via ref innerText update'));
  };

  return (
    <p>
      Visually Hidden Text which uses ref to write innerText
      <VisuallyHiddenText ref={setRef} />
    </p>
  );
};
