import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { VisuallyHiddenText } from '../../../VisuallyHiddenText';
export const DefaultVisuallyHiddenText = () => (
  <p>
    Focus in this section to hear screen reader only text
    <VisuallyHiddenText tabIndex="0" text="This is read by a screen reader" />
  </p>
);
