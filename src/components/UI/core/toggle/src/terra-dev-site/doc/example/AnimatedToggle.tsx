import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Toggle } from '../../../Toggle';
import { SvgIconInformation } from '../../../../../icon/src/icon/IconInformation';
export const AnimatedToggle = (props: {}) => {
  const [state, setState] = createStore({ isOpen: false });

  const handleOnClick = () => {
    setState({ isOpen: !state.isOpen });
  };

  return (
    <div>
      <SvgIconInformation onClick={handleOnClick} aria-expanded={state.isOpen} aria-controls="toggle" />

      <Toggle id="animated-toggle" isOpen={state.isOpen} isAnimated>
        <p>
          Lorem ipsum dolor sit amet,
          <a href="#test">consectetur</a> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
          ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
          sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </Toggle>
    </div>
  );
};
