import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './Toggle.module.scss';
// import AnimateHeight from 'react-animate-height';
import { AnimateHeight } from '../../../AnimateHeight';
const cx = classNames.bind(styles);

interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Content in the body of the toggle component that will be expanded or collapsed
   */
  children: JSX.Element;
  /**
   * When set, will animate the toggle component as it is expanded or collapsed
   */
  isAnimated?: boolean;
  /**
   * Used to expand or collapse toggle content
   */
  isOpen?: boolean;
}
const defaultProps = {
  isAnimated: false,
  isOpen: false,
};
export const Toggle = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  const [extracted, customProps] = splitProps(props, ['isAnimated', 'isOpen', 'children']);
  return (
    <div {...customProps} className={cx('toggle', customProps.className)} aria-hidden={!props.isOpen}>
      {props.isAnimated ? (
        <AnimateHeight duration={250} height={props.isOpen ? 'auto' : 0}>
          {props.children}
        </AnimateHeight>
      ) : (
        props.isOpen && props.children
      )}
    </div>
  );
};
