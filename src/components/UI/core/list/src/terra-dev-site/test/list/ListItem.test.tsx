import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item } from 'terra-list/lib/index';
const ListItemTest = () => (
  <>
    <List>
      <Item key="default">
        <p>test default</p>
      </Item>
      <Item key="chevron" hasChevron>
        <p>test chevron</p>
      </Item>
    </List>
    <List role="listbox" aria-label="test-label">
      <Item key="selectable" isSelectable id="selectable-item">
        <p>test selectable</p>
      </Item>
      <Item key="selected" isSelectable isSelected>
        <p>test selected</p>
      </Item>
    </List>
  </>
);
export default ListItemTest;
