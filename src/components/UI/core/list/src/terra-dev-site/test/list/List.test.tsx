import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item } from 'terra-list/lib/index';
const ListTest = () => (
  <>
    <List>
      <Item key="0">
        <p>test 0</p>
      </Item>
      <Item key="1">
        <p>test 1</p>
      </Item>
      <Item key="2">
        <p>test 2</p>
      </Item>
    </List>
    <List paddingStyle="standard" dividerStyle="standard">
      <Item key="0">
        <p>divided test 0</p>
      </Item>
      <Item key="1">
        <p>divided test 1</p>
      </Item>
      <Item key="2">
        <p>divided test 2</p>
      </Item>
    </List>
    <List paddingStyle="compact" dividerStyle="bottom-only">
      <Item key="0">
        <p>divided test 0</p>
      </Item>
      <Item key="1">
        <p>divided test 1</p>
      </Item>
      <Item key="2">
        <p>divided test 2</p>
      </Item>
    </List>
  </>
);
export default ListTest;
