import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { List } from '../../../List';
import { ListItem } from '../../../ListItem';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import classNames from 'classnames/bind';
import styles from './ListDocCommon.module.scss';
const cx = classNames.bind(styles);
export const ListItemExample = () => (
  <List role="listbox" aria-label="example-label">
    <ListItem key="default">
      <Placeholder title="Default ListItem" className={cx('placeholder')} />
    </ListItem>
    <ListItem key="chevron" hasChevron>
      <Placeholder title="Chevron ListItem" className={cx('placeholder')} />
    </ListItem>
    <ListItem key="selectable" isSelectable>
      <Placeholder title="Selectable ListItem" className={cx('placeholder')} />
    </ListItem>
    <ListItem key="selected" isSelectable isSelected>
      <Placeholder title="Selected ListItem" className={cx('placeholder')} />
    </ListItem>
  </List>
);
