import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { List } from '../../../List';
import { ListSubsectionHeader } from '../../../ListSubsectionHeader';

export const ListSubsectionHeaderExample = () => (
  <List>
    <ListSubsectionHeader title="Default SubsectionHeader" key="default" />
  </List>
);
