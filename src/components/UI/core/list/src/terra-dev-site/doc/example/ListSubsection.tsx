import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { List } from '../../../List';
import { ListItem } from '../../../ListItem';
import { ListSubsection } from '../../../ListSubsection';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import classNames from 'classnames/bind';
import styles from './ListDocCommon.module.scss';
const cx = classNames.bind(styles);
export const ListSubsectionExample = () => (
  <List>
    <ListSubsection key="static-subsection" title="Static Subsection">
      <ListItem key="123">
        <Placeholder title="Item 0-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="124">
        <Placeholder title="Item 0-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="125">
        <Placeholder title="Item 0-2" className={cx('placeholder')} />
      </ListItem>
    </ListSubsection>
    <ListSubsection key="collapsible-subsection" isCollapsible title="Collapsible Subsection">
      <ListItem key="223">
        <Placeholder title="Item 1-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="224">
        <Placeholder title="Item 1-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="225">
        <Placeholder title="Item 1-2" className={cx('placeholder')} />
      </ListItem>
    </ListSubsection>
    <ListSubsection key="collapsible-subsection" isCollapsed isCollapsible title="Collapsed Subsection">
      <ListItem key="323">
        <Placeholder title="Item 2-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="324">
        <Placeholder title="Item 2-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="325">
        <Placeholder title="Item 2-2" className={cx('placeholder')} />
      </ListItem>
    </ListSubsection>
  </List>
);
