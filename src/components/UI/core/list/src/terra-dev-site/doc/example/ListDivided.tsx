import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ListItem } from '../../../ListItem';
import { List } from '../../../List';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import classNames from 'classnames/bind';
import styles from './ListDocCommon.module.scss';
const cx = classNames.bind(styles);
export const ListDividedExample = () => (
  <>
    <List dividerStyle="standard">
      <ListItem key="123">
        <Placeholder title="Standard Divider Item 0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="124">
        <Placeholder title="Standard Divider Item 1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="125">
        <Placeholder title="Standard Divider Item 2" className={cx('placeholder')} />
      </ListItem>
    </List>
    <br />
    <List dividerStyle="bottom-only">
      <ListItem key="123">
        <Placeholder title="Bottom Divider Item 0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="124">
        <Placeholder title="Bottom Divider Item 1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="125">
        <Placeholder title="Bottom Divider Item 2" className={cx('placeholder')} />
      </ListItem>
    </List>
  </>
);
