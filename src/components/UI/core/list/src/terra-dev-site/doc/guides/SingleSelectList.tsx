import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item } from 'terra-list/lib/index';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import classNames from 'classnames/bind';
import mockData from './mock-data/mock-select';
import styles from '../example/ListDocCommon.module.scss';
const cx = classNames.bind(styles);
type SingleSelectListState = {
  selectedKey?: null;
};
class SingleSelectList extends React.Component<{}, SingleSelectListState> {
  constructor(props) {
    super(props);
    createListItem = createListItem.bind(this);
    handleItemSelection = handleItemSelection.bind(this);
    state = { selectedKey: null };
  }
  handleItemSelection(event, metaData) {
    event.preventDefault();
    if (state.selectedKey !== metaData.key) {
      setState({ selectedKey: metaData.key });
    }
  }
  createListItem(itemData) {
    return (
      <Item key={itemData.key} isSelectable isSelected={state.selectedKey === itemData.key} metaData={{ key: itemData.key }} onSelect={handleItemSelection}>
        <Placeholder title={itemData.title} className={cx('placeholder')} />
      </Item>
    );
  }
  createListItems(data) {
    return data.map(childItem => createListItem(childItem));
  }
  render() {
    return (
      <List dividerStyle="standard" role="listbox" aria-label="SingleSelectList-label">
        {createListItems(mockData)}
      </List>
    );
  }
}
export default SingleSelectList;
