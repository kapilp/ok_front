import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item, Section, Utils } from 'terra-list';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import classNames from 'classnames/bind';
import mockData from './mock-data/mock-section';
import styles from '../example/ListDocCommon.module.scss';
const cx = classNames.bind(styles);
const createListItem = itemData => (
  <Item key={itemData.key}>
    <Placeholder title={itemData.title} className={cx('placeholder')} />
  </Item>
);
type SectionListState = {
  collapsedKeys?: undefined[];
  indexOf?: any;
};
class SectionList extends React.Component<{}, SectionListState> {
  constructor(props) {
    super(props);
    createSection = createSection.bind(this);
    createSections = createSections.bind(this);
    handleSectionSelection = handleSectionSelection.bind(this);
    state = { collapsedKeys: [] };
  }
  handleSectionSelection(event, metaData) {
    event.preventDefault();
    setState(state => ({
      collapsedKeys: Utils.updatedMultiSelectedKeys(state.collapsedKeys, metaData.key),
    }));
  }
  createSection(sectionData) {
    return (
      <Section
        key={sectionData.key}
        title={sectionData.title}
        isCollapsed={state.collapsedKeys.indexOf(sectionData.key) >= 0}
        isCollapsible
        metaData={{ key: sectionData.key }}
        onSelect={handleSectionSelection}
      >
        {sectionData.childItems.map(childItem => createListItem(childItem))}
      </Section>
    );
  }
  createSections(data) {
    return data.map(section => createSection(section));
  }
  render() {
    return <List role="listbox">{createSections(mockData)}</List>;
  }
}
export default SectionList;
