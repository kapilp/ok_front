import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { List } from '../../../List';
import { ListItem } from '../../../ListItem';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import classNames from 'classnames/bind';
import styles from './ListDocCommon.module.scss';
const cx = classNames.bind(styles);
export const ListExample = () => (
  <List>
    <ListItem key="123">
      <Placeholder title="Item 0" className={cx('placeholder')} />
    </ListItem>
    <ListItem key="124">
      <Placeholder title="Item 1" className={cx('placeholder')} />
    </ListItem>
    <ListItem key="125">
      <Placeholder title="Item 2" className={cx('placeholder')} />
    </ListItem>
  </List>
);
