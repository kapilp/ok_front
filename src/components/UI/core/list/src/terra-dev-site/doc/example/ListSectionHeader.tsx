import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { SectionHeader } from 'terra-list/lib/index';
const ListSectionHeaderExample = () => (
  <List>
    <SectionHeader title="Default SectionHeader" key="default" />
  </List>
);
export default ListSectionHeaderExample;
