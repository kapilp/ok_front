import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item, Utils } from 'terra-list/lib/index';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import classNames from 'classnames/bind';
import mockData from './mock-data/mock-select';
import styles from '../example/ListDocCommon.module.scss';
const cx = classNames.bind(styles);
const maxSectionCount = 3;
type MutliSelectListState = {
  selectedKeys?: undefined[];
  indexOf?: any;
};
class MutliSelectList extends React.Component<{}, MutliSelectListState> {
  constructor(props) {
    super(props);
    createListItem = createListItem.bind(this);
    handleItemSelection = handleItemSelection.bind(this);
    state = { selectedKeys: [] };
  }
  handleItemSelection(event, metaData) {
    event.preventDefault();
    setState(state => ({
      selectedKeys: Utils.updatedMultiSelectedKeys(state.selectedKeys, metaData.key),
    }));
  }
  createListItem(itemData) {
    return (
      <Item
        key={itemData.key}
        isSelectable={Utils.shouldBeMultiSelectable(maxSectionCount, state.selectedKeys, itemData.key)}
        isSelected={state.selectedKeys.indexOf(itemData.key) >= 0}
        metaData={{ key: itemData.key }}
        onSelect={handleItemSelection}
      >
        <Placeholder title={itemData.title} className={cx('placeholder')} />
      </Item>
    );
  }
  createListItems(data) {
    return data.map(childItem => createListItem(childItem));
  }
  render() {
    return (
      <List dividerStyle="standard" role="listbox" aria-multiselectable aria-label="MultiSelectList-label">
        {createListItems(mockData)}
      </List>
    );
  }
}
export default MutliSelectList;
