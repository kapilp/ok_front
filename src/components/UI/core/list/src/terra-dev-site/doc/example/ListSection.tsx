import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ListItem } from '../../../ListItem';
import { List } from '../../../List';
import { Placeholder } from '../../../../../doc-template/src/Placeholder';
import { ListSection } from '../../../ListSection';
import classNames from 'classnames/bind';
import styles from './ListDocCommon.module.scss';
const cx = classNames.bind(styles);
export const ListSectionExample = () => (
  <List>
    <ListSection key="static-section" title="Static Section">
      <ListItem key="123">
        <Placeholder title="Item 0-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="124">
        <Placeholder title="Item 0-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="125">
        <Placeholder title="Item 0-2" className={cx('placeholder')} />
      </ListItem>
    </ListSection>
    <ListSection key="collapsible-section" isCollapsible title="Collapsible Section">
      <ListItem key="223">
        <Placeholder title="Item 1-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="224">
        <Placeholder title="Item 1-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="225">
        <Placeholder title="Item 1-2" className={cx('placeholder')} />
      </ListItem>
    </ListSection>
    <ListSection key="collapsible-section" isCollapsed isCollapsible title="Collapsed Section">
      <ListItem key="323">
        <Placeholder title="Item 2-0" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="324">
        <Placeholder title="Item 2-1" className={cx('placeholder')} />
      </ListItem>
      <ListItem key="325">
        <Placeholder title="Item 2-2" className={cx('placeholder')} />
      </ListItem>
    </ListSection>
  </List>
);
