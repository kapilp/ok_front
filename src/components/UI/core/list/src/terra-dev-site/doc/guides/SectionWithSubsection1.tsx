import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import List, { Item, Section, Subsection, Utils } from 'terra-list';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import classNames from 'classnames/bind';
import mockData from './mock-data/mock-section-sub';
import styles from '../example/ListDocCommon.module.scss';
const cx = classNames.bind(styles);
const createListItem = itemData => (
  <Item key={itemData.key}>
    <Placeholder title={itemData.title} className={cx('placeholder')} />
  </Item>
);
const createSubsection = subsectionData => (
  <Subsection key={subsectionData.key} title={subsectionData.title}>
    {subsectionData.childItems.map(childItem => createListItem(childItem))}
  </Subsection>
);
type SectionWithSubsection1State = {
  collapsedKeys?: undefined[];
  indexOf?: any;
};
class SectionWithSubsection1 extends React.Component<{}, SectionWithSubsection1State> {
  constructor(props) {
    super(props);
    createSection = createSection.bind(this);
    createSections = createSections.bind(this);
    handleSectionSelection = handleSectionSelection.bind(this);
    state = { collapsedKeys: [] };
  }
  handleSectionSelection(event, metaData) {
    event.preventDefault();
    setState(state => ({
      collapsedKeys: Utils.updatedMultiSelectedKeys(state.collapsedKeys, metaData.key),
    }));
  }
  createSection(sectionData) {
    const isCollapsed = state.collapsedKeys.indexOf(sectionData.key) >= 0;
    return (
      <Section key={sectionData.key} title={sectionData.title} isCollapsed={isCollapsed} isCollapsible metaData={{ key: sectionData.key }} onSelect={handleSectionSelection}>
        {!isCollapsed && sectionData.childItems.map(childItem => createSubsection(childItem))}
      </Section>
    );
  }
  createSections(data) {
    return data.map(section => createSection(section));
  }
  render() {
    return <List dividerStyle="standard">{createSections(mockData)}</List>;
  }
}
export default SectionWithSubsection1;
