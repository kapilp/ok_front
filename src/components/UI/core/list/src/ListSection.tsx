import { ListSectionHeader } from './ListSectionHeader';
import { JSX, mergeProps, splitProps } from 'solid-js';
interface Properties {
  /**
   * The children list items passed to the component.
   */
  children?: JSX.Element;
  /**
   * Whether or not the section is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * Whether or not the section can be collapsed.
   */
  isCollapsible?: boolean;
  /**
   * Optionally sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`.
   */
  level?: 1 | 2 | 3 | 4 | 5 | 6;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript event and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback passthrough for the ref of the section li.
   */
  ref?: () => void;
  /**
   * Title text to be placed within the section header.
   */
  title: string;
}
const defaultProps = {
  children: [],
  isCollapsed: false,
  isCollapsible: false,
  level: 1,
};
export const ListSection = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'isCollapsed', 'isCollapsible']);
  let sectionItems;
  if (!p.isCollapsible || !p.isCollapsed) {
    sectionItems = p.children;
  }
  return (
    <>
      <ListSectionHeader {...customProps} isCollapsible={p.isCollapsible} isCollapsed={p.isCollapsed} />
      {sectionItems}
    </>
  );
};
