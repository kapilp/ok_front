import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './List.module.scss';
import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLUListElement> {
  /**
   * The children list items passed to the component.
   */
  children?: JSX.Element;
  /**
   * Whether or not the list's child items should have a border color applied.
   * One of `'none'`, `'standard'`, `'bottom-only'`.
   */
  dividerStyle?: 'none' | 'standard' | 'bottom-only';
  /**
   * The padding styling to apply to the child list item content.
   * One of `'none'`, `'standard'`, `'compact'`.
   */
  paddingStyle?: 'none' | 'standard' | 'compact';
  /**
   * Function callback for the ref of the ul.
   */
  ref?: (el: HTMLUListElement) => void;
  /**
   * Accessibility role of the list, defaults to 'none'. If creating a list with selectable items, pass 'listbox'.
   */
  role: string;
}
const defaultProps = {
  children: [],
  dividerStyle: 'none',
  paddingStyle: 'none',
  role: 'none',
};
export const List = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [p, customProps] = splitProps(props, ['children', 'dividerStyle', 'paddingStyle', 'ref', 'role', 'className']);

  const attrSpread = {};
  if (p.role && p.role.length > 0 && p.role !== 'none') {
    attrSpread.role = p.role;
  }
  return (
    <ul
      {...customProps}
      {...attrSpread}
      className={classNames(
        cx(
          'list',
          { 'padding-standard': p.paddingStyle === 'standard' },
          { 'padding-compact': p.paddingStyle === 'compact' },
          { 'divider-standard': p.dividerStyle === 'standard' },
          { 'divider-bottom-only': p.dividerStyle === 'bottom-only' },
          theme.className,
        ),
        p.className,
      )}
      ref={p?.ref}
    >
      {p.children}
    </ul>
  );
};
