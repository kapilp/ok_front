import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { ListSubsectionHeader } from './ListSubsectionHeader';
interface Properties {
  /**
   * The children list items passed to the component.
   */
  children?: JSX.Element;
  /**
   * Whether or not the subsection is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * Whether or not the subsection can be collapsed.
   */
  isCollapsible?: boolean;
  /**
   * Optionally sets the heading level. One of `2`, `3`, `4`, `5`, `6`.
   */
  level?: 2 | 3 | 4 | 5 | 6;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript evnt and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback passthrough for the ref of the section li.
   */
  ref?: () => void;
  /**
   * Title text to be placed within the subsection header.
   */
  title: string;
}
const defaultProps = {
  children: [],
  isCollapsed: false,
  isCollapsible: false,
  level: 2,
};
export const ListSubsection = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children', 'isCollapsed', 'isCollapsible']);

  return (
    <>
      <ListSubsectionHeader {...customProps} isCollapsible={p.isCollapsible} isCollapsed={p.isCollapsed} />
      {(!p.isCollapsible || !p.isCollapsed) && p.children}
    </>
  );
};
