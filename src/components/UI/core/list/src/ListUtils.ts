import * as KeyCode from 'keycode-js';
const shouldBeMultiSelectable = (maxSelectionCount, selectedKeys, key) => maxSelectionCount < 0 || selectedKeys.indexOf(key) >= 0 || selectedKeys.length < maxSelectionCount;
/**
 * Returns a new array, updated with the newKey being added or removed from the existing.
 */
export const updatedMultiSelectedKeys = (currentKeys, newKey) => {
  let newKeys = [];
  if (currentKeys.length) {
    if (currentKeys.indexOf(newKey) >= 0) {
      newKeys = currentKeys.slice();
      newKeys.splice(newKeys.indexOf(newKey), 1);
    } else {
      newKeys = currentKeys.concat([newKey]);
    }
  } else {
    newKeys.push(newKey);
  }
  return newKeys;
};
/**
 * Returns a wrapped onClick callback function. If the onSelect method isn't passed, we return the initial onClick.
 */
export const wrappedOnClickForItem = (onClick, onSelect, metaData) => {
  if (!onSelect) {
    return onClick;
  }
  return event => {
    onSelect(event, metaData);
    if (onClick) {
      onClick(event);
    }
  };
};
/**
 * Returns a wrapped onKeyDown callback function with enter and space keys triggering onSelect. If the onSelect method isn't passed, we return the initial onClick.
 */
export const wrappedOnKeyDownForItem = (onKeyDown, onSelect, metaData) => {
  if (!onSelect) {
    return onKeyDown;
  }
  return event => {
    if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
      onSelect(event, metaData);
    }
    if (onKeyDown) {
      onKeyDown(event);
    }
  };
};
/**
 * Returns a function that wraps both the old and new callback.
 */
export const wrappedEventCallback = (callback, newCallback) => {
  if (!callback) {
    return newCallback;
  }
  return event => {
    newCallback(event);
    if (callback) {
      callback(event);
    }
  };
};
