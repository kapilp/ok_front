import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { SvgIconChevronRight } from '../../icon/src/icon/IconChevronRight';
import { wrappedEventCallback, wrappedOnClickForItem, wrappedOnKeyDownForItem } from './ListUtils';
import styles from './List.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The content element to be placed inside the list item for display.
   */
  children?: JSX.Element;
  /**
   * Whether or not the list item has a disclosure indicator presented.
   */
  hasChevron?: boolean;
  /**
   * Whether or not the list item should have selection styles applied.
   */
  isSelected?: boolean;
  /**
   * Whether or not the list item should have styles to indicate the item is selectable.
   */
  isSelectable?: boolean;
  /**
   * The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript evnt and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback for the ref of the li.
   */
  ref?: () => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: MouseEvent) => void;
}
const defaultProps = {
  children: [],
  hasChevron: false,
  isSelected: false,
  isSelectable: false,
};
export const ListItem = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'children',
    'hasChevron',
    'isSelected',
    'isSelectable',
    'metaData',
    'onBlur',
    'onClick',
    'onKeyDown',
    'onMouseDown',
    'onSelect',
    'ref',
  ]);

  const theme = useContext(ThemeContext);
  const listItemClassNames = classNames(cx('list-item', { selected: p.isSelected && p.isSelectable }, { 'is-selectable': p.isSelectable }, theme.className), customProps.className);
  const attrSpread = {};
  if (p.isSelectable) {
    attrSpread.onClick = wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData);
    attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
    attrSpread.tabIndex = '0';
    attrSpread.role = 'option';
    attrSpread['aria-selected'] = p.isSelected;
    attrSpread['data-item-show-focus'] = 'true';
    attrSpread.onBlur = wrappedEventCallback(p.onBlur, (event: FocusEvent) => event.currentTarget.setAttribute('data-item-show-focus', 'true'));
    attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, (event: MouseEvent) => event.currentTarget.setAttribute('data-item-show-focus', 'false'));
  }
  return (
    <li {...customProps} {...attrSpread} className={listItemClassNames} ref={p.ref}>
      <div className={cx('item-fill')} key="item-fill">
        {p.children}
      </div>
      {p.hasChevron && (
        <div className={cx('item-end')} key="item-end">
          <span className={cx('chevron')}>
            <SvgIconChevronRight height="1em" width="1em" />
          </span>
        </div>
      )}
    </li>
  );
};
