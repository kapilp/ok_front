import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { wrappedEventCallback, wrappedOnClickForItem, wrappedOnKeyDownForItem } from './ListUtils';
import styles from './ListSectionHeader.module.scss';

const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * @private Whether or not the section is collapsed.
   */
  isCollapsed?: boolean;
  /**
   * @private Whether or not the section can be collapsed.
   */
  isCollapsible?: boolean;
  /**
   * Optionally sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`.
   */
  level?: 1 | 2 | 3 | 4 | 5 | 6;
  /**
   * @private The associated metaData to be provided in the onSelect callback.
   */
  // eslint-disable-next-line react/forbid-prop-types
  metaData?: {};
  /**
   * @private Function callback for when the appropriate click or key action is performed.
   * Callback contains the javascript evnt and prop metadata, e.g. onSelect(event, metaData)
   */
  onSelect?: () => void;
  /**
   * Function callback passthrough for the ref of the section li.
   */
  ref?: (el: HTMLLIElement) => void;
  /**
   * Title text to be placed within the section header.
   */
  title: string;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * @private Callback function not intended for use with this API, but if set pass it through to the element regardless.
   */
  onMouseDown?: (e: MouseEvent) => void;
}
const defaultProps = {
  isCollapsed: false,
  isCollapsible: false,
  level: 1,
};
export const ListSectionHeader = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['isCollapsed', 'isCollapsible', 'level', 'metaData', 'onBlur', 'onClick', 'onKeyDown', 'onMouseDown', 'onSelect', 'ref', 'title']);
  const theme = useContext(ThemeContext);
  const sectionHeaderClassNames = classNames(cx('section-header', { 'is-collapsible': p.isCollapsible }, theme.className), customProps.className);
  const attrSpread = {};

  let titleElement = (
    <Dynamic component={`h${p.level}`} className={cx('title')}>
      {p.title}
    </Dynamic>
  );
  let accordionIcon;
  if (p.isCollapsible) {
    accordionIcon = (
      <div className={cx('start')}>
        <span className={cx(['accordion-icon', { 'is-open': !p.isCollapsed }])} />
      </div>
    );
    titleElement = <div className={cx('fill')}>{titleElement}</div>;
    attrSpread.onClick = wrappedOnClickForItem(p.onClick, p.onSelect, p.metaData);
    attrSpread.onKeyDown = wrappedOnKeyDownForItem(p.onKeyDown, p.onSelect, p.metaData);
    attrSpread.tabIndex = '0';
    attrSpread.role = 'heading';
    attrSpread['aria-expanded'] = !p.isCollapsed;
    attrSpread['aria-level'] = 1;
    attrSpread['data-item-show-focus'] = 'true';
    attrSpread.onBlur = wrappedEventCallback(p.onBlur, (event: FocusEvent) => event.currentTarget.setAttribute('data-item-show-focus', 'true'));
    attrSpread.onMouseDown = wrappedEventCallback(p.onMouseDown, event => event.currentTarget.setAttribute('data-item-show-focus', 'false'));
  }
  return (
    <li {...customProps} {...attrSpread} className={sectionHeaderClassNames} ref={p?.ref}>
      {accordionIcon}
      {titleElement}
    </li>
  );
};
