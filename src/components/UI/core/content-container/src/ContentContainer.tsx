import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './ContentContainer.module.scss';
import { Scroll } from '../../scroll/src/Scroll';

const cx = classNames.bind(styles);
interface Properties {
  /**
   * The header element to be placed within the header area of the container.
   */
  header?: JSX.Element;
  /**
   * The footer element to be placed within the footer area of the container.
   */
  footer?: JSX.Element;
  /**
   * The children to be placed within the main content area of the container.
   */
  children?: JSX.Element;
  /**
   * Whether or not the container should expanded to fill its parent element.
   */
  fill?: boolean;
  /**
   * Ref callback for the scrollable area of the content container.
   */
  ref?: (el: HTMLDivElement) => void;
}
const defaultProps = {
  header: undefined,
  footer: undefined,
  children: undefined,
  fill: false,
  ref: undefined,
};
export const ContentContainer = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  const [extracted, customProps] = splitProps(props, ['header', 'footer', 'children', 'fill', 'ref']);
  return (
    <div {...customProps} className={cx([`content-container-${props.fill ? 'fill' : 'static'}`, customProps.className])}>
      {props.header && <div className={cx('header')}>{props.header}</div>}
      <div className={cx('main')}>
        <Scroll className={cx('normalizer')} ref={props.ref}>
          {props.children}
        </Scroll>
      </div>
      {props.footer && <div className={cx('footer')}>{props.footer}</div>}
    </div>
  );
};
