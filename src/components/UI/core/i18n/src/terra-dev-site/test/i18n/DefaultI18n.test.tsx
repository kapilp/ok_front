import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { FormattedMessage } from 'react-intl';
import classNames from 'classnames/bind';
import I18nProvider from '../../../I18nProvider';
import i18nLoader from '../../../i18nLoader';
import styles from './DefaultI18n.module.scss';
const cx = classNames.bind(styles);
// Add Portuguese-Guinea-Bissau, Zulu & Zulu-South African locales as test locales (supported by intl)
const testLocales = ['en', 'en-GB', 'pt', 'pt-GW', 'zu', 'zu-ZA'];
interface IBaseProps extends JSX.HTMLAttributes<Element> {
  locale?: string;
}
type BaseState = {
  selectedLocale?: string;
  areTranslationsLoaded?: boolean;
  locale?: any;
  messages?: {};
  includes?: any;
};
class Base extends React.Component<IBaseProps, BaseState> {
  constructor(props) {
    super(props);
    state = {
      selectedLocale: 'en',
      areTranslationsLoaded: false,
      locale: props.locale,
      messages: {},
    };
    handleLocaleChange = handleLocaleChange.bind(this);
  }
  componentDidMount() {
    i18nLoader(props.locale, setState, this);
  }
  handleLocaleChange(e) {
    setState({ selectedLocale: e.target.value });
    i18nLoader(e.target.value, setState, this);
  }
  render() {
    if (!state.areTranslationsLoaded) {
      return <div />;
    }
    return (
      <div>
        <I18nProvider locale={state.locale} messages={state.messages}>
          <label htmlFor="change-locale"> Current locale: </label>
          <select id="change-locale" onChange={handleLocaleChange} value={state.selectedLocale}>
            {testLocales.map(locale => (
              <option key={locale} value={locale}>
                {locale}
              </option>
            ))}
          </select>
          <p id="translated-message">
            <span className={cx('weighted-text')}> Loaded locale message: </span>
            <FormattedMessage id="Terra.ajax.error" />
          </p>
          {state.selectedLocale.includes('zu') && <p className={cx('fallback-message')}>Using the en locale as fallback.</p>}
          {state.selectedLocale === 'pt-GW' && <p className={cx('fallback-message')}>Using the pt locale as fallback.</p>}
        </I18nProvider>
      </div>
    );
  }
}
Base.defaultProps = {
  locale: 'en',
};
export default () => <Base />;
