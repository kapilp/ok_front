import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { FormattedMessage, FormattedDate, FormattedNumber } from 'react-intl';
import { I18nProvider, i18nLoader } from 'terra-i18n';
type DemoState = {
  areTranslationsLoaded?: boolean;
  locale?: string;
  messages?: {};
};
class Demo extends React.Component<{}, DemoState> {
  constructor(props) {
    super(props);
    state = {
      areTranslationsLoaded: false,
      locale: 'en-US',
      messages: {},
    };
    handleLocaleChange = handleLocaleChange.bind(this);
  }
  componentDidMount() {
    i18nLoader(state.locale, setState, this);
  }
  handleLocaleChange(e) {
    i18nLoader(e.target.value, setState, this);
  }
  render() {
    if (!state.areTranslationsLoaded) {
      return <div />;
    }
    return (
      <I18nProvider locale={state.locale} messages={state.messages}>
        <div>
          <span> Example Message Format: </span>
          <FormattedMessage id="Terra.ajax.error" />
        </div>
        <p>
          <span> Example Number Format: </span>
          <FormattedNumber value={parseFloat('1123432.123')} />
        </p>
        <p>
          <span>Example Date Format: </span>
          <FormattedDate value={new Date(1575476163287)} />
        </p>
        <label htmlFor="locale">
          {' '}
          Current locale:
          {state.locale}{' '}
        </label>
        <select id="locale" value={state.locale} onChange={handleLocaleChange}>
          <option value="en">en</option>
          <option value="en-GB">en-GB</option>
          <option value="en-US">en-US</option>
          <option value="de">de</option>
          <option value="es">es</option>
          <option value="fr">fr</option>
          <option value="nl">nl</option>
          <option value="pt">pt</option>
        </select>
      </I18nProvider>
    );
  }
}
export default () => <Demo />;
