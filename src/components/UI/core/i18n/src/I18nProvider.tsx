
import { IntlProvider } from "react-intl";
interface Properties {
  /**
   * The component that will be wrapped by i18n provider.
   */
  children: JSX.Element
  /**
   * The locale name.
   */
  locale: string
  /**
   * Translations messages object.
   */
  messages: {}
};
export const I18nProvider = (props: Properties { children, locale, messages }) => (
  <IntlProvider locale={locale} key={locale} messages={messages}>
    <>{children}</>
  </IntlProvider>
);

