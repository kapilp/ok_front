import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';

import * as KeyCode from 'keycode-js';
import { SvgIconAudio } from '../../icon/src/icon/IconAudio';
import { SvgIconDocuments } from '../../icon/src/icon/IconDocuments';
import { SvgIconExternalLink } from '../../icon/src/icon/IconExternalLink';
import { SvgIconImage } from '../../icon/src/icon/IconImage';
import { SvgIconVideoCamera } from '../../icon/src/icon/IconVideoCamera';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Hyperlink.module.scss';

const cx = classNamesBind.bind(styles);
export enum HyperlinkVariants {
  DEFAULT = 'default',
  EXTERNAL = 'external',
  IMAGE = 'image',
  VIDEO = 'video',
  AUDIO = 'audio',
  DOCUMENT = 'document',
}
const getHyperlinkIcon = (variant: HyperlinkVariants) => {
  switch (variant) {
    case HyperlinkVariants.AUDIO:
      return (
        <span className={cx('icon')}>
          <SvgIconAudio />
        </span>
      );
    case HyperlinkVariants.DOCUMENT:
      return (
        <span className={cx('icon')}>
          <SvgIconDocuments />
        </span>
      );
    case HyperlinkVariants.EXTERNAL:
      return (
        <span className={cx('icon')}>
          <SvgIconExternalLink />
        </span>
      );
    case HyperlinkVariants.IMAGE:
      return (
        <span className={cx('icon')}>
          <SvgIconImage />
        </span>
      );
    case HyperlinkVariants.VIDEO:
      return (
        <span className={cx('icon')}>
          <SvgIconVideoCamera />
        </span>
      );
    default:
      return null;
  }
};
interface IHyperlinkProps extends JSX.HTMLAttributes<Element> {
  /**
   * The content to display inside link.
   */
  children?: JSX.Element;
  /**
   * Sets the href of the link.
   */
  href?: string;
  /**
   * Whether or not the link should be disabled.
   */
  isDisabled?: boolean;
  /**
   * Whether or not the link should display an underline by default. Will still display an underline on hover and focus.
   */
  isUnderlineHidden?: boolean;
  /**
   * Callback function triggered when clicked.
   */
  onClick?: (e: MouseEvent) => void;
  /**
   * Callback function triggered when hyperlink loses focus.
   */
  onBlur?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when hyperlink gains focus.
   */
  onFocus?: (e: FocusEvent) => void;
  /**
   * Callback function triggered when key is pressed.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * Callback function triggered when key is released.
   */
  onKeyUp?: (e: KeyboardEvent) => void;
  /**
   * Sets the hyperlink variant. One of `default`, `external`, `image`, `video`, `audio`, `document`.
   */
  variant?: 'default' | 'external' | 'image' | 'video' | 'audio' | 'document';
}
const defaultProps = {
  isDisabled: false,
  variant: HyperlinkVariants.DEFAULT,
};
type HyperlinkState = {
  active: boolean;
  focused: boolean;
};
export const Hyperlink = (props: IHyperlinkProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);

  //context: any;

  const [state, setState] = createStore({ active: false, focused: false });

  const handleOnBlur = (event: FocusEvent) => {
    setState({ focused: false });
    if (props.onBlur) {
      props.onBlur(event);
    }
  };
  const handleKeyDown = (event: KeyboardEvent) => {
    // Add focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
    }
    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  };
  const handleKeyUp = (event: KeyboardEvent) => {
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }
    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  };

  //const theme = context;
  const ComponentType = props.isDisabled ? 'span' : 'a';

  const [p, customProps] = splitProps(props, [
    'children',
    'isDisabled',
    'isUnderlineHidden',
    'variant',
    'href',
    'onClick',
    'onBlur',
    'onFocus',
    'onKeyDown',
    'onKeyUp',
    'className',
  ]);
  let { target } = customProps; // Defaults to undefined if not set
  let { rel } = customProps; // Defaults to undefined if not set
  // If variant is set to external, we'll add target="_blank" and rel="noopener noreferrer"
  // unless user passes their own target or rel attribute
  if (!customProps.target && props.variant === 'external') {
    target = '_blank';
  }
  if (!customProps.rel && props.variant === 'external') {
    rel = 'noopener noreferrer';
  }
  // <Dynamic component={ComponentType} gives a console warning so used <a> only
  return (
    <a
      {...customProps}
      className={classNames(
        cx(
          'hyperlink',
          p.variant,
          { 'is-disabled': p.isDisabled },
          { 'is-underline-hidden': p.isUnderlineHidden },
          { 'is-active': state.active },
          { 'is-focused': state.focused },
          theme.className,
        ),
        p.className,
      )}
      aria-disabled={p.isDisabled}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
      onClick={p.onClick}
      onFocus={p.onFocus}
      href={p.isDisabled ? '' : p.href}
      target={target}
      rel={rel}
    >
      {p.children}
      {getHyperlinkIcon(p.variant as HyperlinkVariants)}
    </a>
  );
};
