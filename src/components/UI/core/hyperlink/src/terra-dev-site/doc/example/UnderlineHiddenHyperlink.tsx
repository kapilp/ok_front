import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Hyperlink } from '../../../Hyperlink';
export const UnderlineHiddenHyperlink = () => (
  <Hyperlink href="https://www.cerner.com" isUnderlineHidden>
    Underline hidden hyperlink
  </Hyperlink>
);
