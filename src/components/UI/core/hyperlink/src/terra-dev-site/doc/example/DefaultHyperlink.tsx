import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Hyperlink } from '../../../Hyperlink';

export const DefaultHyperlink = () => <Hyperlink href="https://www.cerner.com">Default hyperlink</Hyperlink>;
