import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Hyperlink from '../../../Hyperlink';
export default () => (
  <div role="main">
    <Hyperlink href="https://www.cerner.com" isDisabled>
      Disabled hyperlink
    </Hyperlink>
    <br />
    <Hyperlink href="https://www.cerner.com" isDisabled variant="document">
      Disabled icon hyperlink
    </Hyperlink>
  </div>
);
