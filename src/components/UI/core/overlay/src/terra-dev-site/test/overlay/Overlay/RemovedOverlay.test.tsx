import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Overlay from '../../../../Overlay';
type OverlayExampleState = {
  show?: boolean;
};
class OverlayExample extends React.Component<{}, OverlayExampleState> {
  constructor() {
    super();
    state = { show: true };
    handleTriggerOverlay = handleTriggerOverlay.bind(this);
    handleOnRequestESC = handleOnRequestESC.bind(this);
  }
  handleTriggerOverlay() {
    setState({ show: true });
  }
  handleOnRequestESC() {
    setState({ show: false });
  }
  render() {
    if (state.show) {
      return (
        <Overlay isOpen onRequestClose={handleOnRequestESC} id="fullscreen_overlay" zIndex="6000">
          <h3>The onRequestClose prop gives the overlay close behaviors.</h3>
          <br />
          <p>Close by clicking inside the overlay or pressing the ESC key.</p>
        </Overlay>
      );
    }
    return null;
  }
}
export default OverlayExample;
