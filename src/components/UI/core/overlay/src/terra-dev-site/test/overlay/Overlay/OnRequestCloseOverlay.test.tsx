import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Overlay from '../../../../Overlay';
import OverlayContainer from '../../../../OverlayContainer';
import styles from './OverlayTestCommon.module.scss';
const cx = classNames.bind(styles);
type OverlayExampleState = {
  show?: boolean;
  relative?: boolean;
  id?: string;
};
class OverlayExample extends React.Component<{}, OverlayExampleState> {
  forceUpdate: any;
  forceUpdateForTest: any;
  constructor() {
    super();
    state = { show: false };
    handleTriggerOverlay = handleTriggerOverlay.bind(this);
    handleTriggerFullScreenOverlay = handleTriggerFullScreenOverlay.bind(this);
    handleOnRequestESC = handleOnRequestESC.bind(this);
    forceUpdateForTest = () => {
      forceUpdate();
    };
  }
  componentDidMount() {
    document.addEventListener('overlay.forceUpdateForTest', forceUpdateForTest);
  }
  componentWillUnmount() {
    document.removeEventListener('overlay.forceUpdateForTest', forceUpdateForTest);
  }
  handleTriggerOverlay() {
    setState({
      show: true,
      relative: true,
      id: 'terra-Overlay--container',
    });
  }
  handleTriggerFullScreenOverlay() {
    setState({
      show: true,
      relative: false,
      id: 'terra-Overlay--fullscreen',
    });
  }
  handleOnRequestESC() {
    setState({ show: false });
  }
  addOverlay() {
    return (
      <Overlay isOpen={state.show} isRelativeToContainer={state.relative} onRequestClose={handleOnRequestESC} id={state.id} zIndex="6000">
        <h3>The onRequestClose prop gives the overlay close behaviors.</h3>
        <br />
        <p>Close by clicking inside the overlay or pressing the ESC key.</p>
      </Overlay>
    );
  }
  render() {
    return (
      <OverlayContainer className={cx('overlay-container2')} overlay={addOverlay()} id="test-overlay-container">
        <button type="button" id="trigger_container" onClick={handleTriggerOverlay}>
          Trigger Container Overlay
        </button>
        <button type="button" id="trigger_fullscreen" onClick={handleTriggerFullScreenOverlay}>
          Trigger Fullscreen Overlay
        </button>
      </OverlayContainer>
    );
  }
}
export default OverlayExample;
