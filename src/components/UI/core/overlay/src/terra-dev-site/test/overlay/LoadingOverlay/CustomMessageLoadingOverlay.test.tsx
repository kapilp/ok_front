import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import LoadingOverlay from '../../../../LoadingOverlay';
const CustomMessageLoadingOverlay = () => <LoadingOverlay isOpen message="Custom Loading Message" id="terra-LoadingOverlay" />;
export default CustomMessageLoadingOverlay;
