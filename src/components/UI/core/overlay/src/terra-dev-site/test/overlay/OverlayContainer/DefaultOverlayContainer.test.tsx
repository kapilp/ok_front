import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import OverlayContainer from '../../../../OverlayContainer';
const ContainerExample = () => (
  <OverlayContainer id="terra-OverlayContainer">
    OverlayContainer is a wrapper component that applies the css style `position: relative;` so the overlay relative to container displays correctly.
  </OverlayContainer>
);
export default ContainerExample;
