import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import OverlayContainer from '../../../../OverlayContainer';
import LoadingOverlay from '../../../../LoadingOverlay';
import styles from '../Overlay/OverlayTestCommon.module.scss';
const cx = classNames.bind(styles);
export default () => (
  <OverlayContainer
    className={cx('overlay-container1')}
    overlay={<LoadingOverlay isOpen backgroundStyle={LoadingOverlay.Opts.BackgroundStyles.LIGHT} isRelativeToContainer id="terra-LoadingOverlay" />}
  />
);
