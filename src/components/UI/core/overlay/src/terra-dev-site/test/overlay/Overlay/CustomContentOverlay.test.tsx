import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Overlay from '../../../../Overlay';
import OverlayContainer from '../../../../OverlayContainer';
import styles from './OverlayTestCommon.module.scss';
const cx = classNames.bind(styles);
type OverlayExampleState = {
  show?: boolean;
  toggle?: string;
  relative?: boolean;
  id?: string;
};
class OverlayExample extends React.Component<{}, OverlayExampleState> {
  constructor() {
    super();
    state = { show: false, toggle: 'true' };
    handleTriggerOverlay = handleTriggerOverlay.bind(this);
    handleTriggerFullScreenOverlay = handleTriggerFullScreenOverlay.bind(this);
    handleRandomClick = handleRandomClick.bind(this);
  }
  handleTriggerOverlay() {
    setState({
      show: true,
      relative: true,
      id: 'terra-Overlay--container',
    });
  }
  handleTriggerFullScreenOverlay() {
    setState({
      show: true,
      relative: false,
      id: 'terra-Overlay--fullscreen',
    });
  }
  handleRandomClick() {
    if (state.toggle === 'false') {
      setState({ toggle: 'true' });
    } else {
      setState({ toggle: 'false' });
    }
  }
  addOverlay() {
    return (
      <Overlay isOpen={state.show} isRelativeToContainer={state.relative} id={state.id}>
        <h3>Overlay with custom content.</h3>
        <button
          type="button"
          id="close_overlay"
          onClick={() => {
            setState({ show: false });
          }}
        >
          Close Overlay
        </button>
      </Overlay>
    );
  }
  render() {
    return (
      <div id="custom-content-example">
        <OverlayContainer className={cx('overlay-container2')} overlay={addOverlay()}>
          <button type="button" id="trigger_fullscreen" onClick={handleTriggerFullScreenOverlay}>
            Trigger Fullscreen Overlay
          </button>
          <button type="button" id="trigger_container" onClick={handleTriggerOverlay}>
            Trigger Container Overlay
          </button>
        </OverlayContainer>
        <br />
        <p>
          {' '}
          Outside of overlay container to prove functionality. Click the random button below after clicking the Trigger Container Overlay button to confirm an Overlay relative to
          container does not block outside interactions.{' '}
        </p>
        <button type="button" id="random_button" onClick={handleRandomClick}>
          A Random Button To Change Toggle State{' '}
        </button>
        <p>
          Random Button toggle State is <span id="random_state">{state.toggle}</span>
        </p>
      </div>
    );
  }
}
export default OverlayExample;
