import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { LoadingOverlay } from '../../../LoadingOverlay';
import { OverlayContainer } from '../../../OverlayContainer';
import { Button } from '../../../../../button/src/Button';
import classNames from 'classnames/bind';
import styles from './OverlayDocCommon.module.scss';

const cx = classNames.bind(styles);

export const LoadingOverlayExample = (props: {}) => {
  const [state, setState] = createStore({ show: false, isRelativeToContainer: false });
  const handleTriggerOverlay = () => {
    setState({ show: true, isRelativeToContainer: true });
    setTimeout(() => {
      setState({ show: false });
    }, 5000);
  };
  const handleTriggerFullScreenOverlay = () => {
    setState({ show: true, isRelativeToContainer: false });
    setTimeout(() => {
      setState({ show: false });
    }, 5000);
  };
  const addLoadingOverlay = () => {
    return <LoadingOverlay isOpen={state.show} isAnimated isRelativeToContainer={state.isRelativeToContainer} zIndex="6000" />;
  };

  return (
    <OverlayContainer className={cx('overlay-container')} overlay={addLoadingOverlay()}>
      <Button text="Trigger Container Overlay" onClick={handleTriggerOverlay} />
      <Button text="Trigger FullScreen Overlay" onClick={handleTriggerFullScreenOverlay} />
    </OverlayContainer>
  );
};
