import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { OverlayContainer } from '../../../OverlayContainer';
import { Overlay } from '../../../Overlay';
import { Button } from '../../../../../button/src/Button';
import classNames from 'classnames/bind';
import styles from './OverlayDocCommon.module.scss';

const cx = classNames.bind(styles);
type OverlayExampleState = {
  show?: boolean;
  relative?: boolean;
};
export const OverlayExample = () => {
  const [state, setState] = createStore({ show: false });
  const handleTriggerOverlay = () => {
    setState({ show: true, relative: true });
  };
  const handleTriggerFullScreenOverlay = () => {
    setState({ show: true, relative: false });
  };
  const handleOnRequestESC = () => {
    setState({ show: false });
  };
  const addOverlay = () => {
    return (
      <Overlay isOpen={state.show} isRelativeToContainer={state.relative} onRequestClose={handleOnRequestESC} zIndex="6000">
        <p>Close by clicking the overlay or pressing the ESC key.</p>
      </Overlay>
    );
  };

  return (
    <OverlayContainer className={cx('overlay-container')} overlay={addOverlay()}>
      <Button text="Trigger Container Overlay" onClick={handleTriggerOverlay} />
      <Button text="Trigger FullScreen Overlay" onClick={handleTriggerFullScreenOverlay} />
    </OverlayContainer>
  );
};
