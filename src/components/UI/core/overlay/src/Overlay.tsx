import { JSX, createEffect, createSignal, onCleanup, mergeProps, splitProps, useContext, untrack } from 'solid-js';
import { Portal, render } from 'solid-js/web';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';

import * as KeyCode from 'keycode-js';
// import 'mutationobserver-shim';
import { FocusTrap } from '../../../FocusTrap';

import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import { OverlayContainer } from './OverlayContainer';
// import './_contains-polyfill';
// import './_matches-polyfill';
import styles from './Overlay.module.scss';

const cx = classNamesBind.bind(styles);
const BackgroundStyles = {
  LIGHT: 'light',
  DARK: 'dark',
  CLEAR: 'clear',
};
const zIndexes = ['100', '6000', '7000', '8000', '9000'];
interface IOverlayProps extends JSX.HTMLAttributes<Element> {
  /**
   * The content to be displayed within the overlay.
   */
  children?: JSX.Element;
  /**
   * Indicates if the overlay is open.
   */
  isOpen?: boolean;
  /**
   * The visual theme to be applied to the overlay background. Accepts 'light', 'dark', and 'clear'.
   */
  backgroundStyle?: 'light' | 'dark' | 'clear';
  /**
   * Indicates if the overlay content is scrollable.
   */
  isScrollable?: boolean;
  /**
   * Callback triggered on overlay click or ESC key. Setting this enables close behavior.
   */
  onRequestClose?: () => void;
  /**
   * Indicates if the overlay is relative to the triggering container.
   */
  isRelativeToContainer?: boolean;
  /**
   * Used to select the root mount DOM node. This is used to help prevent focus from shifting outside of the overlay when it is opened in a portal.
   */
  rootSelector?: string;
  /**
   * Z-Index layer to apply to the ModalContent and ModalOverlay. Valid values are '100', '6000', '7000', '8000', or '9000'.
   */
  zIndex?: '100' | '6000' | '7000' | '8000' | '9000';
}
const defaultProps = {
  children: null,
  isOpen: false,
  backgroundStyle: BackgroundStyles.LIGHT,
  isScrollable: false,
  isRelativeToContainer: false,
  onRequestClose: undefined,
  rootSelector: '#root',
  zIndex: '100',
};

export const Overlay = (props: IOverlayProps) => {
  props = mergeProps({}, defaultProps, props);

  const [container, setcontainer_] = createSignal<HTMLDivElement>();
  let overflow: any;

  createEffect(
    untrack(() => {
      document.addEventListener('keydown', shouldHandleESCKeydown);
      if (props.isOpen) {
        disableContainerChildrenFocus();
      }
    }),
  );

  createEffect(() => {
    if (props.isOpen) {
      disableContainerChildrenFocus();
    } else if (!props.isOpen) {
      enableContainerChildrenFocus();
    }
  });
  onCleanup(() => {
    document.removeEventListener('keydown', shouldHandleESCKeydown);
    enableContainerChildrenFocus();
  });
  const setContainer = (el: HTMLDivElement) => {
    if (!el) {
      return;
    } // Ref callbacks happen on mount and unmount, element is null on unmount
    overflow = document.documentElement.style.overflow;
    if (props.isRelativeToContainer) {
      setTimeout(() => setcontainer_(el.parentNode));
    } else {
      setcontainer_(null);
    }
  };
  function disableContainerChildrenFocus() {
    if (props.isRelativeToContainer) {
      if (container() && container().querySelector('[data-terra-overlay-container-content]')) {
        container().querySelector('[data-terra-overlay-container-content]').setAttribute('inert', '');
      }
    } else {
      const selector = props.rootSelector;
      if (document.querySelector(selector) && !document.querySelector(selector).hasAttribute('data-overlay-count')) {
        document.querySelector(selector).setAttribute('data-overlay-count', '1');
        document.querySelector(selector).setAttribute('inert', '');
      } else if (document.querySelector(selector) && document.querySelector(selector).hasAttribute('data-overlay-count')) {
        const inert = +document.querySelector(selector).getAttribute('data-overlay-count');
        document.querySelector(selector).setAttribute('data-overlay-count', `${inert + 1}`);
        document.querySelector(selector).setAttribute('inert', '');
      }
      document.documentElement.style.overflow = 'hidden';
    }
  }
  function enableContainerChildrenFocus() {
    if (props.isRelativeToContainer) {
      if (container() && container().querySelector('[data-terra-overlay-container-content]')) {
        container().querySelector('[data-terra-overlay-container-content]').removeAttribute('inert');
        container().querySelector('[data-terra-overlay-container-content]').removeAttribute('aria-hidden');
      }
    } else {
      const selector = props.rootSelector;
      if (document.querySelector(selector)) {
        // Guard for Jest testing
        const inert = +document.querySelector(selector).getAttribute('data-overlay-count');
        if (inert === 1) {
          document.querySelector(selector).removeAttribute('data-overlay-count');
          document.querySelector(selector).removeAttribute('inert');
          document.querySelector(selector).removeAttribute('aria-hidden');
        } else if (inert && inert > 1) {
          document.querySelector(selector).setAttribute('data-overlay-count', `${inert - 1}`);
        }
      }
      document.documentElement.style.overflow = overflow;
    }
  }
  function shouldHandleESCKeydown(event: Event) {
    if (props.isOpen && event.keyCode === KeyCode.KEY_ESCAPE) {
      handleCloseEvent(event);
      event.preventDefault();
    }
  }
  function shouldHandleClick(event: Event) {
    if (props.isOpen) {
      handleCloseEvent(event);
    }
  }
  const handleCloseEvent = (event: Event) => {
    if (props.onRequestClose) {
      props.onRequestClose(event);
    }
  };
  const [p, customProps] = splitProps(props, ['children', 'isOpen', 'backgroundStyle', 'isScrollable', 'isRelativeToContainer', 'onRequestClose', 'rootSelector', 'zIndex']);
  const theme = useContext(ThemeContext);
  const type = () => (p.isRelativeToContainer ? 'container' : 'fullscreen');

  /*let zIndexLayer = '100';
  if (zIndexes.indexOf(zIndex) >= 0) {
    zIndexLayer = zIndex;
  }*/
  const OverlayClassNames = () =>
    classNames(cx(['overlay', type(), p.backgroundStyle, { scrollable: p.isScrollable }, `layer-${p.zIndex}`, theme.className]), customProps.className);
  /*
          tabIndex set to 0 allows screen readers like VoiceOver to read overlay content when its displayed.
          Key events are added on mount.
        */
  /* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, jsx-a11y/no-noninteractive-tabindex */
  const overlayComponent = () => (
    <div {...customProps} ref={setContainer} onClick={shouldHandleClick} className={OverlayClassNames()} tabIndex="0">
      <div className={cx('content')}>{p.children}</div>
    </div>
  );

  const backgroundScrollContent = () => (
    <div className={cx('background-scroll-content')}>
      <div className={cx('inner')} />
    </div>
  );
  return (
    <>
      {p.isOpen &&
        (p.isRelativeToContainer ? (
          overlayComponent()
        ) : (
          <Portal>
            {backgroundScrollContent()}
            <FocusTrap>
              <div>{overlayComponent}</div>
            </FocusTrap>
          </Portal>
        ))}
    </>
  );
};
const Opts = { BackgroundStyles, zIndexes };

Overlay.Opts = Opts;
Overlay.Container = OverlayContainer;
