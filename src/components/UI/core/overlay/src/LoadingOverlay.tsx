import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
// import { FormattedMessage } from 'react-intl';
import IconSpinner from 'terra-icon/lib/icon/IconSpinner';
import { Overlay } from './Overlay';
import classNames from 'classnames/bind';
import styles from './Overlay.module.scss';
import { SvgIconSpinner } from '../../icon/src/icon/IconSpinner';
const cx = classNames.bind(styles);
const { BackgroundStyles } = Overlay.Opts;
interface Properties {
  /**
   * The visual theme to be applied to the overlay background. Accepts 'light', 'dark', and 'clear'.
   */
  backgroundStyle: 'light' | 'dark' | 'clear';
  /**
   * Indicates if the icon spinner should be animated.
   */
  isAnimated: boolean;
  /**
   * Indicates if the overlay is open.
   */
  isOpen: boolean;
  /**
   * Indicates if the overlay is relative to the triggering container.
   */
  isRelativeToContainer: boolean;
  /**
   * The message to be displayed within the overlay.
   */
  message: string;
  /**
   * Used to select the root mount DOM node. This is used to help prevent focus from shifting outside of the overlay when it is opened in a portal.
   */
  rootSelector: string;
  /**
   * Z-Index layer to apply to the ModalContent and ModalOverlay. Valid values are '100', '6000', '7000', '8000', or '9000'.
   */
  zIndex: '100' | '6000' | '7000' | '8000' | '9000';
}
const defaultProps = {
  isAnimated: false,
  isOpen: false,
  backgroundStyle: BackgroundStyles.LIGHT,
  isRelativeToContainer: false,
  rootSelector: '#root',
};
export const LoadingOverlay = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['isAnimated', 'message', 'rootSelector', 'onRequestClose']);

  return (
    <Overlay {...customProps} className={cx('loading-overlay', customProps.className)} rootSelector={p.rootSelector} aria-live="polite">
      <SvgIconSpinner className={cx('icon')} isSpin={p.isAnimated} height="36" width="36" />
      {p.message !== undefined ? <div className={cx('message')}>{p.message}</div> : <div className={cx('message')}>{'Loading'}</div>}
    </Overlay>
  );
};

LoadingOverlay.Opts = Overlay.Opts;
