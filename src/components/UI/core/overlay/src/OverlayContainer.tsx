import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Overlay.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The overlay and the content to be displayed within the overlay.
   */
  overlay?: JSX.Element;
  /**
   * The elements that overlay should hide when overlay isOpen.
   * elements which are not included in overlay will be wrapped within children for better use of accessibility.
   */
  children?: JSX.Element;
}
const defaultProps = {
  overlay: null,
  children: null,
};
export const OverlayContainer = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['overlay', 'children']);
  const OverlayContainerClassNames = cx(['overlay-container', customProps.className]);
  return (
    <div {...customProps} className={OverlayContainerClassNames}>
      {p.overlay}
      <div data-terra-overlay-container-content className={classNames('container-content')}>
        {p.children}
      </div>
    </div>
  );
};
