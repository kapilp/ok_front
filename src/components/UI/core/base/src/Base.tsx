// import { I18nProvider, i18nLoader } from "terra-i18n"; // Todo fix this
import './baseStyles';
import {  JSX, mergeProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
interface Properties {
  /**
   * The component(s) that will be wrapped by `<Base />`.
   */
  children: JSX.Element;
  /**
   * The locale name.
   */
  locale: string;
  /**
   * Customized translations provided by consuming application only for current locale.
   */
  /* eslint-disable consistent-return */
  /*customMessages: (props, propName, componentName) => {
    if (
      Object.keys(props[propName]).length !== 0 &&
      props.locale === undefined
    ) {
      return new Error(
        `Missing locale prop for ${propName} in ${componentName} props`
      );
    }
  },*/
  /**
   * The component(s) that will be wrapped by `<Base />` ONLY
   * in the event that translations have not been loaded yet.
   * NOTE: Absolutely no locale-dependent logic should be
   * utilized in this placeholder.
   */
  translationsLoadingPlaceholder: JSX.Element;
}
const defaultProps = {
  customMessages: {},
  strictMode: false,
};
interface IBaseProps extends JSX.HTMLAttributes<Element> {
  customMessages?: any;
  strictMode?: any;
  translationsLoadingPlaceholder?: any;
  locale?: any;
}
type BaseState = {
  areTranslationsLoaded?: boolean;
  locale?: any;
  messages?: {};
};
export const Base = (props: IBaseProps) => {
  props = mergeProps({}, defaultProps, props);

  const [state, setState] = createStore({
    areTranslationsLoaded: false,
    locale: props.locale,
    messages: {},
  } as BaseState);

  /*componentDidMount() {
    if (props.locale !== undefined) {
      try {
        i18nLoader(props.locale, setState, this);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
    }
  }*/

  /*componentDidUpdate(prevProps) {
    if (
      props.locale !== undefined &&
      props.locale !== prevProps.locale
    ) {
      try {
        i18nLoader(props.locale, setState, this);
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
    }
  }*/

  /*if (!state.areTranslationsLoaded)
      return <div>{props.translationsLoadingPlaceholder}</div>;
    return (
      <I18nProvider locale={state.locale} messages={ { ...state.messages, ...props.customMessages }}>
        {props.children}
      </I18nProvider>
    );*/
  return props.children;
};
