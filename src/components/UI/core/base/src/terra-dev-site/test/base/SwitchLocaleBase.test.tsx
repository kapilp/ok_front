import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { FormattedMessage } from 'react-intl';
import Base from '../../../Base';
const customMessages = {
  en: {
    'App.locale': 'en',
  },
  'en-US': {
    'App.locale': 'en-US',
  },
};
type SwitchLocaleBaseState = {
  locale?: string;
};
class SwitchLocaleBase extends React.Component<{}, SwitchLocaleBaseState> {
  constructor(props) {
    super(props);
    state = {
      locale: 'en',
    };
    handleSwitchLocale = handleSwitchLocale.bind(this);
  }
  handleSwitchLocale() {
    if (state.locale !== 'en') {
      setState({ locale: 'en' });
    } else {
      setState({ locale: 'en-US' });
    }
  }
  render() {
    return (
      <Base locale={state.locale} customMessages={customMessages[state.locale]}>
        <div id="message">
          <FormattedMessage id="App.locale" />
        </div>
        <button type="button" id="switch" onClick={handleSwitchLocale}>
          Switch
        </button>
      </Base>
    );
  }
}
export default SwitchLocaleBase;
