import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Base from '../../../Base';
const BaseNoTranslationsLoaded = () => {
  const noTranslations = (
    <Base locale="gabcdef" translationsLoadingPlaceholder="No Translations String">
      <div>Translations</div>
    </Base>
  );
  return noTranslations;
};
export default BaseNoTranslationsLoaded;
