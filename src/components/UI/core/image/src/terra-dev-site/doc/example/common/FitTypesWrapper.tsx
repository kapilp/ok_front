import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
// render props is great:
// https://levelup.gitconnected.com/understanding-react-render-props-by-example-71f2162fd0f2
export const FitTypesWrapper = props => {
  const [state, setState] = createStore({ fitType: 'fill', variantType: 'default' });

  const handleOnSelect = event => {
    setState({ [event.target.name]: event.target.value });
  };

  return (
    <div>
      {props.children({ fit: state.fitType, variant: state.variantType })}
      <p>
        <label htmlFor="fitType">Select a Fit Type:</label>
      </p>
      <select id="fitType" name="fitType" value={state.fitType} onChange={handleOnSelect}>
        <option value="fill">fill</option>
        <option value="cover">cover</option>
        <option value="contain">contain</option>
        <option value="scale-down">scale-down</option>
        <option value="none">none</option>
      </select>
      <p>
        <label htmlFor="variantType">Select a Variant Type:</label>
      </p>
      <select id="variantType" name="variantType" value={state.variantType} onChange={handleOnSelect}>
        <option value="default">default</option>
        <option value="circle">circle</option>
        <option value="rounded">rounded</option>
        <option value="thumbnail">thumbnail</option>
      </select>
    </div>
  );
};
