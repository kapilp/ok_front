import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Image } from '../../../Image';
import styles from './ImageFitTypes.module.scss';
import exampleImage from '../../assets/170x251.jpg';
import { FitTypesWrapper } from './common/FitTypesWrapper';
const cx = classNames.bind(styles);
interface Properties {
  fit: string;
  variant: string;
}
export const ImageFitTypes = props => (
  <FitTypesWrapper>
    {p => (
      <div className={cx('image-container')}>
        <Image alt="Toggle fit image" className={cx('image')} src={exampleImage} fit={p.fit} variant={p.variant} />
      </div>
    )}
  </FitTypesWrapper>
);
