import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Image from '../../../Image';
import placeholder150x150 from '../../assets/150x150.jpg';
type ImageLoadingState = {
  errorImageId?: undefined;
  loadedImageId?: undefined;
};
class ImageLoading extends React.Component<{}, ImageLoadingState> {
  constructor(props) {
    super(props);
    state = { errorImageId: undefined, loadedImageId: undefined };
  }
  render() {
    return (
      <div>
        <h2>Loading Image Examples</h2>
        <p>Image is successfully loaded.</p>
        <div>
          <Image
            id={state.loadedImageId}
            alt="loading image"
            src={placeholder150x150}
            onLoad={() => {
              setState({ loadedImageId: 'loadedImage' });
            }}
            onError={() => {
              setState({ errorImageId: 'errorImage' });
            }}
          />
        </div>
        <p>Image fails to load.</p>
        <div>
          <Image
            id={state.errorImageId}
            src="invalid.jpg"
            onLoad={() => {
              setState({ loadedImageId: 'loadedImage' });
            }}
            onError={() => {
              setState({ errorImageId: 'errorImage' });
            }}
            alt="Alt text for image with invalid source"
          />
        </div>
      </div>
    );
  }
}
export default ImageLoading;
