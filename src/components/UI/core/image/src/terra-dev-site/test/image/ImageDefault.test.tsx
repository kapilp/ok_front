import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Image from '../../../Image';
import placeholder150x150 from '../../assets/150x150.jpg';
export default () => <Image src={placeholder150x150} alt="example image" />;
