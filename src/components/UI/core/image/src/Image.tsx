import {  JSX, Match, mergeProps, splitProps, Switch, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
//import objectFitImages from 'object-fit-images'; // Added polyfill for IE.
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './Image.module.scss';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);
export enum ImageVariant {
  DEFAULT = 'default',
  ROUNDED = 'rounded',
  CIRCLE = 'circle',
  THUMBNAIL = 'thumbnail',
}
export enum FitTypes {
  COVER = 'cover',
  SCALEDOWN = 'scale-down',
  FILL = 'fill',
  CONTAIN = 'contain',
  NONE = 'none',
}
interface IImageProps extends JSX.HTMLAttributes<Element> {
  /**
   * The source string for the image which will be loaded and displayed.
   */
  src: string;
  /**
   * Sets the style of the image from the following values; `default`, `rounded`, `circle`, `thumbnail`.
   */
  variant?: ImageVariant;
  /**
   * Sets the fluid behavior of the image, which is `nonfluid` by default.
   */
  isFluid?: boolean;
  /**
   * The text content that specifies an alternative text for an image.
   */
  alt: string;
  /**
   * A React element which will be displayed during loading and in case of src load failure.
   */
  placeholder?: JSX.Element;
  /**
   * Sets the height of the image.
   */
  height?: string;
  /**
   * Sets the width of the image.
   */
  width?: string;
  /**
   * Function to be executed when the image load is successful.
   */
  onLoad?: () => void;
  /**
   * Function to be executed when the image load errors.
   */
  onError?: () => void;
  /**
   * Sets the `object-fit` style of the image from the following values; `cover`, `contain`, `fill`, `scale-down`, `none`.
   */
  fit?: FitTypes;
}
/* eslint-disable react/default-props-match-prop-types */
const defaultProps = {
  variant: 'default',
  isFluid: false,
  alt: ' ',
  fit: 'fill',
};

type ImageState = {
  isLoading?: boolean;
  isError?: boolean;
  prevPropsSrc?: any;
};
/* eslint-enable react/default-props-match-prop-types */
export const Image = (props: IImageProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  let ImageRef: any;
  //let context: any;
  const [state, setState] = createStore({
    isLoading: true,
    isError: false,
    prevPropsSrc: props.src,
  } as ImageState);

  //componentDidUpdate() {
  //objectFitImages(ImageRef);
  //}
  /*static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.src !== prevState.prevPropsSrc) {
      return {
        isLoading: true,
        isError: false,
        prevPropsSrc: nextProps.src,
      };
    }
    return null;
  }*/
  const handleOnLoad = () => {
    setState({ isLoading: false });
    if (props.onLoad !== undefined) {
      props.onLoad();
    }
  };
  const handleOnError = () => {
    setState({ isLoading: false, isError: true });

    if (props.onError !== undefined) {
      props.onError();
    }
  };

  const [extracted, customProps] = splitProps(props, ['src', 'variant', 'isFluid', 'alt', 'placeholder', 'height', 'width', 'onLoad', 'onError', 'fit']);
  const CreateImage = () => {
    return (
      <img
        {...customProps}
        src={props.src}
        alt={props.alt}
        height={props.height}
        width={props.width}
        onLoad={handleOnLoad}
        onError={handleOnError}
        className={classNames(
          cx('image', props.fit, props.variant, theme.className, { fluid: props.isFluid }, { hidden: props.placeholder && state.isLoading }),
          customProps.className,
        )}
        ref={ImageRef}
      />
    );
  };
  //const theme = context;

  //delete customProps.className; // This line remove prop on CreateImage Function
  /*if (!state.isLoading) {
    objectFitImages(ImageRef.current);
  }*/

  return (
    <Switch fallback={<CreateImage />}>
      <Match when={props.placeholder && state.isLoading}>
        <CreateImage />
        {props.placeholder}
      </Match>
      <Match when={state.isError}>{props.placeholder}</Match>
    </Switch>
  );
};
