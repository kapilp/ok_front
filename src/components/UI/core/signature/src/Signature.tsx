import classNamesBind from 'classnames/bind';
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import styles from './Signature.module.scss';
const cx = classNamesBind.bind(styles);
const LINEWIDTHS = {
  EXTRA_FINE: 1,
  FINE: 2,
  MEDIUM: 4,
  HEAVY: 6
};
const MOUSECODES = {
  LEFTCLICK: 0
};
interface Properties {
  /**
   * The line width to use when drawing the signature on the canvas.
   * One of Signature.Opts.Width.EXTRA_FINE, Signature.Opts.Width.FINE, Signature.Opts.Width.MEDIUM, Signature.Opts.Width.HEAVY.
   */
  lineWidth: PropTypes.oneOf([
    LINEWIDTHS.EXTRA_FINE,
    LINEWIDTHS.FINE,
    LINEWIDTHS.MEDIUM,
    LINEWIDTHS.HEAVY
  ]),
  /**
   * Line segments that define signature.
   */
  // eslint-disable-next-line react/forbid-prop-types
  lineSegments: PropTypes.array,
  /**
   * A callback function to execute when a line segment is drawn. The first parameter is the event, the
   * second parameter is all the line segments, and the last parameter is the most recent line segment drawn.
   */
  onChange: (e: Event) => void
};
const defaultProps = {
  lineWidth: LINEWIDTHS.FINE,
  lineSegments: [],
  onChange: undefined
};
interface ISignatureProps extends JSX.HTMLAttributes<Element> {
  lineSegments?: any;
  lineWidth?: any;
  onChange?: any;
  custProps?: any;
}
type SignatureState = {
  lineSegments?: any,
  lastLineSegments?: undefined[],
  painting?: boolean,
  length?: any
};
export const Signature extends React.Component<ISignatureProps, SignatureState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  canvas: any;
  canvasRect: any;
  context: any;
  constructor(props) {
    super(props);
    state = { lineSegments: props.lineSegments };
    mouseInBounds = mouseInBounds.bind(this);
    mouseDown = mouseDown.bind(this);
    mouseUp = mouseUp.bind(this);
    mouseXY = mouseXY.bind(this);
    mouseLeave = mouseLeave.bind(this);
    addLine = addLine.bind(this);
    draw = draw.bind(this);
    drawSignature = drawSignature.bind(this);
    clearSignature = clearSignature.bind(this);
    updateDimensions = updateDimensions.bind(this);
  }
  componentDidMount() {
    if ("ontouchstart" in document.documentElement) {
      canvas.addEventListener("touchstart", mouseDown, false);
      canvas.addEventListener("touchmove", mouseXY, true);
      canvas.addEventListener("touchend", mouseUp, false);
      document.body.addEventListener("touchleave", mouseLeave, false);
      document.body.addEventListener("touchcancel", mouseUp, false);
    } else {
      canvas.addEventListener("mousedown", mouseDown);
      canvas.addEventListener("mousemove", mouseXY);
      document.body.addEventListener("mouseleave", mouseLeave, false);
      document.body.addEventListener("mouseup", mouseUp);
    }
    const context = canvas.getContext("2d");
    context.lineWidth = props.lineWidth;
    updateDimensions();
    canvas.addEventListener("resize", updateDimensions);
  }
  componentDidUpdate(prevProps) {
    if (
      props.lineSegments !== prevProps.lineSegments ||
      props.lineWidth !== prevProps.lineWidth
    ) {
      // eslint-disable-next-line react/no-did-update-set-state
      setState({ lineSegments: props.lineSegments });
      drawSignature(props.lineSegments, props.lineWidth);
    }
  }
  componentWillUnmount() {
    if ("ontouchstart" in document.documentElement) {
      canvas.removeEventListener("touchstart", mouseDown);
      canvas.removeEventListener("touchmove", mouseXY);
      canvas.removeEventListener("touchend", mouseUp);
      document.body.removeEventListener("touchleave", mouseLeave);
      document.body.removeEventListener("touchcancel", mouseUp);
    } else {
      canvas.removeEventListener("mousedown", mouseDown);
      canvas.removeEventListener("mousemove", mouseXY);
      document.body.removeEventListener("mouseleave", mouseLeave);
      document.body.removeEventListener("mouseup", mouseUp);
    }
    canvas.removeEventListener("resize", updateDimensions);
  }
  mouseInBounds(event) {
    const rect = canvasRect;
    return (
      rect.top < event.pageY &&
      rect.left < event.pageX &&
      rect.bottom > event.pageY &&
      rect.right > event.pageX
    );
  }
  mouseDown(event) {
    if ("button" in event && event.button !== MOUSECODES.LEFTCLICK) {
      return;
    }
    setState({ lastLineSegments: [], painting: true });
    canvasRect = canvas.getBoundingClientRect();
    addLine(
      event.pageX - canvasRect.left,
      event.pageY - canvasRect.top,
      false
    );
    draw();
  }
  mouseUp(event) {
    setState({ painting: false });
    if (props.onChange) {
      props.onChange(
        event,
        state.lineSegments,
        state.lastLineSegments
      );
    }
  }
  mouseXY(event) {
    if (state.painting && mouseInBounds(event)) {
      addLine(
        event.pageX - canvasRect.left,
        event.pageY - canvasRect.top,
        true
      );
      draw();
    }
  }
  mouseLeave(event) {
    if (state.painting) {
      setState({ painting: false });
      if (props.onChange) {
        props.onChange(
          event,
          state.lineSegments,
          state.lastLineSegments
        );
      }
    }
  }
  addLine(x, y, dragging) {
    let newSegment;
    if (dragging) {
      const lastLineSegment = state.lineSegments[
        state.lineSegments.length - 1
      ];
      newSegment = {
        x1: lastLineSegment.x2,
        y1: lastLineSegment.y2,
        x2: x,
        y2: y
      };
    } else {
      newSegment = {
        x1: x,
        y1: y,
        x2: x,
        y2: y
      };
    }
    // Record new line segment
    setState(prevState => ({
      lineSegments: [...prevState.lineSegments, newSegment],
      lastLineSegments: [...prevState.lastLineSegments, newSegment]
    }));
  }
  draw() {
    const context = canvas.getContext("2d");
    if (state.lineSegments.length > 0) {
      const lastLineSegment = state.lineSegments[
        state.lineSegments.length - 1
      ];
      const style = window.getComputedStyle(canvas);
      const color = style.getPropertyValue("color");
      context.lineJoin = "round";
      context.beginPath();
      context.moveTo(lastLineSegment.x1, lastLineSegment.y1);
      context.lineTo(lastLineSegment.x2, lastLineSegment.y2);
      context.strokeStyle = color;
      context.stroke();
    }
  }
  drawSignature(lineSegments, lineWidth) {
    const context = canvas.getContext("2d");
    const style = window.getComputedStyle(canvas);
    const color = style.getPropertyValue("color");
    context.lineJoin = "round";
    context.lineWidth = lineWidth;
    // clear canvas
    context.clearRect(0, 0, canvasRect.width, canvasRect.height);
    // iterate and draw all recorded line segments
    const segmentCount = lineSegments.length;
    for (let i = 0; i < segmentCount; i += 1) {
      context.beginPath();
      context.moveTo(lineSegments[i].x1, lineSegments[i].y1);
      context.lineTo(lineSegments[i].x2, lineSegments[i].y2);
      context.strokeStyle = color;
      context.stroke();
    }
  }
  clearSignature() {
    setState({ lineSegments: [] });
    const context = canvas.getContext("2d");
    context.clearRect(0, 0, canvasRect.width, canvasRect.height);
  }
  updateDimensions() {
    setTimeout(() => {
      canvasRect = canvas.getBoundingClientRect();
      canvas.width = canvasRect.width;
      canvas.height = canvasRect.height;
      drawSignature(state.lineSegments);
    }, 250);
  }
  render() {
    const theme = context;
    const { lineSegments, lineWidth, onChange, ...custProps } = props;
    return (
      <canvas
        {...custProps}
        className={cx( "signature", theme.className)}
        ref={node => {
          canvas = node;
        }}
      />
    );
  }
}

Signature.Opts = {};
Signature.Opts.Width = LINEWIDTHS;

