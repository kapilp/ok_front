import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Signature from '../../../Signature';
import styles from './SignatureDefaultTest.module.scss';
const cx = classNames.bind(styles);
type SignatureDefaultState = {
  lineSegments?: undefined[];
  lineWidth?: any;
};
class SignatureDefault extends React.Component<{}, SignatureDefaultState> {
  signature: any;
  constructor() {
    super();
    state = { lineSegments: [], lineWidth: Signature.Opts.Width.FINE };
  }
  render() {
    return (
      <div>
        <div id="othersection" />
        <div className={cx('signature-wrapper')}>
          <Signature
            id="drawline"
            lineWidth={state.lineWidth}
            lineSegments={state.lineSegments}
            ref={instance => {
              signature = instance;
            }}
          />
        </div>
      </div>
    );
  }
}
export default SignatureDefault;
