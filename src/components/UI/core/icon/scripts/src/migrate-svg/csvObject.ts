import { TerraIcon } from "../config";
class csvObject {
  isBidi: boolean;
  isSpin: boolean;
  isThemeable: boolean;
  name: any;
  svgDest: string;
  svgSrc: string;
  constructor(name, filepath, themeable, bidi) {
    this.name = name;
    this.svgSrc = `node_modules/one-cerner-style-icons/${filepath}`;
    this.svgDest = `${TerraIcon.svgDir}${name}.svg`;
    this.isThemeable = !!themeable;
    this.isBidi = bidi === "bi-directional";
    this.isSpin = name === "spinner";
  }
}
export default csvObject;
