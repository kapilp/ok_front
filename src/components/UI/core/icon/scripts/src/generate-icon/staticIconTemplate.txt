/* eslint-disable */

import classNames from 'classnames/bind';
import { IconBase, IconBaseProperties } from '../IconBase';
import styles from '../<%= icon.name %>.module.scss';

const cx = classNames.bind(styles);

const SvgIcon = (customProps) => {
  const attributes = Object.assign({}, customProps);
  const iconClassNames = cx([
    '<%= icon.name %>',
    customProps.className,
  ]);

  return (
    <IconBase className={iconClassNames} {...attributes}>
      <%= icon.children %>
    </IconBase>
  );
};

SvgIcon.displayName = <%= JSON.stringify(icon.name) %>;
SvgIcon.defaultProps = <%= JSON.stringify(icon.attributes) %>;


/* eslint-enable */
