import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Icon.module.scss';
const cx = classNamesBind.bind(styles);

export interface IconBaseProperties extends JSX.HTMLAttributes<Element> {
  /**
   * Should the svg mirror when dir="rtl".
   */
  isBidi?: boolean;
  /**
   * Should the SVG rotate.
   */
  isSpin?: boolean;
  /**
   * Child nodes.
   */
  children?: JSX.Element;
  /**
   * Height of SVG.
   */
  height?: string;
  /**
   * Width of SVG.
   */
  width?: string;
  /**
   * String that labels the current element. If 'aria-label' is present,
   * role is set to 'img' and aria-hidden is removed.
   */
  ariaLabel?: string;
  /**
   * Focusable attribute. IE 10/11 are focusable without this attribute.
   */
  focusable?: string;
}
const defaultProps = {
  isBidi: false,
  isSpin: false,
  children: null,
  height: '1em',
  width: '1em',
  ariaLabel: null,
  focusable: 'false',
};
export const IconBase = (props: IconBaseProperties) => {
  props = mergeProps({}, defaultProps, props);

  const theme = useContext(ThemeContext);
  const addAnimationStyles = () => {
    if (props.isSpin) {
      const css =
        '@-webkit-keyframes tui-spin-ltr{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(359deg);transform:rotate(359deg)}}@keyframes tui-spin-ltr{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(359deg);transform:rotate(359deg)}}@-webkit-keyframes tui-spin-rtl{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(-359deg);transform:rotate(-359deg)}}@keyframes tui-spin-rtl{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(-359deg);transform:rotate(-359deg)}}[dir=ltr] .tui-Icon.is-spin {-webkit-animation: tui-spin-ltr 2s infinite linear;animation: tui-spin-ltr 2s infinite linear;}[dir=rtl] .tui-Icon.is-spin {-webkit-animation: tui-spin-rtl 2s infinite linear;animation: tui-spin-rtl 2s infinite linear;}';
      const head = document.head || document.getElementsByTagName('head')[0];
      const style = document.createElement('style');
      style.type = 'text/css';
      style.id = 'terra-icon-animation';
      // If we haven't written the styles to the DOM yet, add them, otherwise don't do this for subsequent spinner icons.
      if (!document.getElementById(style.id)) {
        style.appendChild(document.createTextNode(css));
        head.appendChild(style);
      }
    }
  };
  addAnimationStyles();
  // append to existing classNames
  const classes = () => classNames(cx('icon', { 'is-bidi': props.isBidi }, { 'is-spin': props.isSpin }, theme.className), props.className);

  function getAttributes() {
    // aria-label is present, remove aria-hidden, set role to img
    const attributes = {};
    if (props.ariaLabel) {
      attributes['aria-label'] = props.ariaLabel;
      attributes.role = 'img';
      attributes['aria-hidden'] = null;
    } else {
      attributes['aria-hidden'] = 'true';
    }
    return attributes;
  }

  const [extracted, customProps] = splitProps(props, ['isBidi', 'isSpin', 'children', 'height', 'width', 'ariaLabel', 'focusable']);
  return (
    <svg {...{ ...customProps, ...getAttributes() }} height={props.height} width={props.width} focusable={props.focusable} className={classes()}>
      {props.children}
    </svg>
  );
};
