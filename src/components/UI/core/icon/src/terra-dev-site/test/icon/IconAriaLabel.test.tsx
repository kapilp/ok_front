import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import IconAlert from '../../../icon/IconAlert';
const IconAriaLabel = () => (
  <div>
    <IconAlert ariaLabel="Alert" />
  </div>
);
export default IconAriaLabel;
