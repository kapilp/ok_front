import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import IconAdd from '../../../icon/IconAdd';
const IconDefault = () => (
  <div>
    <h3>Default Icon</h3>
    <IconAdd id="icon-default" />
  </div>
);
export default IconDefault;
