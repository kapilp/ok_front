/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconTriangleSymbol = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M24 4l24 40H0L24 4z"></path>
    </IconBase>
  );
};
