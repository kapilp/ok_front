/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconTriangleSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M42.7 41H5.3L24 9.8z"></path>
      <path d="M24 4L0 44h48L24 4zm0 5.8L42.7 41H5.3L24 9.8z"></path>
    </IconBase>
  );
};
