/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconPanelLeft = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      isBidi: true,
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M0 0h8v47.9H0V0zm14 0v48h34V0H14zm27 35.8L36.7 40 21 24 36.7 8l4.3 4.2L29.4 24 41 35.8z"></path>
    </IconBase>
  );
};
