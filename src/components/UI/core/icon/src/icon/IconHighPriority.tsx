/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconHighPriority.module.scss';
const cx = classNames.bind(styles);
export const SvgIconHighPriority = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 8 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconHighPriority', customProps.className])} {...customProps}>
      <path d="M0 40h8v8H0v-8zM0 0h8v32H0V0z"></path>
    </IconBase>
  );
};
