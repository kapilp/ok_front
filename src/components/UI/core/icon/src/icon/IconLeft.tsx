/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconLeft = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      'data-name': 'Layer 1',
      isBidi: true,
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M48 21H10.6L27.3 3.9 23.5.1 0 24l23.5 23.9 3.8-3.8L10.6 27H48z"></path>
    </IconBase>
  );
};
