/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconChevronDown = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      'data-name': 'Layer 1',
      isBidi: true,
    },
    customProps,
  );
  return (
    <IconBase {...customProps}>
      <path d="M24 37.7L0 14.2l3.8-3.9L24 30l20.2-19.7 3.8 3.9z"></path>
    </IconBase>
  );
};
