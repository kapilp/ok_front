/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconPlusSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M45 19v10H29v16H19V29H3V19h16V3h10v16z"></path>
      <path d="M32 16V0H16v16H0v16h16v16h16V32h16V16H32zm13 13H29v16H19V29H3V19h16V3h10v16h16v10z"></path>
    </IconBase>
  );
};
