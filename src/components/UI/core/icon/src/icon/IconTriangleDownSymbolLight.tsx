/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconTriangleDownSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M42.7 7L24 38.2 5.3 7z"></path>
      <path d="M0 4l24 40L48 4H0zm42.7 3L24 38.2 5.3 7h37.4z"></path>
    </IconBase>
  );
};
