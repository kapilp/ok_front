/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconHouse = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      'data-name': 'Layer 1',
    },
    customProps,
  );
  return (
    <IconBase {...customProps}>
      <path d="M47 22.5L25.4.6a2.05 2.05 0 00-2.9 0L1 22.5c-.8.8-.6 1.5.6 1.5H6v22a2 2 0 002 2h8a2 2 0 002-2V35a2 2 0 012-2h8a2 2 0 012 2v11a2 2 0 002 2h8a2 2 0 002-2V24h4.4c1.2 0 1.4-.7.6-1.5z"></path>
    </IconBase>
  );
};
