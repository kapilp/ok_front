/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconSuccessInverseLowLight.module.scss';
const cx = classNames.bind(styles);
export const SvgIconSuccessInverseLowLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconSuccessInverseLowLight', customProps.className])} {...customProps}>
      <path fill="#B1B5B6" d="M24 0h-.1C10.7 0 0 10.8 0 24c0 13.3 10.7 24 24 24s24-10.7 24-24S37.3 0 24 0zm-4 36.4L6.7 23.1l3.6-3.6 9.7 9.9 17.7-17.9 3.6 3.6L20 36.4z"></path>
    </IconBase>
  );
};
