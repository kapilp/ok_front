/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconMax.module.scss';
const cx = classNames.bind(styles);
export const SvgIconMax = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconMax', customProps.className])} {...customProps}>
      <path d="M39.5 16L24 0 8.5 16H21v32h6V16z"></path>
    </IconBase>
  );
};
