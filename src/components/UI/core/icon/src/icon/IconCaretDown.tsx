/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconCaretDown = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      isBidi: true,
    },
    customProps,
  );
  return (
    <IconBase {...customProps}>
      <path d="M48 12L24 36 0 12h48z"></path>
    </IconBase>
  );
};
