/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconMin.module.scss';
const cx = classNames.bind(styles);
export const SvgIconMin = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconMin', customProps.className])} {...customProps}>
      <path d="M27 32V0h-6v32H8.5L24 48l15.5-16z"></path>
    </IconBase>
  );
};
