/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconCaretLeft = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      isBidi: true,
    },
    customProps,
  );
  return (
    <IconBase {...customProps}>
      <path d="M36 48L12 24 36 0v48z"></path>
    </IconBase>
  );
};
