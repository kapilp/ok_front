/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconTeardropSymbol = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M24 0s4.8 5.9 14.5 17.6c6.4 8.1 5.1 19.9-2.9 26.3s-19.6 5.1-26-3c-5.4-6.8-5.4-16.5 0-23.4C19.2 5.9 24 0 24 0z"></path>
    </IconBase>
  );
};
