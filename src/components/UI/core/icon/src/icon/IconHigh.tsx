/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconHigh.module.scss';
const cx = classNames.bind(styles);
export const SvgIconHigh = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      'data-name': 'Layer 1',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconHigh', customProps.className])} {...customProps}>
      <path
        fill="#da3b03"
        d="M24 2.3L45.7 24 24 45.7 2.3 24 24 2.3M24 0a1.135 1.135 0 00-.8.3L.3 23.2a1.215 1.215 0 000 1.6l22.9 22.9a1.217 1.217 0 001.6 0l22.9-22.9a1.217 1.217 0 000-1.6L24.8.3A1.135 1.135 0 0024 0zm0 9L13 20h8v19h6V20h8z"
      ></path>
    </IconBase>
  );
};
