/* eslint-disable */
import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { IconBase, IconBaseProperties } from '../IconBase';
import classNames from 'classnames/bind';

import styles from '../IconHighLowLight.module.scss';
const cx = classNames.bind(styles);
export const SvgIconHighLowLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase className={cx(['IconHighLowLight', customProps.className])} {...customProps}>
      <path
        fill="#FF7B29"
        d="M24 2.3L45.7 24 24 45.7 2.3 24 24 2.3M24 0c-.3 0-.6.1-.8.3L.3 23.2c-.4.4-.4 1.1 0 1.6l22.9 22.9c.2.2.5.3.8.3s.6-.1.8-.3l22.9-22.9c.4-.4.4-1.2 0-1.6L24.8.3c-.2-.2-.5-.3-.8-.3zm0 9L13 20h8v19h6V20h8L24 9z"
      ></path>
    </IconBase>
  );
};
