/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconCaution = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M22 35h4v4h-4v-4zm0-18h4v15h-4V17zm2-7.805l18.807 32.616H5.193L24 9.195m0-6.006L0 44.811h48L24 3.189z"></path>
    </IconBase>
  );
};
