/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconSortAscending = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      className: '',
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      isBidi: true,
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M0 35h48v6H0v-6zm0-8h33v-6H0v6zm0-14h20V7H0v6z"></path>
    </IconBase>
  );
};
