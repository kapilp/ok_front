/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconMediaFastForward = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      'data-name': 'Layer 1',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M22 41.28L43.718 24 22 6.72v13.526L5 6.72v34.56l17-13.526V41.28z"></path>
    </IconBase>
  );
};
