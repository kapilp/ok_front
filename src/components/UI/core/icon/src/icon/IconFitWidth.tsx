/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconFitWidth = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M0 10h3v28H0V10zm45 0v28h3V10h-3zM35 22.5H13v-6L5.5 24l7.5 7.5v-6h22v6l7.5-7.5-7.5-7.5v6z"></path>
    </IconBase>
  );
};
