/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconMediaRecord = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
      'data-name': 'Layer 1',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <circle cx="23.667" cy="24" r="16.875"></circle>
    </IconBase>
  );
};
