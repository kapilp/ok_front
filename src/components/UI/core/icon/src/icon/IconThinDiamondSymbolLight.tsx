/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconThinDiamondSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M34.5 24L24 42 13.5 24 24 6z"></path>
      <path d="M24 0L10 24l14 24 14-24L24 0zm0 6l10.5 18L24 42 13.5 24 24 6z"></path>
    </IconBase>
  );
};
