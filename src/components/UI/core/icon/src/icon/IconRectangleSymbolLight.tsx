/* eslint-disable */

import { IconBase, IconBaseProperties } from '../IconBase';
import { JSX, mergeProps } from 'solid-js';
export const SvgIconRectangleSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M17 3h14v42H17z"></path>
      <path d="M14 0v48h20V0H14zm17 45H17V3h14v42z"></path>
    </IconBase>
  );
};
