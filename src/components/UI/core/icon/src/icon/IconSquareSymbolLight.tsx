/* eslint-disable */

import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconSquareSymbolLight = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path fill="#FFF" d="M3 3h42v42H3z"></path>
      <path d="M0 0v48h48V0H0zm45 45H3V3h42v42z"></path>
    </IconBase>
  );
};
