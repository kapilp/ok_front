/* eslint-disable */
import { JSX, mergeProps } from 'solid-js';
import { IconBase, IconBaseProperties } from '../IconBase';
export const SvgIconTriangleDownSymbol = (customProps: IconBaseProperties) => {
  customProps = mergeProps(
    {},
    {
      viewBox: '0 0 48 48',
      xmlns: 'http://www.w3.org/2000/svg',
    },
    customProps,
  );

  return (
    <IconBase {...customProps}>
      <path d="M24 44L0 4h48L24 44z"></path>
    </IconBase>
  );
};
