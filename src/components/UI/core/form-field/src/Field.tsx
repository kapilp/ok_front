import { JSX, Component, mergeProps, Show, splitProps, useContext, createMemo } from 'solid-js';
import { createStore } from 'solid-js/store';
// import { FormattedMessage } from "react-intl";
import { SvgIconError } from '../../icon/src/icon/IconError';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Field.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The form control elements the Field contains.
   */
  children?: JSX.Element;
  /**
   * Error message for when the input is invalid. This will only be displayed if isInvalid is true.
   */
  error?: JSX.Element;
  /**
   * Error Icon to display when the input is invalid.
   */
  errorIcon?: JSX.Element;
  /**
   * Help element to display with the input.
   */
  help?: JSX.Element;
  /**
   * Whether or not to hide the required indicator on the label.
   */
  hideRequired?: boolean;
  /**
   * The htmlFor attribute on the field label.
   */
  htmlFor?: string;
  /**
   * Whether the field displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Whether or not the field is an inline field.
   */
  isInline?: boolean;
  /**
   * Whether or not the label is visible. Use this props to hide a label while still creating it on the DOM for accessibility.
   */
  isLabelHidden?: boolean;
  /**
   * The label of the form control children.
   */
  label: string;
  /**
   * Attributes to attach to the label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelAttrs?: {};
  /**
   * Set the max-width of a field using `length` or `%`.  Best practice recommendation to never exceed
   * a rendered value of 1020px. _(Note?: Providing custom inline styles will take precedence.)_
   */
  maxWidth?: string;
  /**
   * Whether or not the field is required.
   */
  required?: boolean;
  /**
   * Whether or not to append the 'optional' label to a non-required field label.
   */
  showOptional?: boolean;
  /**
   *  Provides first class prop for custom inline styles
   */
  // eslint-disable-next-line react/forbid-prop-types
  style?: {};
}
const defaultProps = {
  children: null,
  error: null,
  errorIcon: <SvgIconError />,
  help: null,
  hideRequired: false,
  htmlFor: undefined,
  isInvalid: false,
  isInline: false,
  isLabelHidden: false,
  labelAttrs: {},
  maxWidth: undefined,
  required: false,
  showOptional: false,
};
const hasWhiteSpace = s => /\s/g.test(s);
// Detect IE 10 or IE 11
// TODO - Delete detection logic when we drop support for IE
const isIE = () => window.navigator.userAgent.indexOf('Trident/6.0') > -1 || window.navigator.userAgent.indexOf('Trident/7.0') > -1;
export const Field: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, [
    'children',
    'error',
    'errorIcon',
    'help',
    'hideRequired',
    'htmlFor',
    'isInvalid',
    'isInline',
    'isLabelHidden',
    'label',
    'labelAttrs',
    'maxWidth',
    'required',
    'showOptional',
    'style',
    'className',
  ]);

  const theme = useContext(ThemeContext);
  const customStyles = createMemo(() => (p.maxWidth ? { maxWidth: p.maxWidth, ...p.style } : p.style));
  const fieldClasses = createMemo(() => classNames(cx(['field', { 'field-inline': p.isInline }, theme.className]), p.className));
  // Checks to run when not in production
  // TODO - Remove this once we make this a required prop.
  // More info: https://github.com/cerner/terra-core/issues/2290
  if (process.env.NODE_ENV !== 'production') {
    if (!p.htmlFor) {
      // eslint-disable-next-line
      console.warn(
        'The htmlFor prop will be required in the next major version bump of terra-form-field. It is needed for creating an accessible mapping from the form field to its related error and help text.',
      );
    }
    if (p.htmlFor && hasWhiteSpace(p.htmlFor)) {
      // eslint-disable-next-line
      console.warn('The htmlFor prop should be a string without white spaces as it will be used as an HTML attribute value. Use - or _ in place of white space characters.');
    }
  }
  /**
   * IE + JAWS has trouble reading aria-describedby content with our form components.
   * Using feature detect for Microsoft browsers and injecting the help and error messages
   * into the label as visually hidden text so JAWS can announce them correctly in IE.
   */
  const IEDescriptionText = (
    <>
      {isIE() && (
        <div className={cx('visually-hidden-text')}>
          {p.isInvalid && p.error ? p.error : null}
          {p.help}
        </div>
      )}
    </>
  );
  /*const content = React.Children.map(p.children, (child: JSX.Element) => {
    if (
      (p.required || p.isInvalid) &&
      child &&
      (child.type.isInput || child.type.isSelect || child.type.isTextarea)
    ) {
      return React.cloneElement(child, {
        ...(p.required && { required: true }),
        ...(p.isInvalid && { isInvalid: true })
      });
    }
    return child;
  });*/
  const labelGroup = (
    <div className={cx(['label-group', { 'label-group-hidden': p.isLabelHidden }])}>
      {p.isInvalid && <div className={cx('error-icon')}>{p.errorIcon}</div>}
      <label htmlFor={p.htmlFor} {...p.labelAttrs} className={cx(['label', p.labelAttrs.className])}>
        {p.required && (p.isInvalid || !p.hideRequired) && <div className={cx('required')}>*</div>}
        {p.label}
        {p.required && !p.isInvalid && p.hideRequired && <div className={cx('required-hidden')}>*</div>}
        {p.showOptional && !p.required && <div className={cx('optional')}>{'(optional)'}</div>}
        {IEDescriptionText}
      </label>
      {!p.isInvalid && <div className={cx('error-icon-hidden')}>{p.errorIcon}</div>}
    </div>
  );
  /* eslint-disable react/forbid-dom-props */
  return (
    <div style={customStyles()} {...customProps} className={fieldClasses()}>
      {labelGroup}
      {/*{content}*/}
      {p.children}
      {p.isInvalid && p.error && (
        <div aria-live="assertive" tabIndex="-1" id={p.htmlFor ? `${p.htmlFor}-error` : undefined} className={cx('error-text')}>
          {p.error}
        </div>
      )}
      {p.help && (
        <div tabIndex="-1" id={p.htmlFor ? `${p.htmlFor}-help` : undefined} className={cx('help-text')}>
          {p.help}
        </div>
      )}
    </div>
  );
  /* eslint-enable react/forbid-dom-props */
};
