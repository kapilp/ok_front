import { JSX, Component, mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Field } from '../../../Field';
import classNames from 'classnames/bind';
import styles from './FieldExamples.module.scss';

const cx = classNames.bind(styles);
type FieldExamplesState = {
  isInvalid?: boolean;
};
export const FieldExamples = (props: {}) => {
  const [state, setState] = createStore({ isInvalid: false });
  const toggleIsInvalid = () => {
    setState({ isInvalid: !state.isInvalid });
  };
  return (
    <div>
      <Field label="Field Label" help="This is a standard, non-required field." error="Error Message" isInvalid={state.isInvalid}>
        <div className={cx('field-content')}>Control Placeholder</div>
      </Field>
      <Field label="Field Label" help="This is an optional field." error="Error Message" isInvalid={state.isInvalid} isInline showOptional>
        <div className={cx('field-content')}>Control Placeholder</div>
      </Field>

      <Field label="Field Label" help="This is a required field." error="Error Message" isInvalid={state.isInvalid} isInline required>
        <div className={cx('field-content')}>Control Placeholder</div>
      </Field>

      <Field
        label="Field Label"
        help="This is a required field, but the required indicator is hidden until the field is in error."
        error="Now the required indicator is showing because the field is in error."
        isInvalid={state.isInvalid}
        isInline
        required
        hideRequired
      >
        <div className={cx('field-content')}>Control Placeholder</div>
      </Field>
      <hr />
      <p>
        If a field is invalid, an error icon will be displayed.
        <button type="button" onClick={toggleIsInvalid}>
          Toggle Invalid State
        </button>
      </p>
    </div>
  );
};
