import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Field from '../../../Field';
import styles from './FormFieldTestCommon.module.scss';
const cx = classNames.bind(styles);
const FieldLabel = () => (
  <div>
    <h3>Field - Label</h3>
    <Field id="label" className={cx('form-field')} label="Field Label">
      <div className={cx('field-content')}>Control Placeholder</div>
    </Field>
  </div>
);
export default FieldLabel;
