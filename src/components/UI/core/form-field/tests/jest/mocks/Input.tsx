import {JSX} from "solid-js";
interface IInputProps extends React.HTMLAttributes<Element> {
  isInvalid?: boolean;
  customProps?: any;
}
// eslint-disable-next-line react/prefer-stateless-function
class Input extends React.Component<IInputProps, {}> {
  render() {
    const { isInvalid, ...customProps } = this.props;
    return <input invalid={isInvalid && "true"} {...customProps} />;
  }
}
Input.isInput = true;
export default Input;
