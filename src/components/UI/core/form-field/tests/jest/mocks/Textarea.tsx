import {JSX} from "solid-js";
interface ITextareaProps extends React.HTMLAttributes<Element> {
  isInvalid?: boolean;
  customProps?: any;
}
// eslint-disable-next-line react/prefer-stateless-function
class Textarea extends React.Component<ITextareaProps, {}> {
  render() {
    const { isInvalid, ...customProps } = this.props;
    return <textarea invalid={isInvalid && "true"} {...customProps} />;
  }
}
Textarea.isTextarea = true;
export default Textarea;
