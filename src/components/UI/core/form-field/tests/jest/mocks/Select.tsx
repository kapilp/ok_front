import {JSX} from "solid-js";
interface ISelectProps extends React.HTMLAttributes<Element> {
  isInvalid?: boolean;
  customProps?: any;
}
// eslint-disable-next-line react/prefer-stateless-function
class Select extends React.Component<ISelectProps, {}> {
  render() {
    const { isInvalid, ...customProps } = this.props;
    return <select invalid={isInvalid && "true"} {...customProps} />;
  }
}
Select.isSelect = true;
export default Select;
