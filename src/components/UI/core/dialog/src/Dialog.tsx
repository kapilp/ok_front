// import { FormattedMessage } from 'react-intl'; // Todo fix this
import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ContentContainer } from '../../content-container/src/ContentContainer';
import { Button } from '../../button/src/Button';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Dialog.module.scss';

const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The children to be placed within the main content area of the dialog.
   */
  children?: JSX.Element;
  /**
   * React node to be placed within the footer area of the dialog.
   * Content exceeding the available space will be hidden.
   */
  footer: JSX.Element;
  /**
   * React node to be placed within the header area of the dialog.
   */
  header: JSX.Element;
  /**
   * Callback function for when the close button is clicked. The close button will not display if this is not set.
   * On small viewports a back button will be displayed instead of a close button.
   */
  onClose?: () => void;
}
const defaultProps = {
  onClose: null,
  children: null,
};
export const Dialog = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [p, customProps] = splitProps(props, ['children', 'footer', 'header', 'onClose', 'className']);

  const dialogClassNames = () => classNames(cx('dialog', theme.className), p.className);

  const closeButton = () =>
    p.onClose ? (
      <div className={cx('dialog-header-close')}>
        <Button variant="utility" text="Close" onClick={p.onClose} isIconOnly icon={() => <span className={cx('close-icon')} />} />
      </div>
    ) : null;
  const dialogHeader = (
    <div className={cx('dialog-header')}>
      <div className={cx('dialog-header-title')}>{p.header}</div>
      {closeButton()}
    </div>
  );
  return (
    <div {...customProps} className={dialogClassNames()}>
      <ContentContainer fill header={dialogHeader} footer={<div className={cx('dialog-footer')}>{p.footer}</div>}>
        <div className={cx('dialog-body')}>{p.children}</div>
      </ContentContainer>
    </div>
  );
};
