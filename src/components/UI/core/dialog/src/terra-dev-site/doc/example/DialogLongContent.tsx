/* eslint-disable no-alert */
import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './DialogDocCommon.module.scss';
import { Dialog } from '../../../Dialog';
const cx = classNames.bind(styles);
const header = 'Header Stuff';
const footer = (
  <div>
    <span>Footer Stuff</span>
    <span>Footer Stuff</span>
    <span>Footer Stuff</span>
    <span>Footer Stuff</span>
    <span>Footer Stuff</span>
  </div>
);
const body = (
  <div>
    <p>This is my body content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
    <p>This is some more content.</p>
  </div>
);
export const DialogLongContent = () => (
  <div className={cx('dialog-wrapper')}>
    <Dialog header={header} footer={footer} onClose={() => alert('Close')}>
      {body}
    </Dialog>
  </div>
);
