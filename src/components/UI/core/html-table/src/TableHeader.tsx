import { JSX, mergeProps, splitProps } from 'solid-js';

interface Properties {
  /**
   * The child TableHeaderCells to place within the header.
   */
  children?: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const TableHeader = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children']);

  return (
    <thead {...customProps}>
      <tr>{p.children}</tr>
    </thead>
  );
};
