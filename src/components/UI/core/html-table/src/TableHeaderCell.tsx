import { JSX, mergeProps, splitProps } from 'solid-js';

interface Properties {
  /**
   * Content to be displayed for the column header
   */
  children?: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const TableHeaderCell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children']);

  return <th {...customProps}>{p.children}</th>;
};
