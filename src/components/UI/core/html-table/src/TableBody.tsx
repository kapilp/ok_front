import { JSX, mergeProps, splitProps } from 'solid-js';

interface Properties {
  /**
   * The child TableRows to render within the body.
   */
  children?: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const TableBody = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children']);

  return <tbody {...customProps}>{p.children}</tbody>;
};
