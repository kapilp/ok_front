import { JSX, mergeProps, splitProps } from 'solid-js';

interface Properties {
  /**
   * Content to be displayed for the row cell.
   */
  children?: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const TableCell = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children']);

  return <td {...customProps}>{p.children}</td>;
};
