import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import { TableHeader } from '../../../TableHeader';
import { TableHeaderCell } from '../../../TableHeaderCell';
import { TableBody } from '../../../TableBody';
import { TableRow } from '../../../TableRow';
import { TableCell } from '../../../TableCell';
export const StandardPaddingHtmlTable = () => (
  <Table paddingStyle="standard">
    <TableHeader>
      <TableHeaderCell key="NAME">Name</TableHeaderCell>
      <TableHeaderCell key="ADDRESS">Address</TableHeaderCell>
      <TableHeaderCell key="PHONE_NUMBER">Phone Number</TableHeaderCell>
    </TableHeader>
    <TableBody>
      <TableRow key="PERSON_0">
        <TableCell key="NAME">John Smith</TableCell>
        <TableCell key="ADDRESS">123 Adams Drive</TableCell>
        <TableCell key="PHONE_NUMBER">111-222-3333</TableCell>
      </TableRow>
      <TableRow key="PERSON_1">
        <TableCell key="NAME">Jane Smith</TableCell>
        <TableCell key="ADDRESS">321 Drive Street</TableCell>
        <TableCell key="PHONE_NUMBER">111-222-3333</TableCell>
      </TableRow>
      <TableRow key="PERSON_2">
        <TableCell key="NAME">Dave Smith</TableCell>
        <TableCell key="ADDRESS">213 Raymond Road</TableCell>
        <TableCell key="PHONE_NUMBER">111-222-3333</TableCell>
      </TableRow>
    </TableBody>
  </Table>
);
