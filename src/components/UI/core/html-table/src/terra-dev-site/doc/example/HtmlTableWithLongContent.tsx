import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import { TableHeader } from '../../../TableHeader';
import { TableHeaderCell } from '../../../TableHeaderCell';
import { TableBody } from '../../../TableBody';
import { TableRow } from '../../../TableRow';
import { TableCell } from '../../../TableCell';
export const HtmlTableWithLongContent = () => (
  <Table>
    <TableHeader>
      <TableHeaderCell key="COL_0">Column Heading 1</TableHeaderCell>
      <TableHeaderCell key="COL_1">
        Very long table header Very long table header Very long table header Very long table header Very long table header Very long table header Very long table header Very long
        table header Very long table header Very long table header End Header
      </TableHeaderCell>
      <TableHeaderCell key="COL_2">Column Heading 3</TableHeaderCell>
    </TableHeader>
    <TableBody>
      <TableRow key="ROW_0">
        <TableCell key="COL_0">Table Data</TableCell>
        <TableCell key="COL_1">Table Data</TableCell>
        <TableCell key="COL_2">
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text End table text
        </TableCell>
      </TableRow>
      <TableRow key="ROW_1">
        <TableCell key="COL_0">Table Data</TableCell>
        <TableCell key="COL_1">Table Data</TableCell>
        <TableCell key="COL_2">
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text End table text
        </TableCell>
      </TableRow>
      <TableRow key="ROW_2">
        <TableCell key="COL_0">Table Data</TableCell>
        <TableCell key="COL_1">Table Data</TableCell>
        <TableCell key="COL_2">
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text Very long table text
          Very long table text End table text
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);
