import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Table } from '../../../Table';
import { TableHeader } from '../../../TableHeader';
import { TableHeaderCell } from '../../../TableHeaderCell';
import { TableBody } from '../../../TableBody';
import { TableRow } from '../../../TableRow';
import { TableCell } from '../../../TableCell';
import classNames from 'classnames/bind';
import styles from './TableWithCustomCells.module.scss';
const cx = classNames.bind(styles);
type CustomCellProps = {
  text?: string;
  subtext?: string;
};
const CustomCell = (props: CustomCellProps) => (
  <div>
    <h3>{props.text}</h3>
    {props.subtext ? <h4 className={cx('custom-cell-header-wrapper')}>{props.subtext}</h4> : null}
  </div>
);
export const HtmlTableWithCustomCells = () => (
  <Table>
    <TableHeader>
      <TableHeaderCell key="NAME">Name</TableHeaderCell>
      <TableHeaderCell key="ADDRESS">Address</TableHeaderCell>
      <TableHeaderCell key="PHONE_NUMBER">Phone Number</TableHeaderCell>
    </TableHeader>
    <TableBody>
      <TableRow key="PERSON_0">
        <TableCell key="NAME">
          <CustomCell text="John Smith" subtext="Super Cool Person" />
        </TableCell>
        <TableCell key="ADDRESS">
          <CustomCell text="123 Adams Drive" subtext="Missouri" />
        </TableCell>
        <TableCell key="PHONE_NUMBER">
          <CustomCell text="111-222-3333" subtext="Home" />
        </TableCell>
      </TableRow>
      <TableRow key="PERSON_1">
        <TableCell key="NAME">
          <CustomCell text="Jane Smith" subtext="Super Cool Person" />
        </TableCell>
        <TableCell key="ADDRESS">
          <CustomCell text="321 Drive Street" subtext="Kansas" />
        </TableCell>
        <TableCell key="PHONE_NUMBER">
          <CustomCell text="111-222-3333" subtext="Cell" />
        </TableCell>
      </TableRow>
      <TableRow key="PERSON_2">
        <TableCell key="NAME">
          <CustomCell text="Dave Smith" subtext="Not Super Cool At All" />
        </TableCell>
        <TableCell key="ADDRESS">
          <CustomCell text="213 Raymond Road" subtext="Alaska" />
        </TableCell>
        <TableCell key="PHONE_NUMBER">
          <CustomCell text="111-222-3333" subtext="Work" />
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
);
