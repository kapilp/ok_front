import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Table.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The child TableCells to be placed within the tr.
   */
  children: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const TableRow = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['children']);

  const rowClassNames = cx(['row']);
  return (
    <tr {...customProps} className={customProps.className ? `${rowClassNames} ${customProps.className}` : rowClassNames}>
      {p.children}
    </tr>
  );
};
