import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Table.module.scss';
import { JSX, mergeProps, splitProps, useContext } from 'solid-js';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The child content for the table consisting of either a TableHeader and/or a TableBody.
   */
  children: JSX.Element;
  /**
   * Whether or not the rows should be zebra striped.
   */
  disableStripes?: boolean;
  /**
   * Indicates the desired divider styles to apply to a row and its children.
   * One of `'none'`, `'standard'`, `'compact'`.
   */
  paddingStyle?: 'none' | 'standard' | 'compact';
}
const defaultProps = {
  disableStripes: false,
  paddingStyle: 'none',
};
export const Table = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);
  const [p, customProps] = splitProps(props, ['children', 'disableStripes', 'paddingStyle']);
  const tableClassNames = classNames(
    cx(
      'tTable',
      'table',
      { striped: !p.disableStripes },
      { 'padding-standard': p.paddingStyle === 'standard' },
      { 'padding-compact': p.paddingStyle === 'compact' },
      theme.className,
    ),
    customProps.className,
  );
  return (
    <table {...customProps} className={tableClassNames}>
      {p.children}
    </table>
  );
};
