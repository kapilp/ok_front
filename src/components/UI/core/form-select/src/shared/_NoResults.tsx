import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
// import { FormattedMessage } from 'react-intl';
import styles from './_NoResults.module.scss';

const cx = classNamesBind.bind(styles);
interface NoResultsProps {
  /**
   * Content to display when no results are found.
   */
  noResultContent?: JSX.Element;
  /**
   * The filter value.
   */
  value?: number | string;
}
const NoResults = (props: NoResultsProps) => {
  // const theme = React.useContext(ThemeContext);
  return (
    <div role="option" className={cx('no-results')} aria-selected="false">
      {props.noResultContent || props.value}
    </div>
  );
};
export default NoResults;
