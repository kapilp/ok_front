export const Variants = {
  COMBOBOX: 'combobox',
  DEFAULT: 'default',
  MULTIPLE: 'multiple',
  TAG: 'tag',
  SEARCH: 'search',
} as const;
