import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './_Tag.module.scss';

const cx = classNamesBind.bind(styles);
interface TagProps {
  /**
   * The content of the tag.
   */
  children: JSX.Element;
  /**
   * Callback function triggered when the tag is deselected.
   */
  onDeselect(...args: unknown[]): unknown;
  /**
   * The value of the tag.
   */
  value: string | number;
}

const Tag = ({ children, onDeselect, value }) => {
  // const theme = React.useContext(ThemeContext);
  return (
    <li className={cx('tag')}>
      <span className={cx('display')}>{children}</span>
      <span
        className={cx('deselect')}
        onClick={() => {
          onDeselect(value);
        }}
        role="presentation"
      >
        <span className={cx('icon')} />
      </span>
    </li>
  );
};
export default Tag;
