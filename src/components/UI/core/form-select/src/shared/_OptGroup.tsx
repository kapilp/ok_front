// import { injectIntl } from 'react-intl';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './_OptGroup.module.scss';

const cx = classNamesBind.bind(styles);
interface OptGroupProps {
  /**
   * The group options.
   */
  children?: JSX.Element;
  /**
   * Whether the option group is disabled.
   */
  disabled?: boolean;
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  intl: {
    formatMessage?(...args: unknown[]): unknown;
  };
  /**
   * The group label.
   */
  label: JSX.Element;
}

const defaultProps = {
  disabled: false,
};
export const OptGroup = (props: OptGroupProps) => {
  // const theme = React.useContext(ThemeContext);
  return (
    <li className={cx('opt-group', { 'is-disabled': propps.disabled })} role="option" aria-selected="false">
      <div className={cx('label')}>{props.label}</div>
      <ul className={cx('options')} role="listbox" aria-label="Options">
        {React.Children.map(children, child =>
          React.cloneElement(child, {
            disabled: props.disabled || !!child.props.disabled,
          }),
        )}
      </ul>
    </li>
  );
};
// OptGroup.defaultProps = defaultProps;
OptGroup.isOptGroup = true;
// export default injectIntl(OptGroup);
