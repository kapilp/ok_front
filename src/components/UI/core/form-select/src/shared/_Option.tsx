import { mergeProps, splitProps } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import SharedUtil from './_SharedUtil';
import styles from './_Option.module.scss';

const cx = classNamesBind.bind(styles);

interface OptionProps {
  /**
   * Whether the option is disabled.
   */
  disabled?: boolean;
  /**
   * The display text of the option.
   */
  display?: string;
  /**
   * @private
   * Whether the option is active.
   */
  isActive?: boolean;
  /**
   * @private
   * Whether the option is a custom text entry.
   */
  isAddOption?: boolean;
  /**
   * @private
   * Whether the option is checkable.
   */
  isCheckable?: boolean;
  /**
   * @private
   * Whether the option is selected.
   */
  isSelected?: boolean;
  /**
   * The value of the option. The value must be unique.
   */
  value: string | number;
  /**
   * @private
   * Variant of select component
   */
  variant?: string;
}

const defaultProps = {
  disabled: false,
  display: undefined,
};
export const Option = (props: OptionProps) => {
  props = mergeProps({}, defaultProps, props);
  const [p, customProps] = splitProps(props, ['disabled', 'display', 'value', 'variant', 'isActive', 'isSelected', 'isCheckable', 'isAddOption']);

  // const theme = React.useContext(ThemeContext);
  const optionClassNames = () =>
    classNames(
      cx(
        'option',
        { 'is-active': p.isActive },
        { 'is-checkable': p.isCheckable && !p.isAddOption },
        { 'is-default': !p.isCheckable && !p.isAddOption },
        { 'is-disabled': p.disabled },
        { 'is-selected': p.isSelected },
        { 'is-add-option': p.isAddOption },
        // theme.className,
      ),
      customProps.className,
    );
  const role = () => {
    let _role = 'option'; // Used for JAWs and VoiceOver on iOS
    /**
     * VoiceOver in Safari on desktop has issues with role="option" with the combination of the
     * aria-live section we have and will stutter when reading options.
     * Switching to role="radio" and role="checkbox" mitigates this behavior.
     */
    if (SharedUtil.isSafari() && !('ontouchstart' in window)) {
      _role = 'radio';
      if (p.variant === 'tag' || p.variant === 'multiple') {
        _role = 'checkbox';
      }
    }
    return _role;
  };
  return (
    <li
      role={role()}
      {...customProps}
      disabled={p.disabled}
      className={optionClassNames()}
      aria-selected={p.isSelected} // Needed to allow VoiceOver on iOS to announce selected state
      aria-checked={p.isSelected} // Needed to allow JAWS to announce "selected" state
      aria-disabled={p.disabled}
      tabIndex="0" // eslint-disable-line jsx-a11y/no-noninteractive-tabindex
      data-terra-select-option
    >
      {(p.isCheckable || p.isAddOption) && <span className={cx('icon')} />}
      <span className={cx('display')}>{p.display}</span>
    </li>
  );
};
// Option.defaultProps = defaultProps;
Option.isOption = true;
