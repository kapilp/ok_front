// import { FormattedMessage } from 'react-intl';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import styles from './_MaxSelection.module.scss';

const cx = classNamesBind.bind(styles);
interface MaxSelectionProps {
  /**
   * The filter value.
   */
  value?: number | string;
}
export const MaxSelection = (props: MaxSelectionProps) => {
  // const theme = React.useContext(ThemeContext);
  return <div className={cx('max-selection')}>{props.value}</div>;
};
