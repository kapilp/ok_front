import { splitProps } from 'solid-js';
// import { injectIntl } from 'react-intl';
import Option from './_Option';

interface AddOptionProps {
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  intl: {
    formatMessage?(...args: unknown[]): unknown;
  };
  /**
   * The value of the option.
   */
  value?: number | string;
}
export const AddOption = (props: AddOptionProps) => {
  const [p, customProps] = splitProps(props, ['value', 'intl']);
  return <Option {...customProps} isAddOption value={p.value} display={p.value} data-terra-select-add-option />;
};
AddOption.isOption = true;
// export default injectIntl(AddOption);
