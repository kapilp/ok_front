import { splitProps } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
// import ThemeContext from 'terra-theme-context';
import { Option } from './_Option';
import styles from './_ClearOption.module.scss';

const cx = classNamesBind.bind(styles);
interface ClearOptionProps {
  /**
   * The display text of the clear option.
   */
  display?: string;
  /**
   * The value of the clear option.
   */
  value?: string;
}
export const ClearOption = (props: ClearOptionProps) => {
  const [p, customProps] = splitProps(props, ['display', 'value']);
  // const theme = React.useContext(ThemeContext);
  return (
    <Option
      {...customProps}
      className={classNames(cx('clear-option'), customProps.className)}
      display={p.display}
      value={p.value}
      isSelected={false}
      data-terra-select-clear-option
    />
  );
};
ClearOption.isOption = true;
