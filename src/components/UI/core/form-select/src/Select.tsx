import Variants from './shared/_variants';
import Option from './shared/_Option';
import OptGroup from './shared/_OptGroup';
import Combobox from './Combobox';
import SingleSelect from './SingleSelect';
import MultiSelect from './MultiSelect';
import Search from './SearchSelect';
import Tag from './TagSelect';

const defaultProps = {
  allowClear: false,
  children: undefined,
  defaultValue: undefined,
  disabled: false,
  dropdownAttrs: undefined,
  isIncomplete: false,
  isInvalid: false,
  isTouchAccessible: false,
  maxSelectionCount: undefined,
  noResultContent: undefined,
  onChange: undefined,
  onDeselect: undefined,
  onSearch: undefined,
  onSelect: undefined,
  optionFilter: undefined,
  placeholder: undefined,
  required: false,
  value: undefined,
  variant: 'default',
};
interface SelectProps {
  /**
   * Whether a clear option is available to clear the selection, will use placeholder text if provided.
   * This is not applicable to the `multiple` or `tag` variants since the selection can already be deselected using the tag.
   */
  allowClear?: boolean;
  /**
   * The dropdown menu options.
   */
  children?: JSX.Element;
  /**
   * The default selected value. Can be a string, number, or array of strings/numbers.
   */
  defaultValue?: string | number | unknown[];
  /**
   * Whether the select is disabled.
   */
  disabled?: boolean;
  /**
   * Additional attributes to spread onto the dropdown. ( Style, ClassNames, etc.. )
   */
  // eslint-disable-next-line react/forbid-prop-types
  dropdownAttrs?: object;
  /**
   * Whether the select displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the select displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Ensures touch accessibility by rendering the dropdown inline without a portal.
   *
   * Note: When enabled the dropdown will clip if rendered within a container that has an overflow: hidden ancestor.
   * The dropdown may also appear beneath content if rendered within a container that has an overflow: auto ancestor.
   *
   * Only applicable to variants that include an input (e.g. `combobox`, `multiple`, `search`, and `tag`).
   */
  isTouchAccessible?: boolean;
  /**
   * The max height of the dropdown.
   */
  maxHeight?: number;
  /**
   * @private The maximum number of options that can be selected. A value less than 2 will be ignored.
   * Only applicable to variants allowing multiple selections (e.g.; `multiple`; `tag`).
   */
  maxSelectionCount?: number;
  /**
   * Content to display when no results are found.
   */
  noResultContent?: JSX.Element;
  /**
   * Callback function triggered when the select loses focus. function(event)
   */
  onBlur?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the value changes. function(value)
   */
  onChange?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the select is clicked. function(event)
   */
  onClick?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when an option is deselected. function(value)
   */
  onDeselect?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the select receives focus. function(event)
   */
  onFocus?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the search criteria changes. function(searchValue)
   */
  onSearch?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when an option is selected. function(value)
   */
  onSelect?(...args: unknown[]): unknown;
  /**
   * Callback function invoked for each option on search change. function(searchValue, option)
   */
  optionFilter?(...args: unknown[]): unknown;
  /**
   * Placeholder text.
   */
  placeholder?: string;
  /**
   * Whether the field is required.
   */
  required?: boolean;
  /**
   * The selected value. Can be a string, number, or array of strings/numbers.
   */
  value?: string | number | unknown[];
  /**
   * The behavior of the select. One of `default`, `combobox`, `multiple`, `tag`, or `search`.
   */
  variant?: typeof Variants[keyof typeof Variants];
}
const Select = props => {
  const { variant, ...otherProps } = props;
  switch (variant) {
    case Variants.COMBOBOX: {
      const { maxSelectionCount, ...comboboxProps } = otherProps;
      return <Combobox {...comboboxProps} />;
    }
    case Variants.MULTIPLE: {
      const { allowClear, ...multipleProps } = otherProps;
      return <MultiSelect {...multipleProps} />;
    }
    case Variants.SEARCH: {
      const { maxSelectionCount, ...searchProps } = otherProps;
      return <Search {...searchProps} />;
    }
    case Variants.TAG: {
      const { noResultContent, allowClear, ...tagProps } = otherProps;
      return <Tag {...tagProps} />;
    }
    case Variants.DEFAULT:
    default: {
      const { isTouchAccessible, maxSelectionCount, onSearch, optionFilter, ...singleSelectProps } = otherProps;
      return <SingleSelect {...singleSelectProps} />;
    }
  }
};
Select.Option = Option;
Select.OptGroup = OptGroup;
Select.defaultProps = defaultProps;
Select.isSelect = true;
export default Select;
