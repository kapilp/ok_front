import { OptionPropType } from '../../../../native-select/_NativePropTypes';

interface NativeOptionsPropsProps {
  /**
   * The option display.
   */
  display: string;
  /**
   * Whether the optgroup is disabled.
   */
  disabled?: boolean;
  /**
   * The array of select options.
   */
  options: OptionPropType[];
}
const NativeOptionsProps = () => <div />;
export default NativeOptionsProps;
