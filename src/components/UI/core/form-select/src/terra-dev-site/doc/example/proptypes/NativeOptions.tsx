interface NativeOptGroupsPropsProps {
  /**
   * The option display.
   */
  display: string;
  /**
   * Whether the option is disabled.
   */
  disabled?: boolean;
  /**
   * The option value.
   */
  value: number | string;
}
const NativeOptGroupsProps = () => <div />;
export default NativeOptGroupsProps;
