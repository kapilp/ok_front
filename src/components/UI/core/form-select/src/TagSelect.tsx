// import { injectIntl } from 'react-intl';
import Frame from './tag/Frame';
import Option from './shared/_Option';
import OptGroup from './shared/_OptGroup';
import Tag from './shared/_Tag';
import SelectUtil from './shared/_SelectUtil';

const defaultProps = {
  children: undefined,
  defaultValue: undefined,
  disabled: false,
  dropdownAttrs: undefined,
  isIncomplete: false,
  isInvalid: false,
  isTouchAccessible: false,
  maxSelectionCount: undefined,
  onChange: undefined,
  onDeselect: undefined,
  onSearch: undefined,
  onSelect: undefined,
  optionFilter: undefined,
  placeholder: undefined,
  required: false,
  value: undefined,
  inputId: undefined,
};
interface ITagSelectProps extends JSX.HTMLAttributes<Element> {
  /**
   * The dropdown menu options.
   */
  children?: JSX.Element;
  /**
   * The default selected value. Can be a string, number, or array of strings/numbers.
   */
  defaultValue?: string | number | unknown[];
  /**
   * Whether the select is disabled.
   */
  disabled?: boolean;
  /**
   * Additional attributes to spread onto the dropdownq. ( Style, ClassNames, etc.. )
   */
  // eslint-disable-next-line react/forbid-prop-types
  dropdownAttrs?: object;
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  intl: {
    formatMessage?(...args: unknown[]): unknown;
  };
  /**
   * Whether the select displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the select displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Ensures touch accessibility by rendering the dropdown inline without a portal.
   *
   * Note: When enabled the dropdown will clip if rendered within a container that has an overflow: hidden ancestor.
   * The dropdown may also appear beneath content if rendered within a container that has an overflow: auto ancestor.
   */
  isTouchAccessible?: boolean;
  /**
   * The max height of the dropdown.
   */
  maxHeight?: number;
  /**
   * @private The maximum number of options that can be selected. A value less than 2 will be ignored.
   */
  maxSelectionCount?: number;
  /**
   * Callback function triggered when the select loses focus. function(event)
   */
  onBlur?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the value changes. function(value)
   */
  onChange?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the select is clicked. function(event)
   */
  onClick?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when an option is deselected. function(value)
   */
  onDeselect?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the select receives focus. function(event)
   */
  onFocus?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when the search criteria changes. function(searchValue)
   */
  onSearch?(...args: unknown[]): unknown;
  /**
   * Callback function triggered when an option is selected. function(value)
   */
  onSelect?(...args: unknown[]): unknown;
  /**
   * Callback function invoked for each option on search change. function(searchValue, option)
   */
  optionFilter?(...args: unknown[]): unknown;
  /**
   * Placeholder text.
   */
  placeholder?: string;
  /**
   * Whether the field is required.
   */
  required?: boolean;
  /**
   * The selected value. Can be a string, number, or array of strings/numbers.
   */
  value?: string | number | unknown[];
  /**
   * @private
   * The id of the input field.
   */
  inputId?: string;
}
type TagSelectState = {
  tags?: undefined[];
  value?: any;
};
export class TagSelect extends React.Component<ITagSelectProps, TagSelectState> {
  constructor(props) {
    super(props);
    const { defaultValue, value } = props;
    this.state = {
      tags: [],
      value: SelectUtil.defaultValue({ defaultValue, value, multiple: true }),
    };
    this.display = this.display.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDeselect = this.handleDeselect.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  /**
   * Communicates changes to the value.
   * @param {array|number|string} value - The value resulting from a change.
   */
  handleChange(value) {
    if (this.props.value === undefined) {
      this.setState({ value });
    }
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }

  /**
   * Communicates the removal of a value from the selected options.
   * @param {number|string} value - The value to be removed.
   */
  handleDeselect(value) {
    this.handleChange(SelectUtil.deselect(this.props, this.state, value));
    if (this.props.onDeselect) {
      this.props.onDeselect(value);
    }
  }

  /**
   * Communicates the selection of a value.
   * @param {number|string} value - The value of the selected option.
   * @param {ReactNode} option - The selected option.
   */
  handleSelect(value, option) {
    const newValue = [...SelectUtil.value(this.props, this.state), value];
    this.handleChange(newValue);
    // Add new tags for uncontrolled components.
    if (this.props.value === undefined && !SelectUtil.findByValue(this.props, this.state, value)) {
      this.setState(prevState => ({
        tags: [...prevState.tags, <Option key={value} display={value} value={value} />],
      }));
    }
    if (this.props.onSelect) {
      this.props.onSelect(value, option);
    }
  }

  /**
   * Returns the appropriate variant display
   */
  display() {
    const selectValue = SelectUtil.value(this.props, this.state);
    return selectValue.map(tag => (
      <Tag value={tag} key={tag} onDeselect={this.handleDeselect}>
        {SelectUtil.valueDisplay(this.props, tag)}
      </Tag>
    ));
  }

  render() {
    const { children, defaultValue, onChange, placeholder, required, value, intl, inputId, ...otherProps } = this.props;
    const defaultPlaceholder = intl.formatMessage({
      id: 'Terra.form.select.defaultDisplay',
    });
    const selectPlaceholder = placeholder === undefined ? defaultPlaceholder : placeholder;
    return (
      <Frame
        {...otherProps}
        data-terra-select
        value={SelectUtil.value(this.props, this.state)}
        display={this.display()}
        onDeselect={this.handleDeselect}
        onSelect={this.handleSelect}
        placeholder={selectPlaceholder}
        required={required}
        totalOptions={SelectUtil.getTotalNumberOfOptions(children)}
        inputId={inputId}
      >
        {this.state.tags}
        {children}
      </Frame>
    );
  }
}
TagSelect.Option = Option;
TagSelect.OptGroup = OptGroup;
TagSelect.defaultProps = defaultProps;
TagSelect.isSelect = true;
// export default injectIntl(TagSelect);
