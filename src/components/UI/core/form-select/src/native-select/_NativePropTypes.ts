/**
 * NOTE: Changes to this file should be mirrored in the `terra-dev-site/doc/example/propTypes` folder
 */
export interface OptionPropType {
  /**
   * The option display.
   */
  display: string;
  /**
   * Whether the option is disabled.
   */
  disabled?: boolean;
  /**
   * The option value.
   */
  value: number | string;
}

export interface OptGroupPropType {
  /**
   * The option display.
   */
  display: string;
  /**
   * Whether the optgroup is disabled.
   */
  disabled?: boolean;
  /**
   * The array of select options.
   */
  options: unknown[];
}
