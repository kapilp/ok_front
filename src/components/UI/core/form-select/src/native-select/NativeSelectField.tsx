import Field from 'terra-form-field';
import NativeSelect from './NativeSelect';
import { OptionPropType, OptGroupPropType } from './_NativePropTypes';

interface NativeSelectFieldProps {
  /**
   * The default value of the select. Can be a string, or number.
   */
  defaultValue?: number | string;
  /**
   * Whether the input is disabled.
   */
  disabled?: boolean;
  /**
   * Error message displayed when the select is invalid.
   */
  error?: JSX.Element;
  /**
   * Help message to display with the select.
   */
  help?: JSX.Element;
  /**
   * Whether to hide the required indicator on the label.
   */
  hideRequired?: boolean;
  /**
   * Whether the field displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the field is displayed inline. Displays block by default.
   */
  isInline?: boolean;
  /**
   * Whether the label is hidden. Allows hiding the label while meeting accessibility guidelines.
   */
  isLabelHidden?: boolean;
  /**
   * Whether the field displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * The field label.
   */
  label: JSX.Element;
  /**
   * Additional attributes to spread onto the label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelAttrs?: object;
  /**
   * Set the max-width of a field using `length` or `%`.  Best practice recommendation to never exceed
   * a rendered value of 1020px. _(Note: Providing custom inline styles will take precedence.)_
   */
  maxWidth?: string;
  /**
   * Callback function triggered when the select value changes. function(event)
   */
  onChange?(...args: unknown[]): unknown;
  /**
   * The array of select options and opt groups.
   */
  options?: Array<OptionPropType | OptGroupPropType>;
  /**
   * Whether the field is required.
   */
  required?: boolean;
  /**
   * Additional attributes to spread onto the select.
   */
  // eslint-disable-next-line react/forbid-prop-types
  selectAttrs?: object;
  /**
   * The Select identifier. Links the htmlFor of the field to the select identifier.
   */
  selectId: string;
  /**
   * Whether to append the 'optional' label to a non-required field label.
   */
  showOptional?: boolean;
  /**
   * The value of the select. Can be a string or number.
   */
  value?: number | string;
}

const defaultProps = {
  disabled: false,
  hideRequired: false,
  isIncomplete: false,
  isInline: false,
  isInvalid: false,
  isLabelHidden: false,
  required: false,
  showOptional: false,
};
const NativeSelectField = ({
  defaultValue,
  disabled,
  error,
  help,
  hideRequired,
  isIncomplete,
  isInline,
  isLabelHidden,
  isInvalid,
  label,
  labelAttrs,
  maxWidth,
  onChange,
  options,
  required,
  selectAttrs,
  selectId,
  showOptional,
  value,
  ...customProps
}) => {
  const helpText = help ? <span>{help}</span> : undefined;
  let ariaDescriptionIds;
  if (help && error && isInvalid) {
    ariaDescriptionIds = `${selectId}-error ${selectId}-help`;
  } else {
    if (help) {
      ariaDescriptionIds = `${selectId}-help`;
    }
    if (error && isInvalid) {
      ariaDescriptionIds = `${selectId}-error`;
    }
  }
  return (
    <Field
      {...customProps}
      label={label}
      labelAttrs={labelAttrs}
      error={error}
      help={helpText}
      hideRequired={hideRequired}
      required={required}
      showOptional={showOptional}
      isInvalid={isInvalid}
      isInline={isInline}
      isLabelHidden={isLabelHidden}
      htmlFor={selectId}
      maxWidth={maxWidth}
    >
      <NativeSelect
        attrs={selectAttrs}
        id={selectId}
        ariaDescribedBy={ariaDescriptionIds}
        ariaLabel={label}
        disabled={disabled}
        isIncomplete={isIncomplete}
        isInvalid={isInvalid}
        defaultValue={defaultValue}
        required={required}
        onChange={onChange}
        options={options}
        value={value}
      />
    </Field>
  );
};
NativeSelectField.defaultProps = defaultProps;
export default NativeSelectField;
