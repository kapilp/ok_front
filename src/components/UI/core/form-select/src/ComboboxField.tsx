import Field from 'terra-form-field';
import Combobox from './Combobox';
import OptGroup from './shared/_OptGroup';
import Option from './shared/_Option';

const defaultProps = {
  allowClear: false,
  children: undefined,
  defaultValue: undefined,
  disabled: false,
  error: undefined,
  help: undefined,
  hideRequired: false,
  isIncomplete: false,
  isInline: false,
  isInvalid: false,
  isTouchAccessible: false,
  isLabelHidden: false,
  labelAttrs: {},
  maxHeight: undefined,
  maxWidth: undefined,
  onChange: undefined,
  placeholder: undefined,
  required: false,
  selectAttrs: {},
  showOptional: false,
  value: undefined,
};
interface ComboboxFieldProps {
  /**
   * Whether a clear option is available to clear the selection.
   */
  allowClear?: boolean;
  /**
   * The select options.
   */
  children?: JSX.Element;
  /**
   * The field label.
   */
  label: string;
  /**
   * The default value of the select. Can be a string, number, or array of strings/numbers.
   */
  defaultValue?: string | number | unknown[];
  /**
   * Whether the input is disabled.
   */
  disabled?: boolean;
  /**
   * Error message displayed when the select is invalid.
   */
  error?: JSX.Element;
  /**
   * Help message to display with the select.
   */
  help?: JSX.Element;
  /**
   * Whether to hide the required indicator on the label.
   */
  hideRequired?: boolean;
  /**
   * Whether the field displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the field is displayed inline. Displays block by default.
   */
  isInline?: boolean;
  /**
   * Whether the field displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Whether the label is hidden. Allows hiding the label while meeting accessibility guidelines.
   */
  isLabelHidden?: boolean;
  /**
   * Ensures touch accessibility by rendering the dropdown inline without a portal.
   *
   * Note: When enabled the dropdown will clip if rendered within a container that has an overflow: hidden ancestor.
   * The dropdown may also appear beneath content if rendered within a container that has an overflow: auto ancestor.
   */
  isTouchAccessible?: boolean;
  /**
   * Additional attributes to spread onto the label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelAttrs?: object;
  /**
   * The max height of the dropdown.
   */
  maxHeight?: number;
  /**
   * Set the max-width of a field using `length` or `%`.  Best practice recommendation to never exceed
   * a rendered value of 1020px. _(Note: Providing custom inline styles will take precedence.)_
   */
  maxWidth?: string;
  /**
   * Callback function triggered when the select value changes. function(value)
   */
  onChange?(...args: unknown[]): unknown;
  /**
   * Placeholder text.
   */
  placeholder?: string;
  /**
   * Whether the field is required.
   */
  required?: boolean;
  /**
   * Additional attributes to spread onto the select.
   */
  // eslint-disable-next-line react/forbid-prop-types
  selectAttrs?: object;
  /**
   * The Select identifier. Links the htmlFor of the field to the select identifier.
   */
  selectId: string;
  /**
   * Whether to append the 'optional' label to a non-required field label.
   */
  showOptional?: boolean;
  /**
   * The value of the select. Can be a string, number, or array of strings/numbers.
   */
  value?: string | number | unknown[];
}
const ComboboxField = ({
  allowClear,
  children,
  defaultValue,
  disabled,
  error,
  help,
  hideRequired,
  isIncomplete,
  isInline,
  isInvalid,
  isLabelHidden,
  isTouchAccessible,
  label,
  labelAttrs,
  maxHeight,
  maxWidth,
  onChange,
  placeholder,
  required,
  selectAttrs,
  selectId,
  showOptional,
  value,
  ...customProps
}) => {
  let ariaDescriptionIds;
  if (help && error && isInvalid) {
    ariaDescriptionIds = `${selectId}-error ${selectId}-help`;
  } else {
    if (help) {
      ariaDescriptionIds = `${selectId}-help`;
    }
    if (error && isInvalid) {
      ariaDescriptionIds = `${selectId}-error`;
    }
  }
  return (
    <Field
      {...customProps}
      label={label}
      labelAttrs={labelAttrs}
      error={error}
      help={help}
      hideRequired={hideRequired}
      required={required}
      showOptional={showOptional}
      isInvalid={isInvalid}
      isInline={isInline}
      isLabelHidden={isLabelHidden}
      htmlFor={selectId}
      maxWidth={maxWidth}
    >
      <Combobox
        {...selectAttrs}
        ariaLabel={label}
        allowClear={allowClear}
        aria-describedby={ariaDescriptionIds}
        disabled={selectAttrs.disabled || disabled}
        inputId={selectId}
        isIncomplete={isIncomplete}
        isInvalid={isInvalid}
        isTouchAccessible={isTouchAccessible}
        defaultValue={defaultValue}
        maxHeight={maxHeight || selectAttrs.maxHeight}
        onChange={onChange}
        placeholder={placeholder}
        required={required}
        value={value}
      >
        {children}
      </Combobox>
    </Field>
  );
};
ComboboxField.defaultProps = defaultProps;
ComboboxField.Option = Option;
ComboboxField.OptGroup = OptGroup;
export default ComboboxField;
