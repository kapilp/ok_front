// import { injectIntl } from 'react-intl';
import Field from 'terra-form-field';
import Select from './Select';
import Variants from './shared/_variants';

interface MyComponentProps {
  /**
   * Whether a clear option is available to clear the selection.
   * This is not applicable to the `multiple` or `tag` variants since the selection can already be deselected using the tag.
   */
  allowClear?: boolean;
  /**
   * The select options.
   */
  children?: JSX.Element;
  /**
   * The field label.
   */
  label: string;
  /**
   * The default value of the select. Can be a string, number, or array of strings/numbers.
   */
  defaultValue?: unknown[] | number | string;
  /**
   * Whether the input is disabled.
   */
  disabled?: boolean;
  /**
   * Error message displayed when the select is invalid.
   */
  error?: JSX.Element;
  /**
   * Help message to display with the select.
   */
  help?: JSX.Element;
  /**
   * Whether to hide the required indicator on the label.
   */
  hideRequired?: boolean;
  /**
   * @private
   * The intl object containing translations. This is retrieved from the context automatically by injectIntl.
   */
  intl: {
    formatMessage?(...args: unknown[]): unknown;
  };
  /**
   * Whether the field displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete?: boolean;
  /**
   * Whether the field is displayed inline. Displays block by default.
   */
  isInline?: boolean;
  /**
   * Whether the field displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid?: boolean;
  /**
   * Whether the label is hidden. Allows hiding the label while meeting accessibility guidelines.
   */
  isLabelHidden?: boolean;
  /**
   * Ensures touch accessibility by rendering the dropdown inline without a portal.
   *
   * Note: When enabled the dropdown will clip if rendered within a container that has an overflow: hidden ancestor.
   * The dropdown may also appear beneath content if rendered within a container that has an overflow: auto ancestor.
   *
   * Only applicable to variants that include an input (e.g. `combobox`, `multiple`, `search`, and `tag`).
   */
  isTouchAccessible?: boolean;
  /**
   * Additional attributes to spread onto the label.
   */
  // eslint-disable-next-line react/forbid-prop-types
  labelAttrs?: object;
  /**
   * The max height of the dropdown.
   */
  maxHeight?: number;
  /**
   * The maximum number of options that can be selected. A value less than 2 will be ignored.
   * Only applicable to variants allowing multiple selections (e.g.; `multiple`; `tag`).
   */
  maxSelectionCount?: number;
  /**
   * Set the max-width of a field using `length` or `%`.  Best practice recommendation to never exceed
   * a rendered value of 1020px. _(Note: Providing custom inline styles will take precedence.)_
   */
  maxWidth?: string;
  /**
   * Callback function triggered when the select value changes. function(value)
   */
  onChange?(...args: unknown[]): unknown;
  /**
   * Placeholder text.
   */
  placeholder?: string;
  /**
   * Whether the field is required.
   */
  required?: boolean;
  /**
   * Additional attributes to spread onto the select.
   */
  // eslint-disable-next-line react/forbid-prop-types
  selectAttrs?: object;
  /**
   * The Select identifier. Links the htmlFor of the field to the select identifier.
   */
  selectId: string;
  /**
   * Whether to append the 'optional' label to a non-required field label.
   */
  showOptional?: boolean;
  /**
   * The value of the select. Can be a string, number, or array of strings/numbers.
   */
  value?: unknown[] | number | string;
  /**
   * The select variant. One of `default`, `combobox`, `multiple`, `tag`, or `search`.
   */
  variant?: typeof Variants[keyof typeof Variants];
}
const defaultProps = {
  allowClear: false,
  children: undefined,
  defaultValue: undefined,
  disabled: false,
  error: undefined,
  help: undefined,
  hideRequired: false,
  isIncomplete: false,
  isInline: false,
  isInvalid: false,
  isLabelHidden: false,
  isTouchAccessible: false,
  labelAttrs: {},
  maxSelectionCount: undefined,
  maxHeight: undefined,
  maxWidth: undefined,
  onChange: undefined,
  placeholder: undefined,
  required: false,
  selectAttrs: {},
  showOptional: false,
  value: undefined,
  variant: 'default',
};
export const SelectField = ({
  allowClear,
  children,
  defaultValue,
  disabled,
  error,
  help,
  hideRequired,
  intl,
  isIncomplete,
  isInline,
  isInvalid,
  isLabelHidden,
  isTouchAccessible,
  label,
  labelAttrs,
  maxSelectionCount,
  maxHeight,
  maxWidth,
  onChange,
  placeholder,
  required,
  selectAttrs,
  selectId,
  showOptional,
  value,
  variant,
  ...customProps
}) => {
  let helpText = help;
  if (maxSelectionCount !== undefined && maxSelectionCount >= 2) {
    const limitSelectionText = intl.formatMessage({ id: 'Terra.form.select.maxSelectionHelp' }, { text: maxSelectionCount });
    if (help) {
      helpText = (
        <span>
          {limitSelectionText} {help}
        </span>
      );
    } else {
      helpText = limitSelectionText;
    }
  }
  let ariaDescriptionIds;
  if (help && error && isInvalid) {
    ariaDescriptionIds = `${selectId}-error ${selectId}-help`;
  } else {
    if (help) {
      ariaDescriptionIds = `${selectId}-help`;
    }
    if (error && isInvalid) {
      ariaDescriptionIds = `${selectId}-error`;
    }
  }
  return (
    <Field
      {...customProps}
      label={label}
      labelAttrs={labelAttrs}
      error={error}
      help={helpText}
      hideRequired={hideRequired}
      required={required}
      showOptional={showOptional}
      isInvalid={isInvalid}
      isInline={isInline}
      isLabelHidden={isLabelHidden}
      htmlFor={selectId}
      maxWidth={maxWidth}
    >
      <Select
        {...selectAttrs}
        ariaLabel={label}
        allowClear={allowClear}
        aria-describedby={ariaDescriptionIds}
        disabled={selectAttrs.disabled || disabled}
        id={selectId}
        isIncomplete={isIncomplete}
        isInvalid={isInvalid}
        isTouchAccessible={isTouchAccessible}
        defaultValue={defaultValue}
        maxHeight={maxHeight || selectAttrs.maxHeight}
        maxSelectionCount={maxSelectionCount !== undefined && maxSelectionCount < 2 ? undefined : maxSelectionCount}
        onChange={onChange}
        placeholder={placeholder}
        required={required}
        value={value}
        variant={variant}
      >
        {children}
      </Select>
    </Field>
  );
};
SelectField.defaultProps = defaultProps;
SelectField.Option = Select.Option;
SelectField.OptGroup = Select.OptGroup;
// export default injectIntl(SelectField);
