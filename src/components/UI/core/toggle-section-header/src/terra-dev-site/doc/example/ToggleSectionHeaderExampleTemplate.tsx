import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ToggleSectionHeader } from '../../../ToggleSectionHeader';

interface Properties {
  /**
   * Props to be spread onto TerraSectionHeader
   */
  // eslint-disable-next-line react/forbid-prop-types
  exampleProps: {};
  /**
   * Props to populate children of TerraSectionHeader
   */
  children: JSX.Element;
}
export const ToggleSectionHeaderExampleTemplate = (props: Properties) => (
  <div>
    <ToggleSectionHeader {...props.exampleProps}>{props.children}</ToggleSectionHeader>
  </div>
);
