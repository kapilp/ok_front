import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import ToggleSectionHeader from '../../../ToggleSectionHeader';
type ProgrammaticToggleState = {
  isOpen?: boolean;
};
class ProgrammaticToggle extends React.Component<{}, ProgrammaticToggleState> {
  constructor(props) {
    super(props);
    state = {
      isOpen: false,
    };
    toggle = toggle.bind(this);
  }
  toggle() {
    setState(prevState => ({ isOpen: !prevState.isOpen }));
  }
  render() {
    return (
      <div>
        <Button id="toggle" text="Trigger Toggle" onClick={toggle} />
        <ToggleSectionHeader isOpen={state.isOpen} title="Default Toggle Section Header">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </ToggleSectionHeader>
      </div>
    );
  }
}
export default ProgrammaticToggle;
