import {  JSX, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { SectionHeader } from '../../section-header/src/SectionHeader';
import { Toggle } from '../../toggle/src/Toggle';

const VALID_LEVELS = [1, 2, 3, 4, 5, 6] as const;
interface IToggleSectionHeaderProps extends JSX.HTMLAttributes<Element> {
  /**
   * Content in the body of the toggle section header component that will be expanded or hidden.
   */
  children: JSX.Element;
  /**
   * Text to be displayed in the section header.
   */
  title: string;
  /**
   * Sets the heading level. One of h1, h2, h3, h4, h5, or h6.
   */
  level?: typeof VALID_LEVELS;
  /**
   * Callback function triggered when toggle-section-header is closed.
   */
  onClose?: () => void;
  /**
   * Callback function triggered when toggle-section-header is opened.
   */
  onOpen?: () => void;
  /**
   * Used to set props and HTML attributes on the underlying section-header.
   */
  // eslint-disable-next-line react/forbid-prop-types
  sectionHeaderAttrs?: {};
  /**
   * Sets the toggle-section-header to be animated when it is opened or closed.
   */
  isAnimated?: boolean;
  /**
   * Sets the toggle-section-header initial state to open.
   */
  isInitiallyOpen?: boolean;
  /**
   * Sets the background of the section header to transparent.
   */
  isTransparent?: boolean;
  /**
   * Programmatically toggle the toggle-section-header component.
   */
  isOpen?: boolean;
}
const defaultProps = {
  level: 2,
  sectionHeaderAttrs: {},
  isAnimated: false,
  isInitiallyOpen: false,
  isTransparent: false,
  isOpen: undefined,
};
function getDerivedStateFromProps(nextProps, prevState) {
  if (nextProps.isOpen !== undefined && prevState.isOpen !== nextProps.isOpen) {
    return { isOpen: nextProps.isOpen };
  }
  return { isOpen: prevState.isOpen };
}
export const ToggleSectionHeader = (props: IToggleSectionHeaderProps) => {
  props = mergeProps({}, defaultProps, props);

  const [state, setState] = createStore({ isOpen: props.isInitiallyOpen ?? false });

  const handleOnClick = (event: Event) => {
    event.preventDefault();

    if (props.isOpen === undefined) {
      if (props.onOpen && !state.isOpen) {
        props.onOpen();
      } else if (props.onClose && state.isOpen) {
        props.onClose();
      }
      setState({
        isOpen: !state.isOpen,
      });
    }
  };
  const wrapOnClick = (onClick: (event: MouseEvent) => void) => {
    return (event: MouseEvent) => {
      handleOnClick(event);
      if (onClick) {
        onClick(event);
      }
    };
  };
  const [p, customProps] = splitProps(props, ['children', 'title', 'level', 'sectionHeaderAttrs', 'isAnimated', 'isInitiallyOpen', 'isTransparent', 'isOpen', 'onOpen', 'onClose']);

  const sectionHeaderProps = () => {
    return { ...p.sectionHeaderAttrs, onClick: wrapOnClick(p.sectionHeaderAttrs.onClick) };
  };

  return (
    <div {...customProps}>
      <SectionHeader {...sectionHeaderProps()} aria-expanded={state.isOpen} isOpen={state.isOpen} level={p.level} title={p.title} isTransparent={p.isTransparent} />
      <Toggle isAnimated={p.isAnimated} isOpen={state.isOpen}>
        {p.children}
      </Toggle>
    </div>
  );
};
