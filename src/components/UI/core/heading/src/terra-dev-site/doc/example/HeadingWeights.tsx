import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Heading } from '../../../Heading';
export const HeadingWeights = () => (
  <div>
    <Heading level={3} weight={200}>
      Font Weight: 200
    </Heading>
    <Heading level={3} weight={400}>
      Font Weight: 400
    </Heading>
    <Heading level={3} weight={700}>
      Font Weight: 700
    </Heading>
  </div>
);
