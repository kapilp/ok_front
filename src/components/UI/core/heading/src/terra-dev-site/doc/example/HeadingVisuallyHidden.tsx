import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { Heading } from '../../../Heading';
export const HeadingVisuallyHidden = () => (
  <div>
    <p>
      The example below includes a <code>&lt;Heading /&gt;</code> component using the
      <code> isVisuallyHidden</code> prop.
    </p>
    <Heading level={3} isVisuallyHidden>
      This text is not visible, however it is accessible to screen readers.
    </Heading>
  </div>
);
