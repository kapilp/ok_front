import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';

import classNames from 'classnames/bind';
import styles from './colors.module.scss';
import { Heading } from '../../../Heading';
const cx = classNames.bind(styles);
export const HeadingColors = () => (
  <div>
    <Heading level={3} colorClass={cx(['attention'])}>
      {"Font Color: 'attention' class"}
    </Heading>
    <Heading level={3} colorClass={cx(['success'])}>
      {"Font Color: 'success' class"}
    </Heading>
    <Heading level={3} colorClass={cx(['info'])}>
      {"Font Color: 'info' class"}
    </Heading>
  </div>
);
