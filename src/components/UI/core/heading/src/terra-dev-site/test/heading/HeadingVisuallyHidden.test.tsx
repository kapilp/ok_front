import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Heading from '../../../Heading';
const HeadingVisuallyHidden = () => (
  <div>
    <span>Heading below is visually hidden but available to screen readers</span>
    <Heading id="heading-visually-hidden" level={1} isVisuallyHidden>
      Visually Hidden
    </Heading>
  </div>
);
export default HeadingVisuallyHidden;
