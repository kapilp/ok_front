import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Heading from '../../../Heading';
const HeadingItalics = () => (
  <div>
    <Heading id="heading-italic" level={1} isItalic>
      Italics
    </Heading>
  </div>
);
export default HeadingItalics;
