import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Heading from '../../../Heading';
const HeadingWeight = () => (
  <div>
    <Heading id="heading-weight-700" level={1} weight={700}>
      700 Weight
    </Heading>
    <Heading id="heading-weight-400" level={1} weight={400}>
      400 Weight
    </Heading>
    <Heading id="heading-weight-200" level={1} weight={200}>
      200 Weight
    </Heading>
  </div>
);
export default HeadingWeight;
