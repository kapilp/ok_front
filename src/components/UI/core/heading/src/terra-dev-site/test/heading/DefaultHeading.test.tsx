import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import Heading from '../../../Heading';
export default () => (
  <Heading id="heading-default" level={1}>
    Default
  </Heading>
);
