import {  JSX, mergeProps, Show, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames/bind';
import styles from './Heading.module.scss';
const cx = classNames.bind(styles);
/* eslint-disable quote-props */
export const HeadingLevel = {
  '1': 1,
  '2': 2,
  '3': 3,
  '4': 4,
  '5': 5,
  '6': 6,
};
enum HeadingSize {
  MINI = 'mini',
  TINY = 'tiny',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  HUGE = 'huge',
}
export const HeadingWeight = {
  '200': 200,
  '400': 400,
  '700': 700,
};
/* eslint-enable quote-props */
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * Child nodes
   */
  children: JSX.Element;
  /**
   * Sets the heading level. One of `1`, `2`, `3`, `4`, `5`, `6`.
   */
  level: 1 | 2 | 3 | 4 | 5 | 6;
  /**
   * Sets the text to display in italics.
   */
  isItalic?: boolean;
  /**
   * Sets the text to appear visually hidden. The text will still be available to screen readers.
   */
  isVisuallyHidden?: boolean;
  /**
   * Sets the text size. One of `mini`, `tiny`, `small`, `medium`, `large`, `huge`.
   */
  size?: 'mini' | 'tiny' | 'small' | 'medium' | 'large' | 'huge';
  /**
   * Sets the text size. One of `200`, `400`, `700`.
   */
  weight?: 200 | 400 | 700;
  /**
   * Sets an author defined class, to control the colors of the heading.
   *
   * ![IMPORTANT](https://badgen.net/badge//IMPORTANT/blue?icon=github)
   * Adding `var(--my-app...` CSS variables is required for proper re-themeability when creating custom color styles _(see included examples)_.
   */
  colorClass?: string;
}
const defaultProps = {
  isItalic: false,
  isVisuallyHidden: false,
  weight: 700,
};
export const Heading = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);

  const Element = `h${props.level}`;
  const [extracted, customProps] = splitProps(props, ['level', 'children', 'isVisuallyHidden', 'isItalic', 'size', 'weight', 'colorClass']);
  return (
    <Dynamic
      component={Element}
      {...customProps}
      className={cx([
        'heading',
        { italic: props.isItalic },
        { 'visually-hidden': props.isVisuallyHidden },
        { [`level-${props.level}`]: props.level },
        { [`size-${props.size}`]: props.size },
        { [`weight-${props.weight}`]: props.weight },
        props.colorClass,
        customProps.className,
      ])}
    >
      {props.children}
    </Dynamic>
  );
};
export { HeadingSize };
