import { ET, makeTableArgs, TableResultAll } from '../../utils/enums';
import { Ws } from '../../utils/ws_events_dispatcher';
import { JSX, createEffect } from 'solid-js';
import { isObj } from 'ramda-adjunct';

export const fetchHtml = async (html: string[]): Promise<TableResultAll> => {
  if (!html.length) return '';
  return new Promise((resolve, reject) => {
    Ws.bindT(
      [ET[ET.get], 'template_list', Ws.uid],
      d => {
        resolve(d);
      },
      makeTableArgs().setFilter({ name: html }).setHeader(false),
    );
  });
};
export const getTemplateHtml = (d: TableResultAll) => {
  let resultString = '';
  if (isObj(d)) {
    const result = d.r.result;

    if (Array.isArray(result)) {
      for (let i = 0; i < result.length; i++) {
        resultString += result[i]['template'] as string;
      }
    }
  }
  return resultString;
};
export const HtmlBaseDiv = (props: { value: string; className?: string }) => {
  const t = document.createElement('div');
  return () => {
    if (props.className) t.className = props.className;
    t.innerHTML = props.value;
    return t;
  };
};
export const HtmlBase = (props: { value: string }) => {
  const t = document.createElement('template');
  return () => {
    t.innerHTML = props.value;
    return [...t.content.childNodes];
  };
};
export const Html = (props: { html: string[] }) => {
  let divRef: HTMLElement;

  createEffect(() => {
    fetchHtml(props.html).then(d => {
      divRef.innerHTML = getTemplateHtml(d);
    });
  });
  return <div ref={divRef}></div>;
};
