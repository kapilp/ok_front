// ideally will get replaced with external dep
// when rafrex/detect-passive-events#4 and rafrex/detect-passive-events#5 get merged in
import { JSX,  onCleanup, mergeProps, splitProps  } from 'solid-js';

export const testPassiveEventSupport = () => {
  if (typeof window === 'undefined' || typeof window.addEventListener !== 'function') {
    return;
  }
  let passive = false;
  const options = Object.defineProperty({}, 'passive', {
    get() {
      passive = true;
    },
  });
  const noop = () => {};
  window.addEventListener('testPassiveEventSupport', noop, options);
  window.removeEventListener('testPassiveEventSupport', noop, options);
  return passive;
};

/**
 * Check whether some DOM node is our Component's node.
 */
export function isNodeFound(current, componentNode, ignoreClass) {
  if (current === componentNode) {
    return true;
  }
  // SVG <use/> elements do not technically reside in the rendered DOM, so
  // they do not have classList directly, but they offer a link to their
  // corresponding element, which can have classList. This extra check is for
  // that case.
  // See: http://www.w3.org/TR/SVG11/struct.html#InterfaceSVGUseElement
  // Discussion: https://github.com/Pomax/react-onclickoutside/pull/17
  if (current.correspondingElement) {
    return current.correspondingElement.classList.contains(ignoreClass);
  }
  return current.classList.contains(ignoreClass);
}
/**
 * Try to find our node in a hierarchy of nodes, returning the document
 * node as highest node if our node is not found in the path up.
 */
export function findHighest(current, componentNode, ignoreClass) {
  if (current === componentNode) {
    return true;
  }
  // If source=local then this event came from 'somewhere'
  // inside and should be ignored. We could handle this with
  // a layered approach, too, but that requires going back to
  // thinking in terms of Dom node nesting, running counter
  // to React's 'you shouldn't care about the DOM' philosophy.
  while (current.parentNode) {
    if (isNodeFound(current, componentNode, ignoreClass)) {
      return true;
    }
    current = current.parentNode;
  }
  return current;
}
/**
 * Check if the browser scrollbar was clicked
 */
export function clickedScrollbar(evt) {
  return document.documentElement.clientWidth <= evt.clientX || document.documentElement.clientHeight <= evt.clientY;
}

export function autoInc(seed = 0) {
  return () => ++seed;
}


let passiveEventSupport;
const handlersMap = {} as {[id: number]: (event: Event)=>void};
const enabledInstances = {} as {[id: number]: boolean};
const touchEvents = ["touchstart", "touchmove"];
export const IGNORE_CLASS_NAME = "ignore-react-onclickoutside";
/**
 * Options for addEventHandler and removeEventHandler
 */
function getEventHandlerOptions(instance, eventName) {
  let handlerOptions = null;
  const isTouchEvent = touchEvents.indexOf(eventName) !== -1;
  if (isTouchEvent && passiveEventSupport) {
    handlerOptions = { passive: !instance.props.preventDefault };
  }
  return handlerOptions;
}
/**
 * This function generates the HOC function that you'll use
 * in order to impart onOutsideClick listening to an
 * arbitrary component. It gets called at the end of the
 * bootstrapping code to yield an instance of the
 * onClickOutsideHOC function defined inside setupHOC().
 */
export const OnClickOutside=(props:{}) =>{
    let __clickOutsideHandlerProp: any;
    let _uid: number;
    let componentNode: any;
    let instanceRef: any;

    props = mergeProps({},  {
      eventTypes: ["mousedown", "touchstart"],
      excludeScrollbar: false,
      outsideClickIgnoreClass: IGNORE_CLASS_NAME,
      preventDefault: false,
      stopPropagation: false
    }, props);

    /**
     * Access the WrappedComponent's instance.
     */

    const __outsideClickHandler = (event:Event) => {
      if (typeof __clickOutsideHandlerProp === "function") {
        __clickOutsideHandlerProp(event);
        return;
      }
      const instance = getInstance();
      if (typeof instance.props.handleClickOutside === "function") {
        instance.props.handleClickOutside(event);
        return;
      }
      if (typeof instance.handleClickOutside === "function") {
        instance.handleClickOutside(event);
        return;
      }
      throw new Error(
        `WrappedComponent: ${componentName} lacks a handleClickOutside(event) function for processing outside click events.`
      );
    };
    const __getComponentNode = () => {
      const instance = getInstance();
      if (config && typeof config.setClickOutsideRef === "function") {
        return config.setClickOutsideRef()(instance);
      }
      if (typeof instance.setClickOutsideRef === "function") {
        return instance.setClickOutsideRef();
      }
      return findDOMNode(instance);
    };
    /**
     * Add click listeners to the current document,
     * linked to this component's state.
     */
    componentDidMount() {
      // If we are in an environment without a DOM such
      // as shallow rendering or snapshots then we exit
      // early to prevent any unhandled errors being thrown.
      if (typeof document === "undefined" || !document.createElement) {
        return;
      }
      const instance = getInstance();
      if (typeof props.handleClickOutside === "function") {
        __clickOutsideHandlerProp = props.handleClickOutside(instance);
        if (typeof __clickOutsideHandlerProp !== "function") {
          throw new Error(
            `WrappedComponent: ${componentName} lacks a function for processing outside click events specified by the handleClickOutside config option.`
          );
        }
      }
      componentNode = __getComponentNode();
      // return early so we dont initiate onClickOutside
      if (props.disableOnClickOutside) return;
      enableOnClickOutside();
    }
   /*componentDidUpdate() {
      componentNode = __getComponentNode();
    }*/
    /**
     * Remove all document's event listeners for this component
     */
    onCleanup(()=> {
      disableOnClickOutside();
    })
    /**
     * Can be called to explicitly enable event listening
     * for clicks and touches outside of this element.
     */
    const enableOnClickOutside = () => {
      if (typeof document === "undefined" || enabledInstances[_uid]) {
        return;
      }
      if (typeof passiveEventSupport === "undefined") {
        passiveEventSupport = testPassiveEventSupport();
      }
      enabledInstances[_uid] = true;
      let events = props.eventTypes;
      if (!events.forEach) {
        events = [events];
      }
      handlersMap[_uid] = (event:Event) => {
        if (componentNode === null) return;
        if (props.preventDefault) {
          event.preventDefault();
        }
        if (props.stopPropagation) {
          event.stopPropagation();
        }
        if (props.excludeScrollbar && clickedScrollbar(event))
          return;
        const current = event.target;
        if (
          findHighest(
            current,
            componentNode,
            props.outsideClickIgnoreClass
          ) !== document
        ) {
          return;
        }
        __outsideClickHandler(event);
      };
      events.forEach(eventName => {
        document.addEventListener(
          eventName,
          handlersMap[_uid],
          getEventHandlerOptions(instanceRef, eventName)
        );
      });
    };
    /**
     * Can be called to explicitly disable event listening
     * for clicks and touches outside of this element.
     */
    const disableOnClickOutside = () => {
      delete enabledInstances[_uid];
      const fn = handlersMap[_uid];
      if (fn && typeof document !== "undefined") {
        let events = props.eventTypes;
        if (!events.forEach) {
          events = [events];
        }
        events.forEach(eventName =>
          document.removeEventListener(
            eventName,
            fn,
            getEventHandlerOptions(instanceRef, eventName)
          )
        );
        delete handlersMap[_uid];
      }
    };
    getRef = ref => (instanceRef = ref);
    /**
     * Pass-through render
     */
  const [extracted, customProps] = splitProps(props, [
    "excludeScrollbar"
      ]);

    /*  if (WrappedComponent.prototype.isReactComponent) {
    customProps.ref = getRef;
      } else {
    customProps.wrappedRef = getRef;
      }
  customProps.disableOnClickOutside = customProps.disableOnClickOutside;
  customProps.enableOnClickOutside = customProps.enableOnClickOutside;
      return createElement(WrappedComponent, customProps);
    */
  };
};
