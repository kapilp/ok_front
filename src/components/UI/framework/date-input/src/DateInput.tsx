import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import { injectIntl, intlShape } from "react-intl";
import Input from "terra-form-input";
import * as KeyCode from "keycode-js";
import DateInputUtil from "./DateInputUtil";
import styles from "./DateInput.module.scss";
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Name of the date input. This name should be unique.
   */
  name: string.isRequired,
  /**
   * Custom input attributes to apply to the day input
   */
  dayAttributes: PropTypes.object,
  /**
   * Whether the date input should be disabled.
   */
  disabled: boolean,
  /**
   * Can be used to set the display format. One of `month-day-year` or `day-month-year`.
   * Overrides default locale display format.
   */
  displayFormat: PropTypes.oneOf(["month-day-year", "day-month-year"]),
  /**
   * @private
   * Intl object injected from injectIntl
   */
  intl: intlShape,
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete: boolean,
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid: boolean,
  /**
   * Custom input attributes to apply to the month select
   */
  monthAttributes: PropTypes.object,
  /**
   * A callback function to execute when the entire date input component loses focus.
   * This event does not get triggered when the focus is moved from one input to another within the date input component because the focus is still within the main date input component.
   */
  onBlur: PropTypes.func,
  /**
   * A callback function to execute when a date is entered.
   * The first parameter is the event. The second parameter is the changed date value.
   */
  onChange: PropTypes.func,
  /**
   * A callback function triggered when the day, month, or year input receives focus.
   */
  onFocus: PropTypes.func,
  /**
   * Callback ref to access date input containing DOM element.
   */
  ref: PropTypes.func,
  /**
   * Whether is the date input is required
   */
  required: boolean,
  /**
   * An date string representation of the date value used for the component. This should be in ISO 8601 format: YYYY-MM-DD.
   */
  value: string,
  /**
   * Custom input attributes to apply to the year input
   */
  yearAttributes: PropTypes.object
};
const defaultProps = {
  disabled: false,
  monthAttributes: {},
  dayAttributes: {},
  yearAttributes: {},
  onBlur: null,
  onChange: null,
  onFocus: undefined,
  ref: undefined,
  value: undefined
};
interface IDateInputProps extends JSX.HTMLAttributes<Element> {
  disabled?: any;
  displayFormat?: any;
  dayAttributes?: any;
  monthAttributes?: any;
  yearAttributes?: any;
  intl?: any;
  isInvalid?: any;
  isIncomplete?: any;
  onBlur?: any;
  onChange?: any;
  onFocus?: any;
  ref?: any;
  required?: any;
  name?: any;
  value?: any;
  customProps?: any;
  locale?: any;
  formatMessage?: any;
}
type DateInputState = {
  month?: any,
  day?: any,
  year?: any,
  isFocused?: boolean,
  monthIsFocused?: boolean,
  dayIsFocused?: boolean,
  yearIsFocused?: boolean,
  isPlaceholderColored?: boolean
};
class DateInput extends React.Component<IDateInputProps, DateInputState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  context: any;
  dateInputContainer: HTMLDivElement;
  dayRef: any;
  monthRef: any;
  yearRef: any;
  constructor(props) {
    super(props);
    let { value } = props;
    if (value && !DateInputUtil.acceptedDateValue(value)) {
      if (process.env !== "production") {
        // eslint-disable-next-line no-console
        console.warn(
          `An invalid date value, ${value}, has been passed to the terra-date-input. ` +
            "This value has been ignored and will not be rendered. " +
            "Date values must be in YYYY-MM-DD format."
        );
      }
      value = undefined;
    }
    dateInputContainer = React.createRef();
    monthRef = React.createRef();
    handleMonthChange = handleMonthChange.bind(this);
    handleDayChange = handleDayChange.bind(this);
    handleYearChange = handleYearChange.bind(this);
    handleMonthKeyDown = handleMonthKeyDown.bind(this);
    handleDayKeyDown = handleDayKeyDown.bind(this);
    handleYearKeyDown = handleYearKeyDown.bind(this);
    handleFocus = handleFocus.bind(this);
    handleMonthFocus = handleMonthFocus.bind(this);
    handleDayFocus = handleDayFocus.bind(this);
    handleYearFocus = handleYearFocus.bind(this);
    focusMonth = focusMonth.bind(this);
    focusDay = focusDay.bind(this);
    focusYear = focusYear.bind(this);
    handleMonthBlur = handleMonthBlur.bind(this);
    handleDayBlur = handleDayBlur.bind(this);
    handleYearBlur = handleYearBlur.bind(this);
    formattedRender = formattedRender.bind(this);
    monthRender = monthRender.bind(this);
    dayRender = dayRender.bind(this);
    yearRender = yearRender.bind(this);
    handleMonthClick = handleMonthClick.bind(this);
    state = {
      month: DateInputUtil.splitMonth(value),
      day: DateInputUtil.splitDay(value),
      year: DateInputUtil.splitYear(value),
      isFocused: false,
      monthIsFocused: false,
      dayIsFocused: false,
      yearIsFocused: false,
      isPlaceholderColored: true
    };
  }
  componentDidUpdate(prevProps) {
    if (props.value === prevProps.value) {
      return;
    }
    // eslint-disable-next-line react/no-did-update-set-state
    setState({
      month: DateInputUtil.splitMonth(props.value),
      day: DateInputUtil.splitDay(props.value),
      year: DateInputUtil.splitYear(props.value)
    });
  }
  handleFocus(event) {
    if (
      props.onFocus &&
      !dateInputContainer.current.contains(event.relatedTarget)
    ) {
      props.onFocus(event);
    }
    setState({ isFocused: true });
  }
  handleMonthFocus(event) {
    handleFocus(event);
    setState({ monthIsFocused: true });
  }
  handleDayFocus(event) {
    handleFocus(event);
    setState({ dayIsFocused: true });
  }
  handleYearFocus(event) {
    handleFocus(event);
    setState({ yearIsFocused: true });
  }
  handleMonthBlur(event) {
    handleBlur(event, DateInputUtil.inputType.MONTH);
    if (state.month === "") {
      setState({ isPlaceholderColored: true });
    }
    setState({ monthIsFocused: false });
  }
  handleDayBlur(event) {
    handleBlur(event, DateInputUtil.inputType.DAY);
    setState({ dayIsFocused: false });
  }
  handleYearBlur(event) {
    handleBlur(event, DateInputUtil.inputType.YEAR);
    setState({ yearIsFocused: false });
  }
  handleBlur(event, type) {
    setState({ isFocused: false });
    if (type === DateInputUtil.inputType.DAY) {
      let stateValue = event.target.value;
      // Prepend a 0 to the value when losing focus and the value is single digit.
      if (stateValue.length === 1) {
        stateValue = "0".concat(stateValue);
        handleValueChange(event, type, stateValue);
      }
    }
    if (props.onBlur) {
      // Modern browsers support event.relatedTarget but event.relatedTarget returns null in IE 10 / IE 11.
      // IE 11 sets document.activeElement to the next focused element before the blur event is called.
      const activeTarget = event.relatedTarget
        ? event.relatedTarget
        : document.activeElement;
      // Handle blur only if focus has moved out of the entire date input component.
      if (!dateInputContainer.current.contains(activeTarget)) {
        props.onBlur(event);
      }
    }
  }
  /**
   * Takes a key input from the month select, and processes it based on the value of the keycode.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleMonthKeyDown(event) {
    const displayFormat = DateInputUtil.computedDisplayFormat(
      props.displayFormat,
      props.intl.locale
    );
    if (
      event.keyCode === KeyCode.KEY_BACK_SPACE ||
      event.keyCode === KeyCode.KEY_DELETE
    ) {
      handleValueChange(event, DateInputUtil.inputType.MONTH, "");
      if (displayFormat === "day-month-year" && event.target.value === "") {
        focusDay(event);
      }
    }
    if (
      event.keyCode === KeyCode.KEY_SPACE ||
      event.keyCode === KeyCode.KEY_UP ||
      event.keyCode === KeyCode.KEY_DOWN
    ) {
      setState({ isPlaceholderColored: false });
    }
  }
  /**
   * Takes a key input from the day input, and processes it based on the value of the keycode.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleDayKeyDown(event) {
    let stateValue = state.day || "0";
    const previousStateValue = stateValue;
    const displayFormat = DateInputUtil.computedDisplayFormat(
      props.displayFormat,
      props.intl.locale
    );
    // prevent e and . characters from being entered into number input on keyDown
    if (event.keyCode === 69 || event.keyCode === 190) {
      event.preventDefault();
      return;
    }
    if (event.keyCode === KeyCode.KEY_UP) {
      event.preventDefault();
      stateValue = DateInputUtil.incrementDay(stateValue);
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      event.preventDefault();
      stateValue = DateInputUtil.decrementDay(stateValue);
    }
    if (previousStateValue !== stateValue) {
      handleValueChange(event, DateInputUtil.inputType.DAY, stateValue);
    }
    if (
      event.keyCode === KeyCode.KEY_BACK_SPACE ||
      event.keyCode === KeyCode.KEY_DELETE
    ) {
      if (displayFormat === "month-day-year" && event.target.value === "") {
        focusMonth(event);
      }
    }
  }
  /**
   * Takes a key input from the year input, and processes it based on the value of the keycode.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleYearKeyDown(event) {
    let stateValue = state.year || "0";
    const previousStateValue = stateValue;
    const displayFormat = DateInputUtil.computedDisplayFormat(
      props.displayFormat,
      props.intl.locale
    );
    // prevent e and . characters from being entered into number input on keyDown
    if (event.keyCode === 69 || event.keyCode === 190) {
      event.preventDefault();
      return;
    }
    if (event.keyCode === KeyCode.KEY_UP) {
      event.preventDefault();
      stateValue = DateInputUtil.incrementYear(stateValue);
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      event.preventDefault();
      stateValue = DateInputUtil.decrementYear(stateValue);
    }
    if (previousStateValue !== stateValue) {
      handleValueChange(event, DateInputUtil.inputType.YEAR, stateValue);
    }
    if (
      event.keyCode === KeyCode.KEY_BACK_SPACE ||
      event.keyCode === KeyCode.KEY_DELETE
    ) {
      if (displayFormat === "month-day-year" && event.target.value === "") {
        focusDay(event);
      }
      if (displayFormat === "day-month-year" && event.target.value === "") {
        focusMonth(event);
      }
    }
  }
  handleMonthChange(event) {
    if (!DateInputUtil.validNumericInput(event.target.value)) {
      return;
    }
    const inputValue = event.target.value;
    const stateValue = state.month;
    const maxValue = 12;
    // Ignore the entry if the value did not change or it is invalid.
    if (inputValue === stateValue || Number(inputValue) > maxValue) {
      return;
    }
    handleValueChange(event, DateInputUtil.inputType.MONTH, inputValue);
  }
  handleDayChange(event) {
    if (!DateInputUtil.validNumericInput(event.target.value)) {
      return;
    }
    const inputValue = event.target.value;
    const stateValue = state.day;
    const maxValue = 31;
    // Ignore the entry if the value did not change or it is invalid.
    // When 'Predictive text' is enabled on Android the maxLength attribute on the input is ignored so we have
    // to check the length of inputValue to make sure that it is less then 2.
    if (
      inputValue === stateValue ||
      inputValue.length > 2 ||
      Number(inputValue) > maxValue ||
      (inputValue.length === 2 && !(inputValue > 0))
    ) {
      return;
    }
    handleValueChange(event, DateInputUtil.inputType.DAY, inputValue);
  }
  handleYearChange(event) {
    if (!DateInputUtil.validNumericInput(event.target.value)) {
      return;
    }
    const inputValue = event.target.value;
    const maxValue = 9999;
    // When 'Predictive text' is enabled on Android the maxLength attribute on the input is ignored so we have
    // to check the length of inputValue to make sure that it is less then 4.
    if (inputValue.length > 5 || Number(inputValue) > maxValue) {
      return;
    }
    handleValueChange(event, DateInputUtil.inputType.YEAR, inputValue);
  }
  /**
   * Shifts programmatic focus to day input
   * @param {Object} event Event object generated from the event delegation.
   */
  focusDay(event) {
    dayRef.focus();
    event.preventDefault();
  }
  /**
   * Shifts programmatic focus to year input
   * @param {Object} event Event object generated from the event delegation.
   */
  focusYear(event) {
    yearRef.focus();
    event.preventDefault();
  }
  /**
   * Shifts programmatic focus to month select
   * @param {Object} event Event object generated from the event delegation.
   */
  focusMonth(event) {
    monthRef.current.focus();
    event.preventDefault();
  }
  /**
   * On Click handler for month select
   */
  handleMonthClick() {
    setState({ isPlaceholderColored: false });
  }
  handleValueChange(event, type, value) {
    if (type === DateInputUtil.inputType.MONTH) {
      setState({ month: value });
    } else if (type === DateInputUtil.inputType.DAY) {
      setState({ day: value });
    } else if (type === DateInputUtil.inputType.YEAR) {
      setState({ year: value });
    }
    if (props.onChange) {
      const month =
        type === DateInputUtil.inputType.MONTH ? value : state.month;
      const day = type === DateInputUtil.inputType.DAY ? value : state.day;
      const year =
        type === DateInputUtil.inputType.YEAR ? value : state.year;
      if (month === "" && day === "" && year === "") {
        handleOnChange(event, "");
      } else {
        const dateString = `${year}-${month}-${day}`;
        handleOnChange(event, dateString);
      }
    }
  }
  /**
   * Calls onChange callback prop if it exists
   * @param {Object} event Event object generated from the event delegation.
   * @param {String} dateString Returns the dateString from the component
   */
  handleOnChange(event, dateString) {
    if (props.onChange) {
      props.onChange(event, dateString);
    }
  }
  /**
   * Renders month input
   */
  monthRender() {
    const DateInputMonthWrapperClassNames = cx([
      "month-select-wrapper",
      { focused: state.monthIsFocused },
      { disabled: props.disabled },
      { error: props.isInvalid },
      {
        incomplete:
          props.isIncomplete &&
          props.required &&
          !props.isInvalid
      }
    ]);
    const DateInputMonthClassNames = cx([
      "month-select",
      {
        "is-placeholder":
          state.month === "" && state.isPlaceholderColored
      },
      { focused: state.monthIsFocused },
      { disabled: props.disabled },
      { error: props.isInvalid },
      {
        incomplete:
          props.isIncomplete &&
          props.required &&
          !props.isInvalid
      }
    ]);
    return (
      <div className={DateInputMonthWrapperClassNames}>
        <select
          {...props.monthAttributes}
          aria-label={props.intl.formatMessage({
            id: "Terra.date.input.monthLabel"
          })}
          ref={monthRef}
          className={DateInputMonthClassNames}
          value={state.month}
          name={"terra-date-month-".concat(props.name)}
          onChange={handleMonthChange}
          onKeyDown={handleMonthKeyDown}
          onClick={handleMonthClick}
          onFocus={handleMonthFocus}
          onBlur={handleMonthBlur}
          disabled={props.disabled}
        >
          <option value="" hidden>
            {props.intl.formatMessage({
              id: "Terra.date.input.monthPlaceholder"
            })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.january"
            })}
            value="01"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.january" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.february"
            })}
            value="02"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.february" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.march"
            })}
            value="03"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.march" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.april"
            })}
            value="04"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.april" })}
          </option>
          <option
            key={props.intl.formatMessage({ id: "Terra.date.input.may" })}
            value="05"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.may" })}
          </option>
          <option
            key={props.intl.formatMessage({ id: "Terra.date.input.june" })}
            value="06"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.june" })}
          </option>
          <option
            key={props.intl.formatMessage({ id: "Terra.date.input.july" })}
            value="07"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.july" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.august"
            })}
            value="08"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.august" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.september"
            })}
            value="09"
          >
            {props.intl.formatMessage({
              id: "Terra.date.input.september"
            })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.october"
            })}
            value="10"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.october" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.november"
            })}
            value="11"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.november" })}
          </option>
          <option
            key={props.intl.formatMessage({
              id: "Terra.date.input.december"
            })}
            value="12"
          >
            {props.intl.formatMessage({ id: "Terra.date.input.december" })}
          </option>
        </select>
      </div>
    );
  }
  /**
   * Renders day input
   */
  dayRender() {
    /**
     * JAWS + Chrome is super buggy when it comes to up/down arrow keys cycling values on the input and only seems to work
     * when input[type=number]. This works great, except in Firefox where <input value="03" type="number" />
     * displays in the browsers as "3".
     * To work around this issue, the day input uses type="number" for all browsers, but if we're in a Mozilla browser,
     * we switch over to using type="text" and pattern="\d*" which allows displaying value="03" in the browser as "03"
     */
    const numberAttributes = window.matchMedia(
      "(min--moz-device-pixel-ratio:0)"
    ).matches
      ? { type: "text", pattern: "\\d*" }
      : { type: "number" };
    return (
      <Input
        {...props.dayAttributes}
        {...numberAttributes}
        ref={inputRef => {
          dayRef = inputRef;
        }}
        aria-label={props.intl.formatMessage({
          id: "Terra.date.input.dayLabel"
        })}
        className={cx("date-input-day", {
          "is-focused": state.dayIsFocused
        })}
        value={state.day}
        name={"terra-date-day-".concat(props.name)}
        placeholder={props.intl.formatMessage({
          id: "Terra.date.input.dayPlaceholder"
        })}
        maxLength="2"
        onChange={handleDayChange}
        onKeyDown={handleDayKeyDown}
        onFocus={handleDayFocus}
        onBlur={handleDayBlur}
        size="2"
        autoComplete="off"
        disabled={props.disabled}
        isInvalid={props.isInvalid}
        isIncomplete={props.isIncomplete}
        required={props.required}
      />
    );
  }
  /**
   * Renders year select
   */
  yearRender() {
    /**
     * JAWS + Chrome is super buggy when it comes to up/down arrow keys cycling values on the input and only seems to work
     * when input[type=number]. This works great, except in Firefox where <input value="03" type="number" /> displays the
     * value in the browsers as "3" instead of "03". https://bugzilla.mozilla.org/show_bug.cgi?id=1005603
     * To work around this issue, the year input uses type="number" for all browsers, but if we're in a Mozilla browser,
     * we switch over to using type="text" and pattern="\d*" which allows displaying value="03" in the browser as "03"
     */
    const numberAttributes = window.matchMedia(
      "(min--moz-device-pixel-ratio:0)"
    ).matches
      ? { type: "text", pattern: "\\d*" }
      : { type: "number" };
    return (
      <Input
        {...props.yearAttributes}
        {...numberAttributes}
        ref={inputRef => {
          yearRef = inputRef;
        }}
        aria-label={props.intl.formatMessage({
          id: "Terra.date.input.yearLabel"
        })}
        className={cx("date-input-year", {
          "is-focused": state.yearIsFocused
        })}
        value={state.year}
        name={"terra-date-year-".concat(props.name)}
        placeholder={props.intl.formatMessage({
          id: "Terra.date.input.yearPlaceholder"
        })}
        maxLength="4"
        onChange={handleYearChange}
        onKeyDown={handleYearKeyDown}
        onFocus={handleYearFocus}
        onBlur={handleYearBlur}
        size="4"
        autoComplete="off"
        disabled={props.disabled}
        isInvalid={props.isInvalid}
        isIncomplete={props.isIncomplete}
        required={props.required}
      />
    );
  }
  /**
   * Renders inputs in month-day-year or day-month-year depending on displayFormat or locale
   * Defaults to day-month-year
   */
  formattedRender() {
    const monthDayYearFormat = (
      <>
        {monthRender()}
        {dayRender()}
        {yearRender()}
      </>
    );
    const dayMonthYearFormat = (
      <>
        {dayRender()}
        {monthRender()}
        {yearRender()}
      </>
    );
    if (
      DateInputUtil.computedDisplayFormat(
        props.displayFormat,
        props.intl.locale
      ) === "month-day-year"
    ) {
      return monthDayYearFormat;
    }
    return dayMonthYearFormat;
  }
  render() {
    const {
      disabled,
      displayFormat,
      dayAttributes,
      monthAttributes,
      yearAttributes,
      intl,
      isInvalid,
      isIncomplete,
      onBlur,
      onChange,
      onFocus,
      ref,
      required,
      name,
      value,
      ...customProps
    } = props;
    const { month, day, year } = state;
    const theme = context;
    const dateInputClassNames = classNames(
      cx(
        { disabled },
        "date-input",
        { "is-focused": state.isFocused },
        theme.className
      ),
      customProps.className
    );
    // Using the state of month, day, and year to create a formatted date value
    let dateValue = "";
    if (month.length > 0 || day.length > 0 || year.length > 0) {
      dateValue = `${year}-${month}-${day}`;
    }
    return (
      <div
        {...customProps}
        className={dateInputClassNames}
        ref={element => {
          dateInputContainer.current = element;
          if (ref) {
            ref(element);
          }
        }}
      >
        <input
          // Create a hidden input for storing the name and value attributes to use when submitting the form.
          // The data stored in the value attribute will be the visible date in the date input but formatted in YYYY-MM-DD format.
          type="hidden"
          name={name}
          value={dateValue}
        />
        {formattedRender()}
      </div>
    );
  }
}

export default injectIntl(DateInput);
