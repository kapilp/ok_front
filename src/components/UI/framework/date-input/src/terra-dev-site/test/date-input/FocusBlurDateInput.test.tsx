import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateInput from 'terra-date-input';
import classNames from 'classnames/bind';
import styles from './common/DateInput.test.module.scss';
const cx = classNames.bind(styles);
type DateInputOnBlurState = {
  blurTriggerCount?: number;
  focusTriggerCount?: number;
  value?: string;
};
class DateInputOnBlur extends React.Component<{}, DateInputOnBlurState> {
  blurCount: number;
  focusCount: number;
  handleOnChange: any;
  constructor(props) {
    super(props);
    state = { blurTriggerCount: 0, focusTriggerCount: 0, value: '' };
    handleBlur = handleBlur.bind(this);
    handleFocus = handleFocus.bind(this);
    blurCount = 0;
    focusCount = 0;
  }
  handleBlur() {
    blurCount += 1;
    setState({ blurTriggerCount: blurCount });
  }
  handleFocus() {
    focusCount += 1;
    setState({ focusTriggerCount: focusCount });
  }
  handleOnChangeFocus(event, dateString) {
    setState({ value: dateString });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <div id="date-input-value">
          <h3>
            onBlur Trigger Count:
            <span id="blur-count">{state.blurTriggerCount}</span>
            <br />
            <br />
            onFocus Trigger Count:
            <span id="focus-count">{state.focusTriggerCount}</span>
          </h3>
        </div>
        <DateInput id="dateInput" name="date-input" value={state.value} onChange={handleOnChange} onBlur={handleBlur} onFocus={handleFocus} />
      </div>
    );
  }
}
export default DateInputOnBlur;
