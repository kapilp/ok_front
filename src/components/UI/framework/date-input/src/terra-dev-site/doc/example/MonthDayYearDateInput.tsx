import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateInput from 'terra-date-input';
const Example = () => {
  const [value, setValue] = useState('');
  return (
    <>
      <DateInput name="date-input-value" value={value} onChange={(event, dateString) => setValue(dateString)} displayFormat="month-day-year" />
      <p>{`DateInput Value: ${value}`}</p>
    </>
  );
};
export default Example;
