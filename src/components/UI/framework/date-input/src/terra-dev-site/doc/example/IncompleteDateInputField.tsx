import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateInputField from 'terra-date-input/lib/DateInputField';
const Example = () => {
  const [value, setValue] = useState('');
  const [isIncomplete, setIsIncomplete] = useState(false);
  return (
    <>
      <DateInputField
        legend="Legend text"
        name="date-input-value"
        value={value}
        onChange={(event, dateString) => setValue(dateString)}
        required
        error="Error message"
        help="Help message"
        isIncomplete={isIncomplete}
      />
      <p>{`DateInputField Value: ${value}`}</p>
      <button type="button" onClick={() => setIsIncomplete(incomplete => !incomplete)}>
        Toggle isIncomplete
      </button>
    </>
  );
};
export default Example;
