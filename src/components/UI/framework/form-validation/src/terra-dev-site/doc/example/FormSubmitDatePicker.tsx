/* eslint-disable class-methods-use-this */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Form, Field } from 'react-final-form';
import DatePicker from 'terra-date-picker';
import TerraField from 'terra-form-field';
import Button from 'terra-button';
import Spacer from 'terra-spacer';
const validateDate = value => {
  if (!value) {
    return 'Required';
  }
  if (value.search(/[0-1][0-9]\/([0-2][0-9]|3[0-1])\/[0-9]{4}/i) <= -1) {
    return 'Date is Invalid';
  }
  return undefined;
};
type MainEntryState = {
  submittedValues?: any;
};
export default class MainEntry extends React.Component<{}, MainEntryState> {
  constructor(props) {
    super(props);
    state = {};
    submitForm = submitForm.bind(this);
  }
  submitForm(values) {
    setState({
      submittedValues: values,
    });
  }
  renderForm({ handleSubmit }) {
    return (
      <form noValidate onSubmit={handleSubmit}>
        <Field name="user_date" validate={validateDate}>
          {({ input, meta }) => (
            <TerraField label="Enter your planned start date" error={meta.error} isInvalid={meta.submitFailed && meta.error !== undefined} required>
              <DatePicker
                name="user_date"
                id="default"
                onChangeRaw={(event, date) => {
                  input.onChange(date);
                }}
              />
            </TerraField>
          )}
        </Field>
        <Button text="Submit" type={Button.Opts.Types.SUBMIT} />
      </form>
    );
  }
  render() {
    return (
      <Spacer marginBottom="small">
        <Form onSubmit={submitForm} render={renderForm} />
        {state.submittedValues && (
          <div>
            <p>Form Submitted Successfully With</p>
            <pre>{JSON.stringify(state.submittedValues, 0, 2)}</pre>
          </div>
        )}
      </Spacer>
    );
  }
}
