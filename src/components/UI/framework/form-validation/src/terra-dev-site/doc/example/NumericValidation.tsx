/* eslint-disable class-methods-use-this */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Form, Field } from 'react-final-form';
import InputField from 'terra-form-input/lib/InputField';
import Button from 'terra-button';
import Spacer from 'terra-spacer';
import FormValidationUtil from '../../../FormValidationUtil';
const validateNumber = value => {
  if (!FormValidationUtil.isOverMinValue(value, 10)) {
    return 'Value should not be less than 10.';
  }
  if (!FormValidationUtil.isUnderMaxValue(value, 100)) {
    return 'Value should not be over 100.';
  }
  if (!FormValidationUtil.isPrecise(value, 3)) {
    return 'Value has more than 3 decimal points';
  }
  return undefined;
};
type ExampleState = {
  submittedValues?: any;
};
export default class Example extends React.Component<{}, ExampleState> {
  constructor(props) {
    super(props);
    state = {};
    submitForm = submitForm.bind(this);
  }
  submitForm(values) {
    setState({
      submittedValues: values,
    });
  }
  renderForm({ handleSubmit }) {
    return (
      <form noValidate onSubmit={handleSubmit}>
        <Field name="numExample" validate={validateNumber}>
          {({ input, meta }) => (
            <InputField
              inputId="numExample"
              label="Numeric Example"
              error={meta.error}
              isInvalid={meta.error !== undefined}
              inputAttrs={{
                type: 'number',
                ...input,
              }}
              onChange={e => {
                input.onChange(e.target.value);
              }}
              value={input.value}
              required
            />
          )}
        </Field>
        <Button text="Submit" type={Button.Opts.Types.SUBMIT} />
      </form>
    );
  }
  render() {
    return (
      <Spacer marginBottom="small">
        <Form
          onSubmit={submitForm}
          render={renderForm}
          initialValues={{ numExample: '' }}
          validate={values => {
            const errors = {};
            if (!values.numExample) {
              errors.numExample = 'Required';
            }
            return errors;
          }}
        />
        {state.submittedValues && (
          <div>
            <p>Form Submitted Successfully With</p>
            <pre>{JSON.stringify(state.submittedValues, 0, 2)}</pre>
          </div>
        )}
      </Spacer>
    );
  }
}
