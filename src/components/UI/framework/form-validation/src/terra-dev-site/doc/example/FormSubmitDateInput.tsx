/* eslint-disable class-methods-use-this */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Form, Field } from 'react-final-form';
import DateInputField from 'terra-date-input/lib/DateInputField';
import Button from 'terra-button';
import Spacer from 'terra-spacer';
/**
 * Ensures the passed in value is an accepted date value
 * @param {string} value The date to validate
 */
const isAcceptedDate = value => {
  if (value && value.length === 10) {
    const splitValue = value.split('-');
    const formattedDay = Number(splitValue[2]);
    const formattedMonth = Number(splitValue[1]) - 1; // Account for 0-indexed month
    const formattedYear = Number(splitValue[0]);
    const date = new Date(formattedYear, formattedMonth, formattedDay);
    const yearMatches = date.getUTCFullYear() === formattedYear;
    const monthMatches = date.getUTCMonth() === formattedMonth;
    const dayMatches = date.getUTCDate() === formattedDay;
    return yearMatches && monthMatches && dayMatches;
  }
  return false;
};
const validateDate = value => {
  if (!value) {
    return 'Required';
  }
  if (!isAcceptedDate(value)) {
    return 'Date is Invalid';
  }
  const splitValue = value.split('-');
  if (splitValue[0] < 2021) {
    return 'Year must be after the year 2020';
  }
  return undefined;
};
type MainEntryState = {
  submittedValues?: any;
};
export default class MainEntry extends React.Component<{}, MainEntryState> {
  constructor(props) {
    super(props);
    state = {};
    submitForm = submitForm.bind(this);
  }
  submitForm(values) {
    setState({
      submittedValues: values,
    });
  }
  renderForm({ handleSubmit }) {
    return (
      <form noValidate onSubmit={handleSubmit}>
        <Field name="user_date" validate={validateDate}>
          {({ input, meta }) => (
            <DateInputField
              name="user_date"
              legend="Enter your birthday"
              value={input.value}
              onChange={(event, date) => {
                input.onChange(date);
              }}
              error={meta.error}
              isInvalid={meta.submitFailed && meta.error !== undefined}
              required
            />
          )}
        </Field>
        <Button text="Submit" type={Button.Opts.Types.SUBMIT} />
      </form>
    );
  }
  render() {
    return (
      <Spacer marginBottom="small">
        <Form onSubmit={submitForm} render={renderForm} />
        {state.submittedValues && (
          <div>
            <p>Form Submitted Successfully With</p>
            <pre>{JSON.stringify(state.submittedValues, 0, 2)}</pre>
          </div>
        )}
      </Spacer>
    );
  }
}
