/* eslint-disable class-methods-use-this */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Form, Field } from 'react-final-form';
import InputField from 'terra-form-input/lib/InputField';
import Button from 'terra-button';
import Spacer from 'terra-spacer';
const validateUniqueUser = async name => {
  const response = new Promise(resolve => {
    if (!name) {
      return resolve('Required');
    }
    if (name !== 'TerraUser') {
      return resolve('');
    }
    return resolve('Name is Unavailable');
  });
  await response;
  return response;
};
type MainEntryState = {
  submittedValues?: any;
};
export default class MainEntry extends React.Component<{}, MainEntryState> {
  constructor(props) {
    super(props);
    state = {};
    submitForm = submitForm.bind(this);
  }
  submitForm(values) {
    setState({
      submittedValues: values,
    });
  }
  renderForm({ handleSubmit }) {
    return (
      <form noValidate onSubmit={handleSubmit}>
        <Field name="user_name" validate={validateUniqueUser}>
          {({ input, meta }) => (
            <InputField
              inputId="user-name-async"
              label="User Name"
              error={meta.error}
              help="TerraUser is unavailable. Use this name to test async"
              isInvalid={meta.submitFailed && meta.error !== undefined}
              onChange={e => {
                input.onChange(e.target.value);
              }}
              inputAttrs={{ ...input }}
              value={input.value}
              required
            />
          )}
        </Field>
        <Button text="Submit" type={Button.Opts.Types.SUBMIT} />
      </form>
    );
  }
  render() {
    return (
      <Spacer marginBottom="small">
        <Form onSubmit={submitForm} render={renderForm} validateOnBlur />
        {state.submittedValues && (
          <div>
            <p>Form Submitted Successfully With</p>
            <pre>{JSON.stringify(state.submittedValues, 0, 2)}</pre>
          </div>
        )}
      </Spacer>
    );
  }
}
