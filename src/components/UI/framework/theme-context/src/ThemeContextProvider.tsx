import {  JSX, Component, mergeProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ThemeContext } from './ThemeContext';
type ThemeObject = {
  name: string;
  className: string;
};
interface Properties {
  /**
   * The components to be rendered within the context of the ThemeContextProvider. Components rendered here are able to interact with ThemeContextProvider through the ThemeContext.
   */
  children: JSX.Element;
  /**
   * An object containing the name and className of the selected theme.
   */
  theme?: ThemeObject;
}
const defaultProps = {
  theme: {},
};
export const ThemeContextProvider: Component = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [contextState, setContextState] = createStore<ThemeObject>(props.theme ?? {});
  return <ThemeContext.Provider value={contextState}>{props.children}</ThemeContext.Provider>;
};
