import {  createContext,   } from 'solid-js';
import { createStore } from 'solid-js/store';

/**
 * The current application theme className.
 * The default theme is indicated as undefined.
 */
const [defaultState, mergePropstate] = createStore({className: ''})
export const ThemeContext = createContext(defaultState);
