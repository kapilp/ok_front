import { JSX, useContext } from 'solid-js';
import classNames from 'classnames/bind';
import styles from './ThemedComponent.module.scss';
import { ThemeContext } from '../../../ThemeContext';

const cx = classNames.bind(styles);
export const ThemedComponent = () => {
  const theme = useContext(ThemeContext);

  return (
    <div className={cx('themed', theme.className)}>
      <h1>{`Theme Name: ${theme.name}`}</h1>
      <div className={cx('themed-block')}>Sample Text </div>
    </div>
  );
};
