import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { ThemedComponent } from './ThemedComponent';
import { ThemeContextProvider } from '../../../ThemeContextProvider';

export const ThemeContextProviderExample = () => (
  <ThemeContextProvider theme={{ name: 'test-theme', className: 'test-theme' }}>
    <ThemedComponent />
  </ThemeContextProvider>
);
