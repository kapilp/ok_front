import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import ContentContainer from "terra-content-container";
import ResponsiveElement from "terra-responsive-element";
import TabPane from "./TabPane";
import CollapsibleTabs from "./_CollapsibleTabs";
import CollapsedTabs from "./_CollapsedTabs";
import TabUtils from "./TabUtils";
import styles from "./Tabs.module.scss";
const cx = classNamesBind.bind(styles);
/**
NOTE: This is being commented out until discussions have been resolved around if modular tabs should be removed.
const variants = {
  MODULAR_CENTERED: 'modular-centered',
  MODULAR_LEFT_ALIGNED: 'modular-left-aligned',
  STRUCTURAL: 'structural',
};
*/
interface Properties {
  /**
     * Tabs style. One of: Tabs.Opts.Variants.MODULAR_CENTERED, Tabs.Opts.Variants.MODULAR_LEFT_ALIGNED, or Tabs.Opts.Variants.STRUCTURAL.
     * NOTE: This is being commented out until discussions have been resolved around if we want modular tabs.
    variant: PropTypes.oneOf([variants.MODULAR_CENTERED, variants.MODULAR_LEFT_ALIGNED, variants.STRUCTURAL]),
    */
  /**
   * Indicates if tabs should fill the width available in the tab bar.
   */
  tabFill: boolean,
  /**
   * Indicates if the pane content should fill to the height of the parent container.
   */
  fill: boolean,
  /**
   * Callback function when selection has changed.
   * Parameters: 1. Event 2. Selected pane's key
   */
  onChange: PropTypes.func,
  /**
   * Tabs.Pane components to be displayed.
   */
  children: JSX.Element.isRequired,
  /**
   * Key of the pane that should be active. Use this prop along with onChange to create controlled tabs.
   */
  activeKey: string,
  /**
   * Key of the pane that should be open initially.
   */
  defaultActiveKey: string,
  /**
   * The viewport the element will be responsive to. One of `window`, `parent` or `none`.
   * Note: If the responsive viewport is set to `none`, then tabs never _completely_
   * collapse into a menu.
   */
  responsiveTo: PropTypes.oneOf(["window", "parent", "none"])
};
const defaultProps = {
  tabFill: false,
  fill: false,
  responsiveTo: "parent"
};
interface ITabsProps extends JSX.HTMLAttributes<Element> {
  tabFill?: any;
  fill?: any;
  onChange?: any;
  activeKey?: any;
  defaultActiveKey?: any;
  responsiveTo?: any;
  customProps?: any;
}
type TabsState = {
  activeKey?: any,
  isLabelTruncated?: boolean,
  showCollapsedTabs?: boolean
};
class Tabs extends React.Component<ITabsProps, TabsState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  context: any;
  constructor(props) {
    super(props);
    getInitialState = getInitialState.bind(this);
    getActiveTabIndex = getActiveTabIndex.bind(this);
    handleOnChange = handleOnChange.bind(this);
    handleTruncationChange = handleTruncationChange.bind(this);
    wrapPaneOnClick = wrapPaneOnClick.bind(this);
    setTabs = setTabs.bind(this);
    state = {
      activeKey: getInitialState(),
      isLabelTruncated: false,
      showCollapsedTabs: false
    };
  }
  getInitialState() {
    if (props.activeKey) {
      return null;
    }
    return TabUtils.initialSelectedTabKey(
      props.children,
      props.defaultActiveKey
    );
  }
  setTabs(event) {
    const showCollapsedTabs = event === "tiny";
    if (state.showCollapsedTabs !== showCollapsedTabs) {
      setState({ showCollapsedTabs });
    }
  }
  getActiveTabIndex() {
    let activeIndex = -1;
    React.Children.forEach(props.children, (child, index) => {
      if (
        child.key === state.activeKey ||
        child.key === props.activeKey
      ) {
        activeIndex = index;
      }
    });
    return activeIndex;
  }
  handleOnChange(event, selectedPane) {
    if (!selectedPane.props.isDisabled) {
      if (props.onChange) {
        props.onChange(event, selectedPane.key);
      }
      if (
        !props.activeKey &&
        TabUtils.shouldHandleSelection(state.activeKey, selectedPane.key)
      ) {
        setState({ activeKey: selectedPane.key });
      }
    }
  }
  handleTruncationChange(isLabelTruncated) {
    if (state.isLabelTruncated !== isLabelTruncated) {
      setState({ isLabelTruncated });
    }
  }
  wrapPaneOnClick(pane) {
    return event => {
      handleOnChange(event, pane);
      if (pane.props.onClick) {
        pane.props.onClick(event);
      }
    };
  }
  render() {
    const {
      tabFill,
      fill,
      onChange,
      children,
      activeKey,
      defaultActiveKey,
      responsiveTo,
      ...customProps
    } = props;
    const theme = context;
    // NOTE: Hardcoding variant to structural until discussions have resolved around if we want modular tabs.
    const variant = "structural";
    const tabsClassNames = classNames(
      cx("tabs-container", { "tab-fill": tabFill }, variant, theme.className),
      customProps.className
    );
    let content = null;
    let isIconOnly = false;
    const clonedPanes = [];
    React.Children.forEach(children, child => {
      let isActive = false;
      if (child.key === state.activeKey || child.key === activeKey) {
        isActive = true;
        content = child.props.children;
      }
      if (child.props.isIconOnly) {
        isIconOnly = true;
      }
      clonedPanes.push(
        React.cloneElement(child, {
          className: cx({ "is-active": isActive }, child.props.className),
          onClick: wrapPaneOnClick(child),
          isActive
        })
      );
    });
    content = React.Children.map(content, contentItem =>
      React.cloneElement(contentItem, {
        isLabelHidden: isIconOnly || state.isLabelTruncated
      })
    );
    const tabContent = (() => {
      const collapsibleTabs = (
        <CollapsibleTabs
          activeKey={activeKey || state.activeKey}
          activeIndex={getActiveTabIndex()}
          onChange={handleOnChange}
          onTruncationChange={handleTruncationChange}
          variant={variant}
        >
          {clonedPanes}
        </CollapsibleTabs>
      );
      if (responsiveTo === "parent" || responsiveTo === "window") {
        const collapsedTabs = (
          <CollapsedTabs
            activeKey={activeKey || state.activeKey}
            onTruncationChange={handleTruncationChange}
          >
            {clonedPanes}
          </CollapsedTabs>
        );
        return (
          <ResponsiveElement
            onChange={setTabs}
            responsiveTo={responsiveTo}
          >
            {state.showCollapsedTabs ? collapsedTabs : collapsibleTabs}
          </ResponsiveElement>
        );
      }
      return collapsibleTabs;
    })();
    return (
      <ContentContainer
        {...customProps}
        className={tabsClassNames}
        fill={fill}
        header={tabContent}
      >
        <div
          role="tabpanel"
          className={cx("pane-content", { "fill-parent": fill })}
        >
          {content}
        </div>
      </ContentContainer>
    );
  }
}

Tabs.Pane = TabPane;
Tabs.Utils = TabUtils;

/**
Note: This is being commented out until discussions have been resolved around if we want modular tabs.
Tabs.Opts = {
  Variants: variants,
};
*/
export default Tabs;
