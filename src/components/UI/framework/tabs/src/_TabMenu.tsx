import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import Menu from 'terra-menu';
import IconCaretDown from 'terra-icon/lib/icon/IconCaretDown';
import * as KeyCode from 'keycode-js';
import { FormattedMessage } from 'react-intl';
import styles from './Tabs.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Key of the current active tab.
   */
  activeKey: string;
  /**
   * Tabs that should be displayed collapsed as selectable menu items.
   */
  children: JSX.Element;
  /**
   * Ref callback for menu toggle.
   */
  ref: PropTypes.func;
}
interface ITabMenuProps extends JSX.HTMLAttributes<Element> {
  activeKey?: any;
  ref?: any;
}
type TabMenuState = {
  isOpen?: boolean;
};
class TabMenu extends React.Component<ITabMenuProps, TabMenuState> {
  theme = useContext(ThemeContext);
  context: any;
  targetRef: any;
  constructor(props) {
    super(props);
    handleOnRequestClose = handleOnRequestClose.bind(this);
    handleOnClick = handleOnClick.bind(this);
    handleOnKeyDown = handleOnKeyDown.bind(this);
    getTargetRef = getTargetRef.bind(this);
    setTargetRef = setTargetRef.bind(this);
    wrapOnClick = wrapOnClick.bind(this);
    state = {
      isOpen: false,
    };
  }
  getTargetRef() {
    return targetRef;
  }
  setTargetRef(node) {
    targetRef = node;
    if (props.ref) {
      props.ref(node);
    }
  }
  handleOnRequestClose() {
    setState({ isOpen: false });
  }
  handleOnClick() {
    setState({ isOpen: true });
  }
  handleOnKeyDown(event) {
    if (event.keyCode === KeyCode.KEY_RETURN) {
      setState({ isOpen: true });
    }
  }
  wrapOnClick(child) {
    return event => {
      event.stopPropagation();
      if (child.props.onClick) {
        child.props.onClick(event);
      }
      setState({ isOpen: false });
    };
  }
  render() {
    const menuItems = [];
    let menuActive = false;
    let toggleText;
    React.Children.forEach(props.children, child => {
      const { label, customDisplay, icon, isIconOnly, ...otherProps } = child.props;
      let isSelected = false;
      if (props.activeKey === child.key) {
        toggleText = label;
        isSelected = true;
        menuActive = true;
      }
      menuItems.push(<Menu.Item {...otherProps} text={label} onClick={wrapOnClick(child)} isSelected={isSelected} isSelectable key={child.key} />);
    });
    const theme = context;
    return (
      <div
        role="button"
        tabIndex="0"
        ref={setTargetRef}
        onClick={handleOnClick}
        onKeyDown={handleOnKeyDown}
        className={cx('tab-menu', { 'is-active': menuActive }, theme.className)}
        data-terra-tabs-menu
      >
        <FormattedMessage id="Terra.tabs.more">{menuToggleText => <span>{toggleText || menuToggleText}</span>}</FormattedMessage>
        <IconCaretDown />
        <Menu onRequestClose={handleOnRequestClose} targetRef={getTargetRef} isOpen={state.isOpen}>
          {menuItems}
        </Menu>
      </div>
    );
  }
}

export default TabMenu;
