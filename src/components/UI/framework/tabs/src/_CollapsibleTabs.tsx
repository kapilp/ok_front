import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import ResizeObserver from "resize-observer-polyfill";
import * as KeyCode from "keycode-js";
import Menu from "./_TabMenu";
import styles from "./Tabs.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Key of the current active tab.
   */
  activeKey: string.isRequired,
  /**
   * Index of the current active tab.
   */
  activeIndex: PropTypes.number.isRequired,
  /**
   * Tabs to be displayed in the collapsible tab bar.
   */
  children: JSX.Element.isRequired,
  /**
   * Tabs style. One of: "modular-centered", "modular-left-aligned", or "structural".
   */
  variant: PropTypes.oneOf([
    "modular-centered",
    "modular-left-aligned",
    "structural"
  ]).isRequired,
  /**
   * Callback function when selection has changed.
   * Parameters: 1. Event 2. Selected pane element
   */
  onChange: PropTypes.func.isRequired,
  /**
   * Callback function when label truncation state has changed.
   * Parameters: 1. Bool indicating if any of the tab labels have been truncated.
   */
  onTruncationChange: PropTypes.func
};
interface ICollapsibleTabsProps extends JSX.HTMLAttributes<Element> {
  activeIndex?: any;
  onTruncationChange?: any;
}
export const CollapsibleTabs: Component = (props:ICollapsibleTabsProps) => {
  const theme = useContext(ThemeContext);
  animationFrameID: null;
  container: any;
  contentWidth: any;
  context: any;
  forceUpdate: any;
  hiddenStartIndex: number;
  isCalculating: boolean;
  menuHidden: boolean;
  menuRef: any;
  resizeObserver: any;
  selectionBar: HTMLDivElement;
  constructor(props) {
    super(props);
    setContainer = setContainer.bind(this);
    setMenuRef = setMenuRef.bind(this);
    resetCache = resetCache.bind(this);
    handleResize = handleResize.bind(this);
    handleSelectionAnimation = handleSelectionAnimation.bind(this);
    handleOnKeyDown = handleOnKeyDown.bind(this);
    handleMenuOnKeyDown = handleMenuOnKeyDown.bind(this);
    handleFocusLeft = handleFocusLeft.bind(this);
    handleFocusRight = handleFocusRight.bind(this);
    resetCache();
  }
  componentDidMount() {
    resizeObserver = new ResizeObserver(entries => {
      contentWidth = entries[0].contentRect.width;
      if (!isCalculating) {
        animationFrameID = window.requestAnimationFrame(() => {
          // Resetting the cache so that all elements will be rendered face-up for width calculations
          resetCache();
          forceUpdate();
        });
      }
    });
    resizeObserver.observe(container);
    handleResize(contentWidth);
    handleSelectionAnimation();
  }
  componentDidUpdate(prevProps) {
    if (isCalculating) {
      isCalculating = false;
      handleResize(contentWidth);
    } else if (
      React.Children.count(props.children) !==
      React.Children.count(prevProps.children)
    ) {
      resetCache();
      forceUpdate();
    } else {
      handleSelectionAnimation();
    }
  }
  componentWillUnmount() {
    window.cancelAnimationFrame(animationFrameID);
    resizeObserver.disconnect(container);
    container = null;
  }
  setContainer(node) {
    if (node === null) {
      return;
    } // Ref callbacks happen on mount and unmount, element will be null on unmount
    container = node;
  }
  setMenuRef(node) {
    if (node === null) {
      return;
    }
    menuRef = node;
  }
  resetCache() {
    animationFrameID = null;
    hiddenStartIndex = -1;
    isCalculating = true;
    menuHidden = false;
  }
  handleResize(width) {
    const menuMarginLeft = parseInt(
      window
        .getComputedStyle(menuRef, null)
        .getPropertyValue("margin-left"),
      10
    );
    const menuMarginRight = parseInt(
      window
        .getComputedStyle(menuRef, null)
        .getPropertyValue("margin-right"),
      10
    );
    const menuToggleWidth =
      menuRef.getBoundingClientRect().width +
      menuMarginLeft +
      menuMarginRight;
    const availableWidth = width - menuToggleWidth;
    // Calculate hide index
    const childrenCount = React.Children.count(props.children);
    let newHideIndex = childrenCount;
    let calcMinWidth = 0;
    let isMenuHidden = true;
    for (let i = 0; i < props.children.length; i += 1) {
      const tab = container.children[i];
      const tabMarginLeft = parseFloat(
        window
          .getComputedStyle(menuRef, null)
          .getPropertyValue("margin-left")
      );
      const tabMarginRight = parseFloat(
        window
          .getComputedStyle(menuRef, null)
          .getPropertyValue("margin-right")
      );
      const minWidth = parseFloat(
        window.getComputedStyle(tab, null).getPropertyValue("min-width")
      );
      calcMinWidth += minWidth + tabMarginLeft + tabMarginRight;
      if (
        calcMinWidth > availableWidth &&
        !(i === childrenCount - 1 && calcMinWidth <= width)
      ) {
        newHideIndex = i;
        isMenuHidden = false;
        break;
      }
    }
    // Calculate if label will be truncated
    let isLabelTruncated = false;
    let calcWidth = 0;
    for (let i = 0; i < newHideIndex; i += 1) {
      const tab = container.children[i];
      calcWidth += tab.getBoundingClientRect().width;
      if (
        (isMenuHidden && calcWidth > width) ||
        (!isMenuHidden && calcWidth > availableWidth)
      ) {
        isLabelTruncated = true;
        break;
      }
    }
    props.onTruncationChange(isLabelTruncated);
    if (
      menuHidden !== isMenuHidden ||
      hiddenStartIndex !== newHideIndex
    ) {
      menuHidden = isMenuHidden;
      hiddenStartIndex = newHideIndex;
      forceUpdate();
    }
  }
  handleSelectionAnimation() {
    if (
      selectionBar &&
      window
        .getComputedStyle(selectionBar, null)
        .getPropertyValue("transition-property")
        .includes("transform")
    ) {
      const activeIndex =
        props.activeIndex > hiddenStartIndex
          ? hiddenStartIndex
          : props.activeIndex;
      const selectedTab = container.children[activeIndex];
      if (selectedTab) {
        const isRTL =
          document.getElementsByTagName("html")[0].getAttribute("dir") ===
          "rtl";
        const tabRect = selectedTab.getBoundingClientRect();
        const barWidth = tabRect.width;
        let barLeft =
          tabRect.left - container.getBoundingClientRect().left;
        if (isRTL) {
          barLeft =
            tabRect.right - container.getBoundingClientRect().right;
        }
        selectionBar.style.width = `${barWidth}px`;
        selectionBar.style.transform = `translate3d(${barLeft}px,0,0)`;
      }
    }
  }
  handleOnKeyDown(event) {
    // If there are less than 2 children we don't need to worry about keyboard navigation
    if (React.Children.count(props.children) < 2) {
      return;
    }
    // We don't want menu keydown events to propagate and conflict when the tabs keydown events
    // Instead of stopping menu key event propagation, we whitelist event.targets so we do tab focus mgmt only on tab based event targets
    const tabList = event.target.getAttribute("role") === "tablist";
    const tabMoreBtn =
      event.target.getAttribute("data-terra-tabs-menu") === "true";
    if (tabList || tabMoreBtn) {
      const isRTL =
        document.getElementsByTagName("html")[0].getAttribute("dir") === "rtl";
      const visibleChildren = container.children;
      if (event.keyCode === KeyCode.KEY_LEFT) {
        if (isRTL) {
          handleFocusRight(visibleChildren, event);
        } else {
          handleFocusLeft(visibleChildren, event);
        }
      } else if (event.keyCode === KeyCode.KEY_RIGHT) {
        if (isRTL) {
          handleFocusLeft(visibleChildren, event);
        } else {
          handleFocusRight(visibleChildren, event);
        }
      }
    }
  }
  handleFocusRight(visibleChildren, event) {
    if (props.activeIndex >= hiddenStartIndex) {
      return;
    }
    for (
      let i = props.activeIndex + 1;
      i < visibleChildren.length;
      i += 1
    ) {
      if (!props.children[i].props.isDisabled) {
        if (visibleChildren[i] === menuRef) {
          menuRef.focus();
          break;
        } else {
          props.onChange(event, props.children[i]);
          break;
        }
      }
    }
  }
  handleFocusLeft(visibleChildren, event) {
    let startIndex = props.activeIndex - 1;
    if (
      startIndex >= hiddenStartIndex ||
      document.activeElement === menuRef
    ) {
      startIndex = hiddenStartIndex - 1;
    }
    for (let i = startIndex; i >= 0; i -= 1) {
      if (!props.children[i].props.isDisabled) {
        if (document.activeElement === menuRef) {
          container.focus();
        }
        props.onChange(event, props.children[i]);
        break;
      }
    }
  }
  /* eslint class-methods-use-this: ["error", { "exceptMethods": ["handleMenuOnKeyDown"] }] */
  handleMenuOnKeyDown(event) {
    // Prevent menu key events from propagating up to CollabsibleTabs handleOnKeyDown listener
    // This prevents left / right arrow key usage in menu from shifting to different tabs
    event.stopPropagation();
  }
  render() {
    const visibleChildren = [];
    const hiddenChildren = [];
    React.Children.forEach(props.children, (child, index) => {
      if (index < hiddenStartIndex || hiddenStartIndex < 0) {
        visibleChildren.push(child);
      } else {
        hiddenChildren.push(child);
      }
    });
    const theme = context;
    const menu = menuHidden ? null : (
      <Menu
        onKeyDown={handleMenuOnKeyDown}
        ref={setMenuRef}
        activeKey={props.activeKey}
      >
        {hiddenChildren}
      </Menu>
    );
    const selectionBar =
      props.variant === "modular-centered" ||
      props.variant === "modular-left-aligned" ? (
        <div
          className={cx("selection-bar")}
          ref={node => {
            if (node) {
              selectionBar = node;
            }
          }}
        />
      ) : null;
    return (
      <div>
        <div
          className={cx(
            "collapsible-tabs-container",
            { "is-calculating": isCalculating },
            theme.className
          )}
          ref={setContainer}
          tabIndex="0"
          onKeyDown={handleOnKeyDown}
          role="tablist"
        >
          {visibleChildren}
          {menu}
        </div>
        {selectionBar}
      </div>
    );
  }
}

export default CollapsibleTabs;
