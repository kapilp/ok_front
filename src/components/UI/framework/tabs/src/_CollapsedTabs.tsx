import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import Menu from './_TabMenu';
import styles from './Tabs.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Key of the current active tab.
   */
  activeKey: string;
  /**
   * Tabs to display in menu.
   */
  children: JSX.Element;
  /**
   * Callback function when label truncation state has changed.
   * Parameters: 1. Bool indicating if any of the tab labels have been truncated.
   */
  onTruncationChange: PropTypes.func;
}
const CollapsedTabs: Component = props => {
  props.onTruncationChange(false);
  const theme = React.useContext(ThemeContext);
  return (
    <div className={cx('collapsed-tabs-container', theme.className)}>
      <Menu activeKey={props.activeKey}>{props.children}</Menu>
    </div>
  );
};
export default CollapsedTabs;
