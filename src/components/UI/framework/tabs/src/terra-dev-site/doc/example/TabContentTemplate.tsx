import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './common/TabExample.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  isLabelHidden: boolean;
  label: string;
  children: JSX.Element;
}
const TabContentTemplate: Component = ({ isLabelHidden, label, children }) => (
  <div className={cx('tab-content-template')}>
    {isLabelHidden ? <h3>{label}</h3> : null}
    {children || (
      <div>
        Content for <i>{label}</i>
      </div>
    )}
  </div>
);
export default TabContentTemplate;
