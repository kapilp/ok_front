import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ResponsiveTabsVariantWrapper from './ResponsiveTabsVariantWrapper';
import TabsTemplate from './TabsTemplate';
import IconOnlyTabs from './IconOnlyTabs';
interface Properties {
  renderIconTabs: boolean;
}
const TabsColorVariants: Component = ({ ...props }) => {
  if (props.renderIconTabs) {
    return <IconOnlyTabs {...props} />;
  }
  return <TabsTemplate {...props} />;
};
export default ResponsiveTabsVariantWrapper(TabsColorVariants);
