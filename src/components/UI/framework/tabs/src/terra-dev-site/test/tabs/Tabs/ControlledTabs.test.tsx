import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Tabs from '../../../../Tabs';
import TabContent from './TabContentTemplate';
type ControlledTabsState = {
  activeKey?: string;
};
class ControlledTabs extends React.Component<{}, ControlledTabsState> {
  constructor(props) {
    super(props);
    state = { activeKey: 'Tab2' };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, selectedKey) {
    if (selectedKey !== state.activeKey) {
      setState({ activeKey: selectedKey });
    }
  }
  render() {
    const tab1 = (
      <Tabs.Pane label="Tab 1" key="Tab1" id="tab1">
        <TabContent label="Tab 1" id="tab1Content" />
      </Tabs.Pane>
    );
    const tab2 = (
      <Tabs.Pane label="Tab 2" key="Tab2" id="tab2">
        <TabContent label="Tab 2" id="tab2Content" />
      </Tabs.Pane>
    );
    const tab3 = (
      <Tabs.Pane label="Tab 3" key="Tab 3" id="tab3">
        <TabContent label="Tab 3" id="tab3Content" />
      </Tabs.Pane>
    );
    return (
      <div>
        <div id="current-selection">
          <p>
            Last Triggered Tab:
            {state.activeKey}
          </p>
        </div>
        <Tabs id="controlledTabs" onChange={handleSelection} activeKey={state.activeKey}>
          {tab1}
          {tab2}
          {tab3}
        </Tabs>
      </div>
    );
  }
}
export default ControlledTabs;
