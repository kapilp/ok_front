import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TabsTemplate from './TabsTemplate';
export default () => <TabsTemplate fill containerClassName="content-wrapper-400" />;
