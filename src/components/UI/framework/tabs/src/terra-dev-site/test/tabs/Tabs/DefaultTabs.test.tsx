import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TabsTemplate from './TabsTemplate';
// This test component has height and width of the container set to "auto".
export default () => <TabsTemplate />;
