import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Tabs from 'terra-tabs';
import TabContent from './TabContentTemplate';
type AdditionalTabsTestState = {
  tabKeys?: number[];
  map?: any;
};
class AdditionalTabsTest extends React.Component<{}, AdditionalTabsTestState> {
  constructor(props) {
    super(props);
    state = {
      tabKeys: [...Array(5).keys()],
    };
    addMoreTabPanes = addMoreTabPanes.bind(this);
  }
  createTabPanes() {
    const tabPanes = state.tabKeys.map(tabKey => (
      <Tabs.Pane label={`Tab ${tabKey}`} key={tabKey} id={tabKey}>
        <TabContent label={`Tab ${tabKey}`} id={`Tab ${tabKey}`} />
      </Tabs.Pane>
    ));
    return tabPanes;
  }
  addMoreTabPanes() {
    setState({
      tabKeys: [...Array(20).keys()],
    });
  }
  render() {
    // eslint-disable-next-line no-nested-ternary
    const tabsWrapperId = state.tabKeys.length === 5 ? 'tabsWrapper-5' : state.tabKeys.length === 20 ? 'tabsWrapper-20' : 'tabsWrapper';
    return (
      <div>
        <button type="button" onClick={addMoreTabPanes}>
          Add Tabs
        </button>
        <Tabs id={tabsWrapperId}>{createTabPanes()}</Tabs>
      </div>
    );
  }
}
export default AdditionalTabsTest;
