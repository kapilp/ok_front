import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TabsTemplate from './TabsTemplate';
type TabsResponsiveToParentState = {
  containerClassName?: string;
};
class TabsResponsiveToParent extends React.Component<{}, TabsResponsiveToParentState> {
  constructor(props) {
    super(props);
    handleOnButtonClick = handleOnButtonClick.bind(this);
    state = {
      containerClassName: 'content-wrapper',
    };
  }
  handleOnButtonClick() {
    setState({
      containerClassName: 'content-wrapper-600',
    });
  }
  render() {
    return (
      <div>
        <button type="button" onClick={handleOnButtonClick}>
          Set Container Width to 600 px
        </button>
        <TabsTemplate responsiveTo="parent" containerClassName={state.containerClassName} />
      </div>
    );
  }
}
export default TabsResponsiveToParent;
