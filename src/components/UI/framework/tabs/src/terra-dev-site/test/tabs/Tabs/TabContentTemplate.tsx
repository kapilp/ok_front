import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './TabsTemplate.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  isLabelHidden: boolean;
  label: string;
  id: string;
}
const TabContentTemplate: Component = ({ isLabelHidden, label, id }) => (
  <div className={cx('tab-content')} id={id}>
    {isLabelHidden ? <h3 className="truncationHeader">{label}</h3> : null}
    Content for <i>{label}</i>
  </div>
);
export default TabContentTemplate;
