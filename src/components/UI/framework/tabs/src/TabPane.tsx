import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Tabs.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Icon to be displayed on the tab.
   */
  icon: JSX.Element;
  /**
   * Text to be displayed on the tab.
   */
  label: string.isRequired;
  /**
   * A custom display for the tab. Component will fallback to label text when collapsed into the menu.
   */
  customDisplay: JSX.Element;
  /**
   * Content to be displayed when the tab is selected.
   */
  children: JSX.Element;
  /**
   * Indicates if the pane should be disabled.
   */
  isDisabled: boolean;
  /**
   * Indicates if the pane label should only display the icon. When tab collapses into menu the label text will be used.
   */
  isIconOnly: boolean;
  /**
   * If enabled, this prop will apply the `aria-selected` style to the pane.
   */
  isActive: boolean;
}
const defaultProps = {
  isDisabled: false,
  isIconOnly: false,
  isActive: false,
};
const TabPane: Component = ({ icon, label, customDisplay, children, isDisabled, isIconOnly, isActive, ...customProps }) => {
  const attributes = { ...customProps };
  const theme = React.useContext(ThemeContext);
  const paneClassNames = classNames(cx('tab', { 'is-disabled': isDisabled }, { 'is-icon-only': isIconOnly }, { 'is-text-only': !icon }, theme.className), attributes.className);
  if (isIconOnly) {
    attributes['aria-label'] = label;
  }
  attributes['aria-selected'] = isActive;
  return (
    <div {...attributes} role="tab" className={paneClassNames}>
      {customDisplay}
      {customDisplay ? null : icon}
      {customDisplay || isIconOnly ? null : <span className={cx('label')}>{label}</span>}
    </div>
  );
};
props = mergeProps({}, defaultProps, props);
export default TabPane;
