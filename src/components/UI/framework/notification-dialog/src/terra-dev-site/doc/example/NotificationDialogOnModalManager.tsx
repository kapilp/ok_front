import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';

import { NotificationDialogModalContainer } from './NotificationDialogModalContainer';
import styles from './example-styles.module.scss';
import { ModalManager } from '../../../../../modal-manager/src/ModalManager';
const cx = classNames.bind(styles);
export const NotificationDialogOnModalManager = () => (
  <div className={cx('example-wrapper')}>
    <ModalManager>
      <p> Notification Dialog has the highest z-index of 9001. Click the button to trigger Notification Dialog </p>
      <NotificationDialogModalContainer />
    </ModalManager>
  </div>
);
