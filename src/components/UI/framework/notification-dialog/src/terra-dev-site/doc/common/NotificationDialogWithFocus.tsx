import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { DisclosureDelegateType } from '../../../../../disclosure-manager/src/DisclosureManager';
import { NotificationDialog, NotificationDialogVariants } from '../../../NotificationDialog';
import { Button } from '../../../../../../core/button/src/Button';
import { Popup } from '../../../../../popup/src/Popup';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';

const clickConfirm = () => {
  alert('You clicked confirm'); // eslint-disable-line no-alert
};
interface INotificationDialogWithFocusProps extends JSX.HTMLAttributes<Element> {
  dismiss?: any;
  disclosureManager?: DisclosureDelegateType;
}

export const NotificationDialogWithFocus = (props: INotificationDialogWithFocusProps) => {
  const [state, setState] = createStore({
    isOpen: false,
  });

  const handleOpenModal = () => {
    setState({ isOpen: true });
  };
  const handleCloseModal = () => {
    setState({ isOpen: false });
  };
  const handlePopupButtonClick = () => {
    setState({ isOpen: true });
  };
  const handlePopupRequestClose = () => {
    setState({ isOpen: false });
  };

  return (
    <div>
      <NotificationDialog
        variant={NotificationDialogVariants.ALERT}
        isOpen={state.isOpen}
        onRequestClose={handleCloseModal}
        title="Make sure that the title relates directly to the choices."
        startMessage="The Main Instruction is text used to provide more detail or define terminology. Don’t repeat the title verbatim."
        acceptAction={{
          text: 'Confirm',
          onClick: clickConfirm,
        }}
        rejectAction={{
          text: 'Close',
          onClick: handleCloseModal,
        }}
        buttonOrder="acceptFirst"
        emphasizedAction="accept"
      />
      <Button text="Trigger NotificationDialog" onClick={handleOpenModal} id="trigger-notification-dialog" />
      <Button text="Dismiss" onClick={props.disclosureManager.dismiss} id="dismiss-modal" />
      <Popup isArrowDisplayed isOpen={state.open} onRequestClose={handlePopupRequestClose} targetRef={() => document.getElementById('popup-in-modal')}>
        <Placeholder title="Popup Content" />
      </Popup>
      <Button id="popup-in-modal" text="Popup In Modal" onClick={handlePopupButtonClick} />
    </div>
  );
};
// export default withDisclosureManager(NotificationDialogWithFocus);
