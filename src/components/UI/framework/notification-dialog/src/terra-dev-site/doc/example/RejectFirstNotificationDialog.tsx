import { JSX, createSignal } from 'solid-js';
import { NotificationDialog, NotificationDialogVariants } from '../../../NotificationDialog';
import { Button } from '../../../../../../core/button/src/Button';

const clickConfirm = () => {
  console.log('You clicked confirm'); // eslint-disable-line no-console
};
export const RejectFirstNotificationDialog = () => {
  const [isOpen, setIsOpen] = createSignal(false);
  const handleOpenModal = () => {
    setIsOpen(true);
  };
  const handleCloseModal = () => {
    setIsOpen(false);
  };
  return (
    <>
      <NotificationDialog
        variant={NotificationDialogVariants.ALERT}
        isOpen={isOpen()}
        title="Make sure that the title relates directly to the choices."
        startMessage="The Main Instruction is text used to provide more detail or define terminology. Don’t repeat the title verbatim."
        acceptAction={{
          text: 'Confirm',
          onClick: clickConfirm,
        }}
        rejectAction={{
          text: 'Close',
          onClick: handleCloseModal,
        }}
        buttonOrder="rejectFirst"
      />
      <Button text="Trigger NotificationDialog" onClick={handleOpenModal} />
    </>
  );
};
