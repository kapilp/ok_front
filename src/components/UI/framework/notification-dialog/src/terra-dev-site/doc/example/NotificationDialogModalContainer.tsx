import { JSX, createSignal } from 'solid-js';
import { NotificationDialog, NotificationDialogVariants } from '../../../NotificationDialog';
import { Button } from '../../../../../../core/button/src/Button';
import { NotificationDialogWithFocus } from './NotificationDialogWithFocus';
interface INotificationDialogModalContainerProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
  disclose?: any;
}
export const NotificationDialogModalContainer = (propss: INotificationDialogModalContainerProps) => {
  const disclose = () => {
    props.disclosureManager.disclose({
      preferredType: 'modal',
      content: {
        key: 'DemoContainer',
        component: <NotificationDialogWithFocus />,
      },
    });
  };

  return (
    <>
      <Button text="Disclose" onClick={disclose} />
    </>
  );
};
// export default withDisclosureManager(NotificationDialogModalContainer);
