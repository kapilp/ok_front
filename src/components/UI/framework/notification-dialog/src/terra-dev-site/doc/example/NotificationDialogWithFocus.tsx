import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { NotificationDialog, NotificationDialogVariants } from '../../../NotificationDialog';
import { Button } from '../../../../../../core/button/src/Button';
import { Popup } from '../../../../../popup/src/Popup';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { DisclosureDelegateType } from '../../../../../disclosure-manager/src/DisclosureManager';
const clickConfirm = () => {
  console.log('You clicked confirm'); // eslint-disable-line no-console
};
interface Properties {
  disclosureManager: DisclosureDelegateType;
}
export const NotificationDialogWithFocus = (props: Properties) => {
  const [isOpen, setIsOpen] = createSignal(false);
  const [isPopupOpen, setIsPopupOpen] = createSignal(false);
  const handleOpenModal = () => {
    setIsOpen(true);
  };
  const handleCloseModal = () => {
    setIsOpen(false);
  };
  const handlePopupButtonClick = () => {
    setIsPopupOpen(true);
  };
  const handlePopupRequestClose = () => {
    setIsPopupOpen(false);
  };
  return (
    <>
      <NotificationDialog
        variant={NotificationDialogVariants.ALERT}
        isOpen={isOpen()}
        title="Make sure that the title relates directly to the choices."
        startMessage="The Main Instruction is text used to provide more detail or define terminology. Don’t repeat the title verbatim."
        acceptAction={{
          text: 'Confirm',
          onClick: clickConfirm,
        }}
        rejectAction={{
          text: 'Close',
          onClick: handleCloseModal,
        }}
        buttonOrder="acceptFirst"
        emphasizedAction="accept"
      />
      <Button text="Trigger NotificationDialog" onClick={handleOpenModal} />
      <Button text="Dismiss" onClick={props.disclosureManager.dismiss} />
      <Popup isArrowDisplayed isOpen={isPopupOpen()} onRequestClose={handlePopupRequestClose} targetRef={() => document.getElementById('popup-in-modal')}>
        <Placeholder title="Popup Content" />
      </Popup>
      <Button id="popup-in-modal" text="Popup In Modal" onClick={handlePopupButtonClick} />
    </>
  );
};
// export default withDisclosureManager(NotificationDialogWithFocus);
