import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { NotificationDialogWithFocus } from './NotificationDialogWithFocus';
import { Button } from '../../../../../../core/button/src/Button';
interface INotificationDialogModalContainerProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
  disclose?: any;
}
export const NotificationDialogModalContainer = (props: {}) => {
  const disclose = () => {
    props.disclosureManager.disclose({
      preferredType: 'modal',
      content: {
        key: 'DemoContainer',
        component: <NotificationDialogWithFocus />,
      },
    });
  };
  return (
    <div>
      <Button text="Disclose" onClick={disclose} id="disclose-modal" />
    </div>
  );
};
// export default withDisclosureManager(NotificationDialogModalContainer);
