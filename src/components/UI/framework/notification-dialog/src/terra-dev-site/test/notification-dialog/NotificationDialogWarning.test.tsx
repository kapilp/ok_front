import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import NotificationDialogVariant from './NotificationDialogVariant.test';
import { NotificationDialogVariants } from '../../../NotificationDialog';
const NotificationDialogWarning = () => (
  <>
    <NotificationDialogVariant variant={NotificationDialogVariants.WARNING} />
  </>
);
export default NotificationDialogWarning;
