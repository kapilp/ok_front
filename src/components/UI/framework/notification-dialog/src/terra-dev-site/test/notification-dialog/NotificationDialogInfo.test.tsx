import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import NotificationDialogVariant from './NotificationDialogVariant.test';
import { NotificationDialogVariants } from '../../../NotificationDialog';
const NotificationDialogInfo = () => (
  <>
    <NotificationDialogVariant variant={NotificationDialogVariants.INFO} />
  </>
);
export default NotificationDialogInfo;
