import {  JSX, Component, onCleanup, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';

import * as KeyCode from 'keycode-js';

import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
// import { FormattedMessage } from "react-intl";
import styles from './NotificationDialog.module.scss';
import { Button, ButtonVariants } from '../../../core/button/src/Button';
import { FocusTrap } from '../../../FocusTrap';
import { AbstractModal } from '../../abstract-modal/src/AbstractModal';
import { render } from 'solid-js/web';
const cx = classNamesBind.bind(styles);
export enum NotificationDialogVariants {
  ALERT = 'alert',
  ERROR = 'error',
  WARNING = 'warning',
  INFO = 'info',
  SUCCESS = 'success',
  CUSTOM = 'custom',
}
interface INotificationDialogProps extends JSX.HTMLAttributes<Element> {
  /**
   * Header of the notification-dialog.
   */
  header?: string;
  /**
   * Title of the notification-dialog.
   */
  title?: string;
  /**
   * **Deprecated**, Message of the notification-dialog. Resolves to `startMessage`.
   */
  message?: string;
  /**
   * Message to be displayed at the start of the notification dialog body.
   */
  startMessage?: string;
  /**
   * Message to be displayed at the end of the notification dialog body.
   */
  endMessage?: string;
  /**
   *  Non-plain text content to be inserted after `startMessage` and/or before `endMessage`
   */
  content?: JSX.Element;
  /**
   * **Deprecated**, The Action of the primary button. Resolves to `acceptAction`
   */
  primaryAction?: {
    text: string;
    onClick: () => void;
  };
  /**
   * The Action of the accept button.
   */
  acceptAction?: {
    text: string;
    onClick: () => void;
  };
  /**
   * **Deprecated**, The Action of the secondary button. Resolves to `rejectAction`.
   */
  secondaryAction?: {
    text: string;
    onClick: () => void;
  };
  /**
   * The Action of the reject button.
   */
  rejectAction?: {
    text: string;
    onClick?: () => void;
  };
  /**
   * The variant of notification to be rendered.
   * Use one of `alert`, `error`, `warning`, `info`, `success`, `custom`.
   */
  variant?: variants;
  /**
   * The icon to be used for a notification of type custom. This will not be used for any other notification types.
   */
  customIcon?: JSX.Element;
  /**
   * Toggle to show notification-dialog or not.
   */
  isOpen: boolean;
  /**
   * Determines the order of notification action buttons.
   * Use one of `acceptFirst`, `rejectFirst`.
   */
  buttonOrder?: 'acceptFirst' | 'rejectFirst';
  /**
   * Determines whether acceptAction, rejectAction or neither is emphasizedAction
   * Use one of `none`, `accept` or `reject`.
   */
  emphasizedAction?: 'none' | 'accept' | 'reject';
}
const defaultProps = {
  title: null,
  startMessage: null,
  endMessage: null,
  content: null,
  variant: NotificationDialogVariants.CUSTOM,
  buttonOrder: 'acceptFirst',
  emphasizedAction: 'none',
};
const actionSection = (acceptAction, rejectAction, buttonOrder, emphasizedAction) => {
  let acceptButton = null;
  let rejectButton = null;
  if (!acceptAction && !rejectAction) {
    return null;
  }
  if (acceptAction) {
    acceptButton =
      emphasizedAction === 'accept' ? (
        <Button text={acceptAction.text} variant={ButtonVariants.EMPHASIS} onClick={acceptAction.onClick} />
      ) : (
        <Button text={acceptAction.text} onClick={acceptAction.onClick} />
      );
  }
  if (rejectAction) {
    rejectButton =
      emphasizedAction === 'reject' ? (
        <Button text={rejectAction.text} variant={ButtonVariants.EMPHASIS} onClick={rejectAction.onClick} />
      ) : (
        <Button text={rejectAction.text} onClick={rejectAction.onClick} />
      );
  }
  if (buttonOrder === 'rejectFirst') {
    return (
      <div className={cx('actions')}>
        {rejectButton}
        {acceptButton}
      </div>
    );
  }
  return (
    <div className={cx('actions')}>
      {acceptButton}
      {rejectButton}
    </div>
  );
};
const getIcon = (variant: NotificationDialogVariants, customIcon = null) => {
  switch (variant) {
    case NotificationDialogVariants.ALERT:
      return <span className={cx(['icon', 'alert'])} />;
    case NotificationDialogVariants.ERROR:
      return <span className={cx(['icon', 'error'])} />;
    case NotificationDialogVariants.WARNING:
      return <span className={cx(['icon', 'warning'])} />;
    case NotificationDialogVariants.INFO:
      return <span className={cx(['icon', 'info'])} />;
    case NotificationDialogVariants.SUCCESS:
      return <span className={cx(['icon', 'success'])} />;
    case NotificationDialogVariants.CUSTOM:
      return customIcon;
    default:
      return null;
  }
};

export const NotificationDialog = (props: INotificationDialogProps) => {
  props = mergeProps({}, defaultProps, props);

  let escapeKey = KeyCode.KEY_ESCAPE;
  //componentDidMount() {
  document.addEventListener('keydown', handleKeydown);
  //}
  onCleanup(() => {
    document.removeEventListener('keydown', handleKeydown);
  });
  function handleKeydown(e: KeyboardEvent) {
    const notificationDialog = document.querySelector('[data-terra-notification-dialog]');
    if (e.keyCode === escapeKey) {
      if (notificationDialog) {
        if (e.target === notificationDialog || notificationDialog.contains(e.target)) {
          e.stopImmediatePropagation();
        }
      }
    }
  }

  const [p, customProps] = splitProps(props, [
    'header',
    'title',
    'startMessage',
    'endMessage',
    'content',
    'acceptAction',
    'rejectAction',
    'variant',
    'customIcon',
    'isOpen',
    'buttonOrder',
    'emphasizedAction',
    'primaryAction',
    'secondaryAction',
    'message',
  ]);
  if (process.env.NODE_ENV !== 'production' && p.acceptAction === undefined && p.primaryAction === undefined && p.rejectAction === undefined && p.secondaryAction === undefined) {
    // eslint-disable-next-line no-console
    console.warn('At least one of `acceptAction`,`primaryAction`,`rejectAction`,`secondaryAction` props must be provided for Notification dialog');
  }
  const translationHack = (v: NotificationDialogVariants) => {
    switch (v) {
      case NotificationDialogVariants.ALERT:
        return 'Alert';
      case NotificationDialogVariants.ERROR:
        return 'Error';
      case NotificationDialogVariants.INFO:
        return 'Information';
      case NotificationDialogVariants.SUCCESS:
        return 'Success';
      case NotificationDialogVariants.WARNING:
        return 'Warning';
      case NotificationDialogVariants.CUSTOM:
        return '';
    }
  };
  const defaultHeader = <> {translationHack(p.variant!)} </>;

  const theme = useContext(ThemeContext);
  const notificationDialogClassNames = classNames(cx('notification-dialog', theme.className), customProps.className);
  /* eslint-disable jsx-a11y/no-noninteractive-tabindex */
  return (
    <>
      {props.isOpen && (
        <AbstractModal
          ariaLabel="Notification Dialog"
          aria-labelledby="notification-dialog-header"
          aria-describedby={p.title ? 'notification-dialog-title' : 'notification-dialog-header'}
          role="alertdialog"
          classNameModal={notificationDialogClassNames}
          isOpen={props.isOpen}
          onRequestClose={() => {}}
          closeOnEsc={false}
          closeOnOutsideClick={false}
          zIndex="9000"
          data-terra-notification-dialog
        >
          <FocusTrap
            focusTrapOptions={{
              returnFocusOnDeactivate: true,
              clickOutsideDeactivates: false,
              escapeDeactivates: false,
            }}
          >
            <div className={cx('notification-dialog-inner-wrapper')}>
              <div className={cx('notification-dialog-container')} tabIndex="0">
                <div id="notification-dialog-header" className={cx('header-body')}>
                  {p.header || defaultHeader}
                </div>
                <div className={cx('notification-dialog-body')}>
                  {p.variant && <div className={cx('icon-container')}>{getIcon(p.variant!, p.customIcon)}</div>}
                  <div className={cx('text-wrapper')}>
                    {p.title && (
                      <div id="notification-dialog-title" className={cx('title')}>
                        {p.title}
                      </div>
                    )}
                    {(p.startMessage || p.message) && <div className={cx('message')}>{p.startMessage || p.message}</div>}
                    {p.content && <div>{p.content}</div>}
                    {p.endMessage && <div className={cx('message')}>{p.endMessage}</div>}
                  </div>
                </div>
                <div className={cx('footer-body')}>{actionSection(p.acceptAction || p.primaryAction, p.rejectAction || p.secondaryAction, p.buttonOrder, p.emphasizedAction)}</div>
              </div>
            </div>
          </FocusTrap>
        </AbstractModal>
      )}
    </>
  );
  /* eslint-enable jsx-a11y/no-noninteractive-tabindex */
};
