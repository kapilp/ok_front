import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Route } from 'react-router-dom';
import styles from './NavigationLayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
type Page1ContentProps = {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
  };
};
const Page1Content: React.SFC<Page1ContentProps> = ({ layoutConfig }) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2>Page 1 Content</h2>
      {layoutConfig.toggleMenu && (
        <button type="button" onClick={layoutConfig.toggleMenu} className={cx('button')}>
          Toggle Menu
        </button>
      )}
      <Route path="/page1/item1" render={() => <h2>Item 1</h2>} />
      <Route path="/page1/item2" render={() => <h2>Item 2</h2>} />
    </div>
  </div>
);
export default Page1Content;
