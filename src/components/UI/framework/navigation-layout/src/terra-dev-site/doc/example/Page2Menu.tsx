import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { withRouter } from 'react-router-dom';
import RoutingStackDelegate from '../../../RoutingStackDelegate';
import styles from './NavigationLayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
type Page2MenuProps = {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
    togglePin?: (...args: any[]) => any;
    menuIsPinned?: boolean;
  };
  routingStackDelegate?: any;
};
const Page2Menu: React.SFC<Page2MenuProps> = ({ layoutConfig, routingStackDelegate }) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      {routingStackDelegate && routingStackDelegate.showParent && (
        <button type="button" onClick={routingStackDelegate.showParent} className={cx('button')}>
          Go Back
        </button>
      )}
      {layoutConfig.toggleMenu && (
        <button type="button" onClick={layoutConfig.toggleMenu} className={cx('button')}>
          Toggle Menu
        </button>
      )}
      {layoutConfig.togglePin && !layoutConfig.menuIsPinned && (
        <button type="button" onClick={layoutConfig.togglePin} className={cx('button')}>
          Pin
        </button>
      )}
      {layoutConfig.togglePin && layoutConfig.menuIsPinned && (
        <button type="button" onClick={layoutConfig.togglePin} className={cx('button')}>
          Unpin
        </button>
      )}
      <h2>Page 2 Menu</h2>
    </div>
  </div>
);
export default withRouter(Page2Menu);
