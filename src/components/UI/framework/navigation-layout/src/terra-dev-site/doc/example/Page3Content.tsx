import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './NavigationLayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
type Page3ContentProps = {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
  };
};
const Page3Content: React.SFC<Page3ContentProps> = ({ layoutConfig }) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2>Page 3 Content</h2>
      {layoutConfig.toggleMenu && (
        <button type="button" onClick={layoutConfig.toggleMenu} className={cx('button')}>
          Toggle Menu
        </button>
      )}
    </div>
  </div>
);
export default Page3Content;
