import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import { Link, withRouter } from "react-router-dom";
import styles from "./HeaderExample.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  layoutConfig: PropTypes.shape({})
};
const HeaderExample: Component = ({ layoutConfig }) => (
  <div className={cx("content-wrapper1")}>
    <div className={cx("content-wrapper2")}>
      <h2>Header</h2>
      {layoutConfig.toggleMenu && (
        <button
          type="button"
          onClick={layoutConfig.toggleMenu}
          className={cx("button")}
        >
          Toggle Menu
        </button>
      )}
      {layoutConfig.size !== "small" && layoutConfig.size !== "tiny" && (
        <Link to="/page1" className={cx("link")}>
          Page 1
        </Link>
      )}
      {layoutConfig.size !== "small" && layoutConfig.size !== "tiny" && (
        <Link to="/page2" className={cx("link")}>
          Page 2
        </Link>
      )}
      {layoutConfig.size !== "small" && layoutConfig.size !== "tiny" && (
        <Link to="/page3" className={cx("link")}>
          Page 3
        </Link>
      )}
    </div>
  </div>
);
/* eslint-enable react/prop-types */
export default withRouter(HeaderExample);
