import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { withRouter } from 'react-router-dom';
import Layout from 'terra-layout';
import NavigationLayoutContent from './NavigationLayoutContent';
import { navigationLayoutConfigPropType, supportedComponentBreakpoints } from './configurationPropTypes';
import { reduceRouteConfig, validateMatchExists } from './routingUtils';
const getBreakpointSize = queryWidth => {
  const width = queryWidth || window.innerWidth;
  const breakpoints = {
    tiny: 544,
    small: 768,
    medium: 992,
    large: 1216,
    huge: 1440,
  };
  const { small, medium, large, huge } = breakpoints;
  if (width >= huge) {
    return 'huge';
  }
  if (width >= large) {
    return 'large';
  }
  if (width >= medium) {
    return 'medium';
  }
  if (width >= small) {
    return 'small';
  }
  return 'tiny';
};
interface Properties {
  /**
   * The component to render within the NavigationLayout's `header` region. If provided, this component
   * must appropriately handle the NavigationLayout-supplied props: `routeConfig`, and `navigationLayoutSize`.
   */
  header: JSX.Element;
  /**
   * The component to render within the NavigationLayout's `menu` region. If provided, this component
   * must appropriately handle the NavigationLayout-supplied props: `routeConfig`, and `navigationLayoutSize`.
   */
  menu: JSX.Element;
  /**
   * The component to render within the NavigationLayout's `content` region. If provided, this component
   * must appropriately handle the NavigationLayout-supplied props: `routeConfig`, and `navigationLayoutSize`.
   */
  children: JSX.Element;
  /**
   * The String to display in the NavigationLayout's hover-target menu disclosure.
   */
  menuText: string;
  /**
   * The configuration Object that will be used to generate the specified regions of the NavigationLayout.
   * Note: The config prop is treated as an immutable object to prevent unnecessary processing and improve performance.
   * If the configuration is changed after the first render, a new configuration object instance must be provided.
   */
  config: navigationLayoutConfigPropType.isRequired;
  /**
   * The index path of the consuming application's routing structure. If provided, the NavigationLayout will
   * ensure Redirects are present where necessary.
   */
  indexPath: string;
  /**
   * The location as provided by the `withRouter()` HOC.
   */
  location: PropTypes.object.isRequired;
  /**
   * The match as provided by the `withRouter()` HOC.
   */
  match: PropTypes.object.isRequired;
  /**
   * The history as provided by the `withRouter()` HOC.
   */
  history: PropTypes.object.isRequired;
  /**
   * The staticContext as provided by the `withRouter()` HOC. This will only be provided when
   * within a StaticRouter.
   */
  staticContext: PropTypes.object;
}
interface INavigationLayoutProps extends JSX.HTMLAttributes<Element> {
  header?: any;
  menu?: any;
  menuText?: any;
  config?: any;
  indexPath?: any;
  location?: any;
  match?: any;
  history?: any;
  staticContext?: any;
  customProps?: any;
}
type NavigationLayoutState = {
  size?: string;
  processedRoutes?: {};
  prevPropsConfig?: any;
};
/**
 * The NavigationLayout utilizes the Terra Layout and a configuration object to generate a routing-based application layout.
 */
class NavigationLayout extends React.Component<INavigationLayoutProps, NavigationLayoutState> {
  static processRouteConfig(config) {
    const processedRoutes = {};
    supportedComponentBreakpoints.forEach(size => {
      const processedRoutesForSize = {};
      processedRoutesForSize.header = reduceRouteConfig(config.header, size);
      processedRoutesForSize.menu = reduceRouteConfig(config.menu, size);
      processedRoutesForSize.content = reduceRouteConfig(config.content, size);
      processedRoutes[size] = processedRoutesForSize;
    });
    return processedRoutes;
  }
  constructor(props) {
    super(props);
    updateSize = updateSize.bind(this);
    state = {
      size: getBreakpointSize(),
      processedRoutes: NavigationLayout.processRouteConfig(props.config),
      prevPropsConfig: props.config,
    };
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.config !== prevState.prevPropsConfig) {
      return {
        processedRoutes: NavigationLayout.processRouteConfig(nextProps.config),
      };
    }
    return null;
  }
  componentDidMount() {
    window.addEventListener('resize', updateSize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', updateSize);
  }
  updateSize() {
    const newSize = getBreakpointSize();
    if (state.size !== newSize) {
      setState({
        size: newSize,
      });
    }
  }
  decorateElement(element, routes) {
    if (!element) {
      return null;
    }
    const { size } = state;
    return React.cloneElement(element, {
      navigationLayoutRoutes: routes,
      navigationLayoutSize: size,
    });
  }
  render() {
    const { header, children, menu, menuText, config, indexPath, location, match, history, staticContext, ...customProps } = props;
    const { size, processedRoutes } = state;
    const headerComponent = header || <NavigationLayoutContent />;
    const contentComponent = children || <NavigationLayoutContent redirectPath={indexPath} />;
    let menuComponent = menu;
    // The routes for the menu are examined for evidence of a valid component for the current location.
    // If one is not found, we do not provide one to the Layout to ensure that the Layout renders appropriately (without a menu present).
    if (!menuComponent && validateMatchExists(location.pathname, processedRoutes[size].menu)) {
      menuComponent = <NavigationLayoutContent stackNavigationIsEnabled />;
    }
    return (
      <Layout
        {...customProps}
        header={decorateElement(headerComponent, processedRoutes[size].header)}
        menu={decorateElement(menuComponent, processedRoutes[size].menu)}
        menuText={menuText}
      >
        {decorateElement(contentComponent, processedRoutes[size].content)}
      </Layout>
    );
  }
}
export default withRouter(NavigationLayout);
