import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Switch, Route, withRouter, matchPath } from 'react-router-dom';
import RoutingStackDelegate from './RoutingStackDelegate';
import { processedRoutesPropType } from './configurationPropTypes';
interface Properties {
  /**
   * The routing configuration from which Routes will be generated.
   */
  routes: processedRoutesPropType;
  /**
   * Flag to enable navigation within the RoutingStack. If true, functions will be exposed to the Routes that
   * will allow for traversal up to parent paths.
   */
  navEnabled: boolean;
  /**
   * The current react-router location. Provided by the `withRouter()` HOC.
   */
  location: PropTypes.object;
  /**
   * The current react-router history. Provided by the `withRouter()` HOC.
   */
  history: PropTypes.object;
  /**
   * Any additional Routes that will be inserted after the configuration-generated Routes. Generally used
   * to insert custom Redirects or fallback Routes.
   */
  children: JSX.Element;
  /**
   * Any additional props that should be propagated to the generated Route components.
   */
  ancestorProps: PropTypes.object;
}
interface IRoutingStackProps extends JSX.HTMLAttributes<Element> {
  routes?: any;
  location?: any;
  navEnabled?: any;
  history?: any;
  ancestorProps?: any;
}
type RoutingStackState = {
  stackLocation?: undefined;
};
class RoutingStack extends React.Component<IRoutingStackProps, RoutingStackState> {
  constructor(props) {
    super(props);
    updateStackLocation = updateStackLocation.bind(this);
    createRoutes = createRoutes.bind(this);
    state = {
      stackLocation: undefined,
    };
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== props) {
      // eslint-disable-next-line react/no-did-update-set-state
      setState({
        // The stackLocation must be reset upon rerendering to be in sync with any navigation that may have occurred.
        stackLocation: undefined,
      });
    }
  }
  updateStackLocation(path) {
    setState({
      stackLocation: { pathname: path },
    });
  }
  createRoutes(routes) {
    const { navEnabled, location, history, ancestorProps } = props;
    const { stackLocation } = state;
    if (!routes || !routes.length) {
      return undefined;
    }
    return routes.map(routeData => {
      const delegateData = {
        location: stackLocation || location,
      };
      delegateData.show = ({ path }) => {
        if (matchPath(location.pathname, { path })) {
          updateStackLocation(path);
        } else {
          history.push(path);
        }
      };
      if (routeData.parentPaths && routeData.parentPaths.length) {
        delegateData.parentPaths = routeData.parentPaths.reduce((matchingPaths, path) => {
          const match = matchPath(location.pathname, { path });
          if (match) {
            matchingPaths.push(match.url);
          }
          return matchingPaths;
        }, []);
        if (navEnabled && delegateData.parentPaths.length) {
          delegateData.showParent = () => {
            updateStackLocation(delegateData.parentPaths[delegateData.parentPaths.length - 1]);
          };
          if (delegateData.parentPaths.length > 1) {
            delegateData.showRoot = () => {
              updateStackLocation(delegateData.parentPaths[0]);
            };
          }
        }
      }
      const ComponentClass = routeData.componentClass;
      return (
        <Route
          path={routeData.path}
          key={routeData.path}
          render={() => (
            <Route
              location={props.location}
              render={() => <ComponentClass {...ancestorProps} {...routeData.componentProps} routingStackDelegate={RoutingStackDelegate.create(delegateData)} />}
            />
          )}
        />
      );
    });
  }
  render() {
    const { routes, location, children } = props;
    return (
      <Switch location={state.stackLocation || location}>
        {createRoutes(routes)}
        {children}
      </Switch>
    );
  }
}
export default withRouter(RoutingStack);
