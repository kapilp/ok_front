import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Redirect } from 'react-router-dom';
import classNames from 'classnames/bind';
import RoutingStack from './RoutingStack';
import { processedRoutesPropType } from './configurationPropTypes';
import styles from './NavigationLayoutContent.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The routing configuration from which Routes will be generated.
   */
  navigationLayoutRoutes: processedRoutesPropType;
  /**
   * The String path to redirect to should the routes specified by the routeConfig all fail to match.
   */
  redirectPath: string;
  /**
   * Flag to enable navigation within the RoutingStack.
   */
  stackNavigationIsEnabled: boolean;
}
const NavigationLayoutContent: Component = ({ navigationLayoutRoutes, redirectPath, stackNavigationIsEnabled, ...customProps }) => (
  <div className={cx('content')}>
    <RoutingStack navEnabled={stackNavigationIsEnabled} routes={navigationLayoutRoutes} ancestorProps={customProps}>
      {redirectPath && <Redirect to={redirectPath} />}
    </RoutingStack>
  </div>
);
export default NavigationLayoutContent;
