import {
  Component,
  createStore,
  mergeProps,
  splitProps,
  useContext
} from 'solid-js';
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import TransitionGroup from "react-transition-group/TransitionGroup";
import CSSTransition from "react-transition-group/CSSTransition";
import Slide from "./Slide";
import transitions from "./Slide.module.scss";
const cx = classNamesBind.bind(transitions);
interface Properties {
  /**
   * The array of components for the group. Only the last component is visible. The others are hidden but still mounted.
   */
  items: JSX.Element[],
  /**
   * When true, the transition between slides is animated.
   */
  isAnimated: boolean
};
const defaultProps = {
  isAnimated: false
};
interface ISlideGroupProps extends JSX.HTMLAttributes<Element> {
  items?: any;
  isAnimated?: any;
  customProps?: any;
}
function hidePreviousSlide(enteredElement: HTMLElement) {
  if (enteredElement.previousSibling) {
    enteredElement.previousSibling.setAttribute("aria-hidden", true);
  }
}
function showPreviousSlide(exitingElement: HTMLElement) {
  if (exitingElement.previousSibling) {
    exitingElement.previousSibling.removeAttribute("aria-hidden");
  }
}
export const SlideGroup: Component = (props:ISlideGroupProps) => {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  let slideGroup: HTMLElement;



  const setContainer = (el: HTMLElement) {
    if (!el) {
      return;
    }
    slideGroup = el;
  }
  const [extracted, customProps] = splitProps(props, [
  "items", "isAnimated"
     ]);

    // We don't want to render the transition group when no children exist. Doing so will cause the first child to
    // animate into place, which in most cases we do not want.
    if (!extracted.items || !extracted.items.length) {
      return null;
    }
    // We use the key from the first child as the key for the transition group. This will cause the transition group to
    // rerender when the root child changes and subsequently prevent that child from animating into position.
    const transitionGroupKey = extracted.items[0].key;
    const itemCount = extracted.items.length - 1;
    const transitionNames = {
      enter: transitions.enter,
      enterActive: transitions["enter-active"],
      exit: transitions.exit,
      exitActive: transitions["exit-active"]
    };

    return (
      <TransitionGroup
        {...customProps}
        ref={setContainer}
        className={classNames(
          cx("slide-group", theme.className),
          customProps.className
        )}
        key={transitionGroupKey}
      >
        {extracted.items.map((item, index) => (
          <CSSTransition
            classNames={transitionNames}
            enter={extracted.isAnimated}
            onEntered={hidePreviousSlide}
            exit={extracted.isAnimated}
            onExit={showPreviousSlide}
            timeout={300}
            key={item.key}
            id="shmmoop"
          >
            <Slide isHidden={!extracted.isAnimated && index !== itemCount}>{item}</Slide>
          </CSSTransition>
        ))}
      </TransitionGroup>
    );

}



