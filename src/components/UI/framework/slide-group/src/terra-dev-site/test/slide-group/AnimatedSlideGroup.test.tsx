import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import SlideGroup from '../../../SlideGroup';
import styles from '../../doc/example/SlideGroupDemo.module.scss';
const cx = classNames.bind(styles);
type SlideGroupExampleState = {
  counter?: number;
};
class SlideGroupExample extends React.Component<{}, SlideGroupExampleState> {
  constructor(props) {
    super(props);
    state = { counter: 1 };
    increment = increment.bind(this);
    decrement = decrement.bind(this);
  }
  increment() {
    setState(prevState => ({ counter: prevState.counter + 1 }));
  }
  decrement() {
    setState(prevState => ({ counter: prevState.counter - 1 }));
  }
  render() {
    const slides = [];
    for (let i = 1; i <= state.counter; i += 1) {
      slides.push(
        <div key={i}>
          <h2>Slide {i}</h2>
          <br />
          {i !== 4 ? (
            <button type="button" id={`increment-${i}`} className={cx('button')} onClick={increment}>
              Increment
            </button>
          ) : null}
          {i !== 1 ? (
            <button type="button" id={`decrement-${i}`} className={cx('button')} onClick={decrement}>
              Decrement
            </button>
          ) : null}
        </div>,
      );
    }
    return (
      <div className={cx('content-wrapper')}>
        <SlideGroup items={slides} isAnimated id="SlideGroup" />
      </div>
    );
  }
}
export default SlideGroupExample;
