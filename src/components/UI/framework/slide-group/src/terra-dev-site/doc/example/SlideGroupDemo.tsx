import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import SlideGroup from 'terra-slide-group';
import styles from './SlideGroupDemo.module.scss';
const cx = classNames.bind(styles);
interface ISlideGroupDemoProps extends JSX.HTMLAttributes<Element> {
  isAnimated?: boolean;
}
type SlideGroupDemoState = {
  counter?: number;
};
class SlideGroupDemo extends React.Component<ISlideGroupDemoProps, SlideGroupDemoState> {
  constructor(props) {
    super(props);
    increment = increment.bind(this);
    decrement = decrement.bind(this);
    state = {
      counter: 1,
    };
  }
  increment() {
    setState(prevState => ({
      counter: prevState.counter + 1,
    }));
  }
  decrement() {
    setState(prevState => ({
      counter: prevState.counter - 1,
    }));
  }
  render() {
    const slides = [];
    for (let i = 0; i < state.counter; i += 1) {
      slides.push(
        <div key={`Slide ${i}`} className={cx('slide')}>
          <h2>
            Slide
            {i}
          </h2>
          <br />
          <button type="button" onClick={increment}>
            Increment
          </button>
          {i !== 0 ? (
            <button type="button" onClick={decrement}>
              Decrement
            </button>
          ) : null}
        </div>,
      );
    }
    return (
      <div className={cx('container')}>
        <SlideGroup items={slides} isAnimated={props.isAnimated} />
      </div>
    );
  }
}
export default SlideGroupDemo;
