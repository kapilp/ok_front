import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './Slide.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * If true, the Slide is marked as hidden using accessiblity attributes.
   */
  isHidden: boolean;
  /**
   * The components to display inside the Slide.
   */
  children: JSX.Element;
}
const defaultProps = {
  isHidden: false,
};
const Slide: Component = props => {
  const theme = React.useContext(ThemeContext);
  return (
    <div className={cx('slide', theme.className)} aria-hidden={props.isHidden || null}>
      <div className={cx('slide-shadow')} />
      {props.children}
    </div>
  );
};
props = mergeProps({}, defaultProps, props);
