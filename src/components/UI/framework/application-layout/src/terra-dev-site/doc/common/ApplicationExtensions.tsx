import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import Button from 'terra-button';
import IconSettings from 'terra-icon/lib/icon/IconSettings';
import IconCalendar from 'terra-icon/lib/icon/IconCalendar';
import IconFeaturedOutline from 'terra-icon/lib/icon/IconFeaturedOutline';
import classNames from 'classnames/bind';
import styles from './ApplicationExtensions.module.scss';
import ExtensionsDisclosure from './ExtensionsDisclosure';
import { Utils } from '../../../ApplicationLayout';
const cx = classNames.bind(styles);
interface Properties {
  disclosureManager: disclosureManagerShape;
  layoutConfig: Utils.propTypes.layoutConfigPropType;
}
interface IApplicationExtensionsProps extends JSX.HTMLAttributes<Element> {
  layoutConfig?: any;
  disclosureManager?: any;
}
class ApplicationExtensions extends React.Component<IApplicationExtensionsProps, {}> {
  constructor(props) {
    super(props);
    discloseExtensionContent = discloseExtensionContent.bind(this);
  }
  discloseExtensionContent(extensionName) {
    const { disclosureManager } = props;
    disclosureManager.disclose({
      preferredType: 'modal',
      size: 'tiny',
      content: {
        key: `${extensionName}-disclosure`,
        component: <ExtensionsDisclosure name={extensionName} />,
      },
    });
  }
  render() {
    const { layoutConfig } = props;
    const isCompactSize = Utils.helpers.isSizeCompact(layoutConfig.size);
    let containerProps;
    let variant;
    if (isCompactSize) {
      containerProps = {
        style: { padding: '10px', width: '100%', backgroundColor: 'lightgrey' },
      };
      variant = Button.Opts.Variants.ACTION;
    }
    return (
      <div {...containerProps}>
        <Button icon={<IconSettings />} text="Settings" variant={variant} isIconOnly className={cx('extension-button')} onClick={() => discloseExtensionContent('Settings')} />
        <Button icon={<IconCalendar />} text="Calendar" variant={variant} isIconOnly className={cx('extension-button')} onClick={() => discloseExtensionContent('Calendar')} />
        <Button
          icon={<IconFeaturedOutline />}
          text="Favorites"
          variant={variant}
          isIconOnly
          className={cx('extension-button')}
          onClick={() => discloseExtensionContent('Favorites')}
        />
      </div>
    );
  }
}
export default withDisclosureManager(ApplicationExtensions);
