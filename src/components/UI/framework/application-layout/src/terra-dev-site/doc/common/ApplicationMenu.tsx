import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import RoutingStackDelegate from 'terra-navigation-layout/lib/RoutingStackDelegate';
import Button from 'terra-button';
import ContentContainer from 'terra-content-container';
import classNames from 'classnames/bind';
import styles from './ApplicationMenu.module.scss';
import RoutingMenu from '../../../menu/RoutingMenu';
const cx = classNames.bind(styles);
interface IApplicationMenuProps extends JSX.HTMLAttributes<Element> {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
    togglePin?: (...args: any[]) => any;
    menuIsPinned?: boolean;
  };
  routingStackDelegate?: any;
  includeNestedMenu?: boolean;
  menuName?: string;
  baseUrl?: string;
}
type ApplicationMenuState = {
  menuItems?: { key: any; text: any; path: any; hasSubMenu: any }[];
};
class ApplicationMenu extends React.Component<IApplicationMenuProps, ApplicationMenuState> {
  constructor(props) {
    super(props);
    const items = [];
    for (let i = 0; i < 20; i += 1) {
      items.push({
        text: `Item ${i}`,
        path: `${props.baseUrl}/item_${i}`,
      });
    }
    if (props.includeNestedMenu) {
      items.push({
        text: 'Nested Menu',
        path: `${props.baseUrl}/nested`,
        hasSubMenu: true,
      });
    }
    const menuItems = items.map(item => ({
      key: item.path,
      text: item.text,
      path: item.path,
      hasSubMenu: item.hasSubMenu,
    }));
    state = {
      menuItems,
    };
  }
  render() {
    const { menuName, layoutConfig, routingStackDelegate } = props;
    const { menuItems } = state;
    return (
      <ContentContainer
        footer={
          <div className={cx('footer')}>
            <Button
              text="Custom Event 1"
              isBlock
              onClick={() => {
                const evt = document.createEvent('CustomEvent');
                evt.initCustomEvent('applicationMenu.itemSelected', false, false, 'Custom Event 1');
                document.dispatchEvent(evt);
                if (layoutConfig && layoutConfig.toggleMenu) {
                  layoutConfig.toggleMenu();
                }
              }}
              className={cx('custom-event-button')}
            />
            <Button
              text="Custom Event 2"
              isBlock
              onClick={() => {
                const evt = document.createEvent('CustomEvent');
                evt.initCustomEvent('applicationMenu.itemSelected', false, false, 'Custom Event 2');
                document.dispatchEvent(evt);
                if (layoutConfig && layoutConfig.toggleMenu) {
                  layoutConfig.toggleMenu();
                }
              }}
            />
          </div>
        }
        fill
      >
        <RoutingMenu title={menuName} menuItems={menuItems} routingStackDelegate={routingStackDelegate} layoutConfig={layoutConfig} />
      </ContentContainer>
    );
  }
}
export default ApplicationMenu;
