import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import ContentContainer from 'terra-content-container';
import ActionHeader from 'terra-action-header';
import classNames from 'classnames/bind';
import styles from './ApplicationLayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  name: string;
  disclosureManager: disclosureManagerShape;
}
const ExtensionsDisclosure: Component = ({ name, disclosureManager }) => (
  <ContentContainer
    fill
    header={
      <ActionHeader
        title={name.charAt(0).toUpperCase() + name.slice(1)}
        onClose={disclosureManager.closeDisclosure}
        onBack={disclosureManager.goBack}
        onMaximize={disclosureManager.maximize}
        onMinimize={disclosureManager.minimize}
      />
    }
  >
    <div className={cx('content-wrapper')}>
      Content for extension:
      {name}
    </div>
  </ContentContainer>
);
export default withDisclosureManager(ExtensionsDisclosure);
