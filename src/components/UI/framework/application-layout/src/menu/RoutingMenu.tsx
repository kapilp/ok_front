import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
// import { withRouter, matchPath } from "react-router-dom";
import NavigationSideMenu  from "../../../navigation-side-menu/src/NavigationSideMenu";
import { RoutingStackDelegateType } from '../../../navigation-layout/src/RoutingStackDelegate';
import * as ApplicationLayoutPropTypes from '../utils/propTypes';

interface Properties {
  /**
   * The title to render within the RoutingMenu's header.
   */
  title: string,
  /**
   * The array of routing shapes to be rendered as menu items within the RoutingMenu.
   */
  menuItems: PropTypes.arrayOf(
    PropTypes.shape({
      text: string.isRequired,
      path: string.isRequired,
      hasSubMenu: boolean
    })
  ),
  /**
   * The Object of layout-related APIs provided to the components of the Layout.
   */
  layoutConfig: ApplicationLayoutPropTypes.layoutConfigPropType.isRequired,
  /**
   * The Object containing RoutingStack APIs provided to children of the RoutingStack. This is
   * used to render a Back button upon presence of a `showParent` implementation.
   */
  routingStackDelegate: import { RoutingStackDelegateType } from '../../../navigation-layout/src/RoutingStackDelegate';.propType.isRequired,
  /**
   * The location from the router context. This prop is provided by the `withRouter` HOC-generator.
   */
  location: PropTypes.shape({
    pathname: string
  }).isRequired
};
const routingMenuRootMenuKey = "routingMenuRootMenuKey";
interface IRoutingMenuProps extends JSX.HTMLAttributes<Element> {
  title?: any;
  routingStackDelegate?: any;
  menuItems?: any;
  layoutConfig?: any;
}
type RoutingMenuState = {
  selectedChildKey?: any,
  prevPropsLocationPathName?: any,
  prevPropsMenuItems?: any
};
class RoutingMenu extends React.Component<IRoutingMenuProps, RoutingMenuState> {
  /**
   * This function converts the given menuItems from the RoutingMenu API to the NavigationSideMenu API.
   * The path is used to uniquely identify the item within the NavigationSideMenu. The path and hasSubMenu
   * values are set as metaData on the item so that `handleMenuChange` will have easy access to those values.
   * @param {Array} menuItems is the Array of menuItem objects as specified by the RoutingMenu's proptype definition.
   */
  static buildSideMenuItems(menuItems) {
    return menuItems.map(item => ({
      key: item.path,
      text: item.text,
      hasSubMenu: !!item.hasSubMenu,
      metaData: {
        path: item.path,
        externalLink: item.externalLink,
        hasSubMenu: !!item.hasSubMenu
      }
    }));
  }
  constructor(props) {
    super(props);
    handleMenuChange = handleMenuChange.bind(this);
    /**
     * The menuItems are checked against the current location at initialization to ensure the
     * the desired selection styles are present.
     */
    state = {
      selectedChildKey: RoutingMenu.getSelectedChildKey(
        props.location.pathname,
        props.menuItems
      ),
      prevPropsLocationPathName: props.location.pathname,
      prevPropsMenuItems: props.menuItems
    };
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.location.pathname !== prevState.prevPropsLocationPathName ||
      nextProps.menuItems !== prevState.prevPropsMenuItems
    ) {
      /**
       * The selectedChildKey is re-evaluated when new props are received to keep the internal state consistent.
       */
      return {
        prevPropsLocationPathName: nextProps.location.pathname,
        prevPropsMenuItems: nextProps.menuItems,
        selectedChildKey: RoutingMenu.getSelectedChildKey(
          nextProps.location.pathname,
          nextProps.menuItems
        )
      };
    }
    return null;
  }
  /**
   * This function compares the given path against the paths of the given menuItems. If a match
   * (partial or otherwise) is detected, that path is returned. If no match is detected, `undefined` is returned.
   * @param {String} path is the path to be matched against
   * @param {Array} menuItems is the Array of menuItem objects as specified by the RoutingMenu's proptype definition.
   */
  static getSelectedChildKey(path, menuItems) {
    for (let i = 0, itemCount = menuItems.length; i < itemCount; i += 1) {
      const navItem = menuItems[i];
      if (navItem.path && matchPath(path, { path: navItem.path })) {
        return navItem.path;
      }
    }
    return undefined;
  }
  handleMenuChange(event, data) {
    const { routingStackDelegate, layoutConfig } = props;
    let routeFunc;
    if (data.metaData.externalLink) {
      routeFunc = () =>
        window.open(
          data.metaData.externalLink.path,
          data.metaData.externalLink.target || "_blank"
        );
    } else {
      setState({
        selectedChildKey: data.selectedChildKey
      });
      routeFunc = () => routingStackDelegate.show({ path: data.metaData.path });
    }
    /**
     * If the menuItem does not indicate the presence of a subsequent menu, it is assumed that a terminal
     * menu item has been selected. If the `toggleMenu` function is defined on the layoutConfig, then it is called
     * to close the menu before navigating.
     */
    if (!data.metaData.hasSubMenu && layoutConfig.toggleMenu) {
      return layoutConfig.toggleMenu().then(() => {
        routeFunc();
      });
    }
    /**
     * Otherwise, the layout is left alone and navigation occurs immediately.
     */
    return Promise.resolve().then(() => routeFunc());
  }
  render() {
    const { title, routingStackDelegate, menuItems } = props;
    const { selectedChildKey } = state;
    /**
     * The items using the simplified RoutingMenu menuItem API must be converted into the NavigationSideMenu's API.
     */
    const processedMenuItems = RoutingMenu.buildSideMenuItems(menuItems);
    /**
     * The RoutingMenu then constructs a menuItem that will act as the main page and render the menuItems as child items.
     * If there is no title and no `showParent` implementation present, the isRootMenu flag is set on the item to hide
     * the header altogether.
     */
    processedMenuItems.push({
      key: routingMenuRootMenuKey,
      text: title || "",
      childKeys: processedMenuItems.map(item => item.key),
      isRootMenu: !routingStackDelegate.showParent && !title
    });
    return (
      <NavigationSideMenu
        menuItems={processedMenuItems}
        onChange={handleMenuChange}
        routingStackBack={routingStackDelegate.showParent}
        selectedMenuKey={routingMenuRootMenuKey}
        selectedChildKey={selectedChildKey}
        data-routing-menu
      />
    );
  }
}
export default withRouter(RoutingMenu);
