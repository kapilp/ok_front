import { JSX, Component, createMemo, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { ApplicationMenuLayout } from '../../../application-menu-layout/src/ApplicationMenuLayout';
import { ApplicationMenuName } from '../../../application-name/src/ApplicationMenuName';
import { RoutingStackDelegateType } from '../../../navigation-layout/src/RoutingStackDelegate';
import { ApplicationMenuUtility } from '../../../application-utility/src/ApplicationMenuUtility';
import { disclosureType as modalDisclosureType } from '../../../modal-manager/src/ModalManager';
// import {  withDisclosureManager } from "terra-disclosure-manager";
import { DisclosureDelegateType } from '../../../disclosure-manager/src/DisclosureManager';
import * as ApplicationLayoutPropTypes from '../utils/propTypes';
import Helpers from '../utils/helpers';
import UtilityMenuWrapper from './_UtilityMenuWrapper';
import styles from './ApplicationMenu.module.scss';
const cx = classNamesBind.bind(styles);
interface IApplicationMenuProps extends JSX.HTMLAttributes<Element> {
  /**
   * The element to be placed within the fill flex styled content area.
   * This content is intended to be the user configured content for the menu.
   */
  content?: JSX.Element;
  /**
   * The element to be placed within the fit top area for extensions within the layout.
   */
  extensions?: JSX.Element;
  /**
   * The Object of layout-related APIs provided to the components of the Layout.
   */
  layoutConfig: ApplicationLayoutPropTypes.layoutConfigPropType;
  /**
   * Configuration values for the ApplicationName component.
   */
  nameConfig?: ApplicationLayoutPropTypes.nameConfigPropType;
  /**
   * Delegate prop that is provided by the NavigationLayout.
   */
  routingStackDelegate: RoutingStackDelegateType;
  /**
   * Configuration to be provided to the ApplicationUtility component.
   */
  utilityConfig?: ApplicationLayoutPropTypes.utilityConfigPropType;
  /**
   * DisclosureManagerDelegate instance automatically provided by a DisclosureManagerProvider.
   */
  disclosureManager?: DisclosureDelegateType;
}

export const ApplicationMenu = (props: IApplicationMenuProps) => {
  const handleUtilityDiscloseRequest = utilityMenu => {
    const { disclosureManager, layoutConfig } = props;
    if (layoutConfig && layoutConfig.toggleMenu) {
      layoutConfig.toggleMenu();
    }
    if (disclosureManager && utilityMenu) {
      disclosureManager.disclose({
        preferredType: modalDisclosureType,
        dimensions: { height: '420', width: '320' },
        content: {
          component: <UtilityMenuWrapper>{utilityMenu}</UtilityMenuWrapper>,
          key: 'application-menu-utility-menu',
        },
      });
    }
  };
  const handleUtilityOnChange = (event, itemData) => {
    const { utilityConfig, disclosureManager } = props;
    utilityConfig.onChange(event, itemData, disclosureManager && disclosureManager.disclose);
  };
  const renderHeader = isCompact => {
    const { nameConfig } = props;
    if (isCompact && nameConfig && (nameConfig.accessory || nameConfig.title)) {
      return (
        <div className={cx('menu-header')}>
          <ApplicationMenuName accessory={nameConfig.accessory} title={nameConfig.title} />
        </div>
      );
    }
    return null;
  };
  const renderExtensions = isCompact => {
    const { layoutConfig, extensions } = props;
    if (isCompact && extensions) {
      return React.cloneElement(extensions, { layoutConfig });
    }
    return null;
  };
  const renderFooter = isCompact => {
    const { utilityConfig } = props;
    if (isCompact && utilityConfig) {
      return (
        <ApplicationMenuUtility
          onChange={handleUtilityOnChange}
          onDisclose={handleUtilityDiscloseRequest}
          title={utilityConfig.title}
          accessory={utilityConfig.accessory}
          menuItems={utilityConfig.menuItems}
          initialSelectedKey={utilityConfig.initialSelectedKey}
          data-application-menu-utility
        />
      );
    }
    return null;
  };
  const [p, customProps] = splitProps(props, ['disclosureManager', 'content', 'extensions', 'layoutConfig', 'nameConfig', 'routingStackDelegate', 'utilityConfig']);
  const theme = useContext(ThemeContext);
  const menuClassNames = createMemo(() => classNames(cx('application-menu', theme.className), customProps.className));
  const isCompact = createMemo(() => Helpers.isSizeCompact(p.layoutConfig.size));
  let clonedContent;
  if (p.content) {
    clonedContent = React.cloneElement(p.content, {
      layoutConfig: p.layoutConfig,
      routingStackDelegate: p.routingStackDelegate,
    });
  }
  return (
    <div {...customProps} className={menuClassNames()}>
      <ApplicationMenuLayout header={renderHeader(isCompact())} extensions={renderExtensions(isCompact())} content={clonedContent} footer={renderFooter(isCompact())} />
    </div>
  );
};

// export default withDisclosureManager(ApplicationMenu);
