import {  Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import {
  withDisclosureManager,
  disclosureManagerShape
} from "terra-disclosure-manager";
interface Properties {
  /**
   * DisclosureManagerDelegate instance automatically provided by a DisclosureManagerProvider.
   */
  disclosureManager: disclosureManagerShape,
  /**
   * The utility menu to be wrapped.
   */
  children: JSX.Element.isRequired
};
const UtilityMenuWrapper: Component = ({
  disclosureManager,
  children,
  ...customProps
}) =>
  React.cloneElement(children, {
    ...customProps,
    onRequestClose: disclosureManager.dismiss,
    isHeightBounded: true
  });
export default withDisclosureManager(UtilityMenuWrapper);
