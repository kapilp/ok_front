import {  JSX, createMemo, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
// import { injectIntl, intlShape } from "react-intl";
import { DisclosureDelegateType } from '../../../disclosure-manager/src/DisclosureManager';
import { ApplicationHeaderLayout } from '../../../application-header-layout/src/ApplicationHeaderLayout';
import { ApplicationHeaderUtility } from '../../../application-utility/src/ApplicationHeaderUtility';
import { ApplicationHeaderName } from '../../../application-name/src/ApplicationHeaderName';
import { ApplicationLinksPropType, ApplicationTabs } from '../../../application-links/src/ApplicationLinks';
import { SvgIconMenu } from '../../../../core/icon/src/icon/IconMenu';
import { Popup } from '../../../popup/src/Popup';
import { processedRoutesPropType } from '../../../navigation-layout/src/configurationPropTypes';
import * as ApplicationLayoutPropTypes from '../utils/propTypes';
import Helpers from '../utils/helpers';
import styles from './ApplicationHeader.module.scss';

const cx = classNamesBind.bind(styles);
interface IApplicationHeaderProps extends JSX.HTMLAttributes<Element> {
  /**
   * Navigation tab alignment. Navigational links that will generate list items that will update the path.
   * These paths are matched with react-router for selection.
   */
  applicationLinks?: ApplicationLinksPropType;
  /**
   * The element to be placed within the fit start area for extensions within the layout.
   */
  extensions?: JSX.Element;
  /**
   * The Object of layout-related APIs provided to the components of the Layout.
   */
  layoutConfig?: ApplicationLayoutPropTypes.layoutConfigPropType;
  /**
   * The set of routes currently identified by the NavigationLayout. This prop is provided by the NavigationLayout.
   */
  navigationLayoutRoutes?: processedRoutesPropType[];
  /**
   * The window size currently identified by the NavigationLayout. This prop is provided by the NavigationLayout.
   */
  navigationLayoutSize?: string;
  /**
   * Configuration values for the ApplicationName component.
   */
  nameConfig?: ApplicationLayoutPropTypes.nameConfigPropType;
  /**
   * Configuration to be provided to the ApplicationUtility component.
   */
  utilityConfig?: ApplicationLayoutPropTypes.utilityConfigPropType;
  /**
   * Internationalization object with translation APIs. Provided by `injectIntl`.
   */
  //intl?: intlShape, //todo fix
  /**
   * DisclosureManagerDelegate instance automatically provided by a DisclosureManagerProvider.
   */
  disclosureManager?: DisclosureDelegateType;
  /**
   * Whether or not the header contains icons
   */
  hasIcons?: boolean;
}
const defaultProps = {
  applicationLinks: {},
  hasIcons: false,
};

export const ApplicationHeader = (props: IApplicationHeaderProps) => {
  props = mergeProps({}, defaultProps, props);

  const [contentNode, setContentNode] = createSignal<HTMLHeadElement>();
  const [state, setState] = createStore({ utilityComponent: undefined });

  const getTargetRef = () => {
    if (contentNode()) {
      return contentNode.querySelector('[data-application-header-utility]');
    }
    return undefined;
  };
  const handleUtilityDiscloseRequest = utility => {
    // setState({
    //   utilityComponent: React.cloneElement(utility, {
    //     onRequestClose: handleUtilityPopupCloseRequest
    //   })
    // });
  };
  const handleUtilityPopupCloseRequest = () => {
    if (state.utilityComponent) {
      setState({ utilityComponent: undefined });
    }
  };
  const handleUtilityOnChange = (event, itemData) => {
    const { utilityConfig, disclosureManager } = props;
    utilityConfig.onChange(event, itemData, disclosureManager && disclosureManager.disclose);
  };
  const renderToggle = () => {
    if (props.layoutConfig.toggleMenu) {
      return (
        <div className={cx('navbar-toggle')}>
          <button type="button" className={cx('toggle-button')} aria-label={'Toggle Menu'} onClick={props.layoutConfig.toggleMenu} data-application-header-toggle>
            <SvgIconMenu />
          </button>
        </div>
      );
    }
    return null;
  };
  const renderAppName = () => {
    if (props.nameConfig && (props.nameConfig.accessory || props.nameConfig.title)) {
      return <ApplicationHeaderName accessory={props.nameConfig.accessory} title={props.nameConfig.title} />;
    }
    return null;
  };
  const renderNavigation = isCompact => {
    if (!isCompact) {
      if (props.applicationLinks.links && props.applicationLinks.links.length) {
        return <ApplicationTabs {...props.applicationLinks} />;
      }
      return null;
    }
    /**
     * When compact, the navigation region of the header renders the application name component instead. At compact
     * sizes, the logo region within the ApplicationHeaderLayout is too small to use, so we instead render within
     * the navigation region which renders with a larger width.
     */
    return renderAppName();
  };
  const renderExtensions = isCompact => {
    const { layoutConfig, extensions } = props;
    if (!isCompact && extensions) {
      return React.cloneElement(extensions, { layoutConfig });
    }
    return null;
  };
  const renderUtilities = isCompact => {
    const { utilityConfig } = props;
    if (!isCompact && utilityConfig) {
      return (
        <ApplicationHeaderUtility
          onChange={handleUtilityOnChange}
          onDisclose={handleUtilityDiscloseRequest}
          title={utilityConfig.title}
          accessory={utilityConfig.accessory}
          menuItems={utilityConfig.menuItems}
          initialSelectedKey={utilityConfig.initialSelectedKey}
          data-application-header-utility
        />
      );
    }
    return null;
  };
  const renderUtilitiesPopup = () => {
    const { utilityComponent } = state;
    if (utilityComponent) {
      return (
        <Popup
          attachmentBehavior="push"
          contentAttachment="top center"
          contentHeight="auto"
          contentWidth="240"
          isArrowDisplayed
          isHeaderDisabled
          isOpen
          onRequestClose={handleUtilityPopupCloseRequest}
          targetRef={getTargetRef}
        >
          {utilityComponent}
        </Popup>
      );
    }
    return null;
  };
  const [p, customProps] = splitProps(props, [
    'disclosureManager',
    'applicationLinks',
    'extensions',
    'layoutConfig',
    'nameConfig',
    'utilityConfig',
    'navigationLayoutRoutes',
    'navigationLayoutSize',
    'intl',
    'hasIcons',
  ]);

  const theme = useContext(ThemeContext);
  const headerClassNames = createMemo(() => classNames(cx('application-navbar', { 'application-navbar-with-icons': p.hasIcons }, theme.className), customProps.className));
  const isCompact = createMemo(() => Helpers.isSizeCompact(p.layoutConfig.size));
  return (
    <header role="banner" {...customProps} className={headerClassNames()} ref={setContentNode}>
      <ApplicationHeaderLayout
        toggle={renderToggle()}
        logo={!isCompact() ? renderAppName() : null}
        navigation={renderNavigation(isCompact())}
        extensions={renderExtensions(isCompact())}
        utilities={renderUtilities(isCompact())}
      />
      {renderUtilitiesPopup()}
    </header>
  );
};

// export default injectIntl(withDisclosureManager(ApplicationHeader));
