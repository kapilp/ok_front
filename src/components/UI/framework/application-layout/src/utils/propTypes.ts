import { ItemShape } from '../../../application-utility/src/Utils';

/**
 * Shape for the `layoutConfig` prop provided to components within the ApplicationLayout.
 */
export type layoutConfigPropType = {
  size?: string;
  toggleMenu?: () => void;
  menuIsOpen?: boolean;
  togglePin?: boolean;
  menuIsPinned?: boolean;
};
/**
 * Shape for ApplicationLayout's `nameConfig` prop.
 */
export type nameConfigPropType = {
  accessory?: JSX.Element;
  title?: string;
};
/**
 * Alignment of the navigational tabs.
 */
export type navigationAlignmentPropType = 'start' | 'center' | 'end';
/**
 * Shape for ApplicationLayout's `navigationItems` prop.
 */
export type navigationItemsPropType = {
  path: string;
  text: string;
  hasSubMenu?: boolean;
  icon?: JSX.Element;
}[];

/**
 * Shape for ApplicationLayout's `utilityConfig` prop.
 */
export type utilityConfigPropType = {
  title?: string;
  accessory?: JSX.Element;
  onChange: () => void;
  menuItems: ItemShape;
  initialSelectedKey: string;
};
/**
 * Shape for utilityConfig's menuItem.
 */
export type utilityMenuItemPropType = ItemShape;
