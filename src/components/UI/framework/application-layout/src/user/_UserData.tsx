import {  JSX, Component, createMemo, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import styles from './UserData.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * More information about the user.
   * Displayed next to the userPhoto and below the userName.
   */
  userDetail?: string;
  /**
   * The name to be displayed next to the userPhoto.
   */
  userName?: string;
  /**
   * The photo to be displayed next to the userName and userDetail.
   */
  userPhoto?: JSX.Element;
}

export const UserData = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['userDetail', 'userName', 'userPhoto']);
  const theme = useContext(ThemeContext);
  const userClassNames = createMemo(() => classNames(cx('user-data', theme.className), customProps.className));
  let userInfo = (
    <>
      {(p.userName || p.userDetail) && (
        <div className={cx('user-info')}>
          {!!p.userName && <div className={cx('name')}>{p.userName}</div>}
          {!!p.userDetail && <div className={cx('detail')}>{p.userDetail}</div>}
        </div>
      )}
    </>
  );

  return (
    <div {...customProps} className={userClassNames()}>
      {/*{!!p.userPhoto && React.cloneElement(p.userPhoto, { className: cx("photo") })}*/}
      {!!p.userPhoto && p.userPhoto}
      {userInfo}
    </div>
  );
};
