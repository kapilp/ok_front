import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import AbstractModal from '../../../AbstractModal';
import './AbstractModalTestStyles.module.scss';
type ModalIsFullscreenState = {
  isOpen?: boolean;
};
class ModalIsFullscreen extends React.Component<{}, ModalIsFullscreenState> {
  constructor() {
    super();
    state = {
      isOpen: false,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    return (
      <div>
        <AbstractModal ariaLabel="Terra Modal" isOpen={state.isOpen} isFullscreen onRequestClose={handleCloseModal} classNameModal="test-background-class">
          <div>
            <h1>Terra Modal</h1>
            <h2>Subtitle</h2>
            <hr />
            <p>The Terra Modal is appended to the document body.</p>
            <p>{"Modal is assigned a role of 'document' for accessibility."}</p>
            <button type="button" id="modal-button" onClick={handleCloseModal}>
              Close Modal
            </button>
          </div>
        </AbstractModal>
        <button type="button" id="modal-open-button" onClick={handleOpenModal}>
          Open isOpen modal
        </button>
      </div>
    );
  }
}
export default ModalIsFullscreen;
