import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import AbstractModal from '../../../AbstractModal';
import './AbstractModalTestStyles.module.scss';
type ModalDisableCloseOnOutsideClickState = {
  isOpen?: boolean;
};
class ModalDisableCloseOnOutsideClick extends React.Component<{}, ModalDisableCloseOnOutsideClickState> {
  constructor() {
    super();
    state = {
      isOpen: true,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    return (
      <div>
        <AbstractModal ariaLabel="Terra Modal" isOpen={state.isOpen} closeOnOutsideClick={false} onRequestClose={handleCloseModal} classNameModal="test-background-class">
          <div>
            <h1>Terra Modal</h1>
            <h2>Subtitle</h2>
            <hr />
            <p>The Terra Modal is appended to the document body.</p>
            <p>{"Modal is assigned a role of 'document' for accessibility."}</p>
            <button type="button" onClick={handleCloseModal}>
              Close Modal
            </button>
          </div>
        </AbstractModal>
        <button type="button" className="button-open-modal" onClick={handleOpenModal}>
          Open Modal
        </button>
      </div>
    );
  }
}
export default ModalDisableCloseOnOutsideClick;
