import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import AbstractModal from '../../../AbstractModal';
import './AbstractModalTestStyles.module.scss';
type ModalNoFocusableContentState = {
  isOpen?: boolean;
};
class ModalNoFocusableContent extends React.Component<{}, ModalNoFocusableContentState> {
  constructor() {
    super();
    state = {
      isOpen: true,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    return (
      <div>
        <AbstractModal ariaLabel="Terra Modal" isOpen={state.isOpen} onRequestClose={handleCloseModal} classNameModal="test-background-class">
          <div>No focusable content inside the modal.</div>
        </AbstractModal>
        <button type="button" className="button-open-modal" onClick={handleOpenModal}>
          Open Modal
        </button>
      </div>
    );
  }
}
export default ModalNoFocusableContent;
