import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import AbstractModal from '../../../AbstractModal';
import './AbstractModalTestStyles.module.scss';
type ModalIsOpenState = {
  isOpen?: boolean;
};
class ModalIsOpen extends React.Component<{}, ModalIsOpenState> {
  constructor() {
    super();
    state = {
      isOpen: false,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    return (
      <div id="modal">
        <AbstractModal ariaLabel="Terra Modal" isOpen={state.isOpen} onRequestClose={handleCloseModal} classNameModal="test-background-class">
          <div>
            <h1>Terra Modal</h1>
            <h2>Subtitle</h2>
            <hr />
            <p>The Terra Modal is appended to the document body.</p>
            <p>{"Modal is assigned a role of 'document' for accessibility."}</p>
            <button id="modal-button" type="button" onClick={handleCloseModal}>
              Close Modal
            </button>
          </div>
        </AbstractModal>
        <button type="button" id="modal-open-button" onClick={handleOpenModal}>
          Open Modal
        </button>
      </div>
    );
  }
}
export default ModalIsOpen;
