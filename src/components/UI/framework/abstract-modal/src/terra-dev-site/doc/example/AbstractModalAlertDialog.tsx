import {AbstractModal} from "../../../AbstractModal";
import {createEffect, createSignal} from "solid-js";
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import styles from "./ExampleAbstractSize.module.scss";
import generalStyles from "./AbstractModalDocCommon.module.scss";


const cx = classNames.bind(generalStyles);
/* eslint-disable-next-line react/prop-types */
const AlertDialogContent = (props:{} ) => {
  const [confirmBtn, setConfirmBtn] = createSignal<HTMLButtonElement>();

  createEffect(() => {
    confirmBtn() && setTimeout(()=>confirmBtn().focus());
  });
  return (
    <div id="alert-dialog-content" className={cx("content-wrapper")}>
      <h1>Alert Dialog Modal</h1>
      <br />
      <p>
        The abstract modal can be used to create an alert dialog modal. You can
        use the role, <code>alertdialog</code> , to create a modal dialog that
        interrupts the users workflow to get a response, usually some sort of
        confirmation.
      </p>
      <button ref={setConfirmBtn} type="button" onClick={props.handleCloseModal}>
        Confirm action
      </button>
      <button type="button" onClick={props.handleCloseModal}>
        Close Modal
      </button>
    </div>
  );
};
/* VoiceOver will not read the content of the modal dialog if the role attribute
 is set to `alertdialog` and ariaLabel is defined */
const ariaLabel = "";
type AbstractModalAlertDialogState = {
  isOpen?: boolean
};
export const AbstractModalAlertDialog = (props:{})=>{

   const [state, setState] = createStore({
      isOpen: false
    });
  const handleOpenModal = () => {
    setState({ isOpen: true });
  }
  const handleCloseModal = () =>  {
    setState({ isOpen: false });
  }

    return (
      <div>
        <AbstractModal
          ariaLabel={ariaLabel}
          aria-labelledby="alert-dialog-content"
          isOpen={state.isOpen}
          onRequestClose={handleCloseModal}
          closeOnOutsideClick={false}
          classNameModal={styles["fixed-size"]}
          role="alertdialog"
        >
          <AlertDialogContent handleCloseModal={handleCloseModal} />
        </AbstractModal>
        <button type="button" onClick={handleOpenModal}>
          Open Modal
        </button>
      </div>
    );

}

