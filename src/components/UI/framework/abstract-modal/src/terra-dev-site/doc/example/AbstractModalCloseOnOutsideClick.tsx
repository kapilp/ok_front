import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { AbstractModal } from '../../../AbstractModal';
import classNames from 'classnames/bind';
import styles from './ExampleAbstractSize.module.scss';
import generalStyles from './AbstractModalDocCommon.module.scss';
const cx = classNames.bind(generalStyles);
type AbstractModalCloseOnOutsideClickState = {
  isOpen?: boolean;
};
export const AbstractModalCloseOnOutsideClick = (props: {}) => {
  const [state, setState] = createStore({
    isOpen: false,
  });
  const handleOpenModal = () => {
    setState({ isOpen: true });
  };
  const handleCloseModal = () => {
    setState({ isOpen: false });
  };
  return (
    <div>
      <AbstractModal
        ariaLabel="Modal disable close on outside click"
        isOpen={state.isOpen}
        closeOnOutsideClick={false}
        onRequestClose={handleCloseModal}
        classNameModal={styles['fixed-size']}
      >
        <div className={cx('content-wrapper')}>
          <h1>Modal disable close on outside click</h1>
          <br />
          <p>You can close the modal by:</p>
          <ul>
            <li>- Pressing the ESC key</li>
            <li>- Clicking on the close button</li>
          </ul>
          <br />
          <button type="button" onClick={handleCloseModal}>
            Close Modal
          </button>
        </div>
      </AbstractModal>
      <button type="button" onClick={handleOpenModal}>
        Open Modal
      </button>
    </div>
  );
};
