import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { AbstractModal } from '../../../AbstractModal';
import classNames from 'classnames/bind';
import styles from './AbstractModalDocCommon.module.scss';
const cx = classNames.bind(styles);
type AbstractModalIsFullscreenState = {
  isOpen?: boolean;
};
export const AbstractModalIsFullscreen = (props: {}) => {
  const [state, setState] = createStore({
    isOpen: false,
  });
  const handleOpenModal = () => {
    setState({ isOpen: true });
  };
  const handleCloseModal = () => {
    setState({ isOpen: false });
  };
  return (
    <div>
      <AbstractModal ariaLabel="Fullscreen Modal" isOpen={state.isOpen} isFullscreen onRequestClose={handleCloseModal}>
        <div className={cx('content-wrapper')}>
          <h1>Fullscreen Modal</h1>
          <br />
          <p>This modal will always take up the full screen.</p>
          <p />
          <br />
          <button type="button" onClick={handleCloseModal}>
            Close Modal
          </button>
        </div>
      </AbstractModal>
      <button type="button" onClick={handleOpenModal}>
        Open Modal
      </button>
    </div>
  );
};
