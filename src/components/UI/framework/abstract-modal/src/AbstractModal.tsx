
import { KEY_ESCAPE } from "keycode-js";
// import "mutationobserver-shim";
// import "./_contains-polyfill";
// import "./_matches-polyfill";
import {ModalContent} from "./_ModalContent";
import {
  Component,
  createEffect, createSignal,
  onCleanup,
  mergeProps,
  splitProps
} from "solid-js";
import {Portal} from "solid-js/web";
type ZIndexes = "6000"| "7000"| "8000"| "9000"
interface Properties {
  /**
   * String that labels the modal for screen readers.
   */
  ariaLabel:string,
  /**
   * Content inside the modal dialog.
   */
  children:JSX.Element,
  /**
   * CSS classnames that are appended to the modal.
   */
  classNameModal?:string,
  /**
   * CSS classnames that are appended to the overlay.
   */
  classNameOverlay?:string,
  /**
   * If set to true, the modal will close when the esc key is pressed.
   */
  closeOnEsc?:boolean,
  /**
   * If set to true, the modal will close when a mouse click is triggered outside the modal.
   */
  closeOnOutsideClick?:boolean,
  /**
   * If set to true, the modal will be fullscreen on all breakpoint sizes.
   */
  isFullscreen?:boolean,
  /**
   * If set to true, the modal will rendered as opened.
   */
  isOpen:boolean,
  /**
   * Callback function indicating a close condition was met, should be combined with isOpen for state management.
   */
  onRequestClose:()=>void,
  /**
   * Role attribute on the modal dialog.
   */
  role?:string,
  /**
   * Allows assigning of root element custom data attribute for easy selecting of document base component.
   */
  rootSelector?:string,
  /**
   * Z-Index layer to apply to the ModalContent and ModalOverlay. Valid values are the standard modal layer?:'6000', and the max layer?:'8000'.
   */
  zIndex?:ZIndexes
};
const defaultProps = {
  classNameModal: null,
  classNameOverlay: null,
  closeOnEsc: true,
  closeOnOutsideClick: true,
  isFullscreen: false,
  role: "dialog",
  rootSelector: "#root",
  zIndex: "6000"
};
export const AbstractModal: Component<Properties> = (props) => {
  props = mergeProps({}, defaultProps, props)
    const [extracted, customProps] = splitProps(props, [
    "ariaLabel",
    "children",
    "classNameModal",
    "classNameOverlay",
    "closeOnEsc",
    "closeOnOutsideClick",
    "isFullscreen",
    "isOpen",
    "role",
    "rootSelector",
    "onRequestClose",
    "zIndex",
  ]);
  const [modalElementRef, setModalElementRef] = createSignal();
  const setModelElement = (el: HTMLDivElement)=>{
  setModalElementRef(el)
  }
  createEffect(() => {
    // eslint-disable-next-line no-prototype-builtins
    if (!Element.prototype.hasOwnProperty("inert")) {
      // IE10 throws an error if wicg-inert is imported too early, as wicg-inert tries to set an observer on document.body which may not exist on import
      // eslint-disable-next-line global-require
      //require("wicg-inert/dist/inert");
    }
  });
  function handleKeydown(e: KeyboardEvent) {
    if (e.keyCode === KEY_ESCAPE && props.closeOnEsc && props.isOpen) {
      if (modalElementRef()) {
        const body = document.querySelector("body");
        if (
          e.target === modalElementRef() ||
          modalElementRef().contains(e.target) ||
          e.target === body
        ) {
          props.onRequestClose();
        }
      }
    }
  }
  createEffect(() => {
    document.addEventListener("keydown", handleKeydown);
  }); // [props.closeOnEsc, props.isOpen, props.onRequestClose, modalElementRef]
  onCleanup(()=>{
    document.removeEventListener("keydown", handleKeydown);
  })

  return ( <>{!props.isOpen ? <></> :
    <Portal>
      <ModalContent
        {...customProps}
        closeOnOutsideClick={props.closeOnOutsideClick}
        ariaLabel={props.ariaLabel}
        classNameModal={props.classNameModal}
        classNameOverlay={props.classNameOverlay}
        role={props.role}
        isFullscreen={props.isFullscreen}
        onRequestClose={props.onRequestClose}
        rootSelector={props.rootSelector}
        zIndex={props.zIndex}
        aria-modal="true"
        ref={setModelElement}
        refEl={modalElementRef()}
      >
        {props.children}
      </ModalContent>
    </Portal>}</>
  );
};


// isOpened={props.isOpen}
