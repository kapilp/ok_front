import {
  Component,
  createEffect,
  onCleanup,
  splitProps,
  useContext
} from "solid-js";
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import styles from "./ModalOverlay.module.scss";

const cx = classNamesBind.bind(styles);
type ZIndexes = "6000"| "7000"| "8000"| "9000"
interface Properties extends JSX.HTMLAttributes<Element>{
  /**
   * Z-Index layer to apply to the ModalContent and ModalOverlay.
   */
  zIndex: ZIndexes
};
export const ModalOverlay: Component<Properties> = props => {
  const { overflow } = document.documentElement.style;
  createEffect(() => {
    // Disable scrolling on the page when Overlay is displayed
    document.documentElement.style.overflow = "hidden";
  });
  onCleanup(()=> {
    // Enable scrolling on the page since Overlay has gone away
    document.documentElement.style.overflow = overflow;
  })
  const [extracted, customProps] = splitProps(props, ["zIndex"    ])
  const theme = useContext(ThemeContext);
  return <div {...customProps} className={classNames(
    cx("overlay", `layer-${props.zIndex}`, theme.className),
    customProps.className
  )} />;
};

