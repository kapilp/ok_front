
// import { FormattedMessage } from "react-intl";
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import {VisuallyHiddenText} from "../../../core/visually-hidden-text/src/VisuallyHiddenText";
import {ModalOverlay} from "./_ModalOverlay";
import { hideModalDomUpdates, showModalDomUpdates } from "./inertHelpers";
import styles from "./ModalContent.module.scss";
import {
  Component,
  createEffect, onCleanup,
  mergeProps,
  splitProps,
  useContext
} from "solid-js";
const cx = classNamesBind.bind(styles);
type ZIndexes = "6000"| "7000"| "8000"| "9000"
interface Properties {
  /**
   * String that labels the modal for screen readers.
   */
  ariaLabel:string,
  /**
   * Content inside the modal dialog.
   */
  children:JSX.Element,
  /**
   * CSS classnames that are appended to the modal.
   */
  classNameModal?:string,
  /**
   * CSS classnames that are appended to the overlay.
   */
  classNameOverlay?:string,
  /**
   * If set to true, the modal will close when a mouseclick is triggered outside the modal.
   */
  closeOnOutsideClick?:boolean,
  /**
   * Callback function indicating a close condition was met, should be combined with isOpen for state management.
   */
  onRequestClose:()=>void,
  /**
   * If set to true, the modal will be fullscreen on all breakpoint sizes.
   */
  isFullscreen?:boolean,
  /**
   * If set to true, the modal dialog with have overflow-y set to scroll.
   */
  isScrollable?:boolean,
  /**
   * Role attribute on the modal dialog.
   */
  role?:string,
  /**
   * Allows assigning of root element custom data attribute for easy selecting of document base component.
   */
  rootSelector?:string,
  /**
   * Z-Index layer to apply to the ModalContent and ModalOverlay.
   */
  zIndex?:ZIndexes
};
const defaultProps = {
  classNameModal: null,
  classNameOverlay: null,
  closeOnOutsideClick: true,
  isFullscreen: false,
  isScrollable: false,
  role: "dialog",
  rootSelector: "#root",
  zIndex: "6000"
};
export const ModalContent: Component<Properties> = (props) => {
    const [extracted, customProps] = splitProps(props, [
    "ariaLabel",
    "children",
    "classNameModal",
    "classNameOverlay",
    "closeOnOutsideClick",
    "onRequestClose",
    "role",
    "isFullscreen",
    "isScrollable",
    "rootSelector",
    "zIndex",
      // Delete the closePortal prop that comes from react-portal.
    "closePortal",
    "fallbackFocus"
  ]);
  props = mergeProps({}, defaultProps, props)
  // Store element that was last focused prior to modal opening
  const modalTrigger = document.activeElement;
  createEffect(() => {
    props.refEl && setTimeout(()=>showModalDomUpdates(props.refEl, props.rootSelector));
  }); // , [ref, rootSelector]
  onCleanup(()=>{
    hideModalDomUpdates(modalTrigger, props.rootSelector);
  })
  /*let zIndexLayer = "6000";
  if (zIndexes.indexOf(zIndex) >= 0) {
    zIndexLayer = zIndex;
  }*/
  const theme = useContext(ThemeContext);

  const platformIsiOS =
    !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
  return (
    <>
      <ModalOverlay
        onClick={props.closeOnOutsideClick ? props.onRequestClose : null}
        className={props.classNameOverlay}
        zIndex={props.zIndex}
      />

      <div
        {...customProps}
        tabIndex={platformIsiOS ? "-1" : "0"}
        aria-label={props.ariaLabel}
        className={classNames(
          cx(
            "abstract-modal",
            { "is-fullscreen": props.isFullscreen },
            `layer-${props.zIndex}`,
            theme.className
          ),
          props.classNameModal
        )}
        role={props.role}
        ref={props.ref}
      >


            <VisuallyHiddenText
              data-terra-abstract-modal-begin
              tabIndex="-1"
              text={"Begin modal dialog"}
            />


        {props.children}

          <VisuallyHiddenText text={"End modal dialog"} />

      </div>
    </>
  );
};
