import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import AbstractModal from '../../src/AbstractModal';
interface Properties {
  zIndex: string;
  isFullscreen: boolean;
}
interface IModalExampleProps extends JSX.HTMLAttributes<Element> {
  zIndex?: any;
  isFullscreen?: any;
  otherPropsToTest?: any;
}
type ModalExampleState = {
  isOpen?: boolean;
};
class ModalExample extends React.Component<IModalExampleProps, ModalExampleState> {
  constructor() {
    super();
    state = {
      isOpen: true,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    const { zIndex, isFullscreen, ...otherPropsToTest } = props;
    return (
      <div>
        <AbstractModal
          ariaLabel="Terra Modal"
          isOpen={state.isOpen}
          onRequestClose={handleCloseModal}
          zIndex={props.zIndex}
          isFullscreen={props.isFullscreen}
          {...otherPropsToTest}
        >
          <div>
            <h1>Terra Modal</h1>
            <button type="button" onClick={handleCloseModal}>
              Close Modal
            </button>
          </div>
        </AbstractModal>
        <button type="button" onClick={handleOpenModal}>
          Open Modal
        </button>
      </div>
    );
  }
}
export default ModalExample;
