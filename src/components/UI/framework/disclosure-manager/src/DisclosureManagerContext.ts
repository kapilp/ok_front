import {createContext} from "solid-js";
type DisclosureManagerContextType = {
  disclose: (data: {}) => void
};
export const DisclosureManagerContext = createContext<DisclosureManagerContextType>();

