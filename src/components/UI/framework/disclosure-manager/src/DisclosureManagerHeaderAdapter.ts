import {Component, createEffect, useContext} from "solid-js";
import {DisclosureManagerHeaderAdapterContext} from "./DisclosureManagerHeaderAdapterContext";

interface Properties {
  /**
   * A title string to render within the DisclosureManager's header.
   */
  title: string,
  /**
   * A CollapsibleMenuView component to render within the DisclosureManager's header.
   */
  collapsibleMenuView: JSX.Element
};
export const DisclosureManagerHeaderAdapter: Component<Properties> = (props:Properties) => {
  const adapterContext = useContext(DisclosureManagerHeaderAdapterContext);
  createEffect(() => {
    adapterContext.register({ title: props.title, collapsibleMenuView: props.collapsibleMenuView });
  });
  return null;
};
