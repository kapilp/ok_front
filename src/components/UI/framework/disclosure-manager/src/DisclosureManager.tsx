// Understand what this component does?
import {  JSX, Component, mergeProps, useContext  } from 'solid-js';
import { createStore, produce } from "solid-js/store";
import { DisclosureManagerDelegate } from './DisclosureManagerDelegate'; // gives 3 functions create, clone, isEqual
import { DisclosureManagerContext } from './DisclosureManagerContext'; // 1st contest
import { DisclosureManagerHeaderAdapterContext } from './DisclosureManagerHeaderAdapterContext'; // 2nd context
///import {DisclosureManagerHeaderAdapter} from "./DisclosureManagerHeaderAdapter"; // Component that register on headerContext on each props change

// Just Get Rid of

export enum AvailableDisclosureSizes {
  TINY = 'tiny',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  HUGE = 'huge',
  FULLSCREEN = 'fullscreen',
}

export const AvailableDisclosureHeights = [240, 420, 600, 690, 780, 870, 960, 1140];
export const AvailableDisclosureWidths = [320, 480, 560, 640, 800, 960, 1120, 1280, 1440, 1600, 1760, 1920];
const defaultDimensions = {
  height: 690,
  width: 1120,
};
const defaultSize = AvailableDisclosureSizes.SMALL;
const isValidDimensions = (dimensions: { height: number; width: number }) =>
  AvailableDisclosureHeights.includes(dimensions.height) && AvailableDisclosureWidths.includes(dimensions.width);
const isValidSize = (size: AvailableDisclosureSizes) => true; // !!AvailableDisclosureSizes[size];
export type DisclosureDelegateType = (propValue, key: string, componentName: string, location, propFullName: string) => void;
interface IDisclosureManagerProps extends JSX.HTMLAttributes<Element> {
  /**
   * The child components that will be provided the disclosure functionality.
   */
  children?: JSX.Element;
  /**
   * A function used to provide rendering capabilities to the DisclosureManager.
   */
  render: Component;
  /**
   * An array of disclosure types that the DisclosureManager should support. If an unsupported disclosure request occurs, the DisclosureManager will
   * utilize its 'disclosureManager' prop and forward the request instead of handling the request itself.
   */
  supportedDisclosureTypes?: [];
  /**
   * A boolean indicating whether or not the DisclosureManager should handle all nested disclosure requests. When enabled, the DisclosureManager will handle all
   * disclose requests coming from disclosed components, regardless of the preferred disclosure type.
   */
  trapNestedDisclosureRequests?: boolean;
  /**
   * @private
   * A DisclosureManagerDelegate instance provided by a parent DisclosureManager. This prop is automatically provided by `withDisclosureManager` and should not
   * be explicitly given to the component.
   */

  // disclosureManager?: {}//DisclosureManagerDelegate.propType,
  disclosureManager?: { [key: string]: DisclosureDelegateType };
  /**
   * @private
   * The container to wrap the disclosed content. This should be provided from the application level.
   */
  withDisclosureContainer?: () => void;
}
const defaultProps = {
  supportedDisclosureTypes: [],
};

export const DisclosureManager = (props: IDisclosureManagerProps) => {
  /**
   * Clones the current disclosure component state objects and returns the structure for further mutation.
   */
  props = mergeProps({}, defaultProps, props);
  /*static cloneDisclosureState(state) {
    const newState = { ...state };
    newState.disclosureComponentKeys = Object.assign(
      [],
      newState.disclosureComponentKeys
    );
    newState.disclosureComponentData = { ...newState.disclosureComponentData };
    newState.disclosureComponentDelegates = Object.assign(
      [],
      newState.disclosureComponentDelegates
    );
    return newState;
  }*/

  // These cached functions are stored outside of state to prevent unnecessary rerenders.
  let dismissChecks = {};
  let onDismissResolvers = {};
  const [state, setState] = createStore({
    childComponentDelegate: generateChildComponentDelegate(),
    disclosureIsOpen: false,
    disclosureIsFocused: false,
    disclosureIsMaximized: false,
    disclosureSize: undefined,
    disclosureDimensions: undefined,
    disclosureComponentKeys: [],
    disclosureComponentData: {},
    disclosureComponentDelegates: [],
    disclosureTypeConfig: {},
  });
  const disclosureManager = useContext(DisclosureManagerContext);

  const generateHeaderContextValue = (key: string, initialTitle: string) => {
    return {
      register: ({ title, collapsibleMenuView }) => {
        setState('disclosureComponentData', produce(l => {
          l[key] = {
            ...state.disclosureComponentData[key],
            headerAdapterData: {
              title: initialTitle || title,
              collapsibleMenuView,
            },
          };
        }));
      },
    };
  };
  function generateChildComponentDelegate() {
    return DisclosureManagerDelegate.create({
      disclose: data => {
        if (disclosureTypeIsSupported(data.preferredType)) {
          return safelyCloseDisclosure().then(() => {
            openDisclosure(data);
            /**
             * The disclose Promise chain is resolved with a set of APIs that the disclosing content can use to
             * manipulate the disclosure, if necessary.
             */
            return {
              /**
               * The afterDismiss value is a deferred Promise that will be resolved when the disclosed component is dismissed.
               */
              afterDismiss: new Promise(resolve => {
                onDismissResolvers[data.content.key] = resolve;
              }),
              /**
               * The dismissDisclosure value is a function that the disclosing component can use to manually close the disclosure.
               * Any and all dismiss checks are still performed.
               */
              dismissDisclosure: safelyCloseDisclosure,
            };
          });
        }
        return disclosureManager.disclose(data);
      },
    });
  }
  const generateDisclosureComponentDelegate = (componentKey, disclosureState) => {
    const { disclosureComponentKeys, disclosureComponentData, disclosureIsMaximized, disclosureIsFocused, disclosureSize } = disclosureState;
    const componentData = disclosureComponentData[componentKey];
    const isFullscreen = disclosureSize === AvailableDisclosureSizes.FULLSCREEN;
    const popContent = generatePopFunction(componentData.key);
    const componentIndex = disclosureComponentKeys.indexOf(componentKey);
    const delegate = {} as { disclose: (data) => void };
    /**
     * The disclose function provided will push content onto the disclosure stack.
     */
    delegate.disclose = data => {
      if (props.trapNestedDisclosureRequests || disclosureTypeIsSupported(data.preferredType)) {
        return Promise.resolve().then(() => {
          pushDisclosure(data);
          return {
            afterDismiss: new Promise(resolve => {
              onDismissResolvers[data.content.key] = resolve;
            }),
            dismissDisclosure: generatePopFunction(data.content.key),
          };
        });
      }
      return disclosureManager.disclose(data);
    };
    /**
     * Allows a component to remove itself from the disclosure stack. If the component is the only element in the disclosure stack,
     * the disclosure is closed.
     */
    delegate.dismiss = popContent;
    /**
     * Allows a component to close the entire disclosure stack.
     */
    delegate.closeDisclosure = safelyCloseDisclosure;
    /**
     * Allows a component to remove itself from the disclosure stack. Functionally similar to `dismiss`, however `onBack` is
     * only provided to components in the stack that have a previous sibling.
     */
    delegate.goBack = componentIndex > 0 ? popContent : undefined;
    /**
     * Allows a component to request focus from the disclosure in the event that the disclosure mechanism in use utilizes a focus trap.
     */
    delegate.requestFocus = disclosureIsFocused ? () => Promise.resolve().then(releaseDisclosureFocus) : undefined;
    /**
     * Allows a component to release focus from itself and return it to the disclosure.
     */
    delegate.releaseFocus = !disclosureIsFocused ? () => Promise.resolve().then(requestDisclosureFocus) : undefined;
    /**
     * Allows a component to maximize its presentation size. This is only provided if the component is not already maximized.
     */
    delegate.maximize = !isFullscreen && !disclosureIsMaximized ? () => Promise.resolve().then(maximizeDisclosure) : undefined;
    /**
     * Allows a component to minimize its presentation size. This is only provided if the component is currently maximized.
     */
    delegate.minimize = !isFullscreen && disclosureIsMaximized ? () => Promise.resolve().then(minimizeDisclosure) : undefined;
    /**
     * Allows a component to register a function with the DisclosureManager that will be called before the component is dismissed for any reason.
     */
    delegate.registerDismissCheck = checkFunc => {
      dismissChecks[componentData.key] = checkFunc;
      return Promise.resolve();
    };
    return DisclosureManagerDelegate.create(delegate);
  };
  /**
   * Determines if the provided disclosure type is supported by the DisclosureManager.
   * @return `true` if the type is supported or if there is no fallback `disclosureManager` present. `false` is returned otherwise.
   */
  const disclosureTypeIsSupported = (type): boolean => {
    const { disclosureManager, supportedDisclosureTypes } = props;
    return supportedDisclosureTypes.indexOf(type) >= 0 || !disclosureManager;
  };
  const openDisclosure = (data: {}) => {
    let { dimensions } = data;
    if (dimensions && !isValidDimensions(dimensions)) {
      // dimensions were provided, but are invalid, set the default
      dimensions = defaultDimensions;
    }
    // eslint-disable-next-line prefer-destructuring
    let size = data.size;
    if (!size || (size && !isValidSize(size))) {
      // no valid size passed
      if (!dimensions) {
        // no valid size and no valid dimensions, set the default
        dimensions = defaultDimensions;
      }
      // ensure size set for passivity
      size = defaultSize;
    }
    const newState = {
      disclosureIsOpen: true,
      disclosureIsFocused: true,
      disclosureSize: size,
      disclosureDimensions: dimensions,
      disclosureComponentKeys: [data.content.key],
      disclosureTypeConfig: data.typeConfig,
      disclosureComponentData: {
        [data.content.key]: {
          key: data.content.key,
          name: data.content.name,
          props: data.content.props,
          component: data.content.component,
          ...(data.content.title !== undefined && {
            headerAdapterData: { title: data.content.title },
          }),
          headerAdapterContextValue: generateHeaderContextValue(data.content.key, data.content.title),
        },
      },
    };
    newState.disclosureComponentDelegates = [generateDisclosureComponentDelegate(data.content.key, newState)];
    setState(newState);
  };
  const pushDisclosure = data => {
    const newState = DisclosureManager.cloneDisclosureState(state);
    newState.disclosureComponentKeys.push(data.content.key);
    newState.disclosureComponentData[data.content.key] = {
      key: data.content.key,
      name: data.content.name,
      props: data.content.props,
      component: data.content.component,
      ...(data.content.title !== undefined && {
        headerAdapterData: { title: data.content.title },
      }),
      headerAdapterContextValue: generateHeaderContextValue(data.content.key, data.content.title),
    };
    newState.disclosureComponentDelegates = newState.disclosureComponentDelegates.concat(generateDisclosureComponentDelegate(data.content.key, newState));
    setState(newState);
  };
  const popDisclosure = callbackfunction => {
    /**
     * If there is only one disclosed component, the disclosure is closed and all state is reset.
     */
    if (state.disclosureComponentKeys.length === 1) {
      closeDisclosure(); // callbackfunction
    } else {
      const newState = DisclosureManager.cloneDisclosureState(state);
      newState.disclosureComponentData[newState.disclosureComponentKeys.pop()] = undefined;
      newState.disclosureComponentDelegates.pop();
      setState(newState); // callbackfunction
    }
  };
  const closeDisclosure = () => {
    // callbackfunction
    setState({
      disclosureIsOpen: false,
      disclosureIsFocused: false,
      disclosureIsMaximized: false,
      disclosureSize: undefined,
      disclosureDimensions: undefined,
      disclosureComponentKeys: [],
      disclosureComponentData: {},
      disclosureComponentDelegates: [],
      disclosureTypeConfig: {},
    });
  };
  const requestDisclosureFocus = () => {
    const newState = DisclosureManager.cloneDisclosureState(state);
    newState.disclosureIsFocused = true;
    newState.disclosureComponentDelegates = newState.disclosureComponentKeys.map(key => generateDisclosureComponentDelegate(key, newState));
    setState(newState);
  };
  const releaseDisclosureFocus = () => {
    const newState = DisclosureManager.cloneDisclosureState(state);
    newState.disclosureIsFocused = false;
    newState.disclosureComponentDelegates = newState.disclosureComponentKeys.map(key => generateDisclosureComponentDelegate(key, newState));
    setState(newState);
  };
  const maximizeDisclosure = () => {
    const newState = DisclosureManager.cloneDisclosureState(state);
    newState.disclosureIsMaximized = true;
    newState.disclosureComponentDelegates = newState.disclosureComponentKeys.map(key => generateDisclosureComponentDelegate(key, newState));
    setState(newState);
  };
  const minimizeDisclosure = () => {
    const newState = DisclosureManager.cloneDisclosureState(state);
    newState.disclosureIsMaximized = false;
    newState.disclosureComponentDelegates = newState.disclosureComponentKeys.map(key => generateDisclosureComponentDelegate(key, newState));
    setState(newState);
  };
  /**
   * Looks up the deferred afterDismiss promise for the provided disclosure key and
   * resolves it.
   */
  const resolveDismissPromise = key => {
    const resolve = onDismissResolvers[key];
    if (resolve) {
      resolve();
    }
    onDismissResolvers[key] = undefined;
  };
  /**
   * Resolves the dismiss checks for the current disclosure components in sequence. The Promise will either resolve if all checks resolve or
   * reject on the first detected rejection. This ensures that checks do not occur for components after the first rejection.
   */
  const resolveDismissChecksInSequence = keys => {
    return new Promise((resolve, reject) => {
      if (!keys.length) {
        resolve();
        return;
      }
      const key = keys.pop();
      generatePopFunction(key)()
        .then(() => {
          resolveDismissChecksInSequence(keys).then(resolve).catch(reject);
        })
        .catch(() => {
          reject();
        });
    });
  };
  /**
   * Closes the disclosure after calling all checks and resolving all deferred promises.
   * @return A Promise, resolved if the close was performed or rejected if it was not.
   */
  const safelyCloseDisclosure = () => {
    const disclosureKeys = Object.assign([], state.disclosureComponentKeys);
    /**
     * Before closing the disclosure, the dismiss checks for components in the stack are
     * executed in stack order. Components will be dismissed in order up until a rejection occurs, at which point
     * the blocking component will be presented.
     */
    return resolveDismissChecksInSequence(disclosureKeys).then(() => {
      dismissChecks = {};
      closeDisclosure();
    });
  };
  /**
   * Creates an instance of a pop function for the component represented by the given key.
   */
  const generatePopFunction = key => {
    return () => {
      const { disclosureComponentKeys } = state;
      if (disclosureComponentKeys[disclosureComponentKeys.length - 1] !== key) {
        /**
         * If the top component key in the disclosure stack does not match
         * the key used to generate this function, or the key is undefined, then the pop action is rejected.
         */
        return Promise.reject();
      }
      let promiseRoot = Promise.resolve();
      const dismissCheck = dismissChecks[key];
      if (dismissCheck) {
        promiseRoot = dismissCheck();
      }
      const callbackfunction = () => {
        dismissChecks[key] = undefined;
        resolveDismissPromise(key);
      };
      return promiseRoot.then(() => {
        popDisclosure(callbackfunction);
      });
    };
  };
  const generateDisclosureComponentMappingForRender = withDisclosureContainer => {
    const { disclosureComponentKeys, disclosureComponentData, disclosureComponentDelegates } = state;
    return disclosureComponentKeys.reduce((accumulator, key, index) => {
      const componentData = disclosureComponentData[key];
      const component = withDisclosureContainer ? withDisclosureContainer(componentData.component) : componentData.component;
      accumulator[key] = {
        component: (
          <DisclosureManagerHeaderAdapterContext.Provider value={componentData.headerAdapterContextValue} key={key}>
            <DisclosureManagerContext.Provider value={disclosureComponentDelegates[index]}>{component}</DisclosureManagerContext.Provider>
          </DisclosureManagerHeaderAdapterContext.Provider>
        ),
        headerAdapterData: componentData.headerAdapterData,
      };
      return accumulator;
    }, {});
  };

  const disclosureComponentMappingForRender = generateDisclosureComponentMappingForRender(props.withDisclosureContainer);
  return (
    <>
      {props.render &&
        props.render({
          dismissPresentedComponent: generatePopFunction(state.disclosureComponentKeys ? state.disclosureComponentKeys[state.disclosureComponentKeys.length - 1] : undefined),
          closeDisclosure: safelyCloseDisclosure,
          maximizeDisclosure:
            state.disclosureSize !== AvailableDisclosureSizes.FULLSCREEN && !state.disclosureIsMaximized ? () => Promise.resolve().then(maximizeDisclosure) : undefined,
          minimizeDisclosure:
            state.disclosureSize !== AvailableDisclosureSizes.FULLSCREEN && state.disclosureIsMaximized ? () => Promise.resolve().then(minimizeDisclosure) : undefined,
          children: {
            components: <DisclosureManagerContext.Provider value={state.childComponentDelegate}>{props.children}</DisclosureManagerContext.Provider>,
          },
          disclosure: {
            isOpen: state.disclosureIsOpen,
            isFocused: state.disclosureIsFocused,
            isMaximized: state.disclosureIsMaximized,
            size: state.disclosureSize,
            dimensions: state.disclosureDimensions,
            components: state.disclosureComponentKeys.map((key: string) => disclosureComponentMappingForRender[key].component),
            typeConfig: state.disclosureTypeConfig,
          },
          /**
           * The below values were added to give DisclosureManager implementations more control over the rendering of the disclosed components.
           * Some of the data provided by these keys is duplicated by the data provided in the above `disclose` value.
           * In a future major release, this render object will be restructured and simplified. Until then, either can be used as needed.
           */
          disclosureComponentKeys: state.disclosureComponentKeys,
          disclosureComponentData: disclosureComponentMappingForRender,
        })}
    </>
  );
};
