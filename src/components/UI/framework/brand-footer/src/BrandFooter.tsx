import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './BrandFooter.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * A set of navigation links to be displayed with optional headers.
   *
   * ```
   * Array structured like:
   * [
   *   {
   *     headerText: string,
   *     links: [
   *       text: required string,
   *       href: required string,
   *       target: string,
   *     ],
   *   },
   * ]
   * ```
   */
  sections: {
    /**
     * The optional text to display as a header
     */
    headerText?: string;
    /**
     * An array of navigation links with each element specifying text, href and target keys with appropriate values.
     */
    links: {
      /**
       * Text to be displayed as navigational link.
       */
      text: string;
      /**
       * URL of the navigational link.
       */
      href: string;
      /**
       * Attribute to open on same or different tab on clicking the navigational link.
       */
      target?: string;
    }[];
  }[];

  /**
   * If true link sections will be laid out from top to bottom, then left to right if the max width is reached
   */
  isVertical: boolean;
  /**
   * The content to be displayed in left side area of the footer.
   */
  contentLeft: JSX.Element;
  /**
   * The content to be displayed in right side area of the footer.
   */
  contentRight: JSX.Element;
  /**
   * The content to be displayed in bottom area of the footer.
   */
  contentBottom: JSX.Element;
}
const defaultProps = {
  sections: [],
  isVertical: false,
  contentLeft: null,
  contentRight: null,
  contentBottom: null,
};
export const BrandFooter: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [extracted, customProps] = splitProps(props, ['sections', 'isVertical', 'contentLeft', 'contentRight', 'contentBottom']);
  const theme = useContext(ThemeContext);
  const BrandFooterClassNames = ()=>classNames(cx(
    'brand-footer',
    theme.className,
  ),
  customProps.className);
  
  // Assign ids to use as keys
  for (let i = 0; i < props.sections.length; i += 1) {
    props.sections[i].id = i;
  }

  const containsASectionHeader = () => props.sections.some(linkGroup => linkGroup.headerText);
  
  let navigation = (
        <nav className={cx('nav', extracted.isVertical ? 'nav-vertical' : 'nav-horizontal')}>
          {props.sections.map(linkGroup => (
            <section className={cx('navigation-section')} key={linkGroup.id}>
              {
                // When displaying vertically if one column has a header all columns are aligned as if they have a header
                ((containsASectionHeader() && extracted.isVertical) || linkGroup.headerText) && (
                <h3 className={cx('list-header')} key={linkGroup.headerText}>
                  {// Insert a zero width space to act as a placeholder section header that doesn't display but takes vertical space
                    linkGroup.headerText || '\u200B'}
                </h3>
              )}
              <ul className={cx('menu')}>
                {linkGroup.links &&
                  linkGroup.links.map((link, index) => {
                    const spreadTarget = link.target !== undefined ? { target: link.target } : {};
                    const separator =
                      !extracted.isVertical && index >= 1 ? (
                        <span className={cx('separator')} aria-hidden>
                          /
                        </span>
                      ) : (
                        ''
                      );
                    return (
                      <li className={cx('list-item')} key={link.text + link.href}>
                        {separator}
                        <a className={cx('link')} href={link.href} {...spreadTarget}>
                          {link.text}
                        </a>
                      </li>
                    );
                  })}
              </ul>
            </section>
          ))}
        </nav>
  );
  return (
    <footer role="contentinfo" {...customProps} className={BrandFooterClassNames()}>
      {navigation}
      <div className={cx('footer-content')}>
        <div className={cx('content-top')}>
          {extracted.contentLeft}
          {extracted.contentRight}
        </div>
        <div className={cx('content-bottom')}>{extracted.contentBottom}</div>
      </div>
    </footer>
  );
};
