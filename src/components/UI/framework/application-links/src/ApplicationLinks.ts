import { ApplicationTabs } from './tabs/ApplicationTabs';
type ApplicationLinksPropType = {
  /**
   * Alignment of the navigational tabs. ( e.g start, center, end )
   */
  alignment?: 'start' | 'center' | 'end';
  /**
   * Navigational links that will generate tabs that will update the path. These paths are matched with react-router to selection.
   */
  links?: {
    /**
     * The id to append to the link.
     */
    id?: string;
    /**
     * The path to push to the route.
     */
    path: string;
    /**
     * The display text for the link.
     */
    text: string;
    /**
     * The display icon for the link
     */
    icon?: JSX.Element;
  }[];
};
export { ApplicationTabs, ApplicationLinksPropType };
