import {  JSX, Component, createEffect, createMemo, createSignal, onCleanup, untrack, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNamesBind from 'classnames/bind';
// import ResizeObserver from "resize-observer-polyfill";
// import { withRouter } from "react-router-dom";
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { Tab } from './_Tab';
import { TabMenu } from './_TabMenu';
import { CollapsedTab } from './_CollapsedTab';
import styles from './ApplicationTabs.module.scss';
const cx = classNamesBind.bind(styles);
interface IApplicationTabsProps extends JSX.HTMLAttributes<Element> {
  /**
   * Alignment of the navigational tabs. ( e.g start, center, end )
   */
  alignment?: 'start' | 'center' | 'end';
  /**
   * Navigational links that will generate tabs that will update the path. These paths are matched with react-router to selection.
   */
  links?: {
    /**
     * The id to append to the link.
     */
    id?: string;
    /**
     * The path to push to the route.
     */
    path: string;
    /**
     * The display text for the link.
     */
    text: string;
    icon?: JSX.Element;
  }[];

  /**
   * The location as provided by the `withRouter()` HOC.
   */
  location: {};
  /**
   * The match as provided by the `withRouter()` HOC.
   */
  match: {};
  /**
   * The history as provided by the `withRouter()` HOC.
   */
  history: {};
  /**
   * The staticContext as provided by the `withRouter()` HOC. This will only be provided when
   * within a StaticRouter.
   */
  staticContext?: {};
  hasIcons?: boolean;
}
const defaultProps = {
  alignment: 'center',
  links: [],
};

export const ApplicationTabs = (props: IApplicationTabsProps) => {
  props = mergeProps({}, defaultProps, props);
  let animationFrameID: null;
  const [container, setContainerNode] = createSignal<HTMLDivElement>();
  let contentWidth: number;
  let hiddenStartIndex: number;
  let isCalculating: boolean;
  let menuHidden: boolean;
  let resizeObserver: any;
  resetCalculations();
  const componentDidMount = () => {
    resizeObserver = new ResizeObserver(entries => {
      contentWidth = entries[0].contentRect.width;
      if (!isCalculating) {
        animationFrameID = window.requestAnimationFrame(() => {
          // Resetting the calculations so that all elements will be rendered face-up for width calculations
          resetCalculations();
          //forceUpdate();
        });
      }
    });
    resizeObserver.observe(container());
    handleResize(contentWidth);
  };
  createEffect(() => container() && untrack(componentDidMount));
  let isRun = false;
  createEffect(() => {
    if (props.links && isRun) {
      resetCalculations();
    }
    isRun = true;
  });
  let isRun2 = false;
  createEffect(() => {
    if (isCalculating && isRun2) {
      isCalculating = false;
      handleResize(contentWidth);
    }
    isRun2 = true;
  });
  onCleanup(() => {
    window.cancelAnimationFrame(animationFrameID);
    resizeObserver.disconnect(container());
    setContainerNode(null);
  });

  function resetCalculations() {
    animationFrameID = null;
    hiddenStartIndex = -1;
    menuHidden = false;
    isCalculating = true;
  }
  const handleResize = (width: number) => {
    // Calculate hide index
    const childrenCount = props.links.length;
    const tabWidth = childrenCount > 1 ? container().children[0].getBoundingClientRect().width : 0;
    const availableWidth = width - tabWidth;
    let newHideIndex = childrenCount;
    let calcMinWidth = 0;
    let isMenuHidden = true;
    for (let i = 0; i < childrenCount; i += 1) {
      calcMinWidth += tabWidth;
      if (calcMinWidth > availableWidth && !(i === childrenCount - 1 && calcMinWidth <= width)) {
        newHideIndex = i;
        isMenuHidden = false;
        break;
      }
    }
    if (hiddenStartIndex !== newHideIndex) {
      hiddenStartIndex = newHideIndex;
      menuHidden = isMenuHidden;
      //forceUpdate();
    }
  };

  const [p, customProps] = splitProps(props, ['alignment', 'links', 'location', 'match', 'history', 'staticContext', 'hasIcons']);
  const visibleTabs: JSX.Element[] = [];
  const collapsedTabs: JSX.Element[] = [];
  p.links.forEach((link, index) => {
    const tabProps = {
      id: link.id,
      path: link.path,
      text: link.text,
      key: link.path,
      externalLink: link.externalLink,
      icon: link.icon,
      location,
      history,
    };
    if (hiddenStartIndex < 0) {
      visibleTabs.push(<Tab {...tabProps} />);
      collapsedTabs.push(<CollapsedTab {...tabProps} />);
    } else if (index < hiddenStartIndex) {
      visibleTabs.push(<Tab {...tabProps} />);
    } else {
      collapsedTabs.push(<CollapsedTab {...tabProps} />);
    }
  });
  const tabClass = createMemo(() => cx('tabs-container', { 'is-calculating': isCalculating }, p.alignment));
  const theme = useContext(ThemeContext);
  return (
    <div {...customProps} className={cx('tabs-wrapper', theme.className)}>
      <div className={tabClass()} role="tablist" ref={setContainerNode}>
        {visibleTabs}
        <TabMenu location={p.location} isHidden={p.menuHidden} hasIcons={p.hasIcons}>
          {collapsedTabs}
        </TabMenu>
        <div className={cx('divider-after-last-tab')} />
      </div>
    </div>
  );
};

// export default withRouter(ApplicationTabs);
