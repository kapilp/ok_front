import {  JSX, Component, createSignal, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Popup } from '../../../popup/src/Popup';
// import { matchPath } from 'react-router-dom';
import * as KeyCode from 'keycode-js';
// import { FormattedMessage } from 'react-intl';
import { TabMenuList } from './_TabMenuList';
import { TabMenuDisplay } from './_TabMenuDisplay';

interface ITabMenuProps extends JSX.HTMLAttributes<Element> {
  /**
   * Child tabs to be placed in the tab menu.
   */
  children?: JSX.Element[];
  /**
   * Should the menu be hidden, set to true if there are no hidden items.
   */
  isHidden?: boolean;
  /**
   * The location as provided by the `withRouter()` HOC.
   */
  location: {};
}

export const TabMenu = (props: ITabMenuProps) => {
  const [getTargetRef, setTargetRef] = createSignal<HTMLElement>();
  const [state, setState] = createStore({
    isOpen: false,
  });
  let shouldResetFocus = false;

  // Todo Fix this:
  /*const componentDidUpdate = (prevProps) => {
    if (prevProps.location !== props.location) {
      // eslint-disable-next-line react/no-did-update-set-state
      setState({ isOpen: false });
    }
    if (shouldResetFocus && targetRef) {
      targetRef.focus();
      shouldResetFocus = targetRef !== document.activeElement;
    }
  }*/

  const handleOnRequestClose = () => {
    if (state.isOpen) {
      shouldResetFocus = true;
      setState({ isOpen: false });
    }
  };
  const handleOnClick = () => {
    if (!state.isOpen) {
      setState({ isOpen: true });
    }
  };
  const handleOnKeyDown = event => {
    if ((event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) && !state.isOpen) {
      setState({ isOpen: true });
    }
  };
  const createDisplay = popup => {
    const { location } = props;
    let icon;
    let isSelected = false;
    let childText;
    const childArray = props.children;
    const count = childArray.length;
    for (let i = 0; i < count; i += 1) {
      const child = childArray[i];
      if (matchPath(location.pathname, { path: child.props.path })) {
        childText = child.props.text;
        icon = child.props.icon;
        isSelected = true;
        break;
      }
    }
    return (
      <TabMenuDisplay
        onClick={handleOnClick}
        onKeyDown={handleOnKeyDown}
        popup={popup}
        ref={setTargetRef}
        isHidden={props.isHidden}
        text={childText || 'More'}
        isSelected={isSelected}
        icon={icon}
        key="application-tab-more"
        data-application-tabs-more
      />
    );
  };

  const [p, customProps] = splitProps(props, ['children']);

  let popup;
  if (state.isOpen) {
    const extraChildProps = { onTabClick: handleOnRequestClose };
    popup = (
      <Popup contentHeight="auto" contentWidth="240" onRequestClose={handleOnRequestClose} targetRef={getTargetRef} isOpen={state.isOpen} isArrowDisplayed>
        {/*<TabMenuList>{React.Children.map(children, child => React.cloneElement(child, extraChildProps))}</TabMenuList>*/}
        <TabMenuList>{p.children}</TabMenuList>
      </Popup>
    );
  }
  // return createDisplay(popup);
  return popup;
};
