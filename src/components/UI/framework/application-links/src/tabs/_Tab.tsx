import {  JSX, createMemo, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Dynamic } from 'solid-js/web';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
// import { matchPath } from "react-router-dom";
import * as KeyCode from 'keycode-js';
import styles from './ApplicationTabs.module.scss';
const cx = classNamesBind.bind(styles);
interface IApplicationTabProps extends JSX.HTMLAttributes<Element> {
  /**
   * The optional external link. Executes on window.open();
   */
  externalLink?: {
    path: string;
    target?: string;
  };
  /**
   * The history as provided by the `withRouter()` HOC.
   */
  history: {};
  /**
   * The location as provided by the `withRouter()` HOC.
   */
  location: {};
  /**
   * The path to push to the route.
   */
  path: string;
  /**
   * The display text for the tab.
   */
  text: string;
  /**
   * The click callback of the tab.
   */
  onTabClick?: () => void;
  /**
   * The display icon for the tab
   */
  icon?: JSX.Element;
}

export const Tab = (props: IApplicationTabProps) => {
  const [state, setState] = createStore({ active: false, focused: false });

  const handleOnBlur = () => {
    setState({ focused: false });
  };
  const handleKeyDown = (event: Event) => {
    // Add active state to FF browsers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: true });
    }
    // Add focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
      event.preventDefault();
      handleOnClick(event);
    }
  };
  const handleKeyUp = (event: Event) => {
    // Remove active state from FF broswers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: false });
    }
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      event.preventDefault();
      event.stopPropagation();
      setState({ focused: true });
    }
  };
  const isCurrentPath = () => {
    // return !!matchPath(props.location.pathname, { path: props.path });
    return false;
  };
  const handleOnClick = (event: Event) => {
    if (props.externalLink) {
      window.open(props.externalLink.path, props.externalLink.target || '_blank');
      if (props.onTabClick) {
        props.onTabClick(event);
      }
      return;
    }
    if (!isCurrentPath()) {
      props.history.push(props.path);
    } else if (props.onTabClick) {
      props.onTabClick(event);
    }
  };
  const [p, customProps] = splitProps(props, ['externalLink', 'history', 'location', 'onTabClick', 'path', 'text', 'icon']);

  const isCollapsed = false;
  const hasIcon = createMemo(() => !!p.icon);
  const isCurrent = isCurrentPath();
  const theme = useContext(ThemeContext);
  const tabClassNames = createMemo(() =>
    classNames(
      cx('tab', { 'tab-with-icon': hasIcon() }, { 'is-disabled': isCurrent && !isCollapsed }, { 'is-active': state.active }, { 'is-focused': state.focused }, theme.className),
      customProps.className,
    ),
  );
  const tabAttr = { 'aria-current': isCurrent };
  const childrenClassNames = createMemo(() =>
    cx('tab-inner', {
      'tab-inner-with-icon': hasIcon(),
    }),
  );
  const ChildElement = createMemo(() => (hasIcon() ? 'div' : 'span'));
  return (
    <button
      {...{ ...customProps, ...tabAttr }}
      role="link"
      type="button"
      tabIndex="0"
      className={tabClassNames()}
      onClick={handleOnClick}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
    >
      <Dynamic component={ChildElement()} className={childrenClassNames()}>
        {hasIcon() && <span className={cx('tab-icon')}>{p.icon}</span>}
        <span className={cx('tab-label')}>{p.text}</span>
      </Dynamic>
    </button>
  );
};
