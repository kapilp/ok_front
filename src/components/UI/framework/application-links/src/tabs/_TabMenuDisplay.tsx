import {  JSX, Component, createMemo, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { SvgIconCaretDown } from '../../../../core/icon/src/icon/IconCaretDown';
import * as KeyCode from 'keycode-js';
import styles from './ApplicationTabs.module.scss';
const cx = classNamesBind.bind(styles);
interface ITabMenuDisplayProps extends JSX.HTMLAttributes<Element> {
  /**
   * Whether or not the menu display should be hidden.
   */
  isHidden?: boolean;
  /**
   * Whether or not the menu display should be animated with selection.
   */
  isSelected?: boolean;
  /**
   * Callback func for onKeyDown.
   */
  onKeyDown?: () => void;
  /**
   * The terra-popup to attach to the menu display.
   */
  popup?: JSX.Element;
  /**
   * The display text for the display.
   */
  text?: string;
  /**
   * Ref callback for menu display.
   */
  ref?: () => void;
  icon?: JSX.Element;
}
const defaultProps = {
  isSelected: false,
  isHidden: false,
};

export const TabMenuDisplay = (props: ITabMenuDisplayProps) => {
  props = mergeProps({}, defaultProps, props);

  const [contentNode, setContentNode] = createSignal<HTMLDivElement>();
  const [state, setState] = createStore({ focused: false, active: false });
  const handleOnBlur = () => {
    if (!props.popup) {
      setState({ focused: false });
    }
  };
  const handleKeyDown = (event: Event) => {
    // Add active state to FF browsers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: true });
    }
    // Add focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
      event.preventDefault();
      if (props.onKeyDown) {
        props.onKeyDown(event);
      }
    }
  };
  const handleKeyUp = (event: Event) => {
    // Remove active state from FF broswers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: false });
    }
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }
  };

  const [p, customProps] = splitProps(props, ['isHidden', 'isSelected', 'onKeyDown', 'popup', 'ref', 'text', 'icon']);
  const hasIcon = createMemo(() => !!p.icon);
  const theme = useContext(ThemeContext);
  const displayClassNames = createMemo(() =>
    classNames(cx('tab-menu-display', { 'is-hidden': p.isHidden }, { 'is-active': state.active }, { 'is-focused': state.focused }, theme.className), customProps.className),
  );
  const attributes = createMemo(() => {
    return { 'aria-current': p.isSelected };
  });
  const moreButtonClassNames = createMemo(() =>
    cx('tab-inner', {
      'tab-inner-with-icon': hasIcon(),
    }),
  );
  return (
    <div
      {...customProps}
      {...attributes()}
      role="tab"
      tabIndex="0"
      className={displayClassNames()}
      ref={(el: HTMLDivElement) => {
        setContentNode(el);
        props.ref(el);
      }}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onBlur={handleOnBlur}
    >
      <div className={moreButtonClassNames()}>
        {hasIcon() && <span className={cx('tab-menu-display-icon')}>{p.icon}</span>}
        <div className={cx('tab-menu-display-label')}>
          <span>{p.text}</span>
          <SvgIconCaretDown />
        </div>
      </div>
      {p.popup}
    </div>
  );
};
