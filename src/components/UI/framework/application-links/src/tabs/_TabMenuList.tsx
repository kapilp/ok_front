import { JSX, Component, createMemo, splitProps, useContext } from 'solid-js';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import styles from './ApplicationTabs.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * The child tabs to be placed in the menu.
   */
  children?: JSX.Element;
  /**
   * Prop from popup, determines if the menu height is bound by the screen.
   */
  isHeightBounded?: boolean;
  /**
   * Prop from popup, determines if the menu width is bound by the screen.
   */
  isWidthBounded?: boolean;
  /**
   * Ref callback used by the popup for do positioning.
   */
  ref?: (el: HTMLElement) => void;
}
export const TabMenuList = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['children', 'isHeightBounded', 'isWidthBounded', 'ref']);
  const theme = useContext(ThemeContext);
  const listClass = createMemo(() => classNames(cx('tab-menu-list', { 'height-bounded': p.isHeightBounded }, { 'width-bounded': p.isWidthBounded }, theme.className)));
  return (
    <ul {...customProps} data-application-tab-menu-content className={listClass()} role="menu" ref={p.ref}>
      {p.children}
    </ul>
  );
};
