import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { MemoryRouter } from 'react-router-dom';
import ApplicationTabs from '../../../tabs/ApplicationTabs';
import testExternalConfig from '../common/testExternalConfig';
import demoStyles from './demoStyles.module.scss';
export default () => (
  <MemoryRouter initialEntries={testExternalConfig.map(link => link.path)} initialIndex={0}>
    <div className={demoStyles['demo-header']}>
      <ApplicationTabs id="test-tabs" alignment="end" links={testExternalConfig} />
    </div>
  </MemoryRouter>
);
