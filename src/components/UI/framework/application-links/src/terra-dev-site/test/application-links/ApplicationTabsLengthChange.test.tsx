import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { MemoryRouter } from 'react-router-dom';
import ApplicationTabs from '../../../tabs/ApplicationTabs';
import testShortConfig from '../common/testShortConfig';
import testLinkConfig from '../common/testLinkConfig';
import demoStyles from './demoStyles.module.scss';
type undefinedState = {
  shortLinks?: boolean;
};
export default class extends React.Component<{}, undefinedState> {
  constructor(props) {
    super(props);
    handleToggle = handleToggle.bind(this);
    state = {
      shortLinks: true,
    };
  }
  handleToggle() {
    setState(prevState => ({ shortLinks: !prevState.shortLinks }));
  }
  render() {
    return (
      <MemoryRouter initialEntries={testLinkConfig.map(link => link.path)} initialIndex={0}>
        <div>
          <div className={demoStyles['demo-header']}>
            <ApplicationTabs id="test-tabs" links={state.shortLinks ? testShortConfig : testLinkConfig} />
          </div>
          <button type="button" onClick={handleToggle}>
            Click to toggle link length
          </button>
        </div>
      </MemoryRouter>
    );
  }
}
