import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ApplicationTabs } from '../../../tabs/ApplicationTabs';
import { applicationLinkWithIconConfig } from '../common/ApplicationLinksWithIconsConfig';
import classNames from 'classnames/bind';
import styles from './demoStyles.module.scss';
const cx = classNames.bind(styles);
export const ApplicationTabsWithIconsExample = () => (
  <div className={cx(['demo-header', 'demo-header-with-icons'])}>
    <ApplicationTabs alignment="center" links={applicationLinkWithIconConfig} />
  </div>
);
