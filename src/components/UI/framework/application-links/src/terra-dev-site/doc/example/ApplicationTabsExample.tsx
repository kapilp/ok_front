import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ApplicationTabs } from '../../../tabs/ApplicationTabs';
import demoStyles from './demoStyles.module.scss';
import { applicationLinkConfig } from '../common/ApplicationLinkConfig';

export const ApplicationTabsExample = () => (
  <div className={demoStyles['demo-header']}>
    <ApplicationTabs alignment="center" links={applicationLinkConfig} />
  </div>
);
