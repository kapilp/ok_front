import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { SvgIconTrash } from '../../../../../../core/icon/src/icon/IconTrash';
import { SvgIconFolder } from '../../../../../../core/icon/src/icon/IconFolder';
import { SvgIconSearch } from '../../../../../../core/icon/src/icon/IconSearch';
import { SvgIconPrinter } from '../../../../../../core/icon/src/icon/IconPrinter';
import { SvgIconAllergy } from '../../../../../../core/icon/src/icon/IconAllergy';
import { SvgIconPadlock } from '../../../../../../core/icon/src/icon/IconPadlock';
import { SvgIconFeatured } from '../../../../../../core/icon/src/icon/IconFeatured';
export const applicationLinkWithIconConfig = [
  {
    path: '/item1',
    text: 'Item 1',
    icon: <SvgIconTrash />,
  },
  {
    path: '/item23',
    text: 'Item 23',
    icon: <SvgIconFolder />,
  },
  {
    path: '/item45',
    text: 'Item 45',
    icon: <SvgIconSearch />,
  },
  {
    path: '/item67',
    text: 'Very very very very long long item 67',
    icon: <SvgIconPrinter />,
  },
  {
    path: '/item89',
    text: 'Item 89',
    icon: <SvgIconAllergy />,
  },
  {
    path: '/item0',
    text: 'Item 0',
    icon: <SvgIconPadlock />,
  },
  {
    path: '/item11',
    text: 'Item 11',
    icon: <SvgIconFeatured />,
  },
  {
    path: '/item22',
    text: 'Super super super super very very long long item 22',
    icon: <SvgIconTrash />,
  },
  {
    path: '/item3',
    text: 'Item 3',
    icon: <SvgIconFolder />,
  },
  {
    path: '/item444',
    text: 'Item 444',
    icon: <SvgIconSearch />,
  },
  {
    path: '/item55',
    text: 'Item 55',
    icon: <SvgIconPrinter />,
  },
  {
    path: '/item6666',
    text: 'Item 6666',
    icon: <SvgIconAllergy />,
  },
];
