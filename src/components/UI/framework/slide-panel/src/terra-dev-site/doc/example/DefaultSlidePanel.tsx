import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SlidePanel } from '../../../SlidePanel';
import classNames from 'classnames/bind';
import styles from './DefaultSlidePanel.module.scss';

const cx = classNames.bind(styles);
const mainContentForSlidePanel = (togglePanelHandler: () => void) => (
  <div>
    <header className={cx('header-content')}>
      <h3>Main Content</h3>
      <button id="mainToggleBtn" type="button" onClick={togglePanelHandler} className={cx('custom-button')}>
        Main Toggle Panel
      </button>
    </header>
    <div className={cx('content-wrapper')}>
      <p>This is the main content area of the slide panel. The overall height of the SlidePanel is determined by the intrinsic height of the content in this container.</p>
      <p>
        {'Focus is moved to the toggle button in the panel container when the panel is opened via the componentDidUpdate lifecycle hook in '}
        <a href="https://github.com/cerner/terra-framework/blob/main/packages/terra-slide-panel/src/terra-dev-site/doc/example/DefaultSlidePanel.jsx">the example code</a>.
      </p>
      <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
        <li>Item 5</li>
        <li>Item 6</li>
        <li>Item 7</li>
        <li>Item 8</li>
      </ul>
    </div>
  </div>
);
const panelContentForSlidePanel = (togglePanelHandler: () => void, toggleFullscreenHandler: () => void) => () => (
  <div>
    <header className={cx('header-content')}>
      <h3 className={cx('heading')}>Panel Content</h3>
      <button id="panelToggleBtn" type="button" onClick={togglePanelHandler} className={cx('extension-button')}>
        Panel Toggle Panel
      </button>
      <button type="button" onClick={toggleFullscreenHandler} className={cx('extension-button')}>
        Toggle Fullscreen
      </button>
    </header>
    <div className={cx('content-wrapper')}>
      <p>This is the panel content area of the slide panel.</p>
      <p>
        {'Focus is moved to the toggle button in the main container when the panel is closed via the componentDidUpdate lifecycle hook in '}
        <a href="https://github.com/cerner/terra-framework/blob/main/packages/terra-slide-panel/src/terra-dev-site/doc/example/DefaultSlidePanel.jsx">the example code</a>.
      </p>
      <ul>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
        <li>Item 4</li>
        <li>Item 5</li>
        <li>Item 6</li>
        <li>Item 7</li>
        <li>Item 8</li>
        <li>Item 9</li>
        <li>Item 10</li>
        <li>Item 11</li>
        <li>Item 12</li>
        <li>Item 13</li>
        <li>Item 14</li>
        <li>Item 15</li>
      </ul>
    </div>
  </div>
);
interface IDefaultSlidePanelProps extends JSX.HTMLAttributes<Element> {
  panelBehavior?: 'overlay' | 'squish';
  panelPosition?: 'start' | 'end';
  panelSize?: 'small' | 'large';
  isFullscreen?: boolean;
  isOpen?: boolean;
  fill?: boolean;
}
type DefaultSlidePanelState = {
  panelIsOpen?: any;
  panelIsFullscreen?: any;
};
export const DefaultSlidePanel = (props: IDefaultSlidePanelProps) => {
  const [state, setState] = createStore({
    panelIsOpen: props.isOpen || false,
    panelIsFullscreen: props.isFullscreen || false,
  });

  const handlePanelToggle = () => {
    setState({
      panelIsOpen: !state.panelIsOpen,
      panelIsFullscreen: state.panelIsOpen, // to open in not fullscreen state
    });
  };
  const handleFullscreenToggle = () => {
    setState({ panelIsFullscreen: !state.panelIsFullscreen });
  };

  return (
    <div className={cx('container')}>
      <div className={cx('container-attributes')}>
        <SlidePanel
          mainContent={mainContentForSlidePanel(handlePanelToggle)}
          panelContent={panelContentForSlidePanel(handlePanelToggle, handleFullscreenToggle)}
          panelSize={props.panelSize}
          panelBehavior={props.panelBehavior}
          panelPosition={props.panelPosition}
          isOpen={state.panelIsOpen}
          isFullscreen={state.panelIsFullscreen}
          fill={props.fill}
        />
      </div>
    </div>
  );
};
