import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { SlidePanel } from '../../../SlidePanel';
import styles from './SlidePanelDocCommon.test.module.scss';
const cx = classNames.bind(styles);
type SlidePanelDemoState = {
  panelIsOpen?: boolean;
};
export const SlidePanelDemo = (props: {}) => {
  const [state, setState] = createStore({ panelIsOpen: false });

  const handlePanelToggle = () => setState({ panelIsOpen: !state.panelIsOpen });

  return (
    <div className={cx('content-wrapper-toggle')}>
      <SlidePanel
        id="test-slide"
        mainContent={
          <div className={cx('main-content')}>
            <button type="button" id="test-toggle" className={cx('button')} onClick={handlePanelToggle}>
              toggle
            </button>
          </div>
        }
        panelContent={
          <div id="panel-content" className={cx('panel-content')}>
            <button id="focus-button" type="button" className={cx('button')} onClick={handlePanelToggle}>
              Close panel
            </button>
          </div>
        }
        panelAriaLabel="Panel content area"
        mainAriaLabel="Main content area"
        panelSize="small"
        panelBehavior="overlay"
        isOpen={state.panelIsOpen}
        fill
      />
    </div>
  );
};
