import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import { SlidePanel, SlidePanelPositions } from '../../../SlidePanel';
import { SlidePanelMainContent } from '../common/SlidePanelMainContent';
import { SlidePanelPanelContent } from '../common/SlidePanelPanelContent';
export const SlidePanelSmall = () => (
  <SlidePanel
    mainContent={<SlidePanelMainContent />}
    panelContent={<SlidePanelPanelContent />}
    panelBehavior="overlay"
    panelPosition={SlidePanelPositions.END}
    panelSize="small"
    isOpen
  />
);
