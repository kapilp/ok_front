import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { SlidePanel } from '../../../SlidePanel';
import styles from './SlidePanelDocCommon.test.module.scss';
const cx = classNames.bind(styles);
export const SlidePanelSquishLarge = () => (
  <div className={cx('content-wrapper-large')}>
    <SlidePanel mainContent={<div className={cx('main-content')} />} panelContent={<div className={cx('panel-content')} />} panelBehavior="squish" panelSize="large" isOpen fill />
  </div>
);
