import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { SlidePanel } from '../../../SlidePanel';
import styles from './SlidePanelDocCommon.test.module.scss';
const cx = classNames.bind(styles);
const ButtonWrapper = () => {
  const [state, setState] = createStore({ count: 0 });
  const increaseCount = () => {
    setState({ count: state.count + 1 });
  };
  return (
    <div id="panel-content" className={cx('panel-content')}>
      <button id="focus-button" type="button" className={cx('button')} onClick={increaseCount}>
        Increase Count{' '}
      </button>
      {state.count}
    </div>
  );
};

export const SlidePanelSideDemo: Component = (props: {}) => {
  const [state, setState] = createStore({ panelHasStartPosition: false });

  const handlePanelToggle = () => setState({ panelHasStartPosition: !state.panelHasStartPosition });

  return (
    <div className={cx('content-wrapper-toggle')}>
      <SlidePanel
        id="test-slide"
        mainContent={
          <div className={cx('main-content')}>
            <button type="button" id="test-toggle" className={cx('button')} onClick={handlePanelToggle}>
              toggle
            </button>
          </div>
        }
        panelContent={<ButtonWrapper />}
        panelAriaLabel="Panel content area"
        mainAriaLabel="Main content area"
        panelSize="small"
        panelBehavior="squish"
        panelPosition={state.panelHasStartPosition ? 'start' : 'end'}
        isOpen
        fill
      />
    </div>
  );
};
