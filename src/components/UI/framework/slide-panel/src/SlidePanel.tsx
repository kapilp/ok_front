import {
  Component, createEffect,
  createStore,
  Dynamic,
  mergeProps,
  Show,
  splitProps,
  useContext
} from 'solid-js';
import classNames from "classnames";
import classNamesBind from "classnames/bind";

import styles from "./SlidePanel.module.scss";
import {ThemeContext} from "../../theme-context/src/ThemeContext";
const cx = classNamesBind.bind(styles);
export enum SlidePanelPositions  {
  START= "start",
  END= "end"
};
interface ISlidePanelProps extends JSX.HTMLAttributes<Element> {
  /**
   * String that labels the Panel content area for screen readers.
   */
  panelAriaLabel?:string,
  /**
   * String that labels the Main content area for screen readers.
   */
  mainAriaLabel?:string,
  /**
   * The component to display in the main content area.
   */
  mainContent?:JSX.Element,
  /**
   * The component to display in the panel content area.
   */
  panelContent?:JSX.Element,
  /**
   * The style of panel presentation. One of `overlay`, `squish`.
   */
  panelBehavior?:"overlay"| "squish",
  /**
   * The position at which the panel will be displayed. This property honors the current direction setting. One of `start`, `end`.
   */
  panelPosition?:SlidePanelPositions
  /**
   * The size at which the panel will be displayed. One of `small`, `large`.
   */
  panelSize?:"small"| "large",
  /**
   * Whether or not, when open, the panel should be displayed with the full width of the SlidePanel.
   */
  isFullscreen?:boolean,
  /**
   * Whether or not the panel should be displayed.
   */
  isOpen?:boolean,
  /**
   * Whether or not the SlidePanel should be sized relative to its parent container.
   */
  fill?:boolean
};
const defaultProps = {
  panelBehavior: "overlay",
  panelPosition: SlidePanelPositions.END,
  panelSize: "small"
};

export const SlidePanel: Component = (props: ISlidePanelProps) => {

  let mainNode: HTMLDivElement;
  let panelNode: HTMLDivElement;
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);

  createEffect(() => {
    if (props.isOpen) {
      panelNode && panelNode.focus();
    } else if (!props.isOpen) {
      mainNode && mainNode.focus();
    }
  })
  const setPanelNode = (el: HTMLDivElement) => {
    panelNode = el;
  }
  const [extracted, customProps] = splitProps(props, [
      "panelAriaLabel",
      "mainAriaLabel",
      "mainContent",
      "panelContent",
      "panelBehavior",
      "panelPosition",
      "panelSize",
      "isFullscreen",
      "isOpen",
      "fill"
  ]);


    const panelDiv = (
      <div
        className={cx(["panel"])}
        key="panel"
        tabIndex="-1"
        aria-label={props.panelAriaLabel}
        aria-hidden={!props.isOpen ? "true" : "false"}
        ref={setPanelNode}
      >
        {props.panelContent}
      </div>
    );
    const mainDiv = (
      <div
        className={cx("main")}
        key="main"
        tabIndex="-1"
        aria-label={props.mainAriaLabel}
        ref={mainNode}
      >
        {props.mainContent}
      </div>
    );
    return (
      <div
        {...customProps}
        className={classNames(
          cx(
            "slide-panel",
            { "is-open": props.isOpen },
            { "is-fullscreen": props.isFullscreen },
            { fill: props.fill },
            theme.className
          ),
          customProps.className
        )}
        data-slide-panel-panel-behavior={props.panelBehavior}
        data-slide-panel-panel-position={props.panelPosition}
        data-slide-panel-panel-size={props.panelSize}
      >
        {props.panelPosition === SlidePanelPositions.START ? (
          <>
            {panelDiv}
            {mainDiv}
          </>
        ) : (
          <>
            {mainDiv}
            {panelDiv}
          </>
        )}
      </div>
    );

}


