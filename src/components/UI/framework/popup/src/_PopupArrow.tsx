import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import styles from './PopupArrow.module.scss';
const cx = classNamesBind.bind(styles);
/**
 * Half the base of the arrow, to use for arrow positioning offset.
 */
export const ARROW_OFFSET = 11;
/**
 * Directional attribute to be applied by a presenting component.
 */
export const ARROW_ATTR = 'data-align-arrow';
interface Properties {
  /**
   * The function returning the frame html reference.
   */
  ref: (el: HTMLDivElement) => void;
}
export const PopupArrow = (props: Properties) => {
  const [p, customProps] = splitProps(props, ['ref']);
  const theme = useContext(ThemeContext);

  return <div {...customProps} className={classNames(cx('popup-arrow', theme.className), customProps.className)} ref={p.ref} />;
};
PopupArrow.Opts = {
  arrowSize: ARROW_OFFSET,
  positionAttr: ARROW_ATTR,
};
