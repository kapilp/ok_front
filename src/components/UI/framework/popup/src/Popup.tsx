import {  JSX, Component, createMemo, createSignal, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Portal } from 'solid-js/web';
import { PopupContent, CORNER_SIZE } from './_PopupContent';
import { PopupArrow } from './_PopupArrow';
import { PopupOverlay } from './_PopupOverlay';
import PopupUtils from './_PopupUtils';
import { PopupHeights } from './PopupHeights';
import { PopupWidths } from './PopupWidths';
import { Hookshot } from '../../hookshot/src/Hookshot';
const ATTACHMENT_POSITIONS = ['top left', 'top center', 'top right', 'middle left', 'middle center', 'middle right', 'bottom left', 'bottom center', 'bottom right'] as const;

interface IPopupProps extends JSX.HTMLAttributes<Element> {
  /**
   * If the primary attachment is not available, how should the content be positioned. Options
   * include 'auto', 'flip', or 'push'.
   */
  attachmentBehavior?: 'auto' | 'flip' | 'push';
  /**
   * The children to be displayed as content within the popup.
   */
  children: JSX.Element;
  /**
   * Callback function indicating a close condition was met, should be combined with isOpen for state management.
   */
  onRequestClose: () => void;
  /**
   * Target element for the popup to anchor to.
   */
  targetRef: () => HTMLElement;
  /**
   * Bounding container for the popup, will use window if no value provided.
   */
  boundingRef?: () => HTMLElement;
  /**
   * @private CSS classnames that are append to the arrow.
   */
  classNameArrow?: string;
  /**
   * @private CSS classnames that are append to the popup content inner.
   */
  classNameContent?: string;
  /**
   * @private CSS classnames that are append to the overlay.
   */
  classNameOverlay?: string;
  /**
   * Attachment point for the popup, this will be mirrored to the target. Options include?: 'top left',
   * 'top center', 'top right', 'middle left', 'middle center', 'middle right', 'bottom left',
   * 'bottom center', or 'bottom right'.
   */
  contentAttachment?: typeof ATTACHMENT_POSITIONS;
  /**
   * A string representation of the height in px, limited to:
   * 40, 80, 120, 160, 240, 320, 400, 480, 560, 640, 720, 800, 880 or auto.
   */
  contentHeight?: '40' | '80' | '120' | '160' | '240' | '320' | '400' | '480' | '560' | '640' | '720' | '800' | '880' | 'auto';

  /**
   * A string representation of the width in px, limited to:
   * 160, 240, 320, 640, 960, 1280, 1760 or auto.
   */
  contentWidth?: '160' | '240' | '320' | '640' | '960' | '1280' | '1760' | 'auto';

  /**
   * Should an arrow be placed at the attachment point.
   */
  isArrowDisplayed?: boolean;
  /**
   * Set this to `true` if your content has focusable elements and you want them to receive focus instead of focusing on the default popup frame when the popup is opened.
   */
  isContentFocusDisabled?: boolean;
  /**
   * Should the default behavior, that inserts a header when constraints are breached, be disabled.
   */
  isHeaderDisabled?: boolean;
  /**
   * Should the popup be presented as open.
   */
  isOpen?: boolean;
  /**
   * Attachment point for the target. Options include: 'top left', 'top center', 'top right', 'middle left', 'middle center',
   * 'middle right', 'bottom left', 'bottom center', or 'bottom right'.
   */
  targetAttachment?: typeof ATTACHMENT_POSITIONS;
  /**
   * @private
   * Prop to set role on popup content container
   */
  popupContentRole?: string;
}
const defaultProps = {
  attachmentBehavior: 'auto',
  boundingRef: null,
  classNameArrow: null,
  classNameContent: null,
  classNameOverlay: null,
  contentAttachment: 'top center',
  contentHeight: '80',
  contentWidth: '240',
  isArrowDisplayed: false,
  isContentFocusDisabled: false,
  isHeaderDisabled: false,
  isOpen: false,
  popupContentRole: 'dialog',
};

export const Popup = (props: IPopupProps) => {
  props = mergeProps({}, defaultProps, props);
  const [arrowNode, setArrowNode] = createSignal<HTMLDivElement>();
  let contentHeight = PopupHeights[props.contentHeight!];
  let contentWidth = PopupWidths[props.contentWidth!];

  const [isContentSized, setIsContentSized] = createSignal(props.contentHeight !== 'auto' && props.contentWidth !== 'auto');
  let windowWidth: number;

  // Todo fix this:
  /*shouldComponentUpdate(nextProps) {
    setIsContentSized(nextProps.contentHeight !== "auto" && nextProps.contentWidth !== "auto");
    contentHeight = PopupHeights[nextProps.contentHeight];
    contentWidth = PopupWidths[nextProps.contentWidth];
    return true;
  }*/
  const setArrowPosition = (contentPosition, targetPosition) => {
    const arrowPosition = PopupUtils.getArrowPosition(contentPosition, targetPosition, PopupArrow.Opts.arrowSize, CORNER_SIZE);
    if (!arrowPosition) {
      arrowNode().removeAttribute(PopupArrow.Opts.positionAttr);
      return;
    }
    arrowNode().setAttribute(PopupArrow.Opts.positionAttr, arrowPosition);
    if (arrowPosition === 'top' || arrowPosition === 'bottom') {
      arrowNode().style.left = PopupUtils.leftOffset(contentPosition, targetPosition, PopupArrow.Opts.arrowSize, CORNER_SIZE);
      arrowNode().style.top = '';
    } else {
      arrowNode().style.left = '';
      arrowNode().style.top = PopupUtils.topOffset(contentPosition, targetPosition, PopupArrow.Opts.arrowSize, CORNER_SIZE);
    }
  };

  const handleOnPosition = (event: Event, positions) => {
    if (arrowNode()) {
      setArrowPosition(positions.content, positions.target);
    }
  };
  const handleOnContentResize = () => {
    setIsContentSized(props.contentHeight !== 'auto' && props.contentWidth !== 'auto');
    contentHeight = PopupHeights[props.contentHeight!];
    contentWidth = PopupWidths[props.contentWidth!];
    //forceUpdate();
  };
  const handleOnResize = (event: Event, width: number) => {
    // Close the popup if the window width is resized.
    if (window.innerWidth !== width) {
      windowWidth = window.innerWidth;
      props.onRequestClose();
    } else {
      setIsContentSized(props.contentHeight !== 'auto' && props.contentWidth !== 'auto');
      contentHeight = PopupHeights[props.contentHeight!];
      contentWidth = PopupWidths[props.contentWidth!];
      //forceUpdate();
    }
  };
  const validateContentNode = (el: HTMLDivElement) => {
    if (el) {
      const contentRect = Hookshot.Utils.getBounds(el);
      if (contentHeight !== contentRect.height || contentWidth !== contentRect.width) {
        contentHeight = contentRect.height;
        contentWidth = contentRect.width;
        //forceUpdate();
      }
      setIsContentSized(true);
    }
  };
  const showArrow = createMemo(() => props.isArrowDisplayed && props.contentAttachment !== 'middle center');
  const createPopupContent = (p: { ref: (el: HTMLDivElement) => void }) => {
    const boundProps = createMemo(() => {
      const boundingFrame = props.boundingRef ? props.boundingRef() : undefined;
      const boundsProps = {
        contentHeight: PopupHeights[props.contentHeight] || PopupHeights['80'],
        contentWidth: PopupWidths[props.contentWidth] || PopupWidths['240'],
      };
      if (boundsProps.contentHeight <= 0 && contentHeight) {
        boundsProps.contentHeight = contentHeight;
      }
      if (boundsProps.contentWidth <= 0 && contentWidth) {
        boundsProps.contentWidth = contentWidth;
      }
      if (boundingFrame) {
        boundsProps.contentHeightMax = boundingFrame.clientHeight;
        boundsProps.contentWidthMax = boundingFrame.clientWidth;
      } else {
        boundsProps.contentHeightMax = window.innerHeight;
        boundsProps.contentWidthMax = window.innerWidth;
      }
      return boundsProps;
    });
    const arrow = <>{showArrow() && <PopupArrow className={props.classNameArrow} ref={setArrowNode} />}</>;
    return (
      <PopupContent
        {...boundProps()}
        arrow={arrow}
        classNameInner={props.classNameContent}
        isHeaderDisabled={props.isHeaderDisabled}
        onRequestClose={props.onRequestClose}
        onContentResize={handleOnContentResize}
        onResize={handleOnResize}
        popupContentRole={props.popupContentRole}
        ref={(el: HTMLDivElement) => {
          setTimeout(() => {
            validateContentNode(el);
            p.ref(el);
          });
        }}
        isHeightAutomatic={props.contentHeight === 'auto'}
        isWidthAutomatic={props.contentWidth === 'auto'}
        isFocusedDisabled={props.isContentFocusDisabled}
      >
        {props.children}
      </PopupContent>
    );
  };
  const [p, customProps] = splitProps(props, [
    'attachmentBehavior',
    'boundingRef',
    'children',
    'classNameArrow',
    'classNameContent',
    'classNameOverlay',
    'contentAttachment',
    'contentHeight',
    'contentWidth',
    'isArrowDisplayed',
    'isContentFocusDisabled',
    'isHeaderDisabled',
    'isOpen',
    'onRequestClose',
    'targetRef',
    'targetAttachment',
  ]);

  const cAttachment = () => PopupUtils.parseAttachment(p.contentAttachment);

  function getTargetAttachment() {
    let tAttachment;
    if (p.targetAttachment) {
      tAttachment = PopupUtils.parseAttachment(p.targetAttachment);
    } else {
      tAttachment = PopupUtils.mirrorAttachment(cAttachment());
    }
    return tAttachment;
  }

  function getContentOffset() {
    let cOffset;
    if (showArrow()) {
      cOffset = PopupUtils.getContentOffset(cAttachment(), getTargetAttachment(), props.targetRef(), PopupArrow.Opts.arrowSize, CORNER_SIZE);
    }
    return cOffset;
  }
  return (
    <>
      {p.isOpen && (
        <>
          <Portal>
            <PopupOverlay className={props.classNameOverlay} onRequestClose={props.onRequestClose} />
          </Portal>
          <Hookshot
            attachmentBehavior={p.attachmentBehavior}
            attachmentMargin={showArrow() ? PopupArrow.Opts.arrowSize : 0}
            boundingRef={p.boundingRef}
            contentAttachment={cAttachment()}
            contentOffset={getContentOffset()}
            isEnabled={isContentSized()}
            isOpen={p.isOpen}
            onPosition={handleOnPosition}
            targetRef={p.targetRef}
            targetAttachment={getTargetAttachment()}
          >
            {createPopupContent}
          </Hookshot>
        </>
      )}
    </>
  );
};
//isOpened={isOpen}
Popup.Opts = {
  attachmentPositions: ATTACHMENT_POSITIONS,
  heights: PopupHeights,
  widths: PopupWidths,
};
