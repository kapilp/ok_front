import {  JSX, Component, onCleanup, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './PopupOverlay.module.scss';
const cx = classNamesBind.bind(styles);
interface IPopupOverlayProps extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * @private
   * Callback function indicating a close condition was met.
   */
  onRequestClose?: () => void;
  /**
   * The string representation of the index.
   */
  children?: JSX.Element;
}
const defaultProps = {
  children: [],
};
export const PopupOverlay: Component = (props: IPopupOverlayProps) => {
  props = mergeProps({}, defaultProps, props);

  let overlayStyle: string;

  //const componentDidMount = () => {
  // Disable scrolling on the page when Overlay is displayed
  overlayStyle = document.documentElement.style.overflow;
  document.documentElement.style.overflow = 'hidden';
  //}
  onCleanup(() => {
    // Enable scrolling on the page since Overlay has gone away
    document.documentElement.style.overflow = overlayStyle;
  });
  const handleOnClick = (event: Event) => {
    event.stopPropagation();
    if (props.onRequestClose) {
      props.onRequestClose(event);
    }
  };
  const [p, customProps] = splitProps(props, [
    'children',
    // Delete the closePortal prop that comes from react-portal.
    'closePortal',
    // Delete onRequestClose prop we use to close popup.
    'onRequestClose',
  ]);
  const theme = useContext(ThemeContext);
  return (
    <>
      <div onClick={handleOnClick} {...customProps} className={classNames(cx('popup-overlay', theme.className), customProps.className)}>
        <div className={cx('inner')} />
        {p.children}
      </div>
    </>
  );
};
