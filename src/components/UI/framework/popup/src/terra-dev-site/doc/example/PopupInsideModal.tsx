import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ModalManager from 'terra-modal-manager';
import ModalContainer from 'terra-popup/lib/terra-dev-site/doc/example/PopupModalContainer';
export function PopupInsideModal() {
  return (
    <ModalManager>
      <ModalContainer />
    </ModalManager>
  );
}
