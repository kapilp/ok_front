import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
export function PopupStandard() {
  let buttonElement;
  const [open, setOpen] = createSignal(false);
  const setButtonNode = node => {
    buttonElement = node;
  };
  const getButtonNode = () => buttonElement;
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Button text="Default Popup" onClick={handleButtonClick} ref={setButtonNode} />
      <Popup isOpen={open()} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
        <Placeholder title="Popup Content" />
      </Popup>
    </>
  );
}
