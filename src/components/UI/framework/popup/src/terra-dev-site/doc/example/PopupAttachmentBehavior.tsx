import {  JSX, mergeProps, splitProps, useContext, createSignal  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
import classNames from 'classnames/bind';
import styles from './PopupAttachmentBehavior.module.scss';

const cx = classNames.bind(styles);
export function PopupAttachmentBehavior() {
  let buttonElement: HTMLButtonElement;
  const [open, setOpen] = createSignal(false);
  const [contentBehavior, setContentBehavior] = createSignal('auto' as 'auto' | 'flip' | 'push');
  const setButtonNode = (el: HTMLButtonElement) => {
    buttonElement = el;
  };
  const getButtonNode = () => buttonElement;
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  const handleChange = (event: Event) => {
    setContentBehavior(event.target.value);
  };
  return (
    <>
      <div>
        <label htmlFor="popup-behavior">Attachment Behavior</label>
        <select id="popup-behavior" onChange={handleChange} value={contentBehavior()} className={cx('selection-list')}>
          <option value="auto">Auto</option>
          <option value="flip">Flip</option>
          <option value="push">Push</option>
        </select>
        <Button className={cx('popup-wrapper')} text="Open Popup" onClick={handleButtonClick} ref={setButtonNode} />
      </div>
      <Popup attachmentBehavior={contentBehavior()} contentAttachment="top center" isOpen={open()} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
        <Placeholder title="Popup Content" />
      </Popup>
    </>
  );
}
