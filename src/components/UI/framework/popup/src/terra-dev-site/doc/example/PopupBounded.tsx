import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
import classNames from 'classnames/bind';
import styles from './PopupDocCommon.module.scss';
const cx = classNames.bind(styles);
export function PopupBounded() {
  let buttonElement;
  let parentElement;
  const [open, setOpen] = createSignal(false);
  const setParentNode = node => {
    parentElement = node;
  };
  const getParentNode = () => parentElement;
  const setButtonNode = node => {
    buttonElement = node;
  };
  const getButtonNode = () => buttonElement;
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div className={cx('content-wrapper')} ref={setParentNode}>
        <Button text="Bounded Popup" onClick={handleButtonClick} ref={setButtonNode} />
        <Popup
          boundingRef={getParentNode}
          classNameArrow="test-arrow"
          classNameContent="test-content"
          contentHeight="240"
          contentWidth="320"
          isOpen={open()}
          onRequestClose={handleRequestClose}
          targetRef={getButtonNode}
          isContentFocusDisabled
        >
          <Placeholder title="Popup Content" />
        </Popup>
      </div>
    </>
  );
}
