import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';

import classNames from 'classnames/bind';
import styles from './PopupModalContent.module.scss';
const cx = classNames.bind(styles);
type ModalContainerProps = {
  disclosureManager?: any;
};
const ModalContainer: React.SFC<ModalContainerProps> = props => {
  const [open, setOpen] = useState(false);
  const handlePopupButtonClick = () => {
    setOpen(true);
  };
  const handlePopupRequestClose = () => {
    setOpen(false);
  };
  const { disclosureManager } = props;
  return (
    <>
      <div className={cx('content-container')}>
        <Popup isArrowDisplayed isOpen={open} onRequestClose={handlePopupRequestClose} targetRef={() => document.getElementById('popup-in-modal')}>
          <Placeholder title="Popup Content" />
        </Popup>
        <Button id="popup-in-modal" text="Popup In Modal" onClick={handlePopupButtonClick} />
        <br />
        <br />
        <Button className="close-disclosure" text="Close Disclosure" onClick={disclosureManager.closeDisclosure} />
      </div>
    </>
  );
};
export default withDisclosureManager(ModalContainer);
