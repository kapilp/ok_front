import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
import classNames from 'classnames/bind';
import styles from './PopupAttachments.module.scss';
const cx = classNames.bind(styles);
const ATTACHMENT_POSITIONS = ['top left', 'top center', 'top right', 'middle left', 'middle center', 'middle right', 'bottom left', 'bottom center', 'bottom right'];
const generateOptions = values =>
  values.map((currentValue, index) => {
    const keyValue = index;
    return (
      <option key={keyValue} value={currentValue}>
        {currentValue}
      </option>
    );
  });
export function PopupAttachment() {
  let buttonElement: HTMLButtonElement;
  const [open, setOpen] = createSignal(false);
  const [contentAttachment, setContentAttachment] = createSignal(ATTACHMENT_POSITIONS[0]);
  const setButtonNode = node => {
    buttonElement = node;
  };
  const getButtonNode = () => buttonElement;
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  const handleContentAttachmentChange = event => {
    setContentAttachment(event.target.value);
  };
  return (
    <>
      <div>
        <select id="ContentAttachment" name="popupContentAttachment" onChange={handleContentAttachmentChange}>
          {generateOptions(ATTACHMENT_POSITIONS)}
        </select>
        <Button className={cx('popup-wrapper')} text="Open Popup" onClick={handleButtonClick} ref={setButtonNode} />
      </div>
      <Popup attachmentBehavior="auto" contentAttachment={contentAttachment()} isOpen={open()} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
        <Placeholder title="Popup Content" />
      </Popup>
    </>
  );
}
``;
