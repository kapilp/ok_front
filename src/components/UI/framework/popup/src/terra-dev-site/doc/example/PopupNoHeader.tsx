import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
import { ContentContainer } from '../../../../../../core/content-container/src/ContentContainer';
import classNames from 'classnames/bind';
import styles from './PopupDocCommon.module.scss';
const cx = classNames.bind(styles);
/* eslint-disable */
const PopupContent = (props: { closeButtonRequired: boolean; handleRequestClose: () => void }) => {
  //todo: _PopupContent.tsx automatically set closeButtonRequired to true:
  const placeHolder = <Placeholder title="Popup Content" />;
  return (
    <>
      {true /*{props.closeButtonRequired (*/ ? (
        <ContentContainer header={<Button text="My Custom Close Button" isBlock onClick={props.handleRequestClose} />} fill>
          {placeHolder}
        </ContentContainer>
      ) : (
        placeHolder
      )}
    </>
  );
};
/* eslint-enable */
export function PopupNoHeader() {
  let parentElement;
  const [open, setOpen] = createSignal(false);
  const setParentNode = node => {
    parentElement = node;
  };
  const getParentNode = () => parentElement;
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  return (
    <>
      <div className={cx('content-wrapper')} ref={setParentNode}>
        <Popup
          boundingRef={getParentNode}
          contentHeight="240"
          contentWidth="320"
          isHeaderDisabled
          isOpen={open()}
          onRequestClose={handleRequestClose}
          targetRef={() => document.getElementById('popup-no-header')}
          isContentFocusDisabled
        >
          <PopupContent title="Popup Content" handleRequestClose={handleRequestClose} />
        </Popup>
        <Button id="popup-no-header" text="No Header Popup" onClick={handleButtonClick} />
      </div>
    </>
  );
}
