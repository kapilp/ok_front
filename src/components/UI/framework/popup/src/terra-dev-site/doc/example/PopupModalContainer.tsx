import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { Button } from '../../../../../../core/button/src/Button';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import PopupModalContent from './PopupModalContent';
type ModalContainerProps = {
  disclosureManager?: any;
};
const ModalContainer: React.SFC<ModalContainerProps> = props => {
  const disclose = () => {
    props.disclosureManager.disclose({
      preferredType: 'modal',
      size: '',
      content: {
        key: 'PopupModalContent',
        component: <PopupModalContent />,
      },
    });
  };
  return <Button className="disclose" text="Disclose Modal" onClick={disclose} />;
};
export default withDisclosureManager(ModalContainer);
