import {  JSX, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Placeholder } from '../../../../../../core/doc-template/src/Placeholder';
import { Popup } from '../../../Popup';
import { Button } from '../../../../../../core/button/src/Button';
const HEIGHT_KEYS = Object.keys(Popup.Opts.heights);
const WIDTH_KEYS = Object.keys(Popup.Opts.widths);
export function PopupDimensions() {
  const [open, setOpen] = createSignal(false);
  const [popupContentHeight, setPopupContentHeight] = createSignal('Default');
  const [popupContentWidth, setPopupContentWidth] = createSignal('Default');
  /* eslint-disable */
  const PopupContent = (props: { contentStyle: {}; isHeightBounded: boolean; isWidthBounded: boolean }) => {
    function getTitle() {
      let title = 'Popup Content';
      if (props.isHeightBounded) {
        title += ' HeightBounded';
      }
      if (props.isWidthBounded) {
        title += ' WidthBounded';
      }
      return title;
    }

    return <Placeholder title={getTitle()} style={props.contentStyle} />;
  };
  /* eslint-enable */
  const generateOptions = values =>
    values.map((currentValue, index) => {
      const keyValue = index;
      return (
        <option key={keyValue} value={currentValue}>
          {currentValue}
        </option>
      );
    });
  const handleButtonClick = () => {
    setOpen(true);
  };
  const handleRequestClose = () => {
    setOpen(false);
  };
  const handleSelectHeightChange = event => {
    setPopupContentHeight(event.target.value);
  };
  const handleSelectWidthChange = event => {
    setPopupContentWidth(event.target.value);
  };

  function getContentDimensions() {
    const contentDimensions = { contentHeight: popupContentHeight(), contentWidth: popupContentWidth() };
    if (popupContentHeight() !== 'Default') {
      contentDimensions.contentHeight = popupContentHeight();
    }
    if (popupContentWidth() !== 'Default') {
      contentDimensions.contentWidth = popupContentWidth();
    }
    return contentDimensions;
  }

  function getContentStyle() {
    const contentStyle = {};
    if (popupContentHeight() === 'auto') {
      contentStyle.height = '500px';
    }
    if (popupContentWidth() === 'auto') {
      contentStyle.width = '500px';
    }
    return contentStyle;
  }
  return (
    <div>
      <label htmlFor="popupContentHeight">Pop Content Height</label>
      <select id="popupContentHeight" name="popupContentHeight" value={popupContentHeight()} onChange={handleSelectHeightChange}>
        <option value="Default">Default</option>
        {generateOptions(HEIGHT_KEYS)}
      </select>
      <br />
      <br />
      <label htmlFor="popupContentWidth">Pop Content Width</label>
      <select id="popupContentWidth" name="popupContentWidth" value={popupContentWidth()} onChange={handleSelectWidthChange}>
        <option value="Default">Default</option>
        {generateOptions(WIDTH_KEYS)}
      </select>
      <br />
      <br />
      <div>
        <Popup
          {...getContentDimensions()}
          classNameContent="test-content"
          isOpen={open()}
          onRequestClose={handleRequestClose}
          targetRef={() => document.getElementById('popup-dimensions')}
        >
          <PopupContent contentStyle={getContentStyle()} />
        </Popup>
        <Button id="popup-dimensions" text={`${popupContentHeight() || 'Default'} x ${popupContentWidth() || 'Default'} Popup`} onClick={handleButtonClick} />
      </div>
    </div>
  );
}
