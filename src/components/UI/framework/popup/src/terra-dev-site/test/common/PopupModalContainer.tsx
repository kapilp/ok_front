import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from "terra-button";
import {
  withDisclosureManager,
  disclosureManagerShape
} from "terra-disclosure-manager";
import PopupModalContent from "./PopupModalContent";
interface IModalContainerProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
  disclose?: any;
}
export const ModalContainer: Component = (props:IModalContainerProps) => {
  constructor(props) {
    super(props);
    disclose = disclose.bind(this);
  }
  disclose() {
    props.disclosureManager.disclose({
      preferredType: "modal",
      size: "",
      content: {
        key: "PopupModalContent",
        component: <PopupModalContent />
      }
    });
  }
  render() {
    return (
      <Button
        className="disclose"
        text="Disclose Modal"
        onClick={disclose}
      />
    );
  }
}
export default withDisclosureManager(ModalContainer);
