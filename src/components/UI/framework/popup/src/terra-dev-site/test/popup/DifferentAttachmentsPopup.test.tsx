/* eslint-disable react/jsx-curly-brace-presence */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './DifferentAttachmentsPopup.test.module.scss';
const cx = classNames.bind(styles);
const targetOptions = {
  'bottom left': 'top right',
  'bottom right': 'top left',
};
type AlignmentPopupState = {
  open?: boolean;
  contentAttachment?: string;
  targetAttachment?: string;
};
// This tests verifies the PopupUtils.getContentOffset methed when (cAttachment.horizontal !== tAttachment.horizontal)
class AlignmentPopup extends React.Component<{}, AlignmentPopupState> {
  buttonNode: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleAttachment = handleAttachment.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = {
      open: false,
      contentAttachment: 'bottom left',
      targetAttachment: 'top right',
    };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleAttachment(event) {
    setState({
      contentAttachment: event.target.value,
      targetAttachment: targetOptions[event.target.value],
      open: true,
    });
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
          <Popup
            boundingRef={getParentNode}
            classNameArrow="test-arrow"
            classNameContent="test-content"
            contentAttachment={state.contentAttachment}
            targetAttachment={state.targetAttachment}
            contentHeight="80"
            contentWidth="160"
            isArrowDisplayed
            isOpen={state.open}
            onRequestClose={handleRequestClose}
            targetRef={getButtonNode}
          >
            <p className={cx('popup-text')}>This popup arrow has vertical attachment.</p>
          </Popup>
          <button type="button" id="alignment-button" className={cx('popup-button')} onClick={handleButtonClick} ref={setButtonNode}>
            {''}
          </button>
        </div>
        <p>Choose Content Attachment. It will flip the target attachment.</p>
        <button type="button" id="attach-Left" value="bottom left" onClick={handleAttachment}>
          Attach Bottom Left
        </button>
        <button type="button" id="attach-Right" value="bottom right" onClick={handleAttachment}>
          Attach Bottom Right
        </button>
      </div>
    );
  }
}
export default AlignmentPopup;
