import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './AutomaticHeightBoundedPopup.test.module.scss';
const cx = classNames.bind(styles);
type PopupExampleState = {
  open?: boolean;
};
class PopupExample extends React.Component<{}, PopupExampleState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
        <Popup
          boundingRef={getParentNode}
          classNameArrow="test-arrow"
          classNameContent="test-content"
          contentHeight="auto"
          isOpen={state.open}
          targetRef={getButtonNode}
          onRequestClose={handleRequestClose}
        >
          <p className={cx('popup-text')}>This is popup content with a automatic height of 400px.</p>
        </Popup>
        <button type="button" id="default-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Popup
        </button>
      </div>
    );
  }
}
export default PopupExample;
