import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
interface IBoundedNoHeaderContentProps extends JSX.HTMLAttributes<Element> {
  closeButtonRequired?: boolean;
}
class BoundedNoHeaderContent extends React.Component<IBoundedNoHeaderContentProps, {}> {
  render() {
    const content = props.closeButtonRequired ? <p>There is no Header and popup is Bounded</p> : <p>This is the content of the popup</p>;
    return content;
  }
}
BoundedNoHeaderContent.defaultProps = {
  closeButtonRequired: false,
};
export default BoundedNoHeaderContent;
