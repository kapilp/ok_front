import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './ArrowPopup.test.module.scss';
const cx = classNames.bind(styles);
type ArrowPopupState = {
  open?: boolean;
};
class ArrowPopup extends React.Component<{}, ArrowPopupState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')}>
        <Popup
          classNameArrow="test-arrow"
          classNameContent="test-content"
          contentAttachment="middle right"
          isArrowDisplayed
          isOpen={state.open}
          onRequestClose={handleRequestClose}
          targetRef={getButtonNode}
        >
          <p>this is popup content</p>
        </Popup>
        <button type="button" id="arrow-button" className={cx('arrow-button')} onClick={handleButtonClick} ref={setButtonNode}>
          Arrow Popup
        </button>
      </div>
    );
  }
}
export default ArrowPopup;
