import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './BoundedHeightPopup.test.module.scss';
const cx = classNames.bind(styles);
type BoundedPopupState = {
  open?: boolean;
};
class BoundedPopup extends React.Component<{}, BoundedPopupState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
        <Popup
          boundingRef={getParentNode}
          contentHeight="480"
          classNameArrow="test-arrow"
          classNameContent="test-content"
          isOpen={state.open}
          onRequestClose={handleRequestClose}
          targetRef={getButtonNode}
        >
          <p className={cx('popup-text')}>This popup is bounded by height.</p>
        </Popup>
        <button type="button" id="bounded-button" onClick={handleButtonClick} ref={setButtonNode}>
          Bounded Height
        </button>
      </div>
    );
  }
}
export default BoundedPopup;
