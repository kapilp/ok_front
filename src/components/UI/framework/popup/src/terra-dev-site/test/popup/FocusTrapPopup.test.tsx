import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './PopupTypeCommon.test.module.scss';
const cx = classNames.bind(styles);
type DefaultPopupState = {
  open?: boolean;
};
class DefaultPopup extends React.Component<{}, DefaultPopupState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')}>
        <Popup classNameArrow="test-arrow" classNameContent="test-content" isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <p>this is popup content</p>
          <button id="button1" type="button">
            Test button 1
          </button>
          <button id="button2" type="button">
            Test button 2
          </button>
        </Popup>
        <button type="button" id="default-button" className={cx('popup-button')} onClick={handleButtonClick} ref={setButtonNode}>
          Default Popup
        </button>
      </div>
    );
  }
}
export default DefaultPopup;
