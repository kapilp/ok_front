import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ModalManager from 'terra-modal-manager';
import ModalContainer from '../common/PopupModalContainer';
const ModalManagerDemo = () => (
  <ModalManager>
    <ModalContainer />
  </ModalManager>
);
export default ModalManagerDemo;
