import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import classNames from 'classnames/bind';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import Popup from '../../../Popup';
import styles from './PopupModalContent.module.scss';
const cx = classNames.bind(styles);
interface IModalContentProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
}
type ModalContentState = {
  open?: boolean;
};
class ModalContent extends React.Component<IModalContentProps, ModalContentState> {
  constructor(props) {
    super(props);
    handlePopupButtonClick = handlePopupButtonClick.bind(this);
    handlePopupRequestClose = handlePopupRequestClose.bind(this);
    handlePopupOnChange = handlePopupOnChange.bind(this);
    state = { open: false };
  }
  handlePopupButtonClick() {
    setState({ open: true });
  }
  handlePopupRequestClose() {
    setState({ open: false });
  }
  handlePopupOnChange() {
    setState({ open: false });
  }
  render() {
    const { disclosureManager } = props;
    return (
      <div id="test-popup-area" className={cx('content-container')}>
        <Popup
          isArrowDisplayed
          classNameContent="test-content"
          isOpen={state.open}
          onRequestClose={handlePopupRequestClose}
          targetRef={() => document.getElementById('popup-in-modal')}
        >
          <p>This is popup content</p>
          <button type="button">Test button 1</button>
          <button type="button">Test button 2</button>
        </Popup>
        <Button id="popup-in-modal" className={cx('popup-button')} text="Popup In Modal" onClick={handlePopupButtonClick} />
        <br />
        <br />
        <Button className="close-disclosure" text="Close Disclosure" onClick={disclosureManager.closeDisclosure} />
      </div>
    );
  }
}
export default withDisclosureManager(ModalContent);
