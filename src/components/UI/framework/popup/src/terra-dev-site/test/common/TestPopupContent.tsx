import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import styles from "./TestPopupContent.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  onClick: PropTypes.func,
  size: PropTypes.oneOf(["small", "large"])
};
const TestPopupContent: Component = ({ onClick, size }) => (
  <div className={cx(`popup-content-${size}`)}>
    <button type="button" id="resize-content" onClick={onClick}>
      Resize
    </button>
  </div>
);
export default TestPopupContent;
