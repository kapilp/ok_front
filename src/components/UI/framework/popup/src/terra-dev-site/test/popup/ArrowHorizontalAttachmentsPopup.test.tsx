import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './ArrowHorizontalAttachmentsPopup.test.module.scss';
const cx = classNames.bind(styles);
type AlignmentPopupState = {
  open?: boolean;
  attachment?: string;
};
class AlignmentPopup extends React.Component<{}, AlignmentPopupState> {
  buttonNode: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleAttachment = handleAttachment.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: false, attachment: 'top right' };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleAttachment(event) {
    setState({ attachment: event.target.value, open: true });
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
          <Popup
            boundingRef={getParentNode}
            classNameArrow="test-arrow"
            classNameContent="test-content"
            contentAttachment="middle left"
            targetAttachment={state.attachment}
            contentHeight="80"
            contentWidth="160"
            isArrowDisplayed
            isOpen={state.open}
            onRequestClose={handleRequestClose}
            targetRef={getButtonNode}
          >
            <p className={cx('popup-text')}>This popup arrow was horizontally attached.</p>
          </Popup>
          <button type="button" id="alignment-button" className={cx('popup-button')} onClick={handleButtonClick} ref={setButtonNode} aria-label="alignment-button" />
        </div>
        <p>Choose Target Attachment:</p>
        <button type="button" id="attach-Top" value="top right" onClick={handleAttachment}>
          Attach Top Right
        </button>
        <button type="button" id="attach-Middle" value="middle right" onClick={handleAttachment}>
          Attach Middle Right
        </button>
        <button type="button" id="attach-Bottom" value="bottom right" onClick={handleAttachment}>
          Attach Bottom Right
        </button>
      </div>
    );
  }
}
export default AlignmentPopup;
