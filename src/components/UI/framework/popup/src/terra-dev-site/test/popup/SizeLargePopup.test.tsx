import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './SizeLargePopup.test.module.scss';
const cx = classNames.bind(styles);
type DimensionPopupState = {
  open?: boolean;
};
class DimensionPopup extends React.Component<{}, DimensionPopupState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
          <Popup
            boundingRef={getParentNode}
            classNameArrow="test-arrow"
            classNameContent="test-content"
            contentHeight="240"
            contentWidth="960"
            isOpen={state.open}
            onRequestClose={handleRequestClose}
            targetRef={getButtonNode}
          >
            <p>This popup is 240h by 960w.</p>
          </Popup>
          <button type="button" id="dimension-button" onClick={handleButtonClick} ref={setButtonNode}>
            Large Popup
          </button>
        </div>
      </div>
    );
  }
}
export default DimensionPopup;
