/* eslint-disable react/jsx-curly-brace-presence */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './ArrowSmallTargetLeftPopup.test.module.scss';
const cx = classNames.bind(styles);
type OffsetPopupState = {
  open?: boolean;
};
// This tests verifies the PopupUtils.getContentOffset methed when (targetNode.clientWidth < segment) & attachment is left
class OffsetPopup extends React.Component<{}, OffsetPopupState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
        <Popup
          boundingRef={getParentNode}
          classNameArrow="test-arrow"
          classNameContent="test-content"
          contentAttachment="top left"
          contentHeight="120"
          contentWidth="160"
          isArrowDisplayed
          isOpen={state.open}
          onRequestClose={handleRequestClose}
          targetRef={getButtonNode}
        >
          <p className={cx('popup-text')}>This popup was adjusted because the target was smaller than the arrow position allowed.</p>
        </Popup>
        <button type="button" id="offset-button" className={cx('popup-button')} onClick={handleButtonClick} ref={setButtonNode}>
          {''}
        </button>
      </div>
    );
  }
}
export default OffsetPopup;
