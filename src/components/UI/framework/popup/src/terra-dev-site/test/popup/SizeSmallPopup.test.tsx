import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './SizePopupCommon.module.scss';
const cx = classNames.bind(styles);
type DimensionPopupState = {
  open?: boolean;
};
class DimensionPopup extends React.Component<{}, DimensionPopupState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <div id="test-popup-area" className={cx('popup-area-small')} ref={setParentNode}>
          <Popup
            boundingRef={getParentNode}
            classNameArrow="test-arrow"
            classNameContent="test-content"
            contentHeight="40"
            contentWidth="160"
            isOpen={state.open}
            onRequestClose={handleRequestClose}
            targetRef={getButtonNode}
          >
            <p>This popup is 40h by 160w.</p>
          </Popup>
          <button type="button" id="dimension-button" onClick={handleButtonClick} ref={setButtonNode}>
            Small Popup
          </button>
        </div>
      </div>
    );
  }
}
export default DimensionPopup;
