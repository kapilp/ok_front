/* eslint-disable react/jsx-curly-brace-presence */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Popup from '../../../Popup';
import styles from './ArrowVerticalRightAdjustmentPopup.test.module.scss';
const cx = classNames.bind(styles);
type AlignmentPopupState = {
  open?: boolean;
};
// This tests verifies the PopupUtils.leftOffset methed when (offset > contentBounds.height - cornerOffset)
class AlignmentPopup extends React.Component<{}, AlignmentPopupState> {
  buttonNode: any;
  forceUpdate: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: true };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="test-popup-area" className={cx('test-popup-area')} ref={setParentNode}>
        <Popup
          boundingRef={getParentNode}
          classNameArrow="test-arrow"
          classNameContent="test-content"
          contentAttachment="top right"
          contentHeight="120"
          contentWidth="160"
          isArrowDisplayed
          isOpen={state.open}
          onRequestClose={handleRequestClose}
          targetRef={getButtonNode}
        >
          <p className={cx('popup-text')}>This popup arrow has vertical-right attachment, but was adjusted to be on the screen.</p>
        </Popup>
        <button type="button" id="alignment-button" className={cx('popup-button')} onClick={handleButtonClick} ref={setButtonNode}>
          {''}
        </button>
      </div>
    );
  }
}
export default AlignmentPopup;
