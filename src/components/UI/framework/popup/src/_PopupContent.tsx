import { JSX, Component, createMemo, mergeProps, splitProps, useContext } from 'solid-js';
// import { FormattedMessage } from "react-intl";
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
import { Button, ButtonVariants } from '../../../core/button/src/Button';
import { ContentContainer } from '../../../core/content-container/src/ContentContainer';
import { FocusTrap } from '../../../FocusTrap';
import { HookshotContent } from '../../hookshot/src/HookshotContent';
import styles from './PopupContent.module.scss';
const cx = classNamesBind.bind(styles);
/**
 * Rounded corner size to be used when calculating the arrow offset.
 */
export const CORNER_SIZE = 3;
interface IPopupContentProps extends JSX.HTMLAttributes<Element> {
  /**
   * The children to be presented as the popup's content.
   */
  children: JSX.Element;
  /**
   * The height value in px, to be applied to the content container.
   */
  contentHeight: number;
  /**
   * The width value in px, to be applied to the content container.
   */
  contentWidth: number;
  /**
   * The function that should be triggered when a close is indicated.
   */
  onRequestClose: () => void;
  /**
   * The function that should be triggered when a resize is indicated.
   */
  onResize: (e: Event, windowWidth: number) => void;
  /**
   * The arrow to be placed within the popup frame.
   */
  arrow?: JSX.Element;
  /**
   * CSS classnames that are appended to the popup content body.
   */
  classNameInner?: string;
  /**
   * The maximum height value in px, to be applied to the content container. Used with responsive behavior for actual height.
   */
  contentHeightMax?: number;
  /**
   * The maximum width value in px, to be applied to the content container. Used with responsive behavior for actual width.
   */
  contentWidthMax?: number;
  /**
   * Set this to `true` if your content has focusable elements and you want them to receive focus instead of focusing on the default popup frame when the popup is opened.
   */
  isFocusedDisabled?: boolean;
  /**
   * Should the default behavior, that inserts a header when constraints are breached, be disabled.
   */
  isHeaderDisabled?: boolean;
  /**
   * Whether or not the height provided is using a predefined value.
   */
  isHeightAutomatic?: boolean;
  /**
   * Whether or not the width provided is using a predefined value.
   */
  isWidthAutomatic?: boolean;
  /**
   * The function that should be triggered when a content resize is indicated.
   */
  onContentResize?: () => void;
  /**
   * @private
   * Prop to set role on popup content container
   */
  popupContentRole?: string;
  /**
   * The function returning the frame html reference.
   */
  ref?: (el: HTMLElement) => void;
}
const defaultProps = {
  classNameInner: null,
  contentHeightMax: -1,
  contentWidthMax: -1,
  isFocusedDisabled: false,
  isHeaderDisabled: false,
  isHeightAutomatic: false,
  isWidthAutomatic: false,
  popupContentRole: 'dialog',
};
function addPopupHeader(children: JSX.Element, onRequestClose: () => void) {
  const icon = () => <span className={cx('close-icon')} />;
  const header = (
    <div className={cx('header')}>
      <Button variant={ButtonVariants.UTILITY} isIconOnly icon={icon} onClick={onRequestClose} text={'Close'} />
    </div>
  );
  return (
    <ContentContainer header={header} fill>
      {children}
    </ContentContainer>
  );
}
function isBounded(value: number, maxValue: number) {
  return value > 0 && maxValue > 0 && value >= maxValue;
}

function getContentStyle(height, maxHeight, width, maxWidth, isHeightAutomatic, isWidthAutomatic) {
  const heightStyle = getDimensionStyle(height, maxHeight, isHeightAutomatic);
  const widthStyle = getDimensionStyle(width, maxWidth, isWidthAutomatic);
  const contentStyle = {};
  if (heightStyle) {
    contentStyle.height = heightStyle;
  }
  if (widthStyle) {
    contentStyle.width = widthStyle;
  }
  return contentStyle;
}
function getDimensionStyle(value, maxValue, isAutomatic) {
  if (value > 0) {
    if (maxValue > 0 && value >= maxValue) {
      return `${maxValue.toString()}px`;
    }
    if (!isAutomatic) {
      return `${value.toString()}px`;
    }
  }
  return null;
}
export const PopupContent: Component = (props: IPopupContentProps) => {
  props = mergeProps({}, defaultProps, props);

  let windowWidth: number;

  //componentDidMount() {
  // Value used to verify horizontal resize.
  windowWidth = window.innerWidth;
  //}

  const handleOnResize = (event: Event) => {
    if (props.onResize) {
      props.onResize(event, windowWidth);
    }
  };
  const [p, customProps] = splitProps(props, [
    'arrow',
    'children',
    'classNameInner',
    'contentHeight',
    'contentHeightMax',
    'contentWidth',
    'contentWidthMax',
    'isFocusedDisabled',
    'isHeaderDisabled',
    'isHeightAutomatic',
    'isWidthAutomatic',
    'onRequestClose',
    'onResize',
    'onContentResize',
    'popupContentRole',
    'ref',
  ]);

  const contentStyle = () => getContentStyle(p.contentHeight, p.contentHeightMax, p.contentWidth, p.contentWidthMax, p.isHeightAutomatic, p.isWidthAutomatic);
  const isHeightBounded = () => isBounded(p.contentHeight, p.contentHeightMax);
  const isWidthBounded = () => isBounded(p.contentWidth, p.contentWidthMax);
  const isFullScreen = () => isHeightBounded() && isWidthBounded();

  // Todo Fix this Array function related function:
  /*function cloneChildren(
    children: JSX.Element,
    isHeightAutomatic: boolean,
    isWidthAutomatic: boolean,
    isHeightBounded: boolean,
    isWidthBounded: boolean,
    isHeaderDisable: boolean,
  ) {
    const newProps = {};
    if (isHeightAutomatic) {
      newProps.isHeightBounded = isHeightBounded;
    }
    if (isWidthAutomatic) {
      newProps.isWidthBounded = isWidthBounded;
    }
    if (isHeightBounded && isWidthBounded && isHeaderDisabled) {
      newProps.closeButtonRequired = 'true';
    }
    return React.Children.map(children, child => React.cloneElement(child, newProps));
  }*/
  function getContent() {
    let content = <>{p.children}</>;
    //let content = cloneChildren(p.children, p.isHeightAutomatic, p.isWidthAutomatic, p.isHeightBounded, p.isWidthBounded, p.isHeaderDisabled);
    content = <>{isFullScreen() && !p.isHeaderDisabled ? addPopupHeader(content, p.onRequestClose) : p.children}</>; // hack content variable not worked
    return content;
  }

  const theme = useContext(ThemeContext);
  const contentClassNames = createMemo(() => classNames(cx('content', theme.className), customProps.className));
  const roundedCorners = createMemo(() => p.arrow && !isFullScreen());
  const arrowContent = createMemo(() => (roundedCorners() ? p.arrow : undefined));
  const innerClassNames = createMemo(() => cx(['inner', { 'is-full-screen': isFullScreen() }, { 'rounded-corners': roundedCorners() }, p.classNameInner]));
  const heightData = createMemo(() => (p.isHeightAutomatic ? { 'data-terra-popup-automatic-height': true } : {}));
  const widthData = createMemo(() => (p.isWidthAutomatic ? { 'data-terra-popup-automatic-width': true } : {}));
  return (
    <FocusTrap
      focusTrapOptions={{
        returnFocusOnDeactivate: true,
        clickOutsideDeactivates: true,
      }}
    >
      <div>
        <HookshotContent
          {...customProps}
          className={contentClassNames()}
          tabIndex={p.isFocusedDisabled ? null : '0'}
          data-terra-popup-content
          onContentResize={p.isHeightAutomatic || p.isWidthAutomatic ? p.onContentResize : undefined}
          onEsc={p.onRequestClose}
          onResize={handleOnResize}
          ref={p.ref}
          role={p.popupContentRole || null}
        >
          {arrowContent}

          <div {...{ ...heightData(), ...widthData() }} className={innerClassNames()} style={contentStyle()}>
            {getContent()}
          </div>
        </HookshotContent>
      </div>
    </FocusTrap>
  );
};
