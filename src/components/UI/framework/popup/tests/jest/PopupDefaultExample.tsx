import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Popup from '../../src/Popup';
import styles from './Popup.test.module.scss';
type PopupDefaultExampleState = {
  open?: boolean;
};
class PopupDefaultExample extends React.Component<{}, PopupDefaultExampleState> {
  buttonNode: any;
  constructor(props) {
    super(props);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: true };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <Popup {...props} isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <p>this is popup content</p>
        </Popup>
        <div className={styles.container} ref={setButtonNode} />
      </div>
    );
  }
}
export default PopupDefaultExample;
