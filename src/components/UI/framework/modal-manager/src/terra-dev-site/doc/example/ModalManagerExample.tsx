/* eslint-disable max-classes-per-file */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import ActionHeader from 'terra-action-header';
import CollapsibleMenuView from 'terra-collapsible-menu-view';
import ContentContainer from 'terra-content-container';
import { availableDisclosureHeights, availableDisclosureWidths, DisclosureManagerHeaderAdapter, DisclosureManagerContext } from 'terra-disclosure-manager';
import ModalManager from 'terra-modal-manager';
import styles from 'terra-modal-manager/lib/terra-dev-site/doc/example/example-styles.module.scss';
const cx = classNames.bind(styles);
interface IDisclosureComponentProps extends JSX.HTMLAttributes<Element> {
  name?: string;
  disclosureType?: string;
}
type DisclosureComponentState = {
  text?: undefined;
  length?: any;
  selectedHeaderAction?: string;
};
class DisclosureComponent extends React.Component<IDisclosureComponentProps, DisclosureComponentState> {
  context: any;
  constructor(props) {
    super(props);
    checkLockState = checkLockState.bind(this);
    state = {
      text: undefined,
    };
  }
  componentDidMount() {
    const disclosureManager = context;
    if (disclosureManager && disclosureManager.registerDismissCheck) {
      disclosureManager.registerDismissCheck(checkLockState);
    }
  }
  checkLockState() {
    if (state.text && state.text.length) {
      return new Promise((resolve, reject) => {
        // eslint-disable-next-line no-restricted-globals
        if (!confirm(`${props.name} has unsaved changes that will be lost. Do you wish to continue?`)) {
          // eslint-disable-line no-alert
          reject();
          return;
        }
        resolve();
      });
    }
    return Promise.resolve();
  }
  render() {
    const { name, disclosureType } = props;
    const disclosureManager = context;
    return (
      <ContentContainer fill>
        <DisclosureManagerHeaderAdapter
          title={name}
          collapsibleMenuView={
            <CollapsibleMenuView>
              <CollapsibleMenuView.Item
                text="Button 1"
                key="button_1"
                onClick={() => {
                  setState({
                    selectedHeaderAction: 'Button 1',
                  });
                }}
              />
              <CollapsibleMenuView.Item
                text="Button 2"
                key="button_2"
                onClick={() => {
                  setState({
                    selectedHeaderAction: 'Button 2',
                  });
                }}
              />
            </CollapsibleMenuView>
          }
        />
        <div className={cx('content-wrapper')}>
          <h3>{name}</h3>
          <p>The disclosed component can disclose content within the same modal.</p>
          <p>It can also render a header (like above) that implements the various DisclosureManager control functions.</p>
          <button
            type="button"
            onClick={() => {
              disclosureManager.dismiss().catch(() => {
                console.log('Dismiss failed. A lock must be in place.'); // eslint-disable-line no-console
              });
            }}
          >
            Dismiss
          </button>
          <button
            type="button"
            onClick={() => {
              disclosureManager.disclose({
                preferredType: disclosureType,
                size: 'small',
                content: {
                  key: `Nested ${name}`,
                  component: <DisclosureComponent name={`Nested ${name}`} disclosureType={disclosureType} />,
                },
              });
            }}
          >
            Disclose Again
          </button>
          <br />
          <br />
          <p>
            The disclosed component can register a dismiss check function that can interrupt and prevent dismissal. This component will prompt the user if text is detected in the
            input field below.
          </p>
          <input
            type="text"
            onChange={event => {
              setState({
                text: event.target.value,
              });
            }}
            value={state.text || ''}
          />
          {state.text && state.text.length ? <p>Component has unsaved changes!</p> : null}
          <br />
          <br />
          <p>
            Selected Header Action: <b>{state.selectedHeaderAction}</b>
          </p>
        </div>
      </ContentContainer>
    );
  }
}
DisclosureComponent.contextType = DisclosureManagerContext;
DisclosureComponent.defaultProps = {
  name: 'Disclosure Component',
};
const HEIGHT_KEYS = Object.keys(availableDisclosureHeights);
const WIDTH_KEYS = Object.keys(availableDisclosureWidths);
const generateDimensionOptions = values =>
  values.map((currentValue, index) => {
    const keyValue = index;
    return (
      <option key={keyValue} value={currentValue}>
        {currentValue}
      </option>
    );
  });
interface IContentComponentProps extends JSX.HTMLAttributes<Element> {
  disclosureType?: string;
}
type ContentComponentState = {
  id?: string;
  disclosureHeight?: string;
  disclosureWidth?: string;
};
class ContentComponent extends React.Component<IContentComponentProps, ContentComponentState> {
  context: any;
  constructor(props) {
    super(props);
    renderButton = renderButton.bind(this);
    handleSelectChange = handleSelectChange.bind(this);
    getId = getId.bind(this);
    state = {
      id: 'dimensions',
      disclosureHeight: HEIGHT_KEYS[0],
      disclosureWidth: WIDTH_KEYS[0],
    };
  }
  getId(name) {
    return name + state.id;
  }
  handleSelectChange(event) {
    setState({ [event.target.name]: event.target.value });
  }
  renderButton(size) {
    const { disclosureType } = props;
    const disclosureManager = context;
    return (
      <button
        type="button"
        onClick={() => {
          disclosureManager.disclose({
            preferredType: disclosureType,
            size,
            content: {
              key: `Content-Disclosure-${size}`,
              component: <DisclosureComponent name="Disclosure Component" disclosureType={disclosureType} />,
            },
          });
        }}
      >
        {`Disclose (${size})`}
      </button>
    );
  }
  renderFormButton() {
    const { disclosureType } = props;
    const disclosureManager = context;
    const name = `Disclose (${state.disclosureHeight}) x (${state.disclosureWidth})`;
    return (
      <button
        type="button"
        onClick={() => {
          disclosureManager.disclose({
            preferredType: disclosureType,
            dimensions: {
              height: state.disclosureHeight,
              width: state.disclosureWidth,
            },
            content: {
              key: 'Content-Disclosure-Dimensions',
              component: <DisclosureComponent name="Disclosure Component" disclosureType={disclosureType} />,
            },
          });
        }}
      >
        {name}
      </button>
    );
  }
  renderForm() {
    return (
      <form>
        <label htmlFor={getId('disclosureHeight')}>Pop Content Height</label>
        <select id={getId('disclosureHeight')} name="disclosureHeight" value={state.disclosureHeight} onChange={handleSelectChange}>
          {generateDimensionOptions(HEIGHT_KEYS)}
        </select>
        <br />
        <br />
        <label htmlFor={getId('disclosureWidth')}>Pop Content Width</label>
        <select id={getId('disclosureWidth')} name="disclosureWidth" value={state.disclosureWidth} onChange={handleSelectChange}>
          {generateDimensionOptions(WIDTH_KEYS)}
        </select>
        <br />
        <br />
      </form>
    );
  }
  render() {
    return (
      <ContentContainer header={<ActionHeader title="ModalManager Child Component" />}>
        <div className={cx('content-wrapper')}>
          {renderButton('default')}
          {renderButton('tiny')}
          {renderButton('small')}
          {renderButton('medium')}
          {renderButton('large')}
          {renderButton('huge')}
          {renderButton('fullscreen')}
          <br />
          <p>The child components can disclose content in the modal at various sizes.</p>
        </div>
        <div className={cx('content-wrapper')}>
          {renderForm()}
          {renderFormButton()}
        </div>
      </ContentContainer>
    );
  }
}
ContentComponent.contextType = DisclosureManagerContext;
type ModalManagerExampleProps = {
  showDisclosureAccessory?: boolean;
};
const ModalManagerExample: React.SFC<ModalManagerExampleProps> = ({ showDisclosureAccessory }) => (
  <div className={cx('example-wrapper')}>
    <ModalManager disclosureAccessory={showDisclosureAccessory ? <div className={cx('disclosure-accessory')}>Disclosure Accessory</div> : undefined}>
      <ContentComponent disclosureType="modal" />
    </ModalManager>
  </div>
);
export default ModalManagerExample;
