import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Placeholder.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  text: string;
  type: string;
}
const defaultProps = {
  text: 'PlaceHolder',
  type: 'default',
};
export const Placeholder: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  return (
    <div className={cx(`wrapper1-${props.type}`)}>
      <div className={cx('wrapper2')}>
        <div className={cx('wrapper3')}>
          <h3>{props.text}</h3>
        </div>
      </div>
    </div>
  );
};
