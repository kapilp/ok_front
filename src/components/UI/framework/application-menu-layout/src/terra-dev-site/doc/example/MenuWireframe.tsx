import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { PlaceHolder } from '../../../../../application-header-layout/src/terra-dev-site/doc/common/Placeholder';
import { ApplicationMenuLayout } from '../../../ApplicationMenuLayout';
import styles from './MenuWireframe.module.scss';
const cx = classNames.bind(styles);
export const MenuWireframe = () => (
  <div className={cx('content-wrapper')}>
    <ApplicationMenuLayout
      header={<PlaceHolder text="Header" type="header" />}
      footer={<PlaceHolder text="Footer" type="footer" />}
      extensions={<PlaceHolder text="Extensions" type="extensions" />}
      content={<PlaceHolder text="Content" />}
    />
  </div>
);
