import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './ApplicationMenuLayout.module.scss';
const cx = classNames.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * Content element to be placed within the fill area of the header.
   * */
  content?: JSX.Element;
  /**
   * Extensions element to be placed before the end of the header.
   * */
  extensions?: JSX.Element;
  /**
   * Footer element to be placed at the end of the header.
   * */
  footer?: JSX.Element;
  /**
   * Header element to be placed at the start of the header.
   * */
  header?: JSX.Element;
}
export const ApplicationMenuLayout: Component<Properties> = (props: Properties) => {
  const [extracted, customProps] = splitProps(props, ['content', 'extensions', 'footer', 'header']);
  const menuClassNames = cx(['menu', 'fill', customProps.className]);
  const headerElement = <>{extracted.header && <div className={cx(['fit', 'header'])}>{extracted.header}</div>}</>;

  let contentElement = (
    <>
      {extracted.content && (
        <div className={cx(['fill', 'content'])}>
          <div className={cx('normalizer')}>{extracted.content}</div>
        </div>
      )}
    </>
  );

  const extensionsElement = <>{extracted.extensions && <div className={cx(['fit', 'widget'])}>{extracted.extensions}</div>}</>;

  let footerElement = <>{extracted.footer && <div className={cx(['fit', 'footer'])}>{extracted.footer}</div>}</>;
  return (
    <div {...customProps} className={menuClassNames}>
      {headerElement}
      <div className={cx(['fill', 'body'])}>
        {extensionsElement}
        {contentElement}
      </div>
      {footerElement}
    </div>
  );
};
