import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import { injectIntl, intlShape } from "react-intl";
import ResizeObserver from "resize-observer-polyfill";
import List from "terra-list";
import VisuallyHiddenText from "terra-visually-hidden-text";
import InfiniteUtils from "./_InfiniteUtils";
import styles from "./InfiniteList.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * String that labels the list for screen readers.
   */
  ariaLabel: string,
  /**
   * The child list items, of type InfiniteList Item, to be placed within the infinite list.
   * For further documentation of InfiniteList Item see terra-list's ListItem.
   */
  children: JSX.Element,
  /**
   * Whether or not the list's child items should have a border color applied.
   * One of `'none'`, `'standard'`, `'bottom-only'`.
   */
  dividerStyle: PropTypes.oneOf(["none", "standard", "bottom-only"]),
  /**
   * An indicator to be displayed when no children are yet present.
   */
  initialLoadingIndicator: JSX.Element,
  /**
   * @private
   * Internationalization object with translation APIs. Provided by `injectIntl`.
   */
  intl: intlShape,
  /**
   * Determines whether or not the loading indicator is visible and if callbacks are triggered.
   */
  isFinishedLoading: boolean,
  /**
   * Callback trigger when new list items are requested.
   */
  onRequestItems: PropTypes.func,
  /**
   * The padding styling to apply to the child list item content.
   * One of `'none'`, `'standard'`, `'compact'`.
   */
  paddingStyle: PropTypes.oneOf(["none", "standard", "compact"]),
  /**
   * An indicator to be displayed at the end of the current loaded children.
   */
  progressiveLoadingIndicator: JSX.Element,
  /**
   * A message to be provided to screen readers as new items are progressively loaded in
   */
  progressiveLoadingMessage: string,
  /**
   * Accessibility role of the list, defaults to 'none'. If creating a list with selectable items, pass 'listbox'.
   */
  role: string,
  /**
   * Function callback for the ref of the List(ul).
   */
  ref: PropTypes.func
};
const defaultProps = {
  children: [],
  dividerStyle: "none",
  isFinishedLoading: false,
  paddingStyle: "none",
  progressiveLoadingMessage: undefined,
  role: "none"
};
/**
 * Returns a ListItem sized according to the provided height to use as a spacer.
 * @param {number} height - Height to set on the ListItem.
 * @param {number} index - Index to use as part of the spacers key.
 */
/* eslint-disable react/forbid-dom-props */
const createSpacer = (height, index) => (
  <div
    className={cx(["spacer"])}
    style={{ height }}
    key={`infinite-spacer-${index}`}
  />
);
interface IInfiniteListProps extends JSX.HTMLAttributes<Element> {
  ariaLabel?: any;
  dividerStyle?: any;
  initialLoadingIndicator?: any;
  intl?: any;
  isFinishedLoading?: any;
  onRequestItems?: any;
  paddingStyle?: any;
  progressiveLoadingIndicator?: any;
  progressiveLoadingMessage?: any;
  role?: any;
  ref?: any;
  customProps?: any;
}
/* eslint-enable react/forbid-dom-props */
export const InfiniteList: Component = (props:IInfiniteListProps) => {
  animationFrameID: null;
  ariaLiveStatus: any;
  boundary: any;
  childCount: any;
  childrenArray: any;
  clientHeight: any;
  content: null;
  contentNode: any;
  disableScroll: any;
  forceUpdate: any;
  hasRequestedItems: boolean;
  isCalculating: boolean;
  isRenderingNew: boolean;
  itemsByIndex: any;
  last: boolean;
  lastChildIndex: number;
  loadingIndex: number;
  preventUpdate: boolean;
  renderNewChildren: boolean;
  resizeObserver: any;
  scrollGroups: any;
  scrollHeight: any;
  timer: Timeout;
  constructor(props) {
    super(props);
    update = update.bind(this);
    resetCache = resetCache.bind(this);
    setContentNode = setContentNode.bind(this);
    updateItemCache = updateItemCache.bind(this);
    initializeItemCache = initializeItemCache.bind(this);
    updateScrollGroups = updateScrollGroups.bind(this);
    handleRenderCompletion = handleRenderCompletion.bind(this);
    handleResize = resizeDebounce(handleResize.bind(this));
    resetTimeout = resetTimeout.bind(this);
    wrapChild = wrapChild.bind(this);
    ariaLiveStatus = "";
    updateAriaLiveLoadingStatus = updateAriaLiveLoadingStatus.bind(
      this
    );
    resetCache();
    initializeItemCache(props);
  }
  componentDidMount() {
    if (contentNode) {
      resizeObserver = new ResizeObserver(entries => {
        content = entries[0].contentRect;
        if (!isCalculating) {
          animationFrameID = window.requestAnimationFrame(() => {
            // Resetting the cache so that all elements will be rendered face-up for width calculations
            resetCache();
            forceUpdate();
          });
        }
      });
      handleResize(content);
      resizeObserver.observe(contentNode);
    }
    contentNode.addEventListener("scroll", update);
    updateScrollGroups();
    handleRenderCompletion();
  }
  shouldComponentUpdate(nextProps) {
    const newChildCount = React.Children.count(nextProps.children);
    if (newChildCount > childCount) {
      lastChildIndex = childCount;
      loadingIndex += 1;
      updateItemCache(nextProps);
    } else if (newChildCount < childCount) {
      initializeItemCache(nextProps);
    } else {
      childrenArray = React.Children.toArray(nextProps.children);
    }
    return true;
  }
  componentDidUpdate() {
    if (isCalculating) {
      isCalculating = false;
      handleResize(content);
    }
    handleRenderCompletion();
  }
  componentWillUnmount() {
    if (contentNode) {
      clearTimeout(timer);
      window.cancelAnimationFrame(animationFrameID);
      resizeObserver.disconnect(contentNode);
      contentNode.removeEventListener("scroll", update);
      content = null;
    }
  }
  /**
   * Sets the html node of contentNode and if it was previously undefined trigger updateScrollGroups.
   * @param {node} node - Html node of the List.
   */
  setContentNode(node) {
    const wasUndefined = !contentNode;
    contentNode = node;
    if (contentNode && wasUndefined) {
      updateScrollGroups();
    }
  }
  /**
   * If a request for items has not been made and/or updates are not pending trigger onRequestItems.
   */
  triggerItemRequest() {
    if (
      !props.isFinishedLoading &&
      !hasRequestedItems &&
      !isRenderingNew &&
      props.onRequestItems
    ) {
      hasRequestedItems = true;
      props.onRequestItems();
      updateAriaLiveLoadingStatus();
    }
  }
  updateAriaLiveLoadingStatus() {
    ariaLiveStatus = props.progressiveLoadingMessage
      ? props.progressiveLoadingMessage
      : props.intl.formatMessage({ id: "Terra.InfiniteList.loading" });
    // Clears status so aria live announces correctly next time more items are loaded
    setTimeout(() => {
      ariaLiveStatus = "";
    }, 1000);
  }
  /**
   * Following a render reset newChildren values. If new items were render trigger an update calculation.
   */
  handleRenderCompletion() {
    renderNewChildren = false;
    preventUpdate = false;
    lastChildIndex = childCount;
    if (isRenderingNew) {
      isRenderingNew = false;
      update(null, false, true); // Prevent from triggering an item request to avoid infinite loop of loading.
    } else if (
      contentNode &&
      InfiniteUtils.shouldTriggerItemRequest(
        InfiniteUtils.getContentData(contentNode)
      )
    ) {
      triggerItemRequest();
    }
  }
  /**
   * Cache the value for the child count and convert the children props to an array.
   * @param {object} props - React element props.
   */
  updateItemCache(props) {
    childCount = React.Children.count(props.children);
    childrenArray = React.Children.toArray(props.children);
    hasRequestedItems = false;
    renderNewChildren = true;
  }
  /**
   * Set the initial state of the virtualization values for the list.
   * @param {object} props - React element props.
   */
  initializeItemCache(props) {
    loadingIndex = 0;
    lastChildIndex = -1;
    itemsByIndex = [];
    scrollGroups = [];
    boundary = {
      topBoundryIndex: -1,
      hiddenTopHeight: -1,
      bottomBoundryIndex: -1,
      hiddenBottomHeight: -1
    };
    updateItemCache(props);
  }
  /**
   * Reset the timeout on timer.
   * @param {function} fn - The handleResize function.
   * @param {object} args - Arguments passed to the handleResize function.
   * @param {context} context - Javascript context.
   * @param {double} now - DOMHighResTimeStamp.
   */
  resetTimeout(fn, args, context, now) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      last = now;
      disableScroll = false;
      fn.apply(context, args);
    }, 250);
  }
  /**
   * Debounce the size event and prevent updates from scrolling.
   * @param {function} fn - The handleResize function.
   */
  resizeDebounce(fn) {
    return (...args) => {
      const context = this;
      const now = performance.now();
      if (last && now < last + 250) {
        resetTimeout(fn, args, context, now);
      } else {
        last = now;
        disableScroll = true;
        resetTimeout(fn, args, context, now);
      }
    };
  }
  resetCache() {
    animationFrameID = null;
    isCalculating = true;
  }
  /**
   * Triggers a height adjustment if the height or scroll height changes.
   */
  handleResize() {
    if (
      scrollHeight !== contentNode.scrollHeight ||
      clientHeight !== contentNode.clientHeight
    ) {
      adjustHeight();
    }
    forceUpdate();
  }
  /**
   * Calculates the visible scroll items and if the hidden items have changed force an update.
   * @param {object} event - Browser DOM event.
   * @param {bool} ensureUpdate - Regardless of calculation ensure a forceUpdate occurs.
   * @param {bool} preventRequest - Should triggerItemRequest be prevented.
   */
  update(event, ensureUpdate, preventRequest) {
    if (!contentNode || disableScroll || preventUpdate) {
      return;
    }
    const contentData = InfiniteUtils.getContentData(contentNode);
    const hiddenItems = InfiniteUtils.getHiddenItems(
      scrollGroups,
      contentData,
      boundary.topBoundryIndex,
      boundary.bottomBoundryIndex
    );
    scrollHeight = contentData.scrollHeight;
    clientHeight = contentData.clientHeight;
    if (
      ensureUpdate ||
      hiddenItems.topHiddenItem.index !== boundary.topBoundryIndex ||
      hiddenItems.bottomHiddenItem.index !== boundary.bottomBoundryIndex
    ) {
      preventUpdate = true;
      boundary = {
        topBoundryIndex: hiddenItems.topHiddenItem.index,
        hiddenTopHeight: hiddenItems.topHiddenItem.height,
        bottomBoundryIndex: hiddenItems.bottomHiddenItem.index,
        hiddenBottomHeight: hiddenItems.bottomHiddenItem.height
      };
      forceUpdate();
    }
    if (
      !preventRequest &&
      InfiniteUtils.shouldTriggerItemRequest(contentData)
    ) {
      triggerItemRequest();
    }
  }
  /**
   * Groups scroll items by height to reduce the number of refreshs that occur on scroll.
   */
  updateScrollGroups() {
    if (!contentNode) {
      return;
    }
    let groupHeight = 0;
    let groupIndex = 0;
    let captureOffsetTop = true;
    const maxGroupHeight = 1 * contentNode.clientHeight;
    scrollGroups = [];
    for (let i = 0; i < itemsByIndex.length; i += 1) {
      const item = itemsByIndex[i];
      if (scrollGroups[groupIndex] && item.height >= maxGroupHeight) {
        groupHeight = 0;
        groupIndex += 1;
        captureOffsetTop = true;
      }
      groupHeight += item.height;
      scrollGroups[groupIndex] = scrollGroups[groupIndex] || {
        items: []
      };
      scrollGroups[groupIndex].items.push(i);
      scrollGroups[groupIndex].height = groupHeight;
      itemsByIndex[i].groupIndex = groupIndex;
      if (captureOffsetTop) {
        scrollGroups[groupIndex].offsetTop = itemsByIndex[
          i
        ].offsetTop;
        captureOffsetTop = false;
      }
      if (groupHeight >= maxGroupHeight) {
        groupHeight = 0;
        groupIndex += 1;
        captureOffsetTop = true;
      }
    }
  }
  /**
   * Checks the boundingClientRect for the scroll item's height and offsetTop then caches it.
   * @param {node} node - The child node for the scroll item.
   * @param {number} index - Index of the child.
   */
  updateHeight(node, index) {
    if (node) {
      itemsByIndex[index] = itemsByIndex[index] || {};
      let updatedHeight = false;
      const newHeight = node.getBoundingClientRect().height;
      if (
        !itemsByIndex[index].height ||
        Math.abs(itemsByIndex[index].height - newHeight) > 1
      ) {
        itemsByIndex[index].height = newHeight;
        updatedHeight = true;
      }
      if (
        !itemsByIndex[index].offsetTop ||
        Math.abs(itemsByIndex[index].offsetTop - node.offsetTop) > 1
      ) {
        itemsByIndex[index].offsetTop = node.offsetTop;
      }
      if (itemsByIndex.length === childCount) {
        if (!scrollGroups.length) {
          updateScrollGroups();
        } else if (updatedHeight) {
          adjustHeight();
        }
      }
    }
  }
  /**
   * Detects which scroll items are on the dom and reads the heights to see if resize has changed the boundClientRect.
   */
  adjustHeight() {
    if (contentNode) {
      itemsByIndex.forEach((item, itemIndex) => {
        const scrollItemNode = contentNode.querySelector(
          `[data-infinite-list-index="${itemIndex}"]`
        );
        if (scrollItemNode) {
          const newHeight = scrollItemNode.getBoundingClientRect().height;
          if (
            !itemsByIndex[itemIndex].height ||
            Math.abs(newHeight - itemsByIndex[itemIndex].height) > 1
          ) {
            itemsByIndex[itemIndex].height = newHeight;
          }
          if (
            !itemsByIndex[itemIndex].offsetTop ||
            Math.abs(
              itemsByIndex[itemIndex].offsetTop - scrollItemNode.offsetTop
            ) > 1
          ) {
            itemsByIndex[itemIndex].offsetTop = scrollItemNode.offsetTop;
          }
          adjustTrailingItems(itemIndex);
        }
      });
      // needs to update offset tops of every other save
      updateScrollGroups();
      boundary = {
        topBoundryIndex: -1,
        hiddenTopHeight: -1,
        bottomBoundryIndex: -1,
        hiddenBottomHeight: -1
      };
      update(null, true);
    }
  }
  /**
   * Updates the offsetTop of scroll items following the element that adjusted it's height.
   * @param {number} index - Index of the scroll item that had adjusted it's height.
   */
  adjustTrailingItems(index) {
    let lastTop = itemsByIndex[index].offsetTop;
    let lastHeight = itemsByIndex[index].height;
    for (let i = index + 1; i < itemsByIndex.length; i += 1) {
      lastTop += lastHeight;
      lastHeight = itemsByIndex[i].height;
      itemsByIndex[i].offsetTop = lastTop;
    }
  }
  /**
   * Clones the child element with new props for selection, ref,  and data attributes.
   * @param {element} child - React child element.
   * @param {number} index - Index of the child element.
   */
  wrapChild(child, index) {
    const wrappedCallback = node => {
      updateHeight(node, index);
      if (child.props.ref) {
        child.props.ref(node);
      }
    };
    const newProps = {
      ref: wrappedCallback,
      "data-infinite-list-index": index
    };
    return React.cloneElement(child, newProps);
  }
  render() {
    const {
      ariaLabel,
      children,
      dividerStyle,
      initialLoadingIndicator,
      intl,
      isFinishedLoading,
      onRequestItems,
      paddingStyle,
      progressiveLoadingIndicator,
      progressiveLoadingMessage,
      role,
      ref,
      ...customProps
    } = props;
    const topSpacer = createSpacer(
      `${
        boundary.hiddenTopHeight > 0 ? boundary.hiddenTopHeight : 0
      }px`,
      0
    );
    const bottomSpacer = createSpacer(
      `${
        boundary.hiddenBottomHeight > 0
          ? boundary.hiddenBottomHeight
          : 0
      }px`,
      1
    );
    let loadingSpinner;
    let initialSpinner;
    if (!isFinishedLoading) {
      if (childCount > 0) {
        loadingSpinner = (
          <div
            className={cx("spacer")}
            key={`infinite-spinner-row-${loadingIndex}`}
          >
            {progressiveLoadingIndicator}
          </div>
        );
      } else {
        /* eslint-disable react/forbid-dom-props */
        initialSpinner = (
          <div
            className={cx("spacer")}
            key="infinite-spinner-full"
            style={{ height: "100%" }}
          >
            {initialLoadingIndicator}
          </div>
        );
        /* eslint-enable react/forbid-dom-props */
      }
    }
    const attrSpread = { dividerStyle, paddingStyle };
    if (role && role.length > 0 && role !== "none") {
      attrSpread.role = role;
    }
    let newChildren;
    let visibleChildren;
    if (!initialSpinner) {
      let upperChildIndex = lastChildIndex;
      if (
        (!scrollGroups.length && lastChildIndex <= 0) ||
        !renderNewChildren
      ) {
        upperChildIndex = childCount;
      } else {
        newChildren = (
          <List {...attrSpread} className={cx(["infinite-hidden"])}>
            {InfiniteUtils.getNewChildren(
              lastChildIndex,
              childrenArray,
              wrapChild
            )}
          </List>
        );
        isRenderingNew = true;
      }
      visibleChildren = InfiniteUtils.getVisibleChildren(
        scrollGroups,
        childrenArray,
        boundary.topBoundryIndex,
        boundary.bottomBoundryIndex,
        wrapChild,
        upperChildIndex
      );
    }
    return (
      <>
        <div
          {...customProps}
          className={cx(["infinite-list", customProps.className])}
          ref={setContentNode}
        >
          {initialSpinner}
          {topSpacer}
          <List
            {...attrSpread}
            aria-label={ariaLabel}
            ref={ref}
          >
            {visibleChildren}
          </List>
          {bottomSpacer}
          {loadingSpinner}
        </div>
        {newChildren}
        <VisuallyHiddenText
          aria-atomic="true"
          aria-live="assertive"
          text={ariaLiveStatus}
        />
      </>
    );
  }
}
props = mergeProps({}, defaultProps, props)
export default injectIntl(InfiniteList);
