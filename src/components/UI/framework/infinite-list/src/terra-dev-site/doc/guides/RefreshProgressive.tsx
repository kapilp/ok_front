import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ProgressiveLoadingList from './ProgressiveLoadingList';
type RefreshProgressiveState = {
  refreshKey?: number;
};
class RefreshProgressive extends React.Component<{}, RefreshProgressiveState> {
  constructor(props) {
    super(props);
    updateKey = updateKey.bind(this);
    state = { refreshKey: 0 };
  }
  updateKey() {
    setState(prevState => ({
      refreshKey: prevState.refreshKey + 1,
    }));
  }
  render() {
    return (
      <div>
        <button type="button" onClick={updateKey}>
          Refresh
        </button>
        <ProgressiveLoadingList key={`${state.refreshKey}`} />
      </div>
    );
  }
}
export default RefreshProgressive;
