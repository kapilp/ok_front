import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import InfiniteList, { Item } from 'terra-infinite-list/lib/index';
import classNames from 'classnames/bind';
import MyInitialLoadingIndicator from '../common/MyInitialLoadingIndicator';
import MyProgressiveLoadingIndicator from '../common/MyProgressiveLoadingIndicator';
import MyExampleContainer from '../common/MyExampleContainer';
import mockRequest from './mock-data/mock-progressive';
import styles from '../example/InfiniteListDocExampleCommon.module.scss';
const cx = classNames.bind(styles);
type ProgressiveLoadingListState = {
  isFinishedLoading?: boolean;
  currentPageKey?: null;
  items?: undefined[];
  then?: any;
  map?: any;
};
class ProgressiveLoadingList extends React.Component<{}, ProgressiveLoadingListState> {
  constructor(props) {
    super(props);
    requestData = requestData.bind(this);
    state = { isFinishedLoading: false, currentPageKey: null, items: [] };
  }
  requestData() {
    mockRequest(state.currentPageKey).then(response =>
      setState(prevState => ({
        isFinishedLoading: response.isFinalPage,
        currentPageKey: response.nextPageKey,
        items: prevState.items.concat(response.items),
      })),
    );
  }
  createItems() {
    return state.items.map(item => (
      <Item key={item.key}>
        <Placeholder title={item.title} className={cx('placeholder')} />
      </Item>
    ));
  }
  render() {
    return (
      <MyExampleContainer>
        <InfiniteList
          dividerStyle="standard"
          isFinishedLoading={state.isFinishedLoading}
          onRequestItems={requestData}
          ariaLabel="Progressive Loading"
          initialLoadingIndicator={<MyInitialLoadingIndicator />}
          progressiveLoadingIndicator={<MyProgressiveLoadingIndicator />}
        >
          {createItems()}
        </InfiniteList>
      </MyExampleContainer>
    );
  }
}
export default ProgressiveLoadingList;
