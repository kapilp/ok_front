import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import InfiniteList from 'terra-infinite-list';
import MyInitialLoadingIndicator from '../common/MyInitialLoadingIndicator';
import MyExampleContainer from '../common/MyExampleContainer';
const InitialLoadingExample = () => (
  <MyExampleContainer>
    <InfiniteList dividerStyle="standard" ariaLabel="Initial Loading" initialLoadingIndicator={<MyInitialLoadingIndicator />} />
  </MyExampleContainer>
);
export default InitialLoadingExample;
