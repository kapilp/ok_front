import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Placeholder from 'terra-doc-template/lib/Placeholder';
import InfiniteList, { Item } from 'terra-infinite-list/lib/index';
import styles from './InfiniteListTestCommon.module.scss';
import MyInitialLoadingIndicator from '../../doc/common/MyInitialLoadingIndicator';
import MyProgressiveLoadingIndicator from '../../doc/common/MyProgressiveLoadingIndicator';
import MyExampleContainer from '../../doc/common/MyExampleContainer';
import mockRequest from '../../doc/guides/mock-data/mock-progressive';
const cx = classNames.bind(styles);
type ProgressiveLoadingListState = {
  isFinishedLoading?: boolean;
  currentPageKey?: null;
  items?: undefined[];
  then?: any;
  map?: any;
};
class ProgressiveLoadingList extends React.Component<{}, ProgressiveLoadingListState> {
  constructor(props) {
    super(props);
    requestData = requestData.bind(this);
    state = { isFinishedLoading: false, currentPageKey: null, items: [] };
  }
  requestData() {
    mockRequest(state.currentPageKey).then(response =>
      setState(prevState => ({
        isFinishedLoading: response.isFinalPage,
        currentPageKey: response.nextPageKey,
        items: prevState.items.concat(response.items),
      })),
    );
  }
  createItems() {
    return state.items.map(item => (
      <Item key={item.key}>
        <Placeholder title={item.title} className={cx('item-placeholder')} />
      </Item>
    ));
  }
  render() {
    return (
      <MyExampleContainer>
        <InfiniteList
          dividerStyle="standard"
          isFinishedLoading={state.isFinishedLoading}
          onRequestItems={requestData}
          initialLoadingIndicator={<MyInitialLoadingIndicator />}
          progressiveLoadingIndicator={<MyProgressiveLoadingIndicator />}
          progressiveLoadingMessage="Loading allergies..."
          ariaLabel="Progressive Loading List"
        >
          {createItems()}
        </InfiniteList>
      </MyExampleContainer>
    );
  }
}
export default ProgressiveLoadingList;
