import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './DialogModal.module.scss';
import { AbstractModal } from '../../abstract-modal/src/AbstractModal';
const cx = classNamesBind.bind(styles);
type widthFromSize = 320 | 640 | 960 | 1120 | 1280 | 1600;

interface Properties {
  /**
   * Aria Label of the dialog modal.
   */
  ariaLabel: string;
  /**
   * Header of the dialog modal.
   */
  header: JSX.Element;
  /**
   * Footer of the dialog modal.
   */
  footer: JSX.Element;
  /**
   * Contents of the dialog modal.
   */
  children?: JSX.Element;
  /**
   * Callback function indicating a close condition was met.
   */
  onRequestClose: () => void;
  /**
   * Toggle to show dialog modal or not.
   */
  isOpen?: boolean;
  /**
   * Width of the dialog modal. Allows one of `320`, `640`, `960`, `1120`, `1280`, or `1600`.
   *
   * _(Uses same sizes as terra-modal-manager?: tiny:320, small:640, medium:960, default:1120, large:1280, huge:1600)_
   */
  width?: widthFromSize;
  /**
   * If set to true, the modal will close when a mouse click is triggered outside the modal.
   */
  closeOnOutsideClick?: boolean;
  /**
   * Used to select the root mount DOM node. This is used to help prevent focus from shifting outside of the DialogModal when it is opened.
   */
  rootSelector?: string;
}
const defaultProps = {
  children: null,
  isOpen: false,
  width: 1120,
  closeOnOutsideClick: false,
  rootSelector: '#root',
};
export const DialogModal: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [extracted, customProps] = splitProps(props, ['header', 'footer', 'children', 'onRequestClose', 'isOpen', 'ariaLabel', 'width', 'closeOnOutsideClick', 'rootSelector']);

  const theme = useContext(ThemeContext);

  function getClassArray() {
    const classArray = ['dialog-modal-wrapper', theme.className];
    /*if (extracted.width in widthFromSize) {
      classArray.push(`width-${widthFromSize[extracted.width]}`);
    } else {
      classArray.push("width-1120");
    }*/
    return classArray;
  }

  return (
    <>
      {extracted.isOpen && (
        <AbstractModal
          ariaLabel={extracted.ariaLabel}
          role="dialog"
          classNameModal={cx(getClassArray())}
          isOpen={extracted.isOpen}
          onRequestClose={extracted.onRequestClose}
          zIndex="7000"
          closeOnOutsideClick={extracted.closeOnOutsideClick}
          rootSelector={extracted.rootSelector}
        >
          <div {...customProps} className={classNames(cx('dialog-modal-inner-wrapper'), customProps.className)}>
            <div className={cx('dialog-modal-container')}>
              <div>{extracted.header}</div>
              <div className={cx('dialog-modal-body')}>{extracted.children}</div>
              <div>{extracted.footer}</div>
            </div>
          </div>
        </AbstractModal>
      )}
    </>
  );
};
