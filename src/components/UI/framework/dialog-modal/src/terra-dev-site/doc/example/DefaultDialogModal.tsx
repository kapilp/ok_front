import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { Button } from '../../../../../../core/button/src/Button';
import { ActionFooter } from '../../../../../../core/action-footer/src/ActionFooter';
import { ActionHeader } from '../../../../../../core/action-header/src/ActionHeader';
import { DialogModal } from '../../../DialogModal';
type DefaultDialogModalState = {
  isOpen: boolean;
};
export const DefaultDialogModal = () => {
  const [state, setState] = createStore({
    isOpen: false,
  });
  const handleOpenModal = () => {
    setState({ isOpen: true });
  };
  const handleCloseModal = () => {
    setState({ isOpen: false });
  };

  const paraOne = [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
    'Maecenas molestie in lorem vel facilisis. Quisque ac enim nec lectus malesuada faucibus.',
    'Integer congue feugiat ultricies.',
    ' Nunc non mauris sed tellus cursus vestibulum nec quis ipsum.',
    'Vivamus ornare magna justo, et volutpat tortor congue ut. Nulla facilisi.',
    ' Cras in venenatis turpis. Nullam id odio justo. Etiam vehicula lectus vel purus consectetur cursus id sit amet diam.',
    'Donec facilisis dui non orci hendrerit pharetra. Suspendisse blandit dictum turpis, in consectetur ipsum hendrerit eget.',
    'Nam vehicula, arcu vitae egestas porttitor,',
    'turpis nisi pulvinar neque, ut lacinia urna purus sit amet elit.',
  ];
  return (
    <div>
      <DialogModal
        ariaLabel="Default Dialog Modal"
        isOpen={state.isOpen}
        onRequestClose={handleCloseModal}
        header={<ActionHeader title="Action Header used here" onClose={handleCloseModal} />}
        footer={<ActionFooter start="Action Footer used here" />}
      >
        <p>{paraOne}</p>
      </DialogModal>
      <Button text="Trigger Dialog Modal" onClick={handleOpenModal} />
    </div>
  );
};
