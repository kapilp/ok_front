import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import ActionHeader from 'terra-action-header';
import ActionFooter from 'terra-action-footer';
import Popup from 'terra-popup';
import classNames from 'classnames/bind';
import DialogModal from '../../../DialogModal';
import styles from './DialogModalWithPopup.test.module.scss';
const cx = classNames.bind(styles);
type DefaultDialogModalState = {
  isOpen?: boolean;
  isPopupOpen?: boolean;
};
class DefaultDialogModal extends React.Component<{}, DefaultDialogModalState> {
  parentNode: any;
  constructor() {
    super();
    state = {
      isOpen: false,
      isPopupOpen: false,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
  }
  setButtonNode(node) {
    parentNode = node;
  }
  getButtonNode() {
    return parentNode;
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  handleButtonClick() {
    setState({ isPopupOpen: true });
  }
  handleRequestClose() {
    setState({ isPopupOpen: false });
  }
  render() {
    const text = [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      'Maecenas molestie in lorem vel facilisis. Quisque ac enim nec lectus malesuada faucibus.',
      'Integer congue feugiat ultricies.',
      ' Nunc non mauris sed tellus cursus vestibulum nec quis ipsum.',
      'Vivamus ornare magna justo, et volutpat tortor congue ut. Nulla facilisi.',
      ' Cras in venenatis turpis. Nullam id odio justo. Etiam vehicula lectus vel purus consectetur cursus id sit amet diam.',
      'Donec facilisis dui non orci hendrerit pharetra. Suspendisse blandit dictum turpis, in consectetur ipsum hendrerit eget.',
      'Nam vehicula, arcu vitae egestas porttitor,',
      'turpis nisi pulvinar neque, ut lacinia urna purus sit amet elit.',
    ];
    return (
      <div>
        <DialogModal
          ariaLabel="Dialog Modal"
          isOpen={state.isOpen}
          onRequestClose={handleCloseModal}
          header={<ActionHeader title="Action Header used here" onClose={handleCloseModal} />}
          footer={<ActionFooter start="Footer Goes here" />}
        >
          <p>{text}</p>
          <Popup isOpen={state.isPopupOpen} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
            <div className={cx('popup-content')}>
              <p>{text}</p>
              <button type="button">Test Button</button>
              <p>{text}</p>
            </div>
          </Popup>
          <Button text="Toggle popup" onClick={handleButtonClick} ref={setButtonNode} />
          <p>{text}</p>
        </DialogModal>
        <Button id="trigger-dialog-modal" text="Trigger Dialog Modal" onClick={handleOpenModal} />
      </div>
    );
  }
}
export default DefaultDialogModal;
