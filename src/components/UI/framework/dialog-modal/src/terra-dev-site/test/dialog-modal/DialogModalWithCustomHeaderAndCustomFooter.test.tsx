import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import classNames from 'classnames/bind';
import DialogModal from '../../../DialogModal';
import styles from './DialogModalWithCustomHeaderAndCustomFooter.test.module.scss';
const cx = classNames.bind(styles);
type DialogModalWithCustomHeaderAndCustomFooterState = {
  isOpen?: boolean;
};
class DialogModalWithCustomHeaderAndCustomFooter extends React.Component<{}, DialogModalWithCustomHeaderAndCustomFooterState> {
  constructor() {
    super();
    state = {
      isOpen: false,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    const text = [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      'Maecenas molestie in lorem vel facilisis. Quisque ac enim nec lectus malesuada faucibus.',
      'Integer congue feugiat ultricies.',
      ' Nunc non mauris sed tellus cursus vestibulum nec quis ipsum.',
      'Vivamus ornare magna justo, et volutpat tortor congue ut. Nulla facilisi.',
      ' Cras in venenatis turpis. Nullam id odio justo. Etiam vehicula lectus vel purus consectetur cursus id sit amet diam.',
      'Donec facilisis dui non orci hendrerit pharetra. Suspendisse blandit dictum turpis, in consectetur ipsum hendrerit eget.',
      'Nam vehicula, arcu vitae egestas porttitor,',
      'turpis nisi pulvinar neque, ut lacinia urna purus sit amet elit.',
    ];
    return (
      <div>
        <DialogModal
          ariaLabel="Dialog Modal"
          isOpen={state.isOpen}
          onRequestClose={handleCloseModal}
          header={
            <div className={cx('header')}>
              Custom Header
              <Button id="close-dialog-modal" text="Close" className={cx('close-button')} onClick={handleCloseModal} />
            </div>
          }
          footer={<div className={cx('footer')}>Custom Footer</div>}
        >
          <p>{text}</p>
        </DialogModal>
        <Button id="trigger-dialog-modal" text="Trigger Dialog Modal" onClick={handleOpenModal} />
      </div>
    );
  }
}
export default DialogModalWithCustomHeaderAndCustomFooter;
