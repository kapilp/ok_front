import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
/* eslint-disable import/no-extraneous-dependencies */
import Button from 'terra-button';
import ActionHeader from 'terra-action-header';
import ActionFooter from 'terra-action-footer';
import DialogModal from '../../src/DialogModal';
/* eslint-enable import/no-extraneous-dependencies */
interface Properties {
  width: string;
}
interface IDialogModalExampleProps extends JSX.HTMLAttributes<Element> {
  width?: any;
}
type DialogModalExampleState = {
  isOpen?: boolean;
};
class DialogModalExample extends React.Component<IDialogModalExampleProps, DialogModalExampleState> {
  constructor() {
    super();
    state = {
      isOpen: false,
    };
    handleOpenModal = handleOpenModal.bind(this);
    handleCloseModal = handleCloseModal.bind(this);
  }
  handleOpenModal() {
    setState({ isOpen: true });
  }
  handleCloseModal() {
    setState({ isOpen: false });
  }
  render() {
    const paraOne = [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
      'Maecenas molestie in lorem vel facilisis. Quisque ac enim nec lectus malesuada faucibus.',
      'Integer congue feugiat ultricies.',
      ' Nunc non mauris sed tellus cursus vestibulum nec quis ipsum.',
      'Vivamus ornare magna justo, et volutpat tortor congue ut. Nulla facilisi.',
      ' Cras in venenatis turpis. Nullam id odio justo. Etiam vehicula lectus vel purus consectetur cursus id sit amet diam.',
      'Donec facilisis dui non orci hendrerit pharetra. Suspendisse blandit dictum turpis, in consectetur ipsum hendrerit eget.',
      'Nam vehicula, arcu vitae egestas porttitor,',
      'turpis nisi pulvinar neque, ut lacinia urna purus sit amet elit.',
    ];
    return (
      <div>
        <DialogModal
          ariaLabel="Dialog Modal Example"
          isOpen={state.isOpen}
          onRequestClose={handleCloseModal}
          header={<ActionHeader title="Action Header used here" onClose={handleCloseModal} />}
          footer={<ActionFooter start="Footer Goes here" />}
          width={props.width}
        >
          <p>{paraOne}</p>
        </DialogModal>
        <Button text="Trigger Dialog Modal" onClick={handleOpenModal} />
      </div>
    );
  }
}
export default DialogModalExample;
