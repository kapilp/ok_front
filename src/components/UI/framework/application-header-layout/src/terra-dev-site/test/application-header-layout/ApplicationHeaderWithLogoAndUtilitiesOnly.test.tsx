import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ApplicationHeaderLayout from '../../../ApplicationHeaderLayout';
const ApplicationHeaderWithLogoAndUtilitiesOnly = () => (
  <ApplicationHeaderLayout id="test-header" logo={<div id="test-logo">Logo&nbsp;</div>} utilities={<div id="test-utilities">Utilities&nbsp;</div>} />
);
export default ApplicationHeaderWithLogoAndUtilitiesOnly;
