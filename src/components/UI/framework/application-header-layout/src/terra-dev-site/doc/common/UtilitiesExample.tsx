import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { PlaceHolder } from './Placeholder';
interface Properties {
  size: string;
}
export const UtilitiesExample: Component<Properties> = (props: Properties) => {
  function getText() {
    let text = 'Utilities';
    if (props.size === 'tiny') {
      text = 'U';
    } else if (props.size === 'small') {
      text = 'Util';
    }
    return text;
  }
  return <PlaceHolder text={getText()} size={props.size} />;
};
