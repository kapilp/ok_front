import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './HeaderWireframe.module.scss';
import { ResponsiveElement } from '../../../../../../core/responsive-element/src/ResponsiveElement';
import { ApplicationHeaderLayout } from '../../../ApplicationHeaderLayout';
import { LogoExample } from '../common/LogoExample';
import { ExtensionsExample } from '../common/ExtensionsExample';
import { NavigationExample } from '../common/NavigationExample';
import { UtilitiesExample } from '../common/UtilitiesExample';
import { ToggleExample } from '../common/ToggleExample';
const cx = classNames.bind(styles);
export const HeaderWireframe = () => {
  const tinyHeader = (
    <ApplicationHeaderLayout
      logo={<LogoExample size="tiny" />}
      utilities={<UtilitiesExample size="tiny" />}
      extensions={<ExtensionsExample size="tiny" />}
      navigation={<NavigationExample size="tiny" />}
      toggle={<ToggleExample size="tiny" />}
    />
  );
  const mediumHeader = (
    <ApplicationHeaderLayout
      logo={<LogoExample size="small" />}
      utilities={<UtilitiesExample size="small" />}
      extensions={<ExtensionsExample size="small" />}
      navigation={<NavigationExample size="small" />}
      toggle={<ToggleExample size="small" />}
    />
  );
  const largeHeader = (
    <ApplicationHeaderLayout
      logo={<LogoExample size="medium" />}
      utilities={<UtilitiesExample size="medium" />}
      extensions={<ExtensionsExample size="medium" />}
      navigation={<NavigationExample size="medium" />}
      toggle={<ToggleExample size="medium" />}
    />
  );
  return (
    <div className={cx('content-wrapper')}>
      <div className={cx('responsive-element-wrapper')}>
        <ResponsiveElement tiny={tinyHeader} medium={mediumHeader} large={largeHeader} />
      </div>
    </div>
  );
};
