import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { PlaceHolder } from './Placeholder';
interface Properties {
  size: string;
}
export const NavigationExample: Component<Properties> = (props: Properties) => {
  function getText() {
    let text = 'Navigation';
    if (props.size === 'tiny') {
      text = 'N';
    } else if (props.size === 'small') {
      text = 'Nav';
    }
    return text;
  }

  return <PlaceHolder text={getText()} size="default" />;
};
