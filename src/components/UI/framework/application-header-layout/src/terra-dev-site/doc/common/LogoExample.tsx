import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { PlaceHolder } from './Placeholder';
interface Properties {
  size: string;
}
export const LogoExample: Component<Properties> = (props: Properties) => {
  function getText() {
    let text = 'Logo';
    if (props.size === 'tiny') {
      text = 'L';
    } else if (props.size === 'small') {
      text = 'Log';
    }
    return text;
  }

  return <PlaceHolder text={getText()} size={props.size} />;
};
