import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { PlaceHolder } from './Placeholder';
interface Properties {
  size: string;
}
export const ExtensionsExample: Component<Properties> = (props: Properties) => {
  function getText() {
    let text = 'Extensions';
    if (props.size === 'tiny') {
      text = 'E';
    } else if (props.size === 'small') {
      text = 'Ext';
    }
    return text;
  }

  return <PlaceHolder text={getText()} size={props.size} />;
};
