import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
// import { injectIntl, intlShape } from "react-intl";
import styles from './ApplicationHeaderLayout.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * Extensions element to be placed before the end of the header.
   * */
  extensions?: JSX.Element;
  /**
   * Logo element to be placed at the start of the header after the toggle element.
   * */
  logo?: JSX.Element;
  /**
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  // intl?:intlShape.isRequired, // Todo fix this
  /**
   * Navigation element to be placed within the fill area of the header.
   * */
  navigation?: JSX.Element;
  /**
   * Toggle element to be placed at the start of the header.
   * */
  toggle?: JSX.Element;
  /**
   * Utilities element to be placed at the end of the header.
   * */
  utilities?: JSX.Element;
}
export const ApplicationHeaderLayout: Component<Properties> = props => {
  const [extracted, customProps] = splitProps(props, ['extensions', 'logo', 'navigation', 'intl', 'toggle', 'utilities']);
  const theme = useContext(ThemeContext);

  const logoElement = <>{extracted.logo && <div className={cx('fit', 'start', 'logo')}>{extracted.logo}</div>}</>;
  const navigationElement = (
    <>
      {extracted.navigation && (
        <nav role="navigation" className={cx('fill')}>
          {extracted.navigation}
        </nav>
      )}
    </>
  );
  const extensionsElement = <>{extracted.extensions && <div className={cx('fit', 'end', 'extensions')}>{extracted.extensions}</div>} </>;
  const utilitiesElement = <>{extracted.utilities && <div className={cx('fit', 'end', 'utilities')}>{extracted.utilities}</div>}</>;
  const headerToggle = <>{extracted.toggle && <div className={cx('fit')}>{extracted.toggle}</div>}</>;
  const headerInner = (
    <div className={cx('fill', 'header-inner')}>
      {navigationElement}
      {extensionsElement}
    </div>
  );
  const skipToContent = () => {
    const mainContainer = document.querySelector(['[data-terra-layout-main]']);
    if (mainContainer) {
      mainContainer.setAttribute('tabindex', '-1');
      mainContainer.focus();
      mainContainer.removeAttribute('tabindex');
    }
  };
  const skipToContentButton = (
    <button type="button" onClick={skipToContent} className={cx('skip-content')}>
      Skip to Content
    </button>
  );
  let headerBody;
  if (headerInner || logoElement || utilitiesElement) {
    headerBody = (
      <div className={cx('fill', 'header-body')}>
        {skipToContentButton}
        {logoElement}
        {headerInner}
        {utilitiesElement}
      </div>
    );
  }
  return (
    <div {...customProps} className={classNames(cx('header', 'fill', theme.className), customProps.className)}>
      {headerToggle}
      {headerBody}
    </div>
  );
};
// export default injectIntl(ApplicationHeaderLayout);
