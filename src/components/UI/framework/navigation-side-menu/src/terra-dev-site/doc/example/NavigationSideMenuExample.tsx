import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import NavigationSideMenu from 'terra-navigation-side-menu';
import styles from './NavigationSideMenuExample.module.scss';
const cx = classNames.bind(styles);
type NavigationSideMenuDefaultState = {
  selectedMenuKey?: string;
  selectedChildKey?: undefined;
};
class NavigationSideMenuDefault extends React.Component<{}, NavigationSideMenuDefaultState> {
  constructor(props) {
    super(props);
    handleOnChange = handleOnChange.bind(this);
    resetMenuState = resetMenuState.bind(this);
    fakeRoutingBack = fakeRoutingBack.bind(this);
    state = { selectedMenuKey: 'menu', selectedChildKey: undefined };
  }
  handleOnChange(event, changeData) {
    setState({
      selectedMenuKey: changeData.selectedMenuKey,
      selectedChildKey: changeData.selectedChildKey,
    });
  }
  resetMenuState() {
    setState({ selectedMenuKey: 'menu', selectedChildKey: undefined });
  }
  fakeRoutingBack() {
    setState({
      selectedMenuKey: 'fake-parent',
      selectedChildKey: undefined,
    });
  }
  render() {
    let content;
    if (state.selectedMenuKey === 'fake-parent') {
      content = (
        <div className={cx('content')}>
          <button type="button" onClick={resetMenuState}>
            Child Route
          </button>
          <p>Parent Route</p>
        </div>
      );
    } else {
      content = (
        <NavigationSideMenu
          id="test-menu"
          menuItems={[
            {
              key: 'menu',
              text: 'Menu',
              childKeys: ['submenu1', 'submenu2', 'submenu3', 'submenu4'],
            },
            {
              key: 'submenu1',
              text: 'Sub Menu 1',
              childKeys: ['subsubmenu1', 'subsubmenu2', 'subsubmenu3'],
              id: 'test-item-1',
            },
            { key: 'submenu2', text: 'Sub Menu 2' },
            { key: 'submenu3', text: 'Sub Menu 3' },
            { key: 'submenu4', text: 'Sub Menu 4' },
            { key: 'subsubmenu1', text: 'Sub-Sub Menu 1', id: 'test-item-2' },
            { key: 'subsubmenu2', text: 'Sub-Sub Menu 2' },
            { key: 'subsubmenu3', text: 'Sub-Sub Menu 3' },
          ]}
          onChange={handleOnChange}
          routingStackBack={fakeRoutingBack}
          selectedMenuKey={state.selectedMenuKey}
          selectedChildKey={state.selectedChildKey}
        />
      );
    }
    return <div className={cx('content-wrapper')}>{content}</div>;
  }
}
export default NavigationSideMenuDefault;
