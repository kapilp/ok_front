import {  JSX,  mergeProps, splitProps, useContext   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { injectIntl, intlShape } from "react-intl";
import classNames from "classnames/bind";
import ActionHeader from "terra-action-header";
import ContentContainer from "terra-content-container";
import VisuallyHiddenText from "terra-visually-hidden-text";
import * as KeyCode from "keycode-js";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import MenuItem from "./_MenuItem";
import styles from "./NavigationSideMenu.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * @private
   * Internationalization object with translation APIs. Provided by `injectIntl`.
   */
  intl: intlShape.isRequired,
  /**
   * An array of configuration for each menu item.
   */
  menuItems: PropTypes.arrayOf(
    PropTypes.shape({
      /**
       * Keys of menu items
       */
      childKeys: PropTypes.arrayOf(string),
      /**
       * Used to match visual style of a menuItem with children on an item without children.
       */
      hasSubMenu: boolean,
      /**
       * Whether or not the menu is the primary navigation links in small form factor.
       */
      isRootMenu: boolean,
      /**
       * ID to be applied to the menu item div.
       */
      id: string,
      /**
       * Optional meta data to be returned along with the item key within the onChange.
       */
      metaData: PropTypes.object,
      /**
       * Unique identifier that will be returned in the onChange callback when an endpoint is reached.
       */
      key: string.isRequired,
      /**
       * Text for the menu row and header title when selected.
       */
      text: string.isRequired
    })
  ),
  /**
   * Callback function when a menu endpoint is reached.
   * returns (event, { selectedMenuKey: String, selectedChildKey: String, metaData: Object})
   */
  onChange: PropTypes.func.isRequired,
  /**
   * Delegate prop showParent function that is provided by the terra-navigation-layout.
   */
  routingStackBack: PropTypes.func,
  /**
   * Key of the currently selected child item on the selected menu page.
   * This is used when traveling back up the menu stack or when the child is an end point.
   */
  selectedChildKey: string,
  /**
   * Key of the currently selected menu page.
   */
  selectedMenuKey: string.isRequired,
  /**
   * An optional toolbar to display below the side menu action header
   */
  toolbar: JSX.Element
};
const defaultProps = {
  menuItems: []
};
const processMenuItems = menuItems => {
  const items = {};
  const parents = {};
  menuItems.forEach(item => {
    items[item.key] = {
      id: item.id,
      text: item.text,
      childKeys: item.childKeys,
      metaData: item.metaData,
      hasSubMenu: item.hasSubMenu,
      isRootMenu: item.isRootMenu
    };
    if (item.childKeys) {
      item.childKeys.forEach(key => {
        parents[key] = item.key;
      });
    }
  });
  return { items, parents };
};
interface INavigationSideMenuProps extends JSX.HTMLAttributes<Element> {
  menuItems?: any;
  onChange?: any;
  routingStackBack?: any;
  selectedChildKey?: any;
  selectedMenuKey?: any;
  toolbar?: any;
  customProps?: any;
  intl?: any;
}
type NavigationSideMenuState = {
  items?: {},
  parents?: {},
  prevPropsMenuItem?: any,
  text?: any
};
class NavigationSideMenu extends Component<
  INavigationSideMenuProps,
  NavigationSideMenuState
> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  context: any;
  visuallyHiddenComponent: string;
  constructor(props) {
    super(props);
    handleBackClick = handleBackClick.bind(this);
    handleItemClick = handleItemClick.bind(this);
    updateAriaLiveContent = updateAriaLiveContent.bind(this);
    setVisuallyHiddenComponent = setVisuallyHiddenComponent.bind(
      this
    );
    const { items, parents } = processMenuItems(props.menuItems);
    state = {
      items,
      parents,
      prevPropsMenuItem: props.menuItems
    };
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.menuItems !== prevState.prevPropsMenuItem) {
      return processMenuItems(nextProps.menuItems);
    }
    return null;
  }
  setVisuallyHiddenComponent(node) {
    visuallyHiddenComponent = node;
  }
  handleBackClick(event) {
    const parentKey = state.parents[props.selectedMenuKey];
    if (parentKey) {
      props.onChange(event, {
        selectedMenuKey: parentKey,
        selectedChildKey: props.selectedMenuKey,
        metaData: state.items[parentKey].metaData
      });
    }
  }
  handleItemClick(event, key) {
    const selectedItem = state.items[key];
    if (state.items[key] && state.items[key].text) {
      updateAriaLiveContent(state.items[key].text);
    }
    if (selectedItem.childKeys && selectedItem.childKeys.length) {
      props.onChange(event, {
        selectedMenuKey: key,
        selectedChildKey: undefined,
        metaData: selectedItem.metaData
      });
    } else {
      props.onChange(event, {
        selectedMenuKey: props.selectedMenuKey,
        selectedChildKey: key,
        metaData: selectedItem.metaData
      });
    }
  }
  buildListItem(key) {
    const item = state.items[key];
    const onKeyDown = event => {
      if (
        event.keyCode === KeyCode.KEY_SPACE ||
        event.keyCode === KeyCode.KEY_RETURN
      ) {
        event.preventDefault();
        handleItemClick(event, key);
      }
    };
    return (
      <MenuItem
        id={item.id}
        hasChevron={
          item.hasSubMenu || (item.childKeys && item.childKeys.length > 0)
        }
        isSelected={key === props.selectedChildKey}
        text={item.text}
        key={key}
        onClick={event => {
          handleItemClick(event, key);
        }}
        onKeyDown={onKeyDown}
        data-menu-item={key}
      />
    );
  }
  buildListContent(currentItem) {
    if (currentItem && currentItem.childKeys && currentItem.childKeys.length) {
      return (
        <nav role="navigation">
          <ul role="menu" className={cx(["side-menu-list"])}>
            {currentItem.childKeys.map(key => buildListItem(key))}
          </ul>
        </nav>
      );
    }
    return null;
  }
  updateAriaLiveContent(item) {
    const { intl } = props;
    const selected = intl.formatMessage({
      id: "Terra.navigation.side.menu.selected"
    });
    // Guard against race condition with the ref being established and updating the ref's innerText
    if (!visuallyHiddenComponent) {
      return;
    }
    visuallyHiddenComponent.innerText = item ? `${item} ${selected}` : "";
  }
  render() {
    const {
      menuItems,
      onChange,
      routingStackBack,
      selectedChildKey,
      selectedMenuKey,
      toolbar,
      ...customProps
    } = props;
    const currentItem = state.items[selectedMenuKey];
    const theme = context;
    let sideMenuContentContainerClassNames = cx([
      "side-menu-content-container",
      theme.className
    ]);
    let onBack;
    const parentKey = state.parents[selectedMenuKey];
    if (parentKey) {
      onBack = handleBackClick;
    } else {
      onBack = routingStackBack;
    }
    let header;
    if (onBack || !currentItem.isRootMenu) {
      header = (
        <Fragment>
          <ActionHeader
            className={cx("side-menu-action-header")}
            onBack={onBack}
            title={currentItem ? currentItem.text : null}
            data-navigation-side-menu-action-header
          />
          {toolbar}
        </Fragment>
      );
    } else {
      sideMenuContentContainerClassNames = cx([
        "side-menu-content-container",
        "is-root"
      ]);
    }
    return (
      <Fragment>
        <VisuallyHiddenText
          aria-atomic="true"
          aria-live="assertive"
          aria-relevant="additions text"
          ref={setVisuallyHiddenComponent}
        />
        <ContentContainer
          {...customProps}
          header={header}
          fill
          className={sideMenuContentContainerClassNames}
        >
          {buildListContent(currentItem)}
        </ContentContainer>
      </Fragment>
    );
  }
}

export default injectIntl(NavigationSideMenu);
