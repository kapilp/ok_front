import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { injectIntl, intlShape } from 'react-intl';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import * as KeyCode from 'keycode-js';
import ChevronRight from 'terra-icon/lib/icon/IconChevronRight';
import VisuallyHiddenText from 'terra-visually-hidden-text';
import styles from './MenuItem.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Whether or not the menu item should display a disclosure indicator.
   * */
  hasChevron: boolean;
  /**
   * @private
   * Internationalization object with translation APIs. Provided by `injectIntl`.
   */
  intl: intlShape.isRequired;
  /**
   * Whether or not the menu item is selection.
   * */
  isSelected: boolean;
  /**
   * Callback function triggered when key is pressed.
   */
  onKeyDown: PropTypes.func;
  /**
   * Callback function triggered when key is released.
   */
  onKeyUp: PropTypes.func;
  /**
   * Text display for the menu item.
   * */
  text: string;
}
interface IMenuItemProps extends JSX.HTMLAttributes<Element> {
  hasChevron?: any;
  intl?: any;
  isSelected?: any;
  text?: any;
  customProps?: any;
  onKeyUp?: any;
  onKeyDown?: any;
}
type MenuItemState = {
  active?: boolean;
  focused?: boolean;
};
class MenuItem extends React.Component<IMenuItemProps, MenuItemState> {
  theme = useContext(ThemeContext);
  context: any;
  constructor(props) {
    super(props);
    state = { active: false, focused: false };
    handleKeyDown = handleKeyDown.bind(this);
    handleKeyUp = handleKeyUp.bind(this);
    handleOnBlur = handleOnBlur.bind(this);
    textRender = textRender.bind(this);
  }
  handleOnBlur() {
    setState({ focused: false });
  }
  handleKeyDown(event) {
    // Add active state to FF browsers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: true });
    }
    // Add focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_RETURN) {
      setState({ focused: true });
    }
    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  }
  handleKeyUp(event) {
    // Remove active state from FF broswers
    if (event.keyCode === KeyCode.KEY_SPACE) {
      setState({ active: false });
    }
    // Apply focus styles for keyboard navigation
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ focused: true });
    }
    if (props.onKeyUp) {
      props.onKeyUp(event);
    }
  }
  textRender() {
    const { intl, isSelected, text } = props;
    const selected = intl.formatMessage({
      id: 'Terra.navigation.side.menu.selected',
    });
    if (isSelected) {
      return (
        <Fragment>
          {text}
          <VisuallyHiddenText text={selected} />
        </Fragment>
      );
    }
    return text;
  }
  render() {
    const { hasChevron, intl, isSelected, text, ...customProps } = props;
    const theme = context;
    const itemClassNames = classNames(
      cx('menu-item', { 'is-selected': isSelected }, { 'is-active': state.active }, { 'is-focused': state.focused }, theme.className),
      customProps.className,
    );
    return (
      <li className={cx('list-item')} role="none">
        <div
          role="menuitem"
          {...customProps}
          tabIndex="0"
          className={itemClassNames}
          onKeyDown={handleKeyDown}
          onKeyUp={handleKeyUp}
          onBlur={handleOnBlur}
          aria-haspopup={hasChevron}
        >
          <div className={cx('title')}>{textRender()}</div>
          {hasChevron && (
            <span className={cx('chevron')}>
              <ChevronRight />
            </span>
          )}
        </div>
      </li>
    );
  }
}

export default injectIntl(MenuItem);
