import {  JSX, Component, createEffect, createSignal, onCleanup, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import * as KeyCode from 'keycode-js';

// import ResizeObserver from "resize-observer-polyfill";
import styles from './HookshotContent.module.scss';
// import 'classlist-polyfill';
import { useOnclickOutside } from '../../../CoolOnclickoutside/CoolOnclickoutside'; // Full polyfill for browsers with no classList support
const cx = classNames.bind(styles);
interface IHookshotContentProps extends JSX.HTMLAttributes<Element> {
  /**
   * The children to be displayed as content within the content.
   */
  children: JSX.Element;
  /**
   * The function callback when a child content resize event occurs.
   */
  onContentResize?: () => void;
  /**
   * The function callback when am escape keydown event occurs.
   */
  onEsc?: (e: Event) => void;
  /**
   * The function callback when a click outside event occurs.
   */
  onOutsideClick?: (el: Event) => void;
  /**
   * The function callback when a resize event occurs.
   */
  onResize?: (e: Event) => void;
  /**
   * The function returning the frame html reference.
   */
  ref?: (el: HTMLDivElement) => void;
}

export const HookshotContent: Component<IHookshotContentProps> = (props: IHookshotContentProps) => {
  let animationFrameID: null;
  const [contentNode, setContentNode] = createSignal<HTMLDivElement>();
  let contentResizeListenerAdded: boolean;
  let escListenerAdded: boolean;
  let resizeListenerAdded: boolean;
  let resizeObserver: any;

  createEffect(() => {
    contentNode() && setTimeout(updateListeners);
  });
  onCleanup(() => {
    disableEscListener();
    disableResizeListener();
    disableContentResizeListener();
  });
  let handleResize = (event: Event) => {
    if (props.onResize) {
      props.onResize(event);
    }
  };
  const debounce = (fn, delay: number) => {
    let timer = null;
    return (...args) => {
      const context = this;
      clearTimeout(timer);
      timer = setTimeout(() => {
        fn.apply(context, args);
      }, delay);
    };
  };
  handleResize = debounce(handleResize.bind(this), 100);
  const handleClickOutside = (event: Event) => {
    if (props.onOutsideClick) {
      props.onOutsideClick(event);
    }
  };
  const handleKeydown = (event: Event) => {
    if (event.keyCode === KeyCode.KEY_ESCAPE) {
      event.preventDefault();
      props.onEsc && props.onEsc(event);
    }
  };
  const updateListeners = () => {
    if (props.onEsc) {
      enableEscListener();
    } else {
      disableEscListener();
    }
    if (props.onResize) {
      enableResizeListener();
    } else {
      disableResizeListener();
    }
    if (props.onContentResize) {
      enableContentResizeListener();
    } else {
      disableContentResizeListener();
    }
  };
  const enableEscListener = () => {
    if (!escListenerAdded) {
      document.addEventListener('keydown', handleKeydown);
      escListenerAdded = true;
    }
  };
  const disableEscListener = () => {
    if (escListenerAdded) {
      document.removeEventListener('keydown', handleKeydown);
      escListenerAdded = false;
    }
  };
  const enableResizeListener = () => {
    if (!resizeListenerAdded) {
      window.addEventListener('resize', handleResize);
      resizeListenerAdded = true;
    }
  };
  const disableResizeListener = () => {
    if (resizeListenerAdded) {
      window.removeEventListener('resize', handleResize);
      resizeListenerAdded = false;
    }
  };
  const enableContentResizeListener = () => {
    if (!contentResizeListenerAdded) {
      resizeObserver = new ResizeObserver(entries => {
        animationFrameID = window.requestAnimationFrame(() => {
          props.onContentResize(entries[0].contentRect);
        });
      });
      resizeObserver.observe(contentNode());
      contentResizeListenerAdded = true;
    }
  };
  const disableContentResizeListener = () => {
    if (contentResizeListenerAdded) {
      window.cancelAnimationFrame(animationFrameID);
      resizeObserver.disconnect(contentNode());
      setContentNode(null);
      contentResizeListenerAdded = false;
    }
  };

  const [p, customProps] = splitProps(props, [
    'children',
    'onContentResize',
    'onEsc',
    'onOutsideClick',
    'onResize',
    'ref',
    // Delete the unnecessary props that come from react-onclickoutside.
    'disableOnClickOutside',
    'enableOnClickOutside',
    'eventTypes',
    'excludeScrollbar',
    'outsideClickIgnoreClass',
    'preventDefault',
    'stopPropagation',
    // Delete the closePortal prop that comes from react-portal.
    'closePortal',
  ]);
  const refOutsideClick = useOnclickOutside(e => {
    handleClickOutside(e);
  });
  return (
    <div
      {...customProps}
      className={cx(['content', customProps.className])}
      ref={(el: HTMLDivElement) => {
        setContentNode(el);
        refOutsideClick(el);
        if (p.ref) {
          p.ref(el);
        }
      }}
    >
      {p.children}
    </div>
  );
};
//const onClickOutsideContent = onClickOutside(HookshotContent);
//export default onClickOutsideContent;
