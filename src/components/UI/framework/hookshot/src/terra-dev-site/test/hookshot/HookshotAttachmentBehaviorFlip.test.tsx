import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotAttachmentBehaviorFlipState = {
  position?: string;
};
class HookshotAttachmentBehaviorFlip extends React.Component<{}, HookshotAttachmentBehaviorFlipState> {
  constructor(props) {
    super(props);
    handlePositionClick = handlePositionClick.bind(this);
    state = { position: 'primary' };
  }
  handlePositionClick(event) {
    setState({ position: event.target.value });
  }
  render() {
    let type = 'primary';
    if (state.position === 'flipped') {
      type = 'flip-flipped';
    } else if (state.position === 'pushed') {
      type = 'flip-pushed';
    }
    return (
      <div>
        <HookshotTemplate attachmentBehavior="flip" id="attachment-behavior-flip" isOpen={false} type={type} />
        <p> Choose the positioning behavior </p>
        <p> Primary position is on the middle right</p>
        <button type="button" id="position-primary" value="primary" onClick={handlePositionClick}>
          Primary
        </button>
        <button type="button" id="position-flipped" value="flipped" onClick={handlePositionClick}>
          Flipped
        </button>
        <button type="button" id="position-pushed" value="pushed" onClick={handlePositionClick}>
          Pushed by bounds
        </button>
      </div>
    );
  }
}
export default HookshotAttachmentBehaviorFlip;
