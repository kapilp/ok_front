import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotAttachmentBehaviorPushState = {
  position?: string;
};
class HookshotAttachmentBehaviorPush extends React.Component<{}, HookshotAttachmentBehaviorPushState> {
  constructor(props) {
    super(props);
    state = { position: 'primary' };
    handlePositionClick = handlePositionClick.bind(this);
  }
  handlePositionClick(event) {
    setState({ position: event.target.value });
  }
  render() {
    let type = 'primary';
    if (state.position === 'pushed') {
      type = 'push-pushed';
    }
    return (
      <div>
        <HookshotTemplate attachmentBehavior="push" id="attachment-behavior-push" isOpen={false} type={type} />
        <p>Choose the positioning behavior</p>
        <p>Primary position is on the middle right</p>
        <button type="button" id="position-primary" value="primary" onClick={handlePositionClick}>
          Primary
        </button>
        <button type="button" id="position-pushed" value="pushed" onClick={handlePositionClick}>
          Pushed by bounds
        </button>
      </div>
    );
  }
}
export default HookshotAttachmentBehaviorPush;
