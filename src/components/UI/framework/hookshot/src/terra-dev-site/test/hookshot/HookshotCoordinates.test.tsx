import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Hookshot from '../../../Hookshot';
import HookshotContent from '../common/HookshotContentTestTemplate';
import styles from '../common/HookshotTestDocCommon.module.scss';
const cx = classNames.bind(styles);
type HookshotTemplateState = {
  open?: boolean;
};
class HookshotTemplate extends React.Component<{}, HookshotTemplateState> {
  constructor(props) {
    super(props);
    triggerHookshot = triggerHookshot.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    state = { open: false };
  }
  triggerHookshot() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div id="coords-test" className={cx('hookshot-wrapper')}>
        <Hookshot contentAttachment={{ vertical: 'top', horizontal: 'start' }} targetCoordinates={{ x: 50, y: 132 }} isEnabled isOpen={state.open}>
          <HookshotContent id="test-coords-content" onEsc={handleRequestClose} onOutsideClick={handleRequestClose} />
        </Hookshot>
        <button type="button" id="coords-button" onClick={triggerHookshot}>
          Trigger Hookshot
        </button>
      </div>
    );
  }
}
export default HookshotTemplate;
