import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotExampleState = {
  attachment?: string;
};
class HookshotExample extends React.Component<{}, HookshotExampleState> {
  constructor(props) {
    super(props);
    handleTargetAttachment = handleTargetAttachment.bind(this);
    state = { attachment: 'top start' };
  }
  handleTargetAttachment(event) {
    setState({ attachment: event.target.value });
  }
  render() {
    return (
      <div>
        <HookshotTemplate
          id="attachment"
          hookshotContentProps={{
            closeOnEsc: false,
            closeOnOutsideClick: false,
            closeOnResize: false,
          }}
          contentAttachment="top center"
          type="primary"
          targetAttachment={state.attachment}
          isOpen
        />
        <p> Content attachement: top center </p>
        <p> Choose the target attachement: </p>
        <button type="button" id="attach-TS" value="top start" onClick={handleTargetAttachment}>
          TOP START
        </button>
        <button type="button" id="attach-TC" value="top center" onClick={handleTargetAttachment}>
          TOP CENTER
        </button>
        <button type="button" id="attach-TE" value="top end" onClick={handleTargetAttachment}>
          TOP END
        </button>
        <button type="button" id="attach-MS" value="middle start" onClick={handleTargetAttachment}>
          MIDDLE START
        </button>
        <button type="button" id="attach-MC" value="middle center" onClick={handleTargetAttachment}>
          MIDDLE CENTER
        </button>
        <button type="button" id="attach-ME" value="middle end" onClick={handleTargetAttachment}>
          MIDDLE END
        </button>
        <button type="button" id="attach-BS" value="bottom start" onClick={handleTargetAttachment}>
          BOTTOM START
        </button>
        <button type="button" id="attach-BC" value="bottom center" onClick={handleTargetAttachment}>
          BOTTOM CENTER
        </button>
        <button type="button" id="attach-BE" value="bottom end" onClick={handleTargetAttachment}>
          BOTTOM END
        </button>
      </div>
    );
  }
}
export default HookshotExample;
