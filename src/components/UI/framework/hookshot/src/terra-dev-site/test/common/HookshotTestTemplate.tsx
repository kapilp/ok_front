import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Hookshot from '../../../Hookshot';
import HookshotContent from './HookshotContentTestTemplate';
import styles from './HookshotTestDocCommon.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The hookshot contents props. Defaults to closesOnEsc=true, closeOnOutsideClick=true & closeOnResize=true.
   * onRequestClose is provided by the template.
   */
  hookshotContentProps: PropTypes.any;
  id: string;
  isOpen: boolean;
  includeSvgs: boolean;
  contentAttachment: string;
  targetAttachment: string;
  type: string;
  attachmentMargin: PropTypes.number;
}
const defaultProps = {
  hookshotContentProps: {},
  id: 'hookshot',
  isOpen: false,
  includeSvgs: false,
  contentAttachment: 'middle end',
  targetAttachment: 'middle start',
};
const attachmentValues = attachment => {
  if (attachment === 'middle start') {
    return { vertical: 'middle', horizontal: 'start' };
  }
  if (attachment === 'middle end') {
    return { vertical: 'middle', horizontal: 'end' };
  }
  if (attachment === 'middle center') {
    return { vertical: 'middle', horizontal: 'center' };
  }
  if (attachment === 'top start') {
    return { vertical: 'top', horizontal: 'start' };
  }
  if (attachment === 'top end') {
    return { vertical: 'top', horizontal: 'end' };
  }
  if (attachment === 'top center') {
    return { vertical: 'top', horizontal: 'center' };
  }
  if (attachment === 'bottom start') {
    return { vertical: 'bottom', horizontal: 'start' };
  }
  if (attachment === 'bottom end') {
    return { vertical: 'bottom', horizontal: 'end' };
  }
  return { vertical: 'bottom', horizontal: 'center' };
};
interface IHookshotTemplateProps extends JSX.HTMLAttributes<Element> {
  hookshotContentProps?: any;
  id?: any;
  isOpen?: any;
  contentAttachment?: any;
  targetAttachment?: any;
  attachmentMargin?: any;
  includeSvgs?: any;
  type?: any;
  hookshotProps?: any;
}
type HookshotTemplateState = {
  open?: any;
};
class HookshotTemplate extends React.Component<IHookshotTemplateProps, HookshotTemplateState> {
  constructor(props) {
    super(props);
    triggerHookshot = triggerHookshot.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    state = { open: props.isOpen };
  }
  triggerHookshot() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    const { hookshotContentProps, id, isOpen, contentAttachment, targetAttachment, attachmentMargin, includeSvgs, type, ...hookshotProps } = props;
    const svgs = (
      <div>
        <svg id="svg1" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg" height="2em" width="2em">
          <path fill="#78C346" d="M24 0c13.3 0 24 10.7 24 24S37.3 48 24 48 0 37.3 0 24C0 10.8 10.7 0 23.9 0h.1z" />
          <path fill="#FFF" d="M20 36.4L6.7 23.1l3.6-3.6 9.7 9.9 17.7-17.9 3.6 3.6L20 36.4z" />
        </svg>
        <svg id="svg2" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg" height="2em" width="2em">
          <path
            fill="#B1B5B6"
            d="M24 0C10.7 0 0 10.7 0 24s10.7 24 24 24 24-10.7 24-24S37.3 0 24 0zm13.9 33.8l-2.1 2.1-1.2 1.2-1 .9-9.6-9.7-9.6 9.7-2.2-2.1-1.1-1.1-1-1 9.7-9.8-9.7-9.8 1-1 1.1-1.1 2.2-2.1 9.6 9.7 9.6-9.7 1 .9 1.2 1.2 2.1 2.1-9.7 9.8 9.7 9.8z"
          />
        </svg>
      </div>
    );
    return (
      <div id={`${id}-bounds`} className={cx(['wrapper', `hookshot-wrapper-${type}`])}>
        <Hookshot
          id={id}
          contentAttachment={attachmentValues(contentAttachment)}
          targetAttachment={attachmentValues(targetAttachment)}
          attachmentMargin={attachmentMargin}
          isEnabled
          isOpen={state.open}
          targetRef={() => document.getElementById(`trigger-${id}`)}
          boundingRef={() => document.getElementById(`${id}-bounds`)}
          {...hookshotProps}
        >
          <HookshotContent
            id={`${id}-content`}
            onEsc={hookshotContentProps.closeOnEsc ? handleRequestClose : undefined}
            onOutsideClick={hookshotContentProps.closeOnOutsideClick ? handleRequestClose : undefined}
            onResize={hookshotContentProps.closeOnResize ? handleRequestClose : undefined}
          />
        </Hookshot>
        <button type="button" id={`trigger-${id}`} className={cx(['content', `hookshot-${type}`])} onClick={triggerHookshot}>
          Trigger Hookshot
        </button>
        {includeSvgs && svgs}
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default HookshotTemplate;
