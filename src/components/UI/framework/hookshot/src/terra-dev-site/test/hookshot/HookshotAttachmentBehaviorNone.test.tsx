import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotAttachmentBehaviorNoneState = {
  position?: string;
};
class HookshotAttachmentBehaviorNone extends React.Component<{}, HookshotAttachmentBehaviorNoneState> {
  constructor(props) {
    super(props);
    handlePositionClick = handlePositionClick.bind(this);
    state = { position: 'primary' };
  }
  handlePositionClick(event) {
    setState({ position: event.target.value });
  }
  render() {
    let type = 'primary';
    if (state.position === 'offset') {
      type = 'none-offset';
    }
    return (
      <div>
        <HookshotTemplate attachmentBehavior="none" id="attachment-behavior-none" isOpen={false} type={type} />
        <p> Choose the positioning behavior </p>
        <p> Primary position is on the middle right</p>
        <button type="button" id="position-primary" value="primary" onClick={handlePositionClick}>
          Primary
        </button>
        <button type="button" id="position-offset" value="offset" onClick={handlePositionClick}>
          Offset
        </button>
      </div>
    );
  }
}
export default HookshotAttachmentBehaviorNone;
