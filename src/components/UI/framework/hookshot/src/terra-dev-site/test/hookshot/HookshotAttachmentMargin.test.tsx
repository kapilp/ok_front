import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotAttachmentMarginState = {
  attachment?: string;
};
class HookshotAttachmentMargin extends React.Component<{}, HookshotAttachmentMarginState> {
  constructor(props) {
    super(props);
    handleTargetAttachment = handleTargetAttachment.bind(this);
    state = { attachment: 'middle start' };
  }
  handleTargetAttachment(event) {
    setState({ attachment: event.target.value });
  }
  render() {
    let targetAttachment;
    const type = 'primary';
    if (state.attachment === 'middle start') {
      targetAttachment = 'middle end';
    } else if (state.attachment === 'middle end') {
      targetAttachment = 'middle start';
    } else if (state.attachment === 'middle center') {
      targetAttachment = 'middle center';
    } else if (state.attachment === 'top start') {
      targetAttachment = 'bottom end';
    } else if (state.attachment === 'top end') {
      targetAttachment = 'bottom start';
    } else if (state.attachment === 'top center') {
      targetAttachment = 'bottom center';
    } else if (state.attachment === 'bottom start') {
      targetAttachment = 'top end';
    } else if (state.attachment === 'bottom end') {
      targetAttachment = 'top start';
    } else if (state.attachment === 'bottom center') {
      targetAttachment = 'top center';
    }
    return (
      <div>
        <HookshotTemplate id="attachment-margin" attachmentMargin={10} targetAttachment={targetAttachment} contentAttachment={state.attachment} type={type} isOpen />
        <p> Shoulp apply attachment margin appropriately for all attachment points </p>
        <p> Choose the content attachement: </p>
        <button type="button" id="attach-TS" value="top start" onClick={handleTargetAttachment}>
          TOP START
        </button>
        <button type="button" id="attach-TC" value="top center" onClick={handleTargetAttachment}>
          TOP CENTER
        </button>
        <button type="button" id="attach-TE" value="top end" onClick={handleTargetAttachment}>
          TOP END
        </button>
        <button type="button" id="attach-MS" value="middle start" onClick={handleTargetAttachment}>
          MIDDLE START
        </button>
        <button type="button" id="attach-MC" value="middle center" onClick={handleTargetAttachment}>
          MIDDLE CENTER
        </button>
        <button type="button" id="attach-ME" value="middle end" onClick={handleTargetAttachment}>
          MIDDLE END
        </button>
        <button type="button" id="attach-BS" value="bottom start" onClick={handleTargetAttachment}>
          BOTTOM START
        </button>
        <button type="button" id="attach-BC" value="bottom center" onClick={handleTargetAttachment}>
          BOTTOM CENTER
        </button>
        <button type="button" id="attach-BE" value="bottom end" onClick={handleTargetAttachment}>
          BOTTOM END
        </button>
      </div>
    );
  }
}
export default HookshotAttachmentMargin;
