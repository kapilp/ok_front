import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Hookshot from '../../../Hookshot';
import styles from '../common/HookshotTestDocCommon.module.scss';
const cx = classNames.bind(styles);
type HookshotStandardState = {
  open?: boolean;
};
class HookshotStandard extends React.Component<{}, HookshotStandardState> {
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    state = { open: false };
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    const hookshotContent = (
      <Hookshot.Content onEsc={handleRequestClose} onOutsideClick={handleRequestClose} onResize={handleRequestClose} id="testDefaultContent">
        <div className={cx('default-hookshot-wrapper')}>Hookshot</div>
      </Hookshot.Content>
    );
    return (
      <div id="default-bounds" className={cx('content-wrapper')}>
        <Hookshot
          contentAttachment={{ vertical: 'bottom', horizontal: 'center' }}
          isEnabled
          isOpen={state.open}
          targetRef={() => document.getElementById('hookshot-standard-button')}
        >
          {hookshotContent}
        </Hookshot>
        <button type="button" id="hookshot-standard-button" onClick={handleButtonClick}>
          Default Hookshot
        </button>
      </div>
    );
  }
}
export default HookshotStandard;
