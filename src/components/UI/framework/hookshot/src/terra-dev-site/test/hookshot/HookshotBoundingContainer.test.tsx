import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import HookshotTemplate from '../common/HookshotTestTemplate';
type HookshotBoundingContainerState = {
  position?: string;
};
class HookshotBoundingContainer extends React.Component<{}, HookshotBoundingContainerState> {
  constructor(props) {
    super(props);
    handlePositionClick = handlePositionClick.bind(this);
    state = { position: 'up' };
  }
  handlePositionClick(event) {
    setState({ position: event.target.value });
  }
  render() {
    let type = 'up';
    let targetAttachment;
    let contentAttachment;
    if (state.position === 'up') {
      type = 'container-up';
    } else if (state.position === 'down') {
      type = 'container-down';
    } else if (state.position === 'left') {
      type = 'container-left';
      targetAttachment = 'middle end';
      contentAttachment = 'middle start';
    } else if (state.position === 'right') {
      type = 'container-right';
    }
    return (
      <div>
        <HookshotTemplate isOpen={false} type={type} targetAttachment={targetAttachment} contentAttachment={contentAttachment} id="bounding-container" />
        <p> Position should be pushed by bounding container </p>
        <p> Choose position behavior </p>
        <button type="button" id="push-up" value="up" onClick={handlePositionClick}>
          Pushed Up
        </button>
        <button type="button" id="push-down" value="down" onClick={handlePositionClick}>
          Pushed Down
        </button>
        <button type="button" id="push-left" value="left" onClick={handlePositionClick}>
          Pushed Left
        </button>
        <button type="button" id="push-right" value="right" onClick={handlePositionClick}>
          Pushed Right
        </button>
      </div>
    );
  }
}
export default HookshotBoundingContainer;
