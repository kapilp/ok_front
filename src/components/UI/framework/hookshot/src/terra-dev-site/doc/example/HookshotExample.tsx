import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ATTACHMENT_BEHAVIORS, ATTACHMENT_BEHAVIORS_, Hookshot } from '../../../Hookshot';
import { Button } from '../../../../../../core/button/src/Button';
import { InputField } from '../../../../../../core/form-input/src/InputField';
import styles from './HookshotDocCommon.module.scss';
import { HookshotContent } from '../../../HookshotContent';
const cx = classNames.bind(styles);
const ATTACHMENT_POSITIONS = ['top start', 'top center', 'top end', 'middle start', 'middle center', 'middle end', 'bottom start', 'bottom center', 'bottom end'];
const generateOptions = values =>
  values.map((currentValue, index) => {
    const keyValue = index;
    return (
      <option key={keyValue} value={currentValue}>
        {currentValue}
      </option>
    );
  });
const attachmentValues = (attachment: string) => {
  if (attachment === 'middle start') {
    return { vertical: 'middle', horizontal: 'start' };
  }
  if (attachment === 'middle end') {
    return { vertical: 'middle', horizontal: 'end' };
  }
  if (attachment === 'middle center') {
    return { vertical: 'middle', horizontal: 'center' };
  }
  if (attachment === 'top start') {
    return { vertical: 'top', horizontal: 'start' };
  }
  if (attachment === 'top end') {
    return { vertical: 'top', horizontal: 'end' };
  }
  if (attachment === 'top center') {
    return { vertical: 'top', horizontal: 'center' };
  }
  if (attachment === 'bottom start') {
    return { vertical: 'bottom', horizontal: 'start' };
  }
  if (attachment === 'bottom end') {
    return { vertical: 'bottom', horizontal: 'end' };
  }
  return { vertical: 'bottom', horizontal: 'center' };
};
const getId = (name: string) => `${name}Example`;

export const HookshotExample = () => {
  const [state, setState] = createStore({
    isOpen: false,
    hookshotContentAttachment: ATTACHMENT_POSITIONS[1],
    hookshotTargetAttachment: ATTACHMENT_POSITIONS[7],
    hookshotAttachmentBehavior: 'auto' as ATTACHMENT_BEHAVIORS_,
    hookshotAttachmentMargin: 0,
  });

  const handleButtonClick = () => {
    setState({ isOpen: !state.isOpen });
  };
  const handleRequestClose = () => {
    setState({ isOpen: false });
  };
  const handleAttachementBehaviorChange = event => {
    setState({ hookshotAttachmentBehavior: event.target.value });
  };
  const handleContentAttachmentChange = event => {
    setState({ hookshotContentAttachment: event.target.value });
  };
  const handleTargetAttachmentChange = event => {
    setState({ hookshotTargetAttachment: event.target.value });
  };
  const handleInputChange = event => {
    const value = Number.parseFloat(event.target.value);
    setState({ [event.target.name]: value });
  };

  const hookshotContent = (p: { ref: (el: HTMLDivElement) => void }): JSX.Element => (
    <HookshotContent onEsc={handleRequestClose} onOutsideClick={handleRequestClose} onResize={handleRequestClose} ref={p.ref}>
      <div className={cx('content-wrapper')}>Hookshot</div>
    </HookshotContent>
  );
  return (
    <div>
      <div className={cx('heading-wrapper')}>
        <label htmlFor={getId('hookshotAttachmentBehavior')} className={cx('label')}>
          Attachment Behavior
        </label>
      </div>
      <select
        id={getId('hookshotAttachmentBehavior')}
        name="hookshotAttachmentBehavior"
        value={state.hookshotAttachmentBehavior}
        onChange={handleAttachementBehaviorChange}
        className={cx('select-wrapper')}
      >
        {generateOptions(ATTACHMENT_BEHAVIORS)}
      </select>
      <br />
      <br />
      <InputField
        label="Attachment Margin in Px"
        inputId={getId('hookshotAttachmentMargin')}
        inputAttrs={{ name: 'hookshotAttachmentMargin', type: 'number' }}
        defaultValue={state.hookshotAttachmentMargin}
        className={cx('input-wrapper')}
        onChange={handleInputChange}
      />
      <div className={cx('heading-wrapper')}>
        <label htmlFor={getId('hookshotContentAttachment')} className={cx('label')}>
          Content Attachment
        </label>
      </div>
      <select
        id={getId('hookshotContentAttachment')}
        name="hookshotContentAttachment"
        value={state.hookshotContentAttachment}
        onChange={handleContentAttachmentChange}
        className={cx('select-wrapper')}
      >
        {generateOptions(ATTACHMENT_POSITIONS)}
      </select>
      <br />
      <br />
      <div className={cx('heading-wrapper')}>
        <label htmlFor={getId('hookshotTargetAttachment')} className={cx('label')}>
          Target Attachment
        </label>
      </div>
      <select
        id={getId('hookshotTargetAttachment')}
        name="hookshotTargetAttachment"
        value={state.hookshotTargetAttachment}
        onChange={handleTargetAttachmentChange}
        className={cx('select-wrapper')}
      >
        {generateOptions(ATTACHMENT_POSITIONS)}
      </select>
      <br />
      <br />
      <Hookshot
        attachmentBehavior={state.hookshotAttachmentBehavior}
        attachmentMargin={state.hookshotAttachmentMargin}
        contentAttachment={attachmentValues(state.hookshotContentAttachment)}
        isEnabled
        isOpen={state.isOpen}
        targetAttachment={attachmentValues(state.hookshotTargetAttachment)}
        targetRef={() => document.getElementById('hookshot-standard-button')}
      >
        {hookshotContent}
      </Hookshot>
      <Button id="hookshot-standard-button" text="Hookshot Example" onClick={handleButtonClick} />
    </div>
  );
};
