import {  JSX, Component, createEffect, createSignal, onCleanup, untrack, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Portal } from 'solid-js/web';
import * as HookshotUtils from './_HookshotUtils';
import { ResizeObserver } from '@juggle/resize-observer';
export const VERTICAL_ATTACHMENTS = ['top', 'middle', 'bottom'];
export const HORIZONTAL_ATTACHMENTS = ['start', 'center', 'end'];
export const ATTACHMENT_BEHAVIORS = ['auto', 'flip', 'push', 'none'];

export type VERTICAL_ATTACHMENTS_ = 'top' | 'middle' | 'bottom';
export type HORIZONTAL_ATTACHMENTS_ = 'start' | 'center' | 'end';
export type ATTACHMENT_BEHAVIORS_ = 'auto' | 'flip' | 'push' | 'none';
type Position = { rect: {}; attachment?: { vertical: string; horizontal: string }; x: number; y: number; offset?: { horizontal: number; vertical: number } };

interface IHookshotProps extends JSX.HTMLAttributes<Element> {
  /**
   * How the content should be positioned when the primary attachment is not available.
   * Valid values:
   *  'auto': returns 180 degrees, returns rotate 90 degree, returns rotate -90 degrees, returns primary attachment
   *  'flip': returns 180 degrees, returns primary attachment
   *  'push': pushes content to remain within the bounding rect, returns primary attachment
   *  'none': returns primary attachment
   */
  attachmentBehavior?: ATTACHMENT_BEHAVIORS_;
  /**
   * Value in px of the margin to place between the target and the content.
   */
  attachmentMargin?: number;
  /**
   * Reference to the bounding container. Defaults to window unless attachment behavior is set to none.
   */
  boundingRef?: () => HTMLElement;
  /**
   * The HookshotContent to be attached.
   */
  children: (props: { ref: (el: HTMLDivElement) => void }) => JSX.Element;
  /**
   * Object containing the vertical and horizontal attachment values for the content.
   * Valid values?: { horizontal?: ['start', 'center', 'end'], vertical?: ['top', 'middle', 'bottom'] }.
   */
  contentAttachment: {
    horizontal: HORIZONTAL_ATTACHMENTS_;
    vertical: VERTICAL_ATTACHMENTS_;
  };
  /**
   * Object containing the vertical and horizontal offset values in px for the content.
   */
  contentOffset?: {
    horizontal: number;
    vertical: number;
  };
  /**
   * Determines whether the content should be actively positioned via hookshot.
   */
  isEnabled?: boolean;
  /**
   * Should the content be presented.
   */
  isOpen?: boolean;
  /**
   * Callback function when the content has been positioned.
   */
  // see getRelativePositions  on HookshotUtils.ts return type for positions type
  onPosition?: (
    event: Event,
    positions: {
      content: Position;
      target: Position;
    },
  ) => void;
  /**
   * Client coordinates to serve as the anchor point for the hookshot'd content.
   */
  targetCoordinates?: {
    x: number; //required
    y: number; // required
  };
  /**
   * Element to serve as the anchor point for the hookshot'd content. (If targetCoordinates are provided, this is ignored.)
   */
  targetRef?: () => HTMLElement;
  /**
   * Object containing the vertical and horizontal attachment values for the target.
   * Valid values?: { horizontal?: ['start', 'center', 'end'], vertical?: ['top', 'middle', 'bottom'] }.
   * If targetCoordinates are provided { horizontal?: 'center', vertical?: 'middle' } will be applied.
   */
  targetAttachment?: {
    horizontal?: HORIZONTAL_ATTACHMENTS_;
    vertical?: VERTICAL_ATTACHMENTS_;
  };
  /**
   * Object containing the vertical and horizontal offset values in px for the target.
   */
  targetOffset?: {
    horizontal?: number;
    vertical?: number;
  };
}
const defaultProps = {
  attachmentMargin: 0,
  attachmentBehavior: 'auto',
  contentOffset: { horizontal: 0, vertical: 0 },
  isEnabled: false,
  isOpen: false,
  targetOffset: { horizontal: 0, vertical: 0 },
};

export const Hookshot = (props: IHookshotProps) => {
  props = mergeProps({}, defaultProps, props);
  let cachedRects: any;
  let content = { offset: { horizontal: 0, vertical: 0 }, attachment: {} };
  const [contentNode, setContentNodeState] = createSignal<HTMLDivElement>();
  let listenersAdded: boolean = false;
  let lastCall: null | number;
  let lastDuration: number | null = null;
  let pendingTimeout: number | null = null;
  let parentListeners: any;

  let target: { offset: {} };

  // taken from : https://stackoverflow.com/a/54427131/4372670
  // also can use native: https://drafts.csswg.org/resize-observer-1/
  const ro = new ResizeObserver(entries => {
    update();
  });
  const onUpdate = () => {
    if (props.isEnabled && props.isOpen) {
      if (!listenersAdded) {
        enableListeners();
        ro.observe(contentNode());
      }
      update();
    } else {
      disableListeners();
    }
  };
  const setUpdateOnResize = () => {};
  createEffect(() => {
    if (contentNode()) {
      setTimeout(onUpdate);
    }
  });
  onCleanup(() => {
    disableListeners();
  });

  const getBoundingRef = () => {
    return props.boundingRef ? props.boundingRef() : undefined;
  };
  const getTargetRef = () => {
    return props.targetRef ? props.targetRef() : undefined;
  };
  const getValidBoundingRect = () => {
    if (props.attachmentBehavior === 'none') {
      return undefined;
    }
    return HookshotUtils.getBoundingRect(getBoundingRef() || 'window');
  };
  const getValidTargetRect = () => {
    if (props.targetCoordinates) {
      return HookshotUtils.getRectFromCoords(props.targetCoordinates);
    }
    return HookshotUtils.getBounds(getTargetRef());
  };
  const getNodeRects = (resetContentCache: boolean) => {
    return {
      contentRect: resetContentCache ? HookshotUtils.getBounds(contentNode()) : cachedRects.contentRect,
      targetRect: getValidTargetRect(),
      boundingRect: getValidBoundingRect(),
    };
  };
  const tick = (event: Event) => {
    console.log('ticking');
    if (lastDuration && lastDuration > 16) {
      // Throttle to 60fps, in order to handle safari and mobile performance
      lastDuration = Math.min(lastDuration - 16, 100);
      // Just in case this is the last event, remember to position just once more
      pendingTimeout = setTimeout(tick, 100);
      return;
    }
    if (lastCall && performance.now() - lastCall < 10) {
      // Some browsers call events a little too frequently, refuse to run more than is reasonable
      return;
    }
    if (pendingTimeout != null) {
      clearTimeout(pendingTimeout);
      pendingTimeout = null;
    }
    lastCall = performance.now();
    update(event);
    lastDuration = performance.now() - lastCall;
  };
  const enableListeners = () => {
    const childElement = getTargetRef() || getBoundingRef();
    if (!childElement) {
      return;
    }
    ['resize', 'scroll', 'touchmove'].forEach(event => window.addEventListener(event, tick));
    parentListeners = [];
    const scrollParents = HookshotUtils.getScrollParents(childElement);
    scrollParents.forEach(parent => {
      if (parent !== childElement.ownerDocument) {
        parent.addEventListener('scroll', tick);
        parentListeners.push(parent);
      }
    });
    listenersAdded = true;
  };
  function disableListeners() {
    ['resize', 'scroll', 'touchmove'].forEach(event => window.removeEventListener(event, tick));
    if (parentListeners) {
      parentListeners.forEach((parent: HTMLElement) => {
        parent.removeEventListener('scroll', tick);
      });
      parentListeners = null;
    }
    listenersAdded = false;
  }
  const position = (event: Event, resetContentCache: boolean) => {
    cachedRects = getNodeRects(resetContentCache);
    content.rect = cachedRects.contentRect; // Todo fix ts error
    target.rect = cachedRects.targetRect;
    const result = HookshotUtils.positionStyleFromBounds(cachedRects.boundingRect, content, target, props.attachmentMargin, props.attachmentBehavior);
    let styleUpdated = false;
    const newTransform = `translate3d(${result.style.left}, ${result.style.top}, 0px)`;
    if (contentNode().style.transform !== newTransform) {
      contentNode().style.transform = newTransform;
      styleUpdated = true;
    }
    if (contentNode().style.opacity !== '1') {
      contentNode().style.opacity = '1';
      styleUpdated = true;
    }
    if (styleUpdated) {
      cachedRects.contentRect = HookshotUtils.getBounds(contentNode());
      result.positions.content.rect = cachedRects.contentRect;
    }
    if (props.onPosition) {
      props.onPosition(event, result.positions);
    }
  };
  const update = (event?: Event) => {
    if ((!getTargetRef() && !props.targetCoordinates) || !contentNode()) {
      return;
    }
    updateHookshot(event);
  };
  const updateHookshot = (event?: Event) => {
    const resetCache = !event || (event.type !== 'scroll' && event.type !== 'touchmove');
    position(event, resetCache);
  };
  // Todo fix this: // disableOnClickOutside: !content.props.onOutsideClick
  /*const cloneContent = (content) => {
    return React.cloneElement(content, {
      ref: wrappedRefCallback(content),
      disableOnClickOutside: !content.props.onOutsideClick
    });
  }*/
  /*/const wrappedRefCallback = (content) => {
    return node => {
      setContentNode(node);
      if (content.props.ref) {
        content.props.ref(node);
      }
    };
  }*/
  const [p, customProps] = splitProps(props, [
    'attachmentBehavior',
    'attachmentMargin',
    'boundingRef',
    'children',
    'contentAttachment',
    'contentOffset',
    'isEnabled',
    'isOpen',
    'targetCoordinates',
    'targetRef',
    'targetAttachment',
    'targetOffset',
    'onPosition',
  ]);

  const isRTL = document.getElementsByTagName('html')[0].getAttribute('dir') === 'rtl';
  createEffect(() => {
    content = {
      offset: HookshotUtils.getDirectionalOffset(p.contentOffset ?? { vertical: 0, horizontal: 0 }, isRTL),
      attachment: HookshotUtils.getDirectionalAttachment(p.contentAttachment, isRTL),
    };
    target = {
      offset: HookshotUtils.getDirectionalOffset(p.targetOffset ?? { vertical: 0, horizontal: 0 }, isRTL),
    };
    if (p.targetCoordinates) {
      target.attachment = HookshotUtils.coordinateAttachment;
    } else if (p.targetAttachment) {
      target.attachment = HookshotUtils.getDirectionalAttachment(p.targetAttachment, isRTL);
    } else {
      target.attachment = HookshotUtils.mirrorAttachment(content.attachment);
    }
  });

  // return <Portal>{cloneContent(p.children)}</Portal>;
  return <>{p.isOpen && <Portal>{p.children({ ref: setContentNodeState })}</Portal>}</>;
};
// isOpened={p.isOpen}
// Hookshot.horizontalAttachments = HORIZONTAL_ATTACHMENTS;
// Hookshot.verticalAttachments = VERTICAL_ATTACHMENTS;
// Hookshot.attachmentBehaviors = ATTACHMENT_POSITIONS;
Hookshot.Utils = HookshotUtils;
// Hookshot.Content = HookshotContent;
// export default Hookshot;
