import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Hookshot from '../../src/Hookshot';
import styles from './Hookshot.test.module.scss';
type HookshotDefaultExampleState = {
  isOpen?: boolean;
};
class HookshotDefaultExample extends React.Component<{}, HookshotDefaultExampleState> {
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    state = { isOpen: false };
  }
  handleButtonClick() {
    setState(prevState => ({ isOpen: !prevState.isOpen }));
  }
  handleRequestClose() {
    setState({ isOpen: false });
  }
  render() {
    const hookshotContent = (
      <Hookshot.Content onRequestClose={handleRequestClose}>
        <div className={styles.content}>Hookshot</div>
      </Hookshot.Content>
    );
    return (
      <div>
        <Hookshot
          contentAttachment={{ vertical: 'top', horizontal: 'center' }}
          isEnabled
          isOpen={state.isOpen}
          targetRef={() => document.getElementById('hookshot-standard-button')}
        >
          {hookshotContent}
        </Hookshot>
        <button type="button" id="hookshot-standard-button" onClick={handleButtonClick} aria-label="hookshot-standard-button" />
      </div>
    );
  }
}
export default HookshotDefaultExample;
