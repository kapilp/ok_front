import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Menu from 'terra-menu';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './CollapsibleMenuView.module.scss';
const cx = classNames.bind(styles);
const contextTypes = {
  isCollapsibleMenuItem: boolean,
};
const CollapsibleMenuViewDivider = (props, { isCollapsibleMenuItem }) => {
  const theme = React.useContext(ThemeContext);
  if (isCollapsibleMenuItem) {
    return <Menu.Divider />;
  }
  return <div className={cx(['divider', 'face-up-item', theme.className])} />;
};
CollapsibleMenuViewDivider.contextTypes = contextTypes;
export default CollapsibleMenuViewDivider;
