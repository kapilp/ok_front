import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import CollapsibleMenuView from '../../../CollapsibleMenuView';
const CollapsibleMenuViewSingleItem = () => (
  <CollapsibleMenuView>
    <CollapsibleMenuView.Item text="-----------------------------------------------------------------------------------------------------------------" key="button" />
  </CollapsibleMenuView>
);
export default CollapsibleMenuViewSingleItem;
