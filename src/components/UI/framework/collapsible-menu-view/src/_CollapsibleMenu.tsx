import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Menu from "terra-menu";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import styles from "./CollapsibleMenuView.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * List of CollapsibleMenuView.Item(s)/CollapsibleMenuView.ItemGroup(s)/CollapsibleMenuView.Divider(s) to be displayed as content within the Menu.
   */
  children: JSX.Element.isRequired,
  /**
   * Bounding container for the menu, will use window if no value provided.
   */
  boundingRef: PropTypes.func,
  /**
   * A string representation of the width in px, limited to:
   * 160, 240, 320, 640, 960, 1280, 1760, or auto
   */
  contentWidth: PropTypes.oneOf([
    "160",
    "240",
    "320",
    "640",
    "960",
    "1280",
    "1760",
    "auto"
  ]),
  /**
   * Button to display the menu from
   */
  button: JSX.Element.isRequired
};
const childContextTypes = {
  isCollapsibleMenuItem: boolean
};
interface ICollapsibleMenuProps extends JSX.HTMLAttributes<Element> {
  button?: any;
  contentWidth?: any;
  customProps?: any;
}
type CollapsibleMenuState = {
  isOpen?: boolean
};
class CollapsibleMenu extends React.Component<
  ICollapsibleMenuProps,
  CollapsibleMenuState
> {
  const theme = useContext(ThemeContext);
  buttonNode: any;
  context: any;
  constructor(props) {
    super(props);
    handleRequestClose = handleRequestClose.bind(this);
    wrappedOnClick = wrappedOnClick.bind(this);
    wrapButtonClick = wrapButtonClick.bind(this);
    wrapChildrenOnClick = wrapChildrenOnClick.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { isOpen: false };
  }
  getChildContext() {
    return { isCollapsibleMenuItem: true };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleRequestClose() {
    setState({ isOpen: false });
  }
  wrappedOnClick(item) {
    return event => {
      handleRequestClose();
      if (item.props.onClick) {
        item.props.onClick(event);
      }
    };
  }
  wrapButtonClick(button) {
    return event => {
      setState({ isOpen: true });
      if (button.props.onClick) {
        button.props.onClick(event);
      }
    };
  }
  wrapChildrenOnClick(children) {
    const cloneChildren = children ? [] : undefined;
    React.Children.forEach(children, item => {
      let clonedElement = item;
      if (item.props.shouldCloseOnClick) {
        clonedElement = React.cloneElement(item, {
          onClick: wrappedOnClick(item)
        });
      } else if (item.props.children) {
        const clonedChildren = wrapChildrenOnClick(item.props.children);
        clonedElement = React.cloneElement(item, { children: clonedChildren });
      } else if (item.props.subMenuItems) {
        const subMenuItems = wrapChildrenOnClick(item.props.subMenuItems);
        clonedElement = React.cloneElement(item, { subMenuItems });
      }
      cloneChildren.push(clonedElement);
    });
    return cloneChildren;
  }
  render() {
    const { children, button, contentWidth, ...customProps } = props;
    const clonedButton = React.cloneElement(button, {
      onClick: wrapButtonClick(button)
    });
    const theme = context;
    return (
      <div
        className={cx("face-up-item", theme.className)}
        ref={setButtonNode}
      >
        <Menu
          {...customProps}
          onRequestClose={handleRequestClose}
          isArrowDisplayed
          isOpen={state.isOpen}
          targetRef={getButtonNode}
          contentWidth={contentWidth}
        >
          {wrapChildrenOnClick(children)}
        </Menu>
        {clonedButton}
      </div>
    );
  }
}
CollapsibleMenu.childContextTypes = childContextTypes;
CollapsibleMenu.Item = Menu.Item;
CollapsibleMenu.ItemGroup = Menu.ItemGroup;
CollapsibleMenu.Opts = {
  widths: Menu.Opts.widths
};

export default CollapsibleMenu;
