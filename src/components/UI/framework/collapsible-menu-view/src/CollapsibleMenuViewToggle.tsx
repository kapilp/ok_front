import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Checkbox from 'terra-form-checkbox';
import Menu from './_CollapsibleMenu';
import styles from './CollapsibleMenuView.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Sets the item's text
   */
  text: string.isRequired;
  /**
   * Indicates if the item is selected.
   */
  isSelected: boolean;
  /**
   * Callback function for when selection state changes. Is called with event and isSelected parameters
   */
  onChange: PropTypes.func;
  /**
   *  Indicates that the toggle should be selectable
   */
  isSelectable: boolean;
  /**
   * Indicates if the toggle should be disabled.
   */
  isDisabled: boolean;
  /**
   * Indicates that clicking on this item while displayed inside a menu should close the menu
   */
  shouldCloseOnClick: boolean;
}
const contextTypes = {
  isCollapsibleMenuItem: boolean,
};
const defaultProps = {
  isSelected: false,
  isSelectable: true,
  shouldCloseOnClick: true,
};
interface ICollapsibleMenuViewToggleProps extends JSX.HTMLAttributes<Element> {
  text?: any;
  isSelected?: any;
  isSelectable?: any;
  isDisabled?: any;
  onChange?: any;
  shouldCloseOnClick?: any;
  customProps?: any;
}
class CollapsibleMenuViewToggle extends React.Component<ICollapsibleMenuViewToggleProps, {}> {
  context: any;
  constructor(props) {
    super(props);
    wrappedOnChange = wrappedOnChange.bind(this);
  }
  // Wrapping onChange event so that the same parameters will be given when the display is a checkbox and a menu.item
  wrappedOnChange(event) {
    if (props.onChange) {
      props.onChange(event, event.target.checked);
    }
  }
  render() {
    const { text, isSelected, isSelectable, isDisabled, onChange, shouldCloseOnClick, ...customProps } = props;
    const { isCollapsibleMenuItem } = context;
    const controllClassName = cx(['control', { 'is-disabled': isDisabled || !isSelectable }, customProps.className]);
    if (isCollapsibleMenuItem) {
      return <Menu.Item {...customProps} text={text} isSelected={isSelected} isSelectable={isSelectable} isDisabled={isDisabled} onChange={onChange} />;
    }
    return (
      <div className={cx(['face-up-item'])}>
        <Checkbox {...customProps} className={controllClassName} labelText={text} defaultChecked={isSelected} onChange={wrappedOnChange} disabled={isDisabled || !isSelectable} />
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
CollapsibleMenuViewToggle.contextTypes = contextTypes;
export default CollapsibleMenuViewToggle;
