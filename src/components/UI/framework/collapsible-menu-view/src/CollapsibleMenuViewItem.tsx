import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Button from "terra-button";
import ButtonGroup from "terra-button-group";
import Menu from "./_CollapsibleMenu";
import styles from "./CollapsibleMenuView.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Sets the item's text
   */
  text: string.isRequired,
  /**
   * Bounding container for the menu, will use window if no value provided.
   */
  boundingRef: PropTypes.func,
  /**
   * An optional icon. Nested inline with the text when provided
   */
  icon: JSX.Element,
  /**
   * Indicates if only the icon should display when item is face-up. (Text should still be given to be displayed when item is in the menu)
   */
  isIconOnly: boolean,
  /**
   * Reverses the position of the icon and text
   */
  isReversed: boolean,
  /**
   * Indicates if the item start as selected when placed in a button group
   */
  isSelected: boolean,
  /**
   * Indicates if the item should be disabled.
   */
  isDisabled: boolean,
  /**
   * Indicates that clicking on this item while displayed inside a menu should close the menu.
   */
  shouldCloseOnClick: boolean,
  /**
   * List of Menu.Items to display in a submenu when this item is selected.
   */
  subMenuItems: PropTypes.arrayOf(JSX.Element),
  /**
   * Callback function for when the item is clicked
   */
  onClick: PropTypes.func,
  /**
   * A string representation of the width in px, limited to:
   * 160, 240, 320, 640, 960, 1280, 1760, or auto
   */
  menuWidth: PropTypes.oneOf([
    "160",
    "240",
    "320",
    "640",
    "960",
    "1280",
    "1760",
    "auto"
  ])
};
const contextTypes = {
  isCollapsibleGroupItem: boolean,
  isCollapsibleMenuItem: boolean
};
const defaultProps = {
  isSelected: false,
  isReversed: false,
  shouldCloseOnClick: true,
  isIconOnly: false
};
interface ICollapsibleMenuViewItemProps extends JSX.HTMLAttributes<Element> {
  icon?: any;
  isIconOnly?: any;
  isReversed?: any;
  text?: any;
  isSelected?: any;
  isDisabled?: any;
  subMenuItems?: any;
  shouldCloseOnClick?: any;
  boundingRef?: any;
  menuWidth?: any;
  customProps?: any;
}
class CollapsibleMenuViewItem extends React.Component<
  ICollapsibleMenuViewItemProps,
  {}
> {
  buttonNode: any;
  context: any;
  handleButtonClick: any;
  constructor(props) {
    super(props);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  render() {
    const {
      icon,
      isIconOnly,
      isReversed,
      text,
      isSelected,
      isDisabled,
      subMenuItems,
      shouldCloseOnClick,
      boundingRef,
      menuWidth,
      ...customProps
    } = props;
    const { isCollapsibleGroupItem, isCollapsibleMenuItem } = context;
    const attributes = { ...customProps };
    let item;
    if (isCollapsibleMenuItem) {
      item = (
        <Menu.Item
          {...attributes}
          text={text}
          isSelected={isSelected && isCollapsibleGroupItem}
          isDisabled={isDisabled}
          subMenuItems={subMenuItems}
        />
      );
    } else if (isCollapsibleGroupItem) {
      item = (
        <ButtonGroup.Button
          {...attributes}
          icon={icon}
          text={text}
          isDisabled={isDisabled}
        />
      );
    } else if (subMenuItems && subMenuItems.length > 0) {
      item = (
        <Menu
          contentWidth={menuWidth}
          boundingRef={boundingRef}
          button={
            <Button
              {...attributes}
              icon={icon}
              text={text}
              isReversed={isReversed}
              isDisabled={isDisabled}
              onClick={handleButtonClick}
              isIconOnly={isIconOnly}
            />
          }
        >
          {subMenuItems}
        </Menu>
      );
    } else {
      item = (
        <div className={cx("face-up-item")}>
          <Button
            {...attributes}
            icon={icon}
            text={text}
            isReversed={isReversed}
            isDisabled={isDisabled}
            isIconOnly={isIconOnly}
          />
        </div>
      );
    }
    return item;
  }
}
props = mergeProps({}, defaultProps, props)
CollapsibleMenuViewItem.contextTypes = contextTypes;
CollapsibleMenuViewItem.Opts = {
  widths: Menu.Opts.widths
};
export default CollapsibleMenuViewItem;
