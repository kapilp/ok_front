import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import ButtonGroup from "terra-button-group";
import Menu from "./_CollapsibleMenu";
import styles from "./CollapsibleMenuView.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Callback function that is called when the group's selection state changes
   */
  onChange: PropTypes.func,
  /**
   * CollapsibleMenuView.Items to be grouped together
   */
  children: JSX.Element.isRequired,
  /**
   * A list of keys of the CollapsibleMenuView.Items that should be selected.
   */
  selectedKeys: PropTypes.arrayOf(string)
};
const defaultProps = {
  selectedKeys: []
};
const childContextTypes = {
  isCollapsibleGroupItem: boolean
};
const contextTypes = {
  isCollapsibleMenuItem: boolean
};
interface ICollapsibleMenuViewItemGroupProps
  extends JSX.HTMLAttributes<Element> {
  onChange?: any;
  selectedKeys?: any;
  customProps?: any;
}
class CollapsibleMenuViewItemGroup extends React.Component<
  ICollapsibleMenuViewItemGroupProps,
  {}
> {
  context: any;
  constructor(props) {
    super(props);
    handleOnChange = handleOnChange.bind(this);
  }
  getChildContext() {
    return { isCollapsibleGroupItem: true };
  }
  handleOnChange(event, selectedIndex) {
    if (props.onChange) {
      let selectedKey = selectedIndex;
      React.Children.forEach(props.children, (child, index) => {
        if (selectedIndex === index) {
          selectedKey = child.key;
        }
      });
      props.onChange(event, selectedKey);
    }
  }
  render() {
    const { children, onChange, selectedKeys, ...customProps } = props;
    const { isCollapsibleMenuItem } = context;
    if (isCollapsibleMenuItem && selectedKeys.length) {
      return (
        <li role="none">
          <Menu.ItemGroup {...customProps} onChange={handleOnChange}>
            {children}
          </Menu.ItemGroup>
        </li>
      );
    }
    if (isCollapsibleMenuItem) {
      return <div {...customProps}>{children}</div>;
    }
    const buttonGroupClassNames = cx(["face-up-item", customProps.className]);
    return (
      <ButtonGroup
        {...customProps}
        onChange={onChange}
        className={buttonGroupClassNames}
        selectedKeys={selectedKeys}
      >
        {children}
      </ButtonGroup>
    );
  }
}
props = mergeProps({}, defaultProps, props)
CollapsibleMenuViewItemGroup.childContextTypes = childContextTypes;
CollapsibleMenuViewItemGroup.contextTypes = contextTypes;
export default CollapsibleMenuViewItemGroup;
