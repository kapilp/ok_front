import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import ResizeObserver from "resize-observer-polyfill";
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import { FormattedMessage } from "react-intl";
import CollapsibleMenuViewItem from "./CollapsibleMenuViewItem";
import CollapsibleMenuViewItemGroup from "./CollapsibleMenuViewItemGroup";
import CollapsibleMenuViewToggle from "./CollapsibleMenuViewToggle";
import CollapsibleMenuViewDivider from "./CollapsibleMenuViewDivider";
import styles from "./CollapsibleMenuView.module.scss";
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Item/ItemGroup/Divider elements to display in the view. Items that will fit in the space available will
   * display face-up the remainder will be rolled into a menu
   */
  children: JSX.Element.isRequired,
  /**
   * A string representation of the width in px, limited to:
   * 160, 240, 320, 640, 960, 1280, 1760, or auto
   */
  menuWidth: PropTypes.oneOf([
    "160",
    "240",
    "320",
    "640",
    "960",
    "1280",
    "1760",
    "auto"
  ]),
  /**
   * Bounding container for the menu, will use window if no value provided.
   */
  boundingRef: PropTypes.func
};
interface ICollapsibleMenuViewProps extends JSX.HTMLAttributes<Element> {
  boundingRef?: any;
  menuWidth?: any;
  customProps?: any;
}
class CollapsibleMenuView extends React.Component<
  ICollapsibleMenuViewProps,
  {}
> {
  const theme = useContext(ThemeContext);
  animationFrameID: null;
  container: any;
  contentWidth: any;
  context: any;
  forceUpdate: any;
  hiddenStartIndex: number;
  isCalculating: boolean;
  menuButton: any;
  menuHidden: boolean;
  resizeObserver: any;
  constructor(props) {
    super(props);
    setContainer = setContainer.bind(this);
    setMenuButton = setMenuButton.bind(this);
    resetCache = resetCache.bind(this);
    handleResize = handleResize.bind(this);
    resetCache();
  }
  componentDidMount() {
    resizeObserver = new ResizeObserver(entries => {
      contentWidth = entries[0].contentRect.width;
      if (!isCalculating) {
        animationFrameID = window.requestAnimationFrame(() => {
          // Resetting the cache so that all elements will be rendered face-up for width calculations
          resetCache();
          forceUpdate();
        });
      }
    });
    handleResize(contentWidth);
    resizeObserver.observe(container);
  }
  componentDidUpdate() {
    if (isCalculating) {
      isCalculating = false;
      handleResize(contentWidth);
    }
  }
  componentWillUnmount() {
    window.cancelAnimationFrame(animationFrameID);
    resizeObserver.disconnect(container);
    container = null;
  }
  setContainer(node) {
    if (node === null) {
      return;
    } // Ref callbacks happen on mount and unmount, element will be null on unmount
    container = node;
  }
  setMenuButton(node) {
    if (node === null) {
      return;
    }
    menuButton = node;
  }
  resetCache() {
    animationFrameID = null;
    hiddenStartIndex = -1;
    isCalculating = true;
    menuHidden = false;
  }
  handleResize(width) {
    const menuButtonWidth = menuButton.getBoundingClientRect().width;
    const availableWidth = width - menuButtonWidth;
    let hiddenStartIndex = -1;
    let calcWidth = 0;
    let menuHidden = true;
    for (let i = 0; i < React.Children.count(props.children); i += 1) {
      const child = container.children[i];
      const childWidth = child.getBoundingClientRect().width;
      calcWidth += childWidth;
      if (calcWidth > availableWidth) {
        // If last child fits in the available space, leave it face up
        if (i === props.children.length - 1 && calcWidth <= width) {
          break;
        }
        // If divider is the last element to be hidden on collapse menu, leave it face up
        if (
          React.Children.count(props.children) > 1 &&
          props.children[i].type === CollapsibleMenuViewDivider
        ) {
          hiddenStartIndex = i - 1;
        } else {
          hiddenStartIndex = i;
        }
        menuHidden = false;
        break;
      }
    }
    if (
      menuHidden !== menuHidden ||
      hiddenStartIndex !== hiddenStartIndex
    ) {
      menuHidden = menuHidden;
      hiddenStartIndex = hiddenStartIndex;
      forceUpdate();
    }
  }
  render() {
    const { children, boundingRef, menuWidth, ...customProps } = props;
    const visibleChildren = React.Children.toArray(children);
    let hiddenChildren = null;
    if (hiddenStartIndex >= 0) {
      hiddenChildren = visibleChildren.splice(hiddenStartIndex);
    }
    const theme = context;
    const collapsibleMenuViewClassName = classNames(
      cx(
        "collapsible-menu-view",
        { "is-calculating": isCalculating },
        theme.className
      ),
      customProps.className
    );
    const menuButtonClassName = cx("menu-button", { hidden: menuHidden });
    return (
      <div
        {...customProps}
        className={collapsibleMenuViewClassName}
        ref={setContainer}
      >
        {visibleChildren}
        <div className={menuButtonClassName} ref={setMenuButton}>
          <FormattedMessage id="Terra.collapsibleMenuView.more">
            {ellipsesText => (
              <CollapsibleMenuViewItem
                data-collapsible-menu-toggle
                icon={<span className={cx("menu-button-icon")} />}
                subMenuItems={hiddenChildren}
                boundingRef={boundingRef}
                menuWidth={menuWidth}
                isIconOnly
                text={ellipsesText}
                variant="utility"
              />
            )}
          </FormattedMessage>
        </div>
      </div>
    );
  }
}
CollapsibleMenuView.Item = CollapsibleMenuViewItem;
CollapsibleMenuView.ItemGroup = CollapsibleMenuViewItemGroup;
CollapsibleMenuView.Toggle = CollapsibleMenuViewToggle;
CollapsibleMenuView.Divider = CollapsibleMenuViewDivider;

export default CollapsibleMenuView;
