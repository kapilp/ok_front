import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ItemShape, VARIANTS } from './Utils';
import { UtilityButton } from './utility/_UtilityButton';
interface Properties {
  /**
   * The array containing the menu items to be rendered within the menu.
   */
  menuItems: ItemShape[];
  /**
   * The function to trigger when a menu item is selected.
   * Returns `(event, { key?:String, metaData?:Object})`
   */
  onChange: () => void;
  /**
   * The function that discloses the menu.
   */
  onDisclose: (content: JSX.Element) => void;
  /**
   * The key of the top level menu page.
   */
  initialSelectedKey: string;
  /**
   * The text to be displayed.
   */
  title?: string;
  /**
   * The accessory element to be displayed next to the title.
   */
  accessory?: JSX.Element;
  /**
   * The role attribute to set on the menu.
   */
  menuRole?: string;
}
const defaultProps = {
  menuRole: 'navigation',
};
export const ApplicationHeaderUtility: Component<Properties> = (props: Properties) => {
  props = mergeProps({}, defaultProps, props);
  const [extracted, customProps] = splitProps(props, ['menuItems', 'onChange', 'onDisclose', 'initialSelectedKey', 'title', 'accessory', 'menuRole']);
  return (
    <UtilityButton
      {...customProps}
      menuItems={extracted.menuItems}
      onChange={extracted.onChange}
      onDisclose={extracted.onDisclose}
      initialSelectedKey={extracted.initialSelectedKey}
      title={extracted.title}
      accessory={extracted.accessory}
      menuRole={extracted.menuRole}
      variant={VARIANTS.HEADER}
    />
  );
};
