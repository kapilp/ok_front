import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Image from 'terra-image';
import classNames from 'classnames/bind';
import MockConfig from '../../doc/common/MockConfig';
import FallbackAvatar from '../../doc/common/FallbackAvatar.svg';
import { ApplicationMenuUtility, UtilityUtils } from '../../../ApplicationUtility';
import styles from './ApplicationUtilityTestCommon.module.scss';
const cx = classNames.bind(styles);
type DefaultApplicationMenuUtilityState = {
  utilityComponent?: boolean;
};
class DefaultApplicationMenuUtility extends React.Component<{}, DefaultApplicationMenuUtilityState> {
  constructor(props) {
    super(props);
    onDiscloseUtility = onDiscloseUtility.bind(this);
    state = {
      utilityComponent: false,
    };
  }
  onDiscloseUtility(utility) {
    if (utility) {
      setState(prevState => ({
        utilityComponent: !prevState.utilityComponent,
      }));
    }
  }
  render() {
    const accessory = <Image src={FallbackAvatar} alt="Fallback Avatar" />;
    const title = 'User Name';
    return (
      <div className={cx('content-wrapper-menu')}>
        <ApplicationMenuUtility
          id="default"
          menuItems={MockConfig(accessory)}
          onChange={() => {}}
          onDisclose={onDiscloseUtility}
          initialSelectedKey="menu"
          title={title}
          accessory={accessory}
          variant={UtilityUtils.VARIANTS.MENU}
        />
      </div>
    );
  }
}
export default DefaultApplicationMenuUtility;
