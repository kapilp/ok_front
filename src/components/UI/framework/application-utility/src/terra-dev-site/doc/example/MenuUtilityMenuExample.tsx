import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Image } from '../../../../../../core/image/src/Image';
import { MockConfig } from '../common/MockConfig';
import FallbackAvatar from '../common/FallbackAvatar.svg';

import { VARIANTS } from '../../../Utils';
import classNames from 'classnames/bind';
import styles from './MenuUtilityMenuExample.module.scss';
import { UtilityMenu } from '../../../utility/_UtilityMenu';

const cx = classNames.bind(styles);
type MenuUtilityMenuExampleState = {
  selectedKey: null | string;
  requestCloseCount: number;
};
export const MenuUtilityMenuExample = (props: {}) => {
  const [state, setState] = createStore({
    selectedKey: null,
    requestCloseCount: 0,
  } as MenuUtilityMenuExampleState);
  const handleOnChange = (event: MouseEvent, object: { key: string }) => {
    setState({ selectedKey: object.key });
  };
  const handleOnRequestClose = () => {
    setState({
      requestCloseCount: state.requestCloseCount + 1,
    });
  };

  const customComponent = <Image alt="Fallback Avatar" src={FallbackAvatar} className={cx('avatar')} />;
  return (
    <div>
      <div className={cx('content-wrapper')}>
        <UtilityMenu
          initialSelectedKey="menu"
          isHeightBounded
          menuItems={MockConfig(customComponent)}
          onChange={handleOnChange}
          onRequestClose={handleOnRequestClose}
          // variant={VARIANTS.MENU}
        />
      </div>
      <div>{`Trigger event for: ${state.selectedKey}`}</div>
      <div>{`Request close count: ${state.requestCloseCount}`}</div>
    </div>
  );
};
