import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Image } from '../../../../../../core/image/src/Image';
import { MockConfig } from '../common/MockConfig';
import FallbackAvatar from '../common/FallbackAvatar.svg';
import { ApplicationHeaderUtility } from '../../../ApplicationHeaderUtility';
import { VARIANTS } from '../../../Utils';
import classNames from 'classnames/bind';
import styles from './ApplicationHeaderUtilityExample.module.scss';
const cx = classNames.bind(styles);
export const ApplicationHeaderUtilityExample = (props: {}) => {
  const [state, setState] = createStore({
    discloseCount: 0,
  });
  const onDiscloseUtility = () => {
    setState({ discloseCount: state.discloseCount + 1 });
  };

  const accessory = <Image alt="Fallback Avatar" src={FallbackAvatar} />;
  const title = 'User Name';
  return (
    <div className={cx('container')}>
      <div className={cx('content-wrapper')}>
        <ApplicationHeaderUtility
          menuItems={MockConfig(accessory)}
          onChange={() => {}}
          onDisclose={onDiscloseUtility}
          initialSelectedKey="menu"
          title={title}
          accessory={accessory}
          // variant={VARIANTS.HEADER}
        />
      </div>
      <div>{`Disclose count: ${state.discloseCount}`}</div>
    </div>
  );
};
