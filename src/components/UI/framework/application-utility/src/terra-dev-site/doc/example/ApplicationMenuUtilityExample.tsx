import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Image } from '../../../../../../core/image/src/Image';
import FallbackAvatar from '../common/FallbackAvatar.svg';
import { ApplicationMenuUtility } from '../../../ApplicationMenuUtility';
import { VARIANTS } from '../../../Utils';
import { MockConfig } from '../common/MockConfig';
import classNames from 'classnames/bind';
import styles from './ApplicationMenuUtilityExample.module.scss';
const cx = classNames.bind(styles);
type ApplicationMenuUtilityExampleState = {
  discloseCount: number;
  selectedKey: null | string;
};
// Note onChange Type not proper: it should set {key:string} object?
export const ApplicationMenuUtilityExample = (props: {}) => {
  const [state, setState] = createStore({
    discloseCount: 0,
    selectedKey: null,
  } as ApplicationMenuUtilityExampleState);

  const onDiscloseUtility = () => {
    setState({
      discloseCount: state.discloseCount + 1,
    });
  };
  const handleOnChange = (event: MouseEvent, key: string) => {
    setState({ selectedKey: key });
  };

  const accessory = <Image alt="Fallback Avatar" src={FallbackAvatar} />;
  const title = 'User Name';
  return (
    <div>
      <div className={cx('content-wrapper')}>
        <ApplicationMenuUtility
          menuItems={MockConfig(accessory)}
          onChange={handleOnChange}
          onDisclose={onDiscloseUtility}
          initialSelectedKey="menu"
          title={title}
          accessory={accessory}
          // variant={VARIANTS.MENU}
        />
      </div>
      <div>{`Trigger event for: ${state.selectedKey}.`}</div>
      <div>{`Disclose count: ${state.discloseCount}.`}</div>
    </div>
  );
};
