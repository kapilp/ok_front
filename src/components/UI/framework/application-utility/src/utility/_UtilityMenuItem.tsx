import {  JSX, Component, createEffect, Match, mergeProps, splitProps, Switch, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { Button, ButtonVariants } from '../../../../core/button/src/Button';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { SvgIconCheckmark } from '../../../../core/icon/src/icon/IconCheckmark';
import { SvgIconChevronRight } from '../../../../core/icon/src/icon/IconChevronRight';
import { Arrange } from '../../../../core/arrange/src/Arrange';
import { KEY_CODES, LOCATIONS, VARIANTS } from '../Utils';
import styles from './_UtilityMenuItem.module.scss';
const cx = classNames.bind(styles);
interface IUtilityMenuItemProps extends JSX.HTMLAttributes<Element> {
  /**
   * The unique key associated with this menu item.
   */
  itemKey: string;
  /**
   * The text to be displayed next to the menu item.
   */
  title?: string;
  /**
   * The component associated with this menu item.
   */
  content?: JSX.Element;
  /**
   * The location of this menu item.
   */
  contentLocation?: LOCATIONS;
  /**
   * Whether this item should be inset to the left. Based on if any other item has isSelected set to true.
   */
  leftInset?: boolean;
  /**
   * Indicates if the item has focus. This is used internally to control focus and does not set initial focus.
   */
  isActive?: boolean;
  /**
   * Whether the item is read-only.
   */
  isReadOnly?: boolean;
  /**
   * Whether this item is selected.
   */
  isSelected?: boolean;
  /**
   * Whether this item can be toggled.
   */
  isSelectable?: boolean;
  /**
   * Whether or not the menu item should display a disclosure indicator.
   */
  hasChevron?: boolean;
  /**
   * Function to trigger when a key is pressed.
   */
  onKeyDown?: (e: KeyboardEvent) => void;
  /**
   * Function to trigger when this item is selected.
   */
  onChange: (event: MouseEvent, key: string) => void;
  /**
   * Whether this item should be inset to the right. Based on if any other item has a chevron.
   */
  rightInset?: boolean;
  /**
   * Sets the Utility variant.
   */
  variant: VARIANTS;
}
const defaultProps = {
  title: '',
};

export const UtilityMenuItem: Component<IUtilityMenuItemProps> = (props: IUtilityMenuItemProps) => {
  props = mergeProps({}, defaultProps, props);

  let itemNode: HTMLDivElement;
  createEffect(() => {
    if (props.isActive && itemNode) {
      itemNode.focus();
    }
  });
  const setItemNode = (el: HTMLDivElement) => {
    if (el) {
      itemNode = el;
    }
  };
  const wrapOnKeyDown = (key: string, onKeyDown: (e: KeyboardEvent) => void) => {
    return (event: KeyboardEvent) => {
      if (event.keyCode === KEY_CODES.ENTER || event.keyCode === KEY_CODES.SPACE || event.keyCode === KEY_CODES.RIGHT_ARROW) {
        handleSelection(event, key);
      }
      if (onKeyDown) {
        onKeyDown(event);
      }
    };
  };
  const handleSelection = (event: MouseEvent, key: string) => {
    event.preventDefault();
    props.onChange(event, key);
  };

  const [extracted, customProps] = splitProps(props, [
    'itemKey',
    'title',
    'content',
    'contentLocation',
    'isActive',
    'isReadOnly',
    'isSelected',
    'isSelectable',
    'hasChevron',
    'leftInset',
    'onChange',
    'onKeyDown',
    'rightInset',
    'variant',
  ]);
  const theme = useContext(ThemeContext);

  const renderBodyItem = (fill: JSX.Element) => (
    <div
      {...customProps}
      tabIndex={!extracted.isReadOnly ? '0' : undefined}
      key={extracted.itemKey}
      onClick={!extracted.isReadOnly ? event => handleSelection(event, extracted.itemKey) : undefined}
      onKeyDown={!extracted.isReadOnly ? wrapOnKeyDown(extracted.itemKey, onKeyDown) : undefined}
      role={extracted.isReadOnly ? 'note' : 'button'}
      className={cx([
        { 'header-utility-body-item': extracted.variant === VARIANTS.HEADER },
        { 'menu-utility-body-item': extracted.variant === VARIANTS.MENU },
        { 'read-only': extracted.isReadOnly },
        theme.className,
      ])}
      aria-label={extracted.title}
      ref={setItemNode}
    >
      <Arrange
        fitStart={extracted.leftInset ? <SvgIconCheckmark className={cx(['checkmark', { selected: extracted.isSelected }])} /> : null}
        fill={fill}
        fillAttributes={{ className: cx('menu-item-fill') }}
        fitEnd={extracted.rightInset ? <SvgIconChevronRight className={cx(['chevron', { 'has-chevron': extracted.hasChevron }])} /> : null}
        align="center"
        className={cx([{ 'default-left-inset': !extracted.leftInset }, { 'default-right-inset': !extracted.rightInset }])}
      />
    </div>
  );
  /* eslint-enable jsx-a11y/no-static-element-interactions */
  const renderFooterButton = () => (
    <Button
      {...customProps}
      onClick={event => handleSelection(event, extracted.itemKey)}
      onKeyDown={wrapOnKeyDown(extracted.itemKey, onKeyDown)}
      variant={ButtonVariants.NEUTRAL}
      className={cx([
        { 'header-utility-footer-item': extracted.variant === VARIANTS.HEADER },
        { 'menu-utility-footer-item': extracted.variant === VARIANTS.MENU },
        theme.className,
      ])}
      text={extracted.title}
    />
  );
  // Footer items are always buttons. Body items are list items.
  // If content exists and is a body item, render content. Else, render the title text.
  return (
    <Switch fallback={renderBodyItem(<div>{extracted.title}</div>)}>
      <Match when={extracted.contentLocation === LOCATIONS.FOOTER}>{renderFooterButton()}</Match>
      <Match when={extracted.content}>{renderBodyItem(extracted.content)}</Match>
    </Switch>
  );
};
