import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';

import styles from './_UtilityMenuDivider.module.scss';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<Element> {
  /**
   * If the divider shall be placed before the header
   */
  isTop: boolean;
}
export const UtilityMenuDivider: Component<Properties> = (props: Properties) => {
  const [extracted, customProps] = splitProps(props, ['isTop']);
  const theme = useContext(ThemeContext);
  const dividerClassNames = classNames(cx('divider', { 'is-bottom': !props.isTop }, { 'is-top': props.isTop }, theme.className), customProps.className);
  return <div {...customProps} className={dividerClassNames} role="separator" />;
};
