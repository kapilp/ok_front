import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';

// import { injectIntl, intlShape } from "react-intl";

import { UtilityMenu } from './_UtilityMenu';
import { ItemShape, VARIANTS } from '../Utils';
import styles from './_UtilityButton.module.scss';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { SvgIconChevronRight } from '../../../../core/icon/src/icon/IconChevronRight';
import { SvgIconChevronDown } from '../../../../core/icon/src/icon/IconChevronDown';
const cx = classNamesBind.bind(styles);
interface IUtilityButtonProps extends JSX.HTMLAttributes<HTMLButtonElement> {
  /**
   * The array containing the menu items to be rendered within the menu.
   */
  menuItems: ItemShape[];
  /**
   * The function to trigger when a menu item is selected.
   * Returns (event, { key?:String, metaData?:Object})
   */
  onChange: (e: MouseEvent, key: string) => void;
  /**
   * The function that discloses the menu.
   */
  onDisclose: (content: JSX.Element) => void;
  /**
   * The intl object to be injected for translations. Provided by the injectIntl function.
   */
  //intl:intlShape.isRequired, //todo fix this
  /**
   * The key of the top level menu page.
   */
  initialSelectedKey: string;
  /**
   * The text associated with utilities.
   */
  title?: string;
  /**
   * The image associated with utilities.
   */
  accessory?: JSX.Element;
  /**
   * The role attribute to set on the menu.
   */
  menuRole?: string;
  /**
   * Sets the Utility variant.
   */
  variant: VARIANTS;
}
const defaultProps = {
  title: '',
};

export const UtilityButton: Component<IUtilityButtonProps> = (props: IUtilityButtonProps) => {
  props = mergeProps({}, defaultProps, props);
  const theme = useContext(ThemeContext);

  const handleOnClick = (event: MouseEvent) => {
    if (props.onDisclose) {
      const content = createContent();
      props.onDisclose(content);
    }
    if (extracted.onClick) {
      extracted.onClick(event);
    }
  };
  function createContent() {
    return <UtilityMenu initialSelectedKey={props.initialSelectedKey} menuRole={props.menuRole} menuItems={props.menuItems} onChange={props.onChange} variant={props.variant} />;
  }
  const [extracted, customProps] = splitProps(props, ['menuItems', 'onChange', 'onDisclose', 'initialSelectedKey', 'intl', 'title', 'accessory', 'variant', 'onClick', 'menuRole']);

  // todo cant add className: cx("accessory")
  let cloneAccessory = <> {extracted.accessory && extracted.accessory}</>;
  let cloneTitle = <> {extracted.title && <span className={cx('title')}>{extracted.title}</span>} </>;
  const contentContainer = (
    <span className={cx('content-container')}>
      {cloneAccessory}
      {cloneTitle}
    </span>
  );
  const buttonText = 'User Options';
  return (
    <button
      type="button"
      {...customProps}
      className={classNames(
        cx({ 'header-utility-button': extracted.variant === VARIANTS.HEADER }, { 'menu-utility-button': extracted.variant === VARIANTS.MENU }, theme.className),
        customProps.className,
      )}
      onClick={event => {
        handleOnClick(event);
      }}
      aria-label={buttonText}
    >
      {extracted.variant === VARIANTS.MENU ? contentContainer : [cloneAccessory, cloneTitle]}
      {extracted.variant === VARIANTS.MENU ? <SvgIconChevronRight className={cx('icon')} /> : <SvgIconChevronDown className={cx('icon')} />}
    </button>
  );
};

// export default injectIntl(UtilityButton);
