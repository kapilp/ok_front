import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';

// import { injectIntl, intlShape } from "react-intl";
import { UtilityMenuDivider } from './_UtilityMenuDivider';
import { ItemShape, KEY_CODES, LOCATIONS, VARIANTS } from '../Utils';
import { UtilityMenuItem } from './_UtilityMenuItem';
import styles from './_UtilityMenu.module.scss';
import { ThemeContext } from '../../../theme-context/src/ThemeContext';
import { ContentContainer } from '../../../../core/content-container/src/ContentContainer';
import { Button } from '../../../../core/button/src/Button';
import { SvgIconLeft } from '../../../../core/icon/src/icon/IconLeft';
const cx = classNamesBind.bind(styles);
interface IUtilityMenuProps extends JSX.HTMLAttributes<Element> {
  /**
   * The initial selected key. Used as the top level menu page.
   */
  initialSelectedKey: string;
  /**
   * The intl object to be injected for translations. Provided by the injectIntl function.
   */
  // intl: intlShape, // Todo fix this
  /**
   * Indicates if the height is bound to it's parent container.
   */
  isHeightBounded?: boolean;
  /**
   * The array of the menu items configs to be rendered.
   */
  UtilityMenuItems: ItemShape[];
  /**
   * The function to trigger when a menu item is selected.
   * Returns (event, { key?: String, metaData?: Object})
   */
  onChange: (e: MouseEvent, key: string) => {};
  /**
   * The function that closes the menu.
   * This will be provided by the terra-application-header or terra-application-menu.
   */
  onRequestClose?: () => void;
  /**
   *
   * The role attribute of Utility Menu.
   */
  menuRole?: string;
  /**
   * Sets the Utility variant. One of VARIANTS.HEADER, VARIANTS.MENU.
   */
  variant?: VARIANTS;
}
interface newType extends ItemShape {
  itemKey: string;
}
const processUtilityMenuItems = (items: ItemShape[]) => {
  const map = new Map<string, newType>();
  items.forEach(item => {
    map.set(item.key, { itemKey: item.key, ...item });
  });
  return map;
};
const defaultProps = {
  menuRole: 'navigation',
};
const hasChevron = (item: ItemShape) => item.childKeys && item.childKeys.length > 0;

type UtilityMenuState = {
  map?: Map<any, any>;
  currentKey?: any;
  focusIndex?: number;
  previousKeyStack?: undefined[];
  prevPropsInitialSelectedKey?: any;
  prevPropsUtilityMenuItems?: any;
  get?: any;
};
export const UtilityMenu: Component<IUtilityMenuProps> = (props: IUtilityMenuProps) => {
  props = mergeProps({}, defaultProps, props);

  let menuNode: HTMLDivElement;
  const [state, setState] = createStore({
    map: processUtilityMenuItems(props.UtilityMenuItems),
    currentKey: props.initialSelectedKey,
    focusIndex: -1,
    previousKeyStack: [],
    prevPropsInitialSelectedKey: props.initialSelectedKey,
    prevPropsUtilityMenuItems: props.UtilityMenuItems,
  });

  /*static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.initialSelectedKey !== prevState.prevPropsInitialSelectedKey
    ) {
      return { currentKey: nextProps.initialSelectedKey };
    }
    if (nextProps.UtilityMenuItems !== prevState.prevPropsUtilityMenuItems) {
      return { map: processUtilityMenuItems(nextProps.UtilityMenuItems) };
    }
    return null;
  }*/
  /*componentDidUpdate() {
    if (menuNode && state.focusIndex === -1) {
      menuNode.focus();
    }
  }*/
  const getItem = (key: string) => {
    return state.map.get(key);
  };
  const setMenuNode = (el: HTMLDivElement) => {
    if (el) {
      menuNode = el;
    }
  };
  const buildItem = (key: string, leftInset?: boolean, rightInset?: boolean, isActive?: boolean, handleOnKeyDown?: (e: KeyboardEvent) => void) => {
    const item = getItem(key);
    const chevron = hasChevron(item);
    return (
      <UtilityMenuItem
        key={key}
        itemKey={key}
        id={item.id}
        title={item.title}
        content={item.content}
        contentLocation={item.contentLocation}
        isActive={isActive}
        isReadOnly={item.isReadOnly}
        isSelected={item.isSelected}
        isSelectable={item.isSelectable}
        hasChevron={chevron}
        leftInset={leftInset}
        rightInset={rightInset}
        onChange={item.isReadOnly ? () => {} : handleOnChange}
        onKeyDown={handleOnKeyDown}
        variant={props.variant}
      />
    );
  };
  const buildListContent = (currentItem: newType) => {
    if (currentItem && currentItem.childKeys && currentItem.childKeys.length) {
      const leftInset = childrenHasCheckmark(currentItem);
      const rightInset = childrenHasChevron(currentItem);
      let index = -1;
      return (
        <div className={cx('utility-menu-body')}>
          {currentItem.childKeys.map(key => {
            if (getItem(key).contentLocation !== LOCATIONS.FOOTER) {
              index += 1;
              const onKeyDown = handleOnKeyDown(index);
              const isActive = index === state.focusIndex;
              return buildItem(key, leftInset, rightInset, isActive, onKeyDown);
            }
            return null;
          })}
        </div>
      );
    }
    return null;
  };
  const buildFooterContent = (currentItem: newType) => {
    if (currentItem && currentItem.childKeys && currentItem.childKeys.length) {
      return currentItem.childKeys.map(key => {
        if (getItem(key).contentLocation === LOCATIONS.FOOTER) {
          return buildItem(key);
        }
        return null;
      });
    }
    return null;
  };
  const childrenHasCheckmark = item => {
    const childrenHasCheckmark = item.childKeys.some(key => {
      const currentItem = getItem(key);
      return currentItem.isSelectable === true && currentItem.contentLocation !== LOCATIONS.FOOTER;
    });
    return childrenHasCheckmark;
  };
  const childrenHasChevron = (item: newType) => {
    const childrenHasChevron = item.childKeys.some(key => {
      const { childKeys } = getItem(key);
      return childKeys && childKeys.length > 0 && getItem(key).contentLocation !== LOCATIONS.FOOTER;
    });
    return childrenHasChevron;
  };
  /**
   * Function to trigger when an item is selected.
   * * Has children: navigate to the next page
   * * Endpoint: close menu and trigger onChange.
   * @param {*} event
   * @param {*} key
   */
  const handleOnChange = (event, key) => {
    const { childKeys } = getItem(key);
    const item = getItem(key);
    if (childKeys && childKeys.length > 0) {
      setState(prevState => {
        const newStack = prevState.previousKeyStack.slice();
        newStack.push(prevState.currentKey);
        return { previousKeyStack: newStack, currentKey: key };
      });
    } else {
      props.onRequestClose();
      props.onChange(event, { key, metaData: item.metaData });
    }
    if (state.focusIndex !== -1) {
      setState({ focusIndex: -1 });
    }
  };
  const handleOnKeyDown = index => {
    return event => {
      if (event.keyCode === KEY_CODES.LEFT_ARROW && state.currentKey !== props.initialSelectedKey) {
        pop();
      } else if (event.keyCode === KEY_CODES.UP_ARROW && state.focusIndex !== 0) {
        setState({ focusIndex: index - 1 });
      } else if (event.keyCode === KEY_CODES.DOWN_ARROW || event.keyCode === KEY_CODES.tab) {
        setState({ focusIndex: index + 1 });
      }
    };
  };
  const pop = () => {
    setState(prevState => {
      const newStack = prevState.previousKeyStack.slice();
      return { previousKeyStack: newStack, currentKey: newStack.pop() };
    });
  };
  const { UtilityMenuItems, initialSelectedKey, intl, isHeightBounded, onChange, onRequestClose, variant, menuRole, ...customProps } = props;
  const { currentKey } = state;
  const currentItem = getItem(currentKey);
  const firstPage = currentKey === initialSelectedKey;
  const theme = useContext(ThemeContext);
  const menuClassNames = classNames(
    cx('utility-menu', { 'header-utility-menu': variant === VARIANTS.HEADER }, { 'menu-utility-menu': variant === VARIANTS.MENU }, theme.className),
    customProps.className,
  );
  const headerClassNames = cx(['utility-menu-header', { 'header-utility-menu-header': variant === VARIANTS.HEADER }, { 'menu-utility-menu-header': variant === VARIANTS.MENU }]);

  const backText = 'Back';
  const backButton = (
    <Button
      onClick={pop}
      icon={
        <SvgIconLeft
          className={cx(['utility-menu-icon-left', { 'header-utility-menu-icon-left': variant === VARIANTS.HEADER }, { 'menu-utility-menu-icon-left': variant === VARIANTS.MENU }])}
        />
      }
      isCompact
      isIconOnly
      text={backText}
      variant={Button.Opts.Variants.UTILITY}
    />
  );
  const closeText = 'Close';
  const closeButton = (
    <Button
      onClick={props.onRequestClose}
      icon={
        <IconClose
          className={cx([
            'utility-menu-icon-close',
            { 'header-utility-menu-icon-close': variant === VARIANTS.HEADER },
            { 'menu-utility-menu-icon-close': variant === VARIANTS.MENU },
          ])}
        />
      }
      isCompact
      isIconOnly
      aria-describedby="utility-menu-header"
      text={closeText}
      variant={Button.Opts.Variants.UTILITY}
      className={cx([{ 'header-utility-menu-button-close': variant === VARIANTS.HEADER }])}
    />
  );
  let headerText;
  if (currentItem !== undefined) {
    headerText = currentItem.title;
  }
  const header = (
    <div className={headerClassNames}>
      <span
        className={cx([
          'utility-menu-content-container',
          {
            'header-utility-menu-content-container': variant === VARIANTS.HEADER,
          },
          { 'menu-utility-menu-content-container': variant === VARIANTS.MENU },
        ])}
      >
        <span
          className={cx([
            'utility-menu-left-content-container',
            {
              'header-utility-menu-left-content-container': variant === VARIANTS.HEADER,
            },
            {
              'menu-utility-menu-left-content-container': variant === VARIANTS.MENU,
            },
          ])}
        >
          {!firstPage && backButton}
          <span
            id="utility-menu-header"
            role="heading"
            aria-level="2"
            className={cx([
              {
                'header-utility-menu-initial-page-header-text': firstPage && variant === VARIANTS.HEADER,
              },
              {
                'menu-utility-menu-initial-page-header-text': firstPage && variant === VARIANTS.MENU,
              },
              {
                'header-utility-menu-noninitial-page-header-text': !firstPage && variant === VARIANTS.HEADER,
              },
              {
                'menu-utility-menu-noninitial-page-header-text': !firstPage && variant === VARIANTS.MENU,
              },
            ])}
          >
            {headerText}
          </span>
        </span>
        <span className={cx('utility-menu-right-content-container')}>{closeButton}</span>
      </span>
      <UtilityMenuDivider isTop />
    </div>
  );

  const footerItems = buildFooterContent(currentItem);
  const hasFooterItems = footerItems ? footerItems.some(item => item !== null) : null;

  let footer;
  if (hasFooterItems) {
    footer = (
      <div className={cx('utility-menu-footer')}>
        <UtilityMenuDivider className={cx('footer-divider')} />
        {footerItems}
      </div>
    );
  }

  const menuText = 'Utility Menu';
  return (
    // eslint-disable-next-line react/forbid-dom-props
    <div ref={setMenuNode} style={{ height: isHeightBounded ? '100%' : 'auto', outline: 'none' }} tabIndex="-1">
      <ContentContainer {...customProps} header={header} footer={footer} fill={isHeightBounded} className={menuClassNames} role={menuRole} aria-label={menuText}>
        {buildListContent(currentItem)}
      </ContentContainer>
    </div>
  );
  /* eslint-enable jsx-a11y/no-noninteractive-tabindex, react/forbid-dom-props */
};

// UtilityMenu.processUtilityMenuItems = processUtilityMenuItems;
// UtilityMenu.hasChevron = hasChevron;

// export default injectIntl(UtilityMenu);
