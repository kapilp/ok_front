export const KEY_CODES = {
  ENTER: 13,
  SPACE: 32,
  UP_ARROW: 38,
  DOWN_ARROW: 40,
  LEFT_ARROW: 37,
  RIGHT_ARROW: 39,
  BACK_SPACE: 8,
  TAB: 9,
};
export enum LOCATIONS {
  BODY = 'body',
  FOOTER = 'footer',
}
export enum VARIANTS {
  HEADER = 'header',
  MENU = 'menu',
}
export interface ItemShape {
  /**
   * Array containing the keys of each child item of this item.
   */
  childKeys?: string[];
  /**
   * The component associated with this item.
   */
  content?: {};
  /**
   * The location to place the item. One of Utils.LOCATIONS.BODY, Utils.LOCATIONS.FOOTER.
   */
  contentLocation?: LOCATIONS;
  /**
   * Boolean indicating if the item is read-only. Takes precedence over isSelected/Selectable.
   */
  isReadOnly?: boolean;
  /**
   * Boolean indicating if the item is selected.
   */
  isSelected?: boolean;
  /**
   * Boolean indicating if the item is selectable.
   */
  isSelectable?: boolean;
  /**
   * The unique key associated with this item.
   */
  key: string;
  /**
   * Optional meta data to be returned along with the item key within the onChange.
   */
  metaData?: {};
  /**
   * The text associated with this item.
   */
  title?: string;
}
