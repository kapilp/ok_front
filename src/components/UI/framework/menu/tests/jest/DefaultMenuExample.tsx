import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../src/Menu';
import styles from './Menu.test.module.scss';
type MenuDefaultExampleState = {
  open?: boolean;
};
class MenuDefaultExample extends React.Component<{}, MenuDefaultExampleState> {
  buttonNode: any;
  constructor(props) {
    super(props);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: true };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <Menu.Item text="testing" />
        </Menu>
        <div className={styles.container} ref={setButtonNode} />
      </div>
    );
  }
}
export default MenuDefaultExample;
