import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { List } from '../../../core/list/src/List';
import * as ListUtils from '../../../core/list/src/ListUtils';
// import List, { Utils } from "terra-list";
interface IMenuItemGroupProps extends JSX.HTMLAttributes<Element> {
  /**
   * Menu.Items to be grouped together.
   */
  children: JSX.Element;
  /**
   * Callback function called when selected index changes.
   */
  onChange?: () => void;
}
const childContextTypes = {
  isGroupItem: false,
};
const initialSingleSelectedIndex = children => {
  const childArray = React.Children.toArray(children);
  for (let i = 0; i < childArray.length; i += 1) {
    if (childArray[i].props.isSelected) {
      return i;
    }
  }
  return -1;
};

export const MenuItemGroup = (props: IMenuItemGroupProps) => {
  const [state, setState] = createStore({ selectedIndex: initialSingleSelectedIndex(props.children) });

  const getChildContext = () => {
    return { isGroupItem: true };
  };
  const handleItemSelection = (event, metaData) => {
    if (state.selectedIndex !== metaData.index) {
      event.preventDefault();
      setState({ selectedIndex: metaData.index });
      if (props.onChange) {
        props.onChange(event, metaData.index);
      }
    }
  };
  const cloneChildren = children => {
    return React.Children.map(children, (child, index) => {
      let isSelectable = true;
      if (child.props.isSelectable === false) {
        isSelectable = false;
      }
      return React.cloneElement(child, {
        isSelectable,
        isSelected: state.selectedIndex === index,
        onClick: ListUtils.wrappedOnClickForItem(child.props.onClick, handleItemSelection, { index }),
        onKeyDown: ListUtils.wrappedOnKeyDownForItem(child.props.onKeyDown, handleItemSelection, { index }),
      });
    });
  };
  const [p, customProps] = splitProps(props, ['children']);

  const managedChildren = cloneChildren(p.children);
  return (
    <List {...customProps} role="group">
      {managedChildren}
    </List>
  );
};
// MenuItemGroup.childContextTypes = childContextTypes;
