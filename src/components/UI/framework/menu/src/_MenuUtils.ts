export const isFullScreen = (isHeightBounded?: boolean, isWidthBounded?: boolean, boundingFrame?: HTMLElement, popupWidth?: number) => {
  const width = popupWidth;
  let maxWidth;
  if (boundingFrame && !isWidthBounded) {
    maxWidth = boundingFrame.clientWidth;
  } else {
    maxWidth = window.innerWidth;
  }
  if (maxWidth <= 0) {
    return false;
  }
  return isHeightBounded && (width >= maxWidth || isWidthBounded);
};
