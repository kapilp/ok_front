import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
const childContextTypes = {
  isSelectableMenu: boolean,
};
type DefaultMenuItemGroupState = {
  selectedIndex?: null;
};
class DefaultMenuItemGroup extends React.Component<{}, DefaultMenuItemGroupState> {
  constructor(props) {
    super(props);
    state = { selectedIndex: null };
    handleSelection = handleSelection.bind(this);
  }
  getChildContext() {
    return { isSelectableMenu: true };
  }
  handleSelection(event, selectedIndex) {
    setState({ selectedIndex });
  }
  render() {
    return (
      <div>
        <div id="selected-index">
          <h3>
            Selected Button:
            {state.selectedIndex}
          </h3>
        </div>
        <Menu.ItemGroup className="TestGroup" onChange={handleSelection}>
          <Menu.Item text="Group Item 1" key="1" className="TestGroupItem1" />
          <Menu.Item text="Group Item 2" key="2" className="TestGroupItem2" />
          <Menu.Item text="Group Item 3" key="3" className="TestGroupItem3" />
        </Menu.ItemGroup>
      </div>
    );
  }
}
DefaultMenuItemGroup.childContextTypes = childContextTypes;
export default DefaultMenuItemGroup;
