import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import List from "terra-list";
import Menu from "../../../../Menu";
const childContextTypes = {
  isSelectableMenu: boolean
};
export const MenuItemWithInstructionsForUse: Component = (props:{}) => {
  getChildContext() {
    return { isSelectableMenu: true };
  }
  render() {
    return (
      <div>
        <div id="isSelected">
          <p>Menu Items with eIFU Icon</p>
        </div>
        <List role="menu">
          <Menu.Item
            text="eIFU Icon Menu Item"
            key="1"
            className="TestMenuItem"
            isInstructionsForUse
          />
          <Menu.Item
            isSelectable
            text="eIFU Icon Nested Menu"
            key="2"
            className="TestMenuItem"
            isInstructionsForUse
            subMenuItems={[
              <Menu.Item
                text="Default 2.1"
                key="2.1"
                className="TestNestedMenuContent"
              />
            ]}
          />
          <Menu.Item
            text="Selectable+Selected eIFU Icon Menu Item"
            key="3"
            className="TestMenuItem"
            isSelectable
            isSelected
            isInstructionsForUse
          />
        </List>
      </div>
    );
  }
}
MenuItemWithInstructionsForUse.childContextTypes = childContextTypes;
export default MenuItemWithInstructionsForUse;
