import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
const childContextTypes = {
  isSelectableMenu: boolean,
};
type MenuItemSelectableState = {
  isSelected?: boolean;
};
class MenuItemSelectable extends React.Component<{}, MenuItemSelectableState> {
  constructor(props) {
    super(props);
    state = { isSelected: false };
    handleSelection = handleSelection.bind(this);
  }
  getChildContext() {
    return { isSelectableMenu: true };
  }
  handleSelection(event, isSelected) {
    setState({ isSelected });
  }
  render() {
    return (
      <div>
        <div id="isSelected">
          <h3>
            Item is selected:
            {state.isSelected ? 'yes' : 'no'}
          </h3>
        </div>
        <ul role="menu">
          <Menu.Item text="Selectable Menu Item" key="1" className="TestSelectableItem" isSelectable onChange={handleSelection} />
        </ul>
      </div>
    );
  }
}
MenuItemSelectable.childContextTypes = childContextTypes;
export default MenuItemSelectable;
