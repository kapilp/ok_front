import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type SelectableMenuState = {
  open?: boolean;
  isSelected?: boolean;
};
class SelectableMenu extends React.Component<{}, SelectableMenuState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    handleItemClick = handleItemClick.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: false, isSelected: false };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  handleItemClick() {
    setState(prevState => ({ isSelected: !prevState.isSelected }));
    handleRequestClose();
  }
  render() {
    return (
      <div>
        <div>This menu contains a selectable item and an unselectable item. All items in the menu should have the same spacing on the left to allow for a checkmark.</div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <Menu.Item text="Default 1" key="1" className="TestNonSelectableItem" />
          <Menu.Item text="Default 2" key="2" isSelectable className="TestSelectableItem" isSelected={state.isSelected} onClick={handleItemClick} />
        </Menu>
        <button type="button" id="default-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default SelectableMenu;
