import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type SubMenuLongTitleState = {
  open?: boolean;
};
class SubMenuLongTitle extends React.Component<{}, SubMenuLongTitleState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: false };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <div>
          This menu has a nested submenu with long Default and sub-menu title. When the Nested Menu item is clicked as submenu should replace the initial menu. There should be a
          header with a back button and a title of Nested Menu.
        </div>
        <Menu
          isOpen={state.open}
          targetRef={getButtonNode}
          onRequestClose={handleRequestClose}
          headerTitle="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        >
          <Menu.Item text="Default 1" key="1" className="TestInitialMenuContent" />
          <Menu.Item
            text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            key="2"
            className="TestNestedMenu"
            subMenuItems={[<Menu.Item text="Default 2.1" key="2.1" className="TestNestedMenuContent" />]}
          />
          <Menu.Item text="Default 3" key="3" />
          <Menu.Item text="Default 4" key="4" />
          <Menu.Item text="Default 5" key="5" />
        </Menu>
        <button type="button" id="sub-menu-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default SubMenuLongTitle;
