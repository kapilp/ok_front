import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type DefaultMenuState = {
  open?: boolean;
  items?: number[];
  map?: any;
};
class DefaultMenu extends React.Component<{}, DefaultMenuState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    addMenuItems = addMenuItems.bind(this);
    removeMenuItems = removeMenuItems.bind(this);
    state = {
      open: false,
      items: [0],
    };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  addMenuItems() {
    setState(prevState => ({
      items: [...prevState.items, prevState.items.length],
    }));
  }
  removeMenuItems() {
    setState(prevState => {
      const newItems = prevState.items.slice();
      newItems.pop();
      return { items: newItems };
    });
  }
  render() {
    return (
      <div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          {state.items.map(item => (
            <Menu.Item key={item} text={`Menu Item ${item}`} id={`TestContent${item}`} />
          ))}
        </Menu>
        <button type="button" id="default-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
        <br />
        <button type="button" id="add-button" onClick={addMenuItems}>
          Add Menu Item
        </button>
        <button type="button" id="remove-button" onClick={removeMenuItems}>
          Remove Menu Item
        </button>
      </div>
    );
  }
}
export default DefaultMenu;
