import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type DefaultMenuState = {
  open?: boolean;
};
class DefaultMenu extends React.Component<{}, DefaultMenuState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: false };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <Menu.Item text="Default Menu" id="TestContent" />
        </Menu>
        <button type="button" id="default-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default DefaultMenu;
