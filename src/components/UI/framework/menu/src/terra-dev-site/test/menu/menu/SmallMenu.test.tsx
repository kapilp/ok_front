import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type SmallMenuState = {
  open?: boolean;
};
class SmallMenu extends React.Component<{}, SmallMenuState> {
  buttonNode: any;
  forceUpdate: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: false };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <div>This menu should have a small height. And all items should be visible without scrolling.</div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <Menu.Item text="Default 1" key="1" className="TestFirstItem" />
          <Menu.Item text="Default 2" key="2" className="TestLastItem" />
        </Menu>
        <button type="button" id="small-menu-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default SmallMenu;
