import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Menu from '../../../../Menu';
import styles from './BoundedMenu.test.module.scss';
const cx = classNames.bind(styles);
type BoundedMenuState = {
  open?: boolean;
};
class BoundedMenu extends React.Component<{}, BoundedMenuState> {
  buttonNode: any;
  forceUpdate: any;
  handleOnChange: any;
  parentNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
    state = { open: false };
  }
  componentDidMount() {
    forceUpdate();
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div className={cx('content-wrapper')} ref={setParentNode}>
        <Menu boundingRef={getParentNode} isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose} classNameContent="TestBoundedContent">
          <Menu.Item text="Default 1" key="1" />
          <Menu.Item
            text="Default 2"
            key="2"
            className="TestNestedMenu"
            subMenuItems={[<Menu.Item text="Default 2.1" key="2.1" />, <Menu.Item text="Default 2.2" key="2.2" />, <Menu.Item text="Default 2.3" key="2.3" />]}
          />
          <Menu.Item text="Default 3" key="3" />
          <Menu.Item text="Default 4" key="4" />
          <Menu.Item text="Default 5" key="5" />
          <Menu.ItemGroup isSelectable onChange={handleOnChange} key="6">
            <Menu.Item text="Default 61" key="61" />
            <Menu.Item text="Default 62" key="62" />
            <Menu.Item text="Default 63" key="63" />
          </Menu.ItemGroup>
          <Menu.Item text="Default 7" key="7" />
          <Menu.Item text="Default 8" key="8" />
        </Menu>
        <button type="button" id="bounded-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default BoundedMenu;
