import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type SelectableMenuState = {
  open?: boolean;
};
class SelectableMenu extends React.Component<{}, SelectableMenuState> {
  buttonNode: any;
  handleSelection: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    state = { open: false };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  render() {
    return (
      <div>
        <div>This menu contains one selectable child (an Item Group). The menu should allow space for the checkmark and the items in the item group should be selectable.</div>
        <Menu isOpen={state.open} targetRef={getButtonNode} onRequestClose={handleRequestClose}>
          <Menu.ItemGroup className="TestGroup" onChange={handleSelection} key="selectable-group">
            <Menu.Item text="Group Item 1" key="1" className="TestGroupItem1" />
            <Menu.Item text="Group Item 2" key="2" className="TestGroupItem2" />
            <Menu.Item text="Group Item 3" key="3" className="TestGroupItem3" />
          </Menu.ItemGroup>
        </Menu>
        <button type="button" id="selectable-menu-button" onClick={handleButtonClick} ref={setButtonNode}>
          Default Menu
        </button>
      </div>
    );
  }
}
export default SelectableMenu;
