import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Menu from '../../../../Menu';
type MenuItemOnClickState = {
  clickNumber?: number;
};
class MenuItemOnClick extends React.Component<{}, MenuItemOnClickState> {
  constructor(props) {
    super(props);
    state = { clickNumber: 0 };
    handleSelection = handleSelection.bind(this);
  }
  handleSelection() {
    setState(prevState => ({ clickNumber: prevState.clickNumber + 1 }));
  }
  render() {
    return (
      <div>
        <div id="clickNumber">
          <h3>
            Item has been clicked
            {state.clickNumber} times
          </h3>
        </div>
        <ul role="menu">
          <Menu.Item text="OnClick Menu Item" key="1" className="TestOnClickItem" onClick={handleSelection} />
        </ul>
      </div>
    );
  }
}
export default MenuItemOnClick;
