import {  JSX, Component, createSignal, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Menu } from '../../../Menu';
import { Button } from '../../../../../../core/button/src/Button';
import classNames from 'classnames/bind';
import styles from './BasicMenu.module.scss';
const cx = classNames.bind(styles);
interface IBasicMenuProps extends JSX.HTMLAttributes<Element> {
  isArrowDisplayed: boolean;
  contentWidth: string;
  boundingRef: () => void;
}

type BasicMenuState = {
  open?: boolean;
  toggle1Selected?: boolean;
  toggle2Selected?: boolean;
  groupSelectedIndex?: undefined;
  actionClickCount?: number;
};
export const BasicMenu = (props: IBasicMenuProps) => {
  const [buttonNode, setButtonNode] = createSignal<HTMLButtonElement>();
  const [state, setState] = createStore({
    open: false,
    toggle1Selected: false,
    toggle2Selected: false,
    groupSelectedIndex: undefined,
    actionClickCount: 0,
  });

  const getButtonNode = () => {
    return buttonNode();
  };
  const handleButtonClick = () => {
    setState({ open: true });
  };
  const handleRequestClose = () => {
    setState({ open: false });
  };
  const handleCloseOnClick = event => {
    event.preventDefault();
    handleRequestClose();
  };
  const handleToggle1OnClick = () => {
    setState({
      toggle1Selected: !state.toggle1Selected,
    });
    handleRequestClose();
  };
  const handleToggle2OnClick = () => {
    setState({
      toggle2Selected: !state.toggle2Selected,
    });
  };
  const handleOnChange = (event, index) => {
    setState({ groupSelectedIndex: index });
  };
  const handleAction = event => {
    event.preventDefault();
    setState({ actionClickCount: state.actionClickCount + 1 });
  };

  return (
    <div>
      <div className={cx('menu-wrapper')} ref={setButtonNode}>
        <Menu
          isOpen={state.open}
          targetRef={getButtonNode}
          onRequestClose={handleRequestClose}
          contentWidth={props.contentWidth}
          isArrowDisplayed={props.isArrowDisplayed}
          boundingRef={props.boundingRef}
          headerTitle="Sample Header"
        >
          <Menu.Item text="Toggle Item 1 - Closes Menu" key="Toggle1" isSelected={state.toggle1Selected} onClick={handleToggle1OnClick} isSelectable />
          <Menu.Item text="Toggle Item 2" key="Toggle2" isSelected={state.toggle2Selected} onClick={handleToggle2OnClick} isSelectable />
          <Menu.Item text="Disabled Item 1" key="Disabled1" isSelected={state.toggle1Selected} onClick={handleToggle1OnClick} isDisabled />
          <Menu.Divider key="Divider1" />
          <Menu.Item
            text="Nested Menu 1"
            key="Nested1"
            subMenuItems={[
              <Menu.Item text="Action 1.1" key="1.1" onClick={handleAction} />,
              <Menu.Item text="Action 1.2" key="1.2" onClick={handleAction} />,
              <Menu.Item text="Action 1.3" key="1.3" onClick={handleAction} />,
              <Menu.Divider key="Divider1.1" />,
              <Menu.Item text="Close Action 1.1" key="1.4" onClick={handleCloseOnClick} />,
              <Menu.Item text="Close Action 1.2" key="1.5" onClick={handleCloseOnClick} />,
              <Menu.Item text="Close Action 1.3" key="1.6" onClick={handleCloseOnClick} />,
            ]}
          />
          <Menu.Item
            text="Nested Menu 2 has a long title that will wrap and a truncated title when clicked"
            key="Nested2"
            isDisabled
            subMenuItems={[<Menu.Item text="Default 2.1" key="2.1" />, <Menu.Item text="Default 2.2" key="2.2" />, <Menu.Item text="Default 2.3" key="2.3" />]}
          />
          <Menu.Divider key="Divider2" />
          <Menu.Item text="Close Action" key="Action2" onClick={handleCloseOnClick} />
          <Menu.Item text="Action" key="Action1" onClick={handleAction} />
          <Menu.Divider key="Divider3" />
          <Menu.ItemGroup key="Group" onChange={handleOnChange}>
            <Menu.Item text="Group Item 1" key="GroupItem1" isSelected={state.groupSelectedIndex === 0} />
            <Menu.Item text="Group Item 2" key="GroupItem2" isSelected={state.groupSelectedIndex === 1} />
            <Menu.Item text="Group Item 3" key="GroupItem3" isSelected={state.groupSelectedIndex === 2} isDisabled />
          </Menu.ItemGroup>
        </Menu>
        <Button onClick={handleButtonClick} text="Click Me" />
      </div>
      <br />
      <p>Action button has been clicked {state.actionClickCount} times.</p>
    </div>
  );
};
