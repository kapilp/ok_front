import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import Menu from 'terra-menu';
import classNames from 'classnames/bind';
import styles from './BasicMenu.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  isArrowDisplayed: boolean;
  contentWidth: string;
  boundingRef: PropTypes.func;
}
interface IMenuWithInstructionsForUseProps extends JSX.HTMLAttributes<Element> {
  boundingRef?: any;
  isArrowDisplayed?: any;
  contentWidth?: any;
}
type MenuWithInstructionsForUseState = {
  open?: boolean;
  actionClickCount?: number;
  toggle1Selected?: any;
};
class MenuWithInstructionsForUse extends React.Component<IMenuWithInstructionsForUseProps, MenuWithInstructionsForUseState> {
  buttonNode: any;
  constructor(props) {
    super(props);
    handleButtonClick = handleButtonClick.bind(this);
    handleRequestClose = handleRequestClose.bind(this);
    setButtonNode = setButtonNode.bind(this);
    getButtonNode = getButtonNode.bind(this);
    handleAction = handleAction.bind(this);
    handleCloseOnClick = handleCloseOnClick.bind(this);
    state = {
      open: false,
      actionClickCount: 0,
    };
  }
  setButtonNode(node) {
    buttonNode = node;
  }
  getButtonNode() {
    return buttonNode;
  }
  handleButtonClick() {
    setState({ open: true });
  }
  handleRequestClose() {
    setState({ open: false });
  }
  handleCloseOnClick(event) {
    event.preventDefault();
    handleRequestClose();
  }
  handleAction(event) {
    event.preventDefault();
    const newState = state;
    newState.actionClickCount += 1;
    setState(newState);
    handleRequestClose();
  }
  render() {
    return (
      <div>
        <div className={cx('menu-wrapper')} ref={setButtonNode}>
          <Menu
            isOpen={state.open}
            targetRef={getButtonNode}
            onRequestClose={handleRequestClose}
            contentWidth={props.contentWidth}
            isArrowDisplayed={props.isArrowDisplayed}
            boundingRef={props.boundingRef}
          >
            <Menu.Item text="{ApplicationName} Help" key="Help1" onClick={handleAction} isInstructionsForUse />
            <Menu.Item text="Getting Started" key="Help2" onClick={handleCloseOnClick} isSelectable />
            <Menu.Item text="About {ApplicationName}" key="Help3" isSelected={state.toggle1Selected} onClick={handleCloseOnClick} />
          </Menu>
          <Button onClick={handleButtonClick} text="Help" />
        </div>
        <br />
        <p>Instructions Icon has been clicked {state.actionClickCount} times.</p>
      </div>
    );
  }
}
export default MenuWithInstructionsForUse;
