import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import BasicMenu from "./BasicMenu";
import styles from "./MenuBounded.module.scss";
const cx = classNames.bind(styles);
export const MenuBounded: Component = (props:{}) => {
  parentNode: any;
  constructor(props) {
    super(props);
    setParentNode = setParentNode.bind(this);
    getParentNode = getParentNode.bind(this);
  }
  setParentNode(node) {
    parentNode = node;
  }
  getParentNode() {
    return parentNode;
  }
  render() {
    return (
      <div className={cx("content-wrapper")} ref={setParentNode}>
        <BasicMenu boundingRef={getParentNode} />
      </div>
    );
  }
}
export default MenuBounded;
