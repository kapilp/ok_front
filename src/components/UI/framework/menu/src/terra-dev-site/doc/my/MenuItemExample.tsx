import { MenuItem } from '../../../MenuItem';
import { MenuDivider } from '../../../MenuDivider';
import { Menu } from '../../../Menu';
import { JSX, createSignal } from 'solid-js';

export const MyMenuItemExample = () => {
  const [ref, setRef] = createSignal<HTMLButtonElement>();
  const [isOpen, setIsOpen] = createSignal(false);
  return (
    <>
      <button ref={setRef} onClick={() => setIsOpen(!isOpen())}>
        Hello
      </button>
      {isOpen() ? 1 : 2}
      <Menu isOpen={isOpen()} targetRef={() => ref()} onRequestClose={() => 0}>
        <MenuItem text="Close Action 1.1" key="1.4" />
        <MenuDivider key="Divider1.1" />,
        <MenuItem text="Close Action 1.1" key="1.4" />
      </Menu>
    </>
  );
};
