import {  JSX, Component, createEffect, createMemo, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Arrange } from '../../../core/arrange/src/Arrange';
import { SvgIconCheckmark } from '../../../core/icon/src/icon/IconCheckmark';
import { SvgIconChevronRight } from '../../../core/icon/src/icon/IconChevronRight';
import { SvgIconConsultInstructionsForUse } from '../../../core/icon/src/icon/IconConsultInstructionsForUse';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
import * as KeyCode from 'keycode-js';
import styles from './MenuItem.module.scss';
const cx = classNamesBind.bind(styles);
const contextTypes = {
  isGroupItem: false,
  isSelectableMenu: false,
};
interface IMenuItemProps extends JSX.HTMLAttributes<Element> {
  /**
   * Sets the item's text.
   */
  text?: string;
  /**
   * Indicates if item should be disabled
   */
  isDisabled?: boolean;
  /**
   * Indicates if the item is selected. A selected item is indicated with a checkmark.
   */
  isSelected?: boolean;
  /**
   * Indicates if the item should be selectable.
   */
  isSelectable?: boolean;
  /**
   * Displays the  eIFU (electronic instructions for use) icon for menu item if set to true. (This icon is used to indicate Help content that is the equivalent of an instruction manual)
   */
  isInstructionsForUse?: boolean;
  /**
   * List of Menu.Items to display in a submenu when this item is selected.
   */
  subMenuItems?: JSX.Element[];
  /**
   * Callback function for when item is clicked
   */
  onClick?: (event: Event) => void;
  /**
   * Callback function for when selection state changes on a selectable item.
   */
  onChange?: (event: Event, isSelected: boolean) => void;
  /**
   * Indicates if the item has focus. This is used internally to control focus and does not set initial focus.
   */
  isActive?: boolean;
}
const defaultProps = {
  text: '',
  isSelected: false,
  isInstructionsForUse: false,
  isActive: false,
  isSelectable: undefined,
  isDisabled: false,
  subMenuItems: [],
};

type MenuItemState = {
  isSelected: boolean;
  isActive: boolean;
};
export const MenuItem = (props: IMenuItemProps) => {
  props = mergeProps({}, defaultProps, props);
  let context = contextTypes;
  let itemNode: any;
  const [state, setState] = createStore({
    isSelected: props.isSelected && props.isSelectable && !context.isGroupItem,
    isActive: false,
  } as MenuItemState);
  createEffect(() => {
    if (props.isActive && itemNode) {
      itemNode.focus();
    }
  });

  const setItemNode = (el: HTMLLIElement) => {
    if (el) {
      itemNode = el;
    }
  };
  const handleSelection = (event: Event) => {
    event.preventDefault();
    if (props.isSelectable && !context.isGroupItem && !props.isDisabled) {
      setState(prevState => ({ isSelected: !prevState.isSelected }));
      if (props.onChange) {
        props.onChange(event, !state.isSelected);
      }
    }
  };
  const wrapOnClick = (event: Event) => {
    handleSelection(event);
    if (props.onClick) {
      props.onClick(event);
    }
  };
  const wrapOnKeyDown = (onKeyDown: (event: KeyboardEvent) => void) => {
    return (event: KeyboardEvent) => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        handleSelection(event);
        // Only add active style if the menu doesn't have a sub menu to avoid active style being stuck enabled
        if (!(props.subMenuItems && props.subMenuItems.length > 0)) {
          setState({ isActive: true });
        }
        if (props.onClick) {
          props.onClick(event);
        }
        // Remove active state when tab key is released while while holding the space key to avoid active style being stuck enabled
      } else if (event.keyCode === KeyCode.KEY_TAB) {
        setState({ isActive: false });
      }
      if (onKeyDown) {
        onKeyDown(event);
      }
    };
  };
  const wrapOnKeyUp = (onKeyUp: (event: KeyboardEvent) => void) => {
    return (event: KeyboardEvent) => {
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        setState({ isActive: false });
      }
      if (onKeyUp) {
        onKeyUp(event);
      }
    };
  };
  const [p, customProps] = splitProps(props, [
    'text',
    'isDisabled',
    'isSelected',
    'isInstructionsForUse',
    'isSelectable',
    'subMenuItems',
    'isActive',
    // added
    'hasChevron',
    'onClick',
    'onKeyDown',
  ]);
  const { isGroupItem, isSelectableMenu } = context;

  const attributes = createMemo(() => {
    const attributes = { ...customProps };
    attributes.tabIndex = p.isDisabled ? '-1' : '0';
    attributes['aria-disabled'] = p.isDisabled;
    // This is passed down by the single select list in group item and not needed

    if (p.isDisabled) {
      //
    } else {
      attributes.onClick = wrapOnClick;
      attributes.onKeyDown = wrapOnKeyDown(props.onKeyDown);
      attributes.onKeyUp = wrapOnKeyUp(props.onKeyUp);
    }
    return attributes;
  });

  const markAsSelected = createMemo(() => state.isSelected || (isGroupItem && p.isSelected));
  const itemClassNames = createMemo(() =>
    cx([
      'item',
      { selected: markAsSelected() },
      { 'is-disabled': p.isDisabled },
      // eslint-disable-next-line quote-props
      { active: state.isActive },
      customProps.className,
    ]),
  );

  const textContainer = <div className={cx('text')}>{p.text}</div>;
  const hasChevron = createMemo(() => props.subMenuItems.length > 0);

  const content = createMemo(() => {
    let content = textContainer;

    if (hasChevron() || isSelectableMenu || p.isInstructionsForUse) {
      let fitStartIcon = null;
      if (p.isInstructionsForUse) {
        fitStartIcon = <SvgIconConsultInstructionsForUse className={cx('start-icon')} />;
      } else if (isSelectableMenu) {
        fitStartIcon = <SvgIconCheckmark className={cx(['checkmark', 'start-icon'])} />;
      }
      content = <Arrange fitStart={fitStartIcon} fill={textContainer} fitEnd={hasChevron() ? <SvgIconChevronRight className={cx('chevron')} /> : null} align="center" />;
    }
    return content;
  });

  const role = createMemo(() => {
    let role = 'menuitem';
    if (isGroupItem) {
      role = 'menuitemradio';
    } else if (p.isSelectable) {
      role = 'menuitemcheckbox';
    }
    return role;
  });

  const theme = useContext(ThemeContext);
  return (
    <li {...attributes()} className={classNames(itemClassNames(), cx(theme.className))} ref={setItemNode} role={role()} aria-checked={markAsSelected()}>
      {content}
    </li>
  );
};

// MenuItem.contextTypes = contextTypes;
// export default MenuItem;
