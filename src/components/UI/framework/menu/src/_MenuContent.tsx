import {  JSX, Component, createEffect, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
/* eslint-disable-next-line import/no-extraneous-dependencies */
// import { injectIntl, intlShape } from "react-intl";
import { ContentContainer } from '../../../core/content-container/src/ContentContainer';
import { Arrange } from '../../../core/arrange/src/Arrange';
import { SvgIconLeft } from '../../../core/icon/src/icon/IconLeft';
import { SvgIconClose } from '../../../core/icon/src/icon/IconClose';
import { List } from '../../../core/list/src/List';
import classNames from 'classnames/bind';
import * as KeyCode from 'keycode-js';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
import { isFullScreen } from './_MenuUtils';
import styles from './Menu.module.scss';
const cx = classNames.bind(styles);
interface IMenuContentProps extends JSX.HTMLAttributes<Element> {
  /**
   * The intl object to be injected for translations. Provided by the injectIntl function.
   */
  // intl: intlShape, // Todo fix
  /**
   * Title the should be displayed in header.
   */
  title?: string;
  /**
   * Callback function for when back button is clicked.
   */
  onRequestBack?: () => void;
  /**
   * Callback function for when close button is clicked.
   */
  onRequestClose?: () => void;
  /**
   * Callback function that takes the content to be displayed next and is called when an item with nested content is clicked.
   */
  onRequestNext: (item: JSX.Element) => void;
  /**
   * Menu Items/Menu Groups/Menu Dividers to be displayed.
   */
  children: JSX.Element;
  /**
   * Index within the Menu Stack.
   */
  index: number;
  /**
   * Bounding container for the menu, will use window if no value provided.
   */
  boundingRef?: () => HTMLElement;
  /**
   * Indicates if the menu content should set default focus on itself.
   */
  isFocused?: boolean;
  /**
   * Indicates if menu's height has been constrained by bounding container.
   */
  isHeightBounded?: boolean;
  /**
   * Indicates if menu's width has been constrained by bounding container.
   */
  isWidthBounded?: boolean;
  /**
   * Fixed height for content.
   */
  fixedHeight?: number;
  /**
   * Fixed width for content.
   */
  fixedWidth?: number;
  /**
   * Width for content.
   */
  contentWidth?: number;
  /**
   * Indicates if the content should be hidden.
   */
  isHidden?: boolean;
  /**
   * Ref callback function to be applied to content container.
   */
  ref?: (el: Element) => void;
  /**
   * Header Title for main-menu(first-tier).
   * Header Title will only be visible if the main-menu contains at least one sub-menu.
   */
  headerTitle?: string;
}
const defaultProps = {
  isFocused: false,
  title: '',
  isWidthBounded: false,
  isHeightBounded: false,
  isHidden: false,
  headerTitle: '',
};
export type ChildContextTypes = {
  isSelectableMenu: boolean;
};

export const MenuContent = (props: IMenuContentProps) => {
  props = mergeProps({}, defaultProps, props);

  const [contentNode, setContentNode] = createSignal<HTMLDivElement>();

  let needsFocus = props.isFocused;
  const [state, setState] = createStore({
    focusIndex: -1,
  });

  const getChildContext = () => {
    return { isSelectableMenu: isSelectable() };
  };
  createEffect(() => {
    if (props.isFocused) {
      needsFocus = true; //needsFocus || props.isFocused !== props.isFocused;
    } else {
      needsFocus = false;
    }
    validateFocus(contentNode());
  });
  const onKeyDown = (event: KeyboardEvent) => {
    const focusableMenuItems = contentNode().querySelectorAll('li[tabindex="0"]');
    if (event.keyCode === KeyCode.KEY_UP) {
      // Shift focus to last focusable menu item
      focusableMenuItems[focusableMenuItems.length - 1].focus();
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      // Shift focus to first focusable menu item
      focusableMenuItems[0].focus();
    }
  };
  const onKeyDownBackButton = (event: KeyboardEvent) => {
    if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE || event.keyCode === KeyCode.KEY_LEFT) {
      event.preventDefault();
      props.onRequestBack();
    }
  };
  const validateFocus = (node: HTMLDivElement) => {
    if (needsFocus && node) {
      node.focus();
      needsFocus = document.activeElement !== node;
      // If nested menu is open
      if (props.index > 0) {
        // Shift focus to the back button
        node.querySelector('[role="button"][tabIndex="0"]').focus();
      }
    }
  };
  const handleContainerRef = (el: HTMLDivElement) => {
    if (props.ref) {
      props.ref(el);
    }
    setContentNode(el);
    validateFocus(el);
  };
  const isSelectable = () => {
    let isSelectable = false;
    // Todo fix this
    if (Array.isArray(props.children)) isSelectable = true; // below temporary solution
    /*React.Children.forEach(props.children, child => {
      if (child.props.children || child.props.isSelectable) {
        isSelectable = true;
      }
    });*/
    return isSelectable;
  };
  const wrapOnClick = item => {
    return (event: MouseEvent) => {
      event.preventDefault();
      if (state.focusIndex !== -1) {
        setState({ focusIndex: -1 });
      }
      if (item.props.subMenuItems && item.props.subMenuItems.length > 0) {
        // Avoid keydown "click" event from enter / space key triggering stack increase here
        // We handle increasing stack with keydown events in a separate handler below
        // Fixes: https://github.com/cerner/terra-core/issues/2015
        if (event.type !== 'keydown') {
          props.onRequestNext(item);
        }
      }
      if (props.onClick) {
        props.onClick(event);
      }
    };
  };
  const wrapOnKeyDown = (item, index: number) => {
    return (event: KeyboardEvent) => {
      const shiftTabClicked = event.shiftKey && event.keyCode === KeyCode.KEY_TAB;
      const tabClicked = event.keyCode === KeyCode.KEY_TAB;
      if (!(shiftTabClicked || tabClicked)) {
        event.preventDefault();
      }
      if (event.keyCode === KeyCode.KEY_RETURN || event.keyCode === KeyCode.KEY_SPACE) {
        if (item.props.subMenuItems && item.props.subMenuItems.length > 0) {
          props.onRequestNext(item);
        }
      } else if (event.keyCode === KeyCode.KEY_RIGHT) {
        if (item.props.subMenuItems && item.props.subMenuItems.length > 0) {
          props.onRequestNext(item);
        }
      } else if (event.keyCode === KeyCode.KEY_LEFT) {
        props.onRequestBack();
      } else if (event.keyCode === KeyCode.KEY_UP) {
        setState({ focusIndex: index - 1 });
      } else if (event.keyCode === KeyCode.KEY_DOWN) {
        setState({ focusIndex: index + 1 });
      }
      if (props.onKeyDown) {
        props.onKeyDown(event);
      }
    };
  };
  const buildHeader = (isFullScreen?: boolean) => {
    const backBtnText = 'Back';
    const closeBtnText = 'Close';
    const closeIcon = <SvgIconClose />;
    let closeButton = <div />;
    if (props.onRequestClose && isFullScreen) {
      closeButton = (
        <button type="button" className={cx('header-button')} onClick={props.onRequestClose} aria-label={closeBtnText}>
          {closeIcon}
        </button>
      );
    }
    const backIcon = <SvgIconLeft />;
    let header = <div />;
    if (props.index > 0) {
      header = (
        <div role="button" onClick={props.onRequestBack} onKeyDown={onKeyDownBackButton} tabIndex="0" aria-label={backBtnText}>
          <div className={cx('header-container')}>
            <div className={cx('header-button')}>{backIcon}</div>
            <h1 className={cx('header-title')}>{props.title}</h1>
          </div>
        </div>
      );
    } else if (props.headerTitle.length > 0) {
      header = <h1 className={cx(['header-title', 'main-header-title'])}>{props.headerTitle}</h1>;
    }
    return <Arrange className={cx('header')} fitEnd={closeButton} fill={header} align="center" />;
  };

  let index = -1;
  let shouldDisplayMainMenuHeader;
  // Todo Must fix this:
  /*const items = props.children ? [] : undefined;
    React.Children.map(props.children, item => {
      const onClick = wrapOnClick(item);
      let newItem = item;
      // Check if child is an enabled Menu.Item
      if (item.props.text && !item.props.isDisabled) {
        index += 1;
        const onKeyDown = wrapOnKeyDown(item, index);
        const isActive = state.focusIndex === index;
        newItem = React.cloneElement(item, {
          onClick,
          onKeyDown,
          isActive
        });
        // If the menu is first-tier and is provided with `headerTitle` prop, terra-menu should render a header.
        // Also the first-tier menu to have a header should possess at least one menu-item that drills-in to a sub-menu with sub-menu items.
        if (
          props.headerTitle.length > 0 &&
          item.props.subMenuItems &&
          item.props.subMenuItems.length > 0
        ) {
          shouldDisplayMainMenuHeader = true;
        }
        // If the child has children then it is an item group, so iterate through it's children
      } else if (item.props.children) {
        const children = item.props.children ? [] : undefined;
        React.Children.forEach(item.props.children, child => {
          if (!child.props.isDisabled) {
            index += 1;
            const clonedElement = React.cloneElement(child, {
              onKeyDown: wrapOnKeyDown(child, index),
              isActive: index === state.focusIndex
            });
            children.push(clonedElement);
          } else {
            children.push(child);
          }
        });
        newItem = React.cloneElement(item, {}, children);
      }
      items.push(newItem);
    });*/
  const boundingFrame = props.boundingRef ? props.boundingRef() : undefined;
  const isFullScreen_ = isFullScreen(props.isHeightBounded, props.isWidthBounded, boundingFrame, props.contentWidth);
  const theme = useContext(ThemeContext);
  const isSubMenu = props.index > 0;

  let header;
  if (isFullScreen_ || isSubMenu || shouldDisplayMainMenuHeader) {
    header = buildHeader(isFullScreen_);
  }

  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions, react/forbid-dom-props */
  return (
    <div
      ref={handleContainerRef}
      className={cx('content', { submenu: isSubMenu }, { 'hidden-page': props.isHidden }, { fullscreen: isFullScreen_ }, theme.className)}
      style={{
        height: props.isHeightBounded ? '100%' : props.fixedHeight,
        width: props.isWidthBounded ? undefined : props.fixedWidth,
        position: props.isHeightBounded ? 'relative' : 'static',
      }}
      tabIndex="-1"
      aria-modal="true"
      role="dialog"
      onKeyDown={onKeyDown}
    >
      <ContentContainer header={header} fill={props.isHeightBounded || props.index > 0}>
        <List className={cx('list')} role="menu">
          {/*{items}*/}
          {props.children}
        </List>
      </ContentContainer>
    </div>
  );
  /* eslint-enable jsx-a11y/no-noninteractive-element-interactions, react/forbid-dom-props */
};

// MenuContent.childContextTypes = childContextTypes;

// export default injectIntl(MenuContent);
