import {  JSX, Component, createEffect, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Popup } from '../../popup/src/Popup';
import classNames from 'classnames/bind';
import { MenuItem } from './MenuItem';
import { MenuItemGroup } from './MenuItemGroup';
import { MenuDivider } from './MenuDivider';
import { MenuContent } from './_MenuContent';
import styles from './Menu.module.scss';
const cx = classNames.bind(styles);
interface IMenuProps extends JSX.HTMLAttributes<Element> {
  /**
   * List of Menu.Item(s)/Menu.ItemGroup(s)/Menu.Divider(s) to be displayed as content within the Menu.
   */
  children: JSX.Element;
  /**
   * Callback function indicating a close condition was met, should be combined with isOpen for state management.
   */
  onRequestClose: () => void;
  /**
   * Target element for the menu to anchor to.
   */
  targetRef: () => void;
  /**
   * Bounding container for the menu, will use window if no value provided.
   */
  boundingRef?: () => void;
  /**
   * CSS classnames that are append to the arrow.
   */
  classNameArrow?: string;
  /**
   * CSS classnames that are append to the menu content inner.
   */
  classNameContent?: string;
  /**
   * CSS classnames that are append to the overlay.
   */
  classNameOverlay?: string;
  /**
   * Should the menu be presented as open.
   */
  isOpen?: boolean;
  /**
   * A string representation of the width in px, limited to:
   * 160, 240, 320, 640, 960, 1280, 1760 or auto
   */
  contentWidth?: '160' | '240' | '320' | '640' | '960' | '1280' | '1760' | 'auto';

  /**
   * Indicates if the menu should have an center aligned arrow displayed on dropdown.
   * Otherwise, the menu will display without an arrow and right aligned.
   */
  isArrowDisplayed?: boolean;
  /**
   * Header Title for main-menu(first-tier).
   * Header Title will only be visible if the main-menu contains at least one sub-menu.
   */
  headerTitle?: string;
}
const defaultProps = {
  isArrowDisplayed: false,
  isOpen: false,
  contentWidth: '240',
  headerTitle: '',
};

export const Menu = (props: IMenuProps) => {
  props = mergeProps({}, defaultProps, props);

  const [state, setState] = createStore({ stack: [], pageHeight: 0, pageWidth: 0 }); // this // todo: This instance

  createEffect(() => {
    if (
      !props.isOpen //|| props.children.length !== props.children.length
    ) {
      /* eslint-disable react/no-did-update-set-state */
      setState({ stack: [] }); // this
    }
  });

  const setPageDimensions = (el: HTMLElement) => {
    if (el) {
      setState({ pageHeight: el.clientHeight });
      if (props.contentWidth === 'auto') {
        setState({ pageWidth: el.clientWidth });
      }
    } else {
      setState({ pageHeight: undefined });
      setState({ pageWidth: undefined });
    }
  };
  const pop = () => {
    if (state.stack.length > 1) {
      setState(prevState => {
        const newStack = prevState.stack.slice();
        newStack.pop();
        // return { stack: newStack }; no need this
      });
    }
  };
  const push = item => {
    setState(prevState => {
      const newStack = prevState.stack.slice();
      newStack.push(item);
      // return { stack: newStack }; no need this
    });
  };

  const [p, customProps] = splitProps(props, [
    'boundingRef',
    'classNameArrow',
    'classNameContent',
    'classNameOverlay',
    'onRequestClose',
    'isOpen',
    'children',
    'targetRef',
    'isArrowDisplayed',
    'contentWidth',
  ]);

  const visiblePage = state.stack.length - 1;
  // Note MenuContent is <ul> list
  const slides = state.stack.map((item, index) => (
    <MenuContent
      // eslint-disable-next-line react/no-array-index-key
      key={`MenuPage-${index}`}
      title={item.props.text}
      onRequestNext={push}
      onRequestBack={pop}
      onRequestClose={props.onRequestClose}
      isHidden={index !== visiblePage}
      fixedHeight={state.pageHeight}
      fixedWidth={state.pageWidth}
      contentWidth={Popup.Opts.widths[p.contentWidth!]}
      ref={visiblePage === 0 ? setPageDimensions : null}
      index={index}
      boundingRef={p.boundingRef}
      isFocused={index === visiblePage}
      headerTitle={props.headerTitle}
    >
      {/*{item.props.children || item.props.subMenuItems}*/}
      {item}
    </MenuContent>
  ));
  return (
    <Popup
      {...customProps}
      boundingRef={p.boundingRef}
      isArrowDisplayed={p.isArrowDisplayed}
      attachmentBehavior="flip"
      contentAttachment={p.isArrowDisplayed ? 'top center' : 'top right'}
      contentHeight="auto"
      contentWidth={props.contentWidth}
      classNameArrow={cx(['arrow', { submenu: state.stack.length > 1 }, p.classNameArrow])}
      classNameContent={p.classNameContent}
      classNameOverlay={p.classNameOverlay}
      isOpen={p.isOpen}
      onRequestClose={p.onRequestClose}
      targetRef={p.targetRef}
      isHeaderDisabled
      isContentFocusDisabled
    >
      {slides}
    </Popup>
  );
};

Menu.Item = MenuItem;
Menu.ItemGroup = MenuItemGroup;
Menu.Divider = MenuDivider;
Menu.Opts = {
  widths: Popup.Opts.widths,
};
// export default Menu;
