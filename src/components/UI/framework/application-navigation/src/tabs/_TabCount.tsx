import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { injectIntl, intlShape } from 'react-intl';
import { useAnimatedCount } from '../utils/helpers';
import styles from './TabCount.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The number of notifications to display.
   */
  value: PropTypes.number;
  /**
   * Whether or not the count represents rolled up counts.
   */
  isRollup: boolean;
  /**
   * @private
   * Object containing intl APIs.
   */
  intl: intlShape;
}
const TabCount: Component = ({ value, isRollup, intl }) => {
  const countRef = useRef();
  useAnimatedCount(countRef, value);
  let validatedValue = value;
  if (isRollup) {
    validatedValue = intl.formatMessage({
      id: 'Terra.applicationNavigation.notifications.new',
    });
  } else if (value >= 999) {
    validatedValue = '99+';
  }
  return (
    <div ref={countRef} className={cx('count', { 'is-rollup': isRollup })}>
      {validatedValue}
    </div>
  );
};
export default injectIntl(TabCount);
