import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import ResizeObserver from 'resize-observer-polyfill';
import LodashDebounce from 'lodash.debounce';
import { injectIntl, intlShape } from 'react-intl';
import Popup from 'terra-popup';
import Tab from './_Tab';
import TabRollup from './_TabRollup';
import PopupMenu from '../common/_PopupMenu';
import { navigationItemsPropType } from '../utils/propTypes';
import styles from './Tabs.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * An array of configuration objects with information specifying the creation of navigation items.
   */
  navigationItems: navigationItemsPropType;
  /**
   * A function to be executed for the render of each navigation item.
   */
  navigationRenderFunction: PropTypes.func;
  /**
   * A string identifying the currently active tab.
   */
  activeTabKey: string;
  /**
   * A function to be executed upon selection of a tab.
   */
  onTabSelect: PropTypes.func;
  /**
   * Key/Value pairs associating a string key entry to a numerical notification count.
   */
  notifications: PropTypes.object;
  /**
   * @private
   * Object containing intl APIs.
   */
  intl: intlShape.isRequired;
}
const defaultProps = {
  navigationItems: [],
  notifications: {},
};
interface ITabsProps extends JSX.HTMLAttributes<Element> {
  navigationItems?: any;
  activeTabKey?: any;
  onTabSelect?: any;
  notifications?: any;
  intl?: any;
  navigationRenderFunction?: any;
  length?: any;
}
type TabsState = {
  popupIsOpen?: boolean;
};
class Tabs extends React.Component<ITabsProps, TabsState> {
  animationFrameID: null;
  childRefs: any;
  containerRef: any;
  contentWidth: any;
  forceUpdate: any;
  hiddenStartIndex: number;
  isCalculating: any;
  menuHidden: boolean;
  previousNotifications: any;
  resizeObserver: any;
  rollupInnerRef: any;
  rollupTabRef: any;
  constructor(props) {
    super(props);
    getRollupTabWidth = getRollupTabWidth.bind(this);
    closePopup = closePopup.bind(this);
    handleResize = handleResize.bind(this);
    renderRollup = renderRollup.bind(this);
    renderPopup = renderPopup.bind(this);
    buildVisibleChildren = buildVisibleChildren.bind(this);
    updateSize = LodashDebounce(updateSize.bind(this), 100);
    resetCalculations = resetCalculations.bind(this);
    containerRef = React.createRef();
    rollupTabRef = React.createRef();
    rollupInnerRef = React.createRef();
    childRefs = [];
    previousNotifications = null;
    resetCalculations();
    state = {
      popupIsOpen: false,
    };
  }
  componentDidMount() {
    if (props.navigationItems && props.navigationItems.length) {
      resizeObserver = new ResizeObserver(entries => {
        contentWidth = entries[0].contentRect.width;
        if (!isCalculating) {
          animationFrameID = window.requestAnimationFrame(() => {
            // Resetting the cache so that all elements will be rendered face-up for width calculations
            updateSize();
          });
        }
      });
      resizeObserver.observe(containerRef.current);
      handleResize(contentWidth);
    }
  }
  shouldComponentUpdate(nextProps) {
    const { navigationItems, activeTabKey, notifications } = props;
    if (navigationItems.length !== nextProps.navigationItems.length || activeTabKey !== nextProps.activeTabKey) {
      resetCalculations();
    }
    previousNotifications = notifications;
    return true;
  }
  componentDidUpdate(prevProps) {
    const { activeTabKey } = props;
    const { popupIsOpen } = state;
    if (isCalculating) {
      isCalculating = false;
      handleResize(contentWidth);
    }
    if (activeTabKey !== prevProps.activeTabKey && popupIsOpen) {
      // If the active tab has changed between updates due to updates outside of Tabs, the popup is closed.
      closePopup();
    }
  }
  componentWillUnmount() {
    updateSize.cancel();
    window.cancelAnimationFrame(animationFrameID);
    if (containerRef.current) {
      resizeObserver.disconnect(containerRef.current);
    }
    containerRef.current = null;
  }
  getRollupTabWidth() {
    if (!rollupTabRef.current) {
      return 0;
    }
    return rollupTabRef.current.getBoundingClientRect().width;
  }
  closePopup(callback) {
    setState(
      {
        popupIsOpen: false,
      },
      callback,
    );
  }
  updateSize() {
    resetCalculations();
    forceUpdate();
  }
  resetCalculations() {
    animationFrameID = null;
    hiddenStartIndex = -1;
    menuHidden = false;
    isCalculating = true;
  }
  handleResize(width) {
    // Calculate hide index
    const childrenCount = props.navigationItems.length;
    const moreWidth = width - getRollupTabWidth();
    let newHideIndex = childrenCount;
    let isMenuHidden = true;
    let calcMinWidth = 0;
    for (let i = 0; i < childrenCount; i += 1) {
      calcMinWidth += childRefs[i].current.getBoundingClientRect().width;
      if (calcMinWidth > moreWidth && !(i === childrenCount - 1 && calcMinWidth <= width)) {
        newHideIndex = i;
        isMenuHidden = false;
        break;
      }
    }
    if (hiddenStartIndex !== newHideIndex) {
      hiddenStartIndex = newHideIndex;
      menuHidden = isMenuHidden;
      forceUpdate();
    }
  }
  shouldPulse(navigationItems, notifications) {
    let shouldPulse = false;
    if (previousNotifications) {
      for (let i = 0; i < navigationItems.length; i += 1) {
        const item = navigationItems[i];
        const previousCount = previousNotifications[item.key];
        const newCount = notifications[item.key];
        if (newCount && (!previousCount || newCount > previousCount)) {
          shouldPulse = true;
          break;
        }
      }
    }
    return shouldPulse;
  }
  buildVisibleChildren(visibleTabs, hasNotifications, onTabSelect, activeTabKey, notifications) {
    return visibleTabs.map((tab, index) => {
      const tabProps = {
        text: tab.text,
        key: tab.key,
        onTabSelect: onTabSelect ? onTabSelect.bind(null, tab.key, tab.metaData) : null,
        isActive: tab.key === activeTabKey,
        notificationCount: hasNotifications ? notifications[tab.key] : 0,
        hasCount: hasNotifications,
      };
      if (isCalculating) {
        const tabRef = React.createRef();
        childRefs[index] = tabRef;
        tabProps.tabRef = tabRef;
      }
      return <Tab {...tabProps} render={props.navigationRenderFunction} />;
    });
  }
  sliceTabs(navigationItems) {
    if (hiddenStartIndex >= 0) {
      return {
        visibleTabs: navigationItems.slice(0, hiddenStartIndex),
        hiddenTabs: navigationItems.slice(hiddenStartIndex),
      };
    }
    return {
      visibleTabs: navigationItems,
      hiddenTabs: [],
    };
  }
  renderRollup(hiddenTabs, hasNotifications, hasHiddenNotification) {
    const { activeTabKey, notifications, intl } = props;
    const tabRollupIsSelected = hiddenTabs.some(tab => tab.key === activeTabKey);
    return (
      <TabRollup
        hasCount={hasNotifications}
        isPulsed={hasHiddenNotification && !isCalculating && shouldPulse(hiddenTabs, notifications)}
        onTabSelect={() => {
          setState({ popupIsOpen: true });
        }}
        tabRef={rollupTabRef}
        innerRef={rollupInnerRef}
        text={intl.formatMessage({
          id: 'Terra.applicationNavigation.tabs.rollupButtonTitle',
        })}
        isSelected={tabRollupIsSelected}
        hasChildNotifications={hasHiddenNotification}
        data-application-tabs-more
      />
    );
  }
  renderPopup(hiddenTabs) {
    const { activeTabKey, onTabSelect, notifications, intl } = props;
    return (
      <Popup
        contentHeight="auto"
        contentWidth="320"
        onRequestClose={() => {
          closePopup();
        }}
        targetRef={() => rollupInnerRef.current}
        isOpen
        isArrowDisplayed
        isContentFocusDisabled
      >
        <PopupMenu
          title={intl.formatMessage({
            id: 'Terra.applicationNavigation.tabs.rollupMenuHeaderTitle',
          })}
          role="list"
          menuItems={hiddenTabs.map(tab => ({
            key: tab.key,
            text: tab.text,
            icon: tab.icon,
            notificationCount: notifications[tab.key],
            metaData: tab.metaData,
            isActive: tab.key === activeTabKey,
          }))}
          onSelectMenuItem={(itemKey, itemMetaData) => {
            closePopup(() => {
              if (onTabSelect) {
                onTabSelect(itemKey, itemMetaData);
              }
            });
          }}
          showSelections
        />
      </Popup>
    );
  }
  render() {
    const { navigationItems, activeTabKey, onTabSelect, notifications } = props;
    if (!navigationItems || !navigationItems.length) {
      return <Tab isPlaceholder text="W" tabKey="" aria-hidden="true" />;
    }
    const { popupIsOpen } = state;
    const { visibleTabs, hiddenTabs } = sliceTabs(navigationItems);
    const hasVisibleNotification = visibleTabs.some(tab => !!notifications[tab.key]);
    const hasHiddenNotification = hiddenTabs.some(tab => !!notifications[tab.key]);
    const hasNotifications = hasVisibleNotification || hasHiddenNotification;
    return (
      <nav
        className={cx('tabs-container', {
          'is-calculating': isCalculating,
        })}
        ref={containerRef}
      >
        {buildVisibleChildren(visibleTabs, hasNotifications, onTabSelect, activeTabKey, notifications)}
        {!menuHidden ? renderRollup(hiddenTabs, hasNotifications, hasHiddenNotification) : null}
        {popupIsOpen ? renderPopup(hiddenTabs) : null}
      </nav>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default injectIntl(Tabs);
