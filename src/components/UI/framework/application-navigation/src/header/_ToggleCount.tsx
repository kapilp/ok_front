import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { useAnimatedCount } from '../utils/helpers';
import styles from './ToggleCount.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The number of notifications to display.
   */
  value: PropTypes.number;
}
const ToggleCount: Component = ({ value }) => {
  const countRef = useRef();
  useAnimatedCount(countRef, value);
  return <div ref={countRef} className={cx('count')} />;
};
export default ToggleCount;
