const titleConfigPropType = PropTypes.shape({
  /**
   * Title to be displayed or set as the aria-label if a title element is passed.
   */
  title: string.isRequired,
  /**
   * Super text to be display above the main title text.
   */
  headline: string,
  /**
   * Sub text to be display below the main title text.
   */
  subline: string,
  /**
   * Element to use in place of title text.
   */
  element: JSX.Element,
  /**
   * Whether or not the title should be hidden when at the compact breakpoint.
   */
  hideTitleWithinDrawerMenu: boolean
});
const userConfigPropType = PropTypes.shape({
  /**
   * User name to be displayed.
   */
  name: string.isRequired,
  /**
   * Additional user details.
   */
  detail: string,
  /**
   * User initials to be displayed within the avatar if no image is present.
   */
  initials: string,
  /**
   * Src to provide to the avatar component.
   */
  imageSrc: string
});
const navigationItemsPropType = PropTypes.arrayOf(
  PropTypes.shape({
    /**
     * Key matching the notification key, used as react key, and returned in the onSelect
     */
    key: string.isRequired,
    /**
     * Text display and/or aria-label.
     */
    text: string.isRequired,
    /**
     * Object to be returned in the onSelect.
     */
    metaData: PropTypes.object
  })
);
const utilityItemsPropType = PropTypes.arrayOf(
  PropTypes.shape({
    /**
     * Key used as react key, and returned in the onSelect.
     */
    key: string.isRequired,
    /**
     * Icon to be rendered.
     */
    icon: JSX.Element,
    /**
     * Text display and/or aria-label.
     */
    text: string.isRequired,
    /**
     * Object to be returned in the onSelect.
     */
    metaData: PropTypes.object
  })
);
const extensionItemsPropType = PropTypes.arrayOf(
  PropTypes.shape({
    /**
     * Key matching the notification key, used as react key, and returned in the onSelect.
     */
    key: string.isRequired,
    /**
     * Icon to be rendered.
     */
    icon: JSX.Element.isRequired,
    /**
     * Text display and/or aria-label.
     */
    text: string.isRequired,
    /**
     * Object to be returned in the onSelect.
     */
    metaData: PropTypes.object
  })
);
export default {
  titleConfigPropType,
  userConfigPropType,
  navigationItemsPropType,
  utilityItemsPropType,
  extensionItemsPropType
};
export {
  titleConfigPropType,
  userConfigPropType,
  navigationItemsPropType,
  utilityItemsPropType,
  extensionItemsPropType
};
