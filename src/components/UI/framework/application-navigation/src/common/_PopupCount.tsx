import {  JSX, createMemo, createSignal, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { useAnimatedCount } from '../utils/helpers';
import styles from './PopupCount.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Whether or not the count should be present for height calculations, but hidden from view.
   * To ensure that row heights are consistent, we need the ability to have the count and not see it.
   */
  isHidden?: boolean;
  /**
   * The number of notifications to display.
   */
  value?: number;
}
export const PopupCount = (props: Properties) => {
  const [countRef, setCountRef] = createSignal<HTMLDivElement>();
  useAnimatedCount(countRef, props.value);
  let validatedValue = props.value;
  if (props.value >= 999) {
    validatedValue = '999+';
  }
  const attrSpread = createMemo(() => (props.isHidden ? { 'aria-hidden': true } : undefined));
  return (
    <div {...attrSpread()} ref={countRef} className={cx('popup-count', { 'is-hidden': props.isHidden })}>
      {validatedValue}
    </div>
  );
};
