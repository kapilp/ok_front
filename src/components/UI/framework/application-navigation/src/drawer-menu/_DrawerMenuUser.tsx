import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Avatar from "terra-avatar";
import { userConfigPropType } from "../utils/propTypes";
import styles from "./DrawerMenuUser.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * A configuration object with information pertaining to the application's user.
   */
  userConfig: userConfigPropType.isRequired,
  /**
   * Size variant of the user. One of values [`small`, `large`].
   */
  variant: PropTypes.oneOf(["small", "large"])
};
const defaultProps = {
  variant: "small"
};
const DrawerMenuUser: Component = ({ userConfig, variant }) => (
  <div
    className={
      variant === "small" ? cx("small-user-layout") : cx("large-user-layout")
    }
  >
    <div className={cx("avatar-container")}>
      <div className={cx("avatar-outline")} />
      <div className={cx("avatar-inner")}>
        <Avatar
          alt={userConfig.name}
          image={userConfig.imageSrc}
          initials={userConfig.initials}
        />
      </div>
    </div>
    <div className={cx("info-container")}>
      <div aria-hidden className={cx("name")}>
        {userConfig.name}
      </div>
      {userConfig.detail ? (
        <div className={cx("detail")}>{userConfig.detail}</div>
      ) : null}
    </div>
  </div>
);
props = mergeProps({}, defaultProps, props)
export default DrawerMenuUser;
