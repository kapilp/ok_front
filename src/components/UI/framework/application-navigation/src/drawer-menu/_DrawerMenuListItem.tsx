import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Count from './_DrawerCount';
import { enableFocusStyles, disableFocusStyles, generateKeyDownSelection } from '../utils/helpers';
import styles from './DrawerMenuListItem.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Icon to be rendered
   */
  icon: JSX.Element;
  /**
   * Text display and/or aria-label
   */
  text: string.isRequired;
  /**
   * The number value representing the notification count.
   */
  notificationCount: PropTypes.number;
  /**
   * Function callback for item selection.
   */
  onSelect: PropTypes.func.isRequired;
  /**
   * Whether or not this item is the active item.
   */
  isSelected: boolean;
}
const DrawerMenuListItem: Component = ({ icon, text, notificationCount, isSelected, onSelect, ...customProps }) => (
  <li
    {...customProps}
    role="option"
    aria-selected={isSelected}
    tabIndex="0"
    className={cx('item', { 'is-selected': isSelected })}
    onClick={onSelect}
    onKeyDown={generateKeyDownSelection(onSelect)}
    onBlur={enableFocusStyles}
    onMouseDown={disableFocusStyles}
    data-focus-styles-enabled
  >
    {icon ? <div className={cx('icon')}>{icon}</div> : null}
    <div className={cx('text')}>{text}</div>
    {notificationCount > 0 && <Count value={notificationCount} />}
  </li>
);
export default DrawerMenuListItem;
