import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { enableFocusStyles, disableFocusStyles } from '../utils/helpers';
import styles from './DrawerMenuFooterButton.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Text display for the footer button.
   */
  text: string;
  /**
   * Function callback for button selection.
   */
  onClick: PropTypes.func;
}
const DrawerMenuFooterButton: Component = ({ text, onClick, ...customProps }) => (
  <button
    {...customProps}
    className={cx('drawer-menu-footer-button')}
    type="button"
    onClick={onClick}
    onBlur={enableFocusStyles}
    onMouseDown={disableFocusStyles}
    data-focus-styles-enabled
  >
    {text}
  </button>
);
export default DrawerMenuFooterButton;
