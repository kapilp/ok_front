import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { useAnimatedCount } from '../utils/helpers';
import styles from './DrawerCount.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The number of notifications to display.
   */
  value: PropTypes.number;
}
const DrawerCount: Component = ({ value }) => {
  const countRef = useRef();
  useAnimatedCount(countRef, value);
  let validatedValue = value;
  if (value >= 999) {
    validatedValue = '999+';
  }
  return (
    <div ref={countRef} className={cx('count')}>
      {validatedValue}
    </div>
  );
};
export default DrawerCount;
