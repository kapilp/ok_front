import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { useAnimatedCount } from '../utils/helpers';
import styles from './ExtensionCount.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The number of notifications to display.
   */
  value: PropTypes.number;
  /**
   * Whether or not the count represents rolled up counts.
   */
  isRollup: boolean;
}
const ExtensionCount: Component = ({ value, isRollup, ...customProps }) => {
  const countRef = useRef();
  useAnimatedCount(countRef, value);
  let validatedValue = value;
  if (isRollup) {
    validatedValue = null;
  } else if (value >= 99) {
    validatedValue = '99+';
  }
  return (
    <div ref={countRef} className={cx('count', { 'is-rollup': isRollup }, customProps.className)} aria-label={validatedValue}>
      {validatedValue}
    </div>
  );
};
export default ExtensionCount;
