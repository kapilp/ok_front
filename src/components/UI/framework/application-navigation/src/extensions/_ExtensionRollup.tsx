import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import { injectIntl, intlShape } from "react-intl";
import IconExtensions from "terra-icon/lib/icon/IconExtensions";
import ExtensionCount from "./_ExtensionCount";
import {
  enableFocusStyles,
  disableFocusStyles,
  generateKeyDownSelection
} from "../utils/helpers";
import styles from "./ExtensionRollup.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Whether or not hidden extensions have notifications.
   */
  hasChildNotifications: boolean,
  /**
   * Function callback for selection of the extension rollup.
   */
  onSelect: PropTypes.func,
  /**
   * Callback function for the rollup node.
   */
  extensionRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  /**
   * Whether or not the notification count should pulse.
   */
  isPulsed: boolean,
  /**
   * @private
   * Object containing intl APIs
   */
  intl: intlShape
};
const defaultProps = {
  hasChildNotifications: false,
  isPulsed: false
};
const ExtensionRollup: Component = ({
  hasChildNotifications,
  isPulsed,
  onSelect,
  extensionRef,
  intl
}) => (
  <div
    role="button"
    tabIndex="0"
    className={cx("extension-rollup")}
    onClick={onSelect}
    onKeyDown={generateKeyDownSelection(onSelect)}
    ref={extensionRef}
    onBlur={enableFocusStyles}
    onMouseDown={disableFocusStyles}
    title={intl.formatMessage({
      id: "Terra.applicationNavigation.extensions.rollupButtonDescription"
    })}
    aria-label={intl.formatMessage({
      id: "Terra.applicationNavigation.extensions.rollupButtonDescription"
    })}
    aria-haspopup
    data-focus-styles-enabled
    data-application-extension-rollup
  >
    <div className={cx("extension-rollup-inner")}>
      <div className={cx("extension-rollup-image")}>
        <IconExtensions />
      </div>
      {hasChildNotifications && (
        <ExtensionCount
          isRollup
          className={cx("extension-rollup-count")}
          value={isPulsed ? 1 : 0}
        />
      )}
    </div>
  </div>
);
props = mergeProps({}, defaultProps, props)
export default injectIntl(ExtensionRollup);
