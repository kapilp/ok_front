/* eslint-disable react/prop-types */
import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './ExampleWrapper.module.scss';
const cx = classNames.bind(styles);
export const ExampleApplication = ({ children }) => {
  const [lastActionKey, setLastActionKey] = useState(null);
  function handleOnAction(key) {
    setLastActionKey(`Current Action: ${key}`);
  }
  return (
    <div>
      {lastActionKey}
      <div className={cx('wrapper')}>{React.Children.map(children, child => React.cloneElement(child, { onAction: handleOnAction }))}</div>
    </div>
  );
};
