/* eslint-disable */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './Logo.module.scss';
const cx = classNames.bind(styles);
const Logo = () => <div className={cx('logo')} />;
export default Logo;
