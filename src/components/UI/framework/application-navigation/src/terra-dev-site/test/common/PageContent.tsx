/* eslint-disable */
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './PageContent.module.scss';
const cx = classNames.bind(styles);
const PageContent = ({ title, subtitle }) => (
  <div className={cx('outer')}>
    <div className={cx('inner')}>
      <div className={cx('center')}>
        <button className={cx('button')}>{title}</button>
        <div>{subtitle}</div>
      </div>
    </div>
  </div>
);
export default PageContent;
