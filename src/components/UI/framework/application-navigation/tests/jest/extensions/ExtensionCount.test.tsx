import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ExtensionCount from '../../../src/extensions/_ExtensionCount';
describe('ExtensionCount', () => {
  it('should render default element', () => {
    const shallowComponent = shallow(<ExtensionCount />);
    expect(shallowComponent).toMatchSnapshot();
  });
  it('should render prop data', () => {
    const shallowComponent = shallow(<ExtensionCount value={1} isRollup />);
    expect(shallowComponent).toMatchSnapshot();
  });
});
