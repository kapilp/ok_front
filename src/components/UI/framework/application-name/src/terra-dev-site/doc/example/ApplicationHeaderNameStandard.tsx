import classNames from 'classnames/bind';
import { Image, ImageVariant } from '../../../../../../core/image/src/Image';
import { ApplicationHeaderName } from '../../../ApplicationHeaderName';
import { ApplicationHeaderLayout } from '../../../../../application-header-layout/src/ApplicationHeaderLayout';
import { Placeholder } from '../common/Placeholder';

import demoColors from '../../test/application-name/demoStyles.module.scss';
import styles from './ApplicationHeaderNameStandard.module.scss';

const cx = classNames.bind(demoColors);
const cy = classNames.bind(styles);
export const ApplicationHeaderNameStandard = () => (
  <ApplicationHeaderLayout
    className={cy('demo-size')}
    logo={
      <ApplicationHeaderName
        title="App-Name"
        accessory={
          <Image
            alt="Terra Logo"
            variant={ImageVariant.ROUNDED}
            src="https://github.com/cerner/terra-framework/raw/main/terra.png"
            className={cx('demo-image-container')}
            isFluid
          />
        }
        className={cx(['demo-background-color'])}
      />
    }
    extensions={<Placeholder text="Extensions" type="header-extensions" />}
    navigation={<Placeholder text="Content" type="default" />}
    utilities={<Placeholder text="Utiltities" type="utiltities" />}
  />
);
