import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { ApplicationMenuLayout } from '../../../../../application-menu-layout/src/ApplicationMenuLayout';
import { Image, ImageVariant } from '../../../../../../core/image/src/Image';
import classNames from 'classnames/bind';
import { Placeholder } from '../common/Placeholder';
import { ApplicationMenuName } from '../../../ApplicationMenuName';
import demoColors from '../../test/application-name/demoStyles.module.scss';
import styles from './ApplicationMenuNameStandard.module.scss';

const cx = classNames.bind(demoColors);
const cy = classNames.bind(styles);
export const ApplicationMenuNameStandard = () => (
  <ApplicationMenuLayout
    className={cy('demo-size')}
    header={
      <ApplicationMenuName
        title="App-Name"
        accessory={
          <Image
            alt="Terra Logo"
            variant={ImageVariant.ROUNDED}
            src="https://github.com/cerner/terra-framework/raw/main/terra.png"
            className={cx('demo-image-container')}
            isFluid
          />
        }
        className={cx(['demo-background-color'])}
      />
    }
    extensions={<Placeholder text="Extensions" type="menu-extensions" />}
    content={<Placeholder text="Content" type="default" />}
    footer={<Placeholder text="Footer" type="footer" />}
  />
);
