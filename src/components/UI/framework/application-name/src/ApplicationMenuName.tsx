import {  JSX, Component, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../../framework/theme-context/src/ThemeContext';
import styles from './ApplicationMenuName.module.scss';
const cx = classNamesBind.bind(styles);
interface Properties extends JSX.HTMLAttributes<HTMLDivElement> {
  /**
   * The accessory element to be displayed next to the title.
   * */
  accessory?: JSX.Element;
  /**
   * The title branding of the application name.
   * */
  title?: JSX.Element;
}
export const ApplicationMenuName: Component<Properties> = (props: Properties) => {
  const [extracted, customProps] = splitProps(props, ['accessory', 'title']);
  const theme = useContext(ThemeContext);

  return (
    <div {...customProps} className={classNames(cx('application-menu-name', theme.className), customProps.className)}>
      {extracted.accessory && <div className={cx('accessory')}>{extracted.accessory}</div>}
      {extracted.title && <div className={cx('title')}>{extracted.title}</div>}
    </div>
  );
};
