import { JSX, Component, createEffect, onCleanup } from 'solid-js';
interface Properties {
  /**
   * The component(s) that will be wrapped by `<ThemeProvider />`
   */
  children: JSX.Element;
  /**
   * Name of class for specified theme
   */
  themeName?: string;
}
// Just add theme on Main Document
export const ThemeProvider: Component = (props: Properties) => {
  let oldClassName = '';
  createEffect(() => {
    oldClassName && document.documentElement.classList.remove(oldClassName);
    if (props.themeName) {
      document.documentElement.classList.add(props.themeName);
      oldClassName = props.themeName;
    }
  });
  onCleanup(() => {
    if (props.themeName) {
      document.documentElement.classList.remove(props.themeName);
    }
  });
  return props.children;
};
