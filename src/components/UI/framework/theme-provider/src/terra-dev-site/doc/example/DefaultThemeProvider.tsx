import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';

import styles from './DefaultThemeProvider.module.scss';
import { ThemeProvider } from '../../../ThemeProvider';
import { MockThemeComponent } from './MockThemeComponent';
const cx = classNames.bind(styles);

type DefaultThemeProviderState = {
  theme: string;
};
export const DefaultThemeProvider = (props: {}) => {
  const [state, setState] = createStore({
    theme: '',
  });

  const handleThemeChange = e => {
    setState({ theme: e.target.value });
  };

  /*let themeSwitcher;
    function supportsCSSVars() {
      // This eslint rule complains of the lack of .CSS support in IE, which is exactly what this line is intended to detect.
      // eslint-disable-next-line compat/compat
      return (
        window.CSS &&
        window.CSS.supports &&
        window.CSS.supports("(--fake-var: 0)")
      );
    }
    if (supportsCSSVars()) {
      themeSwitcher = (

      );
    } else {
      themeSwitcher = <div />;
    }*/
  return (
    <div>
      <div>
        <div className={cx('theme-switcher-wrapper')}>
          <label htmlFor="theme"> Theme: </label>
          <select id="theme" value={state.theme} onChange={handleThemeChange}>
            <option value="">Default</option>
            <option value="cerner-mock-theme">Mock Theme</option>
          </select>
        </div>
      </div>
      <ThemeProvider themeName={state.theme}>
        <div>
          <MockThemeComponent>Themable component</MockThemeComponent>
        </div>
      </ThemeProvider>
    </div>
  );
};
