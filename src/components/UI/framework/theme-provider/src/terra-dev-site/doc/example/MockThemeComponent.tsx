import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './MockThemeComponent.module.scss';
import '../../theme/scoped-theme.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Child node. Component to display next to the status indicator.
   */
  children: JSX.Element;
}
export const MockThemeComponent: Component = props => {
  const [extracted, customProps] = splitProps(props, ['children']);
  return (
    <div {...customProps} className={cx('mock-theme-component', customProps.className)}>
      {props.children}
    </div>
  );
};
