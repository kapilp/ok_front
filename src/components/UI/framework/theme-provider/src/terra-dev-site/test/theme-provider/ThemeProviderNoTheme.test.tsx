import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ThemeProvider from '../../../ThemeProvider';
import MockThemeComponent from '../common/MockThemeComponent';
const DefaultThemeProvider = () => (
  <ThemeProvider id="themeProvider">
    <MockThemeComponent id="themedComponent">Theme Provider Test</MockThemeComponent>
  </ThemeProvider>
);
export default DefaultThemeProvider;
