import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ThemeProvider from '../../../ThemeProvider';
import MockThemeComponent from '../common/MockThemeComponent';
const DefaultThemeProvider = () => (
  // Showing use of a default theme provider with a mock theme.
  // No theme switching in this example
  <ThemeProvider id="themeProvider" themeName="cerner-mock-theme">
    <MockThemeComponent id="themedComponent">Theme Provider Test</MockThemeComponent>
  </ThemeProvider>
);
export default DefaultThemeProvider;
