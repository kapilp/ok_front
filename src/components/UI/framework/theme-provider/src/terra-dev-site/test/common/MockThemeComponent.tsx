import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import '../../theme/scoped-theme.scss';
import styles from './MockThemeComponent.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Child node. Component to display next to the status indicator.
   */
  children: JSX.Element.isRequired;
}
const MockThemeComponent: Component = ({ children, ...customProps }) => (
  <div {...customProps} className={cx('mock-theme-component', customProps.className)}>
    {children}
  </div>
);
export default MockThemeComponent;
