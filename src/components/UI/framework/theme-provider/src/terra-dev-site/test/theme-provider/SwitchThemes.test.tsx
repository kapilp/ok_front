import { JSX, Component,   mergeProps, Show, splitProps } from 'solid-js';
import { Dynamic } from 'solid-js/web';
import { createStore } from 'solid-js/store';
import ThemeProvider from '../../../ThemeProvider';
import MockThemeComponent from '../common/MockThemeComponent';
type SwitchThemesState = {
  themeName?: string;
};
class SwitchThemes extends React.Component<{}, SwitchThemesState> {
  constructor(props) {
    super(props);
    state = {
      themeName: '',
    };
    handleSelectChange = handleSelectChange.bind(this);
  }
  handleSelectChange(event) {
    setState({ [event.target.name]: event.target.value });
  }
  render() {
    return (
      <div>
        <form>
          <label htmlFor="theme">Theme Switcher</label>
          <select id="theme" name="themeName" value={state.themeName} onChange={handleSelectChange}>
            <option value="">Default Theme</option>
            <option value="cerner-mock-theme">Mock Theme</option>
          </select>
        </form>
        <ThemeProvider id="themeProvider" themeName={state.themeName}>
          <MockThemeComponent id="themedComponent">Theme Provider Test</MockThemeComponent>
        </ThemeProvider>
      </div>
    );
  }
}
export default SwitchThemes;
