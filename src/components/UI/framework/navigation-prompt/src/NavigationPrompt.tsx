import {  JSX, Component, createEffect, onCleanup, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { v4 as uuidv4 } from 'uuid';

import { PromptRegistrationContext } from './PromptRegistrationContext';
interface INavigationPromptProps extends JSX.HTMLAttributes<Element> {
  /**
   * A string describing the content or concept for which the NavigationPrompt is being rendered.
   */
  description?: string;
  /**
   * An object containing any other pertinent information related to the NavigationPrompt.
   */
  metaData?: {};
  /**
   * @private
   * An object containing prompt registration APIs provided through the PromptRegistrationContext.
   */
  //promptRegistration: promptRegistrationContextValueShape
}

export const NavigationPrompt = (props: INavigationPromptProps) => {
  /**
   * A unique identifier is generated for each NavigationPrompt during construction. This will be used to
   * uniquely register/unregister the prompt with ancestor checkpoints without requiring consumers to
   * define unique identifiers themselves.
   */
  let uuid = uuidv4();
  const promptRegistration = useContext(PromptRegistrationContext);

  const onMount = () => {
    /**
     * If the promptRegistration value is the ProviderRegistrationContext's default value,
     * then there is not a matching NavigationPromptCheckpoint above it in the hierarchy.
     * This is possible but likely not intentional, so the component warns.
     */
    if (promptRegistration.isDefaultContextValue && process.env.NODE_ENV !== 'production') {
      /* eslint-disable no-console */
      console.warn(
        'A NavigationPrompt was not rendered within the context of a NavigationPromptCheckpoint. If this is unexpected, validate that the expected version of the terra-navigation-prompt package is installed.',
      );
      /* eslint-enable no-console */
    }
    promptRegistration.registerPrompt(uuid, props.description, props.metaData);
  };
  setTimeout(onMount, 10);

  createEffect(() => {
    promptRegistration.registerPrompt(uuid, props.description, props.metaData);
  });
  onCleanup(() => {
    promptRegistration.unregisterPrompt(uuid);
  });

  return <></>;
};
