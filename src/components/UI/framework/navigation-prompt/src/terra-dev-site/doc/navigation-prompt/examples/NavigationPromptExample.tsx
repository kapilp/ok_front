import {  JSX, createSignal,   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';

import styles from './NavigationPromptExample.module.scss';
import { NavigationPromptCheckpoint } from '../../../../NavigationPromptCheckpoint';
import { NavigationPrompt } from '../../../../NavigationPrompt';
const cx = classNames.bind(styles);
const exampleTitle = 'Descriptive Notification Prompt Title';
const exampleMessage =
  "A Notification Prompt usually has an introductory warning instructing the user that there are unsaved changes or that there is a similar situation that warrants capturing the user's attention and requires that they take action before continuing.\n\nIt is good practice to include details about the originating source (page name, side-panel title, modal header title, etc.) and location description (form name, title from the section of the page, general position, etc.) to provide the user a contextual reference as to where they have an area needing attention: e.g. where data is about to be lost, the window about to be closed, the section about to be removed. Following the initial details, it is helpful to include a detailed description educating the user about any danger or caution as to what will happen to the current items about to be lost (form data, page content, etc.) if the user chooses each of the two actions provided in the notification prompt message.";
const exampleRejectButtonText = 'Descriptive Reject Button Action';
const exampleAcceptButtonText = 'Descriptive Accept Button Action';
const exampleButtonOrder = 'acceptFirst';
type InputProps = {
  title?: string;
  ariaLabel?: string;
};
const Input = (props: InputProps) => {
  const [inputValue, setInputValue] = createSignal('');
  const [promptMetaData, setPromptMetaData] = createStore({
    value: '',
  });
  /**
   * The Input renders a NavigationPrompt when it wants to communicate the presence of its unresolved
   * state.
   */
  return (
    <div className={cx('input-content-wrapper')}>
      {inputValue() && inputValue().length ? <NavigationPrompt description={props.title} metaData={promptMetaData} /> : null}
      <div className={cx('content-wrapper')}>
        <span className={cx('title')}>{props.title}</span>
        {inputValue() && inputValue().length ? <span className={cx('prompt-text-wrapper')}>(Unsaved value is present and NavigationPrompt is rendered)</span> : null}
      </div>
      <input
        type="text"
        className={cx('input-text-box')}
        aria-label={props.ariaLabel}
        onChange={event => {
          setPromptMetaData({
            value: event.target.value,
          });
          setInputValue(event.target.value);
        }}
      />
    </div>
  );
};
type FormProps = {
  title?: string;
};
const Form = (props: FormProps) => {
  const [timeStamp, setTimeStamp] = createSignal(Date.now());
  const [inputCheckpointRef, setInputCheckpointRef] = createSignal({});
  return (
    <div className={cx('form-content-wrapper')}>
      <NavigationPromptCheckpoint ref={setInputCheckpointRef}>
        <>
          <h3>{props.title}</h3>
          <p>
            The Form renders a NavigationPromptCheckpoint around its child Input components. If NavigationPrompts are rendered by any child components, the Form will resolve those
            prompts before resetting the Input values.
          </p>
          <p>Last Updated: {new Date(timeStamp()).toLocaleString()}</p>
          <Input title="Text Input A" ariaLabel="Text Input A" />
          <Input title="Text Input B" ariaLabel="Text Input B" />
          <br />
          <button
            type="button"
            onClick={() => {
              inputCheckpointRef()
                .resolvePrompts({
                  title: exampleTitle,
                  startMessage: exampleMessage,
                  rejectButtonText: exampleRejectButtonText,
                  acceptButtonText: exampleAcceptButtonText,
                  buttonOrder: exampleButtonOrder,
                })
                .then(() => {
                  setTimeStamp(Date.now());
                })
                .catch(() => {
                  // User prevented navigation.
                });
            }}
          >
            Reset
          </button>
          <button
            type="button"
            onClick={() => {
              setTimeStamp(Date.now());
            }}
          >
            Submit
          </button>
        </>
      </NavigationPromptCheckpoint>
    </div>
  );
};
/**
 * The FormSwitcher toggles between two versions a Form to demonstrate the functionality of nested NavigationPromptCheckpoints.
 * Any NavigationPrompt registered to the Form's checkpoint will also be registered to the FormSwitcher's checkpoint.
 *
 * Before the FormSwitcher changes the Form type, the FormSwitcher resolves the prompts below its checkpoint with the default prompt title and message.
 * If the user confirms the switch, the Form component is unmounted and replace with a new version containing no unsaved changes. If the user denies the switch,
 * no action is taken.
 */
const FormSwitcher = () => {
  const [activeForm, setActiveForm] = createSignal('Form 1');
  const [formCheckpointRef, setFormCheckpointRef] = createSignal();
  function onSwitchForm(formKey: string) {
    formCheckpointRef()
      .resolvePrompts({
        title: exampleTitle,
        startMessage: exampleMessage,
        rejectButtonText: exampleRejectButtonText,
        acceptButtonText: exampleAcceptButtonText,
        buttonOrder: exampleButtonOrder,
      })
      .then(() => {
        setActiveForm(formKey);
      })
      .catch(() => {
        // User prevented form switch.
      });
  }
  return (
    <div>
      <h2>Form Switcher</h2>
      <p>
        The FormSwitcher is an example component that uses the NavigationPrompt and NavigationPromptCheckpoint. If NavigationPrompts are rendered by any child components, the
        FormSwitcher will resolve those prompts before rendering a different Form.
      </p>
      <button type="button" disabled={activeForm === 'Form 1'} onClick={onSwitchForm.bind(null, 'Form 1')}>
        Switch to Form 1
      </button>
      <button type="button" disabled={activeForm === 'Form 2'} onClick={onSwitchForm.bind(null, 'Form 2')}>
        Switch to Form 2
      </button>
      <br />
      <br />
      <NavigationPromptCheckpoint ref={setFormCheckpointRef}>
        <Form title={activeForm()} key={activeForm()} />
      </NavigationPromptCheckpoint>
    </div>
  );
};
/**
 * The NavigationPromptExample renders a NavigationPromptCheckpoint around the FormSwitcher to demonstrate the
 * functionality of the onPromptChange callback. onPromptChange receives the current set of prompts as its first argument,
 * and it executes whenever a NavigationPrompt registers or unregisters with a NavigationPromptCheckpoint.
 */
export const NavigationPromptExample = () => {
  const [prompts, setPrompts] = createSignal([]);
  return (
    <div className={cx('example-content-wrapper')}>
      <span className={cx('title')}>Registered Prompts: </span>
      {prompts().length ? (
        <span>
          {prompts()
            .map(prompt => `${prompt.description} (${prompt.metaData.value})`)
            .join(', ')}
        </span>
      ) : null}
      <br />
      <hr />
      <NavigationPromptCheckpoint
        onPromptChange={newPrompts => {
          setPrompts(newPrompts);
        }}
      >
        <FormSwitcher />
      </NavigationPromptCheckpoint>
    </div>
  );
};
