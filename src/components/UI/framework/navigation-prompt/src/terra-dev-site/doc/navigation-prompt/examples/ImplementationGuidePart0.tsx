import {  JSX, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
type FormProps = {
  title?: string;
  ariaLabel?: string;
};
const Form: React.SFC<FormProps> = ({ title, ariaLabel }) => {
  const [inputValue, setInputValue] = useState('');
  return (
    <div>
      <p>{title}</p>
      <input
        aria-label={ariaLabel}
        type="text"
        onChange={event => {
          setInputValue(event.target.value);
        }}
        value={inputValue}
      />
      <button
        type="button"
        onClick={() => {
          setInputValue('');
        }}
      >
        Submit
      </button>
    </div>
  );
};
const FormSwitcher = () => {
  const [activeForm, setActiveForm] = useState('Form 1');
  function onSwitchForm(formKey) {
    setActiveForm(formKey);
  }
  return (
    <div>
      <h2>Form Switcher</h2>
      <p>The NavigationPrompt is not implemented, so no prompting occurs.</p>
      <button type="button" disabled={activeForm === 'Form 1'} onClick={onSwitchForm.bind(null, 'Form 1')}>
        Switch to Form 1
      </button>
      <button type="button" disabled={activeForm === 'Form 2'} onClick={onSwitchForm.bind(null, 'Form 2')}>
        Switch to Form 2
      </button>
      <Form title={activeForm} key={activeForm} ariaLabel={activeForm} />
    </div>
  );
};
export default FormSwitcher;
