import {  JSX, onCleanup, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { PromptRegistrationContext, promptRegistrationContextValueShape } from './PromptRegistrationContext';

import { NotificationDialog } from '../../notification-dialog/src/NotificationDialog';
interface INavigationPromptCheckpointProps extends JSX.HTMLAttributes<Element> {
  /**
   * Components to render within the context of the NavigationPromptCheckpoint. Any NavigationPrompts rendered within
   * these child components will be registered to this NavigationPromptCheckpoint instance.
   */
  children?: JSX.Element;
  /**
   * A function that will be executed when NavigationPrompts are registered to or unregistered from the NavigationPromptCheckpoint instance.
   * This can be used to track registered prompts outside of a NavigationPromptCheckpoint and handle prompt resolution directly, if necessary.
   * The function will be provided with an array of data objects representing the registered NavigationPrompts as the sole argument. An empty
   * Array will be provided when no prompts are registered.
   *
   * Function Example:
   *
   * ```
   * (arrayOfPrompts) => {
   *   arrayOfPrompts.forEach((prompt) => {
   *     console.log(prompt.description);
   *     console.log(prompt.metaData);
   *   })
   *   myLocalPromptRegistry = arrayOfPrompts;
   * }
   * ```
   */
  onPromptChange?: (newPrompts: []) => void;
  /**
   * @private
   * An object containing prompt registration APIs provided through the PromptRegistrationContext.
   */
  //promptRegistration: promptRegistrationContextValueShape;
}

function getPromptArray(prompts) {
  return Object.keys(prompts).map(id => prompts[id]);
}
export const NavigationPromptCheckpoint = (props: INavigationPromptCheckpointProps) => {
  const registeredPrompts = {};
  const promptProviderValue = {
    registerPrompt: registerPrompt,
    unregisterPrompt: unregisterPrompt,
  };
  const [state, setState] = createStore({
    notificationDialogProps: undefined,
  });
  const promptRegistration = useContext(PromptRegistrationContext);
  onCleanup(() => {
    if (props.onPromptChange) {
      /**
       * The consumer is notified on unmount with an empty set of prompt data to clean up any previously mounted prompts.
       */
      props.onPromptChange([]);
    }
  });
  function registerPrompt(promptId, description, metaData) {
    if (!promptId && process.env.NODE_ENV !== 'production') {
      /* eslint-disable no-console */
      console.warn('A NavigationPrompt cannot be registered without an identifier.');
      /* eslint-enable no-console */
      return;
    }
    registeredPrompts[promptId] = { description, metaData };
    if (props.onPromptChange) {
      props.onPromptChange(getPromptArray(registeredPrompts));
    }
    promptRegistration.registerPrompt(promptId, description, metaData);
  }
  function unregisterPrompt(promptId) {
    if (!registeredPrompts[promptId]) {
      return;
    }
    delete registeredPrompts[promptId];
    if (props.onPromptChange) {
      props.onPromptChange(getPromptArray(registeredPrompts));
    }
    promptRegistration.unregisterPrompt(promptId);
  }
  /**
   * `resolvePrompts` returns a Promise that will be resolved or rejected based upon the presence of child prompts and
   * the the actions taken by a user from the checkpoint's NotificationDialog.
   *
   * This function is part of the NavigationPromptCheckpoint's public API. Changes to this function name or overall functionality
   * will impact the package's version.
   */
  function resolvePrompts(options = {}) {
    /**
     * If no prompts are registered, then no prompts must be rendered.
     */
    if (!Object.keys(registeredPrompts).length) {
      return Promise.resolve();
    }
    let showDialogOptions = options;
    if (typeof showDialogOptions === 'function') {
      showDialogOptions = showDialogOptions(NavigationPromptCheckpoint.getPromptArray(registeredPrompts));
    }
    /**
     * Otherwise, the NotificationDialog is shown.
     */
    return new Promise((resolve, reject) => {
      setState({
        notificationDialogProps: {
          title: showDialogOptions.title,
          message: showDialogOptions.message,
          startMessage: showDialogOptions.startMessage,
          content: showDialogOptions.content,
          endMessage: showDialogOptions.endMessage,
          acceptButtonText: showDialogOptions.acceptButtonText,
          rejectButtonText: showDialogOptions.rejectButtonText,
          emphasizedAction: showDialogOptions.emphasizedAction,
          buttonOrder: showDialogOptions.buttonOrder,
          onAccept: resolve,
          onReject: reject,
        },
      });
    });
  }
  const renderNotificationDialog = () => {
    const acceptButton = {
      text: state.notificationDialogProps.acceptButtonText,
      onClick: () => {
        state.notificationDialogProps.onAccept();
        setState({ notificationDialogProps: undefined });
      },
    };
    const rejectButton = {
      text: state.notificationDialogProps.rejectButtonText,
      onClick: () => {
        state.notificationDialogProps.onReject();
        setState({ notificationDialogProps: undefined });
      },
    };
    return (
      <NotificationDialog
        isOpen
        title={state.notificationDialogProps.title}
        startMessage={state.notificationDialogProps.startMessage || state.notificationDialogProps.message}
        endMessage={state.notificationDialogProps.endMessage}
        content={state.notificationDialogProps.content}
        acceptAction={acceptButton}
        rejectAction={rejectButton}
        buttonOrder={state.notificationDialogProps.buttonOrder}
        emphasizedAction={state.notificationDialogProps.emphasizedAction}
        variant="warning"
      />
    );
  };

  const [p, customProps] = splitProps(props, ['children']);
  if (props.ref) {
    props.ref({ resolvePrompts });
  }

  return (
    <PromptRegistrationContext.Provider value={promptProviderValue}>
      {p.children}
      {state.notificationDialogProps ? renderNotificationDialog() : undefined}
    </PromptRegistrationContext.Provider>
  );
};
// export default withPromptRegistration(NavigationPromptCheckpoint);
