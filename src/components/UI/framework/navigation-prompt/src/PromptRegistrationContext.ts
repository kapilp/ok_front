import {  Component, createContext, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
/**
 * The default value for the PromptRegistrationContext includes a flag to indicate that the value is the default value,
 * which can be used to detect whether any matching context Providers are rendered. Also included are no-op implementations
 * for register/unregisterPrompt to minimize branching logic in the context consumers.
 */
export const PromptRegistrationContext = createContext({
  isDefaultContextValue: true,
  registerPrompt: () => {},
  unregisterPrompt: () => {},
});
export type promptRegistrationContextValueShape = {
  isDefaultContextValue?: boolean;
  registerPrompt: () => void;
  unregisterPrompt: () => void;
};
