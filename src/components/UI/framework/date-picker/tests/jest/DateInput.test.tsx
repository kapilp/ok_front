import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
/* eslint-disable import/no-extraneous-dependencies */
import { mountWithIntl, renderWithIntl } from 'terra-enzyme-intl';
import ThemeContextProvider from 'terra-theme-context/lib/ThemeContextProvider';
import DateInput from '../../lib/DateInput';
it('should render a default date input', () => {
  const dateInput = renderWithIntl(<DateInput />);
  expect(dateInput).toMatchSnapshot();
});
it('should render a default date input with all props', () => {
  const dateInput = renderWithIntl(
    <DateInput
      inputAttributes={{ id: 'terra-date-input' }}
      name="date-input"
      placeholder="MM/DD/YYYY"
      value="01/01/2017"
      onBlur={() => {}}
      onChange={() => {}}
      onClick={() => {}}
      onKeyDown={() => {}}
    />,
  );
  expect(dateInput).toMatchSnapshot();
});
it('should pass in ref as the ref prop of the calendar button', () => {
  const ref = jest.fn();
  const dateInput = mountWithIntl(<DateInput buttonRefCallback={ref} />);
  const testComponent = dateInput.children();
  expect(ref).toBeCalled();
  expect(testComponent).toMatchSnapshot();
});
it('correctly applies the theme context className', () => {
  const date = mountWithIntl(
    <ThemeContextProvider theme={{ className: 'orion-fusion-theme' }}>
      <DateInput />
    </ThemeContextProvider>,
  );
  expect(date).toMatchSnapshot();
});
