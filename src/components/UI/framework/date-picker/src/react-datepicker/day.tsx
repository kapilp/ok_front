import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import { injectIntl, intlShape } from "react-intl";
import VisuallyHiddenText from "terra-visually-hidden-text";
import {
  getDay,
  getMonth,
  getDate,
  now,
  isSameDay,
  isDayDisabled,
  isDayInRange,
  getDayOfWeekCode,
  getLocalizedDateForScreenReader
} from "./date_utils";
import styles from "./stylesheets/react_datepicker.module.scss";
const cx = classNames.bind(styles);
interface IDayProps extends JSX.HTMLAttributes<Element> {
  day: any;
  dayClassName?: (...args: any[]) => any;
  endDate?: any;
  highlightDates?: any;
  inline?: boolean;
  intl?: any;
  month?: number;
  onClick?: (...args: any[]) => any;
  onMouseEnter?: (...args: any[]) => any;
  preSelection?: any;
  selected?: any;
  selectingDate?: any;
  selectsEnd?: boolean;
  selectsStart?: boolean;
  startDate?: any;
  utcOffset?: number;
  isCalendarKeyboardFocused?: boolean;
}
export const Day: Component = (props:IDayProps) => {
  handleClick = event => {
    if (!isDisabled() && props.onClick) {
      props.onClick(event);
    }
  };
  handleMouseEnter = event => {
    if (!isDisabled() && props.onMouseEnter) {
      props.onMouseEnter(event);
    }
  };
  isSameDay = other => isSameDay(props.day, other);
  isKeyboardSelected = () =>
    !props.inline &&
    !isSameDay(props.selected) &&
    isSameDay(props.preSelection);
  isDisabled = () => isDayDisabled(props.day, props);
  getHighLightedClass = defaultClassName => {
    const { day, highlightDates } = props;
    if (!highlightDates) {
      return false;
    }
    // Looking for className in the Map of {'day string, 'className'}
    const dayStr = day.format("MM.DD.YYYY");
    return highlightDates.get(dayStr);
  };
  isInRange = () => {
    const { day, startDate, endDate } = props;
    if (!startDate || !endDate) {
      return false;
    }
    return isDayInRange(day, startDate, endDate);
  };
  isInSelectingRange = () => {
    const {
      day,
      selectsStart,
      selectsEnd,
      selectingDate,
      startDate,
      endDate
    } = props;
    if (!(selectsStart || selectsEnd) || !selectingDate || isDisabled()) {
      return false;
    }
    if (selectsStart && endDate && selectingDate.isSameOrBefore(endDate)) {
      return isDayInRange(day, selectingDate, endDate);
    }
    if (selectsEnd && startDate && selectingDate.isSameOrAfter(startDate)) {
      return isDayInRange(day, startDate, selectingDate);
    }
    return false;
  };
  isSelectingRangeStart = () => {
    if (!isInSelectingRange()) {
      return false;
    }
    const { day, selectingDate, startDate, selectsStart } = props;
    if (selectsStart) {
      return isSameDay(day, selectingDate);
    } else {
      return isSameDay(day, startDate);
    }
  };
  isSelectingRangeEnd = () => {
    if (!isInSelectingRange()) {
      return false;
    }
    const { day, selectingDate, endDate, selectsEnd } = props;
    if (selectsEnd) {
      return isSameDay(day, selectingDate);
    } else {
      return isSameDay(day, endDate);
    }
  };
  isRangeStart = () => {
    const { day, startDate, endDate } = props;
    if (!startDate || !endDate) {
      return false;
    }
    return isSameDay(startDate, day);
  };
  isRangeEnd = () => {
    const { day, startDate, endDate } = props;
    if (!startDate || !endDate) {
      return false;
    }
    return isSameDay(endDate, day);
  };
  isWeekend = () => {
    const weekday = getDay(props.day);
    return weekday === 0 || weekday === 6;
  };
  isOutsideMonth = () => {
    return (
      props.month !== undefined &&
      props.month !== getMonth(props.day)
    );
  };
  getClassNames = date => {
    const dayClassName = props.dayClassName
      ? props.dayClassName(date)
      : undefined;
    return [
      "react-datepicker-day",
      dayClassName,
      "react-datepicker-day--" + getDayOfWeekCode(props.day),
      {
        "react-datepicker-day--disabled": isDisabled(),
        "react-datepicker-day--selected": isSameDay(props.selected),
        "react-datepicker-day--selected-border":
          isSameDay(props.preSelection) &&
          (document.activeElement.tagName === "DIV" ||
            document.activeElement ===
              document.querySelector('[class*="previous"]')) &&
          props.isCalendarKeyboardFocused,
        "react-datepicker-day--range-start": isRangeStart(),
        "react-datepicker-day--range-end": isRangeEnd(),
        "react-datepicker-day--in-range": isInRange(),
        "react-datepicker-day--in-selecting-range": isInSelectingRange(),
        "react-datepicker-day--selecting-range-start": isSelectingRangeStart(),
        "react-datepicker-day--selecting-range-end": isSelectingRangeEnd(),
        "react-datepicker-day--today": isSameDay(
          now(props.utcOffset)
        ),
        "react-datepicker-day--weekend": isWeekend(),
        "react-datepicker-day--outside-month": isOutsideMonth(),
        "is-calendar-focused--keyboard-focus":
          props.isCalendarKeyboardFocused &&
          isKeyboardSelected() &&
          document.activeElement.tagName === "DIV"
      },
      getHighLightedClass("react-datepicker-day--highlighted")
    ];
  };
  render() {
    const { day } = props;
    return (
      <div
        className={cx(getClassNames(day))}
        onClick={handleClick}
        onMouseEnter={handleMouseEnter}
        aria-disabled={isDisabled()}
      >
        <>
          <VisuallyHiddenText
            text={getLocalizedDateForScreenReader(day, props)}
          />
          <span aria-hidden="true">{getDate(day)}</span>
        </>
      </div>
    );
  }
}
export default injectIntl(Day);
