import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { FormattedMessage } from 'react-intl';
import YearDropdownOptions from './year_dropdown_options';
import onClickOutside from 'react-onclickoutside';
import { getYear } from './date_utils';
import styles from './stylesheets/react_datepicker.module.scss';
const cx = classNames.bind(styles);
var WrappedYearDropdownOptions = onClickOutside(YearDropdownOptions);
interface IYearDropdownProps extends JSX.HTMLAttributes<Element> {
  adjustDateOnChange?: boolean;
  dropdownMode: 'scroll' | 'select';
  maxDate?: any;
  minDate?: any;
  onChange: (...args: any[]) => any;
  scrollableYearDropdown?: boolean;
  year: number;
  yearDropdownItemNumber?: number;
  date?: any;
  onSelect?: (...args: any[]) => any;
  ref?: (...args: any[]) => any;
  setOpen?: (...args: any[]) => any;
  onClick?: (...args: any[]) => any;
  onKeyDown?: (...args: any[]) => any;
}
type YearDropdownState = {
  dropdownVisible?: boolean;
};
export default class YearDropdown extends React.Component<IYearDropdownProps, YearDropdownState> {
  state = {
    dropdownVisible: false,
  };
  renderSelectOptions = () => {
    const minYear = getYear(props.minDate);
    const maxYear = getYear(props.maxDate);
    const options = [];
    for (let i = minYear; i <= maxYear; i++) {
      options.push(
        <option key={i} value={i}>
          {i}
        </option>,
      );
    }
    return options;
  };
  onSelectChange = e => {
    onChange(e.target.value);
  };
  renderSelectMode = () => (
    <FormattedMessage id="Terra.datePicker.yearLabel">
      {label => (
        <select onClick={props.onClick} onKeyDown={props.onKeyDown} aria-label={label} value={props.year} className={cx('react-datepicker-year-select')} onChange={onSelectChange}>
          {renderSelectOptions()}
        </select>
      )}
    </FormattedMessage>
  );
  renderReadView = visible => (
    <div key="read" style={{ visibility: visible ? 'visible' : 'hidden' }} className={cx('react-datepicker-year-read-view')} onClick={event => toggleDropdown(event)}>
      <span className={cx('react-datepicker-year-read-view--down-arrow')} />
      <span className={cx('react-datepicker-year-read-view--selected-year')}>{props.year}</span>
    </div>
  );
  renderDropdown = () => (
    <WrappedYearDropdownOptions
      key="dropdown"
      ref="options"
      year={props.year}
      onChange={onChange}
      onCancel={toggleDropdown}
      minDate={props.minDate}
      maxDate={props.maxDate}
      scrollableYearDropdown={props.scrollableYearDropdown}
      yearDropdownItemNumber={props.yearDropdownItemNumber}
    />
  );
  renderScrollMode = () => {
    const { dropdownVisible } = state;
    let result = [renderReadView(!dropdownVisible)];
    if (dropdownVisible) {
      result.unshift(renderDropdown());
    }
    return result;
  };
  onChange = year => {
    toggleDropdown();
    if (year === props.year) return;
    props.onChange(year);
  };
  toggleDropdown = event => {
    setState(
      {
        dropdownVisible: !state.dropdownVisible,
      },
      () => {
        if (props.adjustDateOnChange) {
          handleYearChange(props.date, event);
        }
      },
    );
  };
  handleYearChange = (date, event) => {
    onSelect(date, event);
    setOpen();
  };
  onSelect = (date, event) => {
    if (props.onSelect) {
      props.onSelect(date, event);
    }
  };
  setOpen = () => {
    if (props.setOpen) {
      props.setOpen(true);
    }
  };
  render() {
    let renderedDropdown;
    switch (props.dropdownMode) {
      case 'scroll':
        renderedDropdown = renderScrollMode();
        break;
      case 'select':
        renderedDropdown = renderSelectMode();
        break;
    }
    return (
      <div ref={props.ref} className={cx(['react-datepicker-year-dropdown-container', `react-datepicker-year-dropdown-container--${props.dropdownMode}`])}>
        {renderedDropdown}
      </div>
    );
  }
}
