import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './stylesheets/react_datepicker.module.scss';
const cx = classNames.bind(styles);
function generateYears(year, noOfYear, minDate, maxDate) {
  var list = [];
  for (var i = 0; i < 2 * noOfYear + 1; i++) {
    const newYear = year + noOfYear - i;
    let isInRange = true;
    if (minDate) {
      isInRange = minDate.year() <= newYear;
    }
    if (maxDate && isInRange) {
      isInRange = maxDate.year() >= newYear;
    }
    if (isInRange) {
      list.push(newYear);
    }
  }
  return list;
}
interface IYearDropdownOptionsProps extends JSX.HTMLAttributes<Element> {
  minDate?: any;
  maxDate?: any;
  onCancel: (...args: any[]) => any;
  onChange: (...args: any[]) => any;
  scrollableYearDropdown?: boolean;
  year: number;
  yearDropdownItemNumber?: number;
}
type YearDropdownOptionsState = {
  yearsList?: any[];
  map?: any;
};
export default class YearDropdownOptions extends React.Component<IYearDropdownOptionsProps, YearDropdownOptionsState> {
  constructor(props) {
    super(props);
    const { yearDropdownItemNumber, scrollableYearDropdown } = props;
    const noOfYear = yearDropdownItemNumber || (scrollableYearDropdown ? 10 : 5);
    state = {
      yearsList: generateYears(props.year, noOfYear, props.minDate, props.maxDate),
    };
  }
  renderOptions = () => {
    var selectedYear = props.year;
    var options = state.yearsList.map(year => (
      <div className={cx('react-datepicker-year-option')} key={year} ref={year} onClick={onChange.bind(this, year)}>
        {selectedYear === year ? <span className={cx('react-datepicker-year-option--selected')}>✓</span> : ''}
        {year}
      </div>
    ));
    const minYear = props.minDate.year();
    const maxYear = props.maxDate.year();
    if (!maxYear || !state.yearsList.find(year => year === maxYear)) {
      options.unshift(
        <div className={cx('react-datepicker-year-option')} ref={'upcoming'} key={'upcoming'} onClick={incrementYears}>
          <a className={cx(['react-datepicker-navigation', 'react-datepicker-navigation--years', 'react-datepicker-navigation--years-upcoming'])} />
        </div>,
      );
    }
    if (!minYear || !state.yearsList.find(year => year === minYear)) {
      options.push(
        <div className={cx('react-datepicker-year-option')} ref={'previous'} key={'previous'} onClick={decrementYears}>
          <a className={cx(['react-datepicker-navigation', 'react-datepicker-navigation--years', 'react-datepicker-navigation--years-previous'])} />
        </div>,
      );
    }
    return options;
  };
  onChange = year => {
    props.onChange(year);
  };
  handleClickOutside = () => {
    props.onCancel();
  };
  shiftYears = amount => {
    var years = state.yearsList.map(function (year) {
      return year + amount;
    });
    setState({
      yearsList: years,
    });
  };
  incrementYears = () => {
    return shiftYears(1);
  };
  decrementYears = () => {
    return shiftYears(-1);
  };
  render() {
    const dropdownClass = cx({
      'react-datepicker-year-dropdown': true,
      'react-datepicker-year-dropdown--scrollable': props.scrollableYearDropdown,
    });
    return <div className={dropdownClass}>{renderOptions()}</div>;
  }
}
