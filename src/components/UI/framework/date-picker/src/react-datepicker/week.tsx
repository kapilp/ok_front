import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import Day from "./day";
import WeekNumber from "./week_number";
import * as utils from "./date_utils";
import styles from "./stylesheets/react_datepicker.module.scss";
const cx = classNames.bind(styles);
interface IWeekProps extends JSX.HTMLAttributes<Element> {
  day: any;
  dayClassName?: (...args: any[]) => any;
  endDate?: any;
  excludeDates?: any[];
  filterDate?: (...args: any[]) => any;
  formatWeekNumber?: (...args: any[]) => any;
  highlightDates?: any;
  includeDates?: any[];
  inline?: boolean;
  maxDate?: any;
  minDate?: any;
  month?: number;
  onDayClick?: (...args: any[]) => any;
  onDayMouseEnter?: (...args: any[]) => any;
  onWeekSelect?: (...args: any[]) => any;
  preSelection?: any;
  selected?: any;
  selectingDate?: any;
  selectsEnd?: boolean;
  selectsStart?: boolean;
  showWeekNumber?: boolean;
  startDate?: any;
  utcOffset?: number;
  isCalendarKeyboardFocused?: boolean;
}
export const Week: Component = (props:IWeekProps) => {
  handleDayClick = (day, event) => {
    if (props.onDayClick) {
      props.onDayClick(day, event);
    }
  };
  handleDayMouseEnter = day => {
    if (props.onDayMouseEnter) {
      props.onDayMouseEnter(day);
    }
  };
  handleWeekClick = (day, weekNumber, event) => {
    if (typeof props.onWeekSelect === "function") {
      props.onWeekSelect(day, weekNumber, event);
    }
  };
  formatWeekNumber = startOfWeek => {
    if (props.formatWeekNumber) {
      return props.formatWeekNumber(startOfWeek);
    }
    return utils.getWeek(startOfWeek);
  };
  renderDays = () => {
    const startOfWeek = utils.getStartOfWeek(utils.cloneDate(props.day));
    const days = [];
    const weekNumber = formatWeekNumber(startOfWeek);
    if (props.showWeekNumber) {
      const onClickAction = props.onWeekSelect
        ? handleWeekClick.bind(this, startOfWeek, weekNumber)
        : undefined;
      days.push(
        <WeekNumber key="W" weekNumber={weekNumber} onClick={onClickAction} />
      );
    }
    return days.concat(
      [0, 1, 2, 3, 4, 5, 6].map(offset => {
        const day = utils.addDays(utils.cloneDate(startOfWeek), offset);
        return (
          <Day
            isCalendarKeyboardFocused={props.isCalendarKeyboardFocused}
            key={offset}
            day={day}
            month={props.month}
            onClick={handleDayClick.bind(this, day)}
            onMouseEnter={handleDayMouseEnter.bind(this, day)}
            minDate={props.minDate}
            maxDate={props.maxDate}
            excludeDates={props.excludeDates}
            includeDates={props.includeDates}
            inline={props.inline}
            highlightDates={props.highlightDates}
            selectingDate={props.selectingDate}
            filterDate={props.filterDate}
            preSelection={props.preSelection}
            selected={props.selected}
            selectsStart={props.selectsStart}
            selectsEnd={props.selectsEnd}
            startDate={props.startDate}
            endDate={props.endDate}
            dayClassName={props.dayClassName}
            utcOffset={props.utcOffset}
          />
        );
      })
    );
  };
  render() {
    return (
      <div className={cx("react-datepicker-week")}>{renderDays()}</div>
    );
  }
}
