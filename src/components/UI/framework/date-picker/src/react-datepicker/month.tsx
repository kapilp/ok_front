import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { FormattedMessage, intlShape } from "react-intl";
import classNames from "classnames/bind";
import Week from "./week";
import * as utils from "./date_utils";
import styles from "./stylesheets/react_datepicker.module.scss";
const cx = classNames.bind(styles);
const FIXED_HEIGHT_STANDARD_WEEK_COUNT = 6;
interface IMonthProps extends JSX.HTMLAttributes<Element> {
  day: any;
  dayClassName?: (...args: any[]) => any;
  endDate?: any;
  excludeDates?: any[];
  filterDate?: (...args: any[]) => any;
  fixedHeight?: boolean;
  formatWeekNumber?: (...args: any[]) => any;
  highlightDates?: any;
  includeDates?: any[];
  inline?: boolean;
  intl?: any;
  locale?: string;
  maxDate?: any;
  minDate?: any;
  onDayClick?: (...args: any[]) => any;
  onDayMouseEnter?: (...args: any[]) => any;
  onMouseLeave?: (...args: any[]) => any;
  onWeekSelect?: (...args: any[]) => any;
  peekNextMonth?: boolean;
  preSelection?: any;
  ref?: (...args: any[]) => any;
  selected?: any;
  selectingDate?: any;
  selectsEnd?: boolean;
  selectsStart?: boolean;
  showWeekNumbers?: boolean;
  startDate?: any;
  utcOffset?: number;
  isCalendarKeyboardFocused?: boolean;
  handleCalendarKeyDown?: any;
  onMonthBlur?: any;
}
export const Month: Component = (props:IMonthProps) => {
  handleDayClick = (day, event) => {
    if (props.onDayClick) {
      props.onDayClick(day, event);
    }
  };
  handleDayMouseEnter = day => {
    if (props.onDayMouseEnter) {
      props.onDayMouseEnter(day);
    }
  };
  handleMouseLeave = () => {
    if (props.onMouseLeave) {
      props.onMouseLeave();
    }
  };
  handleMonthBlur = () => {
    if (props.onMonthBlur) {
      props.onMonthBlur();
    }
  };
  isWeekInMonth = startOfWeek => {
    const day = props.day;
    const endOfWeek = utils.addDays(utils.cloneDate(startOfWeek), 6);
    return (
      utils.isSameMonth(startOfWeek, day) || utils.isSameMonth(endOfWeek, day)
    );
  };
  renderWeeks = () => {
    const weeks = [];
    var isFixedHeight = props.fixedHeight;
    let currentWeekStart = utils.getStartOfWeek(
      utils.getStartOfMonth(utils.cloneDate(props.day))
    );
    let i = 0;
    let breakAfterNextPush = false;
    while (true) {
      weeks.push(
        <Week
          isCalendarKeyboardFocused={props.isCalendarKeyboardFocused}
          key={i}
          day={currentWeekStart}
          month={utils.getMonth(props.day)}
          onDayClick={handleDayClick}
          onDayMouseEnter={handleDayMouseEnter}
          onWeekSelect={props.onWeekSelect}
          formatWeekNumber={props.formatWeekNumber}
          minDate={props.minDate}
          maxDate={props.maxDate}
          excludeDates={props.excludeDates}
          includeDates={props.includeDates}
          inline={props.inline}
          highlightDates={props.highlightDates}
          selectingDate={props.selectingDate}
          filterDate={props.filterDate}
          preSelection={props.preSelection}
          selected={props.selected}
          selectsStart={props.selectsStart}
          selectsEnd={props.selectsEnd}
          showWeekNumber={props.showWeekNumbers}
          startDate={props.startDate}
          endDate={props.endDate}
          dayClassName={props.dayClassName}
          utcOffset={props.utcOffset}
        />
      );
      if (breakAfterNextPush) break;
      i++;
      currentWeekStart = utils.addWeeks(utils.cloneDate(currentWeekStart), 1);
      // If one of these conditions is true, we will either break on this week
      // or break on the next week
      const isFixedAndFinalWeek =
        isFixedHeight && i >= FIXED_HEIGHT_STANDARD_WEEK_COUNT;
      const isNonFixedAndOutOfMonth =
        !isFixedHeight && !isWeekInMonth(currentWeekStart);
      if (isFixedAndFinalWeek || isNonFixedAndOutOfMonth) {
        if (props.peekNextMonth) {
          breakAfterNextPush = true;
        } else {
          break;
        }
      }
    }
    return weeks;
  };
  render() {
    const getClassNames = cx({
      "react-datepicker-month": true,
      "react-datepicker-body": true,
      "react-datepicker-month--selecting-range":
        props.selectingDate &&
        (props.selectsStart || props.selectsEnd)
    });
    return (
      <FormattedMessage id="Terra.datePicker.calendarInstructions">
        {text => (
          <div
            tabIndex="0"
            className={getClassNames}
            onMouseLeave={handleMouseLeave}
            role="application"
            onBlur={handleMonthBlur}
            aria-label={`${utils.getLocalizedDateForScreenReader(
              props.preSelection,
              { intl: props.intl, locale: props.locale }
            )}. ${text}`}
            onKeyDown={props.handleCalendarKeyDown}
            ref={props.ref}
          >
            {renderWeeks()}
          </div>
        )}
      </FormattedMessage>
    );
  }
}
