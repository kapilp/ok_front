import { FormattedMessage, intlShape } from 'react-intl';
import * as KeyCode from 'keycode-js';
import YearDropdown from './year_dropdown';
import MonthDropdown from './month_dropdown';
import Month from './month';
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './stylesheets/react_datepicker.module.scss';
import {
  now,
  setMonth,
  getMonth,
  addMonths,
  subtractMonths,
  getStartOfWeek,
  getStartOfDate,
  addDays,
  cloneDate,
  formatDate,
  localizeDate,
  setYear,
  getYear,
  isBefore,
  isAfter,
  getLocaleData,
  getWeekdayShortInLocale,
  getWeekdayMinInLocale,
  getStartOfMonth,
  isSameDay,
  allDaysDisabledBefore,
  allDaysDisabledAfter,
  getEffectiveMinDate,
  getEffectiveMaxDate,
  isDayDisabled,
} from './date_utils';
const cx = classNames.bind(styles);
const DROPDOWN_FOCUS_CLASSNAMES = ['react-datepicker-year-select', 'react-datepicker-month-select'];
const isDropdownSelect = (element = {}) => {
  const classNamesList = (element.className || '').split(/\s+/);
  return DROPDOWN_FOCUS_CLASSNAMES.some(testClassname => classNamesList.indexOf(testClassname) >= 0);
};
interface ICalendarProps extends JSX.HTMLAttributes<Element> {
  adjustDateOnChange?: boolean;
  className?: string;
  dateFormat: string | any[];
  dayClassName?: (...args: any[]) => any;
  dropdownMode: 'scroll' | 'select';
  endDate?: any;
  excludeDates?: any[];
  filterDate?: (...args: any[]) => any;
  fixedHeight?: boolean;
  formatWeekNumber?: (...args: any[]) => any;
  highlightDates?: any;
  includeDates?: any[];
  inline?: boolean;
  intl?: any;
  locale?: string;
  maxDate?: any;
  minDate?: any;
  monthsShown?: number;
  onClickOutside?: (...args: any[]) => any;
  onMonthChange?: (...args: any[]) => any;
  forceShowMonthNavigation?: boolean;
  onDropdownFocus?: (...args: any[]) => any;
  onSelect: (...args: any[]) => any;
  onWeekSelect?: (...args: any[]) => any;
  openToDate?: any;
  peekNextMonth?: boolean;
  scrollableYearDropdown?: boolean;
  preSelection?: any;
  setPreSelection?: (...args: any[]) => any;
  selected?: any;
  selectsEnd?: boolean;
  selectsStart?: boolean;
  showMonthDropdown?: boolean;
  showWeekNumbers?: boolean;
  showYearDropdown?: boolean;
  startDate?: any;
  todayButton?: string;
  useWeekdaysShort?: boolean;
  utcOffset?: number;
  weekLabel?: string;
  yearDropdownItemNumber?: number;
  setOpen?: (...args: any[]) => any;
  isCalendarKeyboardFocused?: boolean;
  isCalendarOpenedViaKeyboard?: boolean;
  onRequestClose?: any;
}
type CalendarState = {
  isMonthChanged?: boolean;
  date?: any;
  selectingDate?: null;
  calendarIsKeyboardFocused?: any;
};
export default class Calendar extends React.Component<ICalendarProps, CalendarState> {
  closeBtnRef: any;
  monthDropdownRef: any;
  monthRef: any;
  nextMonthBtnRef: any;
  previousMonthBtnRef: any;
  todayBtnRef: any;
  yearDropdownRef: any;
  static get defaultProps() {
    return {
      onDropdownFocus: () => {},
      monthsShown: 1,
      forceShowMonthNavigation: false,
      isCalendarKeyboardFocused: false,
    };
  }
  constructor(props) {
    super(props);
    state = {
      isMonthChanged: false,
      date: localizeDate(getDateInView()),
      selectingDate: null,
      calendarIsKeyboardFocused: props.isCalendarKeyboardFocused,
    };
    todayBtnRef = React.createRef();
    closeBtnRef = React.createRef();
    previousMonthBtnRef = React.createRef();
    nextMonthBtnRef = React.createRef();
    monthRef;
    monthDropdownRef;
    yearDropdownRef;
    handleDropdownClick = handleDropdownClick.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (props.preSelection && !isSameDay(props.preSelection, prevProps.preSelection)) {
      setState({
        date: localizeDate(props.preSelection),
      });
    } else if (props.openToDate && !isSameDay(props.openToDate, prevProps.openToDate)) {
      setState({
        date: localizeDate(props.openToDate),
      });
    }
  }
  handleDropdownClick(event) {
    if (event.keyCode === KeyCode.KEY_UP || event.keyCode === KeyCode.KEY_DOWN) {
      setState({ calendarIsKeyboardFocused: true });
    } else {
      setState({ calendarIsKeyboardFocused: false });
    }
  }
  handleOnClick = event => {
    const calendarControls = [todayBtnRef.current, closeBtnRef.current, previousMonthBtnRef.current, nextMonthBtnRef.current, monthDropdownRef, yearDropdownRef];
    const isEventTargetMatchingCalendarControl = target => {
      return calendarControls.indexOf(target) >= 0;
    };
    const isEventTargetContainedWithinCalendarControl = target => {
      const containsEventTarget =
        (previousMonthBtnRef.current && previousMonthBtnRef.current.contains(target)) ||
        (nextMonthBtnRef.current && nextMonthBtnRef.current.contains(target)) ||
        (monthDropdownRef && monthDropdownRef.contains(target)) ||
        (yearDropdownRef && yearDropdownRef.contains(target));
      return containsEventTarget;
    };
    if (isEventTargetMatchingCalendarControl(event.target) || isEventTargetContainedWithinCalendarControl(event.target)) {
      return;
    }
    // If the user is not clicking on a calendar control, shift focus to the calendar
    if (monthRef) {
      monthRef.focus();
    }
  };
  handleClickOutside = event => {
    props.onClickOutside(event);
  };
  handleDropdownFocus = event => {
    if (isDropdownSelect(event.target)) {
      props.onDropdownFocus();
    }
  };
  handleCloseButtonClick = event => {
    if (props.onRequestClose) {
      props.onRequestClose(event);
    }
  };
  handlePreviousMonthBtnKeyDown = event => {
    if (event.shiftKey && event.keyCode === KeyCode.KEY_TAB) {
      setState({ calendarIsKeyboardFocused: true });
    }
  };
  handleNextMonthBtnKeyDown = event => {
    if (event.keyCode === KeyCode.KEY_RETURN) {
      setState({ calendarIsKeyboardFocused: true });
    }
  };
  handleTodayBtnKeyDown = event => {
    if (event.keyCode === KeyCode.KEY_TAB) {
      setState({ calendarIsKeyboardFocused: true });
    }
  };
  handleMonthBlur = () => {
    setState({ calendarIsKeyboardFocused: false });
  };
  setMonthRef = node => {
    monthRef = node;
  };
  setMonthDropdownRef = node => {
    monthDropdownRef = node;
  };
  setYearDropdownRef = node => {
    yearDropdownRef = node;
  };
  getDateInView = () => {
    const { preSelection, selected, openToDate, utcOffset } = props;
    const minDate = getEffectiveMinDate(props);
    const maxDate = getEffectiveMaxDate(props);
    const current = now(utcOffset);
    const initialDate = openToDate || selected || preSelection;
    if (initialDate) {
      return initialDate;
    } else {
      if (minDate && isBefore(current, minDate)) {
        return minDate;
      } else if (maxDate && isAfter(current, maxDate)) {
        return maxDate;
      }
    }
    return current;
  };
  localizeDate = date => localizeDate(date, props.locale);
  increaseMonth = event => {
    nextMonthBtnRef.current.focus(); // To apply focus style in firefox
    setState(
      {
        isMonthChanged: true,
        date: getStartOfMonth(addMonths(cloneDate(state.date), 1)),
      },
      () => handleMonthChange(state.date),
    );
    props.setPreSelection(getStartOfMonth(addMonths(cloneDate(state.date), 1)));
    // To check if button is pressed using mouse or keyboard
    if (event.target.type === undefined) {
      setState({ calendarIsKeyboardFocused: false });
    }
  };
  decreaseMonth = event => {
    previousMonthBtnRef.current.focus(); // To apply focus style in firefox
    setState(
      {
        isMonthChanged: true,
        date: getStartOfMonth(subtractMonths(cloneDate(state.date), 1)),
      },
      () => handleMonthChange(state.date),
    );
    props.setPreSelection(getStartOfMonth(subtractMonths(cloneDate(state.date), 1)));
    // To check if button is pressed using mouse or keyboard
    if (event.target.type === undefined) {
      setState({ calendarIsKeyboardFocused: false });
    }
  };
  handleDayClick = (day, event) => props.onSelect(day, event);
  handleDayMouseEnter = day => setState({ selectingDate: day });
  handleMonthMouseLeave = () => setState({ selectingDate: null });
  handleMonthChange = date => {
    if (props.onMonthChange) {
      props.onMonthChange(date);
    }
    if (props.adjustDateOnChange) {
      if (props.onSelect) {
        props.onSelect(date);
      }
      if (props.setOpen) {
        props.setOpen(true);
      }
    }
  };
  changeYear = year => {
    setState({
      isMonthChanged: true,
      date: getStartOfMonth(setYear(cloneDate(state.date), year)),
    });
    props.setPreSelection(getStartOfMonth(setYear(cloneDate(state.date), year)));
  };
  changeMonth = month => {
    setState(
      {
        isMonthChanged: true,
        date: getStartOfMonth(setMonth(cloneDate(state.date), month)),
      },
      () => handleMonthChange(state.date),
    );
    props.setPreSelection(getStartOfMonth(setMonth(cloneDate(state.date), month)));
  };
  header = (date = state.date) => {
    const startOfWeek = getStartOfWeek(cloneDate(date));
    const dayNames = [];
    if (props.showWeekNumbers) {
      dayNames.push(
        <div key="W" className={cx('react-datepicker-day-name')}>
          {props.weekLabel || '#'}
        </div>,
      );
    }
    return dayNames.concat(
      [0, 1, 2, 3, 4, 5, 6].map(offset => {
        const day = addDays(cloneDate(startOfWeek), offset);
        const localeData = getLocaleData(day);
        const weekDayName = props.useWeekdaysShort ? getWeekdayShortInLocale(localeData, day) : getWeekdayMinInLocale(localeData, day);
        return (
          <div key={offset} className={cx('react-datepicker-day-name')}>
            {weekDayName}
          </div>
        );
      }),
    );
  };
  renderPreviousMonthButton = () => {
    if (!props.forceShowMonthNavigation && allDaysDisabledBefore(state.date, 'month', props)) {
      return <div></div>;
    }
    return (
      <FormattedMessage id="Terra.datePicker.previousMonth">
        {text => (
          <button
            type="button"
            className={cx('react-datepicker-navigation--previous')}
            aria-label={text}
            onClick={decreaseMonth}
            onKeyDown={handlePreviousMonthBtnKeyDown}
            ref={previousMonthBtnRef}
          >
            <span className={cx('prev-month-icon')} />
          </button>
        )}
      </FormattedMessage>
    );
  };
  renderNextMonthButton = () => {
    if (!props.forceShowMonthNavigation && allDaysDisabledAfter(state.date, 'month', props)) {
      return <div></div>;
    }
    return (
      <FormattedMessage id="Terra.datePicker.nextMonth">
        {text => (
          <button
            type="button"
            className={cx('react-datepicker-navigation--next')}
            aria-label={text}
            onClick={increaseMonth}
            onKeyDown={handleNextMonthBtnKeyDown}
            ref={nextMonthBtnRef}
          >
            <span className={cx('next-month-icon')} />
          </button>
        )}
      </FormattedMessage>
    );
  };
  renderCurrentMonth = (date = state.date) => {
    const classes = ['react-datepicker-current-month'];
    if (props.showYearDropdown) {
      classes.push('react-datepicker-current-month--hasYearDropdown');
    }
    if (props.showMonthDropdown) {
      classes.push('react-datepicker-current-month--hasMonthDropdown');
    }
    return <div className={cx(classes)}>{formatDate(date, props.dateFormat)}</div>;
  };
  renderYearDropdown = (overrideHide = false) => {
    if (!props.showYearDropdown || overrideHide) {
      return;
    }
    return (
      <YearDropdown
        adjustDateOnChange={props.adjustDateOnChange}
        date={state.date}
        onSelect={props.onSelect}
        setOpen={props.setOpen}
        dropdownMode={props.dropdownMode}
        onChange={changeYear}
        minDate={props.minDate}
        maxDate={props.maxDate}
        ref={setYearDropdownRef}
        year={getYear(state.date)}
        scrollableYearDropdown={props.scrollableYearDropdown}
        yearDropdownItemNumber={props.yearDropdownItemNumber}
        onClick={handleDropdownClick}
        onKeyDown={handleDropdownClick}
      />
    );
  };
  renderMonthDropdown = (overrideHide = false) => {
    if (!props.showMonthDropdown) {
      return;
    }
    return (
      <MonthDropdown
        dropdownMode={props.dropdownMode}
        locale={props.locale}
        dateFormat={props.dateFormat}
        onChange={changeMonth}
        month={getMonth(state.date)}
        ref={setMonthDropdownRef}
        onClick={handleDropdownClick}
        onKeyDown={handleDropdownClick}
      />
    );
  };
  renderTodayButton = () => {
    if (!props.todayButton) {
      return;
    }
    const today = getStartOfDate(now(props.utcOffset));
    return (
      <button
        className={cx('react-datepicker-today-button')}
        onClick={e => props.onSelect(today, e)}
        onKeyDown={handleTodayBtnKeyDown}
        ref={todayBtnRef}
        disabled={isDayDisabled(today, props)}
      >
        {props.todayButton}
      </button>
    );
  };
  renderCloseButton = () => {
    return (
      <FormattedMessage id="Terra.datePicker.closeCalendar">
        {text => (
          <button className={cx('react-datepicker-close-button')} type="button" onClick={handleCloseButtonClick} ref={closeBtnRef}>
            {text}
          </button>
        )}
      </FormattedMessage>
    );
  };
  renderMonths = () => {
    let keyboardFocus = false;
    if (props.isCalendarOpenedViaKeyboard || props.isCalendarKeyboardFocused) {
      keyboardFocus = true;
    }
    if (state.isMonthChanged) {
      keyboardFocus = state.calendarIsKeyboardFocused;
    }
    if (!keyboardFocus && state.calendarIsKeyboardFocused) {
      keyboardFocus = true;
    }
    var monthList = [];
    for (var i = 0; i < props.monthsShown; ++i) {
      var monthDate = addMonths(cloneDate(state.date), i);
      var monthKey = `month-${i}`;
      monthList.push(
        <div key={monthKey} onClick={handleOnClick} className={cx('react-datepicker-month-container')}>
          <Month
            day={monthDate}
            isCalendarKeyboardFocused={keyboardFocus}
            dayClassName={props.dayClassName}
            onMonthBlur={handleMonthBlur}
            onDayClick={handleDayClick}
            onDayMouseEnter={handleDayMouseEnter}
            onMouseLeave={handleMonthMouseLeave}
            onWeekSelect={props.onWeekSelect}
            formatWeekNumber={props.formatWeekNumber}
            minDate={props.minDate}
            maxDate={props.maxDate}
            excludeDates={props.excludeDates}
            highlightDates={props.highlightDates}
            selectingDate={state.selectingDate}
            includeDates={props.includeDates}
            inline={props.inline}
            fixedHeight={props.fixedHeight}
            filterDate={props.filterDate}
            preSelection={props.preSelection}
            ref={setMonthRef}
            selected={props.selected}
            selectsStart={props.selectsStart}
            selectsEnd={props.selectsEnd}
            showWeekNumbers={props.showWeekNumbers}
            startDate={props.startDate}
            endDate={props.endDate}
            peekNextMonth={props.peekNextMonth}
            utcOffset={props.utcOffset}
            handleCalendarKeyDown={props.handleCalendarKeyDown}
            locale={props.locale}
            intl={props.intl}
          />
          <div className={cx('react-datepicker-header')}>
            {renderCurrentMonth(monthDate)}
            <div className={cx('react-datepicker-header-controls')}>
              {renderPreviousMonthButton()}
              <div className={cx(['react-datepicker-header-dropdown', `react-datepicker-header-dropdown--${props.dropdownMode}`])} onFocus={handleDropdownFocus}>
                {renderMonthDropdown(i !== 0)}
                {renderYearDropdown(i !== 0)}
              </div>
              {renderNextMonthButton()}
            </div>
            <div className={cx('react-datepicker-day-names')} aria-hidden="true">
              {header(monthDate)}
            </div>
          </div>
        </div>,
      );
    }
    return monthList;
  };
  render() {
    const supportsOnTouchStart = 'ontouchstart' in window;
    /**
     * Ensures focus moves into datepicker popup correctly when it is opened on touch devices
     * by making focusable element (today button) first in the DOM order
     */
    if (supportsOnTouchStart) {
      return (
        <div className={cx(['react-datepicker', 'supports-on-touch-start', props.className])} data-terra-date-picker-calendar>
          <div className={cx('react-datepicker-footer')}>
            {renderTodayButton()}
            {renderCloseButton()}
          </div>
          {renderMonths()}
          {props.children}
        </div>
      );
    }
    /**
     * Ensures users can start interacting with the calendar via up/down/left/right arrow keys
     * when it first opens by making the month component render first in the DOM order
     */
    return (
      <div className={cx(['react-datepicker', props.className])} data-terra-date-picker-calendar>
        {renderMonths()}
        <div className={cx('react-datepicker-footer')}>{renderTodayButton()}</div>
        {props.children}
      </div>
    );
  }
}
