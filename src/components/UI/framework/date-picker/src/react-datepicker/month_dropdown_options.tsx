import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './stylesheets/react_datepicker.module.scss';
const cx = classNames.bind(styles);
interface IMonthDropdownOptionsProps extends JSX.HTMLAttributes<Element> {
  onCancel: (...args: any[]) => any;
  onChange: (...args: any[]) => any;
  month: number;
  monthNames: string[];
  map?: any;
}
export default class MonthDropdownOptions extends React.Component<IMonthDropdownOptionsProps, {}> {
  renderOptions = () => {
    return props.monthNames.map((month, i) => (
      <div className={cx('react-datepicker-month-option')} key={month} ref={month} onClick={onChange.bind(this, i)}>
        {props.month === i ? <span className={cx('react-datepicker-month-option--selected')}>✓</span> : ''}
        {month}
      </div>
    ));
  };
  onChange = month => props.onChange(month);
  handleClickOutside = () => props.onCancel();
  render() {
    return <div className={cx('react-datepicker-month-dropdown')}>{renderOptions()}</div>;
  }
}
