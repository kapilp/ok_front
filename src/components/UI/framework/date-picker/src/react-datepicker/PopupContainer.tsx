import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
interface Properties {
  /**
   * Prop to determine whether or not the container height is bounded.
   */
  isHeightBounded: boolean;
  /**
   * Prop to determine whether or not the container width is bounded.
   */
  isWidthBounded: boolean;
  /**
   * A callback function to execute when a key is pressed.
   */
  onKeyDown: PropTypes.func;
  /**
   * Components to be included in the popup container.
   */
  children: JSX.Element;
}
const defaultProps = {
  isHeightBounded: false,
  isWidthBounded: false,
};
const PopupContent: Component = React.forwardRef((props, ref) => {
  return (
    <div ref={ref} onKeyDown={props.onKeyDown}>
      {props.children}
    </div>
  );
});
props = mergeProps({}, defaultProps, props);
export default PopupContent;
