import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { FormattedMessage } from 'react-intl';
import MonthDropdownOptions from './month_dropdown_options';
import onClickOutside from 'react-onclickoutside';
import * as utils from './date_utils';
import styles from './stylesheets/react_datepicker.module.scss';
const cx = classNames.bind(styles);
const WrappedMonthDropdownOptions = onClickOutside(MonthDropdownOptions);
interface IMonthDropdownProps extends JSX.HTMLAttributes<Element> {
  dropdownMode: 'scroll' | 'select';
  locale?: string;
  dateFormat: string;
  month: number;
  onChange: (...args: any[]) => any;
  ref?: (...args: any[]) => any;
  onClick?: (...args: any[]) => any;
  onKeyDown?: (...args: any[]) => any;
}
type MonthDropdownState = {
  dropdownVisible?: boolean;
};
export default class MonthDropdown extends React.Component<IMonthDropdownProps, MonthDropdownState> {
  state = {
    dropdownVisible: false,
  };
  renderSelectOptions = monthNames =>
    monthNames.map((M, i) => (
      <option key={i} value={i}>
        {M}
      </option>
    ));
  renderSelectMode = monthNames => (
    <FormattedMessage id="Terra.datePicker.monthLabel">
      {label => (
        <select
          onKeyDown={props.onKeyDown}
          onClick={props.onClick}
          aria-label={label}
          value={props.month}
          className={cx('react-datepicker-month-select')}
          onChange={e => onChange(e.target.value)}
        >
          {renderSelectOptions(monthNames)}
        </select>
      )}
    </FormattedMessage>
  );
  renderReadView = (visible, monthNames) => (
    <div key="read" style={{ visibility: visible ? 'visible' : 'hidden' }} className={cx('react-datepicker-month-read-view')} onClick={toggleDropdown}>
      <span className={cx('react-datepicker-month-read-view--down-arrow')} />
      <span className={cx('react-datepicker-month-read-view--selected-month')}>{monthNames[props.month]}</span>
    </div>
  );
  renderDropdown = monthNames => (
    <WrappedMonthDropdownOptions key="dropdown" ref="options" month={props.month} monthNames={monthNames} onChange={onChange} onCancel={toggleDropdown} />
  );
  renderScrollMode = monthNames => {
    const { dropdownVisible } = state;
    let result = [renderReadView(!dropdownVisible, monthNames)];
    if (dropdownVisible) {
      result.unshift(renderDropdown(monthNames));
    }
    return result;
  };
  onChange = month => {
    toggleDropdown();
    if (month !== props.month) {
      props.onChange(month);
    }
  };
  toggleDropdown = () =>
    setState({
      dropdownVisible: !state.dropdownVisible,
    });
  render() {
    const localeData = utils.getLocaleDataForLocale(props.locale);
    const monthNames = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map(M => utils.getMonthInLocale(localeData, utils.newDate({ M }), props.dateFormat));
    let renderedDropdown;
    switch (props.dropdownMode) {
      case 'scroll':
        renderedDropdown = renderScrollMode(monthNames);
        break;
      case 'select':
        renderedDropdown = renderSelectMode(monthNames);
        break;
    }
    return (
      <div ref={props.ref} className={cx(['react-datepicker-month-dropdown-container', `react-datepicker-month-dropdown-container--${props.dropdownMode}`])}>
        {renderedDropdown}
      </div>
    );
  }
}
