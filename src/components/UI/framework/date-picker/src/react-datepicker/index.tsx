import Calendar from './calendar';
import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import FocusTrap from 'focus-trap-react';
import { Portal } from 'react-portal';
import * as KeyCode from 'keycode-js';
import Popup from 'terra-popup';
import classNames from 'classnames/bind';
import { injectIntl, intlShape } from 'react-intl';
import VisuallyHiddenText from 'terra-visually-hidden-text';
import PopupContainer from './PopupContainer';
import DateUtil from '../DateUtil';
import {
  newDate,
  now,
  isMoment,
  isDate,
  isBefore,
  isAfter,
  getMonth,
  addDays,
  addMonths,
  addWeeks,
  addYears,
  subtractDays,
  subtractMonths,
  subtractWeeks,
  subtractYears,
  isSameDay,
  isDayDisabled,
  isDayInRange,
  getEffectiveMinDate,
  getEffectiveMaxDate,
  parseDate,
  safeDateFormat,
  getHightLightDaysMap,
  getLocalizedDateForScreenReader,
} from './date_utils';
import onClickOutside from 'react-onclickoutside';
import styles from './stylesheets/react_datepicker.module.scss';
const cx = classNames.bind(styles);
const outsideClickIgnoreClass = 'react-datepicker-ignore-onclickoutside';
const WrappedCalendar = onClickOutside(Calendar);
interface IDatePickerProps extends JSX.HTMLAttributes<Element> {
  adjustDateOnChange?: boolean;
  allowSameDay?: boolean;
  autoComplete?: string;
  autoFocus?: boolean;
  calendarClassName?: string;
  className?: string;
  customInput?: JSX.Element;
  customInputRef?: string;
  dateFormat?: string | any[];
  dateFormatCalendar?: string;
  dayClassName?: (...args: any[]) => any;
  disabled?: boolean;
  disabledKeyboardNavigation?: boolean;
  dropdownMode: 'scroll' | 'select';
  endDate?: any;
  excludeDates?: any[];
  filterDate?: (...args: any[]) => any;
  fixedHeight?: boolean;
  formatWeekNumber?: (...args: any[]) => any;
  highlightDates?: any[];
  id?: string;
  includeDates?: any[];
  inline?: boolean;
  intl?: any;
  isClearable?: boolean;
  locale?: string;
  maxDate?: any;
  minDate?: any;
  monthsShown?: number;
  name?: string;
  onBlur?: (...args: any[]) => any;
  onChange: (...args: any[]) => any;
  onSelect?: (...args: any[]) => any;
  onWeekSelect?: (...args: any[]) => any;
  onClickOutside?: (...args: any[]) => any;
  onChangeRaw?: (...args: any[]) => any;
  onFocus?: (...args: any[]) => any;
  onKeyDown?: (...args: any[]) => any;
  onMonthChange?: (...args: any[]) => any;
  openToDate?: any;
  peekNextMonth?: boolean;
  placeholderText?: string;
  preventOpenOnFocus?: boolean;
  readOnly?: boolean;
  required?: boolean;
  scrollableYearDropdown?: boolean;
  selected?: any;
  selectsEnd?: boolean;
  selectsStart?: boolean;
  showMonthDropdown?: boolean;
  showWeekNumbers?: boolean;
  showYearDropdown?: boolean;
  forceShowMonthNavigation?: boolean;
  startDate?: any;
  startOpen?: boolean;
  tabIndex?: number;
  title?: string;
  todayButton?: string;
  useWeekdaysShort?: boolean;
  utcOffset?: number;
  value?: string;
  weekLabel?: string;
  withPortal?: boolean;
  yearDropdownItemNumber?: number;
  shouldCloseOnSelect?: boolean;
}
type DatePickerState = {
  highlightDates?: any;
  isCalendarKeyboardFocused?: boolean;
  isCalendarOpenedViaKeyboard?: boolean;
  open?: any;
  preSelection?: any;
  inputValue?: any;
  preventFocus?: boolean;
};
/**
 * General datepicker component.
 */
class DatePicker extends React.Component<IDatePickerProps, DatePickerState> {
  calendar: any;
  datePickerContainer: any;
  datePickerOverlayContainer: any;
  datePickerPopupContainer: any;
  handleOnPosition: any;
  input: any;
  inputFocusTimeout: Timeout;
  preventFocusTimeout: any;
  visuallyHiddenText: any;
  static get defaultProps() {
    return {
      allowSameDay: false,
      dateFormat: 'L',
      dateFormatCalendar: 'MMMM YYYY',
      onChange() {},
      disabled: false,
      disabledKeyboardNavigation: false,
      dropdownMode: 'scroll',
      maxDate: newDate(DateUtil.MAX_DATE),
      minDate: newDate(DateUtil.MIN_DATE),
      onFocus() {},
      onBlur() {},
      onKeyDown() {},
      onSelect() {},
      onClickOutside() {},
      onMonthChange() {},
      preventOpenOnFocus: false,
      monthsShown: 1,
      withPortal: false,
      shouldCloseOnSelect: true,
    };
  }
  constructor(props) {
    super(props);
    state = calcInitialState();
    handleKeydown = handleKeydown.bind(this);
    datePickerContainer = React.createRef();
    datePickerPopupContainer = React.createRef();
    datePickerOverlayContainer = React.createRef();
    handleCalendarKeyDown = handleCalendarKeyDown.bind(this);
    handleOnRequestClose = handleOnRequestClose.bind(this);
    updateAriaLiveStatus = updateAriaLiveStatus.bind(this);
  }
  componentDidMount() {
    document.addEventListener('keydown', handleKeydown);
  }
  componentDidUpdate(prevProps) {
    const currentMonth = prevProps.selected && getMonth(prevProps.selected);
    const nextMonth = props.selected && getMonth(props.selected);
    if (prevProps.inline && currentMonth !== nextMonth) {
      setPreSelection(props.selected);
    }
    if (prevProps.highlightDates !== props.highlightDates) {
      setState({
        highlightDates: getHightLightDaysMap(props.highlightDates),
      });
    }
    // Shift focus into popup date-picker if it exists
    if (datePickerPopupContainer.current) {
      datePickerPopupContainer.current.focus();
    }
    // Shift focus into overlay date-picker if it exists
    if (datePickerOverlayContainer.current) {
      datePickerOverlayContainer.current.focus();
    }
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', handleKeydown);
    clearPreventFocusTimeout();
  }
  handleKeydown(event) {
    if (event.keyCode === KeyCode.KEY_ESCAPE) {
      // If date picker is open in overlay
      if (datePickerOverlayContainer.current) {
        if (event.target === datePickerOverlayContainer.current || datePickerOverlayContainer.current.contains(event.target)) {
          setOpen(false);
        }
      }
    }
  }
  handleOnRequestClose() {
    setState({
      isCalendarKeyboardFocused: false,
      isCalendarOpenedViaKeyboard: false,
    });
    setOpen(false);
  }
  getPreSelection = () =>
    props.openToDate
      ? newDate(props.openToDate)
      : props.selectsEnd && props.startDate
      ? newDate(props.startDate)
      : props.selectsStart && props.endDate
      ? newDate(props.endDate)
      : now(props.utcOffset);
  calcInitialState = () => {
    const defaultPreSelection = getPreSelection();
    const minDate = getEffectiveMinDate(props);
    const maxDate = getEffectiveMaxDate(props);
    const boundedPreSelection = minDate && isBefore(defaultPreSelection, minDate) ? minDate : maxDate && isAfter(defaultPreSelection, maxDate) ? maxDate : defaultPreSelection;
    return {
      isCalendarOpenedViaKeyboard: false,
      isCalendarKeyboardFocused: false,
      open: props.startOpen || false,
      preventFocus: false,
      preSelection: props.selected ? newDate(props.selected) : boundedPreSelection,
      // transforming highlighted days (perhaps nested array)
      // to flat Map for faster access in day.jsx
      highlightDates: getHightLightDaysMap(props.highlightDates),
    };
  };
  clearPreventFocusTimeout = () => {
    if (preventFocusTimeout) {
      clearTimeout(preventFocusTimeout);
    }
  };
  setFocus = () => {
    if (input.focus) {
      input.focus();
    }
  };
  setOpen = open => {
    setState({
      open: open,
      preSelection: open && state.open ? state.preSelection : calcInitialState().preSelection,
    });
  };
  handleFocus = event => {
    if (!state.preventFocus) {
      props.onFocus(event);
      if (!props.preventOpenOnFocus) {
        setOpen(true);
      }
    }
  };
  cancelFocusInput = () => {
    clearTimeout(inputFocusTimeout);
    inputFocusTimeout = null;
  };
  deferFocusInput = () => {
    cancelFocusInput();
    inputFocusTimeout = setTimeout(() => setFocus(), 1);
  };
  handleDropdownFocus = () => {
    cancelFocusInput();
  };
  handleBlur = event => {
    if (state.open) {
      deferFocusInput();
    } else {
      props.onBlur(event);
    }
  };
  handleCalendarClickOutside = event => {
    if (!props.inline) {
      setOpen(false);
    }
    props.onClickOutside(event);
    if (props.withPortal) {
      event.preventDefault();
    }
  };
  handleChange = event => {
    if (props.onChangeRaw) {
      props.onChangeRaw(event);
      if (event.isDefaultPrevented()) {
        return;
      }
    }
    setState({ inputValue: event.target.value });
    const date = parseDate(event.target.value, props);
    if (date || !event.target.value) {
      setSelected(date, event, true);
    }
  };
  handleSelect = (date, event) => {
    // Preventing onFocus event to fix issue
    // https://github.com/Hacker0x01/react-datepicker/issues/628
    setState({ preventFocus: true }, () => {
      preventFocusTimeout = setTimeout(() => setState({ preventFocus: false }), 50);
      return preventFocusTimeout;
    });
    setSelected(date, event);
    if (!props.shouldCloseOnSelect) {
      setPreSelection(date);
    } else if (!props.inline) {
      setOpen(false);
    }
  };
  setSelected = (date, event, keepInput) => {
    let changedDate = date;
    if (changedDate !== null && isDayDisabled(changedDate, props)) {
      return;
    }
    let hasChanged = false;
    if (!isSameDay(props.selected, changedDate) || props.allowSameDay) {
      if (changedDate !== null) {
        setState({
          preSelection: changedDate,
        });
      }
      hasChanged = true;
    }
    props.onSelect(changedDate, event);
    if (hasChanged) {
      props.onChange(changedDate, event);
    }
    if (!keepInput) {
      setState({ inputValue: null });
    }
  };
  setPreSelection = date => {
    const isValidDateSelection = date ? isDayInRange(date, props.minDate, props.maxDate) : true;
    if (isValidDateSelection) {
      setState({
        preSelection: date,
      });
      updateAriaLiveStatus(getLocalizedDateForScreenReader(date, props));
    }
  };
  updateAriaLiveStatus(message) {
    visuallyHiddenText.innerText = message;
  }
  onInputClick = () => {
    if (!props.disabled) {
      setOpen(true);
    }
  };
  onInputKeyDown = event => {
    if (event.key === 'Enter') {
      setState({ isCalendarOpenedViaKeyboard: true });
    }
  };
  handleCalendarKeyDown = event => {
    const keyboardNavKeys = ['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown', 'PageUp', 'PageDown', 'Home', 'End'];
    props.onKeyDown(event);
    const eventKey = event.key;
    const copy = newDate(state.preSelection);
    if (eventKey === 'Enter') {
      event.preventDefault();
      if (isMoment(state.preSelection) || isDate(state.preSelection)) {
        handleSelect(copy, event);
        !props.shouldCloseOnSelect && setPreSelection(copy);
      } else {
        setOpen(false);
      }
    } else if (eventKey === 'Escape') {
      event.preventDefault();
      setOpen(false);
    } else if (!props.disabledKeyboardNavigation && keyboardNavKeys.indexOf(eventKey) !== -1) {
      let newSelection;
      switch (eventKey) {
        case 'ArrowLeft':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = subtractDays(copy, 1);
          break;
        case 'ArrowRight':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = addDays(copy, 1);
          break;
        case 'ArrowUp':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = subtractWeeks(copy, 1);
          break;
        case 'ArrowDown':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = addWeeks(copy, 1);
          break;
        case 'PageUp':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = subtractMonths(copy, 1);
          break;
        case 'PageDown':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = addMonths(copy, 1);
          break;
        case 'Home':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = subtractYears(copy, 1);
          break;
        case 'End':
          setState({ isCalendarKeyboardFocused: true });
          event.preventDefault();
          newSelection = addYears(copy, 1);
          break;
      }
      if (props.adjustDateOnChange) {
        setSelected(newSelection);
      }
      setPreSelection(newSelection);
    }
  };
  onClearClick = event => {
    event.preventDefault();
    props.onChange(null, event);
    setState({ inputValue: null });
  };
  renderCalendar = () => {
    if (!props.inline && (!state.open || props.disabled)) {
      return null;
    }
    if (props.withPortal) {
      return (
        <WrappedCalendar
          ref={elem => {
            calendar = elem;
          }}
          locale={props.locale}
          adjustDateOnChange={props.adjustDateOnChange}
          setOpen={setOpen}
          dateFormat={props.dateFormatCalendar}
          useWeekdaysShort={props.useWeekdaysShort}
          dropdownMode={props.dropdownMode}
          selected={props.selected}
          preSelection={state.preSelection}
          onSelect={handleSelect}
          onWeekSelect={props.onWeekSelect}
          openToDate={props.openToDate}
          minDate={props.minDate}
          maxDate={props.maxDate}
          selectsStart={props.selectsStart}
          selectsEnd={props.selectsEnd}
          startDate={props.startDate}
          endDate={props.endDate}
          excludeDates={props.excludeDates}
          filterDate={props.filterDate}
          onClickOutside={handleCalendarClickOutside}
          formatWeekNumber={props.formatWeekNumber}
          highlightDates={state.highlightDates}
          includeDates={props.includeDates}
          inline={props.inline}
          peekNextMonth={props.peekNextMonth}
          showMonthDropdown={props.showMonthDropdown}
          showWeekNumbers={props.showWeekNumbers}
          showYearDropdown={props.showYearDropdown}
          forceShowMonthNavigation={props.forceShowMonthNavigation}
          scrollableYearDropdown={props.scrollableYearDropdown}
          todayButton={props.todayButton}
          weekLabel={props.weekLabel}
          utcOffset={props.utcOffset}
          outsideClickIgnoreClass={outsideClickIgnoreClass}
          fixedHeight={props.fixedHeight}
          monthsShown={props.monthsShown}
          onDropdownFocus={handleDropdownFocus}
          onMonthChange={props.onMonthChange}
          dayClassName={props.dayClassName}
          className={props.calendarClassName}
          yearDropdownItemNumber={props.yearDropdownItemNumber}
          onRequestClose={handleOnRequestClose}
          handleCalendarKeyDown={handleCalendarKeyDown}
          setPreSelection={setPreSelection}
          isCalendarKeyboardFocused={state.isCalendarKeyboardFocused}
          isCalendarOpenedViaKeyboard={state.isCalendarOpenedViaKeyboard}
        >
          {props.children}
          <VisuallyHiddenText
            aria-atomic="true"
            aria-live="assertive"
            ref={ref => {
              visuallyHiddenText = ref;
            }}
          />
        </WrappedCalendar>
      );
    }
    return (
      <Calendar
        ref={elem => {
          calendar = elem;
        }}
        locale={props.locale}
        adjustDateOnChange={props.adjustDateOnChange}
        setOpen={setOpen}
        dateFormat={props.dateFormatCalendar}
        useWeekdaysShort={props.useWeekdaysShort}
        dropdownMode={props.dropdownMode}
        selected={props.selected}
        preSelection={state.preSelection}
        onSelect={handleSelect}
        onWeekSelect={props.onWeekSelect}
        openToDate={props.openToDate}
        minDate={props.minDate}
        maxDate={props.maxDate}
        selectsStart={props.selectsStart}
        selectsEnd={props.selectsEnd}
        startDate={props.startDate}
        endDate={props.endDate}
        excludeDates={props.excludeDates}
        filterDate={props.filterDate}
        formatWeekNumber={props.formatWeekNumber}
        highlightDates={state.highlightDates}
        includeDates={props.includeDates}
        inline={props.inline}
        peekNextMonth={props.peekNextMonth}
        showMonthDropdown={props.showMonthDropdown}
        showWeekNumbers={props.showWeekNumbers}
        showYearDropdown={props.showYearDropdown}
        forceShowMonthNavigation={props.forceShowMonthNavigation}
        scrollableYearDropdown={props.scrollableYearDropdown}
        todayButton={props.todayButton}
        weekLabel={props.weekLabel}
        utcOffset={props.utcOffset}
        fixedHeight={props.fixedHeight}
        monthsShown={props.monthsShown}
        onDropdownFocus={handleDropdownFocus}
        onMonthChange={props.onMonthChange}
        dayClassName={props.dayClassName}
        className={props.calendarClassName}
        yearDropdownItemNumber={props.yearDropdownItemNumber}
        onRequestClose={handleOnRequestClose}
        handleCalendarKeyDown={handleCalendarKeyDown}
        setPreSelection={setPreSelection}
        isCalendarKeyboardFocused={state.isCalendarKeyboardFocused}
        isCalendarOpenedViaKeyboard={state.isCalendarOpenedViaKeyboard}
      >
        {props.children}
        <VisuallyHiddenText
          aria-atomic="true"
          aria-live="assertive"
          ref={ref => {
            visuallyHiddenText = ref;
          }}
        />
      </Calendar>
    );
  };
  renderDateInput = () => {
    var classNameList = cx(props.className, {
      [outsideClickIgnoreClass]: state.open,
    });
    const customInput = props.customInput || <input type="text" />;
    const customInputRef = props.customInputRef || 'ref';
    const inputValue = typeof props.value === 'string' ? props.value : typeof state.inputValue === 'string' ? state.inputValue : safeDateFormat(props.selected, props);
    return React.cloneElement(customInput, {
      [customInputRef]: input => {
        input = input;
      },
      value: inputValue,
      onBlur: handleBlur,
      onChange: handleChange,
      onClick: onInputClick,
      onFocus: handleFocus,
      onKeyDown: onInputKeyDown,
      id: props.id,
      name: props.name,
      autoFocus: props.autoFocus,
      placeholder: props.placeholderText,
      disabled: props.disabled,
      autoComplete: props.autoComplete,
      className: classNameList,
      title: props.title,
      readOnly: props.readOnly,
      required: props.required,
      tabIndex: props.tabIndex,
    });
  };
  renderClearButton = () => {
    if (props.isClearable && props.selected != null) {
      return <a className={cx('react-datepicker-close-icon')} href="#" onClick={onClearClick} />;
    } else {
      return null;
    }
  };
  render() {
    const calendar = renderCalendar();
    if (props.inline && !props.withPortal) {
      return calendar;
    }
    if (props.withPortal) {
      return (
        <div>
          {!props.inline ? (
            <div className={cx('react-datepicker-input-container')}>
              {renderDateInput()}
              {renderClearButton()}
            </div>
          ) : null}
          {state.open || props.inline ? (
            <Portal isOpened={true}>
              <FocusTrap
                focusTrapOptions={{
                  returnFocusOnDeactivate: false,
                  clickOutsideDeactivates: true,
                }}
              >
                <div ref={datePickerOverlayContainer} className={cx('react-datepicker-portal')}>
                  {calendar}
                </div>
              </FocusTrap>
            </Portal>
          ) : null}
        </div>
      );
    }
    return (
      <>
        <div ref={datePickerContainer} className={cx('react-datepicker-input-container')}>
          {renderDateInput()}
          {renderClearButton()}
        </div>
        {calendar && (
          <Popup
            attachmentBehavior="flip"
            contentAttachment="top center"
            isOpen={state.open && !props.disabled}
            targetAttachment="bottom center"
            targetRef={() => datePickerContainer.current}
            onPosition={handleOnPosition}
            onRequestClose={handleOnRequestClose}
            classNameArrow={cx('react-datepicker-arrow')}
            contentWidth="auto"
            contentHeight="auto"
            isArrowDisplayed
            isHeaderDisabled
            isContentFocusDisabled
            popupContentRole={null}
          >
            <PopupContainer ref={datePickerPopupContainer}>{calendar}</PopupContainer>
          </Popup>
        )}
      </>
    );
  }
}
export default injectIntl(DatePicker);
