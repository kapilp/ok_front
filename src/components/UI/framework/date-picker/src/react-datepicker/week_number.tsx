import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import styles from "./stylesheets/react_datepicker.module.scss";
const cx = classNames.bind(styles);
interface IWeekNumberProps extends JSX.HTMLAttributes<Element> {
  weekNumber: number;
  onClick?: (...args: any[]) => any;
}
export const WeekNumber: Component = (props:IWeekNumberProps) => {
  handleClick = event => {
    if (props.onClick) {
      props.onClick(event);
    }
  };
  render() {
    const weekNumberClasses = {
      "react-datepicker-week-number": true,
      "react-datepicker-week-number--clickable": !!props.onClick
    };
    return (
      <div
        className={cx(weekNumberClasses)}
        aria-label={`week-${props.weekNumber}`}
        onClick={handleClick}
      >
        {props.weekNumber}
      </div>
    );
  }
}
