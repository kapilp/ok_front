import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import DatePicker from '../../../DatePicker';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerOnChangeState = {
  date?: string;
  iSO?: string;
  inputValue?: string;
  isCompleteValue?: string;
  isValidValue?: string;
};
class DatePickerOnChange extends React.Component<{}, DatePickerOnChangeState> {
  constructor(props) {
    super(props);
    state = {
      date: '',
      iSO: '',
      inputValue: '',
      isCompleteValue: 'No',
      isValidValue: 'Yes',
    };
    handleDateChange = handleDateChange.bind(this);
  }
  handleDateChange(event, date, options) {
    setState({
      date,
      iSO: options.iSO,
      inputValue: options.inputValue,
      isCompleteValue: options.isCompleteValue ? 'Yes' : 'No',
      isValidValue: options.isValidValue ? 'Yes' : 'No',
    });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <h3>
          Selected Date:
          <span id="selected-date">{state.date}</span>
          <br />
          <br />
          ISO String: <span id="iso">{state.iSO}</span>
          <br />
          <br />
          Input Value: <span id="input-value">{state.inputValue}</span>
          <br />
          <br />
          Is Date Complete? <span id="complete-date">{state.isCompleteValue}</span>
          <br />
          <br />
          Is Date Valid? <span id="valid-date">{state.isValidValue}</span>
        </h3>
        <DatePicker name="date-input-onchange" onChange={handleDateChange} />
      </div>
    );
  }
}
export default DatePickerOnChange;
