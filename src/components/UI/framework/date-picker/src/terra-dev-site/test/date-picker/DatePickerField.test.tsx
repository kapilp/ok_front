import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import DatePickerField from '../../../DatePickerField';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerExampleState = {
  isInvalid?: boolean;
  isIncomplete?: boolean;
  required?: boolean;
};
class DatePickerExample extends React.Component<{}, DatePickerExampleState> {
  constructor(props) {
    super(props);
    state = {
      isInvalid: false,
      isIncomplete: false,
      required: false,
    };
    handleInvalidButtonClick = handleInvalidButtonClick.bind(this);
    handleIncompleteButtonClick = handleIncompleteButtonClick.bind(this);
  }
  handleInvalidButtonClick() {
    setState(prevState => ({
      isInvalid: !prevState.isInvalid,
    }));
  }
  handleIncompleteButtonClick() {
    setState(prevState => ({
      isIncomplete: !prevState.isIncomplete,
      required: !prevState.required,
    }));
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <button type="button" id="validity-toggle" onClick={handleInvalidButtonClick}>
          Toggle Validity
        </button>
        <button type="button" id="incomplete-toggle" onClick={handleIncompleteButtonClick}>
          Toggle Incomplete
        </button>
        <DatePickerField
          label="Enter Date"
          isInvalid={state.isInvalid}
          isIncomplete={state.isIncomplete}
          required={state.required}
          error="Error message."
          name="date-input"
          datePickerId="default"
          help="Help message."
        />
      </div>
    );
  }
}
export default DatePickerExample;
