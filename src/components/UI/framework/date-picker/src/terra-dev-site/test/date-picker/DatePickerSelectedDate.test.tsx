import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import moment from 'moment';
import classNames from 'classnames/bind';
import DatePicker from '../../../DatePicker';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerDefaultState = {
  date?: any;
};
class DatePickerDefault extends React.Component<{}, DatePickerDefaultState> {
  constructor(props) {
    super(props);
    handleSelectedDateUpdate = handleSelectedDateUpdate.bind(this);
    state = {
      date: moment().format(),
    };
  }
  handleSelectedDateUpdate() {
    setState({ date: moment().format() });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <DatePicker name="date-input" selectedDate={state.date} />
        <button type="button" onClick={handleSelectedDateUpdate}>
          Update selected date
        </button>
      </div>
    );
  }
}
export default DatePickerDefault;
