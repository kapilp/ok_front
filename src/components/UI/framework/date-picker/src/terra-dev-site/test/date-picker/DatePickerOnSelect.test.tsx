import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import DatePicker from '../../../DatePicker';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerOnChangeState = {
  date?: string;
};
class DatePickerOnChange extends React.Component<{}, DatePickerOnChangeState> {
  constructor(props) {
    super(props);
    state = { date: '' };
    handleDateSelect = handleDateSelect.bind(this);
  }
  handleDateSelect(event, date) {
    setState({ date });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <h3>
          Selected Date: <span id="selected-date">{state.date}</span>
        </h3>
        <DatePicker name="date-input-onselect" onSelect={handleDateSelect} />
      </div>
    );
  }
}
export default DatePickerOnChange;
