import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import DatePicker from '../../../DatePicker';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerOnBlurState = {
  blurTriggerCount?: number;
  focusTriggerCount?: number;
  iSO?: string;
  inputValue?: string;
  isCompleteValue?: string;
  isValidValue?: string;
};
class DatePickerOnBlur extends React.Component<{}, DatePickerOnBlurState> {
  blurCount: number;
  focusCount: number;
  constructor(props) {
    super(props);
    state = {
      blurTriggerCount: 0,
      focusTriggerCount: 0,
      iSO: '',
      inputValue: '',
      isCompleteValue: 'No',
      isValidValue: 'Yes',
    };
    handleBlur = handleBlur.bind(this);
    handleFocus = handleFocus.bind(this);
    blurCount = 0;
    focusCount = 0;
  }
  handleBlur(event, options) {
    blurCount += 1;
    setState({
      blurTriggerCount: blurCount,
      iSO: options.iSO,
      inputValue: options.inputValue,
      isCompleteValue: options.isCompleteValue ? 'Yes' : 'No',
      isValidValue: options.isValidValue ? 'Yes' : 'No',
    });
  }
  handleFocus() {
    focusCount += 1;
    setState({ focusTriggerCount: focusCount });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <h3>
          onBlur Trigger Count: <span id="blur-count">{state.blurTriggerCount}</span>
          <br />
          <br />
          onFocus Trigger Count: <span id="focus-count">{state.focusTriggerCount}</span>
          <br />
          <br />
          ISO String: <span id="iso">{state.iSO}</span>
          <br />
          <br />
          Input Value: <span id="input-value">{state.inputValue}</span>
          <br />
          <br />
          Is Date Complete? <span id="complete-date">{state.isCompleteValue}</span>
          <br />
          <br />
          Is Date Valid? <span id="valid-date">{state.isValidValue}</span>
        </h3>
        <DatePicker name="date-input-onblur" onBlur={handleBlur} onFocus={handleFocus} excludeDates={['2019-04-01', '2019-04-02']} />
      </div>
    );
  }
}
export default DatePickerOnBlur;
