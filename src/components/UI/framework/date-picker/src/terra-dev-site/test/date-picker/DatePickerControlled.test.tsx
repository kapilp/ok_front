import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import classNames from 'classnames/bind';
import DatePicker from '../../../DatePicker';
import styles from './common/DatePicker.test.module.scss';
const cx = classNames.bind(styles);
type DatePickerDefaultState = {
  date?: string;
};
class DatePickerDefault extends React.Component<{}, DatePickerDefaultState> {
  constructor(props) {
    super(props);
    handleSelectedDateUpdate = handleSelectedDateUpdate.bind(this);
    handleDateChange = handleDateChange.bind(this);
    handleDateChangeRaw = handleDateChangeRaw.bind(this);
    state = {
      date: '2019-03-07',
    };
  }
  handleDateChange(event, date) {
    setState({ date });
  }
  handleDateChangeRaw(event, date) {
    setState({ date });
  }
  handleSelectedDateUpdate(event) {
    setState({ date: event.currentTarget.textContent });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <DatePicker name="controlled-date-picker" value={state.date} onChange={handleDateChange} onChangeRaw={handleDateChangeRaw} />
        {'  '}
        <Button id="button1" text="" onClick={handleSelectedDateUpdate} />
        {'  '}
        <Button id="button2" text="01/01/2019" onClick={handleSelectedDateUpdate} />
        {'  '}
        <Button id="button3" text="2019-02-02" onClick={handleSelectedDateUpdate} />
        {'  '}
        <Button id="button4" text="2019-03-03T10:30" onClick={handleSelectedDateUpdate} />
        {'  '}
        <Button id="button5" text="0101123" onClick={handleSelectedDateUpdate} />
      </div>
    );
  }
}
export default DatePickerDefault;
