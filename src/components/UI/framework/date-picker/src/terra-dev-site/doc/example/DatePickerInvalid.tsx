import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Field from 'terra-form-field';
import DatePicker from 'terra-date-picker';
import classNames from 'classnames/bind';
import styles from './DatePickerExampleCommon.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The current DatePicker date if selected. Use for the selected date message.
   */
  selectedDate: JSX.Element;
}
const defaultProps = {
  selectedDate: '',
};
type DatePickerInvalidExampleState = {
  date?: any;
};
class DatePickerInvalidExample extends React.Component<{}, DatePickerInvalidExampleState> {
  constructor(props) {
    super(props);
    state = { date: props.selectedDate };
    handleDateChange = handleDateChange.bind(this);
  }
  handleDateChange(event, date) {
    setState({ date });
  }
  render() {
    return (
      <div>
        <p>
          Selected ISO Date:
          <span className={cx('date-wrapper')}>{state.date}</span>
        </p>
        <Field label="Enter Date" htmlFor="default-invalid">
          <DatePicker name="date-input" id="default-invalid" onChange={handleDateChange} isInvalid {...props} />
        </Field>
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default DatePickerInvalidExample;
