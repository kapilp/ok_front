import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import moment from 'moment';
import Field from 'terra-form-field';
import DatePicker from 'terra-date-picker';
import classNames from 'classnames/bind';
import styles from './DatePickerExampleCommon.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * The date value used for a controlled component.
   */
  value: string;
}
const defaultProps = {
  value: undefined,
};
type DatePickerExampleState = {
  date?: any;
};
class DatePickerExample extends React.Component<{}, DatePickerExampleState> {
  constructor(props) {
    super(props);
    state = { date: props.value };
    handleDateChange = handleDateChange.bind(this);
    handleDateChangeRaw = handleDateChangeRaw.bind(this);
  }
  handleDateChange(event, date) {
    setState({ date });
  }
  handleDateChangeRaw(event, date) {
    setState({ date });
  }
  render() {
    return (
      <div>
        <p>
          Selected ISO Date:
          <span className={cx('date-wrapper')}>{state.date}</span>
        </p>
        <Field label="Enter Date" htmlFor="controlled-dates">
          <DatePicker name="controlled-input" id="controlled-dates" onChange={handleDateChange} onChangeRaw={handleDateChangeRaw} value={state.date} />
        </Field>
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
const DatePickerExampleControlled = () => <DatePickerExample value={moment().format('YYYY-MM-DD')} />;
export default DatePickerExampleControlled;
