import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import moment from 'moment';
import DatePicker from './DatePicker';
interface Properties {
  /**
   * An ISO 8601 string representation of the default end date for a date range.
   */
  endDate: string;
  /**
   * Name of the endDate input. The name should be unique.
   */
  endName: string.isRequired;
  /**
   * An ISO 8601 string representation of the selected start date.
   */
  startDate: string;
  /**
   * Name of the startDate input. The name should be unique.
   */
  startName: string.isRequired;
  /**
   * A callback function to execute when a valid date is selected or entered. The parameters in the function are event, start date, end date.
   */
  onChange: PropTypes.func;
}
const defaultProps = {
  endDate: undefined,
  startDate: undefined,
  onChange: undefined,
};
interface IDateRangeProps extends JSX.HTMLAttributes<Element> {
  endDate?: any;
  endName?: any;
  startDate?: any;
  startName?: any;
  onChange?: any;
  customProps?: any;
}
type DateRangeState = {
  startDate?: any;
  endDate?: any;
};
class DateRange extends React.Component<IDateRangeProps, DateRangeState> {
  constructor(props) {
    super(props);
    state = {
      startDate: props.startDate,
      endDate: props.endDate,
    };
    handleChangeStart = handleChangeStart.bind(this);
    handleChangeEnd = handleChangeEnd.bind(this);
  }
  handleChange(event, { startDate = state.startDate, endDate = state.endDate }) {
    let startDateForRange = startDate;
    let endDateForRange = endDate;
    if (moment(startDateForRange).isAfter(moment(endDateForRange))) {
      [startDateForRange, endDateForRange] = [endDateForRange, startDateForRange];
    }
    setState({ startDate: startDateForRange, endDate: endDateForRange });
    if (props.onChange) {
      props.onChange(event, startDateForRange, endDateForRange);
    }
  }
  handleChangeStart(event, startDate) {
    handleChange(event, { startDate });
  }
  handleChangeEnd(event, endDate) {
    handleChange(event, { endDate });
  }
  render() {
    const { endDate, endName, startDate, startName, onChange, ...customProps } = props;
    return (
      <div className="terra-DatePicker-range">
        <DatePicker
          {...customProps}
          selectedDate={state.startDate}
          isStartDateRange
          startDate={state.startDate}
          endDate={state.endDate}
          name={startName}
          onChange={handleChangeStart}
        />
        <DatePicker {...customProps} selectedDate={state.endDate} isEndDateRange startDate={state.startDate} endDate={state.endDate} name={endName} onChange={handleChangeEnd} />
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default DateRange;
