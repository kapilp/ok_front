import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import { activeBreakpointForSize } from "terra-breakpoints";
import ResponsiveElement from "terra-responsive-element";
import { injectIntl, intlShape } from "react-intl";
/* eslint-disable-next-line  */
import {  JSX,  mergeProps, splitProps, useContext   } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateInput from "./DateInput";
import DateUtil from "./DateUtil";
import styles from "./DatePicker.module.scss";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * @private Whether or not to disable focus on the calendar button when the calendar picker dismisses.
   */
  disableButtonFocusOnClose: boolean,
  /**
   * Whether the date input should be disabled.
   */
  disabled: boolean,
  /**
   * An array of ISO 8601 string representation of the dates to disable in the picker. The values must be in the `YYYY-MM-DD` format.
   */
  excludeDates: PropTypes.arrayOf(string),
  /**
   * A function that gets called for each date in the picker to evaluate which date should be disabled.
   * A return value of true will be enabled and false will be disabled.
   */
  filterDate: PropTypes.func,
  /**
   * An array of ISO 8601 string representation of the dates to enable in the picker. All Other dates will be disabled. The values must be in the `YYYY-MM-DD` format.
   */
  includeDates: PropTypes.arrayOf(string),
  /**
   * Custom input attributes to apply to the date input. Use the name prop to set the name for the input.
   * Do not set the name in inputAttribute as it will be ignored.
   */
  inputAttributes: PropTypes.object,
  /**
   * @private
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  intl: intlShape.isRequired,
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete: boolean,
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid: boolean,
  /**
   * String that labels the current element. 'aria-label' must be present,
   * for accessibility.
   */
  ariaLabel: string,
  /**
   * An ISO 8601 string representation of the maximum date that can be selected. The value must be in the `YYYY-MM-DD` format. Must be on or before `12/31/2100`
   */
  maxDate: string,
  /**
   * An ISO 8601 string representation of the minimum date that can be selected. The value must be in the `YYYY-MM-DD` format. Must be on or after `01/01/1900`
   */
  minDate: string,
  /**
   * Name of the date input. The name should be unique.
   */
  name: string.isRequired,
  /**
   * A callback function triggered when the date picker component loses focus.
   * This event does not get triggered when the focus is moved from the date input to the calendar button since the focus is still within the main date picker component.
   * The first parameter is the event. The second parameter is the metadata to describe the current state of the input value at the time when the onBlur callback is triggered.
   */
  onBlur: PropTypes.func,
  /**
   * A callback function to execute when a valid date is selected or entered.
   * The first parameter is the event. The second parameter is the changed date value. The third parameter is the metadata to describe the current state of the input value at the time when the onChange callback is triggered.
   */
  onChange: PropTypes.func,
  /**
   * A callback function to execute when a change is made in the date input.
   * The first parameter is the event. The second parameter is the changed date value. The third parameter is the metadata to describe the current state of the input value at the time when the onChangeRaw callback is triggered.
   */
  onChangeRaw: PropTypes.func,
  /**
   * A callback function to execute when clicking outside of the picker to dismiss it.
   */
  onClickOutside: PropTypes.func,
  /**
   * A callback function triggered when the date picker component receives focus.
   * This event does not get triggered when the focus is moved from the date input to the calendar button since the focus is still within the main date picker component.
   */
  onFocus: PropTypes.func,
  /**
   * A callback function to execute when a date is selected from within the picker.
   */
  onSelect: PropTypes.func,
  /**
   * Whether or not the date is required.
   */
  required: boolean,
  /**
   * An ISO 8601 string representation of the default value to show in the date input. The value must be in the `YYYY-MM-DD` format.
   * This is analogous to defaultvalue in a form input field.
   */
  selectedDate: string,
  /**
   * The date value. This prop should only be used for a controlled date picker.
   * When this prop is set a handler is needed for both the `onChange` and `onChangeRaw` props to manage the date value.
   * If both `selectedDate` and this prop are set, then `selectedDate` will have no effect.
   * The value must be in the `YYYY-MM-DD` format or the all-numeric date format based on the locale.
   */
  value: string
};
const defaultProps = {
  disabled: false,
  excludeDates: undefined,
  filterDate: undefined,
  includeDates: undefined,
  inputAttributes: undefined,
  isIncomplete: false,
  isInvalid: false,
  maxDate: "2100-12-31",
  minDate: "1900-01-01",
  onBlur: undefined,
  onChange: undefined,
  onChangeRaw: undefined,
  onClickOutside: undefined,
  onFocus: undefined,
  onSelect: undefined,
  required: false,
  disableButtonFocusOnClose: false,
  selectedDate: undefined
};
interface IDatePickerProps extends JSX.HTMLAttributes<Element> {
  disableButtonFocusOnClose?: any;
  inputAttributes?: any;
  excludeDates?: any;
  filterDate?: any;
  includeDates?: any;
  intl?: any;
  isIncomplete?: any;
  isInvalid?: any;
  maxDate?: any;
  minDate?: any;
  name?: any;
  onBlur?: any;
  onChange?: any;
  onChangeRaw?: any;
  onClickOutside?: any;
  onFocus?: any;
  onSelect?: any;
  required?: any;
  selectedDate?: any;
  value?: any;
  ariaLabel?: any;
  customProps?: any;
  locale?: any;
}
type DatePickerState = {
  selectedDate?: any,
  showPortalPicker?: boolean,
  prevPropsSelectedDate?: any
};
class DatePicker extends React.Component<IDatePickerProps, DatePickerState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  calendarButton: any;
  containerHasFocus: boolean;
  context: any;
  datePickerContainer: any;
  dateValue: string;
  isDefaultDateAcceptable: boolean;
  onCalendarButtonClick: any;
  constructor(props) {
    super(props);
    const activeBreakpointOnMount = activeBreakpointForSize(window.innerWidth);
    state = {
      selectedDate: DateUtil.defaultValue(props),
      showPortalPicker:
        activeBreakpointOnMount === "tiny" ||
        activeBreakpointOnMount === "small",
      prevPropsSelectedDate: props.value || props.selectedDate
    };
    datePickerContainer = React.createRef();
    isDefaultDateAcceptable = false;
    containerHasFocus = false;
    handleBlur = handleBlur.bind(this);
    handleBreakpointChange = handleBreakpointChange.bind(this);
    handleChange = handleChange.bind(this);
    handleChangeRaw = handleChangeRaw.bind(this);
    handleFilterDate = handleFilterDate.bind(this);
    handleOnSelect = handleOnSelect.bind(this);
    handleOnClickOutside = handleOnClickOutside.bind(this);
    handleOnInputFocus = handleOnInputFocus.bind(this);
    handleFocus = handleFocus.bind(this);
    handleOnCalendarButtonClick = handleOnCalendarButtonClick.bind(
      this
    );
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    const { selectedDate, value } = nextProps;
    let nextDateValue = selectedDate;
    // Use the value for a controlled component if one is provided.
    if (value !== undefined && value !== null) {
      nextDateValue = value;
    }
    if (nextDateValue !== prevState.prevPropsSelectedDate) {
      const nextSelectedDate = DateUtil.createSafeDate(nextDateValue);
      if (nextSelectedDate) {
        return {
          selectedDate: nextSelectedDate,
          prevPropsSelectedDate: nextDateValue
        };
      }
      return {
        prevPropsSelectedDate: nextDateValue
      };
    }
    return null;
  }
  componentDidMount() {
    dateValue =
      DateUtil.formatMomentDate(
        state.selectedDate,
        DateUtil.getFormatByLocale(props.intl.locale)
      ) || "";
    isDefaultDateAcceptable = validateDefaultDate();
  }
  getMetadata() {
    const format = DateUtil.getFormatByLocale(props.intl.locale);
    const isCompleteDate = DateUtil.isValidDate(dateValue, format);
    const iSOString = isCompleteDate
      ? DateUtil.convertToISO8601(dateValue, format)
      : "";
    let isValidDate = false;
    if (
      dateValue === "" ||
      (isCompleteDate &&
        isDateWithinRange(DateUtil.createSafeDate(iSOString)))
    ) {
      isValidDate = true;
    }
    const metadata = {
      iSO: iSOString,
      inputValue: dateValue,
      isCompleteValue: isCompleteDate,
      isValidValue: isValidDate
    };
    return metadata;
  }
  handleBreakpointChange(activeBreakpoint) {
    const showPortalPicker =
      activeBreakpoint === "tiny" || activeBreakpoint === "small";
    if (state.showPortalPicker !== showPortalPicker) {
      setState({ showPortalPicker });
    }
  }
  handleFilterDate(date) {
    if (props.filterDate) {
      return props.filterDate(
        date && date.isValid()
          ? date.format(DateUtil.ISO_EXTENDED_DATE_FORMAT)
          : ""
      );
    }
    return true;
  }
  handleOnSelect(selectedDate, event) {
    // onSelect should only be invoked when selecting a date from the picker.
    // react-datepicker has an issue where onSelect is invoked both when selecting a date from the picker
    // as well as manually entering a valid date or clearing the date,
    // Until a fix is made, we need to return if the event type is 'change' indicating that onSelect was
    // invoked from a manual change. See https://github.com/Hacker0x01/react-datepicker/issues/990
    if (event.type === "change" || !selectedDate || !selectedDate.isValid()) {
      return;
    }
    dateValue = DateUtil.formatISODate(
      selectedDate,
      DateUtil.getFormatByLocale(props.intl.locale)
    );
    isDefaultDateAcceptable = true;
    if (props.onSelect) {
      props.onSelect(
        event,
        selectedDate.format(DateUtil.ISO_EXTENDED_DATE_FORMAT)
      );
    }
    if (!props.disableButtonFocusOnClose) {
      // Allows time for focus-trap to release focus on the picker before returning focus to the calendar button.
      setTimeout(() => {
        /*
         * Make sure the reference to calendarButton still exists before calling focus because it is possible that it is now
         * nullified after the 100 ms timeout due to a force remount of this component with a new `key` prop value.
         * Reference https://github.com/cerner/terra-framework/issues/1086
         */
        if (calendarButton) {
          calendarButton.focus();
        }
      }, 100);
    }
  }
  handleOnClickOutside(event) {
    if (props.onClickOutside) {
      props.onClickOutside(event);
    }
  }
  handleBlur(event) {
    // Modern browsers support event.relatedTarget but event.relatedTarget returns null in IE 10 / IE 11.
    // IE 11 sets document.activeElement to the next focused element before the blur event is called.
    const activeTarget = event.relatedTarget
      ? event.relatedTarget
      : document.activeElement;
    // Handle blur only if focus has moved out of the entire date picker component.
    if (!datePickerContainer.current.contains(activeTarget)) {
      if (props.onBlur) {
        const metadata = getMetadata();
        props.onBlur(event, metadata);
      }
      containerHasFocus = false;
    }
  }
  handleChange(date, event) {
    if (event.type === "change") {
      dateValue = event.target.value;
    }
    setState({
      selectedDate: date
    });
    if (props.onChange) {
      const metadata = getMetadata();
      props.onChange(
        event,
        date && date.isValid()
          ? date.format(DateUtil.ISO_EXTENDED_DATE_FORMAT)
          : "",
        metadata
      );
    }
  }
  handleChangeRaw(event) {
    dateValue = event.target.value;
    if (props.onChangeRaw) {
      const metadata = getMetadata();
      props.onChangeRaw(event, event.target.value, metadata);
    }
  }
  handleOnInputFocus(event) {
    handleFocus(event);
    if (!isDefaultDateAcceptable) {
      dateValue = "";
      handleChange(null, event);
      isDefaultDateAcceptable = true;
    }
  }
  handleFocus(event) {
    // Handle focus only if focus is gained from outside of the entire date picker component.
    // For IE 10/11 we cannot rely on event.relatedTarget since it is always null. Need to also check if containerHasFocus is false to
    // determine if the date-picker component did not have focus but will now gain focus.
    if (
      props.onFocus &&
      !containerHasFocus &&
      !datePickerContainer.current.contains(event.relatedTarget)
    ) {
      props.onFocus(event);
      containerHasFocus = true;
    }
  }
  handleOnCalendarButtonClick(event, onClick) {
    if (onCalendarButtonClick) {
      onCalendarButtonClick(event);
    }
    if (!isDefaultDateAcceptable && !validateDefaultDate()) {
      dateValue = "";
      handleChange(null, event);
    } else if (onClick) {
      // This onClick function is the onInputClick function coming from https://github.com/Hacker0x01/react-datepicker/blob/master/src/index.jsx#L326.
      // It does not take any parameter so there is not a need to pass in the event.
      onClick();
      isDefaultDateAcceptable = true;
    }
  }
  validateDefaultDate() {
    return isDateWithinRange(state.selectedDate);
  }
  isDateWithinRange(date) {
    let isAcceptable = true;
    if (
      DateUtil.isDateOutOfRange(
        date,
        DateUtil.createSafeDate(DateUtil.getMinDate(props.minDate)),
        DateUtil.createSafeDate(DateUtil.getMaxDate(props.maxDate))
      )
    ) {
      isAcceptable = false;
    }
    if (DateUtil.isDateExcluded(date, props.excludeDates)) {
      isAcceptable = false;
    }
    return isAcceptable;
  }
  render() {
    const {
      disableButtonFocusOnClose,
      inputAttributes,
      excludeDates,
      filterDate,
      includeDates,
      intl,
      isIncomplete,
      isInvalid,
      maxDate,
      minDate,
      name,
      onBlur,
      onChange,
      onChangeRaw,
      onClickOutside,
      onFocus,
      onSelect,
      required,
      selectedDate,
      value,
      ariaLabel,
      ...customProps
    } = props;
    onCalendarButtonClick = customProps.onCalendarButtonClick;
    delete customProps.onCalendarButtonClick;
    const dateFormat = DateUtil.getFormatByLocale(intl.locale);
    let formattedValue = DateUtil.strictFormatISODate(value, dateFormat);
    if (!formattedValue) {
      formattedValue = value;
    }
    let selectedDateInPicker;
    // If using this as a controlled component.
    if (value !== undefined) {
      // If value is empty, let selectedDateInPicker be undefined as in clearing the value.
      if (value !== "") {
        selectedDateInPicker = DateUtil.createSafeDate(
          DateUtil.convertToISO8601(value, dateFormat)
        );
        // If value is not a valid date, keep the previous selected date in the picker.
        if (selectedDateInPicker === undefined) {
          selectedDateInPicker = state.selectedDate;
        }
      }
    } else {
      selectedDateInPicker = state.selectedDate;
    }
    const theme = context;
    return (
      <div
        className={cx("date-picker", theme.className)}
        ref={datePickerContainer}
      >
        <ResponsiveElement
          onChange={handleBreakpointChange}
          responsiveTo="window"
        >
          <ReactDatePicker
            {...customProps}
            withPortal={state.showPortalPicker}
            selected={selectedDateInPicker}
            value={formattedValue}
            onBlur={handleBlur}
            onChange={handleChange}
            onChangeRaw={handleChangeRaw}
            onClickOutside={handleOnClickOutside}
            onFocus={handleOnInputFocus}
            onSelect={handleOnSelect}
            required={required}
            customInput={
              <DateInput
                onCalendarButtonClick={handleOnCalendarButtonClick}
                inputAttributes={inputAttributes}
                required={required}
                isIncomplete={isIncomplete}
                isInvalid={isInvalid}
                shouldShowPicker={
                  !isDefaultDateAcceptable &&
                  state.selectedDate === null
                }
                onButtonFocus={handleFocus}
                buttonRefCallback={buttonRef => {
                  calendarButton = buttonRef;
                }}
                ariaLabel={ariaLabel}
              />
            }
            excludeDates={DateUtil.filterInvalidDates(excludeDates)}
            filterDate={handleFilterDate}
            includeDates={DateUtil.filterInvalidDates(includeDates)}
            maxDate={DateUtil.createSafeDate(DateUtil.getMaxDate(maxDate))}
            minDate={DateUtil.createSafeDate(DateUtil.getMinDate(minDate))}
            todayButton={intl.formatMessage({ id: "Terra.datePicker.today" })}
            dateFormatCalendar=" "
            dateFormat={dateFormat}
            fixedHeight
            locale={intl.locale}
            placeholderText={intl.formatMessage({
              id: "Terra.datePicker.dateFormat"
            })}
            dropdownMode="select"
            showMonthDropdown
            showYearDropdown
            preventOpenOnFocus
            name={name}
            allowSameDay
          />
        </ResponsiveElement>
      </div>
    );
  }
}

export default injectIntl(DatePicker);
