import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from "terra-button";
import IconCalendar from "terra-icon/lib/icon/IconCalendar";
import Input from "terra-form-input";
import { injectIntl, intlShape } from "react-intl";
import classNames from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import DateUtil from "./DateUtil";
import { getLocalizedDateForScreenReader } from "./react-datepicker/date_utils";
import styles from "./DatePicker.module.scss";
const cx = classNames.bind(styles);
const Icon = <IconCalendar />;
interface Properties {
  /**
   * Callback ref to pass into the calendar button dom element.
   */
  buttonRefCallback: PropTypes.func,
  /**
   * Custom input attributes to apply to the date input.
   */
  inputAttributes: PropTypes.object,
  /**
   * @private
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  intl: intlShape.isRequired,
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete: boolean,
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid: boolean,
  /**
   * Name of the date input.
   */
  name: string,
  /**
   * A callback function triggered when the input or calendar button loses focus.
   */
  onBlur: PropTypes.func,
  /**
   * A callback function to execute when a valid date is selected or entered.
   */
  onChange: PropTypes.func,
  /**
   * The onInputClick callback function from react-datepicker to show the picker when clicked.
   */
  onClick: PropTypes.func,
  /**
   * A callback function triggered when the date input receives focus.
   */
  onFocus: PropTypes.func,
  /**
   * A callback function triggered when the calendar button receives focus.
   */
  onButtonFocus: PropTypes.func,
  /**
   * The onInputKeyDown callback function from react-datepicker to handle keyboard navigation.
   */
  onKeyDown: PropTypes.func,
  /**
   * The placeholder text to display in the date input.
   */
  placeholder: string,
  /**
   * Whether or not the date is required.
   */
  required: boolean,
  /**
   * @private Internal prop for showing date picker.
   */
  shouldShowPicker: boolean,
  /**
   * The selected or entered date value to display in the date input.
   */
  value: string,
  /**
   * String that labels the current element. 'aria-label' must be present,
   * for accessibility.
   */
  ariaLabel: string
};
const defaultProps = {
  buttonRefCallback: undefined,
  inputAttributes: undefined,
  isIncomplete: false,
  isInvalid: false,
  name: undefined,
  onBlur: undefined,
  onChange: undefined,
  onClick: undefined,
  onFocus: undefined,
  onButtonFocus: undefined,
  onKeyDown: undefined,
  required: false,
  placeholder: undefined,
  value: undefined,
  ariaLabel: undefined
};
interface IDatePickerInputProps extends JSX.HTMLAttributes<Element> {
  buttonRefCallback?: any;
  inputAttributes?: any;
  intl?: any;
  isIncomplete?: any;
  isInvalid?: any;
  name?: any;
  onBlur?: any;
  onChange?: any;
  onClick?: any;
  onFocus?: any;
  onButtonFocus?: any;
  onKeyDown?: any;
  placeholder?: any;
  required?: any;
  value?: any;
  ariaLabel?: any;
  customProps?: any;
}
export const DatePickerInput: Component = (props:IDatePickerInputProps) => {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  context: any;
  onCalendarButtonClick: any;
  shouldShowPicker: any;
  constructor(props) {
    super(props);
    handleOnButtonClick = handleOnButtonClick.bind(this);
    handleOnChange = handleOnChange.bind(this);
    handleOnKeyDown = handleOnKeyDown.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (
      shouldShowPicker &&
      !prevProps.shouldShowPicker &&
      props.onClick
    ) {
      props.onClick();
    }
  }
  handleOnButtonClick(event) {
    const attributes = { ...props.inputAttributes };
    if (
      !attributes.readOnly &&
      onCalendarButtonClick &&
      props.onClick
    ) {
      onCalendarButtonClick(event, props.onClick);
    }
  }
  handleOnChange(event) {
    if (!DateUtil.validDateInput(event.target.value)) {
      return;
    }
    if (props.onChange) {
      props.onChange(event);
    }
  }
  handleOnKeyDown(event) {
    if (props.onKeyDown) {
      props.onKeyDown(event);
    }
  }
  render() {
    const {
      buttonRefCallback,
      inputAttributes,
      intl,
      isIncomplete,
      isInvalid,
      name,
      onBlur,
      onChange,
      onClick,
      onFocus,
      onButtonFocus,
      onKeyDown,
      placeholder,
      required,
      value,
      ariaLabel,
      ...customProps
    } = props;
    onCalendarButtonClick = customProps.onCalendarButtonClick;
    shouldShowPicker = customProps.shouldShowPicker;
    delete customProps.onCalendarButtonClick;
    delete customProps.shouldShowPicker;
    const additionalInputProps = { ...customProps, ...inputAttributes };
    const dateValue = DateUtil.convertToISO8601(
      value,
      DateUtil.getFormatByLocale(intl.locale)
    );
    const inputClasses = cx([
      "input",
      { "is-invalid": isInvalid },
      { "is-incomplete": isIncomplete && required && !isInvalid }
    ]);
    const buttonClasses = cx(["button", { "is-invalid": isInvalid }]);
    const buttonText = intl.formatMessage({
      id: "Terra.datePicker.openCalendar"
    });
    const theme = context;
    const label = props.ariaLabel
      ? props.ariaLabel
      : intl.formatMessage({ id: "Terra.datePicker.date" });
    return (
      <div className={cx("custom-input", theme.className)}>
        <input
          // Create a hidden input for storing the name and value attributes to use when submitting the form.
          // The data stored in the value attribute will be the visible date in the date input but in ISO 8601 format.
          data-terra-date-input-hidden
          type="hidden"
          name={name}
          value={dateValue}
        />
        <Input
          {...additionalInputProps}
          className={inputClasses}
          type="text"
          name={"terra-date-".concat(name)}
          value={value}
          onChange={handleOnChange}
          placeholder={placeholder}
          onFocus={onFocus}
          onBlur={onBlur}
          ariaLabel={
            value
              ? `${label}, ${getLocalizedDateForScreenReader(
                  DateUtil.createSafeDate(dateValue),
                  { intl: props.intl, locale: props.intl.locale }
                )}`
              : label
          }
        />
        <Button
          className={buttonClasses}
          text={buttonText}
          onClick={handleOnButtonClick}
          onKeyDown={handleOnKeyDown}
          icon={Icon}
          isIconOnly
          isCompact
          isDisabled={additionalInputProps.disabled}
          onBlur={onBlur}
          onFocus={onButtonFocus}
          ref={buttonRefCallback}
        />
      </div>
    );
  }
}

export default injectIntl(DatePickerInput);
