import { getCustomProps } from "../../src/LayoutUtils";
describe("LayoutUtils", () => {
  describe("getCustomProps", () => {
    it("filters out custom props", () => {
      interface Properties {
        prop1: string,
        prop2: string,
        prop3: string
      };
      const props = {
        prop1: "1",
        prop2: "2",
        prop3: "3",
        prop4: "4",
        prop5: "5"
      };
      expect(getCustomProps(props, propTypes)).toEqual({
        prop4: "4",
        prop5: "5"
      });
    });
  });
});
