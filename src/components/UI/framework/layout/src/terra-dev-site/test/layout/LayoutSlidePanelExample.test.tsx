import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import LayoutSlidePanel from '../../../_LayoutSlidePanel';
import styles from './TestLayoutCommon.module.scss';
const cx = classNames.bind(styles);
type LayoutSlidePanelExampleState = {
  isOpen?: boolean;
};
class LayoutSlidePanelExample extends React.Component<{}, LayoutSlidePanelExampleState> {
  constructor(props) {
    super(props);
    toggleMenu = toggleMenu.bind(this);
    state = { isOpen: false };
  }
  toggleMenu() {
    setState(prevState => ({ isOpen: !prevState.isOpen }));
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <LayoutSlidePanel
          panelContent={
            <div className={cx('panel-content')}>
              <button type="button" id="test-toggle-2" onClick={toggleMenu}>
                toggle
              </button>
            </div>
          }
          panelBehavior="overlay"
          size="medium"
          isOpen={state.isOpen}
          isToggleEnabled
          isAnimated
          onToggle={() => {}}
          toggleText="toggle text"
        >
          <div className={cx('layout-slide-panel-content')}>
            <button type="button" id="test-toggle-1" onClick={toggleMenu}>
              toggle
            </button>
          </div>
        </LayoutSlidePanel>
      </div>
    );
  }
}
export default LayoutSlidePanelExample;
