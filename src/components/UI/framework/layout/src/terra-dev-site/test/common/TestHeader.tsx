import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './TestHeader.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  layoutConfig: PropTypes.object;
}
const TestHeader: Component = ({ layoutConfig }) => (
  <div id="test-header" className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2 className={cx('content-text')}>Header</h2>
      {layoutConfig.toggleMenu && (
        <button type="button" className={cx('test-header-toggle')} onClick={layoutConfig.toggleMenu}>
          Toggle Menu
        </button>
      )}
    </div>
  </div>
);
export default TestHeader;
