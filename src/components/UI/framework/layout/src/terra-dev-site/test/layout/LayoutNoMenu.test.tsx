import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Layout from '../../../Layout';
import TestHeader from '../common/TestHeader';
import TestContent from '../common/TestContent';
import styles from './TestLayoutCommon.module.scss';
const cx = classNames.bind(styles);
const LayoutNoMenu = () => (
  <Layout className={cx('layout-test')} key="layout-no-menu" header={<TestHeader />} id="test-root">
    <TestContent />
  </Layout>
);
export default LayoutNoMenu;
