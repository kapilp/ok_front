import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './LayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
type menuProps = {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
    togglePin?: (...args: any[]) => any;
    menuIsPinned?: boolean;
  };
};
export const MenuExample = (props: menuProps) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2 className={cx('content-text')}>Menu</h2>
      {props.layoutConfig.toggleMenu && (
        <button type="button" className={cx('button')} onClick={props.layoutConfig.toggleMenu}>
          Toggle Menu
        </button>
      )}
      {props.layoutConfig.togglePin && !props.layoutConfig.menuIsPinned && (
        <button type="button" className={cx('button')} onClick={props.layoutConfig.togglePin}>
          Pin
        </button>
      )}
      {props.layoutConfig.togglePin && props.layoutConfig.menuIsPinned && (
        <button type="button" className={cx('button')} onClick={props.layoutConfig.togglePin}>
          Unpin
        </button>
      )}
    </div>
  </div>
);
