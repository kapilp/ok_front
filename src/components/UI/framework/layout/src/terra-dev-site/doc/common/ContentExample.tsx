import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './LayoutDocCommon.module.scss';
const cx = classNames.bind(styles);
type contentProps = {
  layoutConfig?: {
    toggleMenu?: (...args: any[]) => any;
  };
};
export const ContentExample = (props: contentProps) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2 className={cx('content-text')}>Content</h2>
      {props.layoutConfig.toggleMenu && (
        <button type="button" className={cx('button')} onClick={props.layoutConfig.toggleMenu}>
          Toggle Menu(From Content)
        </button>
      )}
    </div>
  </div>
);
