import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Layout } from '../../../Layout';
import { ContentExample } from '../common/ContentExample';
import { MenuExample } from '../common/MenuExample';
import { ToolbarExample } from '../common/ToolbarExample';
import classNames from 'classnames/bind';
import styles from './LayoutDocExampleCommon.module.scss';
const cx = classNames.bind(styles);

export const LayoutNoHeader = () => (
  <Layout menu={props => <MenuExample {...props} />} menuText="Menu" className={cx('layout-example')}>
    {props => <ContentExample {...props} />}
  </Layout>
);
