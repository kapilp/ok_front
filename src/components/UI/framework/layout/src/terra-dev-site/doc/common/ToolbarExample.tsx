import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import styles from './ToolbarExample.module.scss';
const cx = classNames.bind(styles);
interface toolbarProps {
  layoutConfig: {};
}
export const ToolbarExample = (props: toolbarProps) => (
  <div className={cx('content-wrapper1')}>
    <div className={cx('content-wrapper2')}>
      <h2 className={cx('content-text')}>Header</h2>
      {props.layoutConfig.toggleMenu && (
        <button type="button" className={cx('button')} onClick={props.layoutConfig.toggleMenu}>
          Toggle Menu
        </button>
      )}
    </div>
  </div>
);
