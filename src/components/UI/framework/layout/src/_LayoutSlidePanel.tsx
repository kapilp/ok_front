import {  JSX, Component, createEffect, createMemo, createSignal, onCleanup, mergeProps, splitProps, useContext  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames';
import classNamesBind from 'classnames/bind';
import { ThemeContext } from '../../theme-context/src/ThemeContext';
import { Overlay } from '../../../core/overlay/src/Overlay';
import { OverlayContainer } from '../../../core/overlay/src/OverlayContainer';
import { tabbable } from 'tabbable';
import styles from './LayoutSlidePanel.module.scss';
const cx = classNamesBind.bind(styles);
interface ILayoutSlidePanelProps extends JSX.HTMLAttributes<Element> {
  /**
   * Enables animations for panel state transitions.
   */
  isAnimated?: boolean;
  /**
   * Enables panel visibility.
   */
  isOpen?: boolean;
  /**
   * Enables toggling for the panel.
   */
  isToggleEnabled?: boolean;
  /**
   * The element to display in the main content area.
   */
  children?: JSX.Element;
  /**
   * The style of panel presentation. One of `overlay`, `squish`.
   */
  panelBehavior?: 'overlay' | 'squish';
  /**
   * The component to display in the panel content area.
   */
  panelContent?: JSX.Element;
  /**
   * Current breakpoint size.
   */
  size: string;
  /**
   * The function called when panel state changes are desired.
   */
  onToggle?: () => void;
  /**
   * String to display on menu hover target.
   */
  toggleText?: string;
}
const defaultProps = {
  isAnimated: false,
  isOpen: false,
  isToggleEnabled: false,
  panelBehavior: 'overlay',
};

export const LayoutSlidePanel = (props: ILayoutSlidePanelProps) => {
  props = mergeProps({}, defaultProps, props);
  let isHidden = !props.isOpen;
  let lastIsOpen: boolean;
  const [panelNode, setPanelNode] = createSignal<HTMLDivElement>();

  createEffect(() => {
    document.createElement('main'); // shim for <main> tag not being recognized in IE
    if (panelNode()) {
      panelNode().addEventListener('transitionend', handleTransitionEnd);
    }
  });
  createEffect(() => {
    lastIsOpen = props.isOpen!;
  });
  onCleanup(() => {
    if (panelNode()) {
      panelNode().removeEventListener('transitionend', handleTransitionEnd);
    }
  });

  const handleTransitionEnd = () => {
    if (!props.isOpen && panelNode()) {
      panelNode().setAttribute('aria-hidden', 'true');
      isHidden = true;
      // Sends focus back to the application layout header toggle button if it exists
      if (document.querySelector('button[data-application-header-toggle]')) {
        document.querySelector('button[data-application-header-toggle]').focus();
        // Else, we'll send focus back to first interactable element in the main panel
      } else if (tabbable(document.querySelector('[data-terra-layout-main]'))[0]) {
        tabbable(document.querySelector('[data-terra-layout-main]'))[0].focus();
      }
    }
  };
  const preparePanelForTransition = () => {
    // React 16.3 will be deprecating componentWillRecieveProps and componentWillUpdate, and removed in 17, so code execution prior to render becomes difficult.
    // As a result of this change, we are executing the code in the render block.
    if (props.isOpen && !lastIsOpen && panelNode()) {
      // If the panel is opening remove the hidden attribute so the animation performs correctly.
      panelNode().setAttribute('aria-hidden', 'false');
      isHidden = false;
      if (tabbable(panelNode())[0]) {
        tabbable(panelNode())[0].focus();
      }
    }
  };
  const [p, customProps] = splitProps(props, ['isAnimated', 'isOpen', 'isToggleEnabled', 'children', 'panelBehavior', 'panelContent', 'size', 'onToggle', 'toggleText']);
  preparePanelForTransition();
  const isTiny = createMemo(() => p.size === 'tiny');
  const isSmall = createMemo(() => p.size === 'small');
  const compactSize = createMemo(() => isTiny() || isSmall());
  const isOverlay = createMemo(() => (compactSize() ? true : p.panelBehavior === 'overlay'));
  const isOverlayOpen = createMemo(() => p.isOpen && isOverlay());
  const overlayBackground = createMemo(() => (compactSize() ? 'dark' : 'clear'));
  const theme = useContext(ThemeContext);
  const slidePanelClassNames = createMemo(() =>
    classNames(cx('layout-slide-panel', { 'is-open': p.isOpen }, { 'is-overlay': isOverlay() }, { 'is-squish': !isOverlay() }, theme.className), customProps.className),
  );
  const panelClasses = createMemo(() => cx('panel', { 'is-tiny': isTiny() }, { 'is-small': isSmall() }, { 'is-animated': p.isAnimated && isOverlay() && !!p.panelContent }));
  return (
    <div {...customProps} className={slidePanelClassNames()}>
      <div className={panelClasses()} aria-hidden={isHidden ? 'true' : 'false'} ref={setPanelNode}>
        {p.panelContent}
      </div>
      <OverlayContainer className={cx('content')}>
        <Overlay isRelativeToContainer onRequestClose={p.onToggle} isOpen={isOverlayOpen()} backgroundStyle={overlayBackground()} zIndex="6000" />
        <main role="main" data-terra-layout-main className={cx('main-container')}>
          {p.children}
        </main>
      </OverlayContainer>
    </div>
  );
};
