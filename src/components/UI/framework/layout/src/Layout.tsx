import {  JSX, Component, createEffect, createMemo, onCleanup, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import { ContentContainer } from '../../../core/content-container/src/ContentContainer';
import LodashDebounce from 'lodash.debounce';
import { LayoutSlidePanel } from './_LayoutSlidePanel';
import { getBreakpointSize, getCustomProps } from './LayoutUtils';
import styles from './LayoutSlidePanel.module.scss';
const cx = classNames.bind(styles);
type RenderProps = {
  layoutConfig: {
    toggleMenu?: (...args: any[]) => any;
    togglePin?: (...args: any[]) => any;
    menuIsPinned?: boolean;
  };
};
interface ILayoutProps extends JSX.HTMLAttributes<Element> {
  /**
   * Element to be placed within the header section of the layout.
   */
  header?: (props: RenderProps) => JSX.Element;
  /**
   * Element to be placed within the menu section of the layout. If not set, Layout-provided menu controls will be hidden.
   */
  menu?: (props: RenderProps) => JSX.Element;
  /**
   * String used to decorate menu hover control.
   */
  menuText?: string;
  /**
   * Element to be placed within the main content section of the layout.
   */
  children?: (props: RenderProps) => JSX.Element;
}

function stateForProps(props, currentState) {
  const isToggleMenu = currentState.size === 'tiny' || currentState.size === 'small';
  const isFixedMenu = !isToggleMenu;
  const menuIsPresent = !!props.menu;
  return {
    ...(currentState || {}),
    isFixedMenu,
    isToggleMenu,
    menuIsPresent,
    menuIsOpen: menuIsPresent && (currentState.menuIsOpen || isFixedMenu),
    menuIsPinned: menuIsPresent && currentState.menuIsPinned,
  };
}
type LayoutState = {
  isFixedMenu: boolean;
  isToggleMenu: boolean;
  menuIsPresent: boolean;
  menuIsOpen: boolean;
  menuIsPinned: boolean;
};
export const Layout = (props: ILayoutProps) => {
  const [state, setState] = createStore(
    stateForProps(props, {
      size: getBreakpointSize(),
      prevProps: props,
    }) as LayoutState,
  );
  // createEffect(() => {
  //   setState(
  //     stateForProps(props, {
  //       size: getBreakpointSize(),
  //       prevProps: props,
  //     }) as LayoutState,
  //   );
  // });

  let updateSize = () => {
    const newSize = getBreakpointSize();
    if (state.size !== newSize) {
      setState(
        stateForProps(props, {
          size: newSize,
        }),
      );
    }
  };
  updateSize = LodashDebounce(updateSize.bind(this), 100);
  //const componentDidMount = () => {
  window.addEventListener('resize', updateSize);
  //}
  onCleanup(() => {
    window.removeEventListener('resize', updateSize);
  });
  const toggleMenu = () => {
    return new Promise(resolve => {
      setState({
        menuIsOpen: !state.menuIsOpen,
      });
      resolve();
    });
  };
  const togglePin = () => {
    return new Promise(resolve => {
      setState({
        menuIsPinned: !state.menuIsPinned,
      });
      resolve();
    });
  };
  const renderHeader = () => {
    const shouldAllowMenuToggle = createMemo(() => state.isToggleMenu && state.menuIsPresent);
    return (
      <>
        {props.header &&
          props.header({
            layoutConfig: {
              size: state.size,
              toggleMenu: shouldAllowMenuToggle() ? toggleMenu : undefined,
              menuIsOpen: state.menuIsOpen,
            },
          })}
      </>
    );
  };
  const renderMenu = () => {
    const shouldAllowMenuToggle = createMemo(() => state.isToggleMenu && state.menuIsPresent);

    return (
      <>
        {state.menuIsPresent &&
          props.menu &&
          props.menu({
            layoutConfig: {
              size: state.size,
              toggleMenu: shouldAllowMenuToggle() ? toggleMenu : undefined,
              menuIsOpen: state.menuisOpen,
              menuIsPinned: state.menuIsPinned,
            },
          })}
      </>
    );
  };
  const renderContent = () => {
    const shouldAllowMenuToggle = createMemo(() => state.isToggleMenu && state.menuIsPresent);
    return (
      <ContentContainer fill header={state.isToggleMenu && renderHeader()} className={cx('content-container')}>
        {props.children &&
          props.children({
            layoutConfig: {
              size: state.size,
              toggleMenu: shouldAllowMenuToggle() ? toggleMenu : undefined,
              menuIsOpen: state.menuIsOpen,
            },
          })}
      </ContentContainer>
    );
  };
  const [p, customProps] = splitProps(props, ['header', 'menu', 'menuText', 'children']);
  return (
    <ContentContainer fill header={!state.isToggleMenu && renderHeader()} {...customProps}>
      <LayoutSlidePanel
        panelContent={renderMenu()}
        panelBehavior={state.menuIsPinned || state.isFixedMenu ? 'squish' : 'overlay'}
        size={state.size}
        onToggle={toggleMenu}
        toggleText={props.menuText}
        isOpen={state.menuIsOpen}
        isAnimated
      >
        {renderContent()}
      </LayoutSlidePanel>
    </ContentContainer>
  );
};
