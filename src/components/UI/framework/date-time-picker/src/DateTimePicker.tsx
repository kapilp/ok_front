import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { injectIntl, intlShape } from "react-intl";
import classNames from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import DatePicker from "terra-date-picker";
import TimeInput from "terra-time-input";
import * as KeyCode from "keycode-js";
import DateUtil from "terra-date-picker/lib/DateUtil";
import styles from "./DateTimePicker.module.scss";
import DateTimeUtils from "./DateTimeUtils";
import TimeClarification from "./_TimeClarification";
const cx = classNames.bind(styles);
interface Properties {
  /**
   * Custom input attributes to apply to the date input. Use the name prop to set the name for the date input.
   * Do not set the name in inputAttribute as it will be ignored.
   */
  dateInputAttributes: PropTypes.object,
  /**
   * Whether the date and time inputs should be disabled.
   */
  disabled: boolean,
  /**
   * An array of ISO 8601 string representation of the dates to disable in the picker. The values must be in the `YYYY-MM-DDThh:mm:ss` format.
   */
  excludeDates: PropTypes.arrayOf(string),
  /**
   * A function that gets called for each date in the picker to evaluate which date should be disabled.
   * A return value of true will be enabled and false will be disabled.
   */
  filterDate: PropTypes.func,
  /**
   * An array of ISO 8601 string representation of the dates to enable in the picker. The values must be in the `YYYY-MM-DDThh:mm:ss` format.
   * All Other dates will be disabled.
   */
  includeDates: PropTypes.arrayOf(string),
  /**
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  intl: intlShape.isRequired,
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete: boolean,
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid: boolean,
  /**
   * Whether the selected meridiem displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalidMeridiem: boolean,
  /**
   * An ISO 8601 string representation of the maximum date that can be selected in the date picker. The value must be in the `YYYY-MM-DD` format. Must be on or before `12/31/2100`.
   * The time portion in this value is ignored because this is strictly used in the date picker.
   */
  maxDate: string,
  /**
   * An ISO 8601 string representation of the minimum date that can be selected in the date picker. The value must be in the `YYYY-MM-DD` format. Must be on or after `01/01/1900`.
   * The time portion in this value is ignored because this is strictly used in the date picker.
   */
  minDate: string,
  /**
   * Name of the date input. The name should be unique.
   */
  name: string.isRequired,
  /**
   * A callback function triggered when the entire date time picker component loses focus.
   * This event does not get triggered when the focus is moved from the date input to the time input because the focus is still within the main date time picker component.
   * The first parameter is the event. The second parameter is the metadata to describe the current state of the input value at the time when the onBlur callback is triggered.
   */
  onBlur: PropTypes.func,
  /**
   * A callback function to execute when a valid date is selected or entered.
   * The first parameter is the event. The second parameter is the changed input value. The third parameter is the metadata to describe the current state of the input value at the time when the onChange callback is triggered.
   */
  onChange: PropTypes.func,
  /**
   * A callback function to execute when a change is made in the date or time input.
   * The first parameter is the event. The second parameter is the changed input value. The third parameter is the metadata to describe the current state of the input value at the time when the onChangeRaw callback is triggered
   */
  onChangeRaw: PropTypes.func,
  /**
   * A callback function to execute when clicking outside of the picker to dismiss it.
   */
  onClickOutside: PropTypes.func,
  /**
   * A callback function triggered when the date input, hour input, or minute input receives focus.
   */
  onFocus: PropTypes.func,
  /**
   * A callback function to execute when a selection is made in the date picker.
   * The first parameter is the event. The second parameter is the selected input value in ISO format.
   */
  onSelect: PropTypes.func,
  /**
   * Whether or not the date is required.
   */
  required: boolean,
  /**
   * Whether an input field for seconds should be shown or not. If true then the second field must have a valid
   * number for the overall input to be considered valid.
   */
  showSeconds: boolean,
  /**
   * Custom input attributes to apply to the time input. Use the name prop to set the name for the time input.
   * Do not set the name in inputAttribute as it will be ignored.
   */
  timeInputAttributes: PropTypes.object,
  /**
   * An ISO 8601 string representation of the initial value to show in the date and time inputs. The value must be in the `YYYY-MM-DDThh:mm:ss` format.
   */
  value: string,
  /**
   * Type of time input to initialize. Must be `24-hour` or `12-hour`.
   * The `de`, `es-ES`, `fr-FR`, `fr`, `nl-BE`, `nl`, `pt-BR`, `pt`, `sv-SE` and `sv` locales do not use the 12-hour time notation.
   * If the `variant` prop if set to `12-hour` for one of these supported locales, the variant will be ignored and defaults to `24-hour`.
   */
  timeVariant: PropTypes.oneOf([
    DateTimeUtils.FORMAT_12_HOUR,
    DateTimeUtils.FORMAT_24_HOUR
  ])
};
const defaultProps = {
  dateInputAttributes: undefined,
  disabled: false,
  excludeDates: undefined,
  filterDate: undefined,
  includeDates: undefined,
  isIncomplete: false,
  isInvalid: false,
  isInvalidMeridiem: false,
  maxDate: "2100-12-31",
  minDate: "1900-01-01",
  onBlur: undefined,
  onChange: undefined,
  onChangeRaw: undefined,
  onClickOutside: undefined,
  onFocus: undefined,
  onSelect: undefined,
  required: false,
  showSeconds: false,
  timeInputAttributes: undefined,
  value: undefined,
  timeVariant: DateTimeUtils.FORMAT_24_HOUR
};
interface IDateTimePickerProps extends JSX.HTMLAttributes<Element> {
  dateInputAttributes?: any;
  disabled?: any;
  excludeDates?: any;
  filterDate?: any;
  includeDates?: any;
  isIncomplete?: any;
  isInvalid?: any;
  isInvalidMeridiem?: any;
  onBlur?: any;
  onChange?: any;
  onChangeRaw?: any;
  onClickOutside?: any;
  onFocus?: any;
  onSelect?: any;
  maxDate?: any;
  minDate?: any;
  name?: any;
  required?: any;
  showSeconds?: any;
  timeInputAttributes?: any;
  value?: any;
  timeVariant?: any;
  customProps?: any;
}
type DateTimePickerState = {
  dateTime?: any,
  isAmbiguousTime?: boolean,
  isTimeClarificationOpen?: boolean,
  dateFormat?: any,
  format?: any,
  clone?: any
};
class DateTimePicker extends React.Component<
  IDateTimePickerProps,
  DateTimePickerState
> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  containerHasFocus: boolean;
  context: any;
  dateTimePickerContainer: any;
  dateValue: string;
  forceUpdate: any;
  hourInput: any;
  isDefaultDateAcceptable: boolean;
  isDefaultDateTimeAcceptable: boolean;
  timeValue: any;
  wasOffsetButtonClicked: boolean;
  constructor(props) {
    super(props);
    state = {
      dateTime: DateTimeUtils.createSafeDate(props.value),
      isAmbiguousTime: false,
      isTimeClarificationOpen: false,
      dateFormat: DateUtil.getFormatByLocale(props.intl.locale)
    };
    // The dateValue and timeValue variables represent the actual value in the date input and time input respectively.
    // They are used to keep track of the currently entered value to determine whether or not the entry is valid.
    // Unlike dateValue and timeValue, state.dateTime is the internal moment object representing both the date and time as one entity
    // It is used for date/time manipulation and used to calculate the missing/ambiguous hour.
    // The dateValue and timeValue are tracked outside of the react state to limit the number of renderings that occur.
    dateValue =
      DateUtil.formatMomentDate(state.dateTime, state.dateFormat) ||
      "";
    timeValue = DateTimeUtils.hasTime(props.value)
      ? DateTimeUtils.getTime(props.value, props.showSeconds)
      : "";
    isDefaultDateTimeAcceptable = true;
    wasOffsetButtonClicked = false;
    handleDateChange = handleDateChange.bind(this);
    handleDateChangeRaw = handleDateChangeRaw.bind(this);
    handleTimeChange = handleTimeChange.bind(this);
    handleOnSelect = handleOnSelect.bind(this);
    handleOnDateBlur = handleOnDateBlur.bind(this);
    handleOnTimeBlur = handleOnTimeBlur.bind(this);
    handleBlur = handleBlur.bind(this);
    handleDaylightSavingButtonClick = handleDaylightSavingButtonClick.bind(
      this
    );
    handleStandardTimeButtonClick = handleStandardTimeButtonClick.bind(
      this
    );
    handleOnDateInputFocus = handleOnDateInputFocus.bind(this);
    handleOnTimeInputFocus = handleOnTimeInputFocus.bind(this);
    handleFocus = handleFocus.bind(this);
    handleOnCalendarButtonClick = handleOnCalendarButtonClick.bind(
      this
    );
    handleOffsetButtonClick = handleOffsetButtonClick.bind(this);
    handleOnRequestClose = handleOnRequestClose.bind(this);
    dateTimePickerContainer = React.createRef();
    containerHasFocus = false;
  }
  componentDidMount() {
    isDefaultDateAcceptable = validateDefaultDate();
  }
  componentDidUpdate() {
    // If the entered time (timeValue) is the missing hour during daylight savings,
    // it needs to be updated to the time in state.dateTime to reflect the change and force a render.
    if (
      state.dateTime &&
      DateTimeUtils.isValidTime(timeValue, props.showSeconds)
    ) {
      const displayedTime = DateTimeUtils.getTime(
        state.dateTime.format(),
        props.showSeconds
      );
      if (timeValue !== displayedTime) {
        timeValue = displayedTime;
        forceUpdate();
      }
    }
  }
  getMetadata(momentDateTime) {
    let tempDateTime =
      momentDateTime && DateTimeUtils.isMomentObject(momentDateTime)
        ? momentDateTime.clone()
        : null;
    if (DateUtil.isValidDate(dateValue, state.dateFormat)) {
      const enteredDateTime = DateTimeUtils.convertDateTimeStringToMomentObject(
        dateValue,
        timeValue,
        state.dateFormat,
        props.showSeconds
      );
      // state.dateTime does not get updated if the entered date is outside the minDate/maxDate range or an excluded date.
      // In this case, we need to use the date that was entered instead of the state.dateTime.
      if (enteredDateTime && !enteredDateTime.isSame(tempDateTime, "day")) {
        tempDateTime = enteredDateTime;
      }
    }
    let iSOString = "";
    const isCompleteDateTime = DateTimeUtils.isValidDateTime(
      dateValue,
      timeValue,
      state.dateFormat,
      props.showSeconds
    );
    if (isCompleteDateTime && tempDateTime) {
      iSOString = tempDateTime.format();
    }
    let timeValue = timeValue || "";
    if (iSOString) {
      timeValue = DateTimeUtils.getTime(iSOString, props.showSeconds);
    }
    let isValid = false;
    const inputValue = `${
      dateValue ? dateValue : ""
    } ${timeValue}`.trim();
    if (
      inputValue === "" ||
      (isCompleteDateTime &&
        tempDateTime &&
        isDateTimeAcceptable(tempDateTime))
    ) {
      isValid = true;
    }
    let isAmbiguous = false;
    if (isCompleteDateTime && tempDateTime) {
      isAmbiguous = DateTimeUtils.checkAmbiguousTime(tempDateTime);
    }
    const metadata = {
      iSO: iSOString,
      inputValue,
      dateValue: dateValue || "",
      timeValue,
      isAmbiguousHour: isAmbiguous,
      isCompleteValue: isCompleteDateTime,
      isValidValue: isValid
    };
    return metadata;
  }
  handleOnSelect(event, selectedDate) {
    dateValue = DateUtil.formatISODate(
      selectedDate,
      state.dateFormat
    );
    const previousDateTime = state.dateTime
      ? state.dateTime.clone()
      : null;
    const updatedDateTime = DateTimeUtils.syncDateTime(
      previousDateTime,
      selectedDate,
      timeValue,
      props.showSeconds
    );
    if (
      !previousDateTime ||
      previousDateTime.format() !== updatedDateTime.format()
    ) {
      checkAmbiguousTime(updatedDateTime);
    }
    if (props.onSelect) {
      props.onSelect(event, updatedDateTime.format());
    }
  }
  handleOnDateBlur(event) {
    // Modern browsers support event.relatedTarget but event.relatedTarget returns null in IE 10 / IE 11.
    // IE 11 sets document.activeElement to the next focused element before the blur event is called.
    const activeTarget = event.relatedTarget
      ? event.relatedTarget
      : document.activeElement;
    // Handle blur only if focus has moved out of the entire date time picker component.
    if (!dateTimePickerContainer.current.contains(activeTarget)) {
      const isDateTimeValid = DateTimeUtils.isValidDateTime(
        dateValue,
        timeValue,
        state.dateFormat,
        props.showSeconds
      );
      const enteredDateTime = isDateTimeValid ? state.dateTime : null;
      checkAmbiguousTime(enteredDateTime, () => {
        // If the entered time is ambiguous then do not handle blur just yet. It should be handled _after_
        // the ambiguity is resolved (i.e., after dismissing the Time Clarification dialog).
        if (
          !(state.isAmbiguousTime && state.isTimeClarificationOpen)
        ) {
          handleBlur(event, state.dateTime);
        }
      });
    }
  }
  handleOnTimeBlur(event) {
    // Modern browsers support event.relatedTarget but event.relatedTarget returns null in IE 10 / IE 11.
    // IE 11 sets document.activeElement to the next focused element before the blur event is called.
    const activeTarget = event.relatedTarget
      ? event.relatedTarget
      : document.activeElement;
    // Handle blur only if focus has moved out of the entire date time picker component.
    if (!dateTimePickerContainer.current.contains(activeTarget)) {
      const isDateTimeValid = DateTimeUtils.isValidDateTime(
        dateValue,
        timeValue,
        state.dateFormat,
        props.showSeconds
      );
      let updatedDateTime;
      if (isDateTimeValid) {
        updatedDateTime = DateTimeUtils.updateTime(
          state.dateTime,
          timeValue,
          props.showSeconds
        );
      }
      checkAmbiguousTime(updatedDateTime, () => {
        // If the entered time is ambiguous then do not handle blur just yet. It should be handled _after_
        // the ambiguity is resolved (i.e., after dismissing the Time Clarification dialog).
        if (
          !(state.isAmbiguousTime && state.isTimeClarificationOpen)
        ) {
          handleBlur(event, state.dateTime);
        }
      });
    }
  }
  handleBlur(event, momentDateTime) {
    if (props.onBlur) {
      const metadata = getMetadata(momentDateTime);
      props.onBlur(event, metadata);
    }
    containerHasFocus = false;
  }
  checkAmbiguousTime(dateTime, onCheckCallback) {
    // To prevent multiple time clarification dialogs from rendering, ensure that it is not open before checking for the ambiguous hour.
    // One situation is when using the right arrow key to move focus from the hour input to the minute input, it will invoke onBlur and check for ambiguous hour.
    // If the hour is ambiguous, the dialog would display and steal focus from the minute input, which again will invoke onBlur and check for ambiguous hour.
    if (state.isTimeClarificationOpen) {
      return;
    }
    let isDateTimeAmbiguous = false;
    const isOldTimeAmbiguous = state.isAmbiguousTime;
    if (dateTime && dateTime.isValid()) {
      const tempDateTime = dateTime.clone();
      isDateTimeAmbiguous = DateTimeUtils.checkAmbiguousTime(tempDateTime);
    }
    setState(
      {
        isAmbiguousTime: isDateTimeAmbiguous,
        isTimeClarificationOpen: isDateTimeAmbiguous && !isOldTimeAmbiguous
      },
      onCheckCallback
    );
  }
  handleDateChange(event, date) {
    if (event.type === "change") {
      dateValue = event.target.value;
    }
    let updatedDateTime;
    const formattedDate = DateUtil.formatISODate(date, "YYYY-MM-DD");
    const isDateValid = DateUtil.isValidDate(formattedDate, "YYYY-MM-DD");
    const isTimeValid = DateTimeUtils.isValidTime(
      timeValue,
      props.showSeconds
    );
    if (isDateValid) {
      const previousDateTime = state.dateTime
        ? state.dateTime.clone()
        : DateTimeUtils.createSafeDate(formattedDate);
      updatedDateTime = DateTimeUtils.syncDateTime(
        previousDateTime,
        date,
        timeValue,
        props.showSeconds
      );
      if (isTimeValid) {
        // Update the timeValue in case the updatedDateTime falls in the missing hour and needs to bump the hour up.
        timeValue = DateTimeUtils.getTime(
          updatedDateTime.format(),
          props.showSeconds
        );
      }
    }
    // onChange should only be triggered when both the date and time values are valid or both values are empty/cleared.
    if (
      (isDateValid && isTimeValid) ||
      (dateValue === "" && timeValue === "")
    ) {
      handleChange(event, updatedDateTime);
    } else {
      setState({ dateTime: updatedDateTime });
    }
    if (isDateValid) {
      // Allows time for focus-trap to release focus on the picker before shifting focus to the hour input.
      setTimeout(() => {
        /*
         * Make sure the reference to hourInput still exists before calling focus because it is possible that it is now
         * nullified after the 100 ms timeout due to a force remount of this component with a new `key` prop value.
         * Reference https://github.com/cerner/terra-framework/issues/1086
         */
        if (hourInput) {
          hourInput.focus();
        }
      }, 100);
    }
  }
  handleDateChangeRaw(event, date) {
    dateValue = event.target.value;
    handleChangeRaw(event, date);
  }
  handleTimeChange(event, time) {
    timeValue = time;
    const validDate =
      DateUtil.isValidDate(dateValue, state.dateFormat) &&
      isDateTimeAcceptable(
        DateTimeUtils.convertDateTimeStringToMomentObject(
          dateValue,
          timeValue,
          state.dateFormat,
          props.showSeconds
        )
      );
    const validTime = DateTimeUtils.isValidTime(
      timeValue,
      props.showSeconds
    );
    const previousDateTime = state.dateTime
      ? state.dateTime.clone()
      : null;
    // If both date and time are valid, check if the time is the missing hour and invoke onChange.
    // If the date is valid but time is invalid, the time in the dateTime state needs to be cleared and render.
    if (validDate && validTime) {
      const updatedDateTime = DateTimeUtils.updateTime(
        previousDateTime,
        time,
        props.showSeconds
      );
      if (
        event.keyCode === KeyCode.KEY_DOWN &&
        previousDateTime &&
        updatedDateTime &&
        previousDateTime.format() === updatedDateTime.format()
      ) {
        updatedDateTime.subtract(1, "hours");
      }
      let displayedTimeValue = timeValue;
      // If updatedDateTime is valid, update timeValue (value in the time input) to reflect updatedDateTime since
      // it could have subtracted an hour from above to account for the missing hour.
      if (updatedDateTime) {
        displayedTimeValue = DateTimeUtils.getTime(
          updatedDateTime.format(),
          props.showSeconds
        );
      }
      handleChangeRaw(event, displayedTimeValue);
      handleChange(event, updatedDateTime);
    } else if (dateValue === "" && timeValue === "") {
      handleChangeRaw(event, timeValue);
      handleChange(event, null);
    } else {
      if (!validDate && validTime) {
        const updatedDateTime = DateTimeUtils.updateTime(
          previousDateTime,
          time,
          props.showSeconds
        );
        setState({
          dateTime: updatedDateTime
        });
      }
      handleChangeRaw(event, time);
    }
  }
  handleChange(event, newDateTime) {
    setState({
      dateTime: newDateTime
    });
    // If the new time is ambiguous and the old time is not, do not fire onChange.
    // This allows a user to use TimeClarification before onChange is fired.
    if (
      props.onChange &&
      (state.isAmbiguousTime ||
        !DateTimeUtils.checkAmbiguousTime(newDateTime))
    ) {
      const metadata = getMetadata(newDateTime);
      props.onChange(
        event,
        newDateTime && newDateTime.isValid() ? newDateTime.format() : "",
        metadata
      );
    }
  }
  handleChangeRaw(event, value) {
    if (props.onChangeRaw) {
      const metadata = getMetadata(value);
      props.onChangeRaw(event, value, metadata);
    }
  }
  handleOnDateInputFocus(event) {
    handleOnInputFocus(event);
  }
  handleOnTimeInputFocus(event) {
    handleOnInputFocus(event);
  }
  handleOnInputFocus(event) {
    handleFocus(event);
    if (!isDefaultDateAcceptable) {
      dateValue = "";
      timeValue = "";
      handleChange(event, null);
      isDefaultDateAcceptable = true;
    }
  }
  handleFocus(event) {
    // Handle focus only if focus is gained from outside of the entire date time picker component.
    // For IE 10/11 we cannot rely on event.relatedTarget since it is always null. Need to also check if containerHasFocus is false to
    // determine if the date-time picker component did not have focus but will now gain focus.
    if (
      props.onFocus &&
      !containerHasFocus &&
      !dateTimePickerContainer.current.contains(event.relatedTarget)
    ) {
      props.onFocus(event);
      containerHasFocus = true;
    }
  }
  handleOnCalendarButtonClick(event) {
    if (!isDefaultDateAcceptable && !validateDefaultDate()) {
      dateValue = "";
      timeValue = "";
      handleChange(event, null);
    } else {
      isDefaultDateAcceptable = true;
    }
  }
  validateDefaultDate() {
    return isDateTimeAcceptable(state.dateTime);
  }
  isDateTimeAcceptable(newDateTime) {
    let isAcceptable = true;
    if (
      DateUtil.isDateOutOfRange(
        newDateTime,
        DateTimeUtils.createSafeDate(DateUtil.getMinDate(props.minDate)),
        DateTimeUtils.createSafeDate(DateUtil.getMaxDate(props.maxDate))
      )
    ) {
      isAcceptable = false;
    }
    if (DateUtil.isDateExcluded(newDateTime, props.excludeDates)) {
      isAcceptable = false;
    }
    return isAcceptable;
  }
  handleDaylightSavingButtonClick(event) {
    setState({ isTimeClarificationOpen: false });
    const newDateTime = state.dateTime.clone();
    if (!newDateTime.isDST()) {
      newDateTime.subtract(1, "hour");
      setState({
        dateTime: newDateTime
      });
      if (props.onChange) {
        const metadata = getMetadata(newDateTime);
        props.onChange(
          event,
          newDateTime && newDateTime.isValid() ? newDateTime.format() : "",
          metadata
        );
      }
    } else if (props.onChange && !wasOffsetButtonClicked) {
      // This fires onChange if the TimeClarification dialog was launched without using the OffsetButton.
      // If the user clicks the OffsetButton, onChange should have already been fired and does not need to be fired
      // again (unless they change the DateTime).
      const metadata = getMetadata(newDateTime);
      props.onChange(
        event,
        newDateTime && newDateTime.isValid() ? newDateTime.format() : "",
        metadata
      );
    }
    // When the Time Clarification dialog was launched _without_ using the Offset button, 'blur' event
    // needs to be handled appropriately upon dismissal of the dialog (i.e. after DST resolution).
    if (!wasOffsetButtonClicked) {
      handleBlur(event, newDateTime);
    }
    wasOffsetButtonClicked = false;
  }
  handleStandardTimeButtonClick(event) {
    setState({ isTimeClarificationOpen: false });
    const newDateTime = state.dateTime.clone();
    if (newDateTime.isDST()) {
      newDateTime.add(1, "hour");
      setState({
        dateTime: newDateTime
      });
      if (props.onChange) {
        const metadata = getMetadata(newDateTime);
        props.onChange(
          event,
          newDateTime && newDateTime.isValid() ? newDateTime.format() : "",
          metadata
        );
      }
    } else if (props.onChange && !wasOffsetButtonClicked) {
      // This fires onChange if the TimeClarification dialog was launched without using the OffsetButton.
      // If the user clicks the OffsetButton, onChange should have already been fired and does not need to be fired
      // again (unless they change the DateTime).
      const metadata = getMetadata(newDateTime);
      props.onChange(
        event,
        newDateTime && newDateTime.isValid() ? newDateTime.format() : "",
        metadata
      );
    }
    // When the Time Clarification dialog was launched _without_ using the Offset button, 'blur' event
    // needs to be handled appropriately upon dismissal of the dialog (i.e. after DST resolution).
    if (!wasOffsetButtonClicked) {
      handleBlur(event, newDateTime);
    }
    wasOffsetButtonClicked = false;
  }
  handleOffsetButtonClick() {
    wasOffsetButtonClicked = true;
    setState(prevState => ({
      isTimeClarificationOpen: !prevState.isTimeClarificationOpen
    }));
  }
  handleOnRequestClose() {
    setState({ isTimeClarificationOpen: false });
  }
  renderTimeClarification() {
    return (
      <TimeClarification
        ambiguousDateTime={state.dateTime.format()}
        disabled={props.disabled}
        isOpen={state.isTimeClarificationOpen}
        isOffsetButtonHidden={!state.isAmbiguousTime}
        onDaylightSavingButtonClick={handleDaylightSavingButtonClick}
        onStandardTimeButtonClick={handleStandardTimeButtonClick}
        onOffsetButtonClick={handleOffsetButtonClick}
        onRequestClose={handleOnRequestClose}
        onBlur={handleOnTimeBlur}
        onFocus={handleFocus}
      />
    );
  }
  render() {
    const {
      dateInputAttributes,
      disabled,
      excludeDates,
      filterDate,
      includeDates,
      isIncomplete,
      isInvalid,
      isInvalidMeridiem,
      onBlur,
      onChange,
      onChangeRaw,
      onClickOutside,
      onFocus,
      onSelect,
      maxDate,
      minDate,
      name,
      required,
      showSeconds,
      timeInputAttributes,
      value,
      timeVariant,
      ...customProps
    } = props;
    const dateTime = state.dateTime ? state.dateTime.clone() : null;
    const dateValue = DateUtil.formatMomentDate(dateTime, "YYYY-MM-DD");
    const theme = context;
    return (
      <div
        {...customProps}
        className={cx("date-time-picker", theme.className)}
        ref={dateTimePickerContainer}
      >
        <input
          // Create a hidden input for storing the name and value attributes to use when submitting the form.
          // The data stored in the value attribute will be the visible date in the date input but in ISO 8601 format.
          data-terra-date-time-input-hidden
          type="hidden"
          name={name}
          value={dateTime && dateTime.isValid() ? dateTime.format() : ""}
        />

        <div className={cx("date-facade")}>
          <DatePicker
            onCalendarButtonClick={handleOnCalendarButtonClick}
            onChange={handleDateChange}
            onChangeRaw={handleDateChangeRaw}
            onSelect={handleOnSelect}
            onClickOutside={onClickOutside}
            onBlur={handleOnDateBlur}
            onFocus={handleOnDateInputFocus}
            excludeDates={excludeDates}
            filterDate={filterDate}
            includeDates={includeDates}
            inputAttributes={dateInputAttributes}
            maxDate={DateUtil.getMaxDate(maxDate)}
            minDate={DateUtil.getMinDate(minDate)}
            selectedDate={dateValue}
            name="input"
            disabled={disabled}
            disableButtonFocusOnClose
            isIncomplete={isIncomplete}
            isInvalid={isInvalid}
            required={required}
          />
        </div>
        <div className={cx("time-facade")}>
          <TimeInput
            onBlur={handleOnTimeBlur}
            onChange={handleTimeChange}
            onFocus={handleOnTimeInputFocus}
            inputAttributes={timeInputAttributes}
            name="input"
            value={timeValue}
            disabled={disabled}
            variant={timeVariant}
            ref={inputRef => {
              hourInput = inputRef;
            }}
            showSeconds={showSeconds}
            isIncomplete={isIncomplete}
            isInvalid={isInvalid}
            isInvalidMeridiem={isInvalidMeridiem}
            required={required}
          />

          {state.isAmbiguousTime ? renderTimeClarification() : null}
        </div>
      </div>
    );
  }
}

export default injectIntl(DateTimePicker);
