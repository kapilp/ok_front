import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import AbstractModal from 'terra-abstract-modal';
import Button from 'terra-button';
import { injectIntl, intlShape } from 'react-intl';
import DateTimeUtils from './DateTimeUtils';
import styles from './_TimeClarification.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  /**
   * An ISO 8601 date time with the ambiguous hour.
   */
  ambiguousDateTime: string;
  /**
   * @private
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  intl: intlShape.isRequired;
  /**
   * If set to true, the modal will rendered as opened
   */
  isOpen: boolean.isRequired;
  /**
   * If set to true, the button to open the modal will be hidden.
   */
  isOffsetButtonHidden: boolean.isRequired;
  /**
   * A callback function triggered when the timezone offset button loses focus.
   */
  onBlur: PropTypes.func;
  /**
   * Callback function indicating the before time change option was selected.
   */
  onDaylightSavingButtonClick: PropTypes.func.isRequired;
  /**
   * A callback function triggered when the timezone offset button gains focus.
   */
  onFocus: PropTypes.func;
  /**
   * Callback function indicating the after time change option was selected.
   */
  onStandardTimeButtonClick: PropTypes.func.isRequired;
  /**
   * Callback function indicating the DST offset button was selected.
   */
  onOffsetButtonClick: PropTypes.func.isRequired;
  /**
   * Callback function indicating the modal is requesting to close.
   */
  onRequestClose: PropTypes.func.isRequired;
  /**
   * Whether the clarification is disabled.
   */
  disabled: boolean;
}
const defaultProps = {
  disabled: false,
};
interface ITimeClarificationProps extends JSX.HTMLAttributes<Element> {
  intl?: any;
  disabled?: any;
  onOffsetButtonClick?: any;
  onFocus?: any;
  onBlur?: any;
  onRequestClose?: any;
  isOpen?: any;
  isOffsetButtonHidden?: any;
  onStandardTimeButtonClick?: any;
  onDaylightSavingButtonClick?: any;
}
type TimeClarificationState = {
  offsetDisplay?: string;
  offsetLongDisplay?: string;
};
class TimeClarification extends React.Component<ITimeClarificationProps, TimeClarificationState> {
  constructor(props) {
    super(props);
    state = {
      offsetDisplay: '',
      offsetLongDisplay: '',
    };
    handleDaylightSavingButtonClick = handleDaylightSavingButtonClick.bind(this);
    handleStandardTimeButtonClick = handleStandardTimeButtonClick.bind(this);
  }
  handleDaylightSavingButtonClick(event) {
    setState({
      offsetDisplay: DateTimeUtils.getDaylightSavingTZDisplay(props.ambiguousDateTime),
      offsetLongDisplay: DateTimeUtils.getDaylightSavingExpandedTZDisplay(props.ambiguousDateTime),
    });
    if (props.onDaylightSavingButtonClick) {
      props.onDaylightSavingButtonClick(event);
    }
  }
  handleStandardTimeButtonClick(event) {
    setState({
      offsetDisplay: DateTimeUtils.getStandardTZDisplay(props.ambiguousDateTime),
      offsetLongDisplay: DateTimeUtils.getStandardExpandedTZDisplay(props.ambiguousDateTime),
    });
    if (props.onStandardTimeButtonClick) {
      props.onStandardTimeButtonClick(event);
    }
  }
  render() {
    if (props.isOffsetButtonHidden) {
      state.offsetDisplay = '';
    }
    const offsetButtonClassNames = cx([
      'button-offset',
      {
        'button-offset-hidden': props.isOffsetButtonHidden || !state.offsetDisplay,
      },
    ]);
    const { intl } = props;
    const title = intl.formatMessage({
      id: 'Terra.dateTimePicker.timeClarification.title',
    });
    const message = intl.formatMessage({
      id: 'Terra.dateTimePicker.timeClarification.message',
    });
    const daylightSavingButtonLabel = intl.formatMessage({
      id: 'Terra.dateTimePicker.timeClarification.button.daylightSaving',
    });
    const standardTimeButtonLabel = intl.formatMessage({
      id: 'Terra.dateTimePicker.timeClarification.button.standardTime',
    });
    return (
      <>
        <AbstractModal
          classNameModal={cx('time-clarification')}
          ariaLabel="Time Clarification"
          isOpen={props.isOpen}
          onRequestClose={props.onRequestClose}
          closeOnEsc={false}
          closeOnOutsideClick={false}
          zIndex="9000"
        >
          <div>
            <header className={cx('header')}>
              <h1 className={cx('title')}>{title}</h1>
            </header>
            <div className={cx('body')}>
              <p>{message}</p>
            </div>
            <div className={cx('buttons')}>
              <Button text={daylightSavingButtonLabel} onClick={handleDaylightSavingButtonClick} variant={Button.Opts.Variants.EMPHASIS} className={cx('button-daylight')} />
              <Button text={standardTimeButtonLabel} onClick={handleStandardTimeButtonClick} variant={Button.Opts.Variants.EMPHASIS} className={cx('button-standard')} />
            </div>
          </div>
        </AbstractModal>
        <div className={cx('offset-button-container')}>
          <Button
            title={state.offsetLongDisplay}
            aria-label={state.offsetLongDisplay}
            className={offsetButtonClassNames}
            onBlur={props.onBlur}
            onFocus={props.onFocus}
            onClick={props.onOffsetButtonClick}
            text={state.offsetDisplay}
            isCompact
            isDisabled={props.disabled}
          />
        </div>
      </>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default injectIntl(TimeClarification);
