import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from 'terra-date-time-picker/lib/DateTimePicker';
import DateTimeUtils from '../../DateTimeUtils';
interface Properties {
  /**
   * The current entered date time. Use for the selected date message.
   */
  value: JSX.Element;
}
const defaultProps = {
  value: '',
};
type DatePickerExampleState = {
  dateTime?: any;
};
class DatePickerExample extends React.Component<{}, DatePickerExampleState> {
  constructor(props) {
    super(props);
    let dateTimeDisplay = props.value;
    const dateTime = DateTimeUtils.createSafeDate(dateTimeDisplay);
    if (dateTime && dateTime.isValid()) {
      dateTimeDisplay = dateTime.format();
    }
    state = { dateTime: dateTimeDisplay };
    handleDateTimeChange = handleDateTimeChange.bind(this);
  }
  handleDateTimeChange(event, dateTime) {
    setState({ dateTime });
  }
  render() {
    return (
      <div>
        <p>
          Selected ISO Date Time:
          <span id="date-time-value">{state.dateTime}</span>
        </p>
        <DateTimePicker name="date-time-picker-example" onChange={handleDateTimeChange} {...props} />
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
export default DatePickerExample;
