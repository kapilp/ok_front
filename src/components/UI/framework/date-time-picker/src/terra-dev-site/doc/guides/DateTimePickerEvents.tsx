import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from '../../../DateTimePicker';
import DateTimeUtils from '../../../DateTimeUtils';
type DateTimePickerEventsState = {
  blurTriggerCount?: number;
  focusTriggerCount?: number;
  changeValue?: string;
  changeRawValue?: string;
  selectValue?: string;
  clickOutsideTriggerCount?: number;
};
class DateTimePickerEvents extends React.Component<{}, DateTimePickerEventsState> {
  blurCount: number;
  clickOutsideCount: number;
  focusCount: number;
  constructor(props) {
    super(props);
    state = {
      blurTriggerCount: 0,
      focusTriggerCount: 0,
      changeValue: '',
      changeRawValue: '',
      selectValue: '',
      clickOutsideTriggerCount: 0,
    };
    handleBlur = handleBlur.bind(this);
    handleFocus = handleFocus.bind(this);
    handleChange = handleChange.bind(this);
    handleChangeRaw = handleChangeRaw.bind(this);
    handleSelect = handleSelect.bind(this);
    handleOnClickOutside = handleOnClickOutside.bind(this);
    blurCount = 0;
    focusCount = 0;
    clickOutsideCount = 0;
  }
  handleBlur() {
    blurCount += 1;
    setState({ blurTriggerCount: blurCount });
  }
  handleFocus() {
    focusCount += 1;
    setState({ focusTriggerCount: focusCount });
  }
  handleChange(event, value) {
    setState({ changeValue: value });
  }
  handleChangeRaw(event, vaue) {
    setState({ changeRawValue: vaue });
  }
  handleSelect(event, value) {
    setState({ selectValue: value });
  }
  handleOnClickOutside() {
    clickOutsideCount += 1;
    setState({ clickOutsideTriggerCount: clickOutsideCount });
  }
  render() {
    return (
      <div>
        <p>
          onFocus Trigger Count: <span id="onFocus-count">{state.focusTriggerCount}</span>
          <br />
          <br />
          onBlur Trigger Count: <span id="onBlur-count">{state.blurTriggerCount}</span>
          <br />
          <br />
          onClickOutside Trigger Count: <span id="onClickOutside-count">{state.clickOutsideTriggerCount}</span>
          <br />
          <br />
          onChangeRaw Triggered: <span id="onChangeRaw-count">{state.changeRawValue}</span>
          <br />
          <br />
          onChange Triggered: <span id="onChange-Count">{state.changeValue}</span>
          <br />
          <br />
          onSelect Triggered: <span id="onSelect-count">{state.selectValue}</span>
        </p>
        <DateTimePicker
          name="date-time-picker-events"
          onBlur={handleBlur}
          onFocus={handleFocus}
          onChange={handleChange}
          onChangeRaw={handleChangeRaw}
          onSelect={handleSelect}
          onClickOutside={handleOnClickOutside}
          timeVariant={DateTimeUtils.FORMAT_12_HOUR}
          showSeconds
        />
      </div>
    );
  }
}
export default DateTimePickerEvents;
