import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import moment from 'moment';
import Field from 'terra-form-field';
import DateTimePicker from 'terra-date-time-picker';
import DateTimeUtil from 'terra-date-time-picker/lib/DateTimeUtils';
interface Properties {
  /**
   * The current entered date time. Use for the selected date message.
   */
  value: JSX.Element;
}
const defaultProps = {
  value: '',
};
type DateTimePickerExampleState = {
  dateTime?: any;
};
class DateTimePickerExample extends React.Component<{}, DateTimePickerExampleState> {
  constructor(props) {
    super(props);
    state = { dateTime: props.value };
    handleDateTimeChange = handleDateTimeChange.bind(this);
  }
  handleDateTimeChange(event, dateTime) {
    setState({ dateTime });
  }
  render() {
    return (
      <div>
        <p>
          Selected ISO Date Time:
          {state.dateTime}
        </p>
        <Field label="Enter Date/Time" htmlFor="timeVariant">
          <DateTimePicker name="date-time-picker-example" dateInputAttributes={{ id: 'timeVariant' }} onChange={handleDateTimeChange} {...props} />
        </Field>
      </div>
    );
  }
}
props = mergeProps({}, defaultProps, props);
const DateTimePickerTimeAttributes = () => <DateTimePickerExample value={moment().format()} timeVariant={DateTimeUtil.FORMAT_12_HOUR} />;
export default DateTimePickerTimeAttributes;
