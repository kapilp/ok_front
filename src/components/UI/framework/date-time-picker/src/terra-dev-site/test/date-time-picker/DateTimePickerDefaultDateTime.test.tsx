import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../common/DateTimePickerExampleTemplate';
const DateTimePickerExample = () => <DateTimePickerExampleTemplate value="2017-08-08T10:30:00" />;
export default DateTimePickerExample;
