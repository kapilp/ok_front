import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from '../../../DateTimePicker';
type DatePickerOnChangeState = {
  onchangedate?: string;
  onselectdate?: string;
};
class DatePickerOnChange extends React.Component<{}, DatePickerOnChangeState> {
  constructor(props) {
    super(props);
    state = { onchangedate: '', onselectdate: '' };
    handleDateChange = handleDateChange.bind(this);
    handleDateSelect = handleDateSelect.bind(this);
  }
  handleDateChange(event, onchangedate) {
    setState({ onchangedate });
  }
  handleDateSelect(event, onselectdate) {
    setState({ onselectdate });
  }
  render() {
    return (
      <div>
        <h3>
          OnChange Date:
          <span id="changed-date">{state.onchangedate}</span>
        </h3>
        <h3>
          OnSelect Date:
          <span id="selected-date">{state.onselectdate}</span>
        </h3>
        <DateTimePicker name="date-time-picker-onchange" onChange={handleDateChange} onSelect={handleDateSelect} value="2017-04-01T12:00" />
      </div>
    );
  }
}
export default DatePickerOnChange;
