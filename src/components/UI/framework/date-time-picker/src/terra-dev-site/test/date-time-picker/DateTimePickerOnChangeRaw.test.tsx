import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from '../../../DateTimePicker';
type DatePickerOnChangeState = {
  date?: string;
  iSO?: string;
  inputValue?: string;
  dateValue?: string;
  timeValue?: string;
  isAmbiguousHour?: string;
  isCompleteValue?: string;
  isValidValue?: string;
};
class DatePickerOnChange extends React.Component<{}, DatePickerOnChangeState> {
  constructor(props) {
    super(props);
    state = {
      date: '',
      iSO: '',
      inputValue: '',
      dateValue: '',
      timeValue: '',
      isAmbiguousHour: 'No',
      isCompleteValue: 'No',
      isValidValue: 'Yes',
    };
    handleDateChangeRaw = handleDateChangeRaw.bind(this);
  }
  handleDateChangeRaw(event, date, options) {
    setState({
      date,
      iSO: options.iSO,
      inputValue: options.inputValue,
      dateValue: options.dateValue,
      timeValue: options.timeValue,
      isAmbiguousHour: options.isAmbiguousHour ? 'Yes' : 'No',
      isCompleteValue: options.isCompleteValue ? 'Yes' : 'No',
      isValidValue: options.isValidValue ? 'Yes' : 'No',
    });
  }
  render() {
    return (
      <div>
        <h3>
          Selected Date:
          <span id="selected-date">{state.date}</span>
          <br />
          <br />
          ISO String: <span id="iso">{state.iSO}</span>
          <br />
          <br />
          Input Value: <span id="input-value">{state.inputValue}</span>
          <br />
          <br />
          Date Value: <span id="date-value">{state.dateValue}</span>
          <br />
          <br />
          Time Value: <span id="time-value">{state.timeValue}</span>
          <br />
          <br />
          Is Ambiguous? <span id="ambiguous-date">{state.isAmbiguousHour}</span>
          <br />
          <br />
          Is Date-Time Complete? <span id="complete-date">{state.isCompleteValue}</span>
          <br />
          <br />
          Is Date-Time Valid? <span id="valid-date">{state.isValidValue}</span>
        </h3>
        <DateTimePicker name="date-time-picker-onchangeraw" onChangeRaw={handleDateChangeRaw} />
      </div>
    );
  }
}
export default DatePickerOnChange;
