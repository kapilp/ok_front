import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from '../../../DateTimePicker';
type DateTimePickerOnBlurState = {
  blurTriggerCount?: number;
  iSO?: string;
  isAmbiguousHour?: string;
};
class DateTimePickerOnBlur extends React.Component<{}, DateTimePickerOnBlurState> {
  blurCount: number;
  constructor(props) {
    super(props);
    state = {
      blurTriggerCount: 0,
      iSO: '',
      isAmbiguousHour: 'No',
    };
    handleBlur = handleBlur.bind(this);
    blurCount = 0;
  }
  handleBlur(event, options) {
    blurCount += 1;
    setState({
      blurTriggerCount: blurCount,
      iSO: options.iSO,
      isAmbiguousHour: options.isAmbiguousHour ? 'Yes' : 'No',
    });
  }
  render() {
    return (
      <div>
        <h3>
          onBlur Trigger Count: <span id="blur-count">{state.blurTriggerCount}</span>
          <br />
          <br />
          Selected ISO Date Time: <span id="iso">{state.iSO}</span>
          <br />
          <br />
          Is Ambiguous? <span id="ambiguous-date">{state.isAmbiguousHour}</span>
          <br />
          <br />
        </h3>
        <DateTimePicker name="date-time-picker-onblur-dst" value="2017-11-05T01:30:00" onBlur={handleBlur} />
      </div>
    );
  }
}
export default DateTimePickerOnBlur;
