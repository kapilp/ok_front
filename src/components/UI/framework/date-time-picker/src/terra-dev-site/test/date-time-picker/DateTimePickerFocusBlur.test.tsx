import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePicker from '../../../DateTimePicker';
type DateTimePickerOnBlurState = {
  date?: string;
  blurTriggerCount?: number;
  focusTriggerCount?: number;
  iSO?: string;
  inputValue?: string;
  dateValue?: string;
  timeValue?: string;
  isAmbiguousHour?: string;
  isCompleteValue?: string;
  isValidValue?: string;
};
class DateTimePickerOnBlur extends React.Component<{}, DateTimePickerOnBlurState> {
  blurCount: number;
  focusCount: number;
  constructor(props) {
    super(props);
    state = {
      date: '',
      blurTriggerCount: 0,
      focusTriggerCount: 0,
      iSO: '',
      inputValue: '',
      dateValue: '',
      timeValue: '',
      isAmbiguousHour: 'No',
      isCompleteValue: 'No',
      isValidValue: 'Yes',
    };
    handleBlur = handleBlur.bind(this);
    handleFocus = handleFocus.bind(this);
    handleDateChange = handleDateChange.bind(this);
    blurCount = 0;
    focusCount = 0;
  }
  handleBlur(event, options) {
    blurCount += 1;
    setState({
      blurTriggerCount: blurCount,
      iSO: options.iSO,
      inputValue: options.inputValue,
      dateValue: options.dateValue,
      timeValue: options.timeValue,
      isAmbiguousHour: options.isAmbiguousHour ? 'Yes' : 'No',
      isCompleteValue: options.isCompleteValue ? 'Yes' : 'No',
      isValidValue: options.isValidValue ? 'Yes' : 'No',
    });
  }
  handleFocus() {
    focusCount += 1;
    setState({ focusTriggerCount: focusCount });
  }
  handleDateChange(event, date) {
    setState({ date });
  }
  render() {
    return (
      <div>
        <h3>
          onBlur Trigger Count: <span id="blur-count">{state.blurTriggerCount}</span>
          <br />
          <br />
          onFocus Trigger Count: <span id="focus-count">{state.focusTriggerCount}</span>
          <br />
          <br />
          Selected Date: <span id="selected-date">{state.date}</span>
          <br />
          <br />
          ISO String: <span id="iso">{state.iSO}</span>
          <br />
          <br />
          Input Value: <span id="input-value">{state.inputValue}</span>
          <br />
          <br />
          Date Value: <span id="date-value">{state.dateValue}</span>
          <br />
          <br />
          Time Value: <span id="time-value">{state.timeValue}</span>
          <br />
          <br />
          Is Ambiguous? <span id="ambiguous-date">{state.isAmbiguousHour}</span>
          <br />
          <br />
          Is Date-Time Complete? <span id="complete-date">{state.isCompleteValue}</span>
          <br />
          <br />
          Is Date-Time Valid? <span id="valid-date">{state.isValidValue}</span>
        </h3>
        <DateTimePicker name="date-time-picker-onblur" onBlur={handleBlur} onFocus={handleFocus} onChange={handleDateChange} excludeDates={['2019-04-01', '2019-04-02']} />
      </div>
    );
  }
}
export default DateTimePickerOnBlur;
