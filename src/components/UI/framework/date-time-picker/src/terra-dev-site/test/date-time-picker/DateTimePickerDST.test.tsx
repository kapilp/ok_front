import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../common/DateTimePickerExampleTemplate';
const DateTimePickerExample = () => <DateTimePickerExampleTemplate value="2017-11-05T01:30:00" />;
export default DateTimePickerExample;
