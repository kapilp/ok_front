import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from 'terra-button';
import DateTimePickerExampleTemplate from '../../common/DateTimePickerExampleTemplate';
type undefinedState = {
  active?: boolean;
};
export default class extends React.Component<{}, undefinedState> {
  constructor(props) {
    super(props);
    state = { active: true };
    toggleDateTimePicker = toggleDateTimePicker.bind(this);
  }
  toggleDateTimePicker() {
    setState(prevState => ({
      active: !prevState.active,
    }));
  }
  render() {
    return (
      <div>
        <Button id="date-time-picker-toggler" text={state.active ? 'Disable' : 'Enable'} onClick={toggleDateTimePicker} />
        <DateTimePickerExampleTemplate value="2017-11-05T01:30:00" disabled={!state.active} />
      </div>
    );
  }
}
