import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../common/DateTimePickerExampleTemplate';
const DateTimePickerExample = () => <DateTimePickerExampleTemplate isIncomplete required />;
export default DateTimePickerExample;
