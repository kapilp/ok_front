import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../../common/DateTimePickerExampleTemplate';
import DateTimeUtil from '../../../../DateTimeUtils';
export default class DateTimePickerEmpty12HourMobile extends React.Component<{}, {}> {
  resetontouchstart: any;
  constructor(props) {
    super(props);
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentDidMount() {
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentWillUnmount() {
    if (resetontouchstart) {
      delete window.ontouchstart;
    }
  }
  render() {
    return (
      <>
        <h3>Empty Date-Time-Picker</h3>
        <DateTimePickerExampleTemplate id="datetimeEmpty" timeVariant={DateTimeUtil.FORMAT_12_HOUR} />
      </>
    );
  }
}
