import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../../common/DateTimePickerExampleTemplate';
import DateTimeUtil from '../../../../DateTimeUtils';
type DateTimePicker12HourDisabledMobileState = {
  datetime?: string;
};
export default class DateTimePicker12HourDisabledMobile extends React.Component<{}, DateTimePicker12HourDisabledMobileState> {
  resetontouchstart: any;
  constructor(props) {
    super(props);
    state = { datetime: '2019-04-10T15:23:00-05:00' };
    handleOnChange = handleOnChange.bind(this);
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentDidMount() {
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentWillUnmount() {
    if (resetontouchstart) {
      delete window.ontouchstart;
    }
  }
  handleOnChange(event, datetime) {
    setState({ datetime });
  }
  render() {
    return (
      <>
        <h3 id="titleWithDateTimeValue">Disabled Date-Time-Picker</h3>
        <DateTimePickerExampleTemplate id="disabledDatetimeValueProvided" disabled value={state.datetime} timeVariant={DateTimeUtil.FORMAT_12_HOUR} />
      </>
    );
  }
}
