import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../../common/DateTimePickerExampleTemplate';
import DateTimeUtil from '../../../../DateTimeUtils';
type DateTimePickerValue12HourMobileState = {
  datetime?: string;
};
export default class DateTimePickerValue12HourMobile extends React.Component<{}, DateTimePickerValue12HourMobileState> {
  resetontouchstart: any;
  constructor(props) {
    super(props);
    state = { datetime: '2019-04-10T15:23:00-05:00' };
    handleOnChange = handleOnChange.bind(this);
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentDidMount() {
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = 'true';
    }
  }
  componentWillUnmount() {
    if (resetontouchstart) {
      delete window.ontouchstart;
    }
  }
  handleOnChange(event, datetime) {
    setState({ datetime });
  }
  render() {
    return (
      <>
        <h3 id="titleWithDateTimeValue">
          Date-Time-Picker with value provided - Current Value:&nbsp;
          {state.datetime}
        </h3>
        <DateTimePickerExampleTemplate id="datetimeValueProvided" value={state.datetime} timeVariant={DateTimeUtil.FORMAT_12_HOUR} />
      </>
    );
  }
}
