import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import DateTimePickerExampleTemplate from '../../common/DateTimePickerExampleTemplate';
const DateTimePickerExample = () => <DateTimePickerExampleTemplate includeDates={['2017-08-10']} value="2017-08-10" />;
export default DateTimePickerExample;
