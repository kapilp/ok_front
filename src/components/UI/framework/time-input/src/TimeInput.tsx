import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames";
import classNamesBind from "classnames/bind";
import {ThemeContext} from "../../../framework/theme-context/src/ThemeContext";
import Input from "terra-form-input";
import ButtonGroup from "terra-button-group";
import { injectIntl, intlShape } from "react-intl";
import * as KeyCode from "keycode-js";
import TimeUtil from "./TimeUtil";
import styles from "./TimeInput.module.scss";
const cx = classNamesBind.bind(styles);
interface Properties {
  /**
   * Whether the time input should be disabled.
   */
  disabled: boolean,
  /**
   * Custom input attributes that apply to the hour, minute, and second inputs.
   */
  inputAttributes: PropTypes.object,
  /**
   * Custom input attributes to apply to the hour input
   */
  hourAttributes: PropTypes.object,
  /**
   * @private
   * intl object programmatically imported through injectIntl from react-intl.
   * */
  intl: intlShape.isRequired,
  /**
   * Whether the input displays as Incomplete. Use when no value has been provided. _(usage note: `required` must also be set)_.
   */
  isIncomplete: boolean,
  /**
   * Whether the input displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalid: boolean,
  /**
   * Whether the selected meridiem displays as Invalid. Use when value does not meet validation pattern.
   */
  isInvalidMeridiem: boolean,
  /**
   * Custom input attributes to apply to the minutes input
   */
  minuteAttributes: PropTypes.object,
  /**
   * Name of the time input. The name should be unique.
   */
  name: string.isRequired,
  /**
   * A callback function to execute when the entire time input component loses focus.
   * This event does not get triggered when the focus is moved from the hour input to the minute input or meridiem because the focus is still within the main time input component.
   */
  onBlur: PropTypes.func,
  /**
   * A callback function to execute when either the hour or minute has been changed.
   * The first parameter is the event. The second parameter is the changed time value.
   */
  onChange: PropTypes.func,
  /**
   * A callback function triggered when the hour input or minute input receives focus.
   */
  onFocus: PropTypes.func,
  /**
   * Callback ref to pass into the input dom element.
   */
  ref: PropTypes.func,
  /**
   * Whether or not the time is required.
   */
  required: boolean,
  /**
   * Custom input attributes to apply to the seconds input
   */
  secondAttributes: PropTypes.object,
  /**
   * Whether the input for seconds should be displayed or not. If true then the second field must have a valid
   * number for the overall time to be considered valid.
   */
  showSeconds: boolean,
  /**
   * An ISO 8601 string representation of the time value in the input.
   */
  value: string,
  /**
   * Type of time input to initialize. Must be `24-hour` or `12-hour`.
   * The `de`, `es-ES`, `fr-FR`, `fr`, `nl-BE`, `nl`, `pt-BR`, `pt`, `sv-SE` and `sv` locales do not use the 12-hour time notation.
   * If the `variant` prop if set to `12-hour` for one of these supported locales, the variant will be ignored and defaults to `24-hour`.
   */
  variant: PropTypes.oneOf([TimeUtil.FORMAT_12_HOUR, TimeUtil.FORMAT_24_HOUR])
};
const defaultProps = {
  disabled: false,
  inputAttributes: {},
  isIncomplete: false,
  isInvalid: false,
  isInvalidMeridiem: false,
  minuteAttributes: {},
  hourAttributes: {},
  onBlur: null,
  onChange: null,
  onFocus: undefined,
  ref: undefined,
  required: false,
  secondAttributes: {},
  showSeconds: false,
  value: undefined,
  variant: TimeUtil.FORMAT_24_HOUR
};
interface ITimeInputProps extends JSX.HTMLAttributes<Element> {
  disabled?: any;
  inputAttributes?: any;
  minuteAttributes?: any;
  hourAttributes?: any;
  intl?: any;
  isIncomplete?: any;
  isInvalid?: any;
  isInvalidMeridiem?: any;
  onBlur?: any;
  onChange?: any;
  onFocus?: any;
  name?: any;
  ref?: any;
  required?: any;
  secondAttributes?: any;
  showSeconds?: any;
  value?: any;
  variant?: any;
  customProps?: any;
  formatMessage?: any;
}
type TimeInputState = {
  hour?: any,
  minute?: any,
  second?: any,
  isFocused?: boolean,
  meridiem?: any,
  hourInitialFocused?: boolean,
  minuteInitialFocused?: boolean,
  secondInitialFocused?: boolean,
  length?: any,
  toString?: any
};
class TimeInput extends React.Component<ITimeInputProps, TimeInputState> {
  props = mergeProps({}, defaultProps, props)
  const theme = useContext(ThemeContext);
  anteMeridiem: any;
  context: any;
  hourInput: any;
  minuteInput: any;
  postMeridiem: any;
  secondInput: any;
  timeInputContainer: any;
  constructor(props) {
    super(props);
    let { value } = props;
    const { showSeconds } = props;
    if (value && !TimeUtil.validateTime(value, showSeconds)) {
      if (process.env !== "production") {
        // eslint-disable-next-line no-console
        console.warn(
          `An invalid time value, ${value}, has been passed to the terra-time-picker. ` +
            "This value has been ignored and will not be rendered. " +
            `Time values must be in ${
              showSeconds ? "hh:mm:ss" : "hh:mm"
            } format because showSeconds is ${showSeconds}.`
        );
      }
      value = undefined;
    }
    timeInputContainer = React.createRef();
    handleHourChange = handleHourChange.bind(this);
    handleMinuteChange = handleMinuteChange.bind(this);
    handleSecondChange = handleSecondChange.bind(this);
    handleHourInputKeyDown = handleHourInputKeyDown.bind(this);
    handleMinuteInputKeyDown = handleMinuteInputKeyDown.bind(this);
    handleSecondInputKeyDown = handleSecondInputKeyDown.bind(this);
    handleFocus = handleFocus.bind(this);
    handleHourFocus = handleHourFocus.bind(this);
    handleMinuteFocus = handleMinuteFocus.bind(this);
    handleSecondFocus = handleSecondFocus.bind(this);
    handleHourBlur = handleHourBlur.bind(this);
    handleMinuteBlur = handleMinuteBlur.bind(this);
    handleSecondBlur = handleSecondBlur.bind(this);
    handleMeridiemButtonFocus = handleMeridiemButtonFocus.bind(this);
    handleMeridiemButtonBlur = handleMeridiemButtonBlur.bind(this);
    handleMeridiemButtonChange = handleMeridiemButtonChange.bind(
      this
    );
    let hour = TimeUtil.splitHour(value);
    let meridiem;
    if (TimeUtil.getVariantFromLocale(props) === TimeUtil.FORMAT_12_HOUR) {
      if (
        !props.intl.messages["Terra.timeInput.am"] ||
        !props.intl.messages["Terra.timeInput.pm"]
      ) {
        if (process.env !== "production") {
          // eslint-disable-next-line no-console
          console.warn(
            "This locale only uses 24 hour clock. The ante meridiem and post meridiem will not be displayed"
          );
        }
        anteMeridiem = "";
        postMeridiem = "";
      } else {
        anteMeridiem = props.intl.formatMessage({
          id: "Terra.timeInput.am"
        });
        postMeridiem = props.intl.formatMessage({
          id: "Terra.timeInput.pm"
        });
      }
      if (hour) {
        const parsedHour = TimeUtil.parseTwelveHourTime(
          hour,
          anteMeridiem,
          postMeridiem
        );
        hour = parsedHour.hourString;
        meridiem = parsedHour.meridiem;
      } else {
        meridiem = anteMeridiem;
      }
    }
    state = {
      hour,
      minute: TimeUtil.splitMinute(value),
      second: TimeUtil.splitSecond(value),
      isFocused: false,
      meridiem,
      hourInitialFocused: false,
      minuteInitialFocused: false,
      secondInitialFocused: false
    };
  }
  componentDidUpdate(prevProps) {
    const variant = TimeUtil.getVariantFromLocale(props);
    if (
      props.value === prevProps.value &&
      variant === TimeUtil.getVariantFromLocale(prevProps)
    ) {
      return;
    }
    let hour = TimeUtil.splitHour(props.value);
    let { meridiem } = state;
    if (variant === TimeUtil.FORMAT_12_HOUR) {
      anteMeridiem = props.intl.formatMessage({
        id: "Terra.timeInput.am"
      });
      postMeridiem = props.intl.formatMessage({
        id: "Terra.timeInput.pm"
      });
      if (hour) {
        const parsedHour = TimeUtil.parseTwelveHourTime(
          hour,
          anteMeridiem,
          postMeridiem
        );
        hour = parsedHour.hourString;
        meridiem = parsedHour.meridiem;
      }
    }
    // eslint-disable-next-line react/no-did-update-set-state
    setState({
      hour,
      minute: TimeUtil.splitMinute(props.value),
      second: TimeUtil.splitSecond(props.value),
      meridiem
    });
  }
  handleFocus(event) {
    if (
      props.onFocus &&
      !timeInputContainer.current.contains(event.relatedTarget)
    ) {
      props.onFocus(event);
    }
    setState({ isFocused: true });
  }
  handleSecondFocus(event) {
    handleFocus(event);
    setState({ secondInitialFocused: true });
    // This check is _needed_ to avoid the contextual menu on mobile devices coming up every time the focus shifts.
    if (!TimeUtil.isConsideredMobileDevice()) {
      secondInput.setSelectionRange(0, secondInput.value.length);
    }
  }
  handleMinuteFocus(event) {
    handleFocus(event);
    setState({ minuteInitialFocused: true });
    // This check is _needed_ to avoid the contextual menu on mobile device coming up every time the focus shifts.
    if (!TimeUtil.isConsideredMobileDevice()) {
      minuteInput.setSelectionRange(0, minuteInput.value.length);
    }
  }
  handleHourFocus(event) {
    handleFocus(event);
    setState({ hourInitialFocused: true });
    // This check is _needed_ to avoid the contextual menu on mobile device coming up every time the focus shifts.
    if (!TimeUtil.isConsideredMobileDevice()) {
      hourInput.setSelectionRange(0, hourInput.value.length);
    }
  }
  handleHourBlur(event) {
    handleBlur(event, TimeUtil.inputType.HOUR);
    setState({ hourInitialFocused: false });
  }
  handleMinuteBlur(event) {
    handleBlur(event, TimeUtil.inputType.MINUTE);
    setState({ minuteInitialFocused: false });
  }
  handleSecondBlur(event) {
    handleBlur(event, TimeUtil.inputType.SECOND);
    setState({ secondInitialFocused: false });
  }
  handleMeridiemButtonBlur(event) {
    handleBlur(event, TimeUtil.inputType.MERIDIEM);
  }
  handleBlur(event, type) {
    setState({ isFocused: false });
    if (
      type === TimeUtil.inputType.HOUR ||
      type === TimeUtil.inputType.MINUTE ||
      type === TimeUtil.inputType.SECOND
    ) {
      let stateValue = event.target.value;
      // Prepend a 0 to the value when losing focus and the value is single digit.
      if (stateValue.length === 1) {
        if (
          TimeUtil.getVariantFromLocale(props) ===
            TimeUtil.FORMAT_12_HOUR &&
          type === TimeUtil.inputType.HOUR &&
          stateValue === "0"
        ) {
          stateValue = "12";
        } else {
          stateValue = "0".concat(stateValue);
        }
        handleValueChange(event, type, stateValue, state.meridiem);
      }
    }
    if (props.onBlur) {
      // Modern browsers support event.relatedTarget but event.relatedTarget returns null in IE 10 / IE 11.
      // IE 11 sets document.activeElement to the next focused element before the blur event is called.
      const activeTarget = event.relatedTarget
        ? event.relatedTarget
        : document.activeElement;
      // Handle blur only if focus has moved out of the entire time input component.
      if (!timeInputContainer.current.contains(activeTarget)) {
        props.onBlur(event);
      }
    }
  }
  handleHourChange(event) {
    if (!TimeUtil.validNumericInput(event.target.value)) {
      return;
    }
    let inputValue = event.target.value;
    const stateValue = state.hour;
    const variant = TimeUtil.getVariantFromLocale(props);
    const maxValue = variant === TimeUtil.FORMAT_12_HOUR ? 12 : 23;
    // Ignore the entry if the value did not change or it is invalid.
    // When 'Predictive text' is enabled on Android the maxLength attribute on the input is ignored so we have to
    // check the length of inputValue to make sure that it is less then 2.
    if (
      inputValue === stateValue ||
      inputValue.length > 2 ||
      Number(inputValue) > maxValue
    ) {
      return;
    }
    // If the change made was not a deletion of a digit, then prepend '0' if the input value is a
    // single digit value between 3 and 9 for a 24 hour time, or 2 and 9 for a 12 hour clock
    if (inputValue.length >= stateValue.length) {
      const digitsToPrependZero = ["3", "4", "5", "6", "7", "8", "9"];
      if (variant === TimeUtil.FORMAT_12_HOUR) {
        digitsToPrependZero.push("2");
      }
      if (digitsToPrependZero.indexOf(inputValue) > -1) {
        inputValue = "0".concat(inputValue);
      }
    }
    if (inputValue === "00" && variant === TimeUtil.FORMAT_12_HOUR) {
      inputValue = "12";
    }
    // Move focus to the minute input if the hour input has a valid and complete entry.
    const moveFocusOnChange = () => {
      if (inputValue.length === 2) {
        minuteInput.focus();
      }
    };
    handleValueChange(
      event,
      TimeUtil.inputType.HOUR,
      inputValue,
      state.meridiem,
      moveFocusOnChange
    );
  }
  handleMinuteChange(event) {
    if (!TimeUtil.validNumericInput(event.target.value)) {
      return;
    }
    let inputValue = event.target.value;
    const stateValue = state.minute;
    const maxValue = 59;
    // Ignore the entry if the value did not change or it is invalid.
    // When 'Predictive text' is enabled on Android the maxLength attribute on the input is ignored so we have
    // to check the length of inputValue to make sure that it is less then 2.
    if (
      inputValue === stateValue ||
      inputValue.length > 2 ||
      Number(inputValue) > maxValue
    ) {
      return;
    }
    // If the change made was not a deletion of a digit, then prepend '0'
    // if the input value is a single digit value between 6 and 9.
    if (inputValue.length >= stateValue.length) {
      const digitsToPrependZero = ["6", "7", "8", "9"];
      if (digitsToPrependZero.indexOf(inputValue) > -1) {
        inputValue = "0".concat(inputValue);
      }
    }
    const moveFocusOnChange = () => {
      // Move focus to second if second is shown and minute input has a valid and complete entry
      if (inputValue.length === 2 && props.showSeconds) {
        secondInput.focus();
      }
    };
    handleValueChange(
      event,
      TimeUtil.inputType.MINUTE,
      inputValue,
      state.meridiem,
      moveFocusOnChange
    );
  }
  handleSecondChange(event) {
    if (!TimeUtil.validNumericInput(event.target.value)) {
      return;
    }
    let inputValue = event.target.value;
    const stateValue = state.second;
    const maxValue = 59;
    // Ignore the entry if the value did not change or it is invalid.
    // When 'Predictive text' is enabled on Android the maxLength attribute on the input is ignored so we have
    // to check the length of inputValue to make sure that it is less then 2.
    if (
      inputValue === stateValue ||
      inputValue.length > 2 ||
      Number(inputValue) > maxValue
    ) {
      return;
    }
    // If the change made was not a deletion of a digit, then prepend '0'
    // if the input value is a single digit value between 6 and 9.
    if (inputValue.length >= stateValue.length) {
      const digitsToPrependZero = ["6", "7", "8", "9"];
      if (digitsToPrependZero.indexOf(inputValue) > -1) {
        inputValue = "0".concat(inputValue);
      }
    }
    handleValueChange(
      event,
      TimeUtil.inputType.SECOND,
      inputValue,
      state.meridiem
    );
  }
  handleMeridiemButtonFocus(event) {
    if (
      props.onFocus &&
      !timeInputContainer.current.contains(event.relatedTarget)
    ) {
      props.onFocus(event);
    }
  }
  /**
   * Takes a key input from the hour input, and processes it based on the value of the keycode.
   * If the key is an up or down arrow, it increments/decrements the hour. If the right arrow
   * is pressed, it shifts focus to the minute input.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleHourInputKeyDown(event) {
    let stateValue = state.hour;
    let { meridiem } = state;
    const previousStateValue = stateValue;
    const variant = TimeUtil.getVariantFromLocale(props);
    if (event.keyCode === KeyCode.KEY_UP) {
      stateValue = TimeUtil.incrementHour(stateValue, variant);
      // Hitting 12 when incrementing up changes the meridiem
      if (variant === TimeUtil.FORMAT_12_HOUR && stateValue === "12") {
        if (meridiem === postMeridiem || !previousStateValue) {
          meridiem = anteMeridiem;
        } else {
          meridiem = postMeridiem;
        }
      }
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      stateValue = TimeUtil.decrementHour(stateValue, variant);
      // Hitting 11 when incrementing down changes the meridiem
      if (variant === TimeUtil.FORMAT_12_HOUR && stateValue === "11") {
        meridiem =
          meridiem === postMeridiem
            ? anteMeridiem
            : postMeridiem;
      }
    }
    if (stateValue !== previousStateValue) {
      handleValueChange(
        event,
        TimeUtil.inputType.HOUR,
        stateValue,
        meridiem
      );
    }
    if (event.keyCode === KeyCode.KEY_RIGHT) {
      focusMinuteFromHour(event);
    }
  }
  focusMinuteFromHour(event) {
    // If the hour is empty or the cursor is after the value, move focus to the minute input when the right arrow is pressed.
    if (
      state.hour.length === 0 ||
      state.hour.length === hourInput.selectionEnd
    ) {
      minuteInput.focus();
      minuteInput.setSelectionRange(0, 0);
      event.preventDefault();
    }
  }
  /**
   * Takes a key input from the minute input, and processes it based on the value of the keycode.
   * If the key is an up or down arrow, it increments/decrements the minute. If the left arrow
   * is pressed, it shifts focus to the hour input. If the right arrow is pressed, it shifts
   * focus to the meridiem input.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleMinuteInputKeyDown(event) {
    let stateValue = state.minute;
    const previousStateValue = stateValue;
    if (event.keyCode === KeyCode.KEY_UP) {
      stateValue = TimeUtil.incrementMinute(stateValue);
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      stateValue = TimeUtil.decrementMinute(stateValue);
    }
    if (previousStateValue !== stateValue) {
      handleValueChange(
        event,
        TimeUtil.inputType.MINUTE,
        stateValue,
        state.meridiem
      );
    }
    if (
      event.keyCode === KeyCode.KEY_LEFT ||
      event.keyCode === KeyCode.KEY_DELETE ||
      event.keyCode === KeyCode.KEY_BACK_SPACE
    ) {
      focusHour(event);
    }
    if (event.keyCode === KeyCode.KEY_RIGHT && props.showSeconds) {
      focusSecondFromMinute(event);
    }
  }
  focusHour(event) {
    // If the cursor is at the left most position in the minute input, is empty or the cursor is before the value,
    // move focus to the hour input
    if (minuteInput.selectionEnd === 0) {
      hourInput.focus();
      if (state.hour) {
        hourInput.setSelectionRange(
          state.hour.length,
          state.hour.length
        );
        event.preventDefault();
      }
    }
  }
  focusSecondFromMinute(event) {
    // If the minute is empty or the cursor is after the value, move focus to the meridiem.
    if (
      (state.minute.length === 0 ||
        state.minute.length === minuteInput.selectionEnd) &&
      secondInput
    ) {
      secondInput.focus();
      if (state.second) {
        secondInput.setSelectionRange(0, 0);
        event.preventDefault();
      }
    }
  }
  /**
   * Takes a key input from the second input, and processes it based on the value of the keycode.
   * If the key is an up or down arrow, it increments/decrements the second. If the left arrow
   * is pressed, it shifts focus to the minute input. If the right arrow is pressed, it shifts
   * focus to the meridiem input.
   * @param {Object} event Event object generated from the event delegation.
   */
  handleSecondInputKeyDown(event) {
    let stateValue = state.second;
    const previousStateValue = stateValue;
    if (event.keyCode === KeyCode.KEY_UP) {
      stateValue = TimeUtil.incrementSecond(stateValue);
    }
    if (event.keyCode === KeyCode.KEY_DOWN) {
      stateValue = TimeUtil.decrementSecond(stateValue);
    }
    if (previousStateValue !== stateValue) {
      handleValueChange(
        event,
        TimeUtil.inputType.SECOND,
        stateValue,
        state.meridiem
      );
    }
    if (
      event.keyCode === KeyCode.KEY_LEFT ||
      event.keyCode === KeyCode.KEY_DELETE ||
      event.keyCode === KeyCode.KEY_BACK_SPACE
    ) {
      focusMinuteFromSecond(event);
    }
  }
  focusMinuteFromSecond(event) {
    // If the cursor is at the left most position in the second input, is empty or the cursor is before the value,
    // move focus to the minute input
    if (secondInput.selectionEnd === 0) {
      minuteInput.focus();
      if (state.minute) {
        minuteInput.setSelectionRange(
          state.minute.length,
          state.minute.length
        );
        event.preventDefault();
      }
    }
  }
  handleValueChange(event, type, timeValue, meridiem, moveFocusOnChange) {
    if (type === TimeUtil.inputType.HOUR) {
      setState(
        {
          hour: timeValue,
          meridiem,
          hourInitialFocused: false
        },
        moveFocusOnChange
      );
    } else if (type === TimeUtil.inputType.MINUTE) {
      setState(
        {
          minute: timeValue,
          minuteInitialFocused: false
        },
        moveFocusOnChange
      );
    } else {
      setState(
        {
          second: timeValue,
          secondInitialFocused: false
        },
        moveFocusOnChange
      );
    }
    // Input values of length 1 indicate incomplete time, which means we cannot get a
    // reliable time so onChange isn't triggered.
    if (props.onChange && timeValue.length !== 1) {
      const hour =
        type === TimeUtil.inputType.HOUR ? timeValue : state.hour;
      const minute =
        type === TimeUtil.inputType.MINUTE ? timeValue : state.minute;
      const second =
        type === TimeUtil.inputType.SECOND ? timeValue : state.second;
      if (hour === "" && minute === "" && second === "") {
        props.onChange(event, "");
      } else {
        props.onChange(
          event,
          formatHour(hour, meridiem)
            .concat(":", minute)
            .concat(props.showSeconds ? ":".concat(second) : "")
        );
      }
    }
  }
  formatHour(hour, meridiem) {
    if (!hour) {
      return hour;
    }
    let tempHour = parseInt(hour, 10);
    if (TimeUtil.getVariantFromLocale(props) === TimeUtil.FORMAT_12_HOUR) {
      if (meridiem === postMeridiem && tempHour < 12) {
        tempHour += 12;
      } else if (meridiem === anteMeridiem && tempHour === 12) {
        tempHour = 0;
      }
    }
    let hourString = tempHour.toString();
    if (hourString.length < 2) {
      hourString = "0".concat(hourString);
    }
    return hourString;
  }
  handleMeridiemButtonChange(event, selectedKey) {
    handleValueChange(
      event,
      TimeUtil.inputType.HOUR,
      state.hour.toString(),
      selectedKey
    );
  }
  render() {
    const {
      disabled,
      inputAttributes,
      minuteAttributes,
      hourAttributes,
      intl,
      isIncomplete,
      isInvalid,
      isInvalidMeridiem,
      onBlur,
      onChange,
      onFocus,
      name,
      ref,
      required,
      secondAttributes,
      showSeconds,
      value,
      variant,
      ...customProps
    } = props;
    const anteMeridiemClassNames = cx([
      "meridiem-button",
      {
        "is-invalid":
          isInvalidMeridiem && state.meridiem === anteMeridiem
      }
    ]);
    const postMeridiemClassNames = cx([
      "meridiem-button",
      {
        "is-invalid":
          isInvalidMeridiem && state.meridiem === postMeridiem
      }
    ]);
    const variantFromLocale = TimeUtil.getVariantFromLocale(props);
    // Using the state of hour, minute, and second (if shown) create a time in UTC represented in ISO 8601 format.
    let timeValue = "";
    if (
      state.hour.length > 0 ||
      state.minute.length > 0 ||
      (state.second.length > 0 && showSeconds)
    ) {
      let hour = parseInt(state.hour, 10);
      if (
        variantFromLocale === TimeUtil.FORMAT_12_HOUR &&
        state.meridiem === postMeridiem
      ) {
        hour += 12;
      }
      timeValue = "T".concat(hour, ":", state.minute);
      if (showSeconds) {
        timeValue = timeValue.concat(":", state.second);
      }
    }
    const theme = context;
    const timeInputClassNames = classNames(
      cx(
        { disabled },
        "time-input",
        { "is-focused": state.isFocused },
        { "is-invalid": isInvalid },
        {
          "is-incomplete":
            isIncomplete && required && !isInvalid && !isInvalidMeridiem
        },
        theme.className
      ),
      customProps.className
    );
    const hourClassNames = cx([
      "time-input-hour",
      { "with-second": showSeconds },
      { "initial-focus": state.hourInitialFocused }
    ]);
    const minuteClassNames = cx([
      "time-input-minute",
      { "with-second": showSeconds },
      { "initial-focus": state.minuteInitialFocused }
    ]);
    const secondClassNames = cx([
      "time-input-second",
      { "initial-focus": state.secondInitialFocused }
    ]);
    return (
      <div
        {...customProps}
        ref={timeInputContainer}
        className={cx("time-input-container")}
      >
        <div className={timeInputClassNames}>
          <input
            // Create a hidden input for storing the name and value attributes to use when submitting the form.
            // The data stored in the value attribute will be the visible date in the date input but in ISO 8601 format.
            type="hidden"
            name={name}
            value={timeValue}
          />
          <Input
            {...inputAttributes}
            {...hourAttributes}
            aria-label={intl.formatMessage({ id: "Terra.timeInput.hours" })}
            ref={inputRef => {
              hourInput = inputRef;
              if (ref) ref(inputRef);
            }}
            className={hourClassNames}
            type="text"
            value={state.hour}
            name={"terra-time-hour-".concat(name)}
            placeholder={intl.formatMessage({ id: "Terra.timeInput.hh" })}
            maxLength="2"
            onChange={handleHourChange}
            onKeyDown={handleHourInputKeyDown}
            onFocus={handleHourFocus}
            onBlur={handleHourBlur}
            size="2"
            pattern="\d*"
            disabled={disabled}
          />
          <span className={cx("time-spacer")}>:</span>
          <Input
            {...inputAttributes}
            {...minuteAttributes}
            ref={inputRef => {
              minuteInput = inputRef;
            }}
            aria-label={intl.formatMessage({ id: "Terra.timeInput.minutes" })}
            className={minuteClassNames}
            type="text"
            value={state.minute}
            name={"terra-time-minute-".concat(name)}
            placeholder={intl.formatMessage({ id: "Terra.timeInput.mm" })}
            maxLength="2"
            onChange={handleMinuteChange}
            onKeyDown={handleMinuteInputKeyDown}
            onFocus={handleMinuteFocus}
            onBlur={handleMinuteBlur}
            size="2"
            pattern="\d*"
            disabled={disabled}
          />
          {showSeconds && (
            <>
              <span className={cx("time-spacer")}>:</span>
              <Input
                {...inputAttributes}
                {...secondAttributes}
                ref={inputRef => {
                  secondInput = inputRef;
                }}
                aria-label={intl.formatMessage({
                  id: "Terra.timeInput.seconds"
                })}
                className={secondClassNames}
                type="text"
                value={state.second}
                name={"terra-time-second-".concat(name)}
                placeholder={intl.formatMessage({ id: "Terra.timeInput.ss" })}
                maxLength="2"
                onChange={handleSecondChange}
                onKeyDown={handleSecondInputKeyDown}
                onFocus={handleSecondFocus}
                onBlur={handleSecondBlur}
                size="2"
                pattern="\d*"
                disabled={disabled}
              />
            </>
          )}
        </div>
        {variantFromLocale === TimeUtil.FORMAT_12_HOUR && (
          <ButtonGroup
            selectedKeys={[state.meridiem]}
            onChange={handleMeridiemButtonChange}
            className={cx("meridiem-button-group")}
          >
            <ButtonGroup.Button
              key={anteMeridiem}
              className={anteMeridiemClassNames}
              text={anteMeridiem}
              onBlur={handleMeridiemButtonBlur}
              onFocus={handleMeridiemButtonFocus}
              isDisabled={disabled}
            />
            <ButtonGroup.Button
              key={postMeridiem}
              className={postMeridiemClassNames}
              text={postMeridiem}
              onBlur={handleMeridiemButtonBlur}
              onFocus={handleMeridiemButtonFocus}
              isDisabled={disabled}
            />
          </ButtonGroup>
        )}
      </div>
    );
    /* eslint-enable jsx-a11y/no-static-element-interactions */
  }
}

export default injectIntl(TimeInput);
