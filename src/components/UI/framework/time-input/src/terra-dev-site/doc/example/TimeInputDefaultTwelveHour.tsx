import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TimeInput from 'terra-time-input/lib/TimeInput';
import TimeUtil from 'terra-time-input/lib/TimeUtil';
import classNames from 'classnames/bind';
import styles from './TimeInputDocCommon.module.scss';
const cx = classNames.bind(styles);
type timeInputState = {
  time?: string;
};
class timeInput extends React.Component<{}, timeInputState> {
  constructor(props) {
    super(props);
    state = { time: '21:24' };
    handleTimeChange = handleTimeChange.bind(this);
  }
  handleTimeChange(event, time) {
    setState({ time });
  }
  render() {
    return (
      <div>
        <p>
          Time Provided:
          <span className={cx('time-wrapper')}>{state.time}</span>
        </p>
        <TimeInput name="time-input-value" value={state.time} onChange={handleTimeChange} variant={TimeUtil.FORMAT_12_HOUR} />
      </div>
    );
  }
}
export default timeInput;
