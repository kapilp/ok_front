import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TimeInput from '../../../../TimeInput';
import TimeUtil from '../../../../TimeUtil';
type TimeInputDefaultState = {
  input?: null;
};
class TimeInputDefault extends React.Component<{}, TimeInputDefaultState> {
  constructor(props) {
    super(props);
    state = { input: null };
    handleOnChange = handleOnChange.bind(this);
  }
  handleOnChange(event, input) {
    setState({ input });
  }
  render() {
    return (
      <div>
        <div id="time-input-value">
          <h3>
            Time Input:
            {state.input}
          </h3>
        </div>
        <TimeInput id="timeInput" name="time-input" onChange={handleOnChange} variant={TimeUtil.FORMAT_12_HOUR} />
      </div>
    );
  }
}
export default TimeInputDefault;
