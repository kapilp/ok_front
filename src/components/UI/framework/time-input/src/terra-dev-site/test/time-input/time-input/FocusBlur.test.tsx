import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import TimeInput from '../../../../TimeInput';
import styles from '../common/TimeInput.test.module.scss';
const cx = classNames.bind(styles);
type TimeInputOnBlurState = {
  blurTriggerCount?: number;
  focusTriggerCount?: number;
};
class TimeInputOnBlur extends React.Component<{}, TimeInputOnBlurState> {
  blurCount: number;
  focusCount: number;
  constructor(props) {
    super(props);
    state = { blurTriggerCount: 0, focusTriggerCount: 0 };
    handleBlur = handleBlur.bind(this);
    handleFocus = handleFocus.bind(this);
    blurCount = 0;
    focusCount = 0;
  }
  handleBlur() {
    blurCount += 1;
    setState({ blurTriggerCount: blurCount });
  }
  handleFocus() {
    focusCount += 1;
    setState({ focusTriggerCount: focusCount });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <div id="time-input-value">
          <h3>
            onBlur Trigger Count:
            <span id="blur-count">{state.blurTriggerCount}</span>
            <br />
            <br />
            onFocus Trigger Count:
            <span id="focus-count">{state.focusTriggerCount}</span>
          </h3>
        </div>
        <TimeInput id="timeInput" name="time-input" variant="12-hour" onBlur={handleBlur} onFocus={handleFocus} />
      </div>
    );
  }
}
export default TimeInputOnBlur;
