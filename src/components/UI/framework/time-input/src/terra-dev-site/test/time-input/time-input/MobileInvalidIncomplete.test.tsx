import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import TimeInput from "../../../../TimeInput";
import styles from "../common/TimeInput.test.module.scss";
const cx = classNames.bind(styles);
export const TimeInputElement: Component = (props:{}) => {
  resetontouchstart: any;
  constructor(props) {
    super(props);
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = "true";
    }
  }
  componentDidMount() {
    if (!window.ontouchstart) {
      resetontouchstart = true;
      window.ontouchstart = "true";
    }
  }
  componentWillUnmount() {
    if (resetontouchstart) {
      delete window.ontouchstart;
    }
  }
  render() {
    return (
      <div className={cx("content-wrapper")}>
        <TimeInput
          id="timeInput"
          name="time-input"
          value="10:23"
          isInvalid
          required
          isIncomplete
        />
      </div>
    );
  }
}
