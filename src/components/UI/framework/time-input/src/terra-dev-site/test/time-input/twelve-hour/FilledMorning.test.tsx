import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import TimeInput from '../../../../TimeInput';
import TimeUtil from '../../../../TimeUtil';
const TimeInputDefault = () => <TimeInput id="timeInput" name="time-input" value="09:22" variant={TimeUtil.FORMAT_12_HOUR} />;
export default TimeInputDefault;
