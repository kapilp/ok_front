import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import TimeInput from '../../../../TimeInput';
import TimeUtil from '../../../../TimeUtil';
import styles from '../common/TimeInput.test.module.scss';
const cx = classNames.bind(styles);
const TimeInputDefault = () => (
  <div className={cx('content-wrapper')}>
    <TimeInput id="timeInput" name="time-input" variant={TimeUtil.FORMAT_12_HOUR} required isInvalid />
  </div>
);
export default TimeInputDefault;
