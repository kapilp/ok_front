import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import TimeInput from '../../../../TimeInput';
import styles from '../common/TimeInput.test.module.scss';
const cx = classNames.bind(styles);
const TimeInputDefault = () => (
  <div className={cx('content-wrapper')}>
    <TimeInput id="timeInput" name="time-input" value="12:00" />
  </div>
);
export default TimeInputDefault;
