import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import TimeInput from '../../../../TimeInput';
import styles from '../common/TimeInput.test.module.scss';
const cx = classNames.bind(styles);
type TimeInputDefaultState = {
  input?: null;
};
class TimeInputDefault extends React.Component<{}, TimeInputDefaultState> {
  constructor(props) {
    super(props);
    state = { input: null };
    handleOnChange = handleOnChange.bind(this);
  }
  handleOnChange(event, input) {
    setState({ input });
  }
  render() {
    return (
      <div className={cx('content-wrapper')}>
        <div id="time-input-value">
          <h3>
            Time Input:
            {state.input}
          </h3>
        </div>
        <TimeInput id="timeInput" name="time-input" onChange={handleOnChange} showSeconds />
      </div>
    );
  }
}
export default TimeInputDefault;
