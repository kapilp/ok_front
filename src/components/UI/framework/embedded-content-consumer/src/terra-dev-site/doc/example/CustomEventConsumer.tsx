import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { Consumer } from 'xfc';
import { EmbeddedContentConsumer } from '../../../EmbeddedContentConsumer';
Consumer.init();
const eventHandlers = [
  {
    key: 'EventA',
    handler: () => {
      document.getElementById('CustomEvent').style.border = 'thick dashed #0000FF';
    },
  },
];
export const CustomEventConsumer = () => (
  <div id="CustomEvent">
    <EmbeddedContentConsumer
      src="/#/raw/provider/terra-embedded-content-consumer/embedded-content-consumer/providers/custom-event-provider"
      options={{ iframeAttrs: { title: 'Custom Event Example' } }}
      eventHandlers={eventHandlers}
    />
  </div>
);
