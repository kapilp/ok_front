import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { Consumer } from 'xfc';
import { EmbeddedContentConsumer } from '../../../EmbeddedContentConsumer';
Consumer.init();
export const BasicConsumer = () => (
  <EmbeddedContentConsumer
    src="/#/raw/provider/terra-embedded-content-consumer/embedded-content-consumer/providers/basic-provider"
    options={{ iframeAttrs: { title: 'Basic content example' } }}
  />
);
