import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';

import { Consumer } from 'xfc';
import { EmbeddedContentConsumer } from '../../../EmbeddedContentConsumer';
Consumer.init();
export const CustomEventsConsumer: Component = (props: {}) => {
  let frame: any;
  const onMount = frame => {
    frame = frame;
  };
  const handleEventA = () => {
    document.getElementById('CustomEvents').style.border = 'thick dashed #0000FF';
    frame.trigger('Event-Reply', {
      eventReply: 'eventA',
      borderColor: '#0000FF',
    });
  };
  const handleEventB = () => {
    document.getElementById('CustomEvents').style.border = 'thick dashed #00FF00';
    frame.trigger('Event-Reply', {
      eventReply: 'eventB',
      borderColor: '#00FF00',
    });
  };

  const eventHandlers = [
    {
      key: 'EventA',
      handler: handleEventA,
    },
    {
      key: 'EventB',
      handler: handleEventB,
    },
  ];
  return (
    <div id="CustomEvents">
      <EmbeddedContentConsumer
        src="/#/raw/provider/terra-embedded-content-consumer/embedded-content-consumer/providers/custom-events-provider"
        options={{ iframeAttrs: { title: 'Custom events example' } }}
        onMount={onMount}
        eventHandlers={eventHandlers}
      />
    </div>
  );
};
