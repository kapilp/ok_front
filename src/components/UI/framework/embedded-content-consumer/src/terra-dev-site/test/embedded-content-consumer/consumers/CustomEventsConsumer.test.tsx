import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from "classnames/bind";
import { Consumer } from "xfc";
import EmbeddedContentConsumer from "terra-embedded-content-consumer/lib/EmbeddedContentConsumer";
import styles from "./Consumer.module.scss";
const cx = classNames.bind(styles);
Consumer.init();
export const CustomEventsConsumer: Component = (props:{}) => {
  frame: any;
  constructor(props) {
    super(props);
    onMount = onMount.bind(this);
    handleEventA = handleEventA.bind(this);
    handleEventB = handleEventB.bind(this);
  }
  onMount(frame) {
    frame = frame;
  }
  handleEventA() {
    document.getElementById("CustomEvents").style.border =
      "thick dashed #0000FF";
    frame.trigger("Event-Reply", {
      eventReply: "eventA",
      borderColor: "#0000FF"
    });
  }
  handleEventB() {
    document.getElementById("CustomEvents").style.border =
      "thick dashed #00FF00";
    frame.trigger("Event-Reply", {
      eventReply: "eventB",
      borderColor: "#00FF00"
    });
  }
  render() {
    const eventHandlers = [
      {
        key: "EventA",
        handler: handleEventA
      },
      {
        key: "EventB",
        handler: handleEventB
      }
    ];
    return (
      <div id="CustomEvents">
        <EmbeddedContentConsumer
          className={cx("iframe")}
          src="/#/raw/provider/terra-embedded-content-consumer/embedded-content-consumer/providers/custom-events-provider"
          options={{ iframeAttrs: { title: "Custom events example" } }}
          onMount={onMount}
          eventHandlers={eventHandlers}
        />
      </div>
    );
  }
}
export default CustomEventsConsumer;
