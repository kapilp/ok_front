import {  JSX, Component, onCleanup, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import './ProviderIframe.module.scss';
import './ProviderTestTemplate.module.scss';
interface IEmbeddedContentProviderTestTemplateProps extends JSX.HTMLAttributes<Element> {
  /**
   * The content which will be embedded in an iframe by the XFC consumer.
   */
  children: JSX.Element;
}
export const EmbeddedContentProviderTestTemplate = (props: IEmbeddedContentProviderTestTemplateProps) => {
  //componentDidMount() {
  document.body.classList.toggle('embedded-content-provider-test-template-body');
  //}
  onCleanup(() => {
    document.body.classList.toggle('embedded-content-provider-test-template-body');
  });
  const [p, customProps] = splitProps(props, ['children']);

  return (
    <div data-embedded-content-consumer-provider-test-template {...customProps}>
      {p.children}
    </div>
  );
};
