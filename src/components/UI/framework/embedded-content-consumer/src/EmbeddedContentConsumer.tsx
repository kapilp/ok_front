import {  JSX, Component, createEffect, createSignal, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { Consumer } from 'xfc';
interface IEmbeddedContentConsumerProps extends JSX.HTMLAttributes<Element> {
  /**
   * The source URL of the content to load.
   */
  src: string;
  /**
   * Notifies the component that the container has been mounted. Provides a reference
   * to this component to allow triggering messages on the embedded application.
   */
  onMount?: (xfcFrame: any) => void;
  /**
   * Notifies the component that the container has been launched.
   */
  onLaunch?: () => void;
  /**
   * Notifies the component that the container has been authorized.
   */
  onAuthorize?: () => void;
  /**
   * The component can be configured with consumer frame options.
   * See xfc consumer configuration for details?: https://github.com/cerner/xfc
   */
  options?: {};
  /**
   * A set of event handlers keyed by the event name.
   * Note?: Binding the event handler is necessary to make `this` work in the callback.
   */
  eventHandlers?: {
    key: string;
    handler: () => void;
  }[];
}

export const EmbeddedContentConsumer = (props: IEmbeddedContentConsumerProps) => {
  const [embeddedContentWrapper, setEmbeddedContentWrapper] = createSignal<HTMLDivElement>();
  let xfcFrame: any;
  const onMount = () => {
    // Mount the provided source as the application into the content wrapper.
    xfcFrame = Consumer.mount(embeddedContentWrapper(), props.src, props.options);
    // Notify that the consumer frame has mounted.
    if (props.onMount) {
      props.onMount(xfcFrame);
    }
    // Attach the event handlers to the xfc frame.
    addEventListener('xfc.launched', props.onLaunch);
    addEventListener('xfc.authorized', props.onAuthorize);
    // Attach custom event handlers to the xfc frame.
    addEventListeners(props.eventHandlers);
  };
  createEffect(() => {
    if (embeddedContentWrapper()) onMount();
  });
  function addEventListener(eventName: string, eventHandler: () => void) {
    if (eventName && eventHandler) {
      xfcFrame.on(eventName, eventHandler);
    }
  }
  function addEventListeners(
    customEvents: {
      key: string;
      handler: () => void;
    }[],
  ) {
    (customEvents || []).forEach(e => addEventListener(e.key, e.handler));
  }
  const [p, customProps] = splitProps(props, ['src', 'onMount', 'onLaunch', 'onAuthorize', 'options', 'eventHandlers']);
  return <div {...customProps} ref={setEmbeddedContentWrapper} />;
};
