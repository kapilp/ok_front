import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import classNames from 'classnames/bind';
import Button from 'terra-button';
import ContentContainer from 'terra-content-container';
import Input from 'terra-form-input';
import ActionHeader from 'terra-action-header';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import styles from './example-styles.module.scss';
const cx = classNames.bind(styles);
interface Properties {
  name: string;
  disclosureType: string;
  disclosureManager: disclosureManagerShape;
}
const defaultProps = {
  name: 'Disclosure Component',
};
interface IDisclosureComponentProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
  name?: any;
  disclosureType?: any;
}
type DisclosureComponentState = {
  text?: undefined;
  length?: any;
};
class DisclosureComponent extends React.Component<IDisclosureComponentProps, DisclosureComponentState> {
  constructor(props) {
    super(props);
    checkLockState = checkLockState.bind(this);
    state = {
      text: undefined,
    };
  }
  componentDidMount() {
    const { disclosureManager } = props;
    if (disclosureManager && disclosureManager.registerDismissCheck) {
      disclosureManager.registerDismissCheck(checkLockState);
    }
  }
  checkLockState() {
    if (state.text && state.text.length) {
      return new Promise((resolve, reject) => {
        // eslint-disable-next-line no-restricted-globals
        if (!confirm(`${props.name} has unsaved changes that will be lost. Do you wish to continue?`)) {
          // eslint-disable-line no-alert
          reject();
          return;
        }
        resolve();
      });
    }
    return Promise.resolve();
  }
  render() {
    const { disclosureManager, name, disclosureType } = props;
    return (
      <ContentContainer
        fill
        header={
          <ActionHeader
            title={`Disclosure - ${name}`}
            onClose={disclosureManager.closeDisclosure}
            onBack={disclosureManager.goBack}
            onMaximize={disclosureManager.maximize}
            onMinimize={disclosureManager.minimize}
          />
        }
      >
        <div className={cx('content-wrapper')}>
          <h3>{name}</h3>
          <p>The disclosed component can disclose content within the same panel.</p>
          <p>It can also render a header (like above) that implements the various DisclosureManager control functions.</p>
          <Button
            text="Dismiss"
            onClick={() => {
              disclosureManager.dismiss().catch(() => {
                console.log('Dismiss failed. A lock must be in place.'); // eslint-disable-line no-console
              });
            }}
          />
          <Button
            text="Disclose Again"
            onClick={() => {
              disclosureManager.disclose({
                preferredType: disclosureType,
                size: 'small',
                content: {
                  key: `Nested ${name}`,
                  component: <WrappedDisclosureComponent name={`Nested ${name}`} disclosureType={disclosureType} />,
                },
              });
            }}
          />
          <br />
          <br />
          <p>
            The disclosed component can register a dismiss check function that can interrupt and prevent dismissal. This component will prompt the user if text is detected in the
            input field below.
          </p>
          <Input
            value={state.text || ''}
            onChange={event => {
              setState({
                text: event.target.value,
              });
            }}
          />
          {state.text && state.text.length ? <p>Component has unsaved changes!</p> : null}
        </div>
      </ContentContainer>
    );
  }
}
props = mergeProps({}, defaultProps, props);
const WrappedDisclosureComponent = withDisclosureManager(DisclosureComponent);
export default WrappedDisclosureComponent;
