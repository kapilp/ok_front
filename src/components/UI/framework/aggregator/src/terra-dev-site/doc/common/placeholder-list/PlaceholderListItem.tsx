import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import './PlaceholderList.module.scss';
/* eslint-disable */
const PlaceholderListItem = ({ children, isSelected, onSelect, ...customProps }) => {
  const keyDown = event => {
    if (event.keyCode === 13 || event.keyCode === 32) {
      event.preventDefault();
      onSelect(event);
    }
  };
  const classNames = isSelected ? 'placeholder-list-item is-selected' : 'placeholder-list-item ';
  return (
    <li {...customProps} aria-selected={isSelected} tabIndex="0" className={classNames} onClick={onSelect} onKeyDown={keyDown} role="option">
      {children}
    </li>
  );
};
export default PlaceholderListItem;
