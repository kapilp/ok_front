import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import { withDisclosureManager } from "terra-disclosure-manager";
import Aggregator from "../../../Aggregator";
interface Properties {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      key: string,
      component: JSX.Element
    })
  )
};
const AggregatorContainer: Component = withDisclosureManager(
  ({ items, disclosureManager }) => (
    <Aggregator items={items} disclose={disclosureManager.disclose} />
  )
);
export default AggregatorContainer;
