import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ContentContainer from 'terra-content-container';
import SectionHeader from 'terra-section-header';
interface Properties {
  aggregatorDelegate: PropTypes.object;
  name: string;
}
const SimpleAggregatorItem: Component = ({ name, aggregatorDelegate, ...customProps }) => (
  <ContentContainer {...customProps} header={<SectionHeader title={name} />}>
    {aggregatorDelegate.hasFocus ? (
      <button
        type="button"
        onClick={() => {
          aggregatorDelegate.releaseFocus();
        }}
      >
        Release Focus
      </button>
    ) : (
      <button
        type="button"
        onClick={() => {
          aggregatorDelegate.requestFocus();
        }}
      >
        Get Focus
      </button>
    )}
    {aggregatorDelegate.hasFocus ? <h4>Section has focus!</h4> : null}
  </ContentContainer>
);
export default SimpleAggregatorItem;
