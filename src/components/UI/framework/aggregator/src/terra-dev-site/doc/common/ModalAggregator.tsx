import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ContentContainer from 'terra-content-container';
import ActionHeader from 'terra-action-header';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import Aggregator from '../../../Aggregator';
import AggregatorItem from './AggregatorItem';
import SimpleAggregatorItem from './SimpleAggregatorItem';
const items = Object.freeze([
  {
    key: 'SECTION_0',
    component: <AggregatorItem name="Slide Panel Section" disclosureType="panel" discloseOnSelect />,
  },
  {
    key: 'SECTION_1',
    component: <AggregatorItem name="Modal Section" disclosureType="modal" discloseOnSelect />,
  },
  {
    key: 'SECTION_2',
    component: <SimpleAggregatorItem name="No Disclosure Section" />,
  },
]);
type ModalAggregatorProps = {
  disclosureManager?: any;
};
const ModalAggregator: React.SFC<ModalAggregatorProps> = ({ disclosureManager }) => (
  <ContentContainer fill header={<ActionHeader onClose={disclosureManager.closeDisclosure} onBack={disclosureManager.goBack} />}>
    <Aggregator items={items} disclose={disclosureManager.disclose} />
  </ContentContainer>
);
export default withDisclosureManager(ModalAggregator);
