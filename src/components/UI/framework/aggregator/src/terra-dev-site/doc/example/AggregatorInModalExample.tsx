import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import { withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import ModalManager from 'terra-modal-manager';
import ModalAggregator from 'terra-aggregator/lib/terra-dev-site/doc/common/ModalAggregator';
type ModalButtonProps = {
  disclosureManager?: any;
};
const ModalButton: React.SFC<ModalButtonProps> = withDisclosureManager(({ disclosureManager }) => (
  <button
    type="button"
    onClick={() => {
      disclosureManager.disclose({
        preferredType: 'modal',
        size: 'large',
        content: {
          key: 'MODAL_EXAMPLE',
          component: <ModalAggregator identifier="MODAL_EXAMPLE" />,
        },
      });
    }}
  >
    Launch Modal
  </button>
));
const SimpleAggregatorExample = () => (
  <div>
    <ModalManager>
      <ModalButton />
    </ModalManager>
  </div>
);
export default SimpleAggregatorExample;
