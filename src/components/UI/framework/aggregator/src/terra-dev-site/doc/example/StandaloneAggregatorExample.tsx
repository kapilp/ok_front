import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import Aggregator from 'terra-aggregator';
import AggregatorItem from 'terra-aggregator/lib/terra-dev-site/doc/common/AggregatorItem';
const items = Object.freeze([
  {
    key: 'SECTION_0',
    component: <AggregatorItem name="Section 0" />,
  },
  {
    key: 'SECTION_1',
    component: <AggregatorItem name="Section 1" />,
  },
  {
    key: 'SECTION_2',
    component: <AggregatorItem name="Section 2" />,
  },
  {
    key: 'SECTION_3',
    component: <AggregatorItem name="Section 3" />,
  },
]);
type StandaloneAggregatorExampleState = {
  flip?: boolean;
};
class StandaloneAggregatorExample extends React.Component<{}, StandaloneAggregatorExampleState> {
  forceUpdate: any;
  constructor(props) {
    super(props);
    state = {
      flip: false,
    };
  }
  render() {
    const body = (
      <div>
        <button
          type="button"
          onClick={() => {
            setState(prevState => ({ flip: !prevState.flip }));
          }}
        >
          Flip Section Order
        </button>
        <button
          type="button"
          onClick={() => {
            forceUpdate();
          }}
        >
          Force Aggregator Render
        </button>
        <Aggregator items={state.flip ? Object.assign([], items).reverse() : items} />
      </div>
    );
    return body;
  }
}
export default StandaloneAggregatorExample;
