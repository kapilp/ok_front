import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import './PlaceholderList.module.scss';
/* eslint-disable */
const List = ({ children, isPadded }) => (
  <ul aria-label="Placeholder List" role="listbox" className={isPadded ? 'placeholder-list is-padded' : 'placeholder-list '}>
    {children}
  </ul>
);
export default List;
