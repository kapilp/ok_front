import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import Button from "terra-button";
import ContentContainer from "terra-content-container";
import Arrange from "terra-arrange";
import ActionHeader from "terra-action-header";
import {
  withDisclosureManager,
  disclosureManagerShape
} from "terra-disclosure-manager";
import classNames from "classnames/bind";
import List from "./placeholder-list/PlaceholderList";
import Item from "./placeholder-list/PlaceholderListItem";
import styles from "./AggregatorItem.module.scss";
import DisclosureComponent from "./DisclosureComponent";
const cx = classNames.bind(styles);
type ReadonlyModalProps = {
  disclosureManager?: any
};
const ReadonlyModal: React.SFC<ReadonlyModalProps> = withDisclosureManager(
  ({ disclosureManager }) => (
    <ContentContainer
      header={
        <ActionHeader
          title="Info Modal"
          onClose={disclosureManager.closeDisclosure}
          onBack={disclosureManager.goBack}
        />
      }
    >
      <div className={cx("readonly-modal-content")}>
        <p>
          This modal was not presented through the Aggregator. The Aggregator
          state was maintained.
        </p>
      </div>
    </ContentContainer>
  )
);
interface Properties {
  aggregatorDelegate: PropTypes.object,
  name: string,
  disclosureType: string,
  discloseOnSelect: boolean,
  customDisclose: PropTypes.func
};
interface IAggregatorItemProps extends JSX.HTMLAttributes<Element> {
  name?: any;
  disclosureType?: any;
  aggregatorDelegate?: any;
  discloseOnSelect?: any;
  customDisclose?: any;
  customProps?: any;
}
export const AggregatorItem: Component = (props:IAggregatorItemProps) => {
  constructor(props) {
    super(props);
    handleSelection = handleSelection.bind(this);
    launchModal = launchModal.bind(this);
  }
  handleSelection(event, index) {
    const { aggregatorDelegate, name, discloseOnSelect } = props;
    const disclosureSizeForIndex = {
      0: "tiny",
      1: "small",
      2: "medium",
      3: "large",
      4: "huge",
      5: "fullscreen"
    };
    if (
      aggregatorDelegate.hasFocus &&
      aggregatorDelegate.itemState.selectedIndex === index
    ) {
      aggregatorDelegate.releaseFocus().catch(() => {
        console.log(
          "Section - Focus release failed. Something must be locked."
        ); // eslint-disable-line no-console
      });
      return;
    }
    aggregatorDelegate
      .requestFocus({
        selectedIndex: index
      })
      .then(({ disclose }) => {
        if (discloseOnSelect) {
          disclose({
            preferredType: props.disclosureType,
            size: disclosureSizeForIndex[index],
            content: {
              key: `Nested ${name}`,
              component: (
                <DisclosureComponent
                  name={`Nested ${name}`}
                  disclosureType={props.disclosureType}
                />
              )
            }
          });
        }
      })
      .catch(error => {
        console.log(`Section - Selection denied - ${error}`); // eslint-disable-line no-console
      });
  }
  launchModal() {
    const { customDisclose } = props;
    const key = `ModalContent-${Date.now()}`;
    customDisclose({
      preferredType: "modal",
      size: "small",
      content: {
        key,
        component: <ReadonlyModal />
      }
    });
  }
  render() {
    const {
      name,
      disclosureType,
      aggregatorDelegate,
      discloseOnSelect,
      customDisclose,
      ...customProps
    } = props;
    let index;
    if (
      aggregatorDelegate.hasFocus &&
      aggregatorDelegate.itemState &&
      aggregatorDelegate.itemState.selectedIndex !== undefined
    ) {
      index = aggregatorDelegate.itemState.selectedIndex;
    }
    return (
      <ContentContainer
        {...customProps}
        header={
          <Arrange
            className={cx("header-arrange")}
            fitStart={
              <div className={cx("header-arrange-content")}>
                {customDisclose ? (
                  <Button
                    text="Modal (Without Requesting Focus)"
                    onClick={launchModal}
                  />
                ) : null}
              </div>
            }
            fill={<div>{name}</div>}
          />
        }
      >
        <List isPadded>
          <Item
            isSelected={index === 0}
            onSelect={event => handleSelection(event, 0)}
          >
            <div>Row 0</div>
          </Item>
          <Item
            isSelected={index === 1}
            onSelect={event => handleSelection(event, 1)}
          >
            <div>Row 1</div>
          </Item>
          <Item
            isSelected={index === 2}
            onSelect={event => handleSelection(event, 2)}
          >
            <div>Row 2</div>
          </Item>
          <Item
            isSelected={index === 3}
            onSelect={event => handleSelection(event, 3)}
          >
            <div>Row 3</div>
          </Item>
          <Item
            isSelected={index === 4}
            onSelect={event => handleSelection(event, 4)}
          >
            <div>Row 4</div>
          </Item>
          <Item
            isSelected={index === 5}
            onSelect={event => handleSelection(event, 5)}
          >
            <div>Row 5</div>
          </Item>
        </List>
      </ContentContainer>
    );
  }
}
export default AggregatorItem;
