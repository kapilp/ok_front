import {  JSX,  Component, mergeProps, splitProps   } from 'solid-js';
import { createStore } from 'solid-js/store';
import List from "../../doc/common/placeholder-list/PlaceholderList";
import Item from "../../doc/common/placeholder-list/PlaceholderListItem";
import DisclosureComponent from "./DisclosureComponent";
interface IAggregatorItemProps extends JSX.HTMLAttributes<Element> {
  name?: string;
  aggregatorDelegate?: {};
  targetId?: string;
}
/* eslint-disable react/prop-types */
export const AggregatorItem: Component = (props:IAggregatorItemProps) => {
  constructor(props) {
    super(props);
    handleSelection = handleSelection.bind(this);
  }
  handleSelection(event, key) {
    const { aggregatorDelegate, name } = props;
    if (
      aggregatorDelegate.hasFocus &&
      aggregatorDelegate.itemState.selectedKey === key
    ) {
      aggregatorDelegate.releaseFocus();
      return;
    }
    aggregatorDelegate
      .requestFocus({
        selectedKey: key
      })
      .then(({ disclose }) => {
        if (disclose) {
          disclose({
            preferredType: "panel",
            size: "small",
            content: {
              key: `Disclosure for ${name}`,
              component: (
                <DisclosureComponent identifier={`Disclosure for ${name}`} />
              )
            }
          });
        }
      });
  }
  render() {
    const { name, aggregatorDelegate, targetId } = props;
    let key;
    if (
      aggregatorDelegate.hasFocus &&
      aggregatorDelegate.itemState &&
      aggregatorDelegate.itemState.selectedKey !== undefined
    ) {
      key = aggregatorDelegate.itemState.selectedKey;
    }
    return (
      <List>
        <Item
          isSelected={key === "test-key"}
          onSelect={event => handleSelection(event, "test-key")}
        >
          <div id={targetId}>{name}</div>
        </Item>
      </List>
    );
  }
}
/* eslint-enable react/prop-types */
export default AggregatorItem;
