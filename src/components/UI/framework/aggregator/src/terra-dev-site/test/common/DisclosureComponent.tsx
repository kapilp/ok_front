import {  JSX, Component, mergeProps, splitProps  } from 'solid-js';
import { createStore } from 'solid-js/store';
import ContentContainer from 'terra-content-container';
import { availableDisclosureHeights, availableDisclosureWidths, withDisclosureManager, disclosureManagerShape } from 'terra-disclosure-manager';
import classNames from 'classnames/bind';
import styles from './DisclosureComponent.module.scss';
const cx = classNames.bind(styles);
const HEIGHT_KEYS = Object.keys(availableDisclosureHeights);
const WIDTH_KEYS = Object.keys(availableDisclosureWidths);
interface IDisclosureComponentProps extends JSX.HTMLAttributes<Element> {
  disclosureManager?: any;
  identifier?: string;
  disclosureType?: string;
  nestedIndex?: number;
  releaseFocus?: any;
  requestFocus?: any;
  minimize?: any;
  maximize?: any;
  goBack?: any;
  closeDisclosure?: any;
  dismiss?: any;
  disclose?: any;
}
type DisclosureComponentState = {
  id?: string;
  disclosureHeight?: string;
  disclosureWidth?: string;
};
class DisclosureComponent extends React.Component<IDisclosureComponentProps, DisclosureComponentState> {
  constructor(props) {
    super(props);
    disclose = disclose.bind(this);
    dismiss = dismiss.bind(this);
    closeDisclosure = closeDisclosure.bind(this);
    goBack = goBack.bind(this);
    maximize = maximize.bind(this);
    minimize = minimize.bind(this);
    requestFocus = requestFocus.bind(this);
    releaseFocus = releaseFocus.bind(this);
    generateOptions = generateOptions.bind(this);
    handleSelectChange = handleSelectChange.bind(this);
    renderFormButton = renderFormButton.bind(this);
    renderForm = renderForm.bind(this);
    getId = getId.bind(this);
    state = {
      id: 'disclosureDimensions',
      disclosureHeight: HEIGHT_KEYS[0],
      disclosureWidth: WIDTH_KEYS[0],
    };
  }
  getId(name) {
    return `${state.id}-${name}-${props.nestedIndex}`;
  }
  generateOptions(values, name) {
    return values.map((currentValue, index) => {
      const keyValue = index;
      return (
        <option id={`${name}-${currentValue}-${props.nestedIndex}`} key={keyValue} value={currentValue}>
          {currentValue}
        </option>
      );
    });
  }
  handleSelectChange(event) {
    setState({ [event.target.name]: event.target.value });
  }
  disclose(size, dimensions) {
    const { disclosureType, nestedIndex } = props;
    const newIndex = nestedIndex + 1;
    return () => {
      props.disclosureManager.disclose({
        preferredType: disclosureType,
        size,
        dimensions,
        content: {
          key: `DemoContainer-${newIndex}`,
          component: <WrappedDisclosureComponent identifier={`DemoContainer-${newIndex}`} nestedIndex={newIndex} />,
        },
      });
    };
  }
  dismiss() {
    props.disclosureManager.dismiss();
  }
  closeDisclosure() {
    props.disclosureManager.closeDisclosure();
  }
  goBack() {
    props.disclosureManager.goBack();
  }
  maximize() {
    props.disclosureManager.maximize();
  }
  minimize() {
    props.disclosureManager.minimize();
  }
  requestFocus() {
    props.disclosureManager.requestFocus();
  }
  releaseFocus() {
    props.disclosureManager.releaseFocus();
  }
  renderFormButton() {
    const name = `Disclose (${state.disclosureHeight}) x (${state.disclosureWidth})`;
    return (
      <button
        type="button"
        id={`disclose-dimension-${props.nestedIndex}`}
        onClick={disclose(undefined, {
          height: state.disclosureHeight,
          width: state.disclosureWidth,
        })}
      >
        {name}
      </button>
    );
  }
  renderForm() {
    return (
      <form>
        <label htmlFor={getId('height')}>Pop Content Height</label>
        <select id={getId('height')} name="disclosureHeight" value={state.disclosureHeight} onChange={handleSelectChange}>
          {generateOptions(HEIGHT_KEYS, 'height')}
        </select>
        <br />
        <br />
        <label htmlFor={getId('width')}>Pop Content Width</label>
        <select id={getId('width')} name="disclosureWidth" value={state.disclosureWidth} onChange={handleSelectChange}>
          {generateOptions(WIDTH_KEYS, 'width')}
        </select>
        <br />
        <br />
      </form>
    );
  }
  render() {
    const { disclosureManager, identifier } = props;
    return (
      <ContentContainer id={identifier} className="nested-component" fill header={<h2 className={cx('header')}>Content Component</h2>}>
        <p>id: {identifier}</p>
        {disclosureManager && disclosureManager.releaseFocus ? <p>Modal has lost focus!</p> : null}
        {disclosureManager && disclosureManager.requestFocus ? <p>Modal has gained focus!</p> : null}
        <button type="button" className="disclose" onClick={disclose()}>
          Disclose
        </button>
        <button type="button" className="disclose-tiny" onClick={disclose('tiny')}>
          Disclose Tiny
        </button>
        <button type="button" className="disclose-small" onClick={disclose('small')}>
          Disclose Small
        </button>
        <button type="button" className="disclose-medium" onClick={disclose('medium')}>
          Disclose Medium
        </button>
        <button type="button" className="disclose-large" onClick={disclose('large')}>
          Disclose Large
        </button>
        <button type="button" className="disclose-huge" onClick={disclose('huge')}>
          Disclose Huge
        </button>
        <button type="button" className="disclose-fullscreen" onClick={disclose('fullscreen')}>
          Disclose Fullscreen
        </button>
        <div className={cx('content-form-wrapper')}>
          {renderForm()}
          {renderFormButton()}
        </div>
        {disclosureManager && disclosureManager.dismiss ? (
          <button type="button" className="dismiss" onClick={dismiss}>
            Dismiss
          </button>
        ) : null}
        {disclosureManager && disclosureManager.closeDisclosure ? (
          <button type="button" className="close-disclosure" onClick={closeDisclosure}>
            Close Disclosure
          </button>
        ) : null}
        {disclosureManager && disclosureManager.goBack ? (
          <button type="button" className="go-back" onClick={goBack}>
            Go Back
          </button>
        ) : null}
        {disclosureManager && disclosureManager.maximize ? (
          <button type="button" className="maximize" onClick={maximize}>
            Maximize
          </button>
        ) : null}
        {disclosureManager && disclosureManager.minimize ? (
          <button type="button" className="minimize" onClick={minimize}>
            Minimize
          </button>
        ) : null}
        {disclosureManager && disclosureManager.requestFocus ? (
          <button type="button" className="requestFocus" onClick={requestFocus}>
            Request Focus
          </button>
        ) : null}
        {disclosureManager && disclosureManager.releaseFocus ? (
          <button type="button" className="releaseFocus" onClick={releaseFocus}>
            Release Focus
          </button>
        ) : null}
      </ContentContainer>
    );
  }
}
DisclosureComponent.defaultProps = {
  nestedIndex: 0,
};
const WrappedDisclosureComponent = withDisclosureManager(DisclosureComponent);
export default WrappedDisclosureComponent;
