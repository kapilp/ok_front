// import 'solid-js';
import { Switch, Match } from "solid-js";
import { render } from "solid-js/web";
import { Router, NavLink, pathIntegration } from "@rturnq/solid-router";
import { SvelteToast } from "@zerodevx/svelte-toast";
import * as serviceWorker from "./serviceWorker";
// import { lazy } from 'solid-js';
import { getIsLoggedIn } from "./utils/ws_events_dispatcher";
import { Chat } from "./components/UI/Chat";
import { ApplicationBase } from "./components/UI/terra-application/src/application-base/ApplicationBase";
import { NightMode } from "./components/UI/NightMode";
import { ScrollButton } from "./components/ScrollTopButon";
import { DragLine } from "./components/UI/DragLine";
import { ToggleProduction } from "./components/UI/ToggleProduction";
import { NavigateWsBind } from "./components/UI/Navigate";
import { GuestRoutes } from "./routes/GuestRoutes";
import { LogInRoutes } from "./routes/LogInRoutes";
import "./index.css";
// const StartupLogo = lazy(() => import('./codeSplit/startupLogo/StartupLogo'));
// @ts-ignore
// eslint-disable-next-line no-new
const app = new SvelteToast({
  target: document.body,
  props: {
    options: {},
  },
});

const App = function () {
  return (
    <ApplicationBase>
      <Router integration={pathIntegration()}>
        <NavigateWsBind />
        {/* <Suspense fallback={<StartupLogo />}> */}
        <Switch fallback={<div>Not Found</div>}>
          <Match when={getIsLoggedIn() === null}>Loading...</Match>
          <Match when={getIsLoggedIn() === false}>
            <>
              <Chat />
              {/* <NavLink href="/" end> */}
              {/*  Home */}
              {/* </NavLink> */}
              {/* <NavLink href="contact" end> */}
              {/*  Contact */}
              {/* </NavLink> */}
              {/* <NavLink href="about" end> */}
              {/*  About */}
              {/* </NavLink> */}
              {/* <ToggleProduction /> */}
              <ScrollButton scrollStepInPx="50" delayInMs="2" />
              <GuestRoutes />
            </>
          </Match>
          <Match when={getIsLoggedIn() === true}>
            <LogInRoutes />
          </Match>
        </Switch>
        <NightMode />
        <DragLine />
        {/* </Suspense> */}
      </Router>
    </ApplicationBase>
  );
};
render(() => <App />, document.getElementById("root")!);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
