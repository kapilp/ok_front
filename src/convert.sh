# https://unix.stackexchange.com/questions/102647/how-to-rename-multiple-files-in-single-command-or-script-in-unix
shopt -s globstar;
for file in **/*.{js,jsx}
do
    # echo "${file}"
    react-proptypes-to-typescript  "${file}"
    rm "${file}"
done
