import { JSX, createSignal } from "solid-js";

const portalClassName = 'TerraModalPortal'
export default (): Modal => {
  const [isOpen, setIsOpen] = createSignal<boolean>(false)
  const [content, setContent] = createSignal<JSX.Element>(null)
  const [config, setConfig] = createSignal<object>({})
  const [preventClose, setPreventClose] = createSignal<boolean>(false)

  const appendConfig = (object: object = {}) => {
    setConfig({ ...config, ...object })
  }

  const open = (content: JSX.Element = null, config: object = {}) => {
    appendConfig(config)
    setContent(content)
    setIsOpen(true)
  }

  const close = () => {
    !preventClose && setConfig({})
    !preventClose && setContent(null)
    !preventClose && setIsOpen(false)
  }

  const prevent = (prevent: boolean) => {
    setPreventClose(prevent)
    appendConfig({
      shouldCloseOnOverlayClick: !prevent,
      shouldCloseOnEsc: !prevent,
    })
  }

  const post = { onSubmitting: prevent, onSubmitted: close }
  const actions = { open, close, setContent, prevent, post }
  const defaultConfig = { isOpen, onRequestClose: close, portalClassName }
  return { ...actions, content, config: { ...defaultConfig, ...config } }
}
