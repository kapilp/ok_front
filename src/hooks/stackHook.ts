// StackChildHook
import {  createComputed  } from 'solid-js';
import { createStore,produce } from 'solid-js/store';

export const stackHook = () => {
  interface StackState {
    stack: any[];
    activeIndex: number;
    isBackDisabled: boolean;
    isNextDisabled: boolean
  }

  const [stackState, setStackState] = createStore<StackState>({
    stack: [] as any[],
    activeIndex: 0,
    isBackDisabled: true,
    isNextDisabled: true,
  });

  const setTitle = () => '';

  const onRequestClose = () => {};

  // each <Layout> call pop() on cleanup() to remove last menu
  // it keeps stack data valid
  const pop = () => {
    if (stackState.stack.length > 1) {
      setStackState(
        produce<StackState>(prevState => {
          prevState.stack.pop();
          prevState.activeIndex = prevState.stack.length - 1;
        }),
      );
    }
  };

  const push = (item: any) => {
    setStackState(
      produce<StackState>(prevState => {
        prevState.stack.push(item);
        prevState.activeIndex = prevState.stack.length - 1;
      }),
    );
  };

  createComputed(() => {
    setStackState(
      produce<StackState>(prevState => {
        prevState.isBackDisabled = stackState.activeIndex == 0;
        prevState.isNextDisabled = stackState.activeIndex >= stackState.stack.length - 1;
      }),
    );
  });

  const setActiveIndex = (index: number) => setStackState({ activeIndex: index });

  const onBack = () => {
    const newIndex = stackState.activeIndex - 1;
    if (newIndex <= 0) setActiveIndex(0);
    else setActiveIndex(newIndex);
  };

  const onNext = () => {
    const newIndex = stackState.activeIndex + 1;
    if (newIndex < stackState.stack.length) setActiveIndex(newIndex);
    else setActiveIndex(stackState.stack.length - 1);
  };

  return { stackState, pop, push, onRequestClose, setTitle, setActiveIndex, onBack, onNext };
};
