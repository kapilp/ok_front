import { createEffect } from "solid-js";
import { useRouter } from "@rturnq/solid-router";

export const useScrollToTop = () => {
  const { location } = useRouter();

  createEffect(() => {
    if(location)
      window.scrollTo(0, 0)
  })
}
