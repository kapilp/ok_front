/** @type {import('tailwindcss').Config} */
// For future if we plan to give color pallette
// const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{html,js,tsx,ts}"],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
