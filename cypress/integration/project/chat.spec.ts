import {
  adminChatPageOpen,
  goto_main_menu,
  login
} from "../../support/utils/utils";

const organizations = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Chat", () => {
  it("organization create edit delete test", () => {
    login();
    goto_main_menu();

    // 0. create organizations
    adminChatPageOpen();
    organizations.forEach(organization => {
      cy.get("button[name='table_add']").click();
      cy.contains("Name").parent().parent().find("input").type(organization.name);
      cy.contains("Submit").click();
    });
    organizations.forEach(organization => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(organization.name).should("be.visible");
      cy.contains(organization.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_main_menu();
    adminChatPageOpen();
    organizations.forEach(organization => {
      cy.contains(organization.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
