import {
  adminOrganizationPageOpen,
  adminOrganizationProjectPageOpen,
  adminOrganizationProjectWorkPackagePageOpen,
  goto_main_menu,
  login
} from "../../support/utils/utils";

const organizations = [];
for (let i = 1; i   <= 6 ; i++){
  const projects = [];
  for (let p = 1; p   <= 6 ; p++){
    const work_packages = [];
    for (let w = 1; w <= 6 ; w++){
      work_packages.push({name:`O${i}_P${p}-W${w}`})
    }
    projects.push({name:`O${i}_P${p}`,work_packages})
  }
  organizations.push({name:`O${i}`, projects})
}
/*
Total Organizations = 5
Total Projects = 125
Total Work Packages = 125
Total Documents = 155
Total Edges = 150
*/

describe("Organization", () => {
  /*it("organization create edit delete test", () => {
    login();
    goto_main_menu();

    // 0. create organizations
    adminOrganizationPageOpen();
    organizations.forEach(organization => {
      cy.get("button[name='table_add']").click();
      cy.contains("Name").parent().parent().find("input").type(organization.name);
      cy.contains("Submit").click();
    });
    organizations.forEach(organization => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(organization.name).should("be.visible");
      cy.contains(organization.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_main_menu();
    adminOrganizationPageOpen();
    organizations.forEach(organization => {
      cy.contains(organization.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });*/

  it("organization should not be deletable when it has projects", ()=>{
    // TODO
  })

  it("organization drop above working", ()=>{
    // TODO
  })
  it("organization drop below working", ()=>{
    // TODO
  })
  it("organization drop on center working", ()=>{
    // TODO
  })

  it("organization project work package creation update delete", () =>{
    login();
    goto_main_menu();

    // 0. create organizations
    adminOrganizationPageOpen();
    organizations.forEach(organization => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(organization.name);
      cy.contains("Name").parent().parent().find("input").type(organization.name);
      cy.contains("Submit").click();
      cy.contains("Submit").should("not.exist")
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(organization.name).should("be.visible");
      // go to project
      cy.contains(`${organization.name}456`).click();
      adminOrganizationProjectPageOpen()
      // projects create:
      organization.projects.forEach(project => {
      cy.get("button[name='table_add']").click();
      cy.contains("label", "Key").parent().parent().find("input").type(project.name); // Table also has "Name" Header
      cy.contains("label", "Name").parent().parent().find("input").type(project.name); // Table also has "Name" Header
      cy.contains("Submit").click();
        cy.contains("Submit").should("not.exist")
        cy.contains(`${project.name}456`).click();
        adminOrganizationProjectWorkPackagePageOpen()
        project.work_packages.forEach(work_package => {
          cy.get("button[name='table_add']").click();
          cy.contains("label", "Key").parent().parent().find("input").type(work_package.name);
          cy.contains("label", "ID").parent().parent().find("input").type(work_package.name);
          cy.contains("Submit").click();
          cy.contains("Submit").should("not.exist")
        });
        goto_main_menu()
        adminOrganizationProjectPageOpen()
    });
      goto_main_menu()
      adminOrganizationPageOpen();
    });

    // finally delete test document
    // goto_main_menu();
    // adminOrganizationPageOpen();
    // organizations.forEach(organization => {
    //   cy.contains(organization.name).parent().parent().parent().parent().find("button[name='delete']").click();
    //   cy.contains("Confirm Delete").click();
    // });
  })

});
