import {
  globalTranslationPageOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const translations = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Translation", () => {
  it("translation create edit delete test", () => {
    login();
    goto_global();

    // 0. create translations
    globalTranslationPageOpen();
    translations.forEach(translation => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(translation.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    translations.forEach(translation => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(translation.name).should("be.visible");
      cy.contains(translation.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalTranslationPageOpen();
    translations.forEach(translation => {
      cy.contains(translation.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
