import {
  globalSessionPageOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const sessions = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Session", () => {
  it("session create edit delete test", () => {
    login();
    goto_global();

    // 0. create sessions
    globalSessionPageOpen();
    sessions.forEach(session => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(session.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    sessions.forEach(session => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(session.name).should("be.visible");
      cy.contains(session.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalSessionPageOpen();
    sessions.forEach(session => {
      cy.contains(session.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
