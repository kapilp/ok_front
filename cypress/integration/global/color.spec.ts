import {
  adminColorPageOpen,
  checkKeyExist,
  globalColorPageOpen,
  goto_admin,
  goto_global,
  login,
  openEdit
} from "../../support/utils/utils";

const colors = [
  {
    key: "testName",
    hexCode: "testName"
  },
  {
    key: "westName",
    hexCode: "westName"
  }
];

describe("Color", () => {
  it("color create edit delete test", () => {
    login();
    goto_global();

    // 0. create colors
    globalColorPageOpen();
    colors.forEach(color => {
      cy.get("button[name='table_add']").click();
      cy.contains("label", "Key").parent().parent().find("input").type(color.key);
      cy.contains("label", "Hexcode").parent().parent().find("input").type(color.hexCode);
      cy.contains("label", "Code").parent().parent().find("input").type(color.hexCode);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    colors.forEach(color => {
      openEdit(color.hexCode);
      cy.contains("Submit").click();
    });

    // 1. goto administration, same colors should exist
    goto_admin();
    adminColorPageOpen();
    colors.forEach(color => {
      openEdit(color.hexCode);
      cy.contains("label", "Key").parent().parent().find("input").should("have.value", color.key);
      cy.contains("label", "Hexcode").parent().parent().find("input").should("have.value", color.hexCode);
      // 2. editing administration color not change global color
      cy.contains("label", "Hexcode").parent().parent().find("input").type("1");
      cy.contains("Submit").click();
    });

    goto_global();
    globalColorPageOpen();
    colors.forEach(color => {
      openEdit(color.hexCode);
      cy.contains("label", "Key").parent().parent().find("input").should("have.value", color.key);
      cy.contains("label", "Code").parent().parent().find("input").should("have.value", color.hexCode);
      // 3. editing global colors again changes administration colors
      cy.contains("label", "Hexcode").parent().parent().find("input").clear().type(color.hexCode);
      cy.contains("Submit").click();
    });

    goto_admin();
    adminColorPageOpen();
    colors.forEach(color => {
      openEdit(color.hexCode);
      cy.contains("label", "Key").parent().parent().find("input").should("have.value", color.key);
      cy.contains("label", "Hexcode").parent().parent().find("input").should("have.value", color.hexCode);
      cy.contains("Cancel").click();
    });

    // TODO 4. administration level colors cant deleted if its global
    // TODO 5. add validation: color key must be provided on create.

    // finally delete test document
    goto_global();
    globalColorPageOpen();
    colors.forEach(color => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(color.hexCode).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });

    // It deletes admin documents too
    goto_admin();
    adminColorPageOpen();
    colors.forEach(color => {
      checkKeyExist(color.hexCode, false);
    });
  });
});
