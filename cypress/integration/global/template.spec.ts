import {
  globalTemplatePageOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const templates = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Template", () => {
  it("template create edit delete test", () => {
    login();
    goto_global();

    // 0. create translations
    globalTemplatePageOpen();
    templates.forEach(template => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(template.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    templates.forEach(template => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(template.name).should("be.visible");
      cy.contains(template.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalTemplatePageOpen();
    templates.forEach(template => {
      cy.contains(template.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
