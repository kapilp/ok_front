import {
  globalSchemaPageOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const schemas = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Schema", () => {
  it("translation create edit delete test", () => {
    login();
    goto_global();

    // 0. create schemas
    globalSchemaPageOpen();
    schemas.forEach(schema => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(schema.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    schemas.forEach(schema => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(schema.name).should("be.visible");
      cy.contains(schema.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalSchemaPageOpen();
    schemas.forEach(schema => {
      cy.contains(schema.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
