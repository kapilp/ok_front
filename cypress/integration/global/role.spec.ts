import {
  adminRoleOpenEdit,
  adminRolePageOpen,
  globalRolePageOpen,
  goto_admin,
  goto_global,
  login
} from "../../support/utils/utils";

const roles = [
  {
    name: "testName",
    permissions: [
      { name: "meeting", attributes: ["view", "create", "edit", "delete"] },
      { name: "project", attributes: ["list", "create", "edit", "delete"] }
    ]
  },
  {
    name: "westName",
    permissions: [
      { name: "meeting", attributes: ["view", "create", "edit", "delete"] },
      { name: "project", attributes: ["list", "create", "edit", "delete"] }
    ]
  }
];

describe("Role", () => {
  it("role create edit delete test", () => {
    login();

    // 0. create roles
    goto_global();
    globalRolePageOpen();
    roles.forEach(permission => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(`${permission.name}`);
      cy.contains("Id").parent().parent().find("input").type(`${permission.name}`);
      permission.permissions.forEach(permission => {
        cy.contains("Add").click();
        cy.get(".permissions").find("select").last().select(permission.name);
        permission.attributes.forEach(permission => {
          cy.get(".permissions").find("select").last().parent().parent().contains(permission).find("input").check();
        });
      });
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    roles.forEach(permission => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(permission.name).should("be.visible");
      cy.contains(permission.name).parent().parent().parent().parent().find("button[name='edit']").click();
      cy.contains("Submit").click();
    });

    // goto administration, same roles should exist
    goto_admin();
    adminRolePageOpen();
    roles.forEach((role) => {
      adminRoleOpenEdit(role.name);
      role.permissions.forEach((permission) => {
        cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().should("have.value", permission.name);
        permission.attributes.forEach(attribute => {
          cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().parent().parent().contains(attribute).find("input").should("have.checked");
        });
      });
      // 2. editing administration permission not change global permission
      role.permissions.forEach((permission) => {
        cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().should("have.value", permission.name);
        permission.attributes.forEach(attribute => {
          cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().parent().parent().contains(attribute).find("input").uncheck();
        });
      });
      cy.contains("Submit").click();
    });

    goto_global();
    globalRolePageOpen();
    roles.forEach(role => {
      adminRoleOpenEdit(role.name);
      role.permissions.forEach(permission => {
        cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().should("have.value", permission.name);
        permission.attributes.forEach(attribute => {
          cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().parent().parent().contains(attribute).find("input").should("have.checked");
        });
        // 3. editing global permissions again changes administration permissions
        //cy.get('.permissions').find(`:selected[value='${p1.name}']`).parent().clear().type(permission);
      });
      cy.contains("Submit").click();
    });

    goto_admin();
    adminRolePageOpen(true);
    roles.forEach(role => {
      adminRoleOpenEdit(role.name);
      role.permissions.forEach(permission => {
        // cy.get('.permissions').find('select').should('have.value', permission.name);
        permission.attributes.forEach(attribute => {
          cy.get(".permissions").find(`:selected[value='${permission.name}']`).parent().parent().parent().contains(attribute).find("input").should("have.checked");
        });
      });
      cy.contains("Cancel").click();
    });

    // TODO 4. administration level permissions cant deleted if its global
    // TODO 5. add validation: permission key must be provided on create.

    // finally delete test document
    goto_global();
    globalRolePageOpen();
    roles.forEach(role => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(role.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
