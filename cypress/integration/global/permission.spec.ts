import {
  adminPermissionOpenEdit,
  adminPermissionPageOpen,
  globalPermissionPageOpen,
  goto_admin,
  goto_global,
  goToBottom,
  login
} from "../../support/utils/utils";

const permissions = [
  {
    name: "testName",
    permissions: ["create", "list", "update", "delete"]
  },
  {
    name: "westName",
    permissions: ["access_1", "access_2", "access_3", "access_4"]
  }
];

describe("Permission", () => {
  it("permission create edit delete test", () => {
    login();

    // 0. create permissions
    goto_global();
    globalPermissionPageOpen();
    permissions.forEach(permission => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(`${permission.name}`);
      permission.permissions.forEach(permission => {
        cy.contains("Add").click();
        cy.get(".permissions").find("input").last().type(permission);
      });
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    permissions.forEach(permission => {
      adminPermissionOpenEdit(permission.name);
      cy.contains("Submit").click();
    });

    // 1. goto administration, same permissions should exist
    goto_admin();
    adminPermissionPageOpen();
    permissions.forEach(permission => {
      adminPermissionOpenEdit(permission.name);
      permission.permissions.forEach((permission, i) => {
        cy.get(".permissions").find("input").eq(i).should("have.value", permission);
      });
      // 2. editing administration permission not change global permission
      permission.permissions.forEach((permission, i) => {
        cy.get(".permissions").find("input").eq(i).type(`${i}`);
      });
      cy.contains("Submit").click();
    });

    goto_global();
    globalPermissionPageOpen();
    permissions.forEach(permission => {
      adminPermissionOpenEdit(permission.name);
      permission.permissions.forEach((permission, i) => {
        cy.get(".permissions").find("input").eq(i).should("have.value", permission);
        // 3. editing global permissions again changes administration permissions
        cy.get(".permissions").find("input").eq(i).clear().type(permission);
      });
      cy.contains("Submit").click();
    });

    goto_admin();
    adminPermissionPageOpen(true);
    permissions.forEach(permission => {
      adminPermissionOpenEdit(permission.name);
      permission.permissions.forEach((permission, i) => {
        cy.get(".permissions").find("input").eq(i).should("have.value", permission);
      });
      cy.contains("Cancel").click();
    });

    // TODO 4. administration level permissions cant deleted if its global
    // TODO 5. add validation: permission key must be provided on create.

    // finally delete test document
    goto_global();
    globalPermissionPageOpen();
    permissions.forEach(permission => {
      goToBottom();
      cy.contains(permission.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
