import {
  globalUserPageOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const users = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("User", () => {
  it("user create edit delete test", () => {
    login();
    goto_global();

    // 0. create users
    globalUserPageOpen();
    users.forEach(user => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(user.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    users.forEach(user => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(user.name).should("be.visible");
      cy.contains(user.name).parent().parent().parent().parent().find("button[name='edit']").click();
      // cy.contains('label', 'Name').parent().parent().find('input').type('testName2{enter}');
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalUserPageOpen();
    users.forEach(user => {
      cy.contains(user.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });
  });
});
