import {
  globalConfirmOpen,
  goto_global,
  login
} from "../../support/utils/utils";

const confirms = [
  {
    name: "testName"
  },
  {
    name: "westName"
  }
];

describe("Confirm", () => {
  it("confirm create edit delete test", () => {
    login();
    goto_global();

    // 0. create confirms
    globalConfirmOpen();
    // TODO: confirm mutation actor event is disabled because using customized confirm mutation
    /*confirms.forEach(confirm => {
      cy.get("button[name='table_add']").click();
      cy.contains("Key").parent().parent().find("input").type(confirm.name);
      cy.contains("Submit").click();
    });

    // Test rows exists and edit is successful
    confirms.forEach(confirm => {
      cy.get("#main-container").scrollTo("bottom", { ensureScrollable: false });
      cy.contains(confirm.name).should("be.visible");
      cy.contains(confirm.name).parent().parent().parent().parent().find("button[name='edit']").click();
      cy.contains("Submit").click();
    });

    // finally delete test document
    goto_global();
    globalConfirmOpen();
    confirms.forEach(confirm => {
      cy.contains(confirm.name).parent().parent().parent().parent().find("button[name='delete']").click();
      cy.contains("Confirm Delete").click();
    });*/
  });
});
