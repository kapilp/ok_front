import { login } from "../../support/utils/utils";

describe("login", () => {
  it("logs in user with valid credentials", () => {
    login();

    cy.url().should("include", "/account/confirm");
    cy.getCookie("jwt").should("exist");
    // cy.contains('Signed in successfully.').should('be.visible');
    cy.contains("k1@k1.com").click();
    cy.contains("Sign Out").click();
    cy.url().should("include", "/");
    cy.getCookie("jwt").should("not.exist");
  });

  it("does not sign in user with invalid credentials", () => {
    login("wrong_email@example.com", "password");

    cy.contains("not found").should("be.visible");
    // Todo error should be : Invalid Email or password.
  });

  it("test email confirm is working properly", ()=>{
    // TODO
  })

  it("password change successful", ()=>{

  })
});
