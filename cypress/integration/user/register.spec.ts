import { login, super_register } from "../../support/utils/utils";

describe("super register", () => {
  it("super register user with valid credentials", () => {
    super_register();
    cy.url().should("include", "/account/confirm");
    cy.getCookie("jwt").should("exist");
    cy.pause()
    // cy.contains('Signed in successfully.').should('be.visible');
    cy.contains("k1@k1.com").click();
    cy.contains("Sign Out").click();
    cy.url().should("include", "/");
    cy.getCookie("jwt").should("not.exist");
  });
});
