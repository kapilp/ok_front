export const globalColorPageOpen = () => {
  cy.contains('Color').click();
};
export const globalPermissionPageOpen = () => {
  cy.contains('Permission').click();
};
export const globalRolePageOpen = () => {
  cy.contains('Role').click();
};
export const globalSchemaPageOpen = () => {
  cy.contains('Schema').click();
};
export const globalSessionPageOpen = () => {
  cy.contains('Session').click();
};
export const globalTemplatePageOpen = () => {
  cy.contains('Translation').click();
};
export const globalTranslationPageOpen = () => {
  cy.contains('Translation').click();
};
export const globalUserPageOpen = () => {
  cy.contains('User').click();
};
export const globalConfirmOpen = () => {
  cy.contains('Confirm').click();
};

export const adminPermissionPageOpen = (isMenuOpen = false) => {
  if (!isMenuOpen) cy.contains('Members & Permissions').click();
  cy.contains('Members & Permissions').siblings().contains('Permissions').click();
};
export const adminRolePageOpen = (isMenuOpen = false) => {
  if (!isMenuOpen) cy.contains('Members & Permissions').click();
  cy.contains('Members & Permissions').siblings().contains('Roles').click();
};
export const adminColorPageOpen = () => {
  cy.contains('Colors').click();
};
export const adminOrganizationPageOpen = () => {
  cy.contains('Organizations').click();
};
export const adminChatPageOpen = () => {
  cy.contains('Chat').click();
};
export const adminOrganizationProjectPageOpen = () => {
  cy.contains('Projects').click();
};
export const adminOrganizationProjectWorkPackagePageOpen = () => {
  cy.contains('Work packages').click();
};
export const goToBottom = () => {
  cy.get('#main-container').scrollTo('bottom', { ensureScrollable: false });
};
export const checkKeyExist = (name, isExist = true) => {
  goToBottom();
  if (isExist) {
    cy.contains(name).should(`be.visible`);
  } else {
    cy.contains(name).should('not.exist');
  }
};
export const openEdit = name => {
  checkKeyExist(name);
  cy.contains(name).parent().parent().parent().parent().find('button[name="edit"]').click();
};

export const adminPermissionOpenEdit = name => {
  checkKeyExist(name);
  cy.contains(name).parent().parent().parent().parent().find('button[name="edit"]').click();
};
export const adminRoleOpenEdit = name => {
  checkKeyExist(name);
  cy.contains(name).parent().parent().parent().parent().find('button[name="edit"]').click();
};

export function super_register(email = 'k1@k1.com', password = 'k123456', url="c1") {
  cy.visit('http://admin.chat.l.com/super_register');
  //cy.contains('Login').click();
  cy.contains('Email').parent().parent().find('input').type(email);
  cy.contains('Password').parent().parent().find('input').type(password);
  cy.contains('Url').parent().parent().find('input').type(url);
  cy.contains('Submit').click();
}
export function login(email = 'k1@k1.com', password = 'k123456') {
  cy.visit('/');
  cy.contains('Login').click();
  cy.contains('Email').parent().parent().find('input').type(email);
  cy.contains('Password').parent().parent().find('input').type(password);
  cy.contains('Submit').click();
}

export function goto_admin() {
  cy.contains('k1@k1.com').click();
  cy.contains('Administration').click();
}

export function goto_global() {
  cy.contains('Global').click();
}

export function goto_main_menu() {
  // 1st way:
  cy.get('.back_button')
    .invoke('attr', 'disabled')
    .then(disabled => {
      disabled ? cy.log('buttonIsDisabled') : cy.get('.back_button').click();
    });
  // 2nd way:
  // cy.get('.back_button').then($btn => {
  //   if ($btn.is(':disabled')) {
  //   } else {
  //     cy.wrap($btn).click();
  //   }
  // });
}

// cy.get('input[name=description]').then(els => {
//   [...els].forEach(el => cy.wrap(el).type('Hello World'));
// });
// [...els] converts Cypress array to normal array, so you can forEach().
