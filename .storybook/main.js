const postcss = require('postcss');

module.exports = {
  webpackFinal(config, options) {
    // extend config here

    return config;
  },
  stories: [
    // "../src/**/*.stories.mdx",
    '../src/stories/**/*.stories.@(js|jsx|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-a11y',
    '@storybook/addon-cssresources',
    {
      name: '@storybook/addon-postcss',
      options: {
        postcssLoaderOptions: {
          implementation: postcss,
        },
      },
    },
    {
      name: '@storybook/preset-scss',
      options: {
        cssLoaderOptions: {
          modules: true,
        },
      },
    },
  ],
  features: {
    storyStoreV7: true,
  },
  // "core": {
  //   "builder": "storybook-builder-vite"
  // }
};
