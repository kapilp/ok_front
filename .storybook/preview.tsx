import { Base } from "../src/components/UI/core/base/src/Base";
import { createRoot } from "solid-js";
const { withInfo } = require("@storybook/addon-info");
import "./index.css";

// https://github.com/rturnq/solid-storybook-example

// Global CSS Imports
// import '../static/global.css';

document.getElementsByTagName("html")[0].setAttribute("dir", "ltr");
export const decorators = [
  (storyFn) =>
    createRoot(() => (
      <Base>
        <div>{storyFn()}</div>
      </Base>
    )),
];

// use this instead:
/*export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  decorators: [withInfo],
};*/
