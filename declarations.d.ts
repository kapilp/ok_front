declare module '*.svg'
declare module '*.png'
declare module '*.jpg'
// instead use typescript-plugin-css-modules
// declare module '*.scss' {
//     const content: Record<string, string>;
//     export default content;
// }
// declare module '*.module.css' {
//     const classes: { [key: string]: string };
//     export default classes;
//   }
  
//   declare module '*.module.scss' {
//     const classes: { [key: string]: string };
//     export default classes;
//   }
  
//   declare module '*.module.sass' {
//     const classes: { [key: string]: string };
//     export default classes;
//   }
  
//   declare module '*.module.less' {
//     const classes: { [key: string]: string };
//     export default classes;
//   }
  
//   declare module '*.module.styl' {
//     const classes: { [key: string]: string };
//     export default classes;
//   }